import React from "react";
import { withRouter } from "react-router-dom";
import {
  OtosalesDatePicker,
  OtosalesSegmentDropdown,
  OtosalesSelectFullviewv2,
  Util
} from "../../../otosalescomponents";

export default withRouter(function DownloadReport(props) {
  return (
    <React.Fragment>
      <OtosalesSegmentDropdown title="Report" divid="reportfollowup">
        <React.Fragment>
          <div className="col-xs-12 col-md-3 panel-body-list" style={{marginBottom: "10px"}}>
            {/* <div className="col-xs-12 col-md-4 panel-body-list">
              <span className="labelspanbold">Type Report</span>
            </div> */}
            <div className="col-xs-12 col-md-11 panel-body-list">
              <OtosalesSelectFullviewv2
                name="typeReportDownload"
                value={props.state.typeReportDownload}
                selectOption={props.state.typeReportDownload}
                onOptionsChange={props.onOptionSelect2Change}
                options={props.state.datatypeReportDownload}
                placeholder="pilih tipe report"
              />
            </div>
          </div>
          <div className="col-xs-12 col-md-3 panel-body-list" style={{marginBottom: "10px"}}>
            <div className="col-xs-12 col-md-4 panel-body-list" style={{paddingTop: "2px"}}>
              <span className="labelspanbold">Period</span>
            </div>
            <div className="col-xs-12 col-md-7 panel-body-list">
              <OtosalesDatePicker
                maxDate={Util.convertDate().setFullYear(
                  Util.convertDate().getFullYear() + 10
                )}
                minDate={Util.convertDate().setFullYear(
                  Util.convertDate().getFullYear() - 10
                )}
                className="form-control"
                name="periodfromdownload"
                selected={props.state.periodfromdownload}
                dateFormat="dd mmmm yyyy"
                onChange={props.onChangeFunctionDate}
              />
            </div>
          </div>
          <div className="col-xs-12 col-md-3 panel-body-list" style={{marginBottom: "10px"}}>
            <div className="col-xs-12 col-md-1 panel-body-list" style={{paddingTop: "2px"}}>
              <span className="labelspanbold">to</span>
            </div>
            <div className="col-xs-12 col-md-7 panel-body-list">
              <OtosalesDatePicker
                maxDate={
                  Util.isNullOrEmpty(props.state.periodfromdownload)
                    ? Util.convertDate().setFullYear(
                        Util.convertDate().getFullYear() + 10
                      )
                    : Util.convertDate(
                        Util.formatDate(
                          props.state.periodfromdownload,
                          "yyyy-mm-dd"
                        )
                      ).setMonth(
                        Util.convertDate(
                          Util.formatDate(
                            props.state.periodfromdownload,
                            "yyyy-mm-dd"
                          )
                        ).getMonth() + 1
                      )
                }
                minDate={
                  Util.isNullOrEmpty(props.state.periodfromdownload)
                    ? Util.convertDate().setFullYear(
                        Util.convertDate().getFullYear() - 10
                      )
                    : props.state.periodfromdownload
                }
                className="form-control"
                name="periodtodownload"
                selected={props.state.periodtodownload}
                dateFormat="dd mmmm yyyy"
                onChange={props.onChangeFunctionDate}
              />
            </div>
          </div>
          <div className="col-xs-12 col-md-3 text-center">
              <a className="form-control btn btn-sm btn-info" onClick={() => props.handleDownloadReportData()} ><span className="fa fa-download"/> Download</a>
          </div>
        </React.Fragment>
      </OtosalesSegmentDropdown>
    </React.Fragment>
  );
});
