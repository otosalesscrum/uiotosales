import React from 'react';
import { withRouter } from "react-router-dom";
import { Util } from '../../../otosalescomponents';

export default withRouter(function PageHeaderDashboard(props) {
    let header = null;

    if (!Util.isNullOrEmpty(props.dataHeader)) {
        header = props.dataHeader.map((data, i) => {
            let listParam = data.listParam.map((datalistparam, j) => {
                return (
                    <React.Fragment key={j}>
                        <p><b>{datalistparam.title}</b></p>
                        <p>{datalistparam.value}</p>
                    </React.Fragment>
                );
            })
            let backgroundArray = ["bg-aqua", "bg-green", "bg-orange", "bg-red"]
            return (
                <div className="col-xs-6 col-md-3" key={i}>
                    <div className={"small-box "+backgroundArray[i%4]}>
                        <div className="inner" style={{ height:150 }}>
                            {listParam}
                        </div>
                        <div className="icon">
                            <i className="fa fa-address-book"></i>
                        </div>
                        <a href="#" className="small-box-footer"><b>{data.name}</b></a>
                    </div>
                </div>
            );
        });
    }

    return header;
});
