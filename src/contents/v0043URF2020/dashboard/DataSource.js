import {
    API_URL,
    API_VERSION_2,
    HEADER_API,
    ACCOUNTDATA,
    API_VERSION_0001URF2020
  } from "../../../config";
  import {
    Util,
  } from "../../../otosalescomponents";

  export const getNewDashboard = async(param) => {
      return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getNewDashboard`,
        "POST",
        HEADER_API,
        param
      )
      .then(res => res.json())
  }

  export const downloadReport = async (param) => {
      return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/DownloadReport`,
        "POST",
        HEADER_API,
        param
      )
        .then(res => res.json())
  }