import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import {
  API_URL,
  API_VERSION,
  API_VERSION2,
  API_VERSION_2,
  ACCOUNTDATA,
  HEADER_API,
  API_VERSION_0219URF2019
} from "../../../config";
import {
  Tabs,
  OtosalesModalPremiumSimulation,
  OtosalesModalAccessories,
  OtosalesModalInfo,
  OtosalesModalSendEmail,
  OtosalesModalSendSMS,
  OtosalesModalSetPeriodPremi2,
  OtosalesLoading,
  Util,
  Log
} from "../../../otosalescomponents";
import { ToastContainer, toast } from "react-toastify";
import Header from "../../../components/Header";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";
import TypeProspect from "./SubContent/TypeProspect";
import PremiCalculation from "./SubContent/PremiCalculation";
import PremiEstimation from "./SubContent/PremiEstimation";
import SummaryDetails from "./SubContent/SummaryDetails";
import * as DataSource from "./DataSource";

// const tabstitle = ["Vehicle Info", "Cover", "Summary"];
const tabstitle = ["Premi Calculation", "Premi Estimation", "Summary Details"];
class PagePremiumSimulation extends Component {
  constructor(props) {
    super(props);

    var activetab = null;

    if (JSON.parse(secureStorage.getItem("premiumsimulation")) != null) {
      activetab = JSON.parse(secureStorage.getItem("premiumsimulation"))
        .activetab;
    }
    this.hitratecalculationsimulation = true;

    this.state = JSON.parse(secureStorage.getItem("premiumsimulation")) || {
      GuidTempPenawaran: "",
      FollowUpNumber: "",
      OrderNoQuotationEdit: "",
      FollowUpNoQuotationEdit: "",
      showModal: false,
      showdialogperiod: false,
      showModalAddCopy: false,
      showModalMakeSure: false,
      showModalSendEmail: false,
      showModalAccessories: false,
      showModalInfo: false,
      MessageAlertCover: "",
      sendEmailOrSMS: "",

      ProspectName: "",
      ProspectPhone1: "",
      ProspectPhone2: "",
      ProspectEmail: "",
      OrderNo: "",
      CustId: "",
      Phone1: "",

      vehicleperiodfrom: null,
      vehicleperiodto: null,
      vehicleperiodfrommodalperiod: null,
      vehicleperiodtomodalperiod: null,
      isErrPeriodFrom: false,
      isErrPeriodTo: false,

      IsSRCCCheckedEnable: 1,
      IsETVCheckedEnable: 1,
      IsFLDCheckedEnable: 1,
      IsTRSCheckedEnable: 1,
      IsPADRIVERCheckedEnable: 1,
      IsPAPASSCheckedEnable: 1,

      IsSRCCCheckedEnableDays: 0,
      IsETVCheckedEnableDays: 0,
      IsFLDCheckedEnableDays: 0,

      isErrIsMvGodig: false,

      isErrVehBrand: false,
      isErrVehYear: false,
      isErrVehType: false,
      isErrVehSeries: false,
      isErrVehUsage: false,
      isErrVehRegion: false,
      isErrVehRegistrationNumber: false,

      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrCovSumInsured: false,
      isLoading: false,
      activetab: activetab || tabstitle[0],
      tabsDisabledState: [false, true, true],

      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      SalesOfficerRole: JSON.parse(ACCOUNTDATA).UserInfo.User.Role,

      isDisableIsMvGodig: false,
      isMvGodig: false,
      isNonMvGodig: false,

      isErrProsName: false,
      isErrPhone1: false,
      isErrEmail: false,
      Name: "",
      CustomerPhone: "",
      Email: "",
      RegistrationNumber: "",
      ChasisNumber: "",
      EngineNumber: "",
      isCompany: false,
      VehicleUsedCar: false,
      VehicleCodetemp: "",
      VehicleCode: "",
      datavehiclecode: [],
      VehicleDetailsTemp: "",
      VehicleDetails: "",
      datavehicledetails: [],
      datavehicledetailsall: [],
      VehicleBrand: "",
      datavehiclebrand: [],
      VehicleYear: 0,
      datavehicleyear: [],
      VehicleType: "",
      datavehicletype: [],
      VehicleSeries: "",
      datavehicleseries: [],
      VehicleUsage: "",
      datavehicleusage: [],
      VehicleRegion: "",
      datavehicleregion: [],
      VehicleRegistrationNumber: "",
      VehicleChasisNumber: "",
      VehicleEngineNumber: "",
      vehicleModelCode: "",
      Ndays: null,

      AccessSI: "",
      tempCovSumInsured: 0,
      CoverageId: "",
      dataextsi: [],

      VehicleProductTypeCode: "",
      VehiclePrice: "",
      VehicleTypeType: "",
      VehicleSitting: 0,
      VehicleSumInsured: "",
      loadingvehicletocover: false,

      CoverProductType: "",
      datacoverproducttype: [],
      CoverProductCode: "",
      datacoverproductcode: [],
      datacoverproductcodeall: [],
      CoverBasicCover: "",
      datacoverbasiccover: [],
      CovSumInsured: "0",
      CoverTLOPeriod: 0,
      CoverComprePeriod: 0,
      CoverLastInterestNo: 0,
      CoverLastCoverageNo: 0,
      dataMixCover: [
        "33",
        "34",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42"
      ],

      CalculatedPremiItems: [],
      coveragePeriodItems: [],
      coveragePeriodNonBasicItems: [],
      IsTPLEnabled: 0,
      IsTPLChecked: 0,
      IsTPLSIEnabled: 0,
      IsSRCCChecked: 0,
      IsSRCCEnabled: 0,
      IsFLDChecked: 0,
      IsFLDEnabled: 0,
      IsETVChecked: 0,
      IsETVEnabled: 0,
      IsTSChecked: 0,
      IsTSEnabled: 0,
      IsPADRVRChecked: 0,
      IsPADRVREnabled: 0,
      IsPADRVRSIEnabled: 0,
      IsPASSEnabled: 0,
      IsPAPASSSIEnabled: 0,
      IsPAPASSEnabled: 0,
      IsPAPASSChecked: 0,
      IsACCESSChecked: 0,
      IsACCESSEnabled: 0,
      IsACCESSSIEnabled: 0,
      SRCCPremi: 0,
      FLDPremi: 0,
      ETVPremi: 0,
      TSPremi: 0,
      PADRVRPremi: 0,
      PAPASSPremi: 0,
      TPLPremi: 0,
      ACCESSPremi: 0,
      AdminFee: 0,
      TotalPremi: 0,
      Alert: "",
      tempACCESSOVER: 0,

      MultiYearF: false,
      dataorderdetailcoverageall: [],
      orderDetailCoverage: "",

      chOne: 0,
      chTwo: 0,
      chThree: 0,
      chOneEnabled: 0,
      chTwoEnable: 0,
      chThreeEnabled: 0,
      chOneEnabledVisible: 0,
      chTwoEnableVisible: 0,
      chThreeEnabledVisible: 0,

      TPLCoverageId: null,
      dataTPLSI: [],
      dataAccessories: [],
      PASSCOVER: 0,
      PAPASSICOVER: 0,
      PADRVCOVER: 0,
      ACCESSCOVER: 0,

      isErrorProspect: false,
      isErrorVehicle: false,
      isErrorCover: false,
      messageError: "",

      chItemSetPeriod: false,
      chItemSetPeriodEnable: 0,

      isYearTwoExist: false,
      isYearThreeExist: false,
      isYearFourExist: false,
      isYearFiveExist: false,
      datacoverage: [],
      datas: null,
      TotalYear: "",
      TPLSICOVER: 0,
      odcModel: {
        CascoSI1: 0,
        CascoSI2: 0,
        CascoSI3: 0,
        CascoSI4: 0,
        CascoSI5: 0,
        CascoAccess1: 0,
        CascoAccess2: 0,
        CascoAccess3: 0,
        CascoAccess4: 0,
        CascoAccess5: 0,
        AccessSI1: 0,
        AccessSI2: 0,
        AccessSI3: 0,
        AccessSI4: 0,
        AccessSI5: 0,
        CascoRate1: 0,
        CascoRate2: 0,
        CascoRate3: 0,
        CascoRate4: 0,
        CascoRate5: 0,
        LoadingRate1: 0,
        LoadingRate2: 0,
        LoadingRate3: 0,
        LoadingRate4: 0,
        LoadingRate5: 0,
        BasicPremium1: 0,
        BasicPremium2: 0,
        BasicPremium3: 0,
        BasicPremium4: 0,
        BasicPremium5: 0,
        BundlingPremium1: 0,
        BundlingPremium2: 0,
        BundlingPremium3: 0,
        BundlingPremium4: 0,
        BundlingPremium5: 0,
        IsBundling1: 0,
        IsBundling2: 0,
        IsBundling3: 0,
        IsBundling4: 0,
        IsBundling5: 0,
        IsBundlingTRS1: 0,
        IsBundlingTRS2: 0,
        IsBundlingTRS3: 0,
        IsBundlingTRS4: 0,
        IsBundlingTRS5: 0,
        TSPremium1: 0,
        TSPremium2: 0,
        TSPremium3: 0,
        TSPremium4: 0,
        TSPremium5: 0,
        TPLPremium1: 0,
        TPLPremium2: 0,
        TPLPremium3: 0,
        TPLPremium4: 0,
        TPLPremium5: 0,
        PADRVRPremium1: 0,
        PADRVRPremium2: 0,
        PADRVRPremium3: 0,
        PADRVRPremium4: 0,
        PADRVRPremium5: 0,
        PADRVRRate1: 0,
        PADRVRRate2: 0,
        PADRVRRate3: 0,
        PADRVRRate4: 0,
        PADRVRRate5: 0,
        PAPASSRate1: 0,
        PAPASSRate2: 0,
        PAPASSRate3: 0,
        PAPASSRate4: 0,
        PAPASSRate5: 0,
        TPLRate1: 0,
        TPLRate2: 0,
        TPLRate3: 0,
        TPLRate4: 0,
        TPLRate5: 0,
        TSRate1: 0,
        TSRate2: 0,
        TSRate3: 0,
        TSRate4: 0,
        TSRate5: 0,
        PAPASSPremium1: 0,
        PAPASSPremium2: 0,
        PAPASSPremium3: 0,
        PAPASSPremium4: 0,
        PAPASSPremium5: 0,
        ExtendPremium1: 0,
        ExtendPremium2: 0,
        ExtendPremium3: 0,
        ExtendPremium4: 0,
        ExtendPremium5: 0,
        BExtendPremium1: 0,
        BExtendPremium2: 0,
        BExtendPremium3: 0,
        BExtendPremium4: 0,
        BExtendPremium5: 0,
        TotalPremium1: 0,
        TotalPremium2: 0,
        TotalPremium3: 0,
        TotalPremium4: 0,
        TotalPremium5: 0,
        TPLSI: 0,
        PADRVRSI: 0,
        PASS: 0,
        PAPASSSI: 0,
        AdminFee: 0,

        // 0283/URF/2015 BSY
        BundlingPremiumRate: 0,
        TSPremiumRate: 0,
        TPLPremiumRate: 0,
        PADRVRPremiumRate: 0,
        PAPASSPremiumRate: 0
        // END
      }
    };
  }

  formatMoney = (amount, decimalCount = 0, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      // console.log(e);
    }
  };

  loaddataheader = (followupno, branchcode, channelsource) => {
    this.setState({ isLoading: true });
    DataSource.getOrderDetailHeader(followupno, branchcode, channelsource)
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          spModels: datamapping.spModels,
          BranchData: datamapping.BranchData,
          param: datamapping.param,
          datas: { ...this.state.datas, City: datamapping.BranchData.City },
          isLoading: false
        });
        this.setParams(datamapping.spModels);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
          // this.loaddataheader(followupno, branchcode, channelsource);
        }
      });
  };

  setOrderDetailCoverage = odlcModels => {
    var odcModelState = { ...this.state.odcModel };
    for (let i = 0; i < odlcModels.length; i++) {
      const item = odlcModels[i];
      Log.debugGroup("odlcModels: ", odlcModels);
      if (item.Year == 1) {
        this.setState(
          {
            isYearTwoExist: false,
            isYearThreeExist: false,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "1";
          }
        );

        odcModelState.CascoSI1 = item.VehiclePremi;
        odcModelState.AccessSI1 = item.AccessPremi;
        odcModelState.CascoAccess1 = item.VehicleAccessPremi;
        odcModelState.CascoRate1 = item.Rate;
        odcModelState.LoadingRate1 = item.LoadingPremi;
        odcModelState.BasicPremium1 = item.BasicPremi;
        odcModelState.BundlingPremium1 = item.BasiCoverage;
        odcModelState.TSPremium1 = item.TSPremi;
        odcModelState.TSCoverage1 = item.TSCoverage;
        odcModelState.TPLPremium1 = item.TPLCoverage;
        odcModelState.PADRVRPremium1 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium1 = item.PAPASSCoverage;
        odcModelState.ExtendPremium1 = item.PremiPerluasan;
        odcModelState.BExtendPremium1 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium1 = item.TotalPremi;
        odcModelState.PADRVRRate1 = item.PADRVRRate;
        odcModelState.PAPASSRate1 = item.PAPASSRate;
        odcModelState.TPLRate1 = item.TPLRate;
        odcModelState.TSRate1 = item.TSRate;
        odcModelState.IsBundling1 = item.IsBundling;
        odcModelState.IsBundlingTRS1 = item.IsBundlingTRS;
        odcModelState.SFEPremi1 = item.SFEPremi;
        odcModelState.SFERate1 = item.SFERate;
      } else if (item.Year == 2) {
        odcModelState.CascoSI2 = item.VehiclePremi;
        odcModelState.AccessSI2 = item.AccessPremi;
        odcModelState.CascoAccess2 = item.VehicleAccessPremi;
        odcModelState.CascoRate2 = item.Rate;
        odcModelState.LoadingRate2 = item.LoadingPremi;
        odcModelState.BasicPremium2 = item.BasicPremi;
        odcModelState.BundlingPremium2 = item.BasiCoverage;
        odcModelState.TSPremium2 = item.TSPremi;
        odcModelState.TSCoverage2 = item.TSCoverage;
        odcModelState.TPLPremium2 = item.TPLCoverage;
        odcModelState.PADRVRPremium2 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium2 = item.PAPASSCoverage;
        odcModelState.ExtendPremium2 = item.PremiPerluasan;
        odcModelState.BExtendPremium2 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium2 = item.TotalPremi;
        odcModelState.PADRVRRate2 = item.PADRVRRate;
        odcModelState.PAPASSRate2 = item.PAPASSRate;
        odcModelState.TPLRate2 = item.TPLRate;
        odcModelState.TSRate2 = item.TSRate;
        odcModelState.IsBundling2 = item.IsBundling;
        odcModelState.IsBundlingTRS2 = item.IsBundlingTRS;
        odcModelState.SFEPremi2 = item.SFEPremi;
        odcModelState.SFERate2 = item.SFERate;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: false,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "2";
          }
        );
      } else if (item.Year == 3) {
        odcModelState.CascoSI3 = item.VehiclePremi;
        odcModelState.AccessSI3 = item.AccessPremi;
        odcModelState.CascoAccess3 = item.VehicleAccessPremi;
        odcModelState.CascoRate3 = item.Rate;
        odcModelState.LoadingRate3 = item.LoadingPremi;
        odcModelState.BasicPremium3 = item.BasicPremi;
        odcModelState.BundlingPremium3 = item.BasiCoverage;
        odcModelState.TSPremium3 = item.TSPremi;
        odcModelState.TSCoverage3 = item.TSCoverage;
        odcModelState.TPLPremium3 = item.TPLCoverage;
        odcModelState.PADRVRPremium3 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium3 = item.PAPASSCoverage;
        odcModelState.ExtendPremium3 = item.PremiPerluasan;
        odcModelState.BExtendPremium3 = item.PremiDasarPerluasan;
        odcModelState.PADRVRRate3 = item.PADRVRRate;
        odcModelState.PAPASSRate3 = item.PAPASSRate;
        odcModelState.TPLRate3 = item.TPLRate;
        odcModelState.TSRate3 = item.TSRate;
        odcModelState.TotalPremium3 = item.TotalPremi;
        odcModelState.IsBundling3 = item.IsBundling;
        odcModelState.IsBundlingTRS3 = item.IsBundlingTRS;
        odcModelState.SFEPremi3 = item.SFEPremi;
        odcModelState.SFERate3 = item.SFERate;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "3";
          }
        );
      } else if (item.Year == 4) {
        odcModelState.CascoSI4 = item.VehiclePremi;
        odcModelState.AccessSI4 = item.AccessPremi;
        odcModelState.CascoAccess4 = item.VehicleAccessPremi;
        odcModelState.CascoRate4 = item.Rate;
        odcModelState.LoadingRate4 = item.LoadingPremi;
        odcModelState.BasicPremium4 = item.BasicPremi;
        odcModelState.BundlingPremium4 = item.BasiCoverage;
        odcModelState.TSPremium4 = item.TSPremi;
        odcModelState.TSCoverage4 = item.TSCoverage;
        odcModelState.TPLPremium4 = item.TPLCoverage;
        odcModelState.PADRVRPremium4 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium4 = item.PAPASSCoverage;
        odcModelState.ExtendPremium4 = item.PremiPerluasan;
        odcModelState.BExtendPremium4 = item.PremiDasarPerluasan;
        odcModelState.PADRVRRate4 = item.PADRVRRate;
        odcModelState.PAPASSRate4 = item.PAPASSRate;
        odcModelState.TPLRate4 = item.TPLRate;
        odcModelState.TSRate4 = item.TSRate;
        odcModelState.TotalPremium4 = item.TotalPremi;
        odcModelState.IsBundling4 = item.IsBundling;
        odcModelState.IsBundlingTRS4 = item.IsBundlingTRS;
        odcModelState.SFEPremi4 = item.SFEPremi;
        odcModelState.SFERate4 = item.SFERate;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: true,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "4";
          }
        );
      } else if (item.Year == 5) {
        odcModelState.CascoSI5 = item.VehiclePremi;
        odcModelState.AccessSI5 = item.AccessPremi;
        odcModelState.CascoAccess5 = item.VehicleAccessPremi;
        odcModelState.CascoRate5 = item.Rate;
        odcModelState.LoadingRate5 = item.LoadingPremi;
        odcModelState.BasicPremium5 = item.BasicPremi;
        odcModelState.BundlingPremium5 = item.BasiCoverage;
        odcModelState.TSPremium5 = item.TSPremi;
        odcModelState.TSCoverage5 = item.TSCoverage;
        odcModelState.TPLPremium5 = item.TPLCoverage;
        odcModelState.PADRVRPremium5 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium5 = item.PAPASSCoverage;
        odcModelState.ExtendPremium5 = item.PremiPerluasan;
        odcModelState.BExtendPremium5 = item.PremiDasarPerluasan;
        odcModelState.PADRVRRate5 = item.PADRVRRate;
        odcModelState.PAPASSRate5 = item.PAPASSRate;
        odcModelState.TPLRate5 = item.TPLRate;
        odcModelState.TSRate5 = item.TSRate;
        odcModelState.TotalPremium5 = item.TotalPremi;
        odcModelState.IsBundling5 = item.IsBundling;
        odcModelState.IsBundlingTRS5 = item.IsBundlingTRS;
        odcModelState.SFEPremi5 = item.SFEPremi;
        odcModelState.SFERate5 = item.SFERate;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: true,
            isYearFiveExist: true
          },
          () => {
            this.state.TotalYear = "5";
          }
        );
      }
    }

    this.setState({ odcModel: odcModelState });

    // console.log(odcModelState);
  };

  loaddatacoverage = orderno => {
    this.setState({ isLoading: true });
    DataSource.getOrderDetailCoverage(orderno)
      .then(res => res.json())
      .then(datamapping => {
        this.setState(
          {
            // datas: datamapping.data[0],
            datacoverage: datamapping.data,
            isLoading: false
          },
          () => {
            // this.setOrderDetailCoverage(datamapping.data);
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
          // this.loaddatacoverage(orderno);
        }
      });
  };

  onSubmitVehicle = e => {
    e.preventDefault();
    this.setState({
      // isErrVehDetails: false,
      // isErrVehCode: false,
      // isErrVehBrand: false,
      // isErrVehYear: false,
      isErrVehType: false,
      // isErrVehSeries: false,
      // isErrVehUsage: false,
      // isErrVehRegion: false,
      // isErrVehDetails: false,
      loadingvehicletocover: false,
      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrCovSumInsured: false,
      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrVehDetails: false
    });

    const state = this.state;
    let errorField = "";

    // if(state.VehicleUsedCar.checked && state.VehicleDetailsTemp === ""){
    //   this.setState({
    //     isErrVehDetails: true
    //   });
    //   errorField += "{Vehicle Details} ";
    // }

    // if(state.VehicleDetailsTemp === ""){
    //   this.setState({
    //     isErrVehDetails: true
    //   });
    //   errorField += "{Vehicle Details} ";
    // }

    // if (state.VehicleDetailsTemp === "") {
    //   this.setState({
    //     isErrVehDetails: true
    //   });
    //   errorField += "{Vehicle Details} ";
    // }

    // if (state.VehicleCode === "") {
    //   this.setState({
    //     isErrVehCode: true
    //   });
    //   errorField += "{Vehicle Code} ";
    // }

    // if (state.VehicleBrand === "") {
    //   this.setState({
    //     isErrVehBrand: true
    //   });
    //   errorField += "{Vehicle Brand} ";
    // }

    // if (state.VehicleYear === "") {
    //   this.setState({
    //     isErrVehYear: true
    //   });
    //   errorField += "{Vehicle Year} ";
    // }

    if (state.VehicleType === "") {
      this.setState({
        isErrVehType: true
      });
      errorField += "{Vehicle Type} ";
    }

    // if (state.VehicleSeries === "") {
    //   this.setState({
    //     isErrVehSeries: true
    //   });
    //   errorField += "{Vehicle Series} ";
    // }

    if (state.IsTPLChecked && state.TPLCoverageId == null) {
      errorField += "{TPL} ";
    }

    if (state.IsPADRVRChecked && state.PADRVCOVER == null) {
      errorField += "{PA DRIVER} ";
    }

    if (state.CoverProductType === "" || state.CoverProductType == null) {
      this.setState({
        isErrCovProductType: true
      });
      errorField += "{Product Type} ";
    }

    if (state.CoverProductCode === "" || state.CoverProductCode == null) {
      this.setState({
        isErrCovProductCode: true
      });
      errorField += "{Product Code} ";
    }

    if (state.CoverBasicCover === "") {
      this.setState({
        isErrCovBasicCover: true
      });
      errorField += "{Basic Cover} ";
    }

    if (state.VehicleUsage === "") {
      this.setState({
        isErrVehUsage: true
      });
      errorField += "{Vehicle Usage} ";
    }

    if (state.CovSumInsured === "" || state.tempCovSumInsured == null) {
      this.setState({
        isErrCovSumInsured: true
      });
      errorField += "{Sum Insured} ";
    }

    if (state.VehicleRegion === "") {
      this.setState({
        isErrVehRegion: true
      });
      errorField += "{Vehicle Region} ";
    }

    if (
      state.VehicleUsedCar &&
      (state.VehicleDetailsTemp === "" || state.VehicleDetailsTemp == null)
    ) {
      this.setState({
        isErrVehDetails: true
      });
      errorField += "{Vehicle Description}";
    }

    if (errorField != "") {
      this.setState({
        isErrorVehicle: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan! ", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (Util.isNullOrEmpty(this.state.CalculatedPremiItems)) {
      Util.showToast("Premi belum terhitung", "WARNING");
      this.trigerRateCalculation();
      return;
    }

    if (this.state.CalculatedPremiItems.length == 0) {
      Util.showToast("Premi belum terhitung", "WARNING");
      return;
    }

    var tabsDisabledState = [...this.state.tabsDisabledState];
    tabsDisabledState[1] = false;
    this.setState(
      {
        tabsDisabledState: tabsDisabledState,
        isErrorVehicle: false
      },
      () => {
        this.getOrderDetailCoverage();
      }
    );

    if (state.isErrCovSumInsured) {
      this.onShowAlertModalInfo("Sum Insured melebihi +/-3% dari pricelist");
      this.setState({
        isErrCovSumInsured: true
        // CovSumInsured: 0
      });
      this.onClickTabs(tabstitle[0]);
    } else this.onClickTabs(tabstitle[1]);
  };

  onSubmitProspect = event => {
    event.preventDefault();
    this.setState({
      isErrProsName: false,
      isErrPhone1: false,
      isErrEmail: false,
      isErrVehDetails: false
    });
    const state = this.state;
    let errorField = "";
    if (state.ProspectName === "") {
      this.setState({
        isErrProsName: true
      });
      errorField += "{Prospect Name} ";
    }

    if (state.ProspectEmail === "") {
      this.setState({
        isErrEmail: true
      });
      errorField += "{Prospect Email} ";
    }

    if (state.VehicleDetailsTemp === "" || state.VehicleDetailsTemp == null) {
      this.setState({
        isErrVehDetails: true
      });
      errorField += "{Vehicle Details} ";
    }

    if (state.ProspectPhone1 == null) {
      this.setState({
        isErrPhone1: true
      });
      errorField += "{Prospect Phone Number 1 must cannot empty} ";
      toast.dismiss();
      toast.warning("❗ Prospect Phone Number tidak boleh kosong!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    } else {
      if (state.ProspectPhone1.length < 9 || state.ProspectPhone1.length > 13) {
        this.setState({
          isErrPhone1: true
        });
        errorField +=
          "{Prospect Phone Number 1 must be filled with 9 - 13 characters} ";
        toast.dismiss();
        toast.warning("❗ Prospect Phone Number 1 harus 9 - 13 karakter", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }
    }

    if (
      state.ProspectEmail != "" &&
      /^[\w\.-]+@([\w\-]+\.)+[A-Z]{2,4}$/.test(state.ProspectEmail)
    ) {
      this.setState({
        isErrEmail: true
      });
      errorField += "{Email tidak valid} ";
      toast.dismiss();
      toast.warning("❗ Email tidak valid", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (errorField != "") {
      this.setState({
        isErrorProspect: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (!this.handlePassSubmitByCondition()) {
      return;
    }

    if (this.state.ProspectName.length > 100) {
      toast.warning("❗ Maximum length of Prospect Name is 100 characters", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }

    this.setState({
      isErrorProspect: false
    });
  };

  onSubmitCover = e => {
    e.preventDefault();

    this.setState({
      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrCovSumInsured: false,
      isErrPeriodFrom: false,
      isErrPeriodTo: false
    });

    const state = this.state;
    let errorField = "";

    if (state.CoverProductType === "") {
      this.setState({
        isErrCovProductType: true
      });
      errorField += "{Product Type} ";
    }

    if (state.CoverProductCode === "" || state.CoverProductCode == null) {
      this.setState({
        isErrCovProductCode: true
      });
      errorField += "{Product Code} ";
    }

    if (state.CoverBasicCover === "") {
      this.setState({
        isErrCovBasicCover: true
      });
      errorField += "{Basic Cover} ";
    }

    if (state.CovSumInsured === "") {
      this.setState({
        isErrCovSumInsured: true
      });
      errorField += "{Sum Insured} ";
    }

    if (state.vehicleperiodfrom === null) {
      this.setState({
        isErrPeriodFrom: true
      });
      errorField += "{Period From} ";
    }

    if (state.vehicleperiodto === null) {
      this.setState({
        isErrPeriodTo: true
      });
      errorField += "{Period To} ";
    }

    if (errorField != "") {
      this.setState({
        isErrorCover: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (this.state.CalculatedPremiItems.length == 0) {
      toast.dismiss();
      toast.warning("❗ Please make your coverage first!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (this.state.CovSumInsured.length == 0) {
      this.onShowAlertModalInfo("Please insert vehicle Sum Insured");
      return;
    }

    if (this.state.IsPADRVRChecked == 1 && this.state.PADRVCOVER == 0) {
      this.onShowAlertModalInfo("Please insert PA Driver Sum Insured");
      return;
    }

    if (this.state.IsPASSEnabled == 1 && this.state.PAPASSICOVER == 0) {
      this.onShowAlertModalInfo("Please insert PA Passanger Sum Insured");
      return;
    }

    if (this.state.IsPAPASSSIEnabled == 1 && this.state.PASSCOVER == 0) {
      this.onShowAlertModalInfo("Please insert number of Passanger");
      return;
    }

    if (
      parseInt(this.state.PADRVCOVER + "") >
      parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "PA Driver Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    if (
      parseInt(this.state.PAPASSICOVER + "") >
      parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "PA Passanger Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    if (this.state.IsACCESSChecked == 1 && this.state.ACCESSCOVER == 0) {
      this.onShowAlertModalInfo("Please insert Accessory Sum Insured");
      return;
    }

    if (
      this.state.IsACCESSChecked == 1 &&
      parseInt(this.state.ACCESSCOVER + "") >
        parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "Accessory Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    var tabsDisabledState = [...this.state.tabsDisabledState];
    tabsDisabledState[2] = false;
    this.setState({
      tabsDisabledState: tabsDisabledState,
      isErrorCover: false
    });
    this.onClickTabs(tabstitle[2]);
    // console.log("masuk submit cover");
  };

  sendQuotationEmail = (orderNo, ProspectEmail) => {
    this.onSavePremiumSimulation(() => {
      toast.dismiss();
      toast.info("Send email to " + ProspectEmail, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      this.setState({ showModal: false });
      DataSource.SendQuotationEmail(ProspectEmail, this.state.OrderNo)
        .then(res => res.json())
        .then(json => {
          if (json.status) {
            this.setState({ showModal: false });
            toast.info("✅ Email sudah berhasil dikirimkan! ", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.dismiss();
            toast.warning("❗ Gagal Send Email - " + json.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Gagal Send Email - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          // this.sendQuotationEmail(orderNo, Email);
        });
    });
    // this.setState({ isLoading: true });
  };

  onCloseModalSendEmail = () => {
    this.setState({ showModalSendEmail: false });
  };

  onCloseModalSendSMS = () => {
    this.setState({ showModalSendSMS: false });
  };

  sendQuotationSMS = (orderNo, phone) => {
    this.setState({ isLoading: true });
    this.onSavePremiumSimulation(() => {
      DataSource.SendQuotationSMS(phone, this.state.OrderNo)
        .then(res => res.json())
        .then(json => {
          if (json.status) {
            this.setState({ showModal: false });
            toast.info("✅ SMS berhasil dikirimkan ", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.dismiss();
            toast.warning("❗ Gagal Send SMS - " + json.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false,
            showModal: false
          });
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Gagal Send SMS - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          // this.sendQuotationEmail(orderNo, phone);
        });
    });
  };

  componentWillUnmount() {
    this.abortController.abort();
    this.abortControllerBasicPremium.abort();
    clearInterval(this.accesoriesInterval);
  }

  componentDidMount() {
    // this.getVehicle();
    this.abortController = new AbortController();
    this.abortControllerBasicPremium = new AbortController();

    this.accesoriesInterval = setInterval(() => {
      if (
        this.props.history.action == "POP" &&
        this.state.showModalAccessories
      ) {
        this.setState({ showModalAccessories: false });
      }
    }, 100);

    var BranchCode = JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode;

    this.setState({ BranchCode });

    if (
      secureStorage.getItem("flagquotationdetailspremiumsimulation") != null
    ) {
      var premiumsimulation = JSON.parse(
        secureStorage.getItem("premiumsimulation")
      );

      this.setModelPremiumSimulation(premiumsimulation);
    }

    // Handle default checklist isMVGodig
    this.defaultChecklistMVGodig(() => {
      this.onClickTabs(this.state.activetab);
    });
  }

  // Handle default checklist isMVGodig
  defaultChecklistMVGodig = (callback = () => {}) => {
    let { isMvGodig, isNonMvGodig } = { ...this.state };
    if (this.isRoleTelesales()) {
      isMvGodig = true;
      isNonMvGodig = false;
    } else {
      isMvGodig = false;
      isNonMvGodig = true;
    }

    this.setState(
      {
        isMvGodig,
        isNonMvGodig
      },
      () => {
        callback();
      }
    );
  };

  isRoleTelesales = () => {
    let res = false;

    let account = JSON.parse(ACCOUNTDATA);

    if (Util.stringEquals(account.UserInfo.User.Role, "TELESALES")) {
      res = true;
    }

    return res;
  };

  setModelPremiumSimulation = PremiumSimulation => {
    this.setState(
      {
        ACCESSPremi: PremiumSimulation.ACCESSPremi,
        AccessSI: PremiumSimulation.AccessSI,
        AdminFee: PremiumSimulation.AdminFee,
        Alert: PremiumSimulation.Alert,
        BranchCode: PremiumSimulation.BranchCode,
        CalculatedPremiItems: PremiumSimulation.CalculatedPremiItems,
        CovSumInsured: PremiumSimulation.CovSumInsured,
        CoverBasicCover: PremiumSimulation.CoverBasicCover,
        CoverComprePeriod: PremiumSimulation.CoverComprePeriod,
        CoverLastCoverageNo: PremiumSimulation.CoverLastCoverageNo,
        CoverLastInterestNo: PremiumSimulation.LastInterestNo,
        CoverProductCode: PremiumSimulation.CoverProductCode,
        CoverProductType: PremiumSimulation.CoverProductType,
        CoverTLOPeriod: PremiumSimulation.CoverTLOPeriod,
        ETVPremi: PremiumSimulation.ETVPremi,
        FLDPremi: PremiumSimulation.FLDPremi,
        SalesOfficerID: PremiumSimulation.SalesOfficerID,
        TotalPremi: PremiumSimulation.TotalPremi,
        VehicleBrand: PremiumSimulation.VehicleBrand,
        VehicleChasisNumber: PremiumSimulation.VehicleChasisNumber,
        VehicleCode: PremiumSimulation.VehicleCode,
        VehicleEngineNumber: PremiumSimulation.VehicleEngineNumber,
        VehicleProductTypeCode: PremiumSimulation.VehicleProductTypeCode,
        VehicleRegion: PremiumSimulation.VehicleRegion,
        VehicleRegistrationNumber: PremiumSimulation.VehicleRegistrationNumber,
        VehicleSeries: PremiumSimulation.VehicleSeries,
        VehicleSitting: PremiumSimulation.VehicleSitting,
        VehicleSumInsured: PremiumSimulation.VehicleSumInsured,
        VehicleType: PremiumSimulation.VehicleType,
        VehicleTypeType: PremiumSimulation.VehicleTypeType,
        VehicleUsage: PremiumSimulation.VehicleUsage,
        VehicleUsedCar: PremiumSimulation.VehicleUsedCar,
        VehicleYear: PremiumSimulation.VehicleYear,
        activetab: PremiumSimulation.activetab,
        VehicleDetails: PremiumSimulation.VehicleDetailsTemp,
        vehicleModelCode: PremiumSimulation.vehicleModelCode,

        coveragePeriodItems: PremiumSimulation.coveragePeriodItems,
        coveragePeriodNonBasicItems:
          PremiumSimulation.coveragePeriodNonBasicItems,

        dataTPLSI: PremiumSimulation.dataTPLSI,
        datacoverbasiccover: PremiumSimulation.datacoverbasiccover,
        datacoverproductcode: PremiumSimulation.datacoverproductcode,
        datacoverproducttype: PremiumSimulation.datacoverproducttype,
        datavehiclebrand: PremiumSimulation.datavehiclebrand,
        datavehicleregion: PremiumSimulation.datavehicleregion,
        datavehicleseries: PremiumSimulation.datavehicleseries,
        datavehicletype: PremiumSimulation.datavehicletype,
        datavehicleusage: PremiumSimulation.datavehicleusage,
        datavehicleyear: PremiumSimulation.datavehicleyear,

        loadingvehicletocover: true,
        tabsDisabledState: PremiumSimulation.tabsDisabledState
      },
      () => {
        this.onClickTabs(this.state.activetab);
        secureStorage.removeItem("flagquotationdetailspremiumsimulation");
        secureStorage.removeItem("premiumsimulation");
      }
    );
  };

  getListDealer = () => {
    this.setState({ isLoading: true });
    DataSource.getDealerName()
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.DealerCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datadealer: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListDealer();
        }
      });
  };

  getListBrand = () => {
    this.setState({ isLoading: true });
    DataSource.getVehicleBrand()
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.BrandCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehiclebrand: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListBrand();
        }
      });
  };

  getListVehType = () => {
    this.setState({ isLoading: true });
    DataSource.getVehicleTypeall()
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.VehicleTypeCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicletype: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListVehType();
        }
      });
  };

  getListType = (VehicleBrand, VehicleYear, callback = () => {}) => {
    this.setState({ isLoading: true });
    DataSource.getVehicleType(VehicleBrand, VehicleYear)
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.Type}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            datavehicletype: datamapping,
            isLoading: false
          },
          () => {
            Log.debugGroup(
              "VEHICLE TYPE LIST FROM API: ",
              this.state.datavehicletype
            );
            callback();
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListType(VehicleBrand, VehicleYear);
        }
      });
  };

  getListUsage = () => {
    this.setState({ isLoading: true });
    DataSource.getVehicleUsage(this.state.isMvGodig)
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.insurance_code}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            datavehicleusage: datamapping,
            isLoading: false,
            VehicleUsage:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.VehicleUsage,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.VehicleUsage
                : ""
          },
          () => {
            if (!Util.isNullOrEmpty(this.state.VehicleUsage)) {
              this.onOptionVehUsageChange(this.state.VehicleUsage);
            }
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListBrand();
        }
      });
  };

  getListRegion = () => {
    this.setState({ isLoading: true });
    DataSource.getVehicleRegion()
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.RegionCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicleregion: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListBrand();
        }
      });
  };

  getVehiclePrice = (vehiclecode, year, citycode) => {
    this.setState({ isLoading: true });
    DataSource.getVehiclePrice(vehiclecode, year, citycode)
      .then(res => res.json())
      .then(jsn => {
        this.setState(
          {
            CovSumInsured: jsn.data,
            tempCovSumInsured: jsn.data,
            VehiclePrice: jsn.data,
            isLoading: false
          },
          () => {
            this.checkRateCalculatePremi();
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getVehiclePrice(vehiclecode, year, citycode);
        }
      });
  };

  getListProductType = () => {
    this.setState({ isLoading: true });
    DataSource.getProductType(
      JSON.parse(ACCOUNTDATA).UserInfo.User.Channel,
      JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      this.state.isMvGodig
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.InsuranceType}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            datacoverproducttype: datamapping,
            isLoading: false,
            CoverProductType:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.CoverProductType,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.CoverProductType
                : ""
          },
          () => {
            if (!Util.isNullOrEmpty(this.state.CoverProductType)) {
              this.onOptionCovProductTypeChange(this.state.CoverProductType);
            }
          }
        );
        // if(JSON.parse(ACCOUNTDATA).UserInfo.User.Channel == "EXT"){
        //   this.setState({
        //     CoverProductType: datamapping[0].value
        //   });
        // }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListProductType();
        }
      });
  };

  getListBasicCover = () => {
    this.setState({ isLoading: true });
    DataSource.getBasicCover(
      JSON.parse(ACCOUNTDATA).UserInfo.User.Channel, 
      JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      true,
      "",
      this.state.isMvGodig
      )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ databasiccoverall: jsn.BasicCover });
        return jsn.BasicCover.map(data => ({
          value: `${data.Id}`,
          label: `${data.Description}`,
          ComprePeriod: `${data.ComprePeriod}`,
          TLOPeriod: `${data.TLOPeriod}`,
          CoverageId: `${data.CoverageId}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datacoverbasiccover: datamapping,
            isLoading: false,
            CoverBasicCover:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.CoverBasicCover,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.CoverBasicCover
                : ""
          },
          () => {
            let CoverBasicCover = this.state.datacoverbasiccover.filter(
              data =>
                data.ComprePeriod == this.state.CoverComprePeriod &&
                data.TLOPeriod == this.state.CoverTLOPeriod
            );
            // if (CoverBasicCover.length > 0) {
            //   this.setState(
            //     { CoverBasicCover: CoverBasicCover[0].value },
            //     () => {
            //       this.checkRateCalculatePremi();
            //     }
            //   );
            // }
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListProductType();
        }
      });
  };

  trigerRateCalculation = () => {
    Log.debugGroup("trigerRateCalculation");

    if (Util.isNullOrEmpty(this.state.CoverProductCode)) {
      Log.debugGroup("error vehicleproductcode");
      // Log.debugGroup("error vehicleproductcode");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Product code is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.CoverBasicCover)) {
      Log.debugGroup("error vehiclebasiccoverage");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Basic coverage is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.CovSumInsured)) {
      Log.debugGroup("error vehicletotalsuminsured");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Sum Insured is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.CoverProductType)) {
      Log.debugGroup("error vehicleProductTypeCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleBrand)) {
      Log.debugGroup("error vehicleBrandCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleCodetemp)) {
      Log.debugGroup("error vehiclevehiclecode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleModelCode)) {
      Log.debugGroup("error vehicleModelCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleType)) {
      Log.debugGroup("error vehicleType");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleSeries)) {
      Log.debugGroup("error vehicleSeries");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleYear)) {
      Log.debugGroup("error vehicleYear");
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleUsage)) {
      Log.debugGroup("error vehicleusage");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Vehicle usage is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.VehicleRegion)) {
      Log.debugGroup("error vehicleregion");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Vehicle region is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }

    Log.debugGroup("Hit ratecalculation from triggerratecalculation");
    // this.rateCalculate(false);
    this.basicPremiCalculation();
  };

  onOptionSelect2Change = (value, name) => {
    this.setState({ [name]: value }, () => {
      if (name == "TPLCoverageId") {
        // this.rateCalculateTPLPAPASSPADRV("TPL");
        let TPLSICOVER = [...this.state.dataTPLSIall].filter(
          data => data.CoverageId == value
        )[0].TSI;
        this.setState({
          TPLSICOVER,
          TPLCoverageId: value
        });
      }

      if (
        Util.stringArrayContains(name, [
          "vehiclevehiclecode",
          "vehiclevehiclecodeyear",
          "vehiclebasiccoverage",
          "vehicleproductcode",
          "vehicleregion",
          "vehicletotalsuminsured",
          "vehicleusage",
          "vehicleusedcar"
        ])
      ) {
        this.trigerRateCalculation();
      }

      if (name == "policysentto") {
        this.setState(
          {
            policyname: "",
            policyaddress: "",
            policykodepos: ""
          },
          () => {
            if (value == "CS") {
              this.getNameOnPolicyDelivery(this.state.CustID);
            } else if (value == "BR") {
              if (
                !Util.isNullOrEmpty(
                  JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
                )
              ) {
                this.getNameOnPolicyDelivery(
                  JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
                );
              }
            }
          }
        );
      }
    });

    if (name == "vehicledealer") {
      // this.getSalesmanDealer(value);
      this.getDealerInformation(value);
    }
    if (name == "vehiclevehiclecodeyear") {
      let data = [...this.state.datavehicleall].filter(
        data => data.VehicleCode + "-" + data.Year == value
      );

      // this.getListProductCode(
      //   data[0].ProductTypeCode,
      //   data[0].InsuranceType,
      //   this.state.vehicleusedcar ? 1 : 0
      // );

      console.log(data);
      this.setState(
        {
          vehicletotalsuminsured: data[0].Price,
          vehicletotalsuminsuredtemp: data[0].Price,
          vehicleBrandCode: data[0].BrandCode,
          // vehicleInsuranceType: data[0].InsuranceType,
          vehicleModelCode: data[0].ModelCode,
          vehicleProductTypeCode: data[0].ProductTypeCode,
          vehicleSeries: data[0].Series,
          vehicleSitting: Util.isNullOrEmpty(data[0].Sitting)
            ? 0
            : data[0].Sitting,
          vehicleType: data[0].Type,
          vehicleYear: data[0].Year,
          vehiclevehiclecode: data[0].VehicleCode
        },
        () => this.trigerRateCalculation()
      );

      if (data[0].Year != Util.convertDate().getFullYear()) {
        this.setState({
          vehicleusedcar: true
        });
      }
    }

    if (name == "surveylokasi") {
      this.setState({
        surveykodepos: "",
        surveywaktu: null,
        surveytanggal: null
      });
    }

    if (name == "vehicleproductcode") {
      this.setNdays(value, this.state.datavehicleproductcodeall);
      this.getSegmentCode(value);
      this.getEnableDisableSalesmanDealer(value);
    }

    if (name == "surveykota") {
      this.getSurveyLocation(value);
      this.setState({
        surveylokasi: "",
        surveyalamat: "",
        surveykodepos: "",
        surveytanggal: null,
        surveywaktu: null
      });
    }

    if (name == "surveykodepos") {
      if (!Util.isNullOrEmpty(this.state.surveytanggal)) {
        this.getSurveyScheduleTime(this.state.surveytanggal, value);
      }
    }

    if (name == "vehicleInsuranceType") {
      this.getListProductCode(
        this.state.ProductTypeCode,
        value,
        this.state.vehicleusedcar ? 1 : 0
      );
    }

    if (name == "policyname") {
      let data = [...this.state.datapolicynameall].filter(
        data => data.Name == value
      )[0];
      this.setState({
        policyname: data.Name,
        policyaddress: data.Address,
        policykodepos: data.PostalCode
      });
    }

    if (name == "vehiclebasiccoverage") {
      let tempData = [...this.state.databasiccoverall].filter(
        data => data.Id == value
      );
      if (tempData.length > 0) {
        let year = tempData[0].ComprePeriod + tempData[0].TLOPeriod;
        this.setState(
          {
            vehicleperiodfrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
              ? Util.convertDate()
              : this.state.vehicleperiodfrom
          },
          () => {
            let vehicleperiodto = Util.convertDate(
              Util.formatDate(
                this.state.vehicleperiodfrom,
                "yyyy-mm-dd hh:MM:ss"
              )
            );
            vehicleperiodto.setFullYear(vehicleperiodto.getFullYear() + year);
            if(Util.stringEquals(Util.formatDate(this.state.vehicleperiodfrom, "mm-dd"), "02-29") && (vehicleperiodto.getFullYear() % 4 != 0)){ // handle 29 february kabisat
              vehicleperiodto.setDate(vehicleperiodto.getDate() - 1);
            }
            console.log(vehicleperiodto);
            console.log(this.state.vehicleperiodfrom);
            this.setState({ vehicleperiodto });
          }
        );
      }
    }
  };

  onClickTabs = activetab => {
    if (!navigator.onLine) {
      Util.showToast("Check your connection!", "ERROR");
      return;
    }

    this.setState({
      isErrIsMvGodig: false
    });

    if (activetab != tabstitle[0]) {
      if (!this.isValidChecklistMvGodig()) {
        Util.showToast(
          "Please tick MV Go Digital or MV Non Go Digital!",
          "WARNING"
        );
        return;
      }
    }

    console.log(activetab);
    this.setState({ activetab });

    if (activetab == tabstitle[0]) {
      this.getDataFormPremiCalculation();
    }

    if (activetab == tabstitle[1]) {
      this.setState({
        isDisableIsMvGodig: true
      });
      this.getOrderDetailCoverage();
      if (
        this.state.VehicleDetailsTemp === "" ||
        this.state.VehicleDetailsTemp == null
      ) {
        this.setState(
          {
            datavehiclebrand: [
              { value: "", label: "" },
              { value: "None", label: "None" }
            ],
            // datavehicleall: [{value: "", label: ""}, {value: "None", label: "None"}],
            // datavehicletype: [{value:"", label:""}, {value: "None", label: "None"}],
            // vehicleModelCode: "None",
            VehicleBrand: "None",
            // VehicleType: "None",
            VehicleSeries: "None"
          },
          () => {
            var followupno = secureStorage.getItem(
              "followupnoquotationdetails"
            );
            var branchcode = JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode;
            var channelsource =
              JSON.parse(ACCOUNTDATA).UserInfo.User.ChannelSource || "";
            this.loaddataheader(followupno, branchcode, channelsource);
            var orderno = secureStorage.getItem("ordernoquotationdetails");

            var datas = {
              AOPhone: JSON.parse(ACCOUNTDATA).UserInfo.User.Phone1,
              AccessSI: this.state.ACCESSCOVER,
              AccountName: "PT. Asuransi Astra Buana",
              AccountNo: "0260048007",
              AccountOfficerName: "BIMA SATRIA",
              AdminFee: this.state.AdminFee,
              TotalYear: this.state.TotalYear,
              Bank: "Permata",
              BranchHead: "Eva Duma Natalina",
              City: "",
              ComprePeriod: this.state.CoverComprePeriod,
              Email1: "",
              EntryDate: Util.convertDate(),
              Fax: JSON.parse(ACCOUNTDATA).UserInfo.User.Fax,
              InsuranceType: parseInt(this.state.CoverProductType),
              InsuranceTypeDesc: this.state.datacoverproducttype.filter(
                data => data.value == this.state.CoverProductType
              )[0].label,
              OfficePhone: JSON.parse(ACCOUNTDATA).UserInfo.User.OfficePhone,
              Phone1: "",
              ProspectName: "",
              QuotationNo: "",
              Region: 1,
              RegistrationNumber: this.state.VehicleRegistrationNumber,
              Sitting: this.state.VehicleSitting,
              TLOPeriod: this.state.CoverTLOPeriod,
              TotalPremium: this.state.TotalPremi,
              Usage: this.state.datavehicleusage.filter(
                data => data.value == this.state.VehicleUsage
              )[0].label,
              Vehicle:
                [...this.state.datavehiclebrand].filter(
                  data => data.value == this.state.VehicleBrand
                )[0].label +
                " " +
                this.state.datavehicletype.filter(
                  data => data.value == this.state.VehicleType
                )[0].label +
                " " +
                this.state.VehicleSeries,
              Year: this.state.VehicleYear,
              YearCoverage:
                this.state.CoverComprePeriod + this.state.CoverTLOPeriod
            };

            var jenisperlindungan = null;
            if (datas != null) {
              if (datas.ComprePeriod > 0 && datas.TLOPeriod == 0) {
                jenisperlindungan =
                  "Comprehensive " + datas.ComprePeriod + " Tahun";
              } else if (datas.ComprePeriod == 0 && datas.TLOPeriod > 0) {
                jenisperlindungan = "TLO " + datas.TLOPeriod + " Tahun";
              } else if (datas.ComprePeriod > 0 && datas.TLOPeriod > 0) {
                jenisperlindungan =
                  "Comprehensive " +
                  datas.ComprePeriod +
                  " Tahun\nTLO " +
                  datas.TLOPeriod +
                  " Tahun";
              } else if (datas.ComprePeriod == 0 && datas.TLOPeriod == 0) {
                jenisperlindungan = datas.TotalYear + " Tahun\n";
              }
            }

            this.setState({ jenisperlindungan });
            this.setState(
              {
                datas: datas,
                datacoverage: this.state.CalculatedPremiItems
              },
              () => {
                // this.setOrderDetailCoverage(this.state.datacoverage);
              }
            );
          }
        );
      } else {
        var followupno = secureStorage.getItem("followupnoquotationdetails");
        var branchcode = JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode;
        var channelsource =
          JSON.parse(ACCOUNTDATA).UserInfo.User.ChannelSource || "";
        this.loaddataheader(followupno, branchcode, channelsource);
        var orderno = secureStorage.getItem("ordernoquotationdetails");

        var datas = {
          AOPhone: JSON.parse(ACCOUNTDATA).UserInfo.User.Phone1,
          AccessSI: this.state.ACCESSCOVER,
          AccountName: "PT. Asuransi Astra Buana",
          AccountNo: "0260048007",
          AccountOfficerName: "BIMA SATRIA",
          AdminFee: this.state.AdminFee,
          TotalYear: this.state.TotalYear,
          Bank: "Permata",
          BranchHead: "Eva Duma Natalina",
          City: "",
          ComprePeriod: this.state.CoverComprePeriod,
          Email1: "",
          EntryDate: Util.convertDate(),
          Fax: JSON.parse(ACCOUNTDATA).UserInfo.User.Fax,
          InsuranceType: parseInt(this.state.CoverProductType),
          InsuranceTypeDesc: this.state.datacoverproducttype.filter(
            data => data.value == this.state.CoverProductType
          )[0].label,
          OfficePhone: JSON.parse(ACCOUNTDATA).UserInfo.User.OfficePhone,
          Phone1: "",
          ProspectName: "",
          QuotationNo: "",
          Region: 1,
          RegistrationNumber: this.state.VehicleRegistrationNumber,
          Sitting: this.state.VehicleSitting,
          TLOPeriod: this.state.CoverTLOPeriod,
          TotalPremium: this.state.TotalPremi,
          Usage: this.state.datavehicleusage.filter(
            data => data.value == this.state.VehicleUsage
          )[0].label,
          Vehicle:
            [...this.state.datavehiclebrand].filter(
              data => data.value == this.state.VehicleBrand
            )[0].label +
            " " +
            this.state.datavehicletype.filter(
              data => data.value == this.state.VehicleType
            )[0].label +
            " " +
            this.state.VehicleSeries,
          Year: this.state.VehicleYear,
          YearCoverage: this.state.CoverComprePeriod + this.state.CoverTLOPeriod
        };

        var jenisperlindungan = null;
        if (datas != null) {
          if (datas.ComprePeriod > 0 && datas.TLOPeriod == 0) {
            jenisperlindungan =
              "Comprehensive " + datas.ComprePeriod + " Tahun";
          } else if (datas.ComprePeriod == 0 && datas.TLOPeriod > 0) {
            jenisperlindungan = "TLO " + datas.TLOPeriod + " Tahun";
          } else if (datas.ComprePeriod > 0 && datas.TLOPeriod > 0) {
            jenisperlindungan =
              "Comprehensive " +
              datas.ComprePeriod +
              " Tahun\nTLO " +
              datas.TLOPeriod +
              " Tahun";
          } else if (datas.ComprePeriod == 0 && datas.TLOPeriod == 0) {
            jenisperlindungan = datas.TotalYear + " Tahun\n";
          }
        }

        this.setState({ jenisperlindungan });
        this.setState(
          {
            datas: datas,
            datacoverage: this.state.CalculatedPremiItems
          },
          () => {
            // this.setOrderDetailCoverage(this.state.datacoverage);
          }
        );
      }
    }

    if (activetab == tabstitle[2]) {
      this.hitratecalculationsimulation = false;
      // this.onSaveCover();
    }
  };

  getDataFormPremiCalculation = () => {
    let {
      VehicleBrand,
      VehicleYear,
      VehicleType,
      VehicleSeries,
      CoverProductCode,
      VehicleCode,
      VehicleDetails,
      vehicleModelCode
    } = {
      ...this.state
    };
    // if (Util.isNullOrEmpty(this.state.CoverBasicCover)) {
    this.getListProductType();
    if (!Util.isNullOrEmpty(this.state.CoverProductType)) {
      this.onOptionCovProductTypeChange(this.state.CoverProductType);
    }
    this.getListBasicCover();
    // }
    // if (Util.isNullOrEmpty(this.state.VehicleDetailsTemp)) {
    this.getListVehType();
    // }

    // this.onSaveProspect();
    this.getListBrand();
    this.getListUsage();
    this.getListRegion();
  };

  onOptionVehDetailsChange = VehicleDetailsTemp => {
    this.setState({ VehicleDetailsTemp });
    this.setState({ isLoading: true });

    DataSource.GetVehicle(
      "QUOTATION",
      VehicleDetailsTemp,
      this.state.isMvGodig,
      this.abortController.signal
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.VehicleDescription}`,
          label: `${data.VehicleDescription}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicledetails: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehDetailsChange(VehicleDetailsTemp);
        }
      });
  };

  onOptionVehCodeChange = VehicleCodetemp => {
    this.setState({ VehicleCodetemp });

    this.setState({
      VehicleUsedCar: false,
      VehicleYear: "",
      VehicleType: "",
      VehicleSeries: "",
      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: "",
      VehicleDetails: ""
    });
    this.setState({ isLoading: true });
    DataSource.ResetVehicleCode(VehicleCodetemp)
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.VehicleCode}`,
          label: `${data.VehicleCode}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehiclecode: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehCodeChange(VehicleCodetemp);
        }
      });
  };

  onOptionVehBrandChange = VehicleBrand => {
    this.setState({ VehicleBrand });
    // console.log("Brand : "+VehicleBrand);

    this.setState({
      VehicleUsedCar: false,
      VehicleYear: "",
      VehicleType: "",
      VehicleSeries: "",
      VehicleCode: "",
      VehicleCodetemp: "",
      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: ""
    });

    this.setState({ isLoading: true });
    DataSource.getVehicleYear(VehicleBrand)
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.Year}`,
          label: `${data.Year}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicledetails: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehBrandChange(VehicleBrand);
        }
      });
  };

  onOptionVehYearChange = VehicleYear => {
    this.setState({ VehicleYear });
    // console.log("Year : "+VehicleYear);
    var now = Util.convertDate();
    this.setState({
      VehicleUsedCar: false
    });

    if (now.getFullYear() - parseInt(VehicleYear.trim()) > 1) {
      this.setState({ VehicleUsedCar: true });
    }

    this.setState({
      VehicleType: "",
      VehicleSeries: "",
      VehicleCode: "",
      VehicleCodetemp: "",
      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: ""
    });

    this.setState({ isLoading: true });
    DataSource.getVehicleType(this.state.VehicleBrand, VehicleYear)
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.ModelCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicletype: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehYearChange(VehicleYear);
        }
      });
  };

  onOptionVehTypeChange = VehicleType => {
    this.setState({ VehicleType });
  };

  onOptionVehSeriesChange = VehicleSeries => {
    this.setState({ VehicleSeries });
    // console.log("Series : "+VehicleSeries);

    this.setState({
      VehicleCode: "",
      VehicleCodetemp: "",
      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: ""
    });

    this.setState({ isLoading: true });
    DataSource.getVehicleCode(
      this.state.VehicleBrand,
      this.state.VehicleYear,
      this.state.VehicleType,
      VehicleSeries
    )
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          VehicleCodetemp: datamapping.data[0].VehicleCode,
          VehicleCode: datamapping.data[0].VehicleCode,
          VehicleProductTypeCode: datamapping.data[0].ProductTypeCode,
          VehicleTypeType: datamapping.data[0].Type,
          VehicleSitting: datamapping.data[0].Sitting,
          VehicleSumInsured: datamapping.data[0].Price,
          isLoading: false
        });

        // console.log(this.state.VehicleCode);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        // if(!(error+"").toLowerCase().includes("token")){
        //   this.onOptionVehSeriesChange(VehicleSeries);
        // }
      });
  };

  onOptionVehUsageChange = VehicleUsage => {
    this.setState({ VehicleUsage }, () => () => this.checkRateCalculatePremi());
  };

  onOptionVehRegionChange = VehicleRegion => {
    this.setState({ VehicleRegion }, () => {
      if (
        this.state.VehicleDescription != null ||
        this.state.VehicleDescription != ""
      ) {
        this.getVehiclePrice(
          this.state.VehicleCode,
          this.state.VehicleYear,
          VehicleRegion
        );
      }
    });
  };

  onOptionCovProductTypeChange = CoverProductType => {
    this.setState({ CoverProductType }, () => this.checkRateCalculatePremi());

    // console.log(CoverProductType);

    // this.setState({
    //   CoverProductCode: ""
    // });

    this.setState({ isLoading: true });
    DataSource.getProductCode(
      CoverProductType,
      this.state.SalesOfficerID,
      this.state.isMvGodig,
      !this.state.VehicleUsedCar
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datacoverproductcodeall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.ProductCode}`,
          label: `${data.Description}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datacoverproductcode: datamapping,
            isLoading: false,
            CoverProductCode:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.CoverProductCode,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.CoverProductCode
                : ""
          },
          () => {
            if (!Util.isNullOrEmpty(this.state.CoverProductCode)) {
              this.onOptionCovProductCodeChange(this.state.CoverProductCode);
            } else {
              this.checkRateCalculatePremi();
            }
          }
        );
        // if(JSON.parse(ACCOUNTDATA).UserInfo.User.Channel == "EXT"){
        //   this.setState({
        //     CoverProductCode: datamapping[0].value
        //   }, () => {
        //     this.checkRateCalculatePremi();
        //   });
        // }else{
        // }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionCovProductTypeChange(CoverProductType);
        }
      });
  };

  onOptionCovProductCodeChange = CoverProductCode => {
    this.setState({ CoverProductCode }, () => {
      this.hitratecalculationsimulation = true;
      // this.basicPremiCalculation();
      this.setNullCalculatePremi();
      this.setNdays(
        CoverProductCode,
        this.state.datacoverproductcodeall,
        () => {
          this.checkRateCalculatePremi();
        }
      );
      // this.checkRateCalculatePremi();
    });
    console.log(CoverProductCode);
  };

  setPeriodToPeriodFromTS = (callback = () => {}) => {
    let CalculatedPremiItemsTemp = [
      ...this.state.CalculatedPremiItems
    ].filter(data => data.CoverageID.includes("SRC"));
    let periodto = null;
    let periodfrom = null;

    CalculatedPremiItemsTemp.forEach((data, index) => {
      if (index == 0) {
        periodfrom = Util.convertDate(data.PeriodFrom);
        periodto = Util.convertDate(data.PeriodTo);
      } else {
        if (periodfrom >= Util.convertDate(data.PeriodFrom)) {
          periodfrom = Util.convertDate(data.PeriodFrom);
        }
        if (periodto <= Util.convertDate(data.PeriodTo)) {
          periodto = Util.convertDate(data.PeriodTo);
        }
      }
    });

    this.setState(
      {
        periodfromTS: periodfrom,
        periodtoTS: periodto
      },
      () => {
        callback();
      }
    );
  };

  setPeriodToPeriodFromExtendedCover = (
    TYPE = "",
    CoverageOrInterest = "",
    callback = () => {}
  ) => {
    let CalculatedPremiItemsTemp = [...this.state.CalculatedPremiItems].filter(
      data => {
        if (CoverageOrInterest.toUpperCase() == "COVERAGE") {
          return data.CoverageID.includes(TYPE);
        } else if (CoverageOrInterest.toUpperCase() == "INTEREST") {
          return data.InterestID.includes(TYPE);
        } else {
          return false;
        }
      }
    );

    let basicCoverage = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.CoverBasicCover
    )[0];

    let periodfrom = Util.convertDate(
      Util.formatDate(this.state.vehicleperiodfrom)
    );
    let periodto = Util.convertDate(
      Util.formatDate(this.state.vehicleperiodfrom)
    );
    periodto.setFullYear(
      Util.convertDate(
        Util.formatDate(this.state.vehicleperiodfrom)
      ).getFullYear() +
        (parseInt(basicCoverage.ComprePeriod) +
          parseInt(basicCoverage.TLOPeriod))
    );

    if (TYPE == "TPLPER") {
      periodto = Util.convertDate(
        Util.formatDate(this.state.vehicleperiodfrom)
      ).setFullYear(
        Util.convertDate(
          Util.formatDate(this.state.vehicleperiodfrom)
        ).getFullYear() + basicCoverage.ComprePeriod
      );
    }

    if (basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0) {
      periodto = Util.convertDate(Util.formatDate(this.state.vehicleperiodto));
    }

    CalculatedPremiItemsTemp.forEach((data, index) => {
      if (index == 0) {
        periodfrom = Util.convertDate(data.PeriodFrom);
        periodto = Util.convertDate(data.PeriodTo);
      } else {
        if (periodfrom >= Util.convertDate(data.PeriodFrom)) {
          periodfrom = Util.convertDate(data.PeriodFrom);
        }
        if (periodto <= Util.convertDate(data.PeriodTo)) {
          periodto = Util.convertDate(data.PeriodTo);
        }
      }
    });

    if(Util.stringEquals(Util.formatDate(periodfrom, "mm-dd"), "02-29") && (periodto.getFullYear() % 4 != 0)){ // handle 29 february kabisat 
      periodto.setDate(periodto.getDate() - 1);
    }

    this.setState(
      {
        periodfromTemp: periodfrom,
        periodtoTemp: periodto
      },
      () => {
        callback();
      }
    );
  };

  setNdays = (
    ProductCode = this.state.CoverProductCode,
    datacoverproductcodeall = this.state.datacoverproductcodeall,
    callback = () => {}
  ) => {
    if (datacoverproductcodeall.length > 0) {
      let data = [...datacoverproductcodeall].filter(
        data => data.ProductCode == ProductCode
      );
      data = data[0];
      console.log("setdays", data);
      this.setState(
        {
          Ndays: data.Ndays
        },
        () => {
          callback();
        }
      );
    }
  };

  onOptionCovBasicCoverChange = CoverBasicCover => {
    this.setState({ CoverBasicCover });

    var datatemp = this.state.datacoverbasiccover.filter(
      data => data.value == CoverBasicCover
    );
    Log.debugStr(
      "MASUK DATATEMP COVERBASICOVER: " + this.state.CoverBasicCover
    );
    if (datatemp != null) {
      Log.debugStr("MASUK DATATEMP BASIC COVER");
      let year =
        parseInt(datatemp[0].ComprePeriod) + parseInt(datatemp[0].TLOPeriod);
      Log.debugStr("MASUK DATATEMP BASIC COVER TOTAL YEAR: " + year);
      Log.debugStr(
        "MASUK DATATEMP BASIC COVER DATATEMP COMPRE: " +
          datatemp[0].ComprePeriod
      );
      Log.debugStr(
        "MASUK DATATEMP BASIC COVER DATATEMP TLO: " + datatemp[0].TLOPeriod
      );
      this.setState(
        {
          CoverComprePeriod: parseInt(datatemp[0].ComprePeriod),
          CoverTLOPeriod: parseInt(datatemp[0].TLOPeriod),
          CoverageId: datatemp[0].CoverageId,
          vehicleperiodfrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
            ? Util.convertDate()
            : this.state.vehicleperiodfrom
        },
        () => {
          this.state.vehicleperiodto = Util.convertDate(
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss")
          );

          let vehicleperiodto = Util.convertDate(
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss")
          );
          vehicleperiodto.setFullYear(vehicleperiodto.getFullYear() + year);
          this.setState(
            {
              vehicleperiodto
            },
            () => {
              if (year == 5) {
                this.state.isYearFiveExist = true;
                this.state.isYearFourExist = true;
                this.state.isYearThreeExist = true;
                this.state.isYearTwoExist = true;
              } else if (year == 4) {
                this.state.isYearFiveExist = false;
                this.state.isYearFourExist = true;
                this.state.isYearThreeExist = true;
                this.state.isYearTwoExist = true;
              } else if (year == 3) {
                this.state.isYearFiveExist = false;
                this.state.isYearFourExist = false;
                this.state.isYearThreeExist = true;
                this.state.isYearTwoExist = true;
              } else if (year == 2) {
                this.state.isYearFiveExist = false;
                this.state.isYearFourExist = false;
                this.state.isYearThreeExist = false;
                this.state.isYearTwoExist = true;
              } else if (year == 1) {
                this.state.isYearFiveExist = false;
                this.state.isYearFourExist = false;
                this.state.isYearThreeExist = false;
                this.state.isYearTwoExist = false;
              } else if (year == 0) {
                this.state.isYearFiveExist = false;
                this.state.isYearFourExist = false;
                this.state.isYearThreeExist = false;
                this.state.isYearTwoExist = false;
              }

              this.checkRateCalculatePremi();
            }
          );
        }
      );
    }
    // console.log(datatemp);
  };

  onOptionTPLSIChange = TPLCoverageId => {
    let TPLSICOVER = [...this.state.dataTPLSI].filter(
      data => data.CoverageId == TPLCoverageId
    )[0].TSI;
    this.setState({
      TPLSICOVER
    });
  };

  checkRateCalculatePremi = () => {
    if (
      this.state.CoverBasicCover != null &&
      this.state.CoverBasicCover != "" &&
      this.state.CoverProductCode != null &&
      this.state.CoverProductCode != "" &&
      this.state.CoverProductType != null &&
      this.state.CoverProductType != "" &&
      // this.hitratecalculationsimulation == true &&
      this.state.CovSumInsured != 0
    ) {
      this.setState({
        isErrCovBasicCover: false,
        isErrCovProductCode: false,
        isErrCovProductType: false,
        isErrCovSumInsured: false,
        isErrorCover: false
      });
      this.setState({
        IsACCESSChecked: 0,
        IsACCESSSIEnabled: 0,
        ACCESSCOVER: 0
      });
      console.log("hit rate calculate");
      this.basicPremiCalculation();
    } else {
      this.setNullCalculatePremi();
      console.log("belum hit rate calculate");
    }
  };

  getVehicle = (VehicleDetailsTemp = "") => {
    this.abortController.abort();
    this.abortController = new AbortController();
    this.setState({ isLoading: true });

    DataSource.GetVehicle(
      "QUOTATION", 
      VehicleDetailsTemp, 
      this.state.isMvGodig,
      this.abortController.signal
      )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datavehicledetailsall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.VehicleDescription}`,
          label: `${data.VehicleDescription}`
        }));
      })
      .then(datavehicledetails => {
        this.setState({ datavehicledetails, isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
          // this.searchDataHitApi();
        }
      });
  };

  onChangeFunction = event => {
    var nameEvent = event.target.name;
    let value =
      event.target[
        event.target.type === "checkbox"
          ? "checked"
          : event.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = event.target.name;
    let pattern = event.target.pattern;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        value = value;
      } else {
        return;
      }
    }
    console.log([name]);
    console.log(value);
    this.setState(
      {
        [name]: value
      },
      () => {
        if (name == "chItemSetPeriod") {
          if (value) {
            this.setState({
              IsACCESSSIEnabled: 1
            });
          } else {
            this.setState(
              {
                IsACCESSSIEnabled: 0,
                ACCESSCOVER: 0,
                tempACCESSOVER: 0,
                ACCESSPremi: 0
              },
              () => {
                this.premiumCalculation();
              }
            );
          }
        }
        if (nameEvent == "CovSumInsured") {
          this.checkRateCalculatePremi();
        }
        if (
          nameEvent == "PADRVCOVER" ||
          nameEvent == "PAPASSICOVER" ||
          nameEvent == "ACCESSCOVER"
        ) {
          this.rateCalculateTPLPAPASSPADRV(nameEvent);
        }

        if (nameEvent == "VehicleCodetemp") {
          // console.log("masuk");
          this.onChangeVehicleCode();
        }

        if (nameEvent == "VehicleDetailsTemp") {
          this.onChangeVehicleDetails();
        }

        if (nameEvent == "VehicleUsedCar") {
          this.onChangeVehicleUsedCar();
          this.reloadDataFormIfMVGodig();
        }

        if (name == "chItemSetPeriod") {
          if (value == true) {
            this.setState({
              vehicleperiodfrommodalperiod: this.state.vehicleperiodfrom,
              vehicleperiodtomodalperiod: this.state.vehicleperiodto
            });
          } else {
            this.setState({
              vehicleperiodfrommodalperiod: null,
              vehicleperiodtomodalperiod: null
            });
          }
        }

        if (nameEvent === "isMvGodig" || nameEvent === "isNonMvGodig") {
          this.handleChecklistIsMvGodig(nameEvent, () => {
            this.onClickTabs(this.state.activetab);
          });
        }
      }
    );
  };

  reloadDataFormIfMVGodig = () => {
    if (this.state.isMvGodig) {
      // reload data form if order mv godig
      this.onClickTabs(this.state.activetab);
    }
  };

  handleChecklistIsMvGodig = (nameEvent, callback = () => {}) => {
    let { isNonMvGodig, isMvGodig } = { ...this.state };

    if (nameEvent === "isMvGodig") {
      isNonMvGodig = false;
    }

    if (nameEvent === "isNonMvGodig") {
      isMvGodig = false;
    }

    this.setState(
      {
        isNonMvGodig,
        isMvGodig
      },
      () => {
        callback();
      }
    );
  };

  onChangeVehicleUsedCar = () => {
    console.log("vehicle check masuk");
    let { CoverProductCode, VehicleCodetemp, VehicleDetailsTemp } = {
      ...this.state
    };
  };

  onChangeVehicleDetails0 = (VehicleDetailsTemp, name) => {
    this.setState({ [name]: VehicleDetailsTemp });
    console.log("masuk search");

    let data = [...this.state.datavehicleall].filter(
      data => data.VehicleDescription == VehicleDetailsTemp
    );

    // console.log(data);
    this.setState({
      VehicleSumInsured: data[0].Price,
      VehicleProductTypeCode: data[0].ProductCode,
      VehicleBrand: data[0].BrandCode,
      VehicleCode: data[0].VehicleCode,
      VehicleType: data[0].Type,
      VehicleTypeType: data[0].Type,
      VehicleSeries: data[0].Series,
      VehicleYear: data[0].Year,
      VehicleSitting: data[0].Sitting,
      VehicleUsedCar: data[0].IsNew == 1 ? false : true,
      vehicleModelCode: data[0].ModelCode
    });
  };

  onChangeFunctionChecked = event => {
    let check = event.target.value == 1 ? 0 : 1;
    this.setState({
      [event.target.name]: check
    });
  };

  onCloseModalAccessories = () => {
    this.props.history.goBack();
    this.setState({ showModalAccessories: false });
  };

  ShowAccessoryInfo = () => {
    this.setState({ isLoading: true });
    DataSource.getAccessoriesList()
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          label: `${data.Description}`,
          value: `${data.MaxSI}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            dataAccessories: datamapping,
            isLoading: false,
            showModalAccessories: true
          },
          () => this.props.history.push("/premiumsimulation#")
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.ShowAccessoryInfo();
        }
      });
  };
  onChangeFunction2 = e => {
    // console.log(event.target.type);
    const value =
      e.target[
        e.target.type === "checkbox"
          ? "checked"
          : e.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = e.target.name;

    this.setState(
      {
        [name]: value
      },
      () => {
        if (name == "IsACCESSChecked") {
          if (value) {
            this.setState({
              IsACCESSSIEnabled: 1
            });
          } else {
            this.setState(
              {
                IsACCESSSIEnabled: 0,
                ACCESSCOVER: 0,
                ACCESSPremi: 0
              },
              () => {
                // let CalculatedPremiItems = [...this.state.CalculatedPremiItems];
                // CalculatedPremiItems = CalculatedPremiItems.filter(data => data.InterestID != "ACCESS");

                // // console.log(CalculatedPremiItems);
                // this.setState({CalculatedPremiItems});
                this.rateCalculationNonBasicUnCheckAccesories();
              }
            );
          }
        }
      }
    );
  };

  setNullCalculatePremi = () => {
    this.setState({
      ACCESSPremi: 0,
      AdminFee: 0,
      Alert: 0,
      CalculatedPremiItems: [],
      ETVPremi: 0,
      FLDPremi: 0,
      IsACCESSChecked: 0,
      IsACCESSSIEnabled: 0,
      IsACCESSEnabled: 0,
      IsETVChecked: 0,
      IsETVEnabled: 0,
      IsFLDChecked: 0,
      IsFLDEnabled: 0,
      IsPADRVRChecked: 0,
      IsPADRVREnabled: 0,
      IsPADRVRSIEnabled: 0,
      IsPAPASSChecked: 0,
      IsPAPASSEnabled: 0,
      IsPAPASSSIEnabled: 0,
      IsPASSEnabled: 0,
      IsSRCCChecked: 0,
      IsSRCCEnabled: 0,
      IsTPLChecked: 0,
      IsTPLEnabled: 0,
      IsTPLSIEnabled: 0,
      IsTSChecked: 0,
      IsTSEnabled: 0,
      PADRVRPremi: 0,
      PAPASSPremi: 0,
      SRCCPremi: 0,
      TPLPremi: 0,
      TSPremi: 0,
      TotalPremi: 0,
      coveragePeriodItems: [],
      coveragePeriodNonBasicItems: []
    });
  };

  basicPremiCalculation = () => {
    Log.debugGroup("hit basicPremiCalculation");

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.CoverBasicCover
    )[0];

    this.abortControllerBasicPremium.abort();
    this.abortControllerBasicPremium = new AbortController();

    if (!Util.isNullOrEmpty(BasicCover)) {
      var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;

      fetch(
        `${API_URL + "" + API_VERSION_2}/DataReact/BasicPremiCalculation/`,
        {
          method: "POST",
          signal: this.abortControllerBasicPremium.signal,
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "ComprePeriod=" +
            BasicCover.ComprePeriod +
            "&TLOPeriod=" +
            BasicCover.TLOPeriod +
            "&PeriodFrom=" +
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd") +
            "&PeriodTo=" +
            Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd") +
            "&vtype=" +
            this.state.VehicleType +
            "&vcitycode=" +
            this.state.VehicleRegion +
            "&vusagecode=" +
            this.state.VehicleUsage +
            "&vyear=" +
            this.state.VehicleYear +
            "&vsitting=" +
            this.state.VehicleSitting +
            "&vProductCode=" +
            this.state.CoverProductCode +
            "&vTSInterest=" +
            this.state.CovSumInsured +
            "&vPrimarySI=" +
            this.state.tempCovSumInsured +
            "&vCoverageId=" +
            BasicCover.CoverageId +
            // "&vInterestId=" +
            // "ALLRIK" + /// Pasti ALLRIK ????
            "&pQuotationNo=" +
            "&Ndays=" +
            this.state.Ndays +
            "&vBrand=" +
            this.state.VehicleBrand +
            "&vModel=" +
            this.state.vehicleModelCode +
            "&isNew=" +
            IsNewTemp
        }
      )
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              // if (jsn.Alert != "") {
              //   this.onShowAlertModalInfo(jsn.Alert);
              // }

              // if (isLoading == true) {
              //   let PADRVCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PADRVR"
              //   );
              //   let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PAPASS"
              //   );
              //   let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "ACCESS"
              //   );
              //   let TPLCoverageId = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "TPLPER"
              //   );

              //   if (PADRVCOVER.length > 0) {
              //     this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              //   } else {
              //     this.setState({ PADRVCOVER: 0 });
              //   }

              //   if (PAPASSICOVER.length > 0) {
              //     this.setState({
              //       PAPASSICOVER: PAPASSICOVER[0].SumInsured,
              //       PASSCOVER: PAPASSICOVER[0].CoverageID.trim().replace(
              //         "C",
              //         ""
              //       )
              //     });
              //   } else {
              //     this.setState({
              //       PAPASSICOVER: 0,
              //       PASSCOVER: 0
              //     });
              //   }

              //   if (ACCESSCOVER.length > 0) {
              //     this.setState({
              //       IsACCESSChecked: 1,
              //       IsACCESSSIEnabled: 1,
              //       ACCESSCOVER: ACCESSCOVER[0].SumInsured
              //     });
              //   } else {
              //     this.setState({
              //       IsACCESSChecked: 0,
              //       IsACCESSSIEnabled: 0,
              //       ACCESSCOVER: 0
              //     });
              //   }

              //   if (TPLCoverageId.length > 0) {
              //     this.setState({
              //       TPLCoverageId: TPLCoverageId[0].CoverageID
              //     });
              //   } else {
              //     this.setState({
              //       TPLCoverageId: null
              //     });
              //   }
              // }

              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                IsACCESSChecked: jsn.IsACCESSChecked,
                IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                IsPADRVREnabled: jsn.IsPADRVREnabled,
                IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                IsTPLEnabled: jsn.IsTPLEnabled,
                IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              this.setState({
                // GrossPremium: jsn.TotalPremi - jsn.AdminFee,
                AdminFee: jsn.AdminFee
                // NetPremi: jsn.TotalPremi
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.CoverProductCode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }
              this.setState({ ACCESSCOVER: 0 });
            }
          } else {
            // toast.dismiss();
            // toast.warning("❗ Calculate premi error", {
            //   position: "top-right",
            //   autoClose: 5000,
            //   hideProgressBar: false,
            //   closeOnClick: true,
            //   pauseOnHover: true,
            //   draggable: true
            // });
            if (!Util.isNullOrEmpty(jsn.message)) {
              this.onShowAlertModalInfo(jsn.message);
            }
            this.setNullCalculatePremi();
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.debugGroup("parsing failed", error);
          this.setState({
            isLoading: false
          });
          // toast.dismiss();
          // toast.warning("❗ Calculate premi error - " + error, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
          // if (!(error + "".toLowerCase().includes("token"))) {
          //   this.basicPremiCalculation();
          // }
        });
    } else {
      setTimeout(() => {
        Log.debugGroup("Hit timeout : basicPremiCalculation");
        this.basicPremiCalculation();
      }, 2000);
    }
  };
  getOrderDetailCoverage = () => {
    this.setState({ isLoading: true });
    var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailCoverage/`, {
      method: "POST",

      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "calculatedPremi=" +
        JSON.stringify(this.state.CalculatedPremiItems) +
        "&AdminFee=" +
        this.state.AdminFee +
        "&vtype=" +
        this.state.VehicleType +
        "&vcitycode=" +
        this.state.VehicleRegion +
        "&vusagecode=" +
        this.state.VehicleUsage +
        "&vyear=" +
        this.state.VehicleYear +
        "&vsitting=" +
        this.state.VehicleSitting +
        "&vModel=" +
        this.state.vehicleModelCode +
        "&vBrand=" +
        this.state.VehicleBrand +
        "&vIsNew=" +
        IsNewTemp +
        "&Ndays=" +
        this.state.Ndays +
        "&PeriodFrom=" +
        Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd") +
        "&PeriodTo=" +
        Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd") +
        "&vProductCode=" +
        this.state.CoverProductCode
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState(
          {
            dataorderdetailcoverageall: jsn.data
          },
          () => {
            this.setOrderDetailCoverage(jsn.data.dtlCoverage);
            Log.debugGroup(
              "datamapping.data.dtlCoverage: ",
              jsn.data.dtlCoverage
            );
            Log.debugGroup("datamapping.data: ", jsn.data);
          }
        );
      })
      .then(datamapping => {
        this.setState({
          // datas: datamapping.data[0],
          datacoverage: datamapping.dtlCoverage,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
        }
      });
  };

  checkTotalSumInsured = () => {
    this.setState({
      isErrCovSumInsured: false
    });
    if (
      this.state.VehiclePrice === "" ||
      this.state.VehiclePrice == null ||
      this.state.VehicleDetailsTemp === ""
    ) {
      this.setState({
        tempCovSumInsured: this.state.CovSumInsured
      });
    } else {
      let min = parseInt(
        this.state.tempCovSumInsured - (this.state.tempCovSumInsured * 3) / 100
      );
      let max = parseInt(
        this.state.tempCovSumInsured + (this.state.tempCovSumInsured * 3) / 100
      );
      Log.debugStr("MIN SUM INSURED: " + min);
      Log.debugStr("MAX SUM INSURED: " + max);
      Log.debugStr("SUM INSURED: " + this.state.CovSumInsured);
      Log.debugStr("TEMP SUM INSURED: " + this.state.tempCovSumInsured);
      if (
        this.state.CovSumInsured < min ||
        (this.state.CovSumInsured > max && this.state.VehicleDetailsTemp != "")
      ) {
        this.setState({
          isErrCovSumInsured: true
        });
      } else {
        this.setState({
          isErrCovSumInsured: false
        });
      }
    }

    if (
      this.state.CoverBasicCover != null &&
      this.state.CoverBasicCover != "" &&
      this.state.CoverProductCode != null &&
      this.state.CoverProductCode != "" &&
      this.state.CoverProductType != null &&
      this.state.CoverProductType != "" &&
      !this.state.isErrCovSumInsured
    ) {
      this.setState(
        {
          isErrCovBasicCover: false,
          isErrCovProductCode: false,
          isErrCovProductType: false,
          isErrorCover: false
        },
        () => {
          if (this.state.CovSumInsured != null) {
            this.basicPremiCalculation();
          }
        }
      );
      this.setState({
        IsACCESSChecked: 0,
        IsACCESSSIEnabled: 0,
        ACCESSCOVER: 0
      });
    } else {
      console.log("belum hit rate calculate");
    }

    this.basicPremiCalculation();

    return this.state.isErrCovSumInsured;
  };

  premiumCalculation = () => {
    Log.debugGroup("hit extended premium simulation");

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.CoverBasicCover
    )[0];
    Log.debugGroup("basiccover : ", BasicCover);

    if (!Util.isNullOrEmpty(BasicCover)) {
      let IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;

      let vTSI = this.state.CovSumInsured;
      let vsitting = this.state.VehicleSitting;
      let TPLCoverageId = "";
      let vType = this.state.VehicleType;
      let vPrimarySI = this.state.tempCovSumInsured;

      if (this.state.chItemTemp == "PAPASS") {
        // vsitting = this.state.PASSCOVER;
        vTSI = this.state.PAPASSICOVER;

        if (
          Util.isNullOrEmpty(this.state.PAPASSICOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Passenger is mandatory", "warning");
          return;
        }

        if (Util.isNullOrEmpty(this.state.PASSCOVER)) {
          Util.showToast("Pa Passenger Number is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "PADRVR") {
        vTSI = this.state.PADRVCOVER;

        if (
          Util.isNullOrEmpty(this.state.PADRVCOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Driver is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "ACCESS") {
        vTSI = this.state.ACCESSCOVER;
        vPrimarySI = this.state.CovSumInsured;
        if (
          Util.isNullOrEmpty(this.state.ACCESSCOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Accessories is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "TPLPER") {
        TPLCoverageId = this.state.TPLCoverageId;
        vTSI = this.state.TPLSICOVER;
        if (
          Util.isNullOrEmpty(this.state.TPLSICOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI TPL is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "SRCC") {
        vType = "";
      } else if (this.state.chItemTemp == "ETV") {
        vType = "";
      } else if (this.state.chItemTemp == "FLD") {
        vType = "";
      }

      // if(Util.isNullOrEmpty(vTSI)){
      //   Util.showToast("Sum Insured is mandatory", "warning");
      //   return;
      // }

      this.setState({
        isLoading: true
      });

      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/PremiumCalculation/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "ComprePeriod=" +
          BasicCover.ComprePeriod + /// CATATAN
          "&TLOPeriod=" +
          BasicCover.TLOPeriod + /// CATATAN
          "&PeriodFrom=" +
          (Util.isNullOrEmpty(this.state.vehicleperiodfrommodalperiod)
            ? null
            : Util.formatDate(
                this.state.vehicleperiodfrommodalperiod,
                "yyyy-mm-dd"
              )) +
          "&PeriodTo=" +
          (Util.isNullOrEmpty(this.state.vehicleperiodtomodalperiod)
            ? null
            : Util.formatDate(
                this.state.vehicleperiodtomodalperiod,
                "yyyy-mm-dd"
              )) +
          "&vtype=" +
          vType +
          "&vcitycode=" +
          this.state.VehicleRegion +
          "&vusagecode=" +
          this.state.VehicleUsage +
          "&vyear=" +
          this.state.VehicleYear +
          "&vsitting=" +
          vsitting +
          "&vProductCode=" +
          this.state.CoverProductCode +
          "&vTSInterest=" +
          vTSI +
          "&vPrimarySI=" +
          vPrimarySI +
          "&vCoverageId=" +
          BasicCover.CoverageId +
          "&pQuotationNo=" +
          "&Ndays=" +
          this.state.Ndays +
          "&vBrand=" +
          this.state.VehicleBrand +
          "&vModel=" +
          this.state.vehicleModelCode +
          "&isNew=" +
          IsNewTemp +
          "&chItem=" +
          this.state.chItemTemp +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&TPLCoverageId=" +
          TPLCoverageId
      })
        .then(res => res.json())
        .then(jsn => {
          this.setState({ showdialogperiod: false });
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              // if (jsn.Alert != "") {
              //   this.onShowAlertModalInfo(jsn.Alert);
              // }
              // if (isLoading == true) {
              //   let PADRVCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PADRVR"
              //   );
              //   let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PAPASS"
              //   );
              //   let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "ACCESS"
              //   );
              //   let TPLCoverageId = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "TPLPER"
              //   );

              //   if (PADRVCOVER.length > 0) {
              //     this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              //   } else {
              //     this.setState({ PADRVCOVER: 0 });
              //   }

              //   if (PAPASSICOVER.length > 0) {
              //     this.setState({
              //       PAPASSICOVER: PAPASSICOVER[0].SumInsured,
              //       PASSCOVER: PAPASSICOVER[0].CoverageID.trim().replace(
              //         "C",
              //         ""
              //       )
              //     });
              //   } else {
              //     this.setState({
              //       PAPASSICOVER: 0,
              //       PASSCOVER: 0
              //     });
              //   }

              //   if (ACCESSCOVER.length > 0) {
              //     this.setState({
              //       IsACCESSChecked: 1,
              //       IsACCESSSIEnabled: 1,
              //       ACCESSCOVER: ACCESSCOVER[0].SumInsured
              //     });
              //   } else {
              //     this.setState({
              //       IsACCESSChecked: 0,
              //       IsACCESSSIEnabled: 0,
              //       ACCESSCOVER: 0
              //     });
              //   }

              //   if (TPLCoverageId.length > 0) {
              //     this.setState({
              //       TPLCoverageId: TPLCoverageId[0].CoverageID
              //     });
              //   } else {
              //     this.setState({
              //       TPLCoverageId: null
              //     });
              //   }
              // }

              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PADRVR"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PADRVR",
                    "PADDR1"
                  ])
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PAPASS"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PAPASS",
                    "PA24AV"
                  ])
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "ACCESS"
                data => Util.stringArrayContains(data.InterestID, ["ACCESS"])
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "TPLPER"
                data => Util.stringArrayContains(data.InterestID, ["TPLPER"])
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured,
                  // PASSCOVER: this.state.vehicleSitting - 1,
                  PASSCOVER: parseInt(this.state.VehicleSitting) - 1
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0
                  // PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID,
                  TPLSICOVER: TPLCoverageId[0].SumInsured
                });
              } else {
                this.setState({
                  TPLCoverageId: null,
                  TPLSICOVER: 0
                });
              }

              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                IsACCESSChecked: jsn.IsACCESSChecked,
                IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                IsPADRVREnabled: jsn.IsPADRVREnabled,
                IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                IsTPLEnabled: jsn.IsTPLEnabled,
                IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              this.setState({
                Admin: jsn.AdminFee
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.CoverProductCode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ Calculate premi error", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.debugGroup("parsing failed", error);
          this.setState({ showdialogperiod: false });
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Calculate premi error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          if (!(error + "".toLowerCase().includes("token"))) {
            // this.premiumCalculation();
          }
        });
    }
  };

  isValidChecklistMvGodig = () => {
    let res = true;
    if (!this.state.isMvGodig && !this.state.isNonMvGodig) {
      // Not Checked both IsMvGodig or IsNonMvGodig
      this.setState({
        isErrIsMvGodig: true
      });
      res = false;
    }

    return res;
  };

  // handle disable checklist mv godig
  isDisableMVGodig = () => {
    let res = false;

    if (this.state.isDisableIsMvGodig) {
      res = true;
    }

    return res;
  };

  handleBundling = (CalculatedPremiItems = this.state.CalculatedPremiItems) => {
    this.setState(
      {
        IsSRCCCheckedEnable: 1,
        IsETVCheckedEnable: 1,
        IsFLDCheckedEnable: 1,
        IsTRSCheckedEnable: 1,
        IsPADRIVERCheckedEnable: 1,
        IsPAPASSCheckedEnable: 1
      },
      () => {
        for (let index = 0; index < CalculatedPremiItems.length; index++) {
          const element = CalculatedPremiItems[index];

          if (
            element.CoverageID.toLowerCase().includes("src") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsSRCCCheckedEnable: 0,
              IsSRCCCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("fld") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsFLDCheckedEnable: 0,
              IsFLDCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("etv") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsETVCheckedEnable: 0,
              IsETVCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trs") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trrtlo") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("paddr1") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("padrvr") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("papass") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("pa24av") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          }
        }
      }
    );
  };

  rateCalculate = () => {
    this.loadingproduct = true;
    var param = "";
    if (this.state.loadingvehicletocover == false) {
      param = "&OrderNo=" + this.state.OrderNo;
      this.setState({ isLoading: true });
    }
    var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/rateCalculation/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "ProductCode=" +
        this.state.CoverProductCode +
        "&TLOPeriod=" +
        this.state.CoverTLOPeriod +
        "&ComprePeriod=" +
        this.state.CoverComprePeriod +
        "&SumInsured=" +
        parseInt(this.state.CovSumInsured) +
        "&ProductTypeCode=" +
        this.state.VehicleProductTypeCode +
        "&BrandCode=" +
        this.state.VehicleBrand +
        "&VehicleCode=" +
        this.state.VehicleCode +
        "&ModelCode=" +
        this.state.VehicleType +
        "&Type=" +
        this.state.VehicleTypeType +
        "&Series=" +
        this.state.VehicleSeries +
        "&Year=" +
        this.state.VehicleYear +
        "&UsageCode=" +
        this.state.VehicleUsage +
        "&CityCode=" +
        this.state.VehicleRegion +
        "&IsNew=" +
        IsNewTemp +
        param
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          ////////////////LANJUTKAN COVER
          // console.log(jsn);
          if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
            this.onShowAlertModalInfo(jsn.Alert);
          } else {
            if (jsn.Alert != "") {
              this.onShowAlertModalInfo(jsn.Alert);
            }
            this.setState({
              loadingvehicletocover: true,
              ACCESSPremi: jsn.ACCESSPremi,
              AdminFee: jsn.AdminFee,
              Alert: jsn.Alert,
              CalculatedPremiItems: jsn.CalculatedPremiItems,
              ETVPremi: jsn.ETVPremi,
              FLDPremi: jsn.FLDPremi,
              // IsACCESSChecked: jsn.IsACCESSChecked,
              // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
              IsACCESSEnabled: jsn.IsACCESSEnabled,
              IsETVChecked: jsn.IsETVChecked,
              IsETVEnabled: jsn.IsETVEnabled,
              IsFLDChecked: jsn.IsFLDChecked,
              IsFLDEnabled: jsn.IsFLDEnabled,
              IsPADRVRChecked: jsn.IsPADRVRChecked,
              IsPADRVREnabled: jsn.IsPADRVREnabled,
              IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
              IsPAPASSChecked: jsn.IsPAPASSChecked,
              IsPAPASSEnabled: jsn.IsPAPASSEnabled,
              IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
              IsPASSEnabled: jsn.IsPASSEnabled,
              IsSRCCChecked: jsn.IsSRCCChecked,
              IsSRCCEnabled: jsn.IsSRCCEnabled,
              IsTPLChecked: jsn.IsTPLChecked,
              IsTPLEnabled: jsn.IsTPLEnabled,
              IsTPLSIEnabled: jsn.IsTPLSIEnabled,
              IsTSChecked: jsn.IsTSChecked,
              IsTSEnabled: jsn.IsTSEnabled,
              PADRVRPremi: jsn.PADRVRPremi,
              PAPASSPremi: jsn.PAPASSPremi,
              SRCCPremi: jsn.SRCCPremi,
              TPLPremi: jsn.TPLPremi,
              TSPremi: jsn.TSPremi,
              TotalPremi: jsn.TotalPremi,
              coveragePeriodItems: jsn.coveragePeriodItems,
              coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
            });

            if (jsn.IsTPLChecked == 1) {
              this.getTPLSI(this.state.CoverProductCode);
            }

            if (jsn.IsPADRVRChecked == 0) {
              this.setState({ PADRVCOVER: 0 });
            }

            if (jsn.IsPAPASSChecked == 0) {
              this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
            }

            if (jsn.IsTPLChecked == 0) {
              this.setState({ TPLCoverageId: null });
            }
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Calculate premi error", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.loadingproduct = false;
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        this.loadingproduct = false;
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculate();
        }
      });
  };

  // hit calculate premi when check TPL / PA PASSENGER / PA DRIVER / ACCESSORY
  rateCalculateTPLPAPASSPADRV = jenisCheckList => {
    var param = "";
    if (
      jenisCheckList != null &&
      jenisCheckList != "" &&
      this.loadingproduct == false
    ) {
      if (jenisCheckList == "TPL") {
        param = "&TPLCoverageID=" + this.state.TPLCoverageId;
      } else if (jenisCheckList == "PAPASSICOVER") {
        param =
          "&PASS=" +
          parseInt(this.state.PASSCOVER + "") +
          "&PAPASSSI=" +
          parseInt(this.state.PAPASSICOVER + "");
      } else if (jenisCheckList == "PADRVCOVER") {
        param = "&PADRVRSI=" + parseInt(this.state.PADRVCOVER + "");
      } else if (jenisCheckList == "ACCESSCOVER") {
        param = "&AccessSI=" + parseInt(this.state.ACCESSCOVER + "");
      }

      // this.setState({ isLoading: true });
      var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/rateCalculation/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "CityCode=" +
          this.state.VehicleRegion +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&TLOPeriod=" +
          this.state.CoverTLOPeriod +
          "&ComprePeriod=" +
          this.state.CoverComprePeriod +
          "&SumInsured=" +
          parseInt(this.state.CovSumInsured) +
          "&ProductTypeCode=" +
          this.state.VehicleProductTypeCode +
          "&CalculatedPremi=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems) +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          param
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            ////////////////LANJUTKAN COVER
            // console.log(jsn);
            if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
              toast.dismiss();
              toast.warning("❗ " + jsn.Alert, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
              });
            } else {
              if (jsn.Alert != "") {
                toast.dismiss();
                toast.warning("❗ " + jsn.Alert, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                });
              }
              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                // IsACCESSChecked: jsn.IsACCESSChecked,
                // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                // IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                // IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                // IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                // IsPADRVREnabled: jsn.IsPADRVREnabled,
                // IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                // IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                // IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                // IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                // IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                // IsTPLEnabled: jsn.IsTPLEnabled,
                // IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                // IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }
            }
          }
          // else {
          //   toast.dismiss(); toast.warning("❗ Calculate premi error", {
          //     position: "top-right",
          //     autoClose: 5000,
          //     hideProgressBar: false,
          //     closeOnClick: true,
          //     pauseOnHover: true,
          //     draggable: true
          //   });
          // }
          // this.setState({
          //   isLoading: false
          // });
        })
        .catch(error => {
          console.log("parsing failed", error);
          // this.setState({
          //   isLoading: false
          // });
          // toast.dismiss(); toast.warning("❗ Calculate premi error - " + error, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
          if (!(error + "").toLowerCase().includes("token")) {
            this.rateCalculateTPLPAPASSPADRV(jenisCheckList);
          }
        });
    }
  };

  setDialogPeriod = chType => {
    var chItemSetPeriod = false;
    var chItemSetPeriodEnable = 0;

    var chItemTemp = -1;

    this.setState({ chItemTemp }, () => {
      if (chType == 0) {
        chItemTemp = "SRCC";
        if (this.state.IsSRCCCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 1) {
        chItemTemp = "FLD";
        if (this.state.IsFLDCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 2) {
        chItemTemp = "ETV";
        if (this.state.IsETVCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 3) {
        chItemTemp = "TS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
      } else if (chType == 4) {
        chItemTemp = "TPLPER"; // TPLPER
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
        this.getTPLSI(this.state.CoverProductCode);
      } else if (chType == 5) {
        chItemTemp = "PADRVR";
        chItemSetPeriod = true;
        if (this.state.IsPAPASSChecked == 1) {
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriodEnable = 1;
        }

        this.getRetrieveextsi(chItemTemp);
      } else if (chType == 6) {
        chItemTemp = "PAPASS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;

        this.getRetrieveextsi(chItemTemp);

        // HANDLE PASS
        if (!Util.isNullOrEmpty(this.state.VehicleDetails)) {
          this.setState({
            PASSCOVER: parseInt(this.state.VehicleSitting) - 1
          });
        }
      } else if (chType == 7) {
        chItemTemp = "ACCESS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
      } // ACCESS
      this.setState({ chItemTemp, chItemSetPeriod, chItemSetPeriodEnable });

      this.setState(
        {
          vehicleperiodfrommodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodfrom,
          vehicleperiodtomodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodto
        },
        () => {
          this.setState({ showdialogperiod: true });

          // if (chItemTemp == "TPLPER") {
          //   if (this.detectComprePeriod()) {
          //     let basicCoverage = [...this.state.databasiccoverall].filter(
          //       data => data.Id == this.state.CoverBasicCover
          //     )[0];

          //     this.setState({
          //       vehicleperiodtomodalperiod: Util.convertDate(
          //         Util.formatDate(this.state.vehicleperiodfrom)
          //       ).setFullYear(
          //         Util.convertDate(
          //           Util.formatDate(this.state.vehicleperiodfrom)
          //         ).getFullYear() + basicCoverage.ComprePeriod
          //       )
          //     });
          //   }
          // }

          if (chItemTemp == "PADRVR") {
            this.setPeriodToPeriodFromExtendedCover("PAD1", "COVERAGE", () => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTemp,
                vehicleperiodfrommodalperiod: this.state.periodfromTemp
              });
            });
          }

          if (chItemTemp == "TPLPER") {
            this.setPeriodToPeriodFromExtendedCover(
              "TPLPER",
              "INTEREST",
              () => {
                this.setState({
                  vehicleperiodtomodalperiod: this.state.periodtoTemp,
                  vehicleperiodfrommodalperiod: this.state.periodfromTemp
                });
              }
            );
          }

          if (chItemTemp == "PAPASS") {
            this.setPeriodToPeriodFromExtendedCover("PAP1", "COVERAGE", () => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTemp,
                vehicleperiodfrommodalperiod: this.state.periodfromTemp
              });
            });
          }

          if (chItemTemp == "TS") {
            this.setPeriodToPeriodFromTS(() => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTS,
                vehicleperiodfrommodalperiod: this.state.periodfromTS
              });
            });
          }
        }
      );
    });
  };

  detectComprePeriod = () => {
    let temp = false;
    if (
      !Util.isNullOrEmpty(this.state.CoverBasicCover) &&
      this.state.databasiccoverall.length > 0
    ) {
      let basicCoverage = [...this.state.databasiccoverall].filter(
        data => data.Id == this.state.CoverBasicCover
      )[0];
      if (!Util.isNullOrEmpty(basicCoverage)) {
        if (basicCoverage.ComprePeriod > 0) {
          temp = true;
        }
      }
    }

    return temp;
  };

  rateCalculationNonBasic = () => {
    // console.log("masukkkkk");
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/rateCalculationNonBasic/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "chItem=" +
          this.state.chItemTemp +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&SumInsured=" +
          this.state.CovSumInsured +
          "&chPAPASS=" +
          this.state.IsPAPASSCheckedtemp +
          "&chPADRVR=" +
          this.state.IsPADRVRCheckedtemp +
          "&chTPL=" +
          this.state.IsTPLCheckedtemp +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&CityCode=" +
          this.state.VehicleRegion +
          "&isExtentedBundling=" +
          this.state.isExtentedBundlingtemp +
          "&chOne=" +
          this.state.chOne +
          "&chTwo=" +
          this.state.chTwo +
          "&chThree=" +
          this.state.chThree +
          "&ischOneEnabled=" +
          this.state.chOneEnabled +
          "&ischTwoEnabled=" +
          this.state.chTwoEnable +
          "&ischThreeEnabled=" +
          this.state.chThreeEnabled +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems) +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&PADRVRSI=" +
          this.state.PADRVCOVER +
          "&PAPASSSI=" +
          this.state.PAPASSICOVER +
          "&PASS=" +
          this.state.PASSCOVER +
          "&AccessSI=" +
          this.state.ACCESSCOVER
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn);
        if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
          toast.dismiss();
          toast.warning("❗ " + jsn.Alert, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          this.setState({ showdialogperiod: true });
        } else {
          if (jsn.Alert != "") {
            toast.dismiss();
            toast.warning("❗ " + jsn.Alert, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          if (jsn.IsPADRVRChecked == 0) {
            this.setState({ PADRVCOVER: 0 });
          }

          if (jsn.IsPAPASSChecked == 0) {
            this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
          }

          if (jsn.IsTPLChecked == 0) {
            this.setState({ TPLCoverageId: null });
          }

          this.setState({
            ACCESSPremi: jsn.ACCESSPremi,
            // AdminFee: jsn.AdminFee,
            Alert: jsn.Alert,
            CalculatedPremiItems: jsn.CalculatedPremiItems,
            ETVPremi: jsn.ETVPremi,
            FLDPremi: jsn.FLDPremi,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            IsETVChecked: jsn.IsETVChecked,
            IsFLDChecked: jsn.IsFLDChecked,
            IsPADRVRChecked: jsn.IsPADRVRChecked,
            IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
            IsPAPASSChecked: jsn.IsPAPASSChecked,
            IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
            IsPASSEnabled: jsn.IsPAPASSSIEnabled,
            IsSRCCChecked: jsn.IsSRCCChecked,
            IsTPLChecked: jsn.IsTPLChecked,
            IsTPLSIEnabled: jsn.IsTPLSIEnabled,
            IsTSChecked: jsn.IsTSChecked,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            PADRVRPremi: jsn.PADRVRPremi,
            PAPASSPremi: jsn.PAPASSPremi,
            SRCCPremi: jsn.SRCCPremi,
            TPLPremi: jsn.TPLPremi,
            TSPremi: jsn.TSPremi,
            ACCESSPremi: jsn.ACCESSPremi,
            TotalPremi: jsn.TotalPremi,
            coveragePeriodItems: jsn.coveragePeriodItems,
            coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems,
            showdialogperiod: false
          });
          if (jsn.IsTPLChecked == 1) {
            this.getTPLSI(this.state.CoverProductCode);
          }
        }
        this.setState({ isLoading: false });
        this.hitratecalculationsimulation = true;
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculationNonBasic();
        }
      });
  };

  rateCalculationNonBasicUnCheckAccesories = () => {
    console.log("rateCalculationNonBasicUnCheckAccesories");
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/rateCalculationNonBasic/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "chItem=access" +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&SumInsured=" +
          this.state.CovSumInsured +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&CityCode=" +
          this.state.VehicleRegion +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems)
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn);
        if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
          toast.dismiss();
          toast.warning("❗ " + jsn.Alert, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          if (jsn.Alert != "") {
            toast.dismiss();
            toast.warning("❗ " + jsn.Alert, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
            // this.onShowAlertModalInfo(jsn.Alert);
          }

          if (jsn.IsPADRVRChecked == 0) {
            this.setState({ PADRVCOVER: 0 });
          }

          if (jsn.IsPAPASSChecked == 0) {
            this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
          }

          if (jsn.IsTPLChecked == 0) {
            this.setState({ TPLCoverageId: null });
          }

          this.setState({
            ACCESSPremi: jsn.ACCESSPremi,
            // AdminFee: jsn.AdminFee,
            Alert: jsn.Alert,
            CalculatedPremiItems: jsn.CalculatedPremiItems,
            ETVPremi: jsn.ETVPremi,
            FLDPremi: jsn.FLDPremi,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            IsETVChecked: jsn.IsETVChecked,
            IsFLDChecked: jsn.IsFLDChecked,
            IsPADRVRChecked: jsn.IsPADRVRChecked,
            IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
            IsPAPASSChecked: jsn.IsPAPASSChecked,
            IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
            IsPASSEnabled: jsn.IsPAPASSSIEnabled,
            IsSRCCChecked: jsn.IsSRCCChecked,
            IsTPLChecked: jsn.IsTPLChecked,
            IsTPLSIEnabled: jsn.IsTPLSIEnabled,
            IsTSChecked: jsn.IsTSChecked,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            PADRVRPremi: jsn.PADRVRPremi,
            PAPASSPremi: jsn.PAPASSPremi,
            SRCCPremi: jsn.SRCCPremi,
            TPLPremi: jsn.TPLPremi,
            TSPremi: jsn.TSPremi,
            ACCESSPremi: jsn.ACCESSPremi,
            TotalPremi: jsn.TotalPremi,
            coveragePeriodItems: jsn.coveragePeriodItems,
            coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems,
            showdialogperiod: false
          });
          if (jsn.IsTPLChecked == 1) {
            this.getTPLSI(this.state.CoverProductCode);
          }
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculationNonBasic();
        }
      });
  };

  onChangeVehicleDetails = (VehicleDetailsTemp, name) => {
    let data = [...this.state.datavehicledetailsall].filter(
      data => data.VehicleDescription == VehicleDetailsTemp
    );
    Log.debugGroup("VEHICLE DESCRIPTION: ", data[0]);
    if (this.state.activetab === "Summary Details") {
      this.getListBrand();
    }

    this.getListBasicCover();
    this.setState(
      {
        VehicleSumInsured: data[0].Price,
        VehicleProductTypeCode: data[0].ProductTypeCode,
        VehicleBrand: data[0].BrandCode,
        VehicleCode: data[0].VehicleCode,
        VehicleType: data[0].Type,
        VehicleTypeType: data[0].Type,
        VehicleSeries: data[0].Series,
        VehicleYear: data[0].Year,
        VehicleCodetemp: data[0].VehicleCode,
        VehicleSitting: data[0].Sitting,
        VehicleDetails: data[0].VehicleDescription,
        VehicleDetailsTemp: data[0].VehicleDescription,
        vehicleModelCode: data[0].ModelCode,
        VehicleUsedCar: !data[0].IsNew,
        PASSCOVER: parseInt(data[0].Sitting) - 1
      },
      () => {
        this.onOptionVehTypeChange(this.state.VehicleType);
        if (
          this.state.VehicleRegion != null ||
          this.state.VehicleRegion != ""
        ) {
          this.getVehiclePrice(
            data[0].VehicleCode,
            data[0].Year,
            this.state.VehicleRegion
          );
        }

        this.reloadDataFormIfMVGodig();
      }
    );
  };

  onChangeFunctionDate = (value, name) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        if (name == "vehicleperiodto") {
          this.basicPremiCalculation();
        }

        if (name == "vehicleperiodfrom") {
          this.basicPremiCalculation();
        }

        this.getOrderDetailCoverage();
      }
    );
  };

  onChangeVehicleCode = () => {
    let { CoverProductCode } = {
      ...this.state
    };
    fetch(`${API_URL + "" + API_VERSION2}/DataReact/ResetVehicleCode/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "vehicleCode=" + this.state.VehicleCodetemp
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          this.setState(
            {
              VehicleCode: jsn.data.VehicleCode,
              VehicleDetails: jsn.data.VehicleDescription,
              VehicleDetailsTemp: jsn.data.VehicleDescription,
              VehicleCodetemp: jsn.data.VehicleCode,
              VehicleBrand: jsn.data.BrandCode,
              VehicleProductTypeCode: jsn.data.ProductTypeCode,
              VehicleType: jsn.data.ModelCode,
              VehicleSeries: jsn.data.Series,
              VehicleTypeType: jsn.data.Type,
              VehicleSitting: jsn.data.Sitting,
              VehicleYear: jsn.data.Year,
              VehicleSumInsured: jsn.data.Price
            },
            () => {
              if (jsn.data.VehicleBrand != "") {
                // this.onOptionVehBrandChange(this.state.VehicleBrand);
              }
              if (jsn.data.VehicleYear != "") {
                // this.onOptionVehYearChange(this.state.VehicleYear);
              }
              if (jsn.data.VehicleType != "") {
                // this.onOptionVehTypeChange(this.state.VehicleType);
              }
              if (jsn.data.VehicleSeries != "") {
                // this.onOptionVehSeriesChange(this.state.VehicleSeries);
              }
              if (jsn.data.VehicleDetails != "") {
                this.onOptionVehDetailsChange(this.state.VehicleDetails);
              }
              if (jsn.data.VehicleCode != "") {
                this.getListProductType();
                this.onOptionCovProductTypeChange(this.state.CoverProductType);
                this.setState({ CoverProductCode });
                this.getListBasicCover();
                this.setState({ CovSumInsured: this.state.VehicleSumInsured });
              }
            }
          );
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
      });
  };

  onChangeFunctionReactNumber = (value, name, callback = () => {}) => {
    Log.debugStr("CHECK [" + name + "] => " + value);
    this.setState(
      {
        [name]: value
      },
      () => {
        callback();
      }
    );
  };

  getRetrieveextsi = (interestId, callback = () => {}) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/retrieveextsi/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "InterestId=" + interestId
    })
      .then(res => res.json())
      .then(jsn => {
        // Log.debugGroup(jsn);
        return jsn.data.map(data => ({
          value: `${data}`,
          label: `${Util.formatMoney(data, 0)}`
        }));
      })
      .then(datamapping => {
        this.data = true;
        this.setState(
          {
            dataextsi: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        // this.loadProductType = true;
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  getTPLSI = (ProductCode, callback = () => {}) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getTPLSIList/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "ProductCode=" + ProductCode
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({ dataTPLSIall: jsn.data });
        return jsn.data.map(data => ({
          value: `${data.CoverageId}`,
          label: `${Util.formatMoney(data.TSI, 0)}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            dataTPLSI: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  onSaveSimulation = () => {
    this.setState(() => {
      if (this.state.ProspectEmail === "" || this.state.ProspectEmail == null) {
        this.state.isErrEmail = true;
      } else {
        this.state.isErrEmail = false;
        this.onSavePremiumSimulation();
      }
    });
    console.log("lanjutSave");
  };

  onSendEmail = () => {
    this.setState(
      {
        Email1: this.state.ProspectEmail
      },
      () => {
        this.setState({
          showModalSendEmail: true
        })
      }
    );
  };

  onSendSMS = () => {
    if (
      this.state.ProspectPhone1.length < 9 &&
      this.state.ProspectPhone1.length > 13
    ) {
      this.state.isErrPhone1 = true;
    } else {
      this.setState({ showModalSendSMS: true });
    }
    console.log("lanjutSMS");
  };

  onSavePremiumSimulation = (callback = () => {}) => {
    this.setState({
      isLoading: true,
      showModal: false
    });

    const { state, props } = this;

    const ProspectCustomer = {
      CustID: state.CustId,
      Name: state.ProspectName,
      Phone1: state.ProspectPhone1,
      Phone2: state.ProspectPhone2,
      Email1: state.ProspectEmail,
      Email2: "",
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      BranchCode: props.userInfo.User.BranchCode,
      isCompany: state.isCompany
    };

    const FollowUp = {
      FollowUpNo: this.state.FollowUpNumber,
      LastSeqNo: "0",
      CustID: state.CustId,
      ProspectName: state.ProspectName,
      Phone1: state.ProspectPhone1,
      Phone2: state.ProspectPhone2,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      FollowUpName: state.ProspectName,
      FollowUpStatus: "1",
      FollowUpInfo: "1",
      Remark: "",
      BranchCode: props.userInfo.User.BranchCode,
      SalesAdminID: null
    };

    const OrderSimulation = {
      OrderNo: state.OrderNo,
      CustID: state.CustId,
      FollowUpNo: "",
      QuotationNo: "",
      applyF: true,
      TLOPeriod: this.state.CoverTLOPeriod,
      ComprePeriod: this.state.CoverComprePeriod,
      BranchCode: props.userInfo.User.BranchCode,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      PhoneSales: props.userInfo.User.Phone1,
      ProductTypeCode: this.state.VehicleProductTypeCode,
      ProductCode: this.state.CoverProductCode,
      Phone1: this.state.ProspectPhone1,
      Phone2: this.state.ProspectPhone2,
      Email1: this.state.ProspectEmail,
      Email2: "",
      InsuranceType: parseInt(this.state.CoverProductType),
      PeriodTo: Util.isNullOrEmpty(this.state.vehicleperiodto)
        ? null
        : Util.formatDate(this.state.vehicleperiodto),
      PeriodFrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
        ? null
        : Util.formatDate(this.state.vehicleperiodfrom),
      AdminFee: this.state.AdminFee,
      TotalPremium: this.state.TotalPremi
    };

    const OrderSimulationMV = {
      OrderNo: state.OrderNo,
      ObjectNo: "1",
      ProductTypeCode: state.VehicleProductTypeCode,
      VehicleCode: state.VehicleCode,
      BrandCode: state.VehicleBrand,
      ModelCode: state.vehicleModelCode,
      Series: state.VehicleSeries,
      Type: state.VehicleTypeType,
      Sitting: state.VehicleSitting,
      Year: state.VehicleYear,
      CityCode: state.VehicleRegion,
      UsageCode: state.VehicleUsage,
      SumInsured: state.CovSumInsured,
      RegistrationNumber: state.RegistrationNumber,
      EngineNumber: state.EngineNumber,
      ChasisNumber: state.ChasisNumber,
      IsNew: !state.VehicleUsedCar,
      VehicleDescription: state.VehicleDetailsTemp
    };

    // console.log(OrderSimulation);
    DataSource.SaveProspect(
      {
        State: "PREMIUMSIMULATION",
        GuidTempPenawaran: this.state.GuidTempPenawaran,
        IsMvGodig: this.state.isMvGodig,
        isNonMvGodig: this.state.isNonMvGodig,
        OrderSimulation: JSON.stringify(OrderSimulation),
        OrderSimulationMV: JSON.stringify(OrderSimulationMV),
        ProspectCustomer: JSON.stringify(ProspectCustomer),
        FollowUp: JSON.stringify(FollowUp),
        calculatedPremiItems: JSON.stringify(this.state.CalculatedPremiItems)
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          this.setState(
            {
              isLoading: false,
              OrderNo: jsn.OrderNo,
              FollowUpNumber: jsn.FollowUpNo,
              GuidTempPenawaran: jsn.GuidTempPenawaran,
              CustId: jsn.CustId
            },
            () => {
              callback();
            }
          );
          // console.log(datamapping);
        } else {
          this.onShowAlertModalInfo(jsn.message);
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });

    console.log("save");
  };

  formatMoney = (amount, decimalCount = 0, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  };

  onCloseModalInfo = () => {
    this.setState({ showModalInfo: false });
    // this.onClickTabs("Premi Calculation");
  };

  onShowAlertModalInfo = MessageAlertCover => {
    this.setState({
      showModalInfo: true,
      MessageAlertCover
    });
  };

  handleShowPeriod = () => {
    let temp = false;
    if (
      !Util.isNullOrEmpty(this.state.CoverBasicCover) &&
      this.state.databasiccoverall.length > 0
    ) {
      let basicCoverage = [...this.state.databasiccoverall].filter(
        data => data.Id == this.state.CoverBasicCover
      )[0];
      if (!Util.isNullOrEmpty(basicCoverage)) {
        if (basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0) {
          temp = true;
        }
      }
    }

    return temp;
  };

  isNotValidSaveVehicle = () => {
    let res = false;

    if (!Util.isNullOrEmpty(this.state.RegistrationNumber)) {
      let patt = new RegExp("^[A-Za-z]{1,2}[0-9]{1,4}[A-Za-z]{0,3}$");

      if (!patt.test(this.state.RegistrationNumber)) {
        Util.showToast("Registration number not valid!", "WARNING");
        this.setState({
          isErrVehRegistrationNumber: true
        });
        res = true;
      }
    }

    return res;
  };

  onClickButtonSave = () => {
    if (this.isNotValidSaveVehicle()) {
      return;
    }
    this.setState({ showModal: true });
  };

  setStateCom = (value, callback = () => {}) => {
    this.setState(value, callback);
  };

  functionTypeProspect = () => {
    return {
      isDisableMVGodig: this.isDisableMVGodig,
      onChangeFunction: this.onChangeFunction,
      setState: this.setStateCom
    };
  };

  functionPremiCalculation = () => {
    return {
      onSubmitVehicle: this.onSubmitVehicle,
      onChangeFunction: this.onChangeFunction,
      getVehicle: this.getVehicle,
      onChangeVehicleDetails: this.onChangeVehicleDetails,
      onOptionVehTypeChange: this.onOptionVehTypeChange,
      onOptionCovProductTypeChange: this.onOptionCovProductTypeChange,
      onOptionCovProductCodeChange: this.onOptionCovProductCodeChange,
      onOptionVehUsageChange: this.onOptionVehUsageChange,
      onOptionVehRegionChange: this.onOptionVehRegionChange,
      handleShowPeriod: this.handleShowPeriod,
      onChangeFunctionDate: this.onChangeFunctionDate,
      checkTotalSumInsured: this.checkTotalSumInsured,
      setDialogPeriod: this.setDialogPeriod,
      onOptionCovBasicCoverChange: this.onOptionCovBasicCoverChange,
      onOptionCovBasicCoverChange: this.onOptionCovBasicCoverChange,
      onOptionCovBasicCoverChange: this.onOptionCovBasicCoverChange,
      ShowAccessoryInfo: this.ShowAccessoryInfo,
      setState: this.setStateCom
    };
  };

  functionPremiEstimation = () => {
    return {
      onSubmitCover: this.onSubmitCover,
      formatMoney: this.formatMoney,
      setState: this.setStateCom
    };
  };

  functionSummaryDetails = () => {
    return {
      onSubmitProspect: this.onSubmitProspect,
      formatMoney: this.formatMoney,
      onChangeFunction: this.onChangeFunction,
      getVehicle: this.getVehicle,
      onChangeVehicleDetails: this.onChangeVehicleDetails,
      setState: this.setStateCom
    };
  };

  render() {
    let check =
      this.state.activetab == "Summary Details" &&
      this.state.VehicleBrand != "" &&
      this.state.VehicleType != "" &&
      this.state.vehicleModelCode != "" &&
      this.state.datavehiclebrand.length > 0 &&
      this.state.datavehicletype.length > 0 &&
      this.state.datacoverbasiccover.length > 0
        ? "true"
        : "false";

    Log.debugGroup("RENDER Check State = ", this.state.activetab || "null");
    Log.debugStr("RENDER Check Summary -> " + check);
    Log.debugStr("RENDER ActiveTab => " + this.state.activetab);
    Log.debugStr("RENDER Brand => " + this.state.VehicleBrand);
    Log.debugStr("RENDER Type => " + this.state.VehicleType);
    Log.debugStr("RENDER Model code => " + this.state.vehicleModelCode);
    Log.debugGroup(
      "RENDER databrand => Size=[" + this.state.datavehiclebrand.length + "]",
      this.state.datavehiclebrand
    );
    Log.debugGroup(
      "RENDER datatype => Size=[" + this.state.datavehicletype.length + "]",
      this.state.datavehicletype
    );
    Log.debugGroup(
      "RENDER data basic cover => Size=[" +
        this.state.datacoverbasiccover.length +
        "]",
      this.state.datacoverbasiccover
    );
    //console.log("Check Summary ==> ");

    let stylingreload = {
      padding: "10px",
      overscrollBehavior: "none"
    };

    return (
      <div>
        <Header titles="Premium Simulation">
          {this.state.activetab == "Summary Details" && (
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a
                  onClick={() => {
                    this.onClickButtonSave();
                  }}
                  className="fa fa-save fa-lg"
                />
              </li>
            </ul>
          )}
        </Header>
        <ToastContainer />
        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfo}
          showModalInfo={this.state.showModalInfo}
        />
        <div className="content-wrapper" style={stylingreload}>
          <OtosalesModalAccessories
            showModal={this.state.showModalAccessories}
            onCloseModal={this.onCloseModalAccessories}
            dataAccessories={this.state.dataAccessories}
          />
          {/* <ModalDialogPeriod /> */}
          <OtosalesModalPremiumSimulation
            state={this.state}
            showModal={this.state.showModal}
            closeModal={this.closeModal}
            onSaveSimulation={this.onSaveSimulation}
            onSendEmail={this.onSendEmail}
            onSendSMS={this.onSendSMS}
          />
          <OtosalesModalSendEmail
            sendQuotationEmail={this.sendQuotationEmail}
            onCloseModalSendEmail={this.onCloseModalSendEmail}
            state={this.state}
          />
          <OtosalesModalSendSMS
            sendQuotationSMS={this.sendQuotationSMS}
            onCloseModalSendSMS={this.onCloseModalSendSMS}
            state={this.state}
          />
          <OtosalesModalSetPeriodPremi2
            state={this.state}
            rateCalculationNonBasic={() => this.premiumCalculation()}
            onChangeFunctionDate={this.onChangeFunctionDate}
            onChangeFunctionChecked={this.onChangeFunctionChecked}
            onChangeFunction={this.onChangeFunction}
            onOptionSelect2Change={this.onOptionSelect2Change}
            onChangeFunctionReactNumber={this.onChangeFunctionReactNumber}
            getTPLSI={this.getTPLSI}
            onClose={() => {
              this.setState({
                showdialogperiod: false
              });
            }}
          />
          <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            isLoading={this.state.isLoading}
          >
            <div className={"panel-body panel-body-list"}>
              <TypeProspect
                state={this.state}
                functions={this.functionTypeProspect()}
              />
            </div>
            <div className="panel-body panel-body-list" id="Datalist">
              <Tabs
                activetab={this.state.activetab}
                onClickTabs={this.onClickTabs}
              >
                <div
                  label={tabstitle[0]}
                  disabled={this.state.tabsDisabledState[0]}
                >
                  <PremiCalculation
                    state={this.state}
                    functions={this.functionPremiCalculation()}
                  />
                </div>
                <div
                  label={tabstitle[1]}
                  disabled={this.state.tabsDisabledState[1]}
                >
                  <PremiEstimation
                    state={this.state}
                    functions={this.functionPremiEstimation()}
                  />
                </div>
                <div
                  label={tabstitle[2]}
                  disabled={this.state.tabsDisabledState[2]}
                >
                  <SummaryDetails
                    state={this.state}
                    functions={this.functionSummaryDetails()}
                  />
                </div>
              </Tabs>
            </div>
          </OtosalesLoading>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PagePremiumSimulation);
