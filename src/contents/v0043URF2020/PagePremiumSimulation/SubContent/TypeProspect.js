import React from 'react'

export default function TypeProspect(props) {
    return (
        <React.Fragment>
            <div
                className={
                  "row" + (props.state.isErrIsMvGodig ? " has-error" : "")
                }
              >
                <div className="col-xs-12 col-md-3">
                  <div>
                    <label>
                      <input
                        type="checkbox"
                        disabled={props.functions.isDisableMVGodig()}
                        style={{ marginRight: "10px" }}
                        onChange={props.functions.onChangeFunction}
                        checked={props.state.isMvGodig}
                        name="isMvGodig"
                      />
                      MV GO DIGITAL
                    </label>
                  </div>
                </div>
                <div className="col-xs-12 col-md-3">
                  <div>
                    <label>
                      <input
                        type="checkbox"
                        disabled={props.functions.isDisableMVGodig()}
                        style={{ marginRight: "10px" }}
                        onChange={props.functions.onChangeFunction}
                        checked={props.state.isNonMvGodig}
                        name="isNonMvGodig"
                      />
                      MV NON GO DIGITAL
                    </label>
                  </div>
                </div>
              </div>
              <div className="mask" />
        </React.Fragment>
    )
}
