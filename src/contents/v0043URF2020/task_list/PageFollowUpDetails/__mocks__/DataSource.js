import { Util, Log } from "../../../../../otosalescomponents";
import {
  API_URL,
  API_VERSION,
  API_VERSION_2,
  HEADER_API,
  ACCOUNTDATA,
  AVAYAPIN,
  API_VERSION_0213URF2019,
  API_VERSION_0219URF2019,
  API_VERSION_0008URF2020,
  API_VERSION_0001URF2020
} from "../../../../../config";
import { resolve } from "path";

export const CheckPremiItems = async (OrderNo) => {
  return new Promise((resolve, reject) => {
    Util.isNullOrEmpty(OrderNo) ?
    reject({
      status: true,
      IsPremiItemExist: true
    }) :
    resolve({
      status: false
    })
  })
};
