import React from "react";
// import { render, fireEvent, waitForElement } from "@testing-library/react";
import * as DataSource from "./DataSource";
jest.mock("./DataSource");
import { shallowToJson } from "enzyme-to-json";
import PageFollowUpDetails from "./index";
import { Util, Log } from "../../../../otosalescomponents";
import { MemoryRouter } from "react-router-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
jest.mock("./")


let account;

beforeAll(() => {
  account = `{"UserInfo":{"User":{"SalesOfficerID":"nip","Email":null,"Name":"Nurhabibah Putri NIP.","Role":"TELERENEWAL","Class":"","Password":null,"Validation":"","Expiry":"9999-12-31 00:00:00.000","Password_Expiry":"9999-12-31 00:00:00.000","Department":null,"BranchCode":"022","Phone1":"","Phone2":null,"Phone3":null,"OfficePhone":null,"Ext":null,"Fax":null,"ExtUserID":null,"Channel":null,"ChannelSource":null,"IsCSO":0,"EntryDate":"2018-03-15 08:58:00.743","LastUpdatedTime":"2018-03-15 08:58:00.743","RowStatus":1,"Key":{}},"Region":null,"ReportTo":null,"FormAccessCollections":[{"FormID":"TASKLIST","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"INPUTPROSPECT","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"ORDERSIMULATION","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"DASHBOARD","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"INFO","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1}]},"IsAuthenticated":1,"Status":"OK","Token":"T2eWzmv79kxs5A6R4dYjm+0T+DdOpVzmca7dT0kNCqGO+oBpKp/A1srIZTnIlXdneKILJauiaqXbA38Z0P80acAx1wn9p54QCwTU7yJ5xNQ7KYnFKpam3Tc/eRJ9pijJEbz1Kj3dxX4ayQIiGZdj4Iz5GzlgN6N9XaMTKlAErlRLi4y29wgxE46NOfbLB7G+fo/U9KcJsIfMR08e/2W3uBzqw09rYjN+6M/yMP6oUt7qp/6G2PTyT5WmNrj6PSJy","ValidHour":24}`;
  localStorage.setItem("account", account);
});

describe("function PageFollowUpDetails", () => {
  it("set localstorage", () => {
    expect(localStorage.setItem).toHaveBeenLastCalledWith("account", account);
  });

  it("check account user info", () => {
    expect(Util.isNullOrEmpty(localStorage.getItem("account"))).toBe(false);
  });

  it("render page", () => {
    const wrapper = shallow(<PageFollowUpDetails />);
    // expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot();

  });
});
