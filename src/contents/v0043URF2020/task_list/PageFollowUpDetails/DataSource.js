import { Util, Log } from "../../../../otosalescomponents";
import {
  API_URL,
  API_VERSION,
  API_VERSION_2,
  HEADER_API,
  ACCOUNTDATA,
  AVAYAPIN,
  API_VERSION_0213URF2019,
  API_VERSION_0219URF2019,
  API_VERSION_0008URF2020,
  API_VERSION_0001URF2020
} from "../../../../config";

export const CheckPremiItems = async (OrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/CheckPremiItems/`,
    "POST",
    HEADER_API,
    {
      OrderNo: OrderNo
    }
  )
  .then(res => res.json());
};
