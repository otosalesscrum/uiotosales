import {
  API_URL,
  API_VERSION_2,
  HEADER_API,
  ACCOUNTDATA,
  API_RETRY_LIMIT,
  API_V0213URF2019,
  API_VERSION_0213URF2019,
  API_VERSION_0232URF2019,
  URL_GEN5,
  API_VERSION_0219URF2019,
  API_VERSION_0008URF2020,
  API_VERSION_0001URF2020,
  API_VERSION_0063URF2020,
  API_VERSION_FAYTEST,
  API_VERSION_0090URF2020
} from "../../../../config";
import {
  Util
} from "../../../../otosalescomponents";


export const hitButtonCallMVGodig = async (GuidPenawaran) => {
    return Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          API_VERSION_0219URF2019}/DataReact/hitButtonCallMVGodig/`,
        "POST",
        HEADER_API,
        {
          GuidPenawaran: GuidPenawaran
        }
      )
}

export const checkDoubleInsured = async(ChasisNo, EngineNo, OrderNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/CheckDoubleInsured`,
        "POST",
        HEADER_API,
        {
          ChasisNo: ChasisNo,
          EngineNo: EngineNo,
          OrderNo: OrderNo
        }
      )
}

export const checkSurveyResult = async (OrderNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/CheckSurveyResult`,
        "POST",
        HEADER_API,
        {
          OrderNo
        }
      )
}

export const onUpdateOrder = async (OrderNo, SalesOfficerID, DeviceID ) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/UpdateOrder`,
        "POST",
        HEADER_API,
        {
          OrderNo,
          SalesOfficerID,
          DeviceID
        }
      )
}

export const getFollowUpStatus = async (followupstatus, followupstatusinfo, followupno, isRenewal) => {
    return Util.fetchAPIAdditional(
        // `${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpStatus`,
        `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/getFollowUpStatus`,
        "POST",
        HEADER_API,
        {
          followupstatus,
          followupstatusinfo,
          followupno,
          isRenewal
        }
      )
}

export const getTaskDetail = async(CustID, FollowUpNo) => {
    return Util.fetchAPIAdditional(
        // `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/getTaskDetail`,
        // `${API_URL + "" + API_VERSION_0008URF2020}/DataReact/getTaskDetail`,
        // `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/getTaskDetail`,
        `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getTaskDetail`,
        // `${API_URL + "" + API_VERSION_FAYTEST}/DataReact/getTaskDetail`,
        "POST",
        HEADER_API,
        {
          CustID: CustID,
          FollowUpNo: FollowUpNo
        }
      )
}

export const getTaskDetailDocument = async (CustID, FollowUpNo, Type) => {
    return Util.fetchAPIAdditional(
        // `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/getTaskDetail`,
        // `${API_URL + "" + API_VERSION_0008URF2020}/DataReact/getTaskDetail`,
        // `${API_URL + "" + API_VERSION_FAYTEST}/DataReact/getTaskDetailDocument`,
        // `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/getTaskDetailDocument`,
        `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getTaskDetailDocument`,
        "POST",
        HEADER_API,
        {
          CustID,
          FollowUpNo,
          Type
        }
      )
}

export const getPostalCode = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetPostalCode`,
        "POST",
        HEADER_API,
        {
          search: ""
        }
      )
}

export const getSoIdQuotationLead = async (ReferenceNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          API_VERSION_0219URF2019}/DataReact/GetSoIdQuotationLead`,
        "POST",
        HEADER_API,
        {
          ReferenceNo
        }
      )
}

export const getPostalCodeSurvey = async(CityID) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetSurveyZipCode`,
        "POST",
        HEADER_API,
        {
          CityID
        }
      )
}

export const getSurveyLocation = async (idlocation) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyLocation`,
        "POST",
        HEADER_API,
        {
          pageType: "branch",
          type: 0,
          location: idlocation
        }
      )
}

export const getSlotTimeBooking = async (BranchID, ZipCode, AvailableDate, CityId) =>{
    return Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          API_VERSION_0219URF2019}/DataReact/GetSlotTimeBooking`,
        // API_VERSION_0001URF2020}/DataReact/GetSlotTimeBooking`,
        "POST",
        HEADER_API,
        {
          BranchID,
          ZipCode,
          AvailableDate,
          CityId
        }
      )
}


export const getSurveyScheduleTime = async (BranchID, SurveyDate, PostalCodeID, Language) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetScheduleTime`,
        "POST",
        HEADER_API,
        {
          BranchID,
          SurveyDate,
          PostalCodeID,
          Language
        }
      )
}

export const getSurveyCity = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyCity`,
        "POST",
        HEADER_API,
        {
          pageType: "Branch",
          filterType: 0
        }
      )
}

export const getSurveyDays = async (CityID) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyDays`,
        "POST",
        HEADER_API,
        {
          CityID /// BranchID
        }
      )
}

export const getBasicCover = async (Channel, vYear, isBasic) => {
    return Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_2}/DataReact/getBasicCover`,
      `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getBasicCover`,
        "POST",
        HEADER_API,
        {
          Channel,
          vYear,
          isBasic
        }
      )
}

export const getListUsage = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleUsage/`,
        "POST",
        HEADER_API
      )
}

export const getListProductType = async (salesofficerid) => {
    return Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_2}/DataReact/getProductType/`,
      `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getProductType/`,
        "POST",
        HEADER_API,
        {
          salesofficerid
        }
      )
}

export const getListProductCode = async (producttypecode, insurancetype, isVehicleNew, salesofficerid, isRenewal) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getProductCode/`,
        "POST",
        HEADER_API,
        {
          producttypecode,
          insurancetype,
          isVehicleNew,
          salesofficerid,
          isRenewal
        }
      )
}

export const getSegmentCode = async (ProductCode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetSegmentCode`,
        "POST",
        HEADER_API,
        {
          ProductCode
        }
      )
}

export const getEnableDisableSalesmanDealer = async (ProductCode, BranchCode) => {
    return Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          // API_VERSION_2}/DataReact/GetEnableDisableSalesmanDealer`,
          API_VERSION_0063URF2020}/DataReact/GetEnableDisableSalesmanDealer`,
        "POST",
        HEADER_API,
        {
          ProductCode,
          BranchCode
        }
      )
}

export const getShowIntermediaryConfirmationCustomer = async (ProductCode, SalesOfficerID) => {
    return Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          API_VERSION_0001URF2020}/DataReact/GetShowIntermediaryConfirmationCustomer`,
  
        "POST",
        HEADER_API,
        {
          ProductCode,
          SalesOfficerID
        }
      )
}

export const getRetrieveextsi = async (InterestId) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/retrieveextsi/`,
        "POST",
        HEADER_API,
        {
          InterestId
        }
      )
}

export const getVehicle = async (IsRenewal, search, signal) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetVehicle`,
        "POST",
        HEADER_API,
        {
          moduleName: "QUOTATION",
          IsRenewal,
          search
        },
        {
          signal
        }
      )
}


export const getUpliner = async (SalesOfficerId) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getUpliner/`,
      "POST",
      HEADER_API,
      {
        SalesOfficerId
      }
    )
}

export const getListRegion = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleRegion/`,
        "POST",
        HEADER_API
      )
}

export const getListDealer = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getDealerName/`,
        "POST",
        HEADER_API
      )
}

export const getDealerInformation = async (SalesmanCode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetBankInformation/`,
        "POST",
        HEADER_API,
        {
          SalesmanCode
        }
      )
}

export const getListBank = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetBank/`,
        "POST",
        HEADER_API,
        {
          ParamSearch: ""
        }
      )
}

export const getSalesmanDealer = async (dealercode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getSalesmanDealerName/`,
        "POST",
        HEADER_API,
        {
          dealercode
        }
      )
}

export const getPolicySentTo = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetPolicySentTo`,
        "POST",
        HEADER_API
      )
}

export const getNameOnPolicyDelivery = async (PolicySentToID, ParamSearch) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetPolicyDeliveryName/`,
        "POST",
        HEADER_API,
        {
          PolicySentToID,
          ParamSearch
        }
      )
}

export const uploadImage = async (ext, ImageType, FollowUpNo, DeviceID, SalesOfficerID, Data, ThumbnailData) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/UploadImage/`,
        "POST",
        HEADER_API,
        {
          ext,
          ImageType,
          FollowUpNo,
          DeviceID,
          SalesOfficerID,
          Data,
          ThumbnailData
        }
      )
}

export const checkNSAapproval = async (OrderNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/CheckNSA/`,
        "POST",
        {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        },
        {
          OrderNo
        }
      )
}

export const loadPremiumCalculation = async (paramReq = {}) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/PremiumCalculation/`,
      "POST",
      HEADER_API,
      paramReq
    )
}

export const basicPremiCalculation = async (paramReq, signal) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/BasicPremiCalculation/`,
      "POST",
      HEADER_API,
      paramReq,
      {
        signal
      }
    )
}

export const ShowAccessoryInfo = async () => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getAccessoriesList`,
      "POST",
      HEADER_API
    )
}

export const sendQuotationEmail = async (Email, OrderNo) => {
    return Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationEmail/`,
      `${API_URL +
        "" +
        API_VERSION_0001URF2020}/DataReact/SendQuotationEmail/`,
      "POST",
      HEADER_API,
      {
        Email,
        OrderNo
      }
    )
}

export const sendQuotationSMS = async (Phone, OrderNo) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationSMS/`,
      "POST",
      HEADER_API,
      {
        Phone,
        OrderNo
      }
    )
}

export const getSendEmailTemplate = async (OrderNo) => {
    return Util.fetchAPIAdditional(
      `${API_URL +
        "" +
        API_VERSION_0219URF2019}/DataReact/GetSendEmailTemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
}

export const getSendEmailTemplateWA = async (orderNo) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSendWATemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
}

export const getSendEmailTemplateSMSTelerenewal = async (orderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/GetSendSMSTemplate/`,
    "POST",
    HEADER_API,
    {
      OrderNo: orderNo
    }
  )
}

export const SendViaEmail = async (paramReq) => {
  return Util.fetchAPIAdditional(
    // `${API_URL + "" + API_VERSION_2}/DataReact/SendViaEmail/`,
    `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/SendViaEmail/`,
    "POST",
    { ...HEADER_API, "Content-Type": "application/json" },
    paramReq
  )
}

export const SendViaWA = async (paramReq) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SendViaWA/`,
    "POST",
    { ...HEADER_API, "Content-Type": "application/json" },
    paramReq
  )
}

export const SendViaSMS = async (PhoneNo, SMSText) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/SendViaSMS/`,
    "POST",
    { ...HEADER_API, "Content-Type": "application/json" },
    {
      PhoneNo,
      SMSText
    }
  )
}

export const getTemplateCustomerConfirmation = async (OrderNo, Type, Time, Date) => {
  return Util.fetchAPIAdditional(
    `${API_URL +
      "" +
      API_VERSION_0001URF2020}/DataReact/GetTemplateCustomerConfirmation/`,
    "POST",
    HEADER_API,
    {
      OrderNo,
      Type, 
      Time,
      Date
    }
  )
}

export const getPhoneList = async (OrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetPhoneList/`,
    "POST",
    HEADER_API,
    {
      OrderNo
    }
  )
}

export const getTPLSIList = async (ProductCode) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/getTPLSIList/`,
    "POST",
    HEADER_API,
    {
      ProductCode
    }
  )
}

export const getvehicleprice = async (vehiclecode, Year, citycode) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/getvehicleprice/`,
    "POST",
    HEADER_API,
    {
      vehiclecode,
      Year,
      citycode
    }
  )
}

export const DownloadQuotation = async (OrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/DownloadQuotation/`,
    "POST",
    HEADER_API,
    {
      OrderNo
    }
  )
}

export const getQuotationHistory = async (FollowUpNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`,
    "POST",
    HEADER_API,
    {
      followupno: FollowUpNo
    }
  )
}

export const GetOrderNoQuotation = async (ReferenceNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetOrderNoQuotation`,
    "POST",
    HEADER_API,
    {
      ReferenceNo
    }
  )
}

export const UpdateTaskDetail = async (paramReq = {}, signal) => {
  return Util.fetchAPIAdditional(
    // `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/UpdateTaskDetail/`,
    // `${API_URL + "" + API_VERSION_0001URF2020}/DataReact/UpdateTaskDetail/`,
    `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/UpdateTaskDetail/`,
    // `${API_URL + "" + API_VERSION_FAYTEST}/DataReact/UpdateTaskDetail/`,
    "POST",
    { ...HEADER_API, "Content-Type": "application/json" },
    paramReq,
    {
      signal
    }
  )
}

export const GetPaymentInfo = async (paramReq) => {
  return Util.fetchAPIAdditional(
    // `${API_URL + API_VERSION_0008URF2020}/DataReact/GetPaymentInfo`,
    `${API_URL + API_VERSION_0001URF2020}/DataReact/GetPaymentInfo`,
    "POST",
    HEADER_API,
    paramReq
  )
}

export const GenerateShortenLinkPaymentOtosales = async (OrderNo, IsInstallment) => {
  return Util.fetchAPIAdditional(
    `${API_URL + API_VERSION_0090URF2020}/DataReact/GenerateShortenLinkPaymentOtosales`,
    "POST",
    HEADER_API,
    {
      OrderNo,
      IsInstallment
    }
  )
}

export const GetGenerateLinkPaymentStatus = async (OrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + API_VERSION_0090URF2020}/DataReact/GetApprovalPaymentStatus`,
    "POST",
    HEADER_API,
    {
      OrderNo
    }
  )
}

export const CheckVAPayment = async (OrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + API_VERSION_0232URF2019}/DataReact/CheckVAPayment`,
    "POST",
    HEADER_API,
    {
      OrderNo
    }
  )
}

export const ReactiveVA = async (VANumber, PolicyOrderNo) => {
  return Util.fetchAPIAdditional(
    `${API_URL + API_VERSION_0232URF2019}/DataReact/ReactiveVA`,
    "POST",
    HEADER_API,
    {
      VANumber,
      PolicyOrderNo
    }
  )
}

export const editQuotation = async (followupno, orderNo, custid) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/editQuotation/`,
    "POST",
    HEADER_API,
    {
      followupno,
      orderNo,
      custid
    }
  )
}

export const addCopyQuotation = async (followUpNo, orderNo, state) => {
  return Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_2}/DataReact/addCopyQuotation/`,
    "POST",
    HEADER_API,
    {
      followUpNo,
      orderNo,
      state
    }
  )
}

export const GetMappingVehiclePlateGeoArea = async (plateRegion, signal) => {
  return Util.fetchAPIAdditional(
    `${API_URL +
      "" +
      API_VERSION_2}/DataReact/GetMappingVehiclePlateGeoArea/`,
    "POST",
    HEADER_API,
    {
      VehiclePlateCode: plateRegion
    },
    {
      signal
    }
  )
}
