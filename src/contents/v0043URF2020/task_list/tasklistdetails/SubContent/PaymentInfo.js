import React, { Component } from "react";
import {
  OtosalesDropzonev2,
  Util,
  OtosalesSegmentDropdown,
  OtosalesSelectFullviewrev,
  OtosalesRadioButton,
} from "../../../../../otosalescomponents";

export default class PaymentInfo extends Component {
  constructor(props) {
    super(props);
  }

  isNotHideButtonGenerateLinkPayment = () => {
    let res = false;
    if (
      Util.stringArrayElementEquals(this.props.state.StatusLinkPayment, [
        "1",
        "3",
        "4"
      ])
    ) {
      res = true;
    }

    return res;
  };

  isNotHideFieldTextShortenURL = () => {
    let res = false;

    if (
      Util.stringArrayElementEquals(this.props.state.StatusLinkPayment, [
        "1",
        "3",
        "4",
        "5"
      ])
    ) {
      res = true;
    }

    return res;
  };

  render() {
    return (
      <div>
        <OtosalesSegmentDropdown title="PAYMENT INFO" divid="paymentinfo">
          <div className="row">
            <span className="col-xs-4 col-md-3 labelspanbold">
              Due Date Pembayaran
            </span>
            <div className="col-xs-8 col-md-9">
              <input
                type="text"
                name="paymentduedate"
                value={this.props.state.paymentduedate || ""}
                // onChange={this.props.functions.onChangeFunction}
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
          {Util.isNullOrEmpty(this.props.state.paymentnorek) && (
            <React.Fragment>
              <div className="row">
                <span className="col-xs-4 col-md-3 labelspanbold">
                  VA Permata
                </span>
                <div className="col-xs-8 col-md-9">
                  <input
                    type="text"
                    name="paymentvapermata"
                    value={this.props.state.paymentvapermata}
                    onChange={() => {}}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-4 col-md-3 labelspanbold">
                  VA Mandiri
                </span>
                <div className="col-xs-8 col-md-9">
                  <input
                    type="text"
                    name="paymentvamandiri"
                    value={this.props.state.paymentvamandiri}
                    onChange={() => {}}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-4 col-md-3 labelspanbold">VA BCA</span>
                <div className="col-xs-8 col-md-9">
                  <input
                    type="text"
                    name="paymentvabca"
                    value={this.props.state.paymentvabca}
                    onChange={() => {}}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
          {!Util.isNullOrEmpty(this.props.state.paymentnorek) && (
            <div className="row">
              <span className="col-xs-4 col-md-3 labelspanbold">
                Nomor Rekening
              </span>
              <div className="col-xs-8 col-md-9">
                <input
                  type="text"
                  name="paymentnorek"
                  value={this.props.state.paymentnorek}
                  onChange={() => {}}
                  readOnly={true}
                  className="form-control"
                />
              </div>
            </div>
          )}
          {/* {this.props.state.paymentisvaactive == false && (
                  <div className="row" style={{ marginBottom: "5px" }}>
                    <div className="col-xs-12">
                      <div className="pull-right">
                        <a
                          className="btn btn-success"
                          onClick={() => this.props.functions.onClickReactivedVA()}
                        >
                          Reactive VA
                        </a>
                      </div>
                    </div>
                  </div>
                )} */}
          {(this.props.state.IsReGenerateVA || this.props.state.IsGenerateVA) &&
            Util.isNullOrEmpty(this.props.state.PolicyOrderNo) && (
              <div className="row" style={{ marginBottom: "5px" }}>
                <div className="col-xs-12">
                  <div className="pull-right">
                    <a
                      className="btn btn-success"
                      onClick={() =>
                        this.props.functions.onClickGenerateRegenerateVA()
                      }
                    >
                      {this.props.state.IsGenerateVA
                        ? "Generate VA"
                        : this.props.state.IsReGenerateVA
                        ? "Regenerate VA"
                        : "Not Defined"}
                    </a>
                  </div>
                </div>
              </div>
            )}
          <div className="row">
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.props.state.dataBUKTIBAYAR}
                  name="BUKTIBAYAR"
                  handleDeleteImage={this.props.functions.handleDeleteImage}
                  handleUploadImage={this.props.functions.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.props.state.dataBUKTIBAYAR2}
                  name="BUKTIBAYAR2"
                  handleDeleteImage={this.props.functions.handleDeleteImage}
                  handleUploadImage={this.props.functions.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.props.state.dataBUKTIBAYAR3}
                  name="BUKTIBAYAR3"
                  handleDeleteImage={this.props.functions.handleDeleteImage}
                  handleUploadImage={this.props.functions.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.props.state.dataBUKTIBAYAR4}
                  name="BUKTIBAYAR4"
                  handleDeleteImage={this.props.functions.handleDeleteImage}
                  handleUploadImage={this.props.functions.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.props.state.dataBUKTIBAYAR5}
                  name="BUKTIBAYAR5"
                  handleDeleteImage={this.props.functions.handleDeleteImage}
                  handleUploadImage={this.props.functions.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
          </div>
          {this.isNotHideButtonGenerateLinkPayment() && (
            <React.Fragment>
              {/* <div className="row">
                <span className="col-xs-4 col-md-3 labelspanbold">
                  Metode Pembayaran
                </span>
                <div className="col-xs-8 col-md-9">
                  <OtosalesSelectFullviewrev
                    name="paymentInfoTypeCC"
                    // isDisabled={this.props.functions.disabledPersonalDataJenisKelamin()}
                    value={this.props.state.paymentInfoTypeCC}
                    selectOption={this.props.state.paymentInfoTypeCC}
                    onOptionsChange={this.props.functions.onOptionSelect2Change}
                    options={this.props.state.dataPaymentInfoTypeCC}
                    placeholder="Pilih metode pembayaran"
                  />
                </div>
              </div> */}
              <div className="row" style={{marginTop:20}}>
                <span className="col-xs-4 col-md-3 labelspanbold">
                  Metode Pembayaran
                </span>
                <div className="col-xs-8 col-md-9">
                  <OtosalesRadioButton
                    name="paymentInfoTypeCC"
                    // isDisabled={this.props.functions.disabledPersonalDataJenisKelamin()}
                    value={this.props.state.paymentInfoTypeCC}
                    selectOption={this.props.state.paymentInfoTypeCC}
                    onOptionsChange={this.props.functions.onOptionSelect2Change}
                    options={this.props.state.dataPaymentInfoTypeCC}
                  />
                </div>
              </div>
            </React.Fragment>
          )}
          {this.isNotHideFieldTextShortenURL() && (
            <React.Fragment>
              <div className="row">
                <div className="col-xs-12 col-md-3 labelspanbold">
                  Link Payment
                </div>
                <div className="col-xs-12 col-md-9">
                  <textarea
                    className="form-control"
                    rows="4"
                    style={{ resize: "none" }}
                    onChange={() => {}}
                    name="LinkPayment"
                    value={this.props.state.LinkPayment}
                  ></textarea>
                </div>
              </div>
            </React.Fragment>
          )}
          {this.isNotHideButtonGenerateLinkPayment() && (
            <React.Fragment>
              <div className="row" style={{ marginBottom: "5px" }}>
                <div className="col-xs-12">
                  <div className="pull-right">
                    <a
                      className="btn btn-success"
                      onClick={() =>
                        this.props.functions.GenerateShortenLinkPaymentOtosales()
                      }
                    >
                      Generate Link Payment
                    </a>
                  </div>
                </div>
              </div>
            </React.Fragment>
          )}
        </OtosalesSegmentDropdown>
      </div>
    );
  }
}
