import React, { Component } from 'react';
import { Util, OtosalesSegmentDropdown } from "../../../../../otosalescomponents";
import NumberFormat from "react-number-format";

export default class ExtendedCover extends Component {
    render() {
        return (
            <div className={this.props.functions.disabledExtendedCover() ? "disablediv" : ""}>
              <OtosalesSegmentDropdown title="EXTENDED COVER" divid="extendedcover">
                <div>
                  {this.props.functions.detectComprePeriod() && this.props.state.IsTPLEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsTPLChecked}
                              name="IsTPLChecked"
                              value={this.props.state.IsTPLChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(4);
                              }}
                            />
                            TJH
                          </label>
                        </div>
                        {/* <OtosalesSelectFullviewv2
                          // isDisabled={this.props.state.IsTPLSIEnabled == 1 ? false : true}
                          isDisabled={true}
                          name="TPLCoverageId"
                          value={this.props.state.TPLCoverageId}
                          selectOption={this.props.state.TPLCoverageId}
                          onOptionsChange={this.props.functions.onOptionSelect2Change}
                          options={this.props.state.dataTPLSI}
                          placeholder=""
                        /> */}
                        <NumberFormat
                          allowNegative={false}
                          disabled={true}
                          className="form-control"
                          value={this.props.state.TPLSICOVER}
                          thousandSeparator={true}
                          prefix={""}
                        />
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {Util.formatMoney(this.props.state.TPLPremi, 0)}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsSRCCEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsSRCCChecked}
                              name="IsSRCCChecked"
                              value={this.props.state.IsSRCCChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(0);
                              }}
                              disabled={
                                this.props.state.IsSRCCCheckedEnable == 1 ? false : true
                              }
                            />
                            STRIKE, RIOT, COMMOTION (SRCC)
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {Util.formatMoney(this.props.state.SRCCPremi, 0)}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsFLDEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsFLDChecked}
                              name="IsFLDChecked"
                              value={this.props.state.IsFLDChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(1);
                              }}
                              disabled={
                                this.props.state.IsFLDCheckedEnable == 1 ? false : true
                              }
                            />
                            FLOOD &amp; WINDSTROM
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {" "}
                          {Util.formatMoney(this.props.state.FLDPremi, 0)}{" "}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsETVEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsETVChecked}
                              name="IsETVChecked"
                              value={this.props.state.IsETVChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(2);
                              }}
                              disabled={
                                this.props.state.IsETVCheckedEnable == 1 ? false : true
                              }
                            />
                            EARTHQUAKE, TSUNAMI, VOLCANO ERUPTION
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {Util.formatMoney(this.props.state.ETVPremi, 0)}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsTSEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              name="IsTSChecked"
                              checked={this.props.state.IsTSChecked}
                              value={this.props.state.IsTSChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(3);
                              }}
                              disabled={
                                this.props.state.IsTRSCheckedEnable == 1 ? false : true
                              }
                            />
                            TERRORISM &amp; SABOTAGE
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {" "}
                          {Util.formatMoney(this.props.state.TSPremi, 0)}{" "}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsPADRVREnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsPADRVRChecked}
                              name="IsPADRVRChecked"
                              value={this.props.state.IsPADRVRChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(5);
                              }}
                              disabled={
                                this.props.state.IsPADRIVERCheckedEnable == 1 ? false : true
                              }
                            />
                            PA DRIVER
                          </label>
                        </div>
                        <NumberFormat
                          allowNegative={false}
                          disabled={true}
                          className="form-control"
                          // onValueChange={value => {
                          //   this.props.functions.setState(
                          //     { PADRVCOVER: value.floatValue },
                          //     () => {
                          //       this.props.functions.rateCalculateTPLPAPASSPADRV(
                          //         "PADRVCOVER"
                          //       );
                          //     }
                          //   );
                          // }}
                          value={this.props.state.PADRVCOVER}
                          thousandSeparator={true}
                          prefix={""}
                        />
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {" "}
                          {Util.formatMoney(this.props.state.PADRVRPremi, 0)}{" "}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsPAPASSEnabled == 0 ? (
                    ""
                  ) : (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsPAPASSChecked}
                              name="IsPAPASSChecked"
                              value={this.props.state.IsPAPASSChecked}
                              onChange={event => {
                                this.props.functions.setDialogPeriod(6);
                              }}
                              disabled={
                                this.props.state.IsPAPASSCheckedEnable == 1 ? false : true
                              }
                            />
                            PA PASSENGER
                          </label>
                        </div>
                        <div className="row">
                          <div className="col-md-3 col-xs-6 panel-body-list">
                            <div className="col-xs-8" style={{ paddingRight: "0px" }}>
                              <input
                                className="form-control"
                                type="number"
                                name="PASSCOVER"
                                value={this.props.state.PASSCOVER}
                                // onChange={event => {
                                //   this.props.functions.onChangeFunction(event);
                                // }}
                                readOnly={true}
                              />
                            </div>
                            <div className="col-xs-4">@</div>
                          </div>
                          <div
                            className="col-md-9 col-xs-6 panel-body-list"
                            style={{
                              marginLeft: "-5px",
                              paddingRight: "10px"
                            }}
                          >
                            <NumberFormat
                              disabled={true}
                              className="form-control"
                              // onValueChange={value => {
                              //   this.props.functions.setState(
                              //     { PAPASSICOVER: value.floatValue },
                              //     () => {
                              //       this.props.functions.rateCalculateTPLPAPASSPADRV(
                              //         "PAPASSICOVER"
                              //       );
                              //     }
                              //   );
                              // }}
                              value={this.props.state.PAPASSICOVER}
                              thousandSeparator={true}
                              prefix={""}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {Util.formatMoney(this.props.state.PAPASSPremi, 0)}
                        </p>
                      </div>
                    </div>
                  )}
                  {this.props.state.IsACCESSEnabled == 1 && (
                    <div className="row">
                      <div className="col-xs-8 col-md-10">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={this.props.state.IsACCESSChecked}
                              name="IsACCESSChecked"
                              onChange={event => {
                                this.props.functions.setDialogPeriod(7);
                              }}
                            />
                            ACCESSORY
                          </label>
                          <i
                            className="fa fa-info-circle fa-2x"
                            style={{
                              marginLeft: "5px",
                              opacity: "0.3"
                            }}
                            onClick={() => {
                              this.props.functions.ShowAccessoryInfo();
                            }}
                          />
                        </div>
                        <NumberFormat
                          allowNegative={false}
                          disabled={true}
                          className="form-control"
                          // onValueChange={value => {
                          //   this.props.functions.setState(
                          //     { ACCESSCOVER: value.floatValue },
                          //     () => {
                          //       this.props.functions.rateCalculateTPLPAPASSPADRV(
                          //         "ACCESSCOVER"
                          //       );
                          //     }
                          //   );
                          // }}
                          value={this.props.state.ACCESSCOVER}
                          thousandSeparator={true}
                          prefix={""}
                        />
                      </div>
                      <div className="col-xs-4 col-md-2 text-right">
                        <p style={{ marginTop: "10px" }}>
                          {Util.formatMoney(this.props.state.ACCESSPremi, 0)}
                        </p>
                      </div>
                    </div>
                  )}
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
