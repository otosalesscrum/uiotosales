import React, { Component } from 'react'
import { Util, OtosalesSegmentDropdown } from "../../../../../otosalescomponents";

export default class TemplateCustomerConfirmation extends Component {
    render() {
        return (
            <div>
              <OtosalesSegmentDropdown title="TEMPLATE KONFIRMASI CUSTOMER" divid="templatecustomerconfirmation">
              <div className={"panel-body panel-body-list"}>
                  <div className="row">
                    <div className="col-xs-12 col-md-3">
                      <div>
                        <label className="smallerCheckbox">
                          <input
                            type="checkbox"
                            style={{ marginRight: "10px" }}
                            onChange={this.props.functions.onChangeFunction}
                            checked={this.props.state.istemplatecustomerconfirmationCust}
                            name="istemplatecustomerconfirmationCust"
                          />
                          CUSTOMER
                        </label>
                      </div>
                    </div>
                    {
                      this.props.state.IsShowIntermediaryConfirmationCustomer && 
                      <div className="col-xs-12 col-md-3">
                      <div>
                        <label className="smallerCheckbox">
                          <input
                            type="checkbox"
                            style={{ marginRight: "10px" }}
                            onChange={this.props.functions.onChangeFunction}
                            checked={
                              this.props.state.istemplatecustomerconfirmationIntermediary
                            }
                            name="istemplatecustomerconfirmationIntermediary"
                          />
                          INTERMEDIARY
                        </label>
                      </div>
                    </div>
                    }
                  </div>
                </div>
                <div className="panel-body panel-body-list">
                  <div className="col-xs-4 col-md-2 panel-body-list">
                    <a
                      className="btn btn-success"
                      onClick={() => {
                        this.props.functions.setState({
                          isLoading : true
                        });
                        this.props.functions.onDownloadTaskDetail(
                          () => {
                            this.props.functions.getTemplateCustomerConfirmation(this.props.state.istemplatecustomerconfirmationCust ? 1 : 0) 
                          }
                        )
                      }}
                      style={{width: "100px"}}
                    >
                      Generate
                    </a>
                  </div>
                  <div className="col-xs-4 col-md-2 panel-body-list">
                  <a
                      className="btn btn-success"
                      onClick={() => {
                        Util.copyToClipboard(this.props.state.ConfirmationCustomer);
                        Util.showToast("Customer confirmation  has been copied");
                      }}
                      style={{width: "100px"}}
                    >
                      Copy
                    </a>
                  </div>
                </div>
                <div className="panel-body panel-body-list">
                  <textarea
                    className="form-control"
                    rows="4"
                    style={{ resize: "none" }}
                    onChange={() => {}}
                    name="ConfirmationCustomer"
                    value={this.props.state.ConfirmationCustomer}
                  ></textarea>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
