import React, { Component } from 'react'
import NumberFormat from "react-number-format"
import {
    OtosalesDatePicker,
    OtosalesSelectFullviewv2,
    Util,
    OtosalesSegmentDropdown
  } from "../../../../../otosalescomponents"

export default class CompanyData extends Component {
    render() {
        return (
            <div className={this.props.functions.disabledCompanyData() ? "disablediv" : ""}>
              <OtosalesSegmentDropdown title="COMPANY DATA" divid="companydata">
                <div
                  className={
                    this.props.state.IsNotErrorcompanyname ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">
                    Nama Perusahaan di NPWP
                  </span>
                  <div className="col-xs-12">
                    <input
                      name="companyname"
                      pattern="^[a-zA-Z0-9 ,-.\/]+$"
                      maxLength={50}
                      disabled={this.props.functions.disabledCompanyDataName()}
                      value={this.props.state.companyname}
                      onChange={this.props.functions.onChangeFunction}
                      type="text"
                      className="form-control"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanynpwpnumber ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Nomor NPWP</span>
                  <div className="col-xs-12">
                    <NumberFormat
                      className="form-control"
                      onValueChange={value => {
                        this.props.functions.setState({
                          companynpwpnumber: value.value
                        });
                      }}
                      disabled={this.props.functions.disabledCompanyDataNpwpNo()}
                      value={this.props.state.companynpwpnumber}
                      format="##.###.###.#-###.### ###############"
                      prefix={""}
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanynpwpdata ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Tanggal NPWP</span>
                  <div className="col-xs-12">
                    <OtosalesDatePicker
                      className="form-control"
                      disabled={this.props.functions.disabledCompanyDataNpwpDate()}
                      name="companynpwpdata"
                      maxDate={Util.convertDate()}
                      selected={this.props.state.companynpwpdata}
                      dateFormat="dd mmm yyyy"
                      onChange={this.props.functions.onChangeFunctionDate}
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanynpwpaddress ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Alamat NPWP</span>
                  <div className="col-xs-12">
                    <input
                      type="text"
                      disabled={this.props.functions.disabledCompanyDataAlamat()}
                      maxLength="152"
                      value={this.props.state.companynpwpaddress}
                      pattern="^[a-zA-Z0-9 ,-.\/]+$"
                      name="companynpwpaddress"
                      onChange={this.props.functions.onChangeFunction}
                      className="form-control"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanypostalcode ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Kode Pos</span>
                  <div className="col-xs-12">
                    <OtosalesSelectFullviewv2
                      name="companypostalcode"
                      isDisabled={this.props.functions.disabledCompanyDataKodePos()}
                      value={this.props.state.companypostalcode}
                      selectOption={this.props.state.companypostalcode}
                      onOptionsChange={this.props.functions.onOptionSelect2Change}
                      options={this.props.state.dataKodePos}
                      placeholder="Pilih Kode Pos"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanypdcname ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Nama PIC</span>
                  <div className="col-xs-12">
                    <input
                      type="text"
                      pattern="^[a-zA-Z0-9 ,-.\/]+$"
                      maxLength={50}
                      disabled={this.props.functions.disabledCompanyDataPICName()}
                      value={this.props.state.companypdcname}
                      name="companypdcname"
                      onChange={this.props.functions.onChangeFunction}
                      className="form-control"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanypichp ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Nomor Handphone PIC</span>
                  <div className="col-xs-12">
                    {/* <NumberFormat
                      className="form-control"
                      onValueChange={value => {
                        this.props.functions.setState({
                          companypichp: value.value
                        });
                      }}
                      value={this.props.state.companypichp}
                      format="#### #### #### ####"
                      prefix={""}
                    /> */}
                    <input
                      name="companypichp"
                      maxLength="16"
                      disabled={this.props.functions.disabledCompanyDataPhoneNo()}
                      value={
                        this.props.functions.isNotMaskingPhoneOrEmail(this.props.state.isFocusPhoneCompany)
                          ? this.props.state.companypichp
                          : Util.maskingPhoneNumberv2(
                              Util.clearPhoneNo(this.props.state.companypichp)
                            )
                      }
                      onBlur={() => {
                        this.props.functions.setState({
                          isFocusPhoneCompany: false
                        });
                      }}
                      onFocus={() => {
                        this.props.functions.setState({
                          isFocusPhoneCompany: true
                        });
                      }}
                      onChange={this.props.functions.onChangeFunction}
                      type="text"
                      pattern="^[0-9]+$"
                      className="form-control"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorcompanyemail ? "row has-error" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Email PIC</span>
                  <div className="col-xs-12">
                    <input
                      type="email"
                      maxLength={50}
                      name="companyemail"
                      disabled={this.props.functions.disabledCompanyDataPICEmail()}
                      // value={this.props.state.companyemail}
                      value={
                        this.props.functions.isNotMaskingPhoneOrEmail(
                          this.props.state.isFocusEmailCompany
                        ) || this.props.functions.isRoleCTIandRenewal()
                          ? this.props.state.companyemail
                          : Util.maskingEmail(this.props.state.companyemail)
                      }
                      onBlur={() => {
                        this.props.functions.setState({
                          isFocusEmailCompany: false
                        });
                      }}
                      onFocus={() => {
                        this.props.functions.setState({
                          isFocusEmailCompany: true
                        });
                      }}
                      onChange={this.props.functions.onChangeFunction}
                      // pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
                      className="form-control"
                    />
                  </div>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
