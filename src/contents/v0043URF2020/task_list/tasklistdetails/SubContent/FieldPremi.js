import React, { Component } from 'react'
import { Util } from "../../../../../otosalescomponents"

export default class FieldPremi extends Component {
    render() {
        return (
            <React.Fragment>
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Gross Premium</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.props.state.GrossPremium, 0)}
          </span>
        </div>
        {this.props.state.IsRenewal && (
          <React.Fragment>
            {this.props.state.DiscountPremi > 0 ? (
              <div
                className="col-xs-12 backgroundgrey labelspanbold"
                style={{
                  padding: "0px",
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <span className="pull-left">Discount Premi</span>
                <span className="pull-right">
                  {"Rp." + Util.formatMoney(this.props.state.DiscountPremi, 0)}
                </span>
              </div>
            ) : (
              <div
                className="col-xs-12 backgroundgrey labelspanbold"
                style={{
                  padding: "0px",
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <span className="pull-left">No Claim Bonus</span>
                <span className="pull-right">
                  {"Rp." + Util.formatMoney(this.props.state.NoClaimBonus, 0)}
                </span>
              </div>
            )}
            {/* <div
              className="col-xs-12 backgroundgrey labelspanbold"
              style={{
                padding: "0px",
                paddingTop: "5px",
                paddingBottom: "5px"
              }}
            >
              <span className="pull-left">No Claim Bonus</span>
              <span className="pull-right">
                {"Rp." + Util.formatMoney(this.props.state.NoClaimBonus, 0)}
              </span>
            </div> */}
          </React.Fragment>
        )}
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Admin</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.props.state.Admin, 0)}
          </span>
        </div>
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Net Premi</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.props.state.NetPremi, 0)}
          </span>
        </div>
      </React.Fragment>
        )
    }
}
