import React, { Component } from "react";
import { ACCOUNTDATA } from "../../../../../config";
import {
  OtosalesDatePicker,
  OtosalesSelectFullviewrev,
  OtosalesSelectFullviewv2,
  OtosalesSegmentDropdown,
  Util
} from "../../../../../otosalescomponents";
import NumberFormat from "react-number-format";

export default class VehicleData extends Component {

  isDisabledUsedCar = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(
      this.props.functions.disabledUsedCar() ||
      this.props.functions.isBackToAO()
    ){
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    return temp;
  }

  isDisabledVehicle = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(
      this.props.functions.disabledFieldRenewal(
        "vehiclevehiclecodeyear"
      ) || this.props.functions.isBackToAO()
    ){
      temp = true;
    }

    return temp;
  }

  isDisabledBasicCover = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }
    
    if(this.props.functions.isBackToAO()){
      temp = true;
    }
    return temp;
  }

  isDisabledBasicCoverPeriod = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(this.props.functions.isBackToAO()){
      temp = true;
    }
    return temp;
  }

  isDisabledUsage = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(this.props.functions.isBackToAO()){
      temp = true;
    }
    return temp;
  }

  isDisabledVehicleColor = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(
      this.props.functions.disabledFieldRenewal("vehiclecolor") ||
      this.props.functions.isBackToAO()
    ){
      temp = true;
    }
    return temp;
  }

  isDisabledProductType = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(
      this.props.functions.disabledProductType()
    ){
      temp = true;
    }
    return temp;
  }

  isDisabledProductCode = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(
      this.props.functions.disabledProductCode()
    ){
      temp = true;
    }
    return temp;
  }

  isDisabledSegmentCode = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(
      this.props.functions.disabledSegmentCode()
    ){
      temp = true;
    }
    return temp;
  }

  isDisabledPlateNo = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }
    
    if(this.props.functions.isBackToAO()){
      temp = true;
    }
    return temp;
  }

  isDisabledChasisNo = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(this.props.functions.disabledFieldRenewal(
      "vehiclechasisno"
    ) || this.props.functions.isBackToAO()){
      temp = true;
    }

    return temp;
  }
  
  isDisabledEngineNo = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(this.props.functions.disabledFieldRenewal(
      "vehicleengineno"
    ) || this.props.functions.isBackToAO()){
      temp = true;
    }

    return temp;
  }

  isDisabledDealerName = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(this.props.functions.disableDealerVehicle()){
      temp = true;
    }

    return temp;
  }

  isDisabledSalesmanDealer = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(this.props.functions.disableSalesmanVehicle()){
      temp = true;
    }

    return temp;
  }

  isDisabledVehicleTSI = () => {
    let temp = false;

    if(this.props.functions.disabledVehicleData()){
      temp = true;
    }

    if (this.props.functions.isNSAApproval() && !Util.isNullOrEmpty(this.props.state.SurveyNo)) {
      temp = true;
    }

    if(this.props.functions.isFollowUpSurveyResult()){
      temp = true;
    }

    if(this.props.functions.isBackToAO()){
      temp = true;
    }

    return temp;
  }

  render() {
    let account = JSON.parse(ACCOUNTDATA);
    return (
      <div
        className={
          this.props.functions.disabledVehicleData() ? "disablediv" : ""
        }
      >
        <OtosalesSegmentDropdown title="DATA KENDARAAN" divid="datakendaraan">
          <div className="row">
            <div className="col-xs-12">
              <input
                name="personalpayercompany"
                checked={this.props.state.personalpayercompany}
                onChange={this.props.functions.onChangeFunction}
                disabled={this.props.functions.disableChecklistPayerCompany()}
                type="checkbox"
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">
                DIBAYARKAN OLEH PAYER COMPANY
              </label>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <input
                name="vehicleusedcar"
                checked={this.props.state.vehicleusedcar}
                onChange={this.props.functions.onChangeFunction}
                type="checkbox"
                style={{ marginRight: "10px" }}
                disabled={
                  this.isDisabledUsedCar()
                }
              />
              <label className="smallerCheckbox">USED CAR</label>
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehiclevehiclecode
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Vehicle</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                minHeight={40}
                isDisabled={
                  this.isDisabledVehicle()
                }
                name="vehiclevehiclecodeyear"
                onSearchFunction={this.props.functions.getVehicle}
                value={this.props.state.vehiclevehiclecodeyear}
                selectOption={this.props.state.vehiclevehiclecode}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehicle}
                placeholder="Pilih Kendaraan"
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehiclebasiccoverage
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Basic Cover</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehiclebasiccoverage"
                isDisabled={this.isDisabledBasicCover()}
                value={this.props.state.vehiclebasiccoverage}
                selectOption={this.props.state.vehiclebasiccoverage}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.databasiccover}
                placeholder="Pilih Basic Cover"
              />
            </div>
          </div>
          <div
            className={
              "row " + (this.isDisabledBasicCoverPeriod() ? "disablediv" : "")
            }
          >
            {(this.props.functions.handleShowPeriod() ||
              !this.props.state.IsRenewal) && (
              <div
                className={
                  this.props.state.IsNotErrorvehicleperiodfrom
                    ? "has-error col-xs-6"
                    : "col-xs-6"
                }
              >
                <span className="labelspanbold">Period From</span>
                <OtosalesDatePicker
                  // maxDate={Util.convertDate().setDate(Util.convertDate().getDate() + 30)}
                  minDate={Util.convertDate().setFullYear(
                    Util.convertDate().getFullYear() - 10
                  )}
                  className="form-control"
                  name="vehicleperiodfrom"
                  selected={this.props.state.vehicleperiodfrom}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.props.functions.onChangeFunctionDate}
                />
              </div>
            )}
            {this.props.functions.handleShowPeriod() && (
              <div
                className={
                  this.props.state.IsNotErrorvehicleperiodto
                    ? "has-error col-xs-6"
                    : "col-xs-6"
                }
              >
                <span className="labelspanbold">Period To</span>
                <OtosalesDatePicker
                  maxDate={(this.props.state.vehicleperiodfrom != null
                    ? Util.convertDate(
                        Util.formatDate(this.props.state.vehicleperiodfrom)
                      )
                    : Util.convertDate()
                  ).setDate(Util.convertDate().getFullYear() + 5)}
                  minDate={(this.props.state.vehicleperiodfrom != null
                    ? Util.convertDate(
                        Util.formatDate(this.props.state.vehicleperiodfrom)
                      )
                    : Util.convertDate()
                  ).setDate(Util.convertDate().getDate() + 1)}
                  className="form-control"
                  name="vehicleperiodto"
                  selected={this.props.state.vehicleperiodto}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.props.functions.onChangeFunctionDate}
                />
              </div>
            )}
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehicleusage ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Usage</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehicleusage"
                isDisabled={this.isDisabledUsage()}
                value={this.props.state.vehicleusage}
                selectOption={this.props.state.vehicleusage}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehicleusage}
                placeholder="Pilih Usage"
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehiclecolor ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">
              Warna Kendaraan Sesuai BPKB
            </span>
            <div className="col-xs-12">
              <input
                type="text"
                disabled={
                  this.isDisabledVehicleColor()
                }
                maxLength="50"
                value={this.props.state.vehiclecolor}
                name="vehiclecolor"
                onChange={this.props.functions.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehicleproducttype
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Product Type</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                isLoading={this.props.state.isLoading}
                minHeight={50}
                isDisabled={
                  this.isDisabledProductType()
                }
                onClick={(callbackShow = () => {}) => {
                  this.props.functions.getCheckVAPayment(
                    this.props.state.selectedQuotation,
                    () => {
                      this.props.functions.onShowAlertModalInfoIsPaid();
                    },
                    () => {
                      callbackShow();
                    }
                  );
                }}
                name="vehicleInsuranceType"
                value={this.props.state.vehicleInsuranceType}
                selectOption={this.props.state.vehicleInsuranceType}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehicleproducttype}
                placeholder="Pilih Product Type"
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehicleproductcode
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Product Code</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                isLoading={this.props.state.isLoading}
                minHeight={50}
                isDisabled={
                  this.isDisabledProductCode()
                }
                onClick={(callbackShow = () => {}) => {
                  this.props.functions.getCheckVAPayment(
                    this.props.state.selectedQuotation,
                    () => {
                      this.props.functions.onShowAlertModalInfoIsPaid();
                    },
                    () => {
                      callbackShow();
                    }
                  );
                }}
                name="vehicleproductcode"
                value={this.props.state.vehicleproductcode}
                selectOption={this.props.state.vehicleproductcode}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehicleproductcode}
                placeholder="Pilih Product Code"
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehiclesegmentcode
                ? "row has-error"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Segment Code</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                onClick={callBack => {
                  callBack();
                }}
                minHeight={50}
                isDisabled={this.isDisabledSegmentCode()}
                name="vehiclesegmentcode"
                value={this.props.state.vehiclesegmentcode}
                selectOption={this.props.state.vehiclesegmentcode}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehiclesegmentcode}
                placeholder="Pilih Segment Code"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              <span className="labelspanbold">Policy No</span>
              <input
                type="text"
                value={this.props.state.vehiclepolicyno}
                name="vehiclepolicyno"
                readOnly={true}
                // onChange={this.props.functions.onChangeFunction}
                className="form-control"
              />
            </div>
            <div
              className={
                this.props.state.IsNotErrorvehicleplateno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Plate No</span>
              <input
                type="text"
                maxLength="9"
                disabled={this.isDisabledPlateNo()}
                value={
                  Util.isNullOrEmpty(this.props.state.vehicleplateno)
                    ? this.props.state.vehicleplateno
                    : (this.props.state.vehicleplateno + "").toUpperCase()
                }
                name="vehicleplateno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.props.functions.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div
              className={
                this.props.state.IsNotErrorvehiclechasisno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Chasis No</span>
              <input
                type="text"
                value={
                  Util.isNullOrEmpty(this.props.state.vehiclechasisno)
                    ? this.props.state.vehiclechasisno
                    : (this.props.state.vehiclechasisno + "").toUpperCase()
                }
                maxLength="30"
                name="vehiclechasisno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.props.functions.onChangeFunction}
                className="form-control"
                disabled={
                  this.isDisabledChasisNo()
                }
              />
            </div>
            <div
              className={
                this.props.state.IsNotErrorvehicleengineno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Engine No</span>
              <input
                type="text"
                value={
                  Util.isNullOrEmpty(this.props.state.vehicleengineno)
                    ? this.props.state.vehicleengineno
                    : (this.props.state.vehicleengineno + "").toUpperCase()
                }
                maxLength="30"
                name="vehicleengineno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.props.functions.onChangeFunction}
                className="form-control"
                disabled={
                  this.isDisabledEngineNo()
                }
              />
            </div>
          </div>
          <div
            className={
              this.props.state.IsNotErrorvehicleregion ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Region</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehicleregion"
                isDisabled={
                  // this.props.functions.disabledFieldRenewal("vehicleregion") ||
                  // this.props.functions.isBackToAO()
                  true
                }
                value={this.props.state.vehicleregion}
                selectOption={this.props.state.vehicleregion}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.datavehicleregion}
                placeholder="Pilih Region"
              />
            </div>
          </div>
          {/* {this.props.state.IsSalesmanDealerEnable && ( */}
          {this.props.state.IsDealerEnable && (
            <React.Fragment>
              <div
                className={
                  (this.props.state.IsDealerEnable
                    ? "row"
                    : "row disablediv") +
                  " " +
                  (this.props.state.IsNotErrorvehicledealer ? "has-error" : "")
                }
              >
                <span className="col-xs-12 labelspanbold">Dealer Name</span>
                <div className="col-xs-12">
                  <OtosalesSelectFullviewv2
                    isDisabled={this.isDisabledDealerName()}
                    name="vehicledealer"
                    value={this.props.state.vehicledealer}
                    selectOption={this.props.state.vehicledealer}
                    onOptionsChange={this.props.functions.onOptionSelect2Change}
                    options={this.props.state.datavehicledealer}
                    placeholder="Pilih Dealer Name"
                  />
                </div>
              </div>
              </React.Fragment>
              )}
              {this.props.state.IsSalesmanInfoEnable && (
            <React.Fragment>
              <div
                className={
                  (this.props.state.IsSalesmanInfoEnable
                    ? "row"
                    : "row disablediv") +
                  " " +
                  (this.props.state.IsNotErrorvehiclesalesman
                    ? "has-error"
                    : "")
                }
              >
                <span className="col-xs-12 labelspanbold">
                  Salesman Dealer Name
                </span>
                <div className="col-xs-12">
                  {/* <input
                        type="text"
                        value={this.props.state.vehiclesalesmanname}
                        name="vehiclesalesmanname"
                        readOnly
                        className="form-control"
                      /> */}
                  <OtosalesSelectFullviewv2
                    isDisabled={this.isDisabledSalesmanDealer()}
                    name="vehiclesalesman"
                    value={this.props.state.vehiclesalesman}
                    selectOption={this.props.state.vehiclesalesman}
                    onOptionsChange={this.props.functions.onOptionSelect2Change}
                    options={this.props.state.datavehiclesalesman}
                    placeholder="Pilih Salesman Name"
                  />
                </div>
              </div>
              <div className="row">
                <div
                  className={
                    "col-xs-6 " +
                    (this.props.state.IsNotErrorvehiclenamebank
                      ? "has-error"
                      : "")
                  }
                >
                  <span className="labelspanbold">Nama Bank</span>
                  <input
                    type="text"
                    value={this.props.state.vehiclenamebank}
                    name="vehiclenamebank"
                    readOnly
                    className="form-control"
                  />
                </div>
                <div
                  className={
                    "col-xs-6 " +
                    (this.props.state.IsNotErrorvehiclenomorrek
                      ? "has-error"
                      : "")
                  }
                >
                  <span className="labelspanbold">Nomor Rekening</span>
                  <input
                    type="text"
                    value={this.props.state.vehiclenomorrek}
                    name="vehiclenomorrek"
                    readOnly
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
          {Util.isNullOrEmpty(account.UserInfo.User.Channel) ||
          account.UserInfo.User.Channel != "EXT" ? (
            ""
          ) : (
            <div className="row">
              <span className="col-xs-12 labelspanbold">Agency</span>
              <div className="col-xs-12">
                <input
                  type="text"
                  value={this.props.state.vehicleagency}
                  name="vehicleagency"
                  readOnly
                  className="form-control"
                />
              </div>
            </div>
          )}
          {Util.isNullOrEmpty(account.UserInfo.User.Channel) ||
          account.UserInfo.User.Channel != "EXT" ? (
            ""
          ) : (
            <div className="row">
              <span className="col-xs-12 labelspanbold">Upliner</span>
              <div className="col-xs-12">
                <input
                  type="text"
                  value={this.props.state.vehicleupliner}
                  name="vehicleupliner"
                  readOnly
                  className="form-control"
                />
              </div>
            </div>
          )}
          <div
            className={
              this.props.state.IsNotErrorvehicletotalsuminsured
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Total Sum Insured</span>
            <div className="col-xs-12">
              <NumberFormat
                className="form-control"
                disabled={this.isDisabledVehicleTSI()}
                onValueChange={value => {
                  // if(!Util.isNullOrEmpty(this.props.state.vehicletotalsuminsuredtemp) &&(this.props.state.vehicletotalsuminsuredtemp != 0)){
                  //   this.props.functions.checkMaxPriceTSI(value.floatValue, () => {
                  this.props.functions.setState(
                    {
                      vehicletotalsuminsuredtemp: value.floatValue
                    },
                    () => {
                      if (this.props.functions.isLoadingDataFirst()) {
                        this.props.functions.trigerRateCalculation();
                      }
                      if (
                        !Util.isNullOrEmpty(
                          this.props.state.vehicletotalsuminsured
                        ) &&
                        this.props.state.vehicletotalsuminsured != 0
                      ) {
                        setTimeout(() => {
                          this.props.functions.checkMaxPriceTSI(
                            this.props.state.vehicletotalsuminsuredtemp
                          );
                        }, 3000);
                      }
                    }
                  );
                  //   })
                  // }
                }}
                value={this.props.state.vehicletotalsuminsuredtemp}
                thousandSeparator=","
                prefix={"Rp."}
              />
            </div>
          </div>
          {/* {this.props.state.IsRenewal && this.props.functions.hideVehicleOriginalDefect() && ( */}
          {this.props.state.IsRenewal &&
            Util.isNullOrEmpty(this.props.state.SurveyNo) && (
              <React.Fragment>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Cacat Semula</span>
                  <div className="col-xs-12">
                    <textarea
                      class="form-control"
                      rows="5"
                      value={this.props.state.vehiclecacatsemula}
                      readOnly
                      style={{ resize: "none" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">No Cover</span>
                  <div className="col-xs-12">
                    <textarea
                      class="form-control"
                      rows="5"
                      value={this.props.state.vehiclenocover}
                      readOnly
                      style={{ resize: "none" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Accesories</span>
                  <div className="col-xs-12">
                    <textarea
                      class="form-control"
                      rows="5"
                      value={this.props.state.vehicleaccesories}
                      readOnly
                      style={{ resize: "none" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12">
                    <input
                      name="vehicleisordefectrepair"
                      checked={this.props.state.vehicleisordefectrepair}
                      onChange={this.props.functions.onChangeFunction}
                      type="checkbox"
                      disabled={this.props.functions.isBackToAO()}
                      style={{ marginRight: "10px" }}
                    />
                    <label className="smallerCheckbox">
                      ADA PERBAIKAN CACAT SEMULA
                    </label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12">
                    <input
                      name="vehicleisaccessorieschange"
                      checked={this.props.state.vehicleisaccessorieschange}
                      onChange={this.props.functions.onChangeFunction}
                      disabled={this.props.functions.isBackToAO()}
                      type="checkbox"
                      style={{ marginRight: "10px" }}
                    />
                    <label className="smallerCheckbox">
                      ADA PENAMBAHAN AKSESORIS TAMBAHAN
                    </label>
                  </div>
                </div>
              </React.Fragment>
            )}
          {this.props.state.IsRenewal && (
            <React.Fragment>
              <div className="row">
                <div className="col-xs-12">
                  <input
                    name="vehicleispolicyissuedbeforpaying"
                    checked={this.props.state.vehicleispolicyissuedbeforpaying}
                    onChange={this.props.functions.onChangeFunction}
                    disabled={
                      this.props.functions.disabledPolisTerbitSebelumBayar()
                    }
                    type="checkbox"
                    style={{ marginRight: "10px" }}
                  />
                  <label className="smallerCheckbox">
                    POLIS TERBIT SEBELUM BAYAR ATAU BAYAR DENGAN CC/DEBIT
                  </label>
                </div>
              </div>
              <div className="row" style={{ paddingLeft: "20px" }}>
                <span className="col-xs-12 labelspanbold">Remarks</span>
                <div className="col-xs-12">
                  <input
                    type="text"
                    name="vehicleremarks"
                    disabled={this.props.functions.disabledPolisTerbitSebelumBayar()}
                    value={this.props.state.vehicleremarks}
                    onChange={this.props.functions.onChangeFunction}
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
        </OtosalesSegmentDropdown>
      </div>
    );
  }
}
