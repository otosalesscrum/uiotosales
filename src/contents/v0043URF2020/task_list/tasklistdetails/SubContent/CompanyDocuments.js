import React, { Component } from "react";
import {
  OtosalesDropzonev2,
  OtosalesLoadingSegment,
  OtosalesSegmentDropdown,
  Util
} from "../../../../../otosalescomponents";

export default class CompanyDocuments extends Component {
  render() {
    return (
      <div>
        <OtosalesSegmentDropdown
          title="COMPANY DOCUMENTS"
          divid="companydocument"
        >
          <OtosalesLoadingSegment
            isLoading={this.props.state.isLoadingCompanyDocument}
          >
            <div>
              {/* {!this.props.state.IsRenewal && ( */}
              <React.Fragment>
                <div
                  className={
                    "col-xs-4 col-md-2 panel-body-list" +
                    (this.props.state.IsNotErrorNPWP ? " has-error" : "") +
                    (this.props.functions.disabledCompanyDocuments()
                      ? " disablediv"
                      : "")
                  }
                >
                  <div className="text-center">
                    <OtosalesDropzonev2
                      src={this.props.state.dataNPWP}
                      name="NPWP"
                      handleDeleteImage={this.props.functions.handleDeleteImage}
                      handleUploadImage={this.props.functions.handleUploadImage}
                    />
                  </div>
                  <center className="smallerCheckbox">
                    NPWP {this.props.state.IsRenewal ? "" : "*"}
                  </center>
                </div>
                <div
                  className={
                    "col-xs-4 col-md-2 panel-body-list" +
                    (this.props.state.IsNotErrorSIUP ? " has-error" : "") +
                    (this.props.functions.disabledCompanyDocuments()
                      ? " disablediv"
                      : "")
                  }
                >
                  <div className="text-center">
                    <OtosalesDropzonev2
                      src={this.props.state.dataSIUP}
                      name="SIUP"
                      handleDeleteImage={this.props.functions.handleDeleteImage}
                      handleUploadImage={this.props.functions.handleUploadImage}
                    />
                  </div>
                  <center className="smallerCheckbox">SIUP</center>
                </div>
              </React.Fragment>
              {/* )} */}
              {!this.props.state.IsRenewal && (
                <div
                  className={
                    "col-xs-4 col-md-2 panel-body-list" +
                    (this.props.state.IsNotErrorBSTB ? " has-error" : "") +
                    (this.props.functions.disabledCompanyDocuments()
                      ? " disablediv"
                      : "")
                  }
                >
                  <div className="text-center">
                    <OtosalesDropzonev2
                      src={this.props.state.dataBSTB}
                      name="BSTB"
                      handleDeleteImage={this.props.functions.handleDeleteImage}
                      handleUploadImage={this.props.functions.handleUploadImage}
                    />
                  </div>
                  <center className="smallerCheckbox">BSTB</center>
                </div>
              )}
              {/* {this.props.state.IsRenewal && ( */}
              {/* <React.Fragment> */}
              {!this.props.functions.isHideDocRep() && (
                <React.Fragment>
                  <div
                    className={
                      "col-xs-4 col-md-2 panel-body-list" +
                      (this.props.state.IsNotErrorDOCREP ? " has-error" : "")
                    }
                  >
                    <div className="text-center">
                      <OtosalesDropzonev2
                        src={this.props.state.dataDOCREP}
                        name="DOCREP"
                        handleDeleteImage={
                          this.props.functions.handleDeleteImage
                        }
                        handleUploadImage={
                          this.props.functions.handleUploadImage
                        }
                      />
                    </div>
                    <center className="smallerCheckbox">DOC REP</center>
                  </div>
                </React.Fragment>
              )}
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.props.state.IsNotErrorKONFIRMASICUST
                    ? " has-error"
                    : "") +
                  (this.props.functions.disabledCompanyDocuments()
                    ? " disablediv"
                    : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.props.state.dataKONFIRMASICUST}
                    name="KONFIRMASICUST"
                    handleDeleteImage={this.props.functions.handleDeleteImage}
                    handleUploadImage={this.props.functions.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  KONFIRMASI CUSTOMER *
                </center>
              </div>
              {/* </React.Fragment> */}
              {/* )} */}
            </div>
          </OtosalesLoadingSegment>
        </OtosalesSegmentDropdown>
      </div>
    );
  }
}
