import { Util, Log } from "../../../../otosalescomponents";
import {
  API_URL,
  ACCOUNTDATA,
  API_VERSION_0063URF2020
} from "../../../../config";

export const GetTaskList = async(state, SIZE_PAGING, txtsearch) => {
    return Util.fetchAPIAdditional(`${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getTaskList/`, "POST", 
    { // Header
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
    }, { // Body
      salesofficerid: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      allowSearchOthers: JSON.parse(ACCOUNTDATA).UserInfo.User.IsCSO,
      indexOrderByItem: state.indexOrderByItem,
      indexOrderByTypeItem: state.indexOrderByTypeItem,
      chFollowUpMonthlyChecked: state.chFollowUpMonthlyChecked || false,
      chNeedFUChecked: state.chNeedFUChecked || false,
      chPotentialChecked: state.chPotentialChecked || false,
      chCallLaterChecked: state.chCallLaterChecked || false,
      chCollectDocChecked: state.chCollectDocChecked || false,
      chOrderRejectedChecked: state.chOrderRejectedChecked || false,
      chNotDealChecked: state.chNotDealChecked || false, 
      chSentToSAChecked: state.chSentToSAChecked || false,
      chBackToAOChecked: state.chBackToAOChecked || false,
      chPolicyCreatedChecked: state.chPolicyCreatedChecked || false,
      chFollowUpDateChecked: state.chFollowUpDateChecked || false,
      getFollowUpDate: state.getFollowUpDate +" 23:59:59.725" || false,
      chIsSearchbyEngNo: state.chIsSearchbyEngNo || false,
      chIsSearchbyChasNo: state.chIsSearchbyChasNo || false,
      chIsSearchbyPolicyNo: state.chIsSearchbyPolicyNo || false,
      chIsSearchbyProsName: state.chIsSearchbyProsName || false,
      search: txtsearch,
      currentPage: state.currentPage,
      PageSize: SIZE_PAGING,
      PageSizeLead: SIZE_PAGING,
      chNewChecked: state.chNewChecked || false,
      chRenewableChecked: state.chRenewableChecked || false
    })
    .then(res => res.json())
}
