import React from 'react'
import {
  OtosalesDropzonev2,
  OtosalesSelectFullviewv2,
  Util
} from "../../../../otosalescomponents";
import {
  Call
} from "../../../../assets";
import { secureStorage } from "../../../../otosalescomponents/helpers/SecureWebStorage";
import NumberFormat from "react-number-format";


export default function ProspectInfo(props) {
    return (
        <React.Fragment>
            <form onSubmit={props.functions.onSubmitProspect} autoComplete="off">
                    {!props.state.isMvGodig && (
                      <div>
                        <input
                          type="checkbox"
                          style={{ marginRight: "10px" }}
                          onChange={props.functions.onChangeFunction}
                          checked={props.state.isCompany}
                          name="isCompany"
                        />
                        <label>COMPANY</label>
                      </div>
                    )}
                    <div
                      className={
                        props.state.isErrProsName
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT NAME * </label>
                      <div className="input-group">
                        <input
                          type="text"
                          maxLength="50"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          pattern="^[a-zA-Z0-9 ,-.\/]+$"
                          value={props.state.Name}
                          name="Name"
                          className="form-control "
                          placeholder="Enter new prospect name"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrPhone1
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT PHONE NUMBER 1 *</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            props.functions.setState({ Phone1: value.value });
                            if (props.state.Phone1.length > 13)
                              props.state.isErrPhone1 = true;
                            else if (props.state.Phone1.length < 9)
                              props.state.isErrPhone1 = true;
                            else props.state.isErrPhone1 = false;
                          }}
                          value={props.state.Phone1}
                          format="#### #### #### ####"
                          prefix={""}
                          isNumericString={true}
                        />
                        <div
                          className="input-group-addon "
                          style={{ border: "0px" }}
                          onClick={() => {
                            if (props.state.Phone1 != "") {
                              window.location.href = "tel:" + props.state.Phone1;
                            }
                          }}
                        >
                          <img src={Call} style={{ width: "20px" }} />
                        </div>
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrPhone2
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT PHONE NUMBER 2</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            props.functions.setState({ Phone2: value.value });
                            if (props.state.Phone2.length > 13)
                              props.state.isErrPhone2 = true;
                            else if (props.state.Phone2.length < 9)
                              props.state.isErrPhone2 = true;
                            else props.state.isErrPhone2 = false;
                          }}
                          value={props.state.Phone2}
                          format="#### #### #### ####"
                          prefix={""}
                          isNumericString={true}
                        />
                        <div
                          className="input-group-addon "
                          style={{ border: "0px" }}
                          onClick={() => {
                            if (props.state.Phone2 != "") {
                              window.location.href = "tel:" + props.state.Phone2;
                            }
                          }}
                        >
                          <img src={Call} style={{ width: "20px" }} />
                        </div>
                      </div>
                    </div>
                    {window.location.pathname == "/tasklist-edit" &&
                      JSON.parse(secureStorage.getItem("FollowUpSelected")) &&
                      JSON.parse(secureStorage.getItem("FollowUpSelected"))
                        .FollowUpStatus == 9 && (
                        <div className="form-group">
                          <label>PREFERED CALLING TIME (PCT) </label>
                          <div className="input-group">
                            <input
                              type="text"
                              style={{ marginBottom: "0px" }}
                              value={
                                secureStorage.getItem(
                                  "followUpDateTimeSelected"
                                ) != "null"
                                  ? props.functions.formatDate(
                                      Util.convertDate(
                                        secureStorage.getItem(
                                          "followUpDateTimeSelected"
                                        )
                                      )
                                    ) +
                                    " " +
                                    props.functions.formatTimeFromDate(
                                      Util.convertDate(
                                        secureStorage.getItem(
                                          "followUpDateTimeSelected"
                                        )
                                      )
                                    )
                                  : " "
                              }
                              className="form-control "
                              placeholder="Enter new prospect name"
                            />
                          </div>
                        </div>
                      )}
                    <div
                      className={
                        props.state.isErrEmail
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT EMAIL</label>
                      <div className="input-group">
                        <input
                          type="email"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          value={props.state.Email1}
                          name="Email1"
                          className="form-control"
                          placeholder="Enter prospect email"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrDealer
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>DEALER NAME</label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrDealer}
                          value={props.state.DealerCode}
                          selectOption={props.state.DealerCode}
                          onOptionsChange={props.functions.onOptionDealerChange}
                          options={props.state.datadealer}
                          placeholder="Select dealer name"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrSalesman
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>SALESMAN DEALER NAME </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrSalesman}
                          value={props.state.SalesDealer}
                          selectOption={props.state.SalesDealer}
                          onOptionsChange={props.functions.onOptionSalesmanChange}
                          options={props.state.datasalesman}
                          placeholder="Select salesman dealer name"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>POLICY NUMBER</label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          value={props.state.PolicyNo}
                          disabled={true}
                          name="PolicyNo"
                          className="form-control "
                          placeholder="Enter policy number"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <legend>
                        <b>ATTACHMENT</b>
                      </legend>
                    </div>
                    <div className="form-group">
                      <div className="col-xs-3 panel-body-list">
                        <label>KTP/KITAS/SIM</label>
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={props.state.dataKTP}
                            name="KTP"
                            handleDeleteImage={props.functions.handleDeleteImage}
                            handleUploadImage={props.functions.handleUploadImage}
                          />
                        </div>
                      </div>
                      <div
                        className="col-xs-3 text-center panel-body-list"
                        style={{ marginLeft: "25px" }}
                      >
                        <label>STNK</label>
                        <OtosalesDropzonev2
                          src={props.state.dataSTNK}
                          name="STNK"
                          handleDeleteImage={props.functions.handleDeleteImage}
                          handleUploadImage={props.functions.handleUploadImage}
                        />
                      </div>
                      <div
                        className="col-xs-3 text-center panel-body-list"
                        style={{ marginLeft: "25px" }}
                      >
                        <label>SPPAKB</label>
                        <OtosalesDropzonev2
                          src={props.state.dataSPPAKB}
                          name="SPPAKB"
                          handleDeleteImage={props.functions.handleDeleteImage}
                          handleUploadImage={props.functions.handleUploadImage}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-xs-12">
                        <label>BSTB</label>
                      </div>
                      <div className="col-xs-12 text-center">
                        <div className="col-xs-3 panel-body-list">
                          <OtosalesDropzonev2
                            src={props.state.dataBSTB1}
                            name="BSTB1"
                            handleDeleteImage={props.functions.handleDeleteImage}
                            handleUploadImage={props.functions.handleUploadImage}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                        <br />
                        <br />
                        <br />
                      </div>
                    </div>
                  </form>
        </React.Fragment>
    )
}
