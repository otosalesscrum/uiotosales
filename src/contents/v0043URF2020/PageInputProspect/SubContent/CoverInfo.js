import React from 'react'
  import {
    OtosalesSelectFullviewv2,
    Util
  } from "../../../../otosalescomponents";
  import OtosalesDatePicker from "../../../../otosalescomponents/otosalesdatepicker";
  import NumberFormat from "react-number-format";

export default function CoverInfo(props) {
    return (
        <React.Fragment>
            <form onSubmit={props.functions.onSubmitCover} autoComplete="off">
                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <div className="row separatorTop backgroundgrey">
                          <h5 className="col-xs-8 col-md-10">
                            <b>PRODUCT INFO</b>
                          </h5>
                        </div>
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrCovProductType
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PRODUCT TYPE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrCovProductType}
                          value={props.state.CoverProductType}
                          selectOption={props.state.CoverProductType}
                          onOptionsChange={props.functions.onOptionCovProductTypeChange}
                          options={props.state.datacoverproducttype}
                          placeholder="Select product type"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrCovProductCode
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PRODUCT CODE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrCovProductCode}
                          value={props.state.CoverProductCode}
                          selectOption={props.state.CoverProductCode}
                          onOptionsChange={props.functions.onOptionCovProductCodeChange}
                          options={props.state.datacoverproductcode}
                          placeholder="Select product code"
                        />
                      </div>
                    </div>
                    {/* )} */}

                    <div
                      className={
                        props.state.isErrCovBasicCover
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>BASIC COVER * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrCovBasicCover}
                          value={props.state.CoverBasicCover}
                          selectOption={props.state.CoverBasicCover}
                          onOptionsChange={props.functions.onOptionCovBasicCoverChange}
                          options={props.state.datacoverbasiccover}
                          placeholder="Select basic cover"
                        />
                      </div>
                    </div>
                    {props.functions.handleShowPeriod() && (
                      <div className="row">
                        <div className="col-xs-6">
                          <span className="labelspanbold">Period From</span>
                          <OtosalesDatePicker
                            minDate={Util.convertDate()}
                            maxDate={Util.convertDate().setDate(
                              Util.convertDate().getDate() + 30
                            )}
                            className="form-control"
                            name="vehicleperiodfrom"
                            selected={props.state.vehicleperiodfrom}
                            dateFormat="dd/mm/yyyy"
                            onChange={props.functions.onChangeFunctionDate}
                          />
                        </div>
                        <div className="col-xs-6">
                          <span className="labelspanbold">Period To</span>
                          <OtosalesDatePicker
                            maxDate={(props.state.vehicleperiodfrom != null
                              ? Util.convertDate(
                                  Util.formatDate(props.state.vehicleperiodfrom)
                                )
                              : Util.convertDate()
                            ).setDate(Util.convertDate().getFullYear() + 5)}
                            minDate={
                              props.state.vehicleperiodfrom != null
                                ? Util.convertDate(
                                    Util.formatDate(
                                      props.state.vehicleperiodfrom
                                    )
                                  )
                                : Util.convertDate()
                            }
                            className="form-control"
                            name="vehicleperiodto"
                            selected={props.state.vehicleperiodto}
                            dateFormat="dd/mm/yyyy"
                            onChange={props.functions.onChangeFunctionDate}
                          />
                        </div>
                      </div>
                    )}

                    <div
                      className={
                        props.state.isErrCovSumInsured
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>SUM INSURED *</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            props.functions.setState(
                              { CovSumInsured: value.floatValue },
                              () => {
                                props.state.isErrCovSumInsured = props.functions.checkRateCalculatePremi();
                              }
                            );
                          }}
                          value={props.state.CovSumInsured}
                          thousandSeparator={true}
                          prefix={""}
                        />
                      </div>
                    </div>
                    <div>
                      <div className="row backgroundgrey labelspanbold labelfontgrey">
                        <div
                          className="col-xs-12"
                          style={{ paddingTop: "5px", paddingBottom: "5px" }}
                          data-toggle="collapse"
                          data-target="#extendedcover"
                        >
                          <span className="pull-left">EXTENDED COVER</span>
                          <span className="pull-right collapsebutton" />
                        </div>
                      </div>
                      <div
                        id="extendedcover"
                        className="panel-body panel-padding0 collapse in"
                      >
                        <div>
                          {props.state.IsTPLEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsTPLChecked}
                                      name="IsTPLChecked"
                                      value={props.state.IsTPLChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(4);
                                      }}
                                    />
                                    TJH
                                  </label>
                                </div>
                                <NumberFormat
                                  allowNegative={false}
                                  disabled={true}
                                  className="form-control"
                                  value={props.state.TPLSICOVER}
                                  thousandSeparator={true}
                                  prefix={""}
                                />
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(props.state.TPLPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsSRCCEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsSRCCChecked}
                                      name="IsSRCCChecked"
                                      value={props.state.IsSRCCChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(0);
                                      }}
                                      disabled={
                                        props.state.IsSRCCCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    STRIKE, RIOT, COMMOTION (SRCC)
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(props.state.SRCCPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsFLDEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsFLDChecked}
                                      name="IsFLDChecked"
                                      value={props.state.IsFLDChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(1);
                                      }}
                                      disabled={
                                        props.state.IsFLDCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    FLOOD &amp; WINDSTROM
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(
                                    props.state.FLDPremi,
                                    0
                                  )}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsETVEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsETVChecked}
                                      name="IsETVChecked"
                                      value={props.state.IsETVChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(2);
                                      }}
                                      disabled={
                                        props.state.IsETVCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    EARTHQUAKE, TSUNAMI, VOLCANO ERUPTION
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(props.state.ETVPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsTSEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      name="IsTSChecked"
                                      checked={props.state.IsTSChecked}
                                      value={props.state.IsTSChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(3);
                                      }}
                                      disabled={
                                        props.state.IsTRSCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    TERRORISM &amp; SABOTAGE
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(props.state.TSPremi, 0)}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsPADRVREnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsPADRVRChecked}
                                      name="IsPADRVRChecked"
                                      value={props.state.IsPADRVRChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(5);
                                      }}
                                      disabled={
                                        props.state.IsPADRIVERCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    PA DRIVER
                                  </label>
                                </div>
                                <NumberFormat
                                  allowNegative={false}
                                  disabled={true}
                                  className="form-control"
                                  value={props.state.PADRVCOVER}
                                  thousandSeparator={true}
                                  prefix={""}
                                />
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(
                                    props.state.PADRVRPremi,
                                    0
                                  )}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsPAPASSEnabled == 0 ? (
                            ""
                          ) : (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={props.state.IsPAPASSChecked}
                                      name="IsPAPASSChecked"
                                      value={props.state.IsPAPASSChecked}
                                      onChange={event => {
                                        props.functions.setDialogPeriod(6);
                                      }}
                                      disabled={
                                        props.state.IsPAPASSCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    PA PASSENGER
                                  </label>
                                </div>
                                <div className="row">
                                  <div className="col-md-3 col-xs-6 panel-body-list">
                                    <div
                                      className="col-xs-8"
                                      style={{ paddingRight: "0px" }}
                                    >
                                      <input
                                        className="form-control"
                                        type="number"
                                        name="PASSCOVER"
                                        value={props.state.PASSCOVER}
                                        readOnly={true}
                                      />
                                    </div>
                                    <div className="col-xs-4">@</div>
                                  </div>
                                  <div
                                    className="col-md-9 col-xs-6 panel-body-list"
                                    style={{
                                      marginLeft: "-5px",
                                      paddingRight: "10px"
                                    }}
                                  >
                                    <NumberFormat
                                      disabled={true}
                                      className="form-control"
                                      value={props.state.PAPASSICOVER}
                                      thousandSeparator={true}
                                      prefix={""}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(props.state.PAPASSPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {props.state.IsACCESSEnabled == 1 &&
                            !props.state.isMvGodig && (
                              <div className="row">
                                <div className="col-xs-8 col-md-10">
                                  <div className="checkbox">
                                    <label>
                                      <input
                                        type="checkbox"
                                        checked={props.state.IsACCESSChecked}
                                        name="IsACCESSChecked"
                                        onChange={event => {
                                          props.functions.setDialogPeriod(7);
                                        }}
                                      />
                                      ACCESSORY
                                    </label>
                                    <i
                                      className="fa fa-info-circle fa-2x"
                                      style={{
                                        marginLeft: "5px",
                                        opacity: "0.3"
                                      }}
                                      onClick={() => {
                                        props.functions.ShowAccessoryInfo();
                                      }}
                                    />
                                  </div>
                                  <NumberFormat
                                    allowNegative={false}
                                    disabled={true}
                                    className="form-control"
                                    // onValueChange={value => {
                                    //   props.functions.setState(
                                    //     { ACCESSCOVER: value.floatValue },
                                    //     () => {
                                    //       props.functions.rateCalculateTPLPAPASSPADRV(
                                    //         "ACCESSCOVER"
                                    //       );
                                    //     }
                                    //   );
                                    // }}
                                    value={props.state.ACCESSCOVER}
                                    thousandSeparator={true}
                                    prefix={""}
                                  />
                                </div>
                                <div className="col-xs-4 col-md-2 text-right">
                                  <p style={{ marginTop: "10px" }}>
                                    {Util.formatMoney(
                                      props.state.ACCESSPremi,
                                      0
                                    )}
                                  </p>
                                </div>
                              </div>
                            )}
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                      </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                  </form>
        </React.Fragment>
    )
}
