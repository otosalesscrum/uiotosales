import {
    API_URL,
    API_VERSION,
    API_VERSION_2,
    ACCOUNTDATA,
    HEADER_API,
    API_VERSION_0219URF2019,
    API_VERSION_0063URF2020
  } from "../../../config";
  import {
    Tabs,
    OtosalesModalSendEmail,
    OtosalesModalSendSMS,
    OtosalesModalAccessories,
    OtosalesModalInfo,
    OtosalesModalQuotationHistory,
    OtosalesModalSetPeriodPremi2,
    OtosalesLoading,
    Util,
    Log
  } from "../../../otosalescomponents";
  import { ToastContainer, toast } from "react-toastify";
  import Modal from "react-responsive-modal";
  import imageCompression from "browser-image-compression";
  import { DeviceUUID } from "device-uuid";


export const getAccessoriesList = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getAccessoriesList`,
        "POST",
        HEADER_API
    )
  }

  export const getDealerName = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getDealerName/`,
        "POST",
        HEADER_API
    )
  }

  export const getVehicleBrand = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleBrand/`,
        "POST",
        HEADER_API
    )
  }

  export const getVehicleUsage = async (isMvGodig) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getVehicleUsage/`,
        "POST",
        HEADER_API,
        {
          isMvGodig
        }
      )
  }

  export const getVehicleRegion = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleRegion/`,
        "POST",
        HEADER_API
    )
  }

  export const getVehiclePrice = async (vehiclecode, year, citycode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehiclePrice/`,
        "POST",
        HEADER_API,
        {
            vehiclecode,
            year,
            citycode
        }
    )
  }

  export const getProductType = async (Channel, salesofficerid, isMvGodig) => {
    return Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getProductType/`,
      `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getProductType/`,
        "POST",
        HEADER_API,
        {
          Channel,
          salesofficerid,
          isMvGodig
        }
      )
  }

  export const getBasicCover = async (Channel, salesofficerid, isBasic, vyear, isMvGodig) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getBasicCover/`,
        "POST",
        HEADER_API,
        {
          Channel,
          salesofficerid,
          isBasic,
          vyear,
          isMvGodig
        }
      )
  }

