import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { OtosalesDashboardTabsHandler,OtosalesModalDashboard } from "../otosalescomponents";
import BlockUi from "react-block-ui";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";


class PageDashboardBranch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ViewCode: "0",
      PeriodCode: "Monthly",
      ViewList: [
        { value: "0", label: "Total Prospect" },
        { value: "1", label: "Total Need FU" },
        { value: "2", label: "Total Deal" },
        { value: "3", label: "Total Potential" },
        { value: "4", label: "Total Call Later" },
        { value: "5", label: "Total Not Deal" },
        { value: "6", label: "Total Send To SA" },
        { value: "7", label: "Total Collect Doc" },
        { value: "8", label: "Total Back To AO" },
        { value: "9", label: "Total Policy Created" }
      ],
      PeriodList: [
        { value: "Monthly", label: "Month to Date" },
        { value: "Yearly", label: "Year to Date" }
      ],
      fuModel: [],
      aabHeads: [],
      isLoading: false,
      datasList: [],
      Param: "",
      Role:"",
      showModal:false,
      showModalFilter:false

    }

  }
  componentDidMount() {
    var Period =JSON.parse(secureStorage.getItem("Period"));
    this.state.PeriodCode= Period==null?this.state.PeriodCode:Period;
    this.state.Param=JSON.parse(secureStorage.getItem("RegStepParam"));
    this.state.Role= this.props.userInfo.User.Role;
    this.state.Param=this.state.Param==null?this.props.userInfo.User.BranchCode:this.state.Param;
    this.getDataDashboard();
  }
  setEmailDefault(){
    var email="";
      if(this.props.userInfo.Role=="NATIONALMGR" || this.props.userInfo.Role=="BRANCHSUPPORT" || this.props.userInfo.Role =="REGIONALMGR" ||this.props.userInfo.User.Role=="BRANCHMGR" || this.props.userInfo.User.Role=="SALESSECHEAD"){
        email = this.props.userInfo.User.Email;
        }else{
        email = this.props.userInfo.User.Email + "; " + this.props.userInfo.ReportTo;
        }
        return email;
    }
  onCloseModal = () => {
    this.setState({ showModalFilter: false });
  };
  onLoading = isLoading => {
    this.setState({ isLoading });
  }
  onModalChange = showModal => {
    this.setState({ showModal });
  }
  getDataDashboard = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getDashboard/`, {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "salesofficerid=" + this.props.username + "&periodView=" + this.state.PeriodCode + "&userRole=" + this.state.Role + "&StateDashboard=BRANCHMGR"
        + "&Channel=" + this.props.userInfo.User.Channel + "&ChannelSource=" + this.props.userInfo.User.ChannelSource + "&param=" + this.state.Param
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          fuModel: jsn.fuModel,
          aabHeads: jsn.aabHeads,
          isLoading: false
        })
      }
      )
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
        //  this.getDataDashboard();
        }
      });


  };
  onOptionViewChange = ViewCode => {
    this.setState({ ViewCode });
  }
  onOptionPeriodChange = PeriodCode => {
    this.setState({ PeriodCode }, () =>{
      this.getDataDashboard();
    });
    secureStorage.setItem("Period", JSON.stringify(this.state.PeriodCode));
  }
  render() {
    return (
      <div>
        <Header titles="Branch Dashboard">
        <ul class="nav navbar-nav navbar-right">
            <li>
              <a 
              className="fa fa-envelope fa-lg"
              onClick={() => this.setState({ showModalFilter: true })}
               />
            </li>
          </ul>
        </Header>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            className="col-xs-12 panel-body-list"
            blocking={this.state.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >
                 <OtosalesModalDashboard
        showModal={this.state.showModalFilter}
        onModalChange={this.onModalChange}
        onCloseModal={this.onCloseModal}
        Role={this.props.userInfo.User.Role}
        Email={this.props.userInfo.User.Email}
        ReportTo={this.props.userInfo.ReportTo}
        EmailDefault={this.setEmailDefault()}
        SalesOfficer={this.props.userInfo.User.SalesOfficerID}
        PeriodCode={this.state.PeriodCode}
        Loading={this.onLoading}
      />
              {this.state.fuModel.length>0 &&<OtosalesDashboardTabsHandler
                  onClickListItems = {this.onClickListItems}
                  onOptionPeriodChange={this.onOptionPeriodChange}
                  onOptionViewChange={this.onOptionViewChange}
                  ViewCode={this.state.ViewCode}
                  ViewList={this.state.ViewList}
                  PeriodCode={this.state.PeriodCode}
                  PeriodList={this.state.PeriodList}
                  rowscount={this.state.aabHeads.length}
                  viewFilter={this.state.ViewCode}
                  dataitems={this.state.fuModel}
                  dataitems2={this.state.aabHeads}
                  Role="BRANCHMGR"
                />}

          </BlockUi>
        </div>
      </div>
    );
  }
}

export default withRouter(PageDashboardBranch);
