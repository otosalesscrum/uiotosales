import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter, Link } from "react-router-dom";
import { OtosalesModalDashboard,OtosalesDashboardPersonal } from "../otosalescomponents";
import BlockUi from "react-block-ui";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { ToastContainer } from "react-toastify";


class PageDashboardPersonal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fuModel: [],
      aabHeads: [],
      isLoading: false,
      datasList: [],
      Param: "",
      showModal:false,
      showModalFilter:false

    }

  }
  componentDidMount() {
    this.getDataDashboard();
  }
  setEmailDefault(){
    var email="";
      if(this.props.userInfo.Role=="NATIONALMGR" || this.props.userInfo.Role=="BRANCHSUPPORT" || this.props.userInfo.Role =="REGIONALMGR" ||this.props.userInfo.User.Role=="BRANCHMGR" || this.props.userInfo.User.Role=="SALESSECHEAD"){
        email = this.props.userInfo.User.Email;
        }else{
        email = this.props.userInfo.User.Email + "; " + this.props.userInfo.ReportTo;
        }
        // console.log("ahay"+email);
        return email;
    }
  onCloseModal = () => {
    this.setState({ showModalFilter: false });
  };
  onLoading = isLoading => {
    this.setState({ isLoading });
  }
  onModalChange = showModal => {
    this.setState({ showModal });
  }

  setDate = () => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
      "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    const d = new Date();
    return monthNames[d.getMonth()] + " " + d.getFullYear();
  }
  getDataDashboard = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getDashboard/`, {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "salesofficerid=" + this.props.username + "&periodView=" + this.state.PeriodCode + "&userRole=" + this.props.userInfo.User.Role + "&StateDashboard=" + this.props.userInfo.User.Role
        + "&Channel=" + this.props.userInfo.User.Channel + "&ChannelSource=" + this.props.userInfo.User.ChannelSource + "&param=" + this.state.Param
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          fuModel: jsn.fuModel,
          isLoading: false
        })
      }
      )
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.getDataDashboard();
        }
      });


  };

  render() {
    return (
      <div>   
        <ToastContainer/>
        <Header titles="Personal Dashboard">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a 
              className="fa fa-envelope fa-lg"
              onClick={() => this.setState({ showModalFilter: true })}
               />
            </li>
          </ul>
        </Header>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            className="col-xs-12 panel-body-list"
            blocking={this.state.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >  <OtosalesModalDashboard
          showModal={this.state.showModalFilter}
          onModalChange={this.onModalChange}
          onCloseModal={this.onCloseModal}
          Role={this.props.userInfo.User.Role}
          Email={this.props.userInfo.User.Email}
          ReportTo={this.props.userInfo.ReportTo}
          EmailDefault={this.setEmailDefault()}
          SalesOfficer={this.props.userInfo.User.SalesOfficerID}
          PeriodCode={this.state.PeriodCode}
          Loading={this.onLoading}
        />
            <div className="">
        
              <div className="panel-heading" style={{ background: "#eaeaea" }}>
                <div class="row">
                  <div class="col-md-4 col-xs-6" style={{ float: "left" }}>
                    <p style={{ textAlign: "left", color: "#606060" }}><b>NEW PROSPECT</b></p>
                  </div>
                  <div class="col-md-4 col-xs-6" style={{ float: "right" }}>
                    <p style={{ textAlign: "right", color: "#606060" }} id="textDate"><b> {this.setDate()}</b></p>
                  </div>
                </div>
              </div>
              <div className="panel-body">
                {this.state.fuModel[0]!=null &&<OtosalesDashboardPersonal
                dataitems={this.state.fuModel}/>}
              </div>
            </div>

          </BlockUi>
        </div>
      </div>
    );
  }
}

export default withRouter(PageDashboardPersonal);
