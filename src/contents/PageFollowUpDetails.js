import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import {
  OtosalesSelect2,
  OtosalesDatePicker,
  OtosalesTimePicker,
  OtosalesListFollowUpHistory,
  OtosalesModalSenttoSA,
  Util
} from "../otosalescomponents";
import { API_URL, API_VERSION, API_VERSION_2, ACCOUNTDATA } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { ToastContainer, toast } from "react-toastify";
import Header from "../components/Header";

class PageFollowUpDetails extends Component {
  constructor(props) {
    super(props);
    var nextFU = Util.convertDate();
    var lastFU = null;
    if (
      JSON.parse(secureStorage.getItem("FUSelected")).NextFollowUpDate != null
    ) {
      nextFU = Util.convertDate(
        JSON.parse(secureStorage.getItem("FUSelected")).NextFollowUpDate + ""
      );
    }
    if (
      JSON.parse(secureStorage.getItem("FUSelected")).LastFollowUpDate != null
    ) {
      lastFU = Util.convertDate(
        JSON.parse(secureStorage.getItem("FUSelected")).LastFollowUpDate + ""
      );
    }
    this.state = {
      selectedQuotation: null,
      dataquotationhistory: [],
      showModal: false,
      isLoading: false,
      isErrorSave: false,
      messageError: "",
      isErrNextFUDateTime: false,
      isErrFUStatus: false,
      isErrFUStatusInfo: false,
      isErrLastFUPerson: false,
      FUStatus: JSON.parse(secureStorage.getItem("FUSelected")).FollowUpStatus,
      datafustatus: [],
      FollowUpEditStatus:
        parseInt(secureStorage.getItem("FollowUpEditStatus-statusInfo")) || "",
      FUStatusInfo: JSON.parse(secureStorage.getItem("FUSelected")).FollowUpInfo,
      oldFUStatusInfo: JSON.parse(secureStorage.getItem("FUSelected"))
        .FollowUpInfo,
      datafustatusinfo: [],
      lastFollowUpDate: lastFU,
      lastFollowUpTime: lastFU,
      nextFollowUpDate: nextFU,
      nextFollowUpTime: nextFU,
      lastFUPerson:
        JSON.parse(secureStorage.getItem("FUSelected")).FollowUpName || "",
      remarks: JSON.parse(secureStorage.getItem("FUSelected")).Remark || "",
      FollowUpNo: JSON.parse(secureStorage.getItem("FUSelected")).FollowUpNo,
      datafollowup: JSON.parse(secureStorage.getItem("FUSelected")),
      datas: [
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        }
      ],
      datastatusinfoall: []
    };
  }

  componentDidMount() {
    this.loadProspectCustomer(
      JSON.parse(secureStorage.getItem("FUSelected")).CustID
    );
    this.loadFUStatus(this.state.FUStatus, this.state.FollowUpEditStatus);
    if (
      this.state.FUStatus != null ||
      this.state.FUStatus != undefined ||
      this.state.FUStatus != ""
    ) {
      this.loadFUStatusInfo(this.state.FUStatus);
    }
    this.loadFUHistory(this.state.FollowUpNo);
    this.loadFUStatusInfoAll();
  }

  showQuotationHistory = FollowUpNo => {
    // console.log("showQuotationHistory followupno : ", FollowUpNo);
    this.setState({ dataquotationhistory: [], isLoading: true });
    if (FollowUpNo != null) {
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + FollowUpNo
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status != false && jsn.data.length > 0) {
            return jsn.data.map(data => ({
              ...data,
              ProspectName: JSON.parse(secureStorage.getItem("PCSelected")).Name,
              FollowUpNo: JSON.parse(secureStorage.getItem("FUSelected"))
                .FollowUpNo
            }));
          } else {
            return [];
          }
        })
        .then(datamapping => {
          this.setState(
            {
              dataquotationhistory: datamapping,
              selectedQuotation: datamapping[0].OrderNo || null,
              showModal: true
            },
            () => {
              if (this.state.dataquotationhistory.length > 0) {
                this.setState({ showModal: true });
              } else {
                // toast.dismiss();
                toast.warning(
                  "❗ There is no quotation for this prospect. Please make quotation first.",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                  }
                );
              }
              this.setState({ isLoading: false });
            }
          );
        })
        .catch(error => {
          console.log("parsing failed", error);
          // this.showQuotationHistory(FollowUpNo);
          this.setState({ isLoading: false });
        });
    } else {
      console.log("FollowUpNo tidak ada");
    }
  };

  loadFUStatus = (followupstatus, followupstatusactivity) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpStatus/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "followupstatus=" +
        followupstatus +
        "&followupstatusactivity=" +
        followupstatusactivity
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.StatusCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datafustatus: datamapping,
          isLoading: false
        });
        if (followupstatusactivity != null && followupstatusactivity != "") {
          this.setState({ FUStatus: followupstatusactivity }, () => {
            this.loadFUStatusInfo(this.state.FUStatus);
          });
          if (followupstatusactivity != followupstatus) {
            this.setState({ FUStatusInfo: "" });
          }
        }
        // this.onOptionFUStatusChange();
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatus(followupstatus);
        }
      });
  };

  loadFUStatusInfo = FUStatus => {
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpDetailsStatusInfo/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "statuscode=" + FUStatus
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.InfoCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        if (
          FUStatus == 6 &&
          secureStorage.getItem("FollowUpEditStatus-statusInfo") != undefined
        ) {
          this.setState({ FUStatusInfo: 27 });
        }

        if(FUStatus == 4 &&  secureStorage.getItem("FollowUpEditStatus-statusInfo") != undefined){
          this.setState({ FUStatusInfo: 30});
        }

        this.setState({
          datafustatusinfo:
            FUStatus == 6 &&
            secureStorage.getItem("FollowUpEditStatus-statusInfo") != undefined
              ? datamapping.filter(data => data.value == 27)
              : datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatusInfo(FUStatus);
        }
      });
  };

  loadFUHistory = FollowUpNo => {
    this.setState({ datas: [] });
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpHistory/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "followupno=" + FollowUpNo
    })
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        if (datamapping.length > 0) {
          this.setState({ datas: datamapping });
        }
        this.setState({
          isLoading: false
        });
        // console.log(datamapping);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUHistory(FollowUpNo);
        }
      });
  };

  loadFUStatusInfoAll = () => {
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/getStatusInfoDescriptionAll/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        })
      }
    )
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        this.setState({
          datastatusinfoall: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatusInfoAll();
        }
      });
  };

  loadProspectCustomer = custid => {
    this.setState({ datas: [] });
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getProspectCustomer/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "custid=" + custid
    })
      .then(res => res.json())
      .then(jsn => jsn.ProspectCustomer)
      .then(datamapping => {
        secureStorage.setItem("PCSelected", JSON.stringify(datamapping));
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadProspectCustomer(custid);
        }
      });
  };

  onOptionFUStatusChange = FUStatus => {
    this.setState({ FUStatus });
    this.setState({ FUStatusInfo: null });

    this.loadFUStatusInfo(FUStatus);
  };

  onOptionFUStatusInfoChange = FUStatusInfo => {
    this.setState({ FUStatusInfo });
    // console.log(FUStatusInfo);
  };

  onChangeFunction = (value, name) => {
    // console.log(value);
    this.setState({
      [name]: value
    });
  };

  onClickSave = () => {
    console.log("save");
    const state = this.state;

    this.setState({
      isErrNextFUDateTime: false,
      isErrFUStatus: false,
      isErrFUStatusInfo: false,
      isErrLastFUPerson: false
    });

    let errorField = "";

    if (
      state.nextFollowUpDate == "" ||
      state.nextFollowUpDate == null ||
      state.nextFollowUpTime == "" ||
      state.nextFollowUpTime == null
    ) {
      this.setState({ isErrNextFUDateTime: true });
      errorField += "{Next Follow Up}";
    }

    if (state.FUStatus == "" || state.FUStatus == null) {
      this.setState({ isErrFUStatus: true });
      errorField += "{Follow Up Status}";
    }

    if (state.FUStatusInfo == "" || state.FUStatusInfo == null) {
      this.setState({ isErrFUStatusInfo: true });
      errorField += "{Reason / Explanation}";
    }

    if (state.lastFUPerson == "" || state.lastFUPerson == null) {
      this.setState({ isErrLastFUPerson: true });
      errorField += "{Last Contact Person}";
    }

    if (errorField != "") {
      this.setState({
        isErrorSave: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("❗ Please fill all required data", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    let IsInfoChanged = true;

    if (state.oldFUStatusInfo == state.FUStatusInfo) {
      // toast.dismiss();toast.warning("❗ Nothing has changed information.", {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      // toast.dismiss();toast.info("Follow up details has been saved", {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      // return;
      IsInfoChanged = false;
    }

    this.setState({ isErrorSave: false, messageError: "" });

    if (state.FUStatus == 6) {
      if (
        state.datafollowup.IdentityCard == null ||
        state.datafollowup.IdentityCard == ""
      ) {
        toast.dismiss();
        toast.warning("❗ Please complete photo of KTP/KITAS/SIM first.", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }

      if (
        (state.datafollowup.STNK == null || state.datafollowup.STNK == "") &&
        (state.datafollowup.BSTB1 == null || state.datafollowup.BSTB1 == "")
      ) {
        toast.dismiss();
        toast.warning("❗ Please complete photo of STNK or BSTB first.", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }

      if (
        state.datafollowup.SPPAKB == null ||
        state.datafollowup.SPPAKB == ""
      ) {
        toast.dismiss();
        toast.warning("❗ Please complete photo of SPPAKB first.", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }

      if (state.FUStatusInfo == "28" || state.FUStatusInfo == 28) {
        toast.dismiss();
        toast.warning(
          "❗ You could not send document of this follow up because the policy status on follow up is already awaiting approval.",
          {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          }
        );
        return;
      }

      this.setState({ isLoading: true });
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/isValidatedSendToSA/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + state.FollowUpNo
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.vaildToSA == 1) {
            // toast.dismiss();toast.info("✔️ NEXT CHANGE STEP READ SEND TO SA", {
            //   position: "top-right",
            //   autoClose: 5000,
            //   hideProgressBar: false,
            //   closeOnClick: true,
            //   pauseOnHover: true,
            //   draggable: true
            // });
            this.setState({ isLoading: false });

            //////////// NEXT SEND TO SA
            this.showQuotationHistory(this.state.FollowUpNo);
          } else {
            toast.dismiss();
            toast.warning("❗ " + jsn.toast, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });

            this.setState({ isLoading: false });
          }
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.error("❗ Something error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    } else {
      // const FollowUpLastSend = JSON.stringify(this.state.datafollowup);

      const dateNextFollowUpSend =
        this.formatDate(this.state.nextFollowUpDate) +
        "T" +
        this.formatTimeFromDate(this.state.nextFollowUpTime);

      var FollowUpSend = {
        ...this.state.datafollowup,
        FollowUpInfo: this.state.FUStatusInfo,
        FollowUpName: this.state.lastFUPerson,
        FollowUpNo: this.state.FollowUpNo,
        FollowUpStatus: this.state.FUStatus,
        LastSeqNo: 0,
        Remark: this.state.remarks,
        NextFollowUpDate: dateNextFollowUpSend
      };

      // console.log(FollowUpSend);

      FollowUpSend = JSON.stringify(FollowUpSend);

      this.saveFollowUpHistory(FollowUpSend, IsInfoChanged);
    }
  };

  onClickSaveRev = () => {
    console.log("save");
    const state = this.state;

    this.setState({
      isErrNextFUDateTime: false,
      isErrFUStatus: false,
      isErrFUStatusInfo: false,
      isErrLastFUPerson: false
    });

    let errorField = "";

    if (state.FUStatus == "" || state.FUStatus == null) {
      this.setState({ isErrFUStatus: true });
      errorField += "{Follow Up Status}";
    }

    if (state.FUStatusInfo == "" || state.FUStatusInfo == null) {
      this.setState({ isErrFUStatusInfo: true });
      errorField += "{Reason / Explanation}";
    }

    if (state.lastFUPerson == "" || state.lastFUPerson == null) {
      this.setState({ isErrLastFUPerson: true });
      errorField += "{Last Contact Person}";
    }

    if (errorField != "") {
      this.setState({
        isErrorSave: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("❗ Please fill all required data", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (
      (this.state.FUStatus != null || this.state.FUStatus != "") &&
      (this.state.FUStatusInfo != null || this.state.FUStatusInfo != "") &&
      this.state.FUStatus == 1 &&
      this.state.lastFUPerson != ""
    ) {
      this.save(false);
    } else if (
      (this.state.FUStatus != null || this.state.FUStatus != "") &&
      (this.state.FUStatusInfo != null || this.state.FUStatusInfo != "") &&
      this.state.lastFUPerson != "" &&
      (state.nextFollowUpDate != "" ||
        state.nextFollowUpDate != null ||
        state.nextFollowUpTime != "" ||
        state.nextFollowUpTime != null)
    ) {
      if (this.state.FUStatus == 6) {
        this.isValidatedSendToSA(state.FollowUpNo);
      } else {
        this.save(false);
      }
    } else {
      toast.warning("❗ Please fill all required data", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }
  };

  save = isSentToSA => {
    if (
      this.state.datafollowup.FollowUpStatus == 9 &&
      this.state.datafollowup.FollowUpStatus != this.state.FUStatus
    ) {
      //////////////// hit MGOBitFeature STATE == MGOBID
      const dateNextFollowUpSend =
        this.formatDate(this.state.nextFollowUpDate) +
        "T" +
        this.formatTimeFromDate(this.state.nextFollowUpTime);

      var FollowUpSend = {
        ...this.state.datafollowup,
        // LastFollowUpDate: Util.convertDate(),
        FollowUpInfo: this.state.FUStatusInfo,
        FollowUpName: this.state.lastFUPerson,
        FollowUpNo: this.state.FollowUpNo,
        FollowUpStatus: this.state.FUStatus,
        LastSeqNo: 0,
        Remark: this.state.remarks,
        NextFollowUpDate: dateNextFollowUpSend
      };

      FollowUpSend = JSON.stringify(FollowUpSend);
      this.setState({ isLoading: true });

      fetch(
        `${API_URL + "" + API_VERSION_2}/DataReact/SaveFollowUpStatusDetails/`,
        {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization:
              "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "FollowUpLast=" +
            JSON.stringify(this.state.datafollowup) +
            "&FollowUp=" +
            FollowUpSend +
            "&isSentToSA=" +
            isSentToSA +
            "&STATE=MGOBID"
        }
      )
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.status == 1) {
            toast.info("✔️ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.warning("❗ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          secureStorage.setItem(
            "FollowUpSelected",
            JSON.stringify(datamapping.data)
          );
          this.setState({
            datafollowup: datamapping.data,
            lastFollowUpDate: Util.convertDate(datamapping.data.LastFollowUpDate + ""),
            lastFollowUpTime: Util.convertDate(datamapping.data.LastFollowUpDate + ""),
            nextFollowUpDate: Util.convertDate(datamapping.data.NextFollowUpDate + ""),
            nextFollowUpTime: Util.convertDate(datamapping.data.NextFollowUpDate + ""),
            FUStatus: datamapping.data.FollowUpStatus,
            oldFUStatusInfo: datamapping.data.FollowUpInfo,
            FUStatusInfo: datamapping.data.FollowUpInfo,
            lastFUPerson: datamapping.data.FollowUpName,
            remarks: datamapping.data.Remark,
            isLoading: false
          });

          secureStorage.setItem("FUSelected", JSON.stringify(datamapping.data));
          this.loadFUHistory(this.state.FollowUpNo);
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.error("❗ Something error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    } else {
      const dateNextFollowUpSend =
        this.formatDate(this.state.nextFollowUpDate) +
        "T" +
        this.formatTimeFromDate(this.state.nextFollowUpTime);

      var FollowUpSend = {
        ...this.state.datafollowup,
        FollowUpInfo: this.state.FUStatusInfo,
        FollowUpName: this.state.lastFUPerson,
        FollowUpNo: this.state.FollowUpNo,
        FollowUpStatus: this.state.FUStatus,
        LastSeqNo: 0,
        Remark: this.state.remarks,
        NextFollowUpDate: dateNextFollowUpSend
      };

      FollowUpSend = JSON.stringify(FollowUpSend);

      fetch(
        `${API_URL + "" + API_VERSION_2}/DataReact/SaveFollowUpStatusDetails/`,
        {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization:
              "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "FollowUpLast=" +
            JSON.stringify(this.state.datafollowup) +
            "&FollowUp=" +
            FollowUpSend +
            "&isSentToSA=" +
            isSentToSA +
            "&STATE="
        }
      )
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.status == 1) {
            toast.info("✔️ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.warning("❗ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          secureStorage.setItem(
            "FollowUpSelected",
            JSON.stringify(datamapping.data)
          );
          this.setState({
            datafollowup: datamapping.data,
            lastFollowUpDate: Util.convertDate(datamapping.data.LastFollowUpDate + ""),
            lastFollowUpTime: Util.convertDate(datamapping.data.LastFollowUpDate + ""),
            nextFollowUpDate: Util.convertDate(datamapping.data.NextFollowUpDate + ""),
            nextFollowUpTime: Util.convertDate(datamapping.data.NextFollowUpDate + ""),
            FUStatus: datamapping.data.FollowUpStatus,
            oldFUStatusInfo: datamapping.data.FollowUpInfo,
            FUStatusInfo: datamapping.data.FollowUpInfo,
            lastFUPerson: datamapping.data.FollowUpName,
            remarks: datamapping.data.Remark,
            isLoading: false
          });
          secureStorage.setItem("FUSelected", JSON.stringify(datamapping.data));
          this.loadFUHistory(this.state.FollowUpNo);
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.error("❗ Something error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    }
  };

  isValidatedSendToSA = async followupno => {
    const response = await fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/isValidatedSendToSArev/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + followupno
      }
    );
    const json = await response.json();

    if (json.vaildToSA == 0) {
      toast.warning("❗ " + json.toast, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    } else {
      //////////// NEXT SEND TO SA
      this.showQuotationHistory(this.state.FollowUpNo);
    }
  };

  onclickSendToSAValid = () => {
    this.setState({ showModal: false });
    const dateNextFollowUpSend =
      this.formatDate(this.state.nextFollowUpDate) +
      "T" +
      this.formatTimeFromDate(this.state.nextFollowUpTime);

    var FollowUpSend = {
      ...this.state.datafollowup,
      FollowUpInfo: this.state.FUStatusInfo,
      FollowUpName: this.state.lastFUPerson,
      FollowUpNo: this.state.FollowUpNo,
      FollowUpStatus: this.state.FUStatus,
      LastSeqNo: 0,
      Remark: this.state.remarks,
      NextFollowUpDate: dateNextFollowUpSend
    };

    // console.log(FollowUpSend);

    FollowUpSend = JSON.stringify(FollowUpSend);

    // this.saveFollowUpHistory(FollowUpSend, true, false);
    this.save(true);
  };

  saveFollowUpHistory = (
    FollowUpSend,
    IsInfoChanged = true,
    issendtosanot = true
  ) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/SaveFollowUpDetails/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "FollowUpLast=" +
        FollowUpSend +
        "&FollowUp=" +
        FollowUpSend +
        "&isInfoChanged=" +
        IsInfoChanged
    })
      .then(res => res.json())
      .then(jsn => (jsn.status == 1 ? jsn.data : null))
      .then(datamapping => {
        if (datamapping != null) {
          secureStorage.setItem("FUSelected", JSON.stringify(datamapping));
          secureStorage.setItem("FollowUpSelected", JSON.stringify(datamapping));

          this.setState({
            datafollowup: datamapping,
            lastFollowUpDate: Util.convertDate(datamapping.LastFollowUpDate + ""),
            lastFollowUpTime: Util.convertDate(datamapping.LastFollowUpDate + ""),
            nextFollowUpDate: Util.convertDate(datamapping.NextFollowUpDate + ""),
            nextFollowUpTime: Util.convertDate(datamapping.NextFollowUpDate + ""),
            FUStatus: datamapping.FollowUpStatus,
            oldFUStatusInfo: datamapping.FollowUpInfo,
            FUStatusInfo: datamapping.FollowUpInfo,
            lastFUPerson: datamapping.FollowUpName,
            remarks: datamapping.Remark,
            isLoading: false
          });
          // console.log(datamapping);
          secureStorage.setItem("FUSelected", JSON.stringify(datamapping));
          this.loadFUHistory(this.state.FollowUpNo);
          if (issendtosanot) {
            // toast.dismiss();
            toast.info("✔️ Follow Up Details has been changed", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
        } else {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.error("❗ Something error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  formatDate = dates => {
    var dd = dates.getDate();
    var mm = dates.getMonth() + 1; //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }

    return yyyy + "-" + mm + "-" + dd;
  };

  formatTimeFromDate = dates => {
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    var ss = dates.getMinutes();
    if (ss < 10) {
      ss = "0" + ss;
    }

    return hh + ":" + mm + ":" + ss;
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    var disabledFUSTATUS =
      secureStorage.getItem("FollowUpEditStatus-statusInfo") != undefined &&
      secureStorage.getItem("FollowUpEditStatus-statusInfo") != "" &&
      secureStorage.getItem("FollowUpEditStatus-statusInfo") != null
        ? true
        : false;
    return (
      <div>
        <Header titles="Follow Up Details">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-save fa-lg"
                onClick={() => this.onClickSaveRev()}
              />
            </li>
          </ul>
        </Header>
        <ToastContainer />
        <OtosalesModalSenttoSA
          onclickSendToSAValid={this.onclickSendToSAValid}
          showModal={this.state.showModal}
          closeModal={this.closeModal}
          FollowUpNo={this.state.FollowUpNo}
          showModal={this.state.showModal}
          dataquotationhistory={this.state.dataquotationhistory}
          selectedQuotation={this.state.selectedQuotation}
        />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            className="col-xs-12"
            blocking={this.state.isLoading}
            loader={
              <Loader active type="ball-spin-fade-loader" color="#02a17c" />
            }
            message="Loading, please wait"
          >
            <div className="panel-body panel-body-list" id="Datalist">
              <div className="form-group">
                <label>LAST FOLLOW UP * </label>
                <div className="row">
                  <div className="col-xs-6">
                    <div className="form-group">
                      <div className="input-group">
                        <OtosalesDatePicker
                          className="form-control"
                          name="txtdatesample"
                          selected={this.state.lastFollowUpDate}
                          dateFormat="dd-MM-yyyy"
                          readOnly={true}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-6">
                    <div className="form-group">
                      <div className="input-group">
                        <OtosalesTimePicker
                          selected={this.state.lastFollowUpTime}
                          className="form-control"
                          name="txtdatesample"
                          readOnly={true}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <label>NEXT REMINDER * </label>
                <div className="row">
                  <div className="col-xs-6">
                    <div
                      className={
                        this.state.isErrNextFUDateTime
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <div className="input-group">
                        <OtosalesDatePicker
                          className={
                            this.state.isErrNextFUDateTime
                              ? "form-control has-error"
                              : "form-control"
                          }
                          selected={this.state.nextFollowUpDate}
                          dateFormat="dd-MM-yyyy"
                          onChange={this.onChangeFunction}
                          name="nextFollowUpDate"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-6">
                    <div className="form-group">
                      <div className="input-group">
                        {}
                        <OtosalesTimePicker
                          selected={this.state.nextFollowUpTime}
                          className={
                            this.state.isErrNextFUDateTime
                              ? "form-control has-error"
                              : "form-control"
                          }
                          onChange={this.onChangeFunction}
                          name="nextFollowUpTime"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={
                  this.state.isErrFUStatus
                    ? "form-group has-error"
                    : "form-group"
                }
              >
                <label>FOLLOW UP STATUS * </label>
                <div className="input-group">
                  {/* <div className="input-group-addon ">
                      <i className="fa fa-pencil" />
                    </div> */}
                  <OtosalesSelect2
                    isDisabled={disabledFUSTATUS}
                    error={this.state.isErrFUStatus}
                    value={this.state.FUStatus}
                    selectOption={this.state.FUStatus}
                    onOptionsChange={this.onOptionFUStatusChange}
                    options={this.state.datafustatus}
                  />
                </div>
              </div>
              <div
                className={
                  this.state.isErrFUStatusInfo
                    ? "form-group has-error"
                    : "form-group"
                }
              >
                <label>REASON / EXPLANATION * </label>
                <div className="input-group">
                  {/* <div className="input-group-addon ">
                      <i className="fa fa-pencil" />
                    </div> */}
                  <OtosalesSelect2
                    error={this.state.isErrFUStatusInfo}
                    value={this.state.FUStatusInfo}
                    selectOption={this.state.FUStatusInfo}
                    onOptionsChange={this.onOptionFUStatusInfoChange}
                    options={this.state.datafustatusinfo}
                    isDisabled={
                      (secureStorage.getItem("FollowUpEditStatus-statusInfo") == 1 || secureStorage.getItem("FollowUpEditStatus-statusInfo") == 4)
                        ? true
                        : false
                    }
                  />
                </div>
              </div>
              <div
                className={
                  this.state.isErrLastFUPerson
                    ? "form-group has-error"
                    : "form-group"
                }
              >
                <label>LAST CONTACT PERSON * </label>
                <div className="input-group">
                  {/* <div className="input-group-addon ">
                      <i className="fa fa-pencil" />
                    </div> */}
                  <input
                    className="form-control"
                    name="lastFUPerson"
                    value={this.state.lastFUPerson}
                    onChange={event =>
                      this.onChangeFunction(
                        event.target.value,
                        event.target.name
                      )
                    }
                  />
                </div>
              </div>
              <div className="form-group">
                <label>NOTES </label>
                <div className="input-group">
                  {/* <div className="input-group-addon ">
                      <i className="fa fa-pencil" />
                    </div> */}
                  <textarea
                    className="form-control"
                    name="remarks"
                    value={this.state.remarks}
                    onChange={event =>
                      this.onChangeFunction(
                        event.target.value,
                        event.target.name
                      )
                    }
                  />
                </div>
              </div>
              <div className="form-group">
                <legend>FOLLOW UP HISTORY</legend>
                <div className="row">
                  <OtosalesListFollowUpHistory
                    name="sampleList"
                    rowscount={this.state.datas.length}
                    dataitems={this.state.datas}
                    datastatusinfoall={this.state.datastatusinfoall}
                  />
                </div>
              </div>
            </div>
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageFollowUpDetails);
