import React, { Component } from "react";
import Footer from "../../../components/Footer";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { OtosalesModalAlert, Util, Log } from "../../../otosalescomponents";
import { A2isTextArea } from "../../../components/a2is";
// import { API_URL, API_VERSION } from "../../../config";
import * as Config from "../../../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Modal from "react-responsive-modal";
import Header from "../../../components/Header";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";

class PageOrderApprovalDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showModalAlertRevise: false,
      remarksAlertRevise: "",
      datas: null,
      dataSendTo: null,
      datasobject: null,
      isLoading: false,
      isDetailFound: true, // To Flag if the Detail Founded or Not 
      donehitOrderApprovalDetail: false,
      donehitOrderApprovalDetailObject: false,
      doneHitOrderApprovalAdjustmentSendAddress: false,
      OrderID: "",
      Name: "",
      OrderNo: "",
      year: [],
      objects: [],
      interests: [],
      allObjects: [],
      remarksrevise: "",
      type: "",
      Role: "",
      ApprovalType: "",
      ErrType: 0,
      ErrorCodeStr: ""
    };
  }

  onOpenModal = type => {
    // console.log(type);
    this.setState({ showModal: true, type: type });
  };

  onCloseModal = () => {
    this.setState({ showModal: false });
  };

  onApprove = () => {
    // this.ProcessOrderApproval("Approve", "");
    if(this.state.ApprovalType == "PMT1" || this.state.ApprovalType == "PMT2"){
      this.ProcessApprovalPayment("Approve", "");
    }else{
      this.ProcessOrderApprovalV2("Approve", ""); 
    }
  };

  onReject = () => {
    // this.ProcessOrderApproval("Reject", "");
    if(this.state.ApprovalType == "PMT1" || this.state.ApprovalType == "PMT2"){
      this.ProcessApprovalPayment("Reject", "");
    }else{
      // this.ProcessOrderApprovalV2("Reject", this.textInput.value);
      this.ProcessOrderApprovalV2("Reject", "");
    }
  };

  onSubmitRemarksRevise = event => {
    event.preventDefault();
    if (this.textInput.value == "") {
      toast.dismiss();
      toast.warning("❗ Please Input Remarks for SA", {
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }
    // console.log(this.textInput.value);
    this.setState({
      showModalAlertRevise: true,
      remarksAlertRevise: this.textInput.value+""
    });

    // this.ProcessOrderApproval("Revise", this.textInput.value);
  };

  ProcessOrderApprovalV2 = (approvalType, remarks) => {
    this.setState({ isLoading: true });
    var routeAPI = "/";
    var param = {nothing: "nothing"};
    switch(approvalType) {
      case "Approve": {
        routeAPI = "/approveOrderApproval";
        param = {
          OrderId: this.state.OrderId,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType
        };
        break;
      }
      case "Reject": {
        routeAPI = "/rejectOrderApproval";
        param = {
          OrderId: this.state.OrderId,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType,
          Remark: remarks
        };
        break;
      }
      case "Revise": {
        routeAPI = "/reviseOrderApproval";
        param = {
          OrderId: this.state.OrderId,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType,
          Remark: remarks
        };
        break;
      }
    }

    Util.fetchAPI(`${Config.API_URL + "" + Config.API_V2}/OrderApproval` + routeAPI, "POST",
      { // Header
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
      }, 
      // Body
      param
    )
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          isLoading: false
        });
        if (datamapping.Status == 1) {
          secureStorage.setItem(
            "orderapprovalnotif",
            "success-" + datamapping.Message
          );
          toast.dismiss();
          toast.success("✔️ " + datamapping.Message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          if (datamapping.ResponseCode == "666") {
            Log.debugStr("Something Happens => ", datamapping.Message);
          }
          secureStorage.setItem(
            "orderapprovalnotif",
            "failed-" + datamapping.Message
          );
          toast.dismiss();
          toast.warning("❗ " + (datamapping.ResponseCode == "666"? "Something Wrong Happen":datamapping.Message), {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.props.history.push("/orderapproval");
        secureStorage.removeItem("OrderIdOA");
        secureStorage.removeItem("OrderNoOA");
        secureStorage.removeItem("IsSOOA");
      })
      .catch(error => {
        // console.log("parsing failed", error);
        Log.error("Error on ProcessOrderApprovalV2 (route " + routeAPI + ") => ", error)
        toast.dismiss();
        toast.error("❗ Something error", {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        this.setState({
          isLoading: false
        });
      });

  }

  ProcessOrderApproval = (submitOrder, remarks) => {
    this.setState({ isLoading: true });
    fetch(
      `${Config.API_URL + "" + Config.API_VERSION}/Synchronization/ProcessOrderApproval/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
        }),
        body:
          "OrderId=" +
          this.state.OrderID +
          "&OrderNo=" +
          this.state.OrderNo +
          "&userID=" +
          this.props.username +
          "&submitOrder=" +
          submitOrder +
          "&remarksOrder=" +
          remarks
      }
    )
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          isLoading: false
        });
        if (datamapping.Status == 1) {
          secureStorage.setItem(
            "orderapprovalnotif",
            "success-" + datamapping.Message
          );
          toast.dismiss();
          toast.success("✔️ " + datamapping.Message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          secureStorage.setItem(
            "orderapprovalnotif",
            "failed-" + datamapping.Message
          );
          toast.dismiss();
          toast.warning("❗ " + datamapping.Message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.props.history.push("/orderapproval");
        secureStorage.removeItem("NameOA");
        secureStorage.removeItem("OrderIdOA");
        secureStorage.removeItem("OrderNoOA");
        secureStorage.removeItem("IsSOOA");
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ Something error", {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        // this.props.history.push("/orderapproval");
        // secureStorage.removeItem("NameOA");
        // secureStorage.removeItem("OrderIdOA");
        // this.ProcessOrderApproval(submitOrder);
      });
  };

  ProcessApprovalPayment = (approvalType, remarks) => {
    this.setState({ isLoading: true });
    var routeAPI = "/ProcessApprovalPayment";
    var param = {nothing: "nothing"};
    switch(approvalType) {
      case "Approve": {
        param = {
          ActionType: 1,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType
        };
        break;
      }
      case "Reject": {
        param = {
          ActionType: 2,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType,
          Remark: remarks
        };
        break;
      }
      case "Revise": {
        param = {
          ActionType: 2,
          OrderNo: this.state.OrderNo,
          UserId: this.props.username,
          ApprovalType: this.state.ApprovalType,
          Remark: remarks
        };
        break;
      }
    }

    // Util.fetchAPI(`${Config.API_URL + "" + Config.API_V2}/OrderApproval` + routeAPI, "POST",
    Util.fetchAPI(`${Config.API_URL + "" + Config.API_VERSION_0090URF2020}/DataReact` + routeAPI, "POST",
      { // Header
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
      }, 
      // Body
      param
    )
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          isLoading: false
        });
        if (datamapping.status == 1) {
          secureStorage.setItem(
            "orderapprovalnotif",
            "success-" + datamapping.data
          );
          toast.dismiss();
          toast.success("✔️ " + datamapping.message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          if (datamapping.ResponseCode == "666") {
            Log.debugStr("Something Happens => ", datamapping.message);
          }
          secureStorage.setItem(
            "orderapprovalnotif",
            "failed-" + datamapping.message
          );
          toast.dismiss();
          toast.warning("❗ " + (datamapping.ResponseCode == "666"? "Something Wrong Happen":datamapping.message), {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.props.history.push("/orderapproval");
        secureStorage.removeItem("OrderIdOA");
        secureStorage.removeItem("OrderNoOA");
        secureStorage.removeItem("IsSOOA");
      })
      .catch(error => {
        // console.log("parsing failed", error);
        Log.error("Error on ProcessOrderApprovalV2 (route " + routeAPI + ") => ", error)
        toast.dismiss();
        toast.error("❗ Something error", {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        this.setState({
          isLoading: false
        });
      });

  }

  closeAlertRevise = () => {
    this.setState({ showModalAlertRevise: false });
  };

  onSubmitAlertRevise = () => {
    this.setState({
      showModalAlertRevise: false,
      showModal: false
    });
    // this.ProcessOrderApproval("Revise", this.textInput.value);
    if(this.state.ApprovalType == "PMT1" || this.state.ApprovalType == "PMT2"){
      this.ProcessApprovalPayment("Revise", this.state.remarksAlertRevise);
    }else{
      this.ProcessOrderApprovalV2("Revise", this.state.remarksAlertRevise);
    }
  };

  componentDidMount() {
    try {
      const OrderID = secureStorage.getItem("OrderIdOA").trim();
      const OrderNo = secureStorage.getItem("OrderNoOA").trim();
      const ApprovalType = secureStorage.getItem("ApprovalType").trim();
      const Role = (JSON.parse(Config.ACCOUNTDATA)).UserInfo.User.Role;
      Log.debugStr("Check OrderNo => " + OrderNo);
      Log.debugStr("Check OrderID => " + OrderID);
      Log.debugStr("Check Role => " + Role);
      Log.debugStr("Check ApprType => " + ApprovalType);

      if (Util.isNullOrEmpty(OrderID) || Util.isNullOrEmpty(OrderNo) || Util.isNullOrEmpty(ApprovalType) ) {
        Log.debugStr("OrderID => " + Util.isNullOrEmpty(OrderID));
        Log.debugStr("OrderNo => " + Util.isNullOrEmpty(OrderNo));
        Log.debugStr("ApprovalType => " + Util.isNullOrEmpty(ApprovalType));
        this.props.history.push("/");
      }

      this.setState({ OrderID, OrderNo, ApprovalType, Role }, ()=>{
        this.searchDataHitApi(OrderID, OrderNo);
        if (ApprovalType.trim() != "NEXTLMT") { 
          this.orderobjectApi(OrderNo);

          if (ApprovalType.trim() == "ADJUST") {
            this.getPolicySendToApi(OrderNo);
          }else {
            this.setState({doneHitOrderApprovalAdjustmentSendAddress: true});
          }
        } else {
            this.setState({donehitOrderApprovalDetailObject: true,
                            doneHitOrderApprovalAdjustmentSendAddress: true});
        }
      });

        
    } catch (error) {
      Log.error("Something wrong happens: ", error);
      this.props.history.push("/");
    }
  }

  getPolicySendToApi = OrderNo => {

    this.setState({
      datasobject: [],
      isLoading: true,
      donehitOrderApprovalDetailObject: false
    }, ()=>{
        Util.fetchAPI(
          `${Config.API_URL + "" + Config.API_V2}/OrderApproval/fetchPolicySendAddress/`,
          "POST",
          new Headers({
              "Content-Type": "application/x-www-form-urlencoded",
              Authorization:
                "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
            }),
          {OrderNo: OrderNo}
        )
          .then(res => res.json())
          .then(jsn => {
            if(jsn.Status == 1) {
              return jsn.Data;
            } else {
              throw jsn.Message;
            }
          }
          )
          .then(datamapping => {
            let tmpDoneHitOrderApprovalAdjustmentSendAddress = true;
            this.setState({
              dataSendTo: datamapping,
              isLoading:
                !this.state.donehitOrderApprovalDetail ||
                !this.state.donehitOrderApprovalDetailObject || 
                !tmpDoneHitOrderApprovalAdjustmentSendAddress,
              doneHitOrderApprovalAdjustmentSendAddress: tmpDoneHitOrderApprovalAdjustmentSendAddress
            });

            // console.log("data mapping");
            // console.log(datamapping);
            // this.loadOrderObjects(datamapping);
          })
          .catch(error => {
            Log.error("PageOrderApprovalDetail on getPolicySendToApi: ", error);
            let tmpDoneHitOrderApprovalAdjustmentSendAddress = true;
            this.setState({
              isLoading:
                !this.state.donehitOrderApprovalDetail ||
                !this.state.donehitOrderApprovalDetailObject || 
                !tmpDoneHitOrderApprovalAdjustmentSendAddress,
              doneHitOrderApprovalAdjustmentSendAddress: tmpDoneHitOrderApprovalAdjustmentSendAddress
            });
            if(!(error+"".toLowerCase().includes("token"))){
              // this.orderobjectApi(OrderNo);
            }
          });
    });
  }

  searchDataHitApi = (OrderID, OrderNo) => {
    this.setState({
      datas: [],
      donehitOrderApprovalDetail: false,
      isLoading: true
    });
    var tmpRole = this.state.Role;
    if (this.state.ApprovalType == "COMSAMD") {
      tmpRole = (JSON.parse(Config.ACCOUNTDATA).UserInfo.User.SalesOfficerID.length > 3)? "MGO": "AO";
    } else if (this.state.ApprovalType == "KOMISI") {
      tmpRole = "MGO";
    }
    var rawjson = "";
    // Util.fetchAPI(`${Config.API_URL + "" + Config.API_V2}/OrderApproval/fetchDetailApproval`,
    // Util.fetchAPI(`${Config.API_URL + "" + Config.API_V0213URF2019}/datareact/fetchDetailApproval`,
    Util.fetchAPI(`${Config.API_URL + "" + Config.API_VERSION_0090URF2020}/datareact/fetchDetailApproval`,
      "POST",
      { // Headers
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
      },
      { // Body
        OrderID: OrderID,
        OrderNo: OrderNo,
        ApprovalType: this.state.ApprovalType,
        Role: tmpRole
      }
     )
      .then(res => res.json())
      .then(jsn => {
        Log.debugStr("Status Response = " + jsn.Status == 1);
        var isSuccess = false;
        if (jsn.Status != undefined && jsn.Status == 1) {
          if (jsn.Data != null && jsn.Data != undefined) {
            isSuccess = true;
            return jsn.Data;
          } else {
            this.setState({isDetailFound: false, ErrType: 1});
            Log.debugStr("Error on searchDataHitApi with Response => ", jsn);
          }
        } else {
          if (jsn.ResponseCode == "6661") {
            this.setState({isDetailFound: false, ErrType: 1});
          } else {
            Log.debugStr("Error on searchDataHitApi with Response => ", jsn);
          }
        }
        let tmpDonehitOrderApprovalDetail = true;
        rawjson = jsn;
        var errcodestr = Util.generateErrorEncodedCode({
                          url: `${Config.API_URL + "" + Config.API_V0213URF2019}/datareact/fetchDetailApproval`,
                          error: "Response Code server = "+jsn.ResponseCode,
                          data: rawjson
                        });
        this.setState({
          isLoading:
            !tmpDonehitOrderApprovalDetail ||
            !this.state.donehitOrderApprovalDetailObject ||
            !this.state.doneHitOrderApprovalAdjustmentSendAddress,
          donehitOrderApprovalDetail: tmpDonehitOrderApprovalDetail,
          isDetailFound: false,
          ErrorCodeStr: errcodestr,
          ErrType: 666
        });
      })
      .then(datamapping => {
        Log.debugGroup("Datamapping = ", datamapping);
        let tmpDonehitOrderApprovalDetail = true;
        this.setState({
          datas: datamapping,
          isLoading:
            !tmpDonehitOrderApprovalDetail ||
            !this.state.donehitOrderApprovalDetailObject || 
            !this.state.doneHitOrderApprovalAdjustmentSendAddress,
          donehitOrderApprovalDetail: tmpDonehitOrderApprovalDetail
        });
      })
      .catch(error => {
        Log.error("Error on searchDataHitApi at PageOrderApprovalDetail => ", error);
        let tmpDonehitOrderApprovalDetail = true;
        var errcodestr = Util.generateErrorEncodedCode({
                          url: `${Config.API_URL + "" + Config.API_V0213URF2019}/datareact/fetchDetailApproval`,
                          error: error.message,
                          data: 
                                { // Body
                                  OrderID: OrderID,
                                  OrderNo: OrderNo,
                                  ApprovalType: this.state.ApprovalType,
                                  Role: tmpRole
                                }
                        });
        this.setState({
          isLoading:
            !tmpDonehitOrderApprovalDetail ||
            !this.state.donehitOrderApprovalDetailObject ||
            !this.state.doneHitOrderApprovalAdjustmentSendAddress,
          donehitOrderApprovalDetail: tmpDonehitOrderApprovalDetail,
          isDetailFound: false,
          ErrorCodeStr: errcodestr,
          ErrType: 666
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  };

  orderobjectApi = OrderNo => {
    this.setState({
      datasobject: [],
      isLoading: true,
      donehitOrderApprovalDetailObject: false
    });
    Util.fetchAPI(
      // `${Config.API_URL + "" + Config.API_VERSION}/Replication/OrderApprovalDetailObject/`,
      `${Config.API_URL + "" + Config.API_V2}/OrderApproval/fetchDetailObjectApproval`,
      "POST",
      new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(Config.ACCOUNTDATA).Token
        }),
      {OrderNo: OrderNo}
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == 1) {
          return jsn.data;
        } else {
          let tmpDonehitOrderApprovalDetailObject = true;
          this.setState({
            isLoading:
              !this.state.donehitOrderApprovalDetail ||
              !tmpDonehitOrderApprovalDetailObject || 
              !this.state.doneHitOrderApprovalAdjustmentSendAddress,
            donehitOrderApprovalDetailObject: tmpDonehitOrderApprovalDetailObject,
            isDetailFound: false,
            ErrType: 1
          });
        }
      })
      .then(datamapping => {
        let tmpDonehitOrderApprovalDetailObject = true;
        this.setState({
          datasobject: datamapping,
          isLoading:
            !this.state.donehitOrderApprovalDetail ||
            !tmpDonehitOrderApprovalDetailObject || 
            !this.state.doneHitOrderApprovalAdjustmentSendAddress,
          donehitOrderApprovalDetailObject: tmpDonehitOrderApprovalDetailObject
        });

        // console.log("data mapping");
        // console.log(datamapping);
        this.loadOrderObjects(datamapping);
      })
      .catch(error => {
        Log.error("parsing orderobjectApi failed", error);
        let tmpDonehitOrderApprovalDetailObject = true;
        var errcodestr = Util.generateErrorEncodedCode({
                          url: `${Config.API_URL + "" + Config.API_V2}/OrderApproval/fetchDetailObjectApproval`,
                          error: error.message,
                          data: {OrderNo: OrderNo}
                        });
        this.setState({
          isLoading:
            !this.state.donehitOrderApprovalDetail ||
            !tmpDonehitOrderApprovalDetailObject ||
            !this.state.doneHitOrderApprovalAdjustmentSendAddress,
          donehitOrderApprovalDetailObject: tmpDonehitOrderApprovalDetailObject,
          isDetailFound: false,
          ErrorCodeStr: errcodestr,
          ErrType: 666
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.orderobjectApi(OrderNo);
        }
      });
  };

  loadOrderObjects = datamapping => {
    if (datamapping === undefined) {
      toast.dismiss();
      toast.warning("❗ Something error, Network Connection is Not Connected", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }

    if (datamapping === null) {
      toast.dismiss();
      toast.warning(
        "❗ Something error, The server is down or the network connection is not connected to the Internet.",
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        }
      );
    } else {
      datamapping.map(data => {
        var date = Util.convertDate(data.Begin_Date);
        if (!this.state.year.includes(date.getFullYear() + "")) {
          this.setState({
            year: [...this.state.year, date.getFullYear() + ""]
          });
        }

        if (!this.state.objects.includes(data.Object_No)) {
          this.setState({ objects: [...this.state.objects, data.Object_No] });
        }

        if (!this.state.interests.includes(data.Interest_Code)) {
          this.setState({
            interests: [...this.state.interests, data.Interest_Code]
          });
        }
      });

      this.state.year.map(year => {
        var objectYearly = {
          Year: "",
          Object: []
        };
        Log.debugStr("START MAPPING YEAR ["+year+"]");

        this.state.objects.map((object, index) => {
          var objectVehicle = {
            ObjectName: "",
            PoliceNumber: "",
            Year: "",
            Interests: []
          };
          Log.debugGroupCollapsed("START MAPPING YEAR["+year+"] OBJECT ["+index+"] => ", object);

          this.state.interests.map(interest => {
            var objectInterest = {
              InterestName: "",
              SumInsured: "",
              Coverages: []
            };

            datamapping.map(data => {
              var date = Util.convertDate(data.Begin_Date);
              if (
                year == date.getFullYear() + "" &&
                data.Object_No == object &&
                data.Interest_Code == interest
              ) {
                Log.debugGroupCollapsed("START MAPPING YEAR["+year+"] OBJECT ["+index+"] INTEREST["+interest+"] => ", data);
                //////////////Order Approval 350
                let objectCoverage = {
                  Coverage: "",
                  Premium: "",
                  LoadingRate: ""
                };

                if (data.LoadingRate != 0) {
                  let objectCoveragetemp = {
                    Coverage: "Loading",
                    Premium: "",
                    LoadingRate: data.LoadingRate
                  };
                  objectCoverage = objectCoveragetemp;

                  let objectInteresttemp = {
                    InterestName: { ...objectInterest.InterestName },
                    SumInsured: { ...objectInterest.SumInsured },
                    Coverages: [...objectInterest.Coverages, objectCoverage]
                  };
                  objectInterest = objectInteresttemp;
                }

                objectCoverage = {
                  Coverage: data.Coverage_Description,
                  Premium: data.Cover_Premium,
                  LoadingRate: ""
                };

                let objectInteresttemp = {
                  InterestName: data.Interest_Description,
                  SumInsured: data.Sum_Insured,
                  Coverages: [...objectInterest.Coverages, objectCoverage]
                };
                objectInterest = objectInteresttemp;

                var objectVehicletemp = {
                  ObjectName: data.VehicleName,
                  PoliceNumber: data.PoliceNumber,
                  Year: data.Year,
                  Interests: [...objectVehicle.Interests]
                };

                objectVehicle = objectVehicletemp;

                var objectYearlytemp = {
                  Year: Util.convertDate(data.Begin_Date).getFullYear() + "",
                  Object: []
                };

                objectYearly = objectYearlytemp;
              }
            });

            // if (objectInterest.InterestName != null) { ==> WHY THIS IS NULL ONLY!!!
              if (!Util.isNullOrEmpty(objectInterest.InterestName)) {
              var objectVehicleTemp = {
                ObjectName: objectVehicle.ObjectName,
                PoliceNumber: objectVehicle.PoliceNumber,
                Year: objectVehicle.Year,
                Interests: [...objectVehicle.Interests, objectInterest]
              };

              objectVehicle = objectVehicleTemp;
            }
          });

          var objectYearlytemp = {
            Year: objectYearly.Year,
            Object: [...objectYearly.Object, objectVehicle]
          };

          objectYearly = objectYearlytemp;
        });
        this.setState({ allObjects: [...this.state.allObjects, objectYearly] });
      });

      // console.log(this.state.year);
      // console.log(this.state.objects);
      // console.log(this.state.interests);
      // console.log(this.state.allObjects);
      // secureStorage.setItem("allObjects", JSON.stringify(this.state.allObjects));
    }
  };

  PolicySendTo = (props) => {
    Log.debugGroup("DataSendTo on PolicySendTo => ", props.data);
    if (Util.isNullOrEmpty(props.data.PolicySentToDes) ||
        Util.isNullOrEmpty(props.data.Address) 
      ) {
      return ("");
    } else 
    return (
        <div>
            <div className="row separatorTop backgroundgrey">
              <h4 style={{ marginLeft: "15px", marginTop: "15px" }}>
                <b>ALAMAT PENGIRIMAN POLICY</b>
              </h4>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12 col-md-5">
                <p>Dikirim ke</p>
              </div>
              <div className="col-xs-12 col-md-5 pull-right">
                <strong>{props.data.PolicySentToDes}</strong>
              </div>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12 col-md-5">
                <p>Nama</p>
              </div>
              <div className="col-xs-12 col-md-5 pull-right">
                <strong>{ Util.isNullOrEmpty(props.data.NAME)?"":props.data.NAME }</strong>
              </div>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12 col-md-5">
                <p>Alamat</p>
              </div>
              <div className="col-xs-12 col-md-5 pull-right">
                <strong>{ props.data.Address }</strong>
              </div>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12 col-md-5">
                <p>Kode Pos</p>
              </div>
              <div className="col-xs-12 col-md-5 pull-right">
                <strong>{ Util.isNullOrEmpty(props.data.Postal)?"":props.data.Postal }</strong>
              </div>
            </div>
            <div className="row separatorTop">
              &nbsp;
            </div>
        </div>
      );

  }

  ViewCommAdjstApproval = props => {
    try {
      let datas = props.data;
      let datasendto = props.datasendto;
      let detailsobject = props.detailsobject;
      let ApprovalType = this.state.ApprovalType
      let UserName = JSON.parse(Config.ACCOUNTDATA)
      // if(datasendto == null && ApprovalType == "ADJUST") {
      //   return("");
      // } else 
      Log.debugGroup("DataSendTo on ViewCommAdjstApproval => ", datasendto);
      Log.debugGroup("Datas on ViewCommAdjstApproval => ", datas);
      return (
              <div
                className="panel panel-default"
                style={{ paddingBottom: "10px" }}
              >
                <div className="panel-heading">
                  {
                    Util.isNullOrEmpty(datas.Name_On_Policy)?
                      <h3 className="panel-title">Order Approval Details</h3>

                    :

                      <h3 className="panel-title"> {datas.Name_On_Policy} </h3>
                  }
                </div>
                <div className="panel-body">
                    { // TODO LCY: fix this into real APPROVAL TYPE
                      (datas.isPTSB != null && datas.isPTSB == 1) ?
                        <React.Fragment>
                          <div className="row">
                            <div className="col-xs-12">
                              <h5><input type="checkbox" checked="true" enabled="false"/> POLIS TERBIT SEBELUM BAYAR</h5>
                            </div>
                          </div>
                          
                          <div className="row">
                            <div className="col-xs-12">
                              <p>REMARKS</p>
                            </div>
                            <div className="col-xs-12">
                              {
                                (!Util.isNullOrEmpty(datas.Remarks))?
                                <textarea name="" id="" cols="30" rows="5" enabled="false" style={{width: '100%', fontWeight: '700' }}>
                                  {datas.Remarks}
                                </textarea>
                                :""
                              }
                            </div>
                          </div>
                        </React.Fragment>
                        :""
                    }

                    { (ApprovalType == "PMT1" || ApprovalType == "PMT2") &&
                      <div className="row">
                        <div className="col-xs-12">
                          <h5><input type="checkbox" checked="true" enabled="false"/> <b>Installment Payment Adjustment</b></h5>
                        </div>
                      </div>
                    }

                  <div className={(datas.isPTSB != null && datas.isPTSB == 1)?"row separatorTop": "row"}>
                    <div className="col-xs-5">
                    MARKETING
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>
                        {
                          (
                            Util.isNullOrEmpty(datas.MarketingName)?
                            "-":datas.MarketingName
                          )
                        }
                        </strong>
                      </p>
                    </div>
                  </div>

                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      BRANCH
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>
                        {
                          (
                            Util.isNullOrEmpty(datas.BranchName)?
                            "-":datas.BranchName
                          )
                        }
                        </strong>
                      </p>
                    </div>
                  </div>

                  <div className="row separatorTop">
                    <div className="col-xs-5">
                    {
                      (Util.isNullOrEmpty(UserName))?"":
                      ( UserName.length > 3 || ApprovalType == "KOMISI"? 
                      <p>AGENT</p> : <p>PAY TO</p> )
                    }
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>
                        {
                          (Util.isNullOrEmpty(UserName))?"":
                          ( 
                            (UserName.length > 3 || ApprovalType == "KOMISI")? 
                            (Util.isNullOrEmpty(datas.AgentCode)? "" : datas.AgentCode) + 
                            " " +
                            (Util.isNullOrEmpty(datas.AgentName)? "" : datas.AgentName) 
                            :
                            (Util.isNullOrEmpty(datas.PayToCode)? "" : datas.PayToCode) + 
                            " " +
                            (Util.isNullOrEmpty(datas.PayToName)? "" : datas.PayToName) 
                          )
                        }
                        </strong>
                      </p>
                    </div>
                  </div>
                  {
                    !Util.isNullOrEmpty(datas.Dealer)
                    ? 
                    <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>DEALER</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>
                          {datas.Dealer != null && datas.Dealer != ""
                            ? datas.Dealer
                            : ""}
                        </strong>
                      </p>
                    </div>
                  </div>
                    : ""
                  }
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                    {
                      (Util.isNullOrEmpty(UserName))?"":
                      ( UserName.length > 3 || ApprovalType == "KOMISI"? 
                      <p>UPLINER</p> : <p>ACCOUNT INFO</p> )
                    }
                    </div>
                    {
                          (Util.isNullOrEmpty(UserName))?"":
                          ( UserName.length > 3 || ApprovalType == "KOMISI"? 
                            <div className="col-xs-7">
                              <p className="text-right">
                                <strong>
                                  {
                                    (Util.isNullOrEmpty(datas.UplinerCode)?"":datas.UplinerCode) + 
                                    " " + 
                                    (Util.isNullOrEmpty(datas.UplinerName)?"":datas.UplinerName)
                                  }
                                </strong>
                              </p>
                            </div> 
                          : 
                            <div className="col-xs-7">
                              <p className="text-right">
                                <strong>
                                  {
                                    (Util.isNullOrEmpty(datas.Account_Info_BankName)?"":datas.Account_Info_BankName) +
                                    " " +
                                    (Util.isNullOrEmpty(datas.Account_Info_AccountNo)?"":datas.Account_Info_AccountNo)
                                  }
                                  {
                                    (Util.isNullOrEmpty(datas.Account_Info_AccountHolder))?"":<br/>
                                  }
                                  {
                                    (Util.isNullOrEmpty(datas.Account_Info_AccountHolder)?"": ("an " + datas.Account_Info_AccountHolder))
                                  }
                                </strong>
                              </p>
                            </div>
                          )
                    }
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>POLICY NUMBER</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{datas.Policy_No}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>PRODUCT</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{datas.Description}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>GROSS PREMIUM</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{Util.formatMoney(datas.GrossPremium, 2)}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>DISCOUNT</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{Util.formatMoney(datas.Discount,2)}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>PREMIUM</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{Util.formatMoney(datas.Premium, 2)}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>ADMIN FEE</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{Util.formatMoney(datas.AdminFee, 2)}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>TOTAL PREMIUM</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>{Util.formatMoney(datas.TotalPremium, 2)}</strong>
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>TOTAL COMMISSION ({Util.formatMoney(datas.CommissionPercentage,2)}%)</p>
                    </div>
                    <div className="col-xs-7">
                      <p className="text-right">
                        <strong>
                          {Util.formatMoney(datas.TotalCommission, 2)}
                        </strong>
                      </p>
                    </div>
                  </div>
                  {detailsobject}
                  {
                    (datasendto == null)?
                    "":
                    <this.PolicySendTo
                        data={datasendto}
                    />
                  }
                  <div className="row">
                    <div className="col-xs-6">
                      {
                        (ApprovalType == "PMT1" || ApprovalType == "PMT2") ? 
                        (
                          <button
                            onClick={() => this.onOpenModal("REJECT")}
                            className="btn btn-warning form-control"
                          >
                            Reject
                          </button>
                        )
                        :
                        (
                          secureStorage.getItem("IsSOOA") == 1 ? (
                            <button
                              onClick={() => this.onOpenModal("REVISE")}
                              className="btn btn-warning form-control"
                            >
                              Revise
                            </button>
                          ) : (
                            <button
                              onClick={() => this.onOpenModal("REJECT")}
                              className="btn btn-warning form-control"
                            >
                              Reject
                            </button>
                          )
                        )
                      }
                    </div>
                    <div className="col-xs-6">
                      <button
                        onClick={() => this.onOpenModal("APPROVE")}
                        className="btn btn-info form-control"
                      >
                        Approve
                      </button>
                    </div>
                  </div>
                </div>
              </div>);
    } catch(error) {
      Log.error("Error on ViewCommAdjstApproval => ", error);
      return ("");
    }
  }

  onChangeRemarkText = (text) => {

  }

  ViewLimitApprovalV2 = props => {
    let datas = props.data;
    Log.debugGroup("Datas on ViewLimitApprovalV2 => ", datas);

    return (
        <div>
            <div className="panel panel-default" 
              style={{ paddingBottom: "10px" }}>

              <div className="panel-body">
                <div className="row">
                  <div className="col-xs-12">
                    <pre style={{ fontSize: 'x-small', whiteSpace: 'pre-wrap', overflowX: 'auto' }}>
                      {datas}
                    </pre>
                  </div>
                </div>

                <div className="row">
                  <label className="col-xs-12 col-sm-3" htmlFor="">Remarks</label>
                  <div className="col-xs-12 col-sm-9">
                    <textarea
                      type="text"
                      name="remarkreject"
                      className="form-control"
                      placeholder="Remarks"
                      ref={input => (this.textInput = input)}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-xs-6">
                    {secureStorage.getItem("IsSOOA") == 1 ? (
                      <button
                        onClick={() => this.onOpenModal("REVISE")}
                        className="btn btn-warning form-control"
                      >
                        Revise
                      </button>
                    ) : (
                      <button
                        onClick={() => this.onOpenModal("REJECT")}
                        className="btn btn-warning form-control"
                      >
                        Reject
                      </button>
                    )}
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={() => this.onOpenModal("APPROVE")}
                      className="btn btn-info form-control"
                    >
                      Approve
                    </button>
                  </div>
                </div>

              </div>



            </div>

        </div>
      )
  }

  ViewLimitApprovalV1 = props => {
    let datas = props.data;
    let itemInfo = props.data.ItemInfo;
    return (
      <div>
        
            <div
              className="panel panel-default"
              style={{ paddingBottom: "10px" }}
            >
            {
              // TODO LCY: fix this into real APPROVAL TYPE
              (this.state.ApprovalType == "Approval Adjustment" && (itemInfo.isPTSB != null && itemInfo.isPTSB == 1) ) ?
                <h5 style={{ paddingLeft: '15px' }}><input type="checkbox" checked="true" disabled="true"/> POLIS TERBIT SEBELUM BAYAR</h5>
                :""
            }
              <div className="panel-heading">
                <h3 className="panel-title"> <strong>Order Limit Approval Details</strong> </h3>
              </div>
              <div className="panel-body">

                <div className="row">
                  <div className="col-xs-5">
                    <p>Customer Name</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>163484533 - JOSHUA SATRIAWAN SUPER</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Policy Holder</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>163484533 - JOSHUA SATRIAWAN SUPER</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Name On Policy</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>JOSHUA SATRIAWAN SUPER</strong>
                  </div>
                </div>

                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Payer Name</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>163484533 - JOSHUA SATRIAWAN SUPER</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Transaction Type</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>Order (New Policy)</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Product</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>LDN61 - LDN61 LI New Car OJK Dealer 2017 (25%)</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Mou</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>LDN61 - LDN61 LI New Car OJK Dealer 2017 (25%)</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Period</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>09/11/2018 To 09/11/2019</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Marketing</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>DOC</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Business Source</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>P1C100 - Dealer Astra</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Dealer Name</p>
                  </div>
                  <div className="col-xs-7">
                    <strong></strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Acceptance</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>DIRECT</strong>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>Branch</p>
                  </div>
                  <div className="col-xs-7">
                    <strong>022 - RETAIL OPERATIONS</strong>
                  </div>
                </div>

              </div>


            </div>






            <fieldset key="0">
              <div
                className="panel panel-default"
                style={{ paddingBottom: "10px" }}
              >
                <div className="panel-heading"
                      data-toggle="collapse" data-target="#objectCollapse0" role="button" aria-expanded="true" aria-controls="objectCollapse0">
                      <h3 className="panel-title"> <strong>Object 1</strong> </h3>
                </div>
                <div className="panel-body collapse in" aria-expanded="true" id="objectCollapse0">
                      <div className="row" >
                        <div className="col-xs-5">
                          <p>Type</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>SPORT UTILITY VEHICLE</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Description</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>LEXUS RX NEW 300 F SPORT 4X2 A/T 2018</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Status</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>Active</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Manufacturing Year</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>2018</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Market Price</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>IDR 1,375,000,000.00</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>ENGINE #</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>OSU016</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Chasis #</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>OSU016</strong>
                        </div>
                      </div>
                      <div className="row separatorTop" >
                        <div className="col-xs-5">
                          <p>Color On STNK</p>
                        </div>
                        <div className="col-xs-7">
                          <strong>OSU016</strong>
                        </div>
                      </div>

                      <div className="row separatorTop">
                        <div className="col-xs-5">
                          <p>Interest List: </p>
                        </div>
                      </div>

                      <fieldset key="interest0">
                          <div className="row separatorTop backgroundgrey">
                            <h5 style={{ marginLeft: "15px", marginTop: "15px" }}>
                              <b>Interest : KENDARAAN</b>
                            </h5>
                          </div>
                          <div className="row separatorTop">
                            <div className="col-xs-5">
                              <p>Sum Insured</p>
                            </div>
                            <div className="col-xs-7">
                              <strong>IDR 1,375,000,000.00</strong>
                            </div>
                          </div>
                          <div className="row separatorTop">
                            <div className="col-xs-5">
                              <p>Description</p>
                            </div>
                            <div className="col-xs-7">
                              <strong></strong>
                            </div>
                          </div>
                          <div className="row separatorTop">
                            <div className="col-xs-5">
                              <p>Coverage List</p>
                            </div>
                            <div className="col-xs-5">:</div>
                          </div>

                          <fieldset key="coverage0">
                              <div
                                className="panel panel-default"
                                style={{ paddingBottom: "10px" }}
                              >
                                <div className="panel-heading"
                                      data-toggle="collapse" data-target="#interestCollapse0" role="button" aria-expanded="true" aria-controls="interestCollapse0">
                                    <h6 className="panel-title"><strong>Coverage: COMPREHENSIVE</strong></h6>
                                </div>
                                <div className="panel-body collapse in" aria-expanded="true" id="interestCollapse0">
                                  
                                    <div className="row">
                                      <div className="col-xs-5">Status</div>
                                      <div className="col-xs-7"><strong>Active</strong></div>
                                    </div>

                                    <div className="row">
                                      <div className="col-xs-5">From</div>
                                      <div className="col-xs-7"><strong>09/11/2018</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">To</div>
                                      <div className="col-xs-7"><strong>09/11/2019</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Insured </div>
                                      <div className="col-xs-7"><strong>IDR 1,375,000,000.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Rate </div>
                                      <div className="col-xs-7"><strong>1.4500%</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Loading </div>
                                      <div className="col-xs-7"><strong>0.0000%</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Premium </div>
                                      <div className="col-xs-7"><strong>IDR 19,937,500.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Deductible </div>
                                      <div className="col-xs-7"><strong>FLT94D - 300,000.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Comission </div>
                                      <div className="col-xs-7">
                                        <ul style={{ paddingLeft: '15px' }}>
                                          <li><strong>Comission = 21%</strong></li>
                                        </ul>
                                      </div>
                                    </div>

                                  

                                </div>
                              </div>
                          </fieldset>


                          <fieldset key="coverage1">
                              <div
                                className="panel panel-default"
                                style={{ paddingBottom: "10px" }}
                              >
                                <div className="panel-heading"
                                      data-toggle="collapse" data-target="#interestCollapse1" role="button" aria-expanded="false" aria-controls="interestCollapse1">
                                    <h6 className="panel-title"><strong>Coverage: EARTHQUAKE</strong></h6>
                                </div>
                                <div className="panel-body collapse" aria-expanded="false" id="interestCollapse1">
                                    <div className="row">
                                      <div className="col-xs-5">Status </div>
                                      <div className="col-xs-7"><strong>Active</strong></div>
                                    </div>

                                    <div className="row">
                                      <div className="col-xs-5">From</div>
                                      <div className="col-xs-7"><strong>09/11/2018</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">To</div>
                                      <div className="col-xs-7"><strong>09/11/2019</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Insured </div>
                                      <div className="col-xs-7"><strong>IDR 1,375,000,000.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Rate </div>
                                      <div className="col-xs-7"><strong>1.4500%</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Loading </div>
                                      <div className="col-xs-7"><strong>0.0000%</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Premium </div>
                                      <div className="col-xs-7"><strong>IDR 19,937,500.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Deductible </div>
                                      <div className="col-xs-7"><strong>FLT94D - 300,000.00</strong></div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Comission </div>
                                      <div className="col-xs-7">
                                        <ul style={{ paddingLeft: '15px' }}>
                                          <li><strong>Comission = 21%</strong></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-xs-5">Net Premium </div>
                                      <div className="col-xs-7">
                                      <ul style={{ paddingLeft: '15px' }}>
                                        <li><strong>IDR 1,375,000.00</strong></li>
                                      </ul>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </fieldset>



                      </fieldset>




                </div>
              </div>
            </fieldset>

            

            <div className="panel panel-default">
              
              <div className="panel-body">
                <div className="row">
                  <label className="col-xs-12 col-sm-3" htmlFor="">Remarks</label>
                  <div className="col-xs-12 col-sm-9">
                    <textarea className="form-control" rows="10"/>
                  </div>
                </div>
                <div className="row" style={ { paddingBottom: '10px' } }>
                  <div className="col-xs-6">
                    {secureStorage.getItem("IsSOOA") == 1 ? (
                      <button
                        onClick={() => this.onOpenModal("REVISE")}
                        className="btn btn-warning form-control"
                      >
                        Revise
                      </button>
                    ) : (
                      <button
                        onClick={() => this.onOpenModal("REJECT")}
                        className="btn btn-warning form-control"
                      >
                        Reject
                      </button>
                    )}
                  </div>
                    <div className="col-xs-6">
                    <button
                      onClick={() => this.onOpenModal("APPROVE")}
                      className="btn btn-info form-control"
                    >
                      Approve
                    </button>
                    </div>
                  </div>
                </div>
              </div>

      </div>
      );
  }

  ViewDataDetailNotFound = (props) => {
    switch(props.ErrType) {
      case 0: {
        return("");
      } 
      case 1: {
        return(
          <div>
                <div
                  className="panel panel-default text-center"
                ><h3 className="panel-title" style={{ margin: "5px" }}> <strong>
                  Data Not Found !</strong> </h3>
                </div>
          </div>
        );
      }
      case 666: {
        return(
          <div>
                <div className="panel panel-default" >

                  <div className="error-container">
                    <div className="row error-header">
                      <div className="col-md-8 col-xs-7">
                      <span className="error-title">Oops! <br/> Something went wrong</span>
                      </div>
                      <div className="col-md-4 col-xs-5">
                        <span className="error-main-icon"><i className="fa fa-meh-o" aria-hidden="true"></i></span>
                
                      </div>
                      <div className="col-12">
                        <span className="error-sub-title">
                          Please copy &amp; send this code bellow to the IT Dev (Screenshot scares them):
                        </span>
                      </div>
                    </div>
                    <br/>
                    <div className="error-body">
                      <br/>
                      <span className="error-code">
                        {props.ErrorCodeStr}
                      </span>
                    </div>
                  </div>

                </div>
          </div>
          );
      }
    }
  }

  render() {
    const ModalComponent = () => {
      if (this.state.type == "APPROVE") {
        return (
          <Modal
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <div
              className="modal-content panel panel-primary"
              style={{ margin: "-17px" }}
            >
              <div className="modal-header panel-heading">
                <h5 className="modal-title" id="exampleModalLabel">
                  Order Approval
                </h5>
              </div>
              <div className="modal-body">Do you want to approve?</div>
              <div className="modal-footer">
                <div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onCloseModal}
                      className="btn btn-warning form-control"
                    >
                      Cancel
                    </button>
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onApprove}
                      className="btn btn-info form-control"
                    >
                      Approve
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        );
      } else if (this.state.type == "REVISE") {
        return (
          <Modal
            styles={{
              modal: {
                width: "80%"
              }
            }}
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <form onSubmit={this.onSubmitRemarksRevise}>
              <div
                className="modal-content panel panel-primary"
                style={{ margin: "-22px" }}
              >
                <div className="modal-header panel-heading">
                  <h5 className="pull-left" id="exampleModalLabel">
                    <b>Send to SA</b>
                  </h5>

                  {/* <button className="btn btn-primary" type="button"> */}
                  <button
                    type="submit"
                    style={{
                      backgroundColor: "#18516c",
                      borderColor: "#18516c"
                    }}
                    className="btn btn-danger pull-right"
                  >
                    <i className="fa fa-send" />
                  </button>
                  {/* </button> */}
                </div>
                <div className="modal-body">
                  <p>Remarks</p>
                  <textarea
                    type="text"
                    style={{ marginBottom: "0px", resize: "none" }}
                    name="remarksrevise"
                    className="form-control"
                    placeholder="Remarks"
                    ref={input => (this.textInput = input)}
                  />
                </div>
                <div className="modal-footer">
                  {/* <div>
                    <div className="col-xs-6">
                      <button
                        onClick={this.onCloseModal}
                        className="btn btn-warning form-control"
                      >
                        Cancel
                      </button>
                    </div>
                    <div className="col-xs-6">
                      
                    </div>
                  </div> */}
                </div>
              </div>
            </form>
          </Modal>
        );
      } else if (this.state.type == "REJECT") {
        return (
          <Modal
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <div
              className="modal-content panel panel-primary"
              style={{ margin: "-17px" }}
            >
              <div className="modal-header panel-heading">
                <h5 className="modal-title" id="exampleModalLabel">
                  Order Approval
                </h5>
              </div>
              <div className="modal-body">Do you want to reject?</div>
              <div className="modal-footer">
                <div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onCloseModal}
                      className="btn btn-warning form-control"
                    >
                      Cancel
                    </button>
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onReject}
                      className="btn btn-danger form-control"
                    >
                      Reject
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        );
      }

      return null;
    };

    var detailsobject = null;

    if (this.state.allObjects != []) {
      // console.log(this.state.datasobject)
      Log.debugGroup("DATA ALL OBJECTS => ", this.state.allObjects);
      detailsobject = this.state.allObjects.map((data, index) => {
        return (
          <fieldset key={index}>
            <div className="row separatorTop backgroundgrey">
              <h4 style={{ marginLeft: "15px", marginTop: "15px" }}>
                <b>TAHUN {index + 1}</b>
              </h4>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12">
                <p>
                  <b>
                    {data.Object[0].ObjectName +
                      " " +
                      data.Object[0].Year +
                      " " +
                      data.Object[0].PoliceNumber}
                  </b>
                </p>
              </div>
              {data.Object[0].Interests.map((interest, index) => {
                return (
                  <div
                    className= {"col-xs-12 panel-body-list separatorTop " + index + 908 }
                    key={index + 908}
                  >
                    <p style={{ marginLeft: "15px", marginRight: "15px" }}>
                      <b>{interest.InterestName.toUpperCase()}</b>
                      <br />
                      {Util.formatMoney(interest.SumInsured, 2)}
                    </p>
                    {interest.Coverages.map((coverage, index) => {
                      return (
                        <div
                          key={index + 19032}
                          className="col-xs-12 separatorTop panel-body-list"
                        >
                          <div
                            style={{ marginLeft: "15px", marginRight: "15px" }}
                          >
                            <div className="col-xs-6 panel-body-list">
                              <p className="pull-left">
                                {coverage.Coverage.toUpperCase()}
                              </p>
                            </div>
                            {
                              coverage.Coverage.toUpperCase().includes("LOADING") ? 
                              <div className="col-xs-6 panel-body-list">
                                <p className="pull-right">
                                  <b>{Util.formatMoney(coverage.LoadingRate, 2)}%</b>
                                </p>
                              </div> : <div className="col-xs-6 panel-body-list">
                                <p className="pull-right">
                                  <b>{Util.formatMoney(coverage.Premium, 2)}</b>
                                </p>
                              </div>
                            }
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </div>
          </fieldset>
        );
      });
    }

    const datas = this.state.datas;
    const dataSendTo = this.state.dataSendTo;
    Log.debugGroup("DataSendTo on Render => ", dataSendTo);
    return (
      <div>
        <Header />
        <OtosalesModalAlert
          message="Do you want to revise?"
          onClose={this.closeAlertRevise}
          onSubmit={this.onSubmitAlertRevise}
          showModalInfo={this.state.showModalAlertRevise}
        />
        <ModalComponent />
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            blocking={this.state.isLoading}
            loader={
              <Loader active type="ball-spin-fade-loader" color="#02a17c" />
            }
            message="Loading, please wait"
           >
           <div style={ this.state.isLoading? {paddingTop: "35%", paddingBottom: '50%'} : {} }>
               {
                  (this.state.isLoading)? "" :
                    (this.state.isDetailFound)?
                    (
                      (this.state.ApprovalType != null && this.state.ApprovalType != "" && datas != null)?
                                ((this.state.ApprovalType == "ADJUST" || this.state.ApprovalType == "COMSAMD" || this.state.ApprovalType == "KOMISI" || this.state.ApprovalType == "PMT1" || this.state.ApprovalType == "PMT2")?
                                <this.ViewCommAdjstApproval data={datas} detailsobject={detailsobject} datasendto={dataSendTo}/> 
                                : <this.ViewLimitApprovalV2 data={datas} />) : ""
                    ): <this.ViewDataDetailNotFound 
                        ErrType={this.state.ErrType}
                        ErrorCodeStr={this.state.ErrorCodeStr}
                        />
               }
            </div>

          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageOrderApprovalDetails);
