import { Util } from "../../../../otosalescomponents";
import * as Config from "../../../../config";

export const populateOrderApproval = async (UserID) => {
    return Util.fetchAPIAdditional(
        // `${Config.API_URL+""+Config.API_V2}/OrderApproval/populateOrderApproval`,
        `${Config.API_URL+""+Config.API_VERSION_0090URF2020}/DataReact/populateOrderApproval`,
        "POST",
        Config.HEADER_API,
        {
          UserID
        }
      );
}

export const populatePaymentApproval = async (UserID) => {
    return Util.fetchAPIAdditional(
        // `${Config.API_URL+""+Config.API_V2}/OrderApproval/populateOrderApproval`,
        `${Config.API_URL+""+Config.API_VERSION_0090URF2020}/DataReact/populatePaymentApproval`,
        "POST",
        Config.HEADER_API,
        {
          UserID
        }
      );
}