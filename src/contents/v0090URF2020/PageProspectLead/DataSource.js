import { API_URL, API_VERSION_2, ACCOUNTDATA, API_VERSION_0090URF2020, HEADER_API } from "../../../config";
import { Util } from "./../../../otosalescomponents";

export const getPernawaran = async (userID) => {
    return Util.fetchAPIAdditional(
        `${API_URL+""+API_VERSION_0090URF2020}/datareact/penawaran`,
        "POST",
        HEADER_API,
        {
            userID
        }
      );
}

export const getItMGOBid = async (paramReq) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/Synchronization/GetItMGOBid`,
        "POST",
        HEADER_API,
        paramReq
      );
}




