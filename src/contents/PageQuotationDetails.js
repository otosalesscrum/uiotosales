import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { API_URL, API_VERSION, API_VERSION_2, ACCOUNTDATA } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { Otosalesheader, Log, Util } from "../otosalescomponents";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";
//flag
class PageQuotationDetails extends Component {
  constructor(props) {
    super(props);

    // if(!document.body.classList.contains("sidebar-collapse")){
    //   document.body.classList.add("sidebar-collapse");
    // }

    this.state = {
      isLoading: true,
      followupno: "",
      datas: null,
      datacoverage: [],
      odcModel: {
        CascoSI1: 0,
        CascoSI2: 0,
        CascoSI3: 0,
        CascoSI4: 0,
        CascoSI5: 0,
        CascoAccess1: 0,
        CascoAccess2: 0,
        CascoAccess3: 0,
        CascoAccess4: 0,
        CascoAccess5: 0,
        AccessSI1: 0,
        AccessSI2: 0,
        AccessSI3: 0,
        AccessSI4: 0,
        AccessSI5: 0,
        CascoRate1: 0,
        CascoRate2: 0,
        CascoRate3: 0,
        CascoRate4: 0,
        CascoRate5: 0,
        LoadingRate1: 0,
        LoadingRate2: 0,
        LoadingRate3: 0,
        LoadingRate4: 0,
        LoadingRate5: 0,
        BasicPremium1: 0,
        BasicPremium2: 0,
        BasicPremium3: 0,
        BasicPremium4: 0,
        BasicPremium5: 0,
        BundlingPremium1: 0,
        BundlingPremium2: 0,
        BundlingPremium3: 0,
        BundlingPremium4: 0,
        BundlingPremium5: 0,
        IsBundling1: 0,
        IsBundling2: 0,
        IsBundling3: 0,
        IsBundling4: 0,
        IsBundling5:0,
        TSPremium1: 0,
        TSPremium2: 0,
        TSPremium3: 0,
        TSPremium4: 0,
        TSPremium5: 0,
        TPLPremium1: 0,
        TPLPremium2: 0,
        TPLPremium3: 0,
        TPLPremium4: 0,
        TPLPremium5: 0,
        PADRVRPremium1: 0,
        PADRVRPremium2: 0,
        PADRVRPremium3: 0,
        PADRVRPremium4: 0,
        PADRVRPremium5: 0,
        PAPASSPremium1: 0,
        PAPASSPremium2: 0,
        PAPASSPremium3: 0,
        PAPASSPremium4: 0,
        PAPASSPremium5: 0,
        ExtendPremium1: 0,
        ExtendPremium2: 0,
        ExtendPremium3: 0,
        ExtendPremium4: 0,
        ExtendPremium5: 0,
        BExtendPremium1: 0,
        BExtendPremium2: 0,
        BExtendPremium3: 0,
        BExtendPremium4: 0,
        BExtendPremium5: 0,
        TotalPremium1: 0,
        TotalPremium2: 0,
        TotalPremium3: 0,
        TotalPremium4: 0,
        TotalPremium5: 0,
        TPLSI: 0,
        PADRVRSI: 0,
        PASS: 0,
        PAPASSSI: 0,
        AdminFee: 0,

        // 0283/URF/2015 BSY
        BundlingPremiumRate: 0,
        TSPremiumRate: 0,
        TPLPremiumRate: 0,
        PADRVRPremiumRate: 0,
        PAPASSPremiumRate: 0
        // END
      },

      spModels: [],
      BranchData: {},
      param: [],

      isYearTwoExist: false,
      isYearThreeExist: false,
      callcenter: null,
      callcenterTINSHP: null,
      callcenterTINSPhone: null,
      basicOwnRisk: null,
      coverOwnRisk: null,
      extendOwnRisk: null
    };
  }

  setParams = (spModels) => {
    for (let index = 0; index < spModels.length; index++) {
      const item = spModels[index];
      if (item.Param == "CallCenter") {
        this.setState({ callcenter: item.Value });
      } else if (item.Param == "CallCenterTINSHP") {
        this.setState({ callcenterTINSHP: item.Value });
      } else if (item.Param == "CallCenterTINSPhone") {
        this.setState({ callcenterTINSPhone: item.Value });
      } else if (item.Param == "BasicOwnRisk") {
        this.setState({ basicOwnRisk: item.Value });
      } else if (item.Param == "CoverOwnRisk") {
        this.setState({ coverOwnRisk: item.Value });
      } else if (item.Param == "ExtendOwnRisk") {
        this.setState({ extendOwnRisk: item.Value });
      }
    }
  }

  formatMoney = (amount, decimalCount = 0, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;

      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      // console.log(e)
    }
  };

  componentDidMount() {
    var followupno = secureStorage.getItem("followupnoquotationdetails");
    var branchcode = JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode;
    var channelsource = JSON.parse(ACCOUNTDATA).UserInfo.User.ChannelSource || "";
    this.loaddataheader(followupno, branchcode, channelsource);
    var orderno = secureStorage.getItem("ordernoquotationdetails");
    // this.loaddatacoverage(orderno);
    this.getOrderDetailCoverage();
    var setOrderDetail = setInterval(() => {
      if (this.state.datacoverage.length > 0) {
        clearInterval(setOrderDetail);
        // this.setOrderDetailCoverage(this.state.datacoverage);
      }
    }, 100);
    this.state.odcModel = JSON.parse(secureStorage.getItem("inputprospect-state"))
    .odcModel
  }

  setOrderDetailCoverage = odlcModels => {
    var odcModelState = { ...this.state.odcModel };
    for (let i = 0; i < odlcModels.length; i++) {
      const item = odlcModels[i];
      Log.debugGroup("odlcModels: ", odlcModels); 
      if (item.Year == 1) {
        this.setState({ 
          isYearTwoExist: false,
          isYearThreeExist: false,
          isYearFourExist: false,
          isYearFiveExist: false
        });

        odcModelState.CascoSI1 = item.VehiclePremi;
        odcModelState.AccessSI1 = item.AccessPremi;
        odcModelState.CascoAccess1 = item.VehicleAccessPremi;
        odcModelState.CascoRate1 = item.Rate;
        odcModelState.LoadingRate1 = item.LoadingPremi;
        odcModelState.BasicPremium1 = item.BasicPremi;
        odcModelState.BundlingPremium1 = item.BasiCoverage;
        odcModelState.TSPremium1 = item.TSCoverage;
        odcModelState.TPLPremium1 = item.TPLCoverage;
        odcModelState.PADRVRPremium1 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium1 = item.PAPASSCoverage;
        odcModelState.ExtendPremium1 = item.PremiPerluasan;
        odcModelState.BExtendPremium1 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium1 = item.TotalPremi;

      } else if (item.Year == 2) {

        odcModelState.CascoSI2 = item.VehiclePremi;
        odcModelState.AccessSI2 = item.AccessPremi;
        odcModelState.CascoAccess2 = item.VehicleAccessPremi;
        odcModelState.CascoRate2 = item.Rate;
        odcModelState.LoadingRate2 = item.LoadingPremi;
        odcModelState.BasicPremium2 = item.BasicPremi;
        odcModelState.BundlingPremium2 = item.BasiCoverage;
        odcModelState.TSPremium2 = item.TSCoverage;
        odcModelState.TPLPremium2 = item.TPLCoverage;
        odcModelState.PADRVRPremium2 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium2 = item.PAPASSCoverage;
        odcModelState.ExtendPremium2 = item.PremiPerluasan;
        odcModelState.BExtendPremium2 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium2 = item.TotalPremi;


        this.setState({  
          isYearTwoExist: true,
          isYearThreeExist: false,
          isYearFourExist: false,
          isYearFiveExist: false 
        });

      } else if (item.Year == 3) {

        odcModelState.CascoSI3 = item.VehiclePremi;
        odcModelState.AccessSI3 = item.AccessPremi;
        odcModelState.CascoAccess3 = item.VehicleAccessPremi;
        odcModelState.CascoRate3 = item.Rate;
        odcModelState.LoadingRate3 = item.LoadingPremi;
        odcModelState.BasicPremium3 = item.BasicPremi;
        odcModelState.BundlingPremium3 = item.BasiCoverage;
        odcModelState.TSPremium3 = item.TSCoverage;
        odcModelState.TPLPremium3 = item.TPLCoverage;
        odcModelState.PADRVRPremium3 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium3 = item.PAPASSCoverage;
        odcModelState.ExtendPremium3 = item.PremiPerluasan;
        odcModelState.BExtendPremium3 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium3 = item.TotalPremi;



        this.setState({ 
          isYearTwoExist: true,
          isYearThreeExist: true,
          isYearFourExist: false,
          isYearFiveExist: false  
        });
      } else if (item.Year == 4) {
        
        odcModelState.CascoSI4 = item.VehiclePremi;
        odcModelState.AccessSI4 = item.AccessPremi;
        odcModelState.CascoAccess4 = item.VehicleAccessPremi;
        odcModelState.CascoRate4 = item.Rate;
        odcModelState.LoadingRate4 = item.LoadingPremi;
        odcModelState.BasicPremium4 = item.BasicPremi;
        odcModelState.BundlingPremium4 = item.BasiCoverage;
        odcModelState.TSPremium4 = item.TSCoverage;
        odcModelState.TPLPremium4 = item.TPLCoverage;
        odcModelState.PADRVRPremium4 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium4 = item.PAPASSCoverage;
        odcModelState.ExtendPremium4 = item.PremiPerluasan;
        odcModelState.BExtendPremium4 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium4 = item.TotalPremi;


        this.setState({ 
          isYearTwoExist: true,
          isYearThreeExist: true,
          isYearFourExist: true,
          isYearFiveExist: false  
        });
      } else if (item.Year == 5) {

        odcModelState.CascoSI5 = item.VehiclePremi;
        odcModelState.AccessSI5 = item.AccessPremi;
        odcModelState.CascoAccess5 = item.VehicleAccessPremi;
        odcModelState.CascoRate5 = item.Rate;
        odcModelState.LoadingRate5 = item.LoadingPremi;
        odcModelState.BasicPremium5 = item.BasicPremi;
        odcModelState.BundlingPremium5 = item.BasiCoverage;
        odcModelState.TSPremium5 = item.TSCoverage;
        odcModelState.TPLPremium5 = item.TPLCoverage;
        odcModelState.PADRVRPremium5 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium5 = item.PAPASSCoverage;
        odcModelState.ExtendPremium5 = item.PremiPerluasan;
        odcModelState.BExtendPremium5 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium5 = item.TotalPremi;


        this.setState({ 
          isYearTwoExist: true,
          isYearThreeExist: true,
          isYearFourExist: true,
          isYearFiveExist: true  
        });
      }
    }

    this.setState({ odcModel: odcModelState });

    // console.log(odcModelState);
  };

  loaddataheader = (followupno, branchcode, channelsource) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailHeader/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "followupno=" + followupno + "&branchcode=" + branchcode + "&channelsource=" + channelsource
    })
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          datas: datamapping.data[0],
          spModels: datamapping.spModels,
          BranchData: datamapping.BranchData,
          param: datamapping.param,
          isLoading: false
        });
        this.setParams(datamapping.spModels);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.loaddataheader(followupno, branchcode, channelsource);
        }
      });
  };

  getOrderDetailCoverage = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailCoverage/`, {
      method: "POST",

      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "calculatedPremi=" + JSON.stringify(JSON.parse(secureStorage.getItem("inputprospect-state"))
      .CalculatedPremiItems)
      + "&AdminFee=" + JSON.parse(secureStorage.getItem("inputprospect-state")).AdminFee
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          dataorderdetailcoverageall: jsn.data
        }, () => {
          this.setOrderDetailCoverage(jsn.data.dtlCoverage);
          Log.debugGroup("datamapping.data.dtlCoverage: ", jsn.data.dtlCoverage);
          Log.debugGroup("datamapping.data: ", jsn.data);
        });
      })
      .then(datamapping => {
        this.setState(
          {
            // datas: datamapping.data[0],
            datacoverage: datamapping.dtlCoverage,
            isLoading: false
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
        }
      });

  }

  loaddatacoverage = orderno => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailCoverage/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "orderno=" + orderno
    })
      .then(res => res.json())
      .then(datamapping => {
        this.setState(
          {
            // datas: datamapping.data[0],
            datacoverage: datamapping.data,
            isLoading: false
          },
          () => {
            // this.setOrderDetailCoverage(datamapping.data);
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.loaddatacoverage(orderno);
        }
      });
  };

  formatDate = dates => {
    var dd = dates.getDate();
    var mm = dates.getMonth(); //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }

    var mL = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    var mS = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ];
    return dd + " " + mS[mm] + " " + yyyy;
  };

  formatTimeFromDate = dates => {
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    var ss = dates.getMinutes();

    return hh + ":" + mm + ":" + ss;
  };

  handleBackButton = () => {
    this.props.history.goBack();
  }

  render() {
    var jenisperlindungan = null;
    if (this.state.datas != null && this.state.datas.CoverageType != null) {
      if (
        this.state.datas.CoverageType.toLowerCase().includes("comprehensive") &&
        this.state.datas.CoverageType.toLowerCase().includes("tlo")
      ) {
        jenisperlindungan = "ALL";
        var CoverageType = { ...this.state.datas };
        CoverageType = CoverageType.CoverageType;
        if (CoverageType != null) {
          var partsCoverageType = CoverageType.split("Tahun\\n");
          // console.log(partsCoverageType);
          jenisperlindungan = partsCoverageType[0] + " Tahun " + partsCoverageType[1];
        }
      } else if (this.state.datas.CoverageType.toLowerCase().includes("comprehensive")) {
        jenisperlindungan = "Comprehensive";
      } else if (this.state.datas.CoverageType.toLowerCase().includes("tlo")) {
        jenisperlindungan = "TLO";
      }
    }

    var fiturlayanan = null;
    if (this.state.datas != null) {
      switch (this.state.datas.InsuranceType) {
        case 1:
          fiturlayanan = <ul style={{ paddingLeft: "20px", paddingRight: "20px" }}>
            <li style={{ wordWrap: "break-word" }}>Garda Siaga (ERA &amp; Towing Car 24 Jam)</li>
            <li style={{ wordWrap: "break-word" }}>Garda Akses CALL 24 Jam</li>
            <li style={{ wordWrap: "break-word" }}>Perbaikan di Seluruh Jaringan Bengkel Garda Oto Termasuk Bengkel ATPM (untuk Pertanggungan Comprehensive)</li>
            <li style={{ wordWrap: "break-word" }}>Klaim makin mudah di Garda Center, buka setiap hari</li>
            <li style={{ wordWrap: "break-word" }}>Garda Mobile Otocare, peace of mind in your pocket</li>
          </ul>
          break;
        case 2:
          fiturlayanan = <ul style={{ paddingLeft: "20px", paddingRight: "20px" }}>
            <li style={{ wordWrap: "break-word" }}>ERA (Emergency Roadside Assistance), EMA (Emergency Medical Assistance) &amp; Towing Car 24 Jam</li>
            <li style={{ wordWrap: "break-word" }}>Call Center 1500 112 (24 JAM)</li>
            <li style={{ wordWrap: "break-word" }}>Perbaikan di Seluruh Jaringan Bengkel Authorized Toyota (untuk Pertanggungan Comprehensive)</li>
            <li style={{ wordWrap: "break-word" }}>Pencapaian kerugian Total Loss sebesar 50% dari nilai kerugian (untuk tahun pertama)</li>
            <li style={{ wordWrap: "break-word" }}>Klaim makin mudah di Garda Center, buka setiap hari</li>
            <li style={{ wordWrap: "break-word" }}>Garda Mobile Otocare, peace of mind in your pocket</li>
            <li style={{ wordWrap: "break-word" }}>Jaminan Hasil Perbaikan Bengkel Selama 12 Bulan</li>
          </ul>
          break;
        case 3:
          fiturlayanan = <ul style={{ paddingLeft: "20px", paddingRight: "20px" }}>
            <li style={{ wordWrap: "break-word" }}>Layanan ERA (Emergency Road Assistance) 24 Jam</li>
            <li style={{ wordWrap: "break-word" }}>CALL CENTER 24 Jam</li>
            <li style={{ wordWrap: "break-word" }}>Perbaikan di Seluruh Jaringan Bengkel Authorized Lexus</li>
          </ul>
          break;
      }
    }


    // console.log(jenisperlindungan);
    return (
      <div>
        <Otosalesheader handleBackButton={this.handleBackButton} titles="Quotation Details"/>
        <div className="" style={{ padding: "10px", fontSize: "8pt" }}>
          <BlockUi
            tag="div"
            blocking={this.state.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >
            <div className="panel panel-default">
              {/* <div className="panel-heading">
                <h3 className="panel-title"> Quotation Details </h3>
              </div> */}
              {this.state.datas != null && (
                <div className="panel-body">
                  <h3>
                    <strong>PERHITUNGAN PREMI ASURANSI {this.state.datas.InsuranceTypeDesc.toUpperCase()}</strong>
                  </h3>
                  <div className="row">
                    <div className="col-xs-5">
                      <p>
                        <strong>TANGGAL</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>
                        {this.state.datas.City +
                          ", " +
                          this.formatDate(Util.convertDate(this.state.datas.EntryDate))}
                      </p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>NO SURAT</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.QuotationNo}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>KEPADA YTH</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.ProspectName}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>NO TELEPON</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.Phone1}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>EMAIL</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.Email1}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>PERIHAL</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>Penawaran Asuransi Kendaraan Bermotor (NEW)</p>
                    </div>
                  </div>
                  {(!this.state.isYearTwoExist && !this.state.isYearThreeExist) ? "" : (
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>
                          <strong>JENIS PERTANGGUNGAN</strong>
                        </b>
                        </p>
                      </div>
                      <div className="col-xs-8">
                        <p style={{ textAlign: "right" }}>{jenisperlindungan}</p>
                      </div>
                    </div>
                  )}
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>MEREK / TIPE KENDARAAN</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.Vehicle}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>NO POLISI</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.RegistrationNumber || "-"}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>TAHUN</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.Year}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>PENGGUNAAN</strong>
                      </p>
                    </div>
                    <div className="col-xs-7">
                      <p style={{ textAlign: "right" }}>{this.state.datas.Usage}</p>
                    </div>
                  </div>
                  <div className="row separatorTop">
                    <div className="col-xs-5">
                      <p>
                        <strong>TOTAL PREMI</strong>
                      </p>
                    </div>
                    <div className="col-xs-7" style={{ textAlign: "right" }}>
                      <p style={{ color: "#D59F00" }}>
                        <b>
                          {this.formatMoney(this.state.datas.TotalPremium, 0)}
                        </b>
                      </p>
                    </div>
                  </div>
                  <div id="table-objek-quotation">
                  <fieldset>
                    <div className="row separatorTop backgroundgrey"><h5 className="col-xs-12"><b>OBJEK PERTANGGUNGAN</b></h5></div>
                    {(!this.state.isYearTwoExist && !this.state.isYearThreeExist) ? "" :
                      <div className="row separatorTop">
                        <div className="col-xs-4">
                          <p />
                        </div>
                        <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                            <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                              <p><b>Tahun 5</b></p>
                            </div>
                          }
                          {this.state.isYearFourExist &&
                            <div className={(this.state.isYearFiveExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                              <p><b>Tahun 4</b></p>
                            </div>
                          }
                          {this.state.isYearThreeExist &&
                            <div className={(this.state.isYearFourExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                              <p><b>Tahun 3</b></p>
                            </div>
                          }
                          {this.state.isYearTwoExist &&
                            <div className={(this.state.isYearThreeExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                              <p><b>Tahun 2</b></p>
                            </div>
                          }
                          <div className="col-xs-2 pull-right panel-body-list table-objek-right table-objek-left" style={{ textAlign: "right"}}>
                            <p><b>Tahun 1</b></p>
                          </div>


                        </div>
                      </div>
                    }
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>KENDARAAN</b></p>
                      </div>
                      <div className="col-xs-8 text-right">
                      {this.state.isYearFiveExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <span>{this.formatMoney(this.state.odcModel.CascoSI5, 0)}</span>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <span>{this.formatMoney(this.state.odcModel.CascoSI4, 0)}</span>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <span>{this.formatMoney(this.state.odcModel.CascoSI3, 0)}</span>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <span>{this.formatMoney(this.state.odcModel.CascoSI2, 0)}</span>
                          </div>
                        }
                          <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
                            <span>{this.formatMoney(this.state.odcModel.CascoSI1, 0)}</span>
                          </div>
                      </div>
                    </div>
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>AKSESORIS NON STANDARD</b></p>
                      </div>
                      <div className="col-xs-8 text-right">

                      {this.state.isYearFiveExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p>{this.state.odcModel.AccessSI5 > 0 ? this.formatMoney(this.state.odcModel.AccessSI5, 0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p>{this.state.odcModel.AccessSI4 > 0 ? this.formatMoney(this.state.odcModel.AccessSI4, 0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p>{this.state.odcModel.AccessSI3 > 0 ? this.formatMoney(this.state.odcModel.AccessSI3, 0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p>{this.state.odcModel.AccessSI2 > 0 ? this.formatMoney(this.state.odcModel.AccessSI2, 0) : "No Cover"}</p>
                          </div>
                        }
                        <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
                          <p>{this.state.odcModel.AccessSI1 != 0 ? this.formatMoney(this.state.odcModel.AccessSI1, 0) : "No Cover"}</p>
                        </div>
                      </div>
                    </div>

                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>Kendaraan + Aksesoris Non Standard</b></p>
                      </div>
                      <div className="col-xs-8 text-right">

                        {this.state.isYearFiveExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.CascoAccess5 > 0 ? (this.formatMoney(this.state.odcModel.CascoAccess5)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.CascoAccess4 > 0 ? (this.formatMoney(this.state.odcModel.CascoAccess4)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.CascoAccess3 > 0 ? (this.formatMoney(this.state.odcModel.CascoAccess3)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.CascoAccess2 > 0 ? (this.formatMoney(this.state.odcModel.CascoAccess2)) : "No Cover"}</b></p>
                          </div>
                        }
                        <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
                          <p><b>{this.state.odcModel.CascoAccess1 != 0 ? (this.formatMoney(this.state.odcModel.CascoAccess1)) : "No Cover"}</b></p>
                        </div>
                      </div>
                    </div>

                    {(!this.state.isYearTwoExist && !this.state.isYearThreeExist) ? "" :
                      <div className="row separatorTop">
                        <div className="col-xs-4">
                          <p><b>TARIF RATE</b></p>
                        </div>
                        <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                            <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                              <p>{this.formatMoney(this.state.odcModel.CascoRate5, 2) + "%"}</p>
                            </div>
                          }
                          {this.state.isYearFourExist &&
                            <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                              <p>{this.formatMoney(this.state.odcModel.CascoRate4, 2) + "%"}</p>
                            </div>
                          }
                          {this.state.isYearThreeExist &&
                            <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                              <p>{this.formatMoney(this.state.odcModel.CascoRate3, 2) + "%"}</p>
                            </div>
                          }
                          {this.state.isYearTwoExist &&
                            <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                              <p>{this.formatMoney(this.state.odcModel.CascoRate2, 2) + "%"}</p>
                            </div>
                          }
                          <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right" }}>
                            <p>{this.formatMoney(this.state.odcModel.CascoRate1, 2) + "%"}</p>
                          </div>
                        </div>
                      </div>
                    }
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>LOADING PREMI</b></p>
                      </div>
                      <div className="col-xs-8 text-right">
                      {this.state.isYearFiveExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                            <p>{this.formatMoney(this.state.odcModel.LoadingRate5, 2) + "%"}</p>
                          </div>
                        } 
                        {this.state.isYearFourExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                            <p>{this.formatMoney(this.state.odcModel.LoadingRate4, 2) + "%"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                            <p>{this.formatMoney(this.state.odcModel.LoadingRate3, 2) + "%"}</p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right" }}>
                            <p>{this.formatMoney(this.state.odcModel.LoadingRate2, 2) + "%"}</p>
                          </div>
                        }
                        <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right" }}>
                          <p>{this.formatMoney(this.state.odcModel.LoadingRate1, 2) + "%"}</p>
                        </div>
                      </div>
                    </div>

                    <div className="row separatorTop table-objek-top-bottom ">
                      <div className="col-xs-4">
                        <p><b>Premi Dasar</b></p>
                      </div>
                      <div className="col-xs-8 text-right">

                        {this.state.isYearFiveExist &&
                          <div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.BasicPremium5 > 0 ? (this.formatMoney(this.state.odcModel.BasicPremium5)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div className={(this.state.isYearFiveExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.BasicPremium4 > 0 ? (this.formatMoney(this.state.odcModel.BasicPremium4)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div className={(this.state.isYearFourExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.BasicPremium3 > 0 ? (this.formatMoney(this.state.odcModel.BasicPremium3)) : "No Cover"}</b></p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div className={(this.state.isYearThreeExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
                            <p><b>{this.state.odcModel.BasicPremium2 > 0 ? (this.formatMoney(this.state.odcModel.BasicPremium2)) : "No Cover"}</b></p>
                          </div>
                        }
                        <div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left table-objek-right" : "col-xs-12 pull-right panel-body-list table-objek-left" }style={{ textAlign: "right"}}>
                          <p><b>{this.state.odcModel.BasicPremium1 != 0 ? (this.formatMoney(this.state.odcModel.BasicPremium1)) : "No Cover"}</b></p>
                        </div>
                      </div>
                    </div>

                  </fieldset>
                  <fieldset>
                    <div className="row separatorTop backgroundgrey"><h5 className="col-xs-12"><b>{!this.state.isYearTwoExist && !this.state.isYearThreeExist ? "TARIF & PREMI " + jenisperlindungan.toUpperCase() : "PERLUASAN PERTANGGUNGAN"}</b></h5></div>
                    {!this.state.isYearTwoExist && !this.state.isYearThreeExist ?
                      <div className="row separatorTop">
                        <div className="col-xs-4"></div>
                        <div className="col-xs-8">
                          <div className="col-xs-6 text-right" style={{ textAlign: "right" }}>TARIF</div>
                          <div className="col-xs-6 text-right panel-body-list" style={{ textAlign: "right" }}>PREMI</div>
                        </div>
                      </div>
                      : ""
                    }
                    {/* {!(this.state.isYearTwoExist || this.state.isYearThreeExist) ? 
                    <div>
                      <div className="row separatorTop">
                        <div className="col-xs-4">
                          <p><b> KENDARAAN </b></p>
                        </div>
                        <div className="col-xs-8 text-right">
                          <div className="col-xs-6" style={{ textAlign: "right" }}>
                            <p> {this.formatMoney(this.state.odcModel.CascoRate1, 2) + "%"} </p>
                          </div>
                          <div className="col-xs-6 panel-body-list" style={{ textAlign: "right" }}>
                            <p> {this.formatMoney((this.state.odcModel.CascoSI1 * this.state.odcModel.CascoRate1) / 100.0,0)} </p>
                          </div>
                        </div>
                      </div>
                      <div className="row separatorTop">
                        <div className="col-xs-4">
                          <p><b> AKSESORIS NON PREMI</b> </p>
                        </div>
                        <div className="col-xs-8 text-right">
                          <div className="col-xs-6" style={{ textAlign: "right" }}>
                            <p> {this.formatMoney(this.state.odcModel.CascoRate1, 2) + "%"} </p>
                          </div>
                          <div className="col-xs-6 panel-body-list" style={{ textAlign: "right" }}>
                            <p> {this.formatMoney((this.state.odcModel.CascoRate1 * this.state.odcModel.AccessSI1) / 100.0, 0)} </p>
                          </div>
                        </div>
                      </div>
                      <div className="row separatorTop">
                        <div className="col-xs-4">
                          <p><b> LOADING PREMI</b> </p>
                        </div>
                        <div className="col-xs-8 text-right">
                          <div className="col-xs-6" style={{ textAlign: "right" }}>
                            <p> {this.formatMoney(this.state.odcModel.LoadingRate1, 2) + "%"} </p>
                          </div>
                          <div className="col-xs-6">
                            <p> {""} </p>
                          </div>
                        </div>
                      </div>
                    </div> : ""} */}
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>SRCC, FLOOD &amp; WINDSTORM, ETV</b></p>
                      </div>
                      <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right" }}
                          >
                            <p>{this.state.odcModel.BundlingPremium5 > 0 ? (this.state.odcModel.IsBundling5 == 1 ? "Include" :  this.formatMoney(this.state.odcModel.BundlingPremium5,0)) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right" }}
                          >
                            <p>{this.state.odcModel.BundlingPremium4 > 0 ? (this.state.odcModel.IsBundling4 == 1 ? "Include" :  this.formatMoney(this.state.odcModel.BundlingPremium4,0)) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right" }}
                          >
                            <p>{this.state.odcModel.BundlingPremium3 > 0 ? (this.state.odcModel.IsBundling3 == 1 ? "Include" :  this.formatMoney(this.state.odcModel.BundlingPremium3,0)) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right" }}
                          >
                            <p>{this.state.odcModel.BundlingPremium2 > 0 ? (this.state.odcModel.IsBundling2 == 1 ? "Include" :  this.formatMoney(this.state.odcModel.BundlingPremium2,0)) : "No Cover"}</p>
                          </div>
                        }
                        {
                          this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list table-objek-left"
                            style={{ textAlign: "right" }}
                          >
                            <p>{this.state.odcModel.BundlingPremium1 > 0 ? (this.state.odcModel.IsBundling1 == 1 ? "Include" : this.formatMoney(this.state.odcModel.BundlingPremiumRate, 2) + "% " + this.state.odcModel.BundlingPremium1) : "No Cover"} </p>
                          </div>
                        }
                        {
                          !this.state.isYearTwoExist &&
                          <div>
                            <div
                              className="col-xs-6"
                              style={{ textAlign: "right" }}
                            >
                              <p>{this.state.odcModel.BundlingPremium1 > 0 ? (this.state.odcModel.IsBundling1 == 1 ? "Include" : this.formatMoney(this.state.odcModel.BundlingPremiumRate, 2) + "% " + this.state.odcModel.BundlingPremium1) : "No Cover"} </p>
                            </div>
                            <div
                              className="col-xs-6 panel-body-list"
                              style={{ textAlign: "right" }}
                            >
                              <p>{this.state.odcModel.BundlingPremium1 > 0 ? (this.state.odcModel.IsBundling1 == 1 ? "Include" : this.formatMoney(this.state.odcModel.BundlingPremiumRate, 2) + "% " + this.state.odcModel.BundlingPremium1) : "No Cover"} </p>
                            </div>
                          </div>
                        }
                      </div>
                    </div>
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>TERORISME &amp; SABOTASE</b></p>
                      </div>
                      <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TSPremium5 > 0 ?  this.formatMoney(this.state.odcModel.TSPremium5,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TSPremium4 > 0 ?  this.formatMoney(this.state.odcModel.TSPremium4,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TSPremium3 > 0 ?  this.formatMoney(this.state.odcModel.TSPremium3,0) : "No Cover"}</p>
                          </div>
                        }

                        {this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TSPremium2 > 0 ?  this.formatMoney(this.state.odcModel.TSPremium2,0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list table-objek-left"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TSPremium1 > 0 ? (!this.state.isYearTwoExist ? this.formatMoney(this.state.odcModel.TSPremiumRate, 2) + "% " : "") + this.formatMoney(this.state.odcModel.TSPremium1, 0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          !this.state.isYearTwoExist &&
                          <div>
                            <div
                              className="col-xs-6"
                              style={{ textAlign: "right"}}
                            >
                              <p>{this.formatMoney(this.state.odcModel.TSPremiumRate, 2) + "%"}</p>
                            </div>
                            <div
                              className="col-xs-6 panel-body-list"
                              style={{ textAlign: "right"}}
                            >
                              <p>{this.state.odcModel.TSPremium1 > 0 ? this.formatMoney(this.state.odcModel.TSPremium1, 0) : "No Cover"}</p>
                            </div>
                          </div>
                        }
                      </div>
                    </div>
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>TJH PIHAK KE 3</b></p>
                        <span>{this.state.odcModel.TPLSI > 0 ? "(" + this.formatMoney(this.state.odcModel.TPLSI,0) + ")" : ""}</span>
                      </div>
                      <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TPLPremium5 > 0 ? this.formatMoney(this.state.odcModel.TPLPremium5,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TPLPremium4 > 0 ? this.formatMoney(this.state.odcModel.TPLPremium4,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TPLPremium3 > 0 ? this.formatMoney(this.state.odcModel.TPLPremium3,0) : "No Cover"}</p>
                          </div>
                        }

                        {this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TPLPremium2 > 0 ? this.formatMoney(this.state.odcModel.TPLPremium2,0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list table-objek-left"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.TPLPremium1 > 0 ? (!this.state.isYearTwoExist && !this.state.isYearThreeExist && !this.state.isYearFourExist && !this.state.isYearFiveExist ? this.formatMoney(this.state.odcModel.TPLPremiumRate, 2) + "%" : "") + this.formatMoney(this.state.odcModel.TPLPremium1, 0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          !this.state.isYearTwoExist &&
                          <div>
                            <div
                              className="col-xs-6"
                              style={{ textAlign: "right"}}
                            >
                              <p>{this.formatMoney(this.state.odcModel.TPLPremiumRate, 2) + "%"}</p>
                            </div>
                            <div
                              className="col-xs-6 panel-body-list"
                              style={{ textAlign: "right"}}
                            >
                              <p>{this.state.odcModel.TPLPremium1 > 0 ? this.formatMoney(this.state.odcModel.TPLPremium1, 0) : "No Cover"}</p>
                            </div>
                          </div>
                        }
                      </div>
                    </div>
                    <div className="row separatorTop">
                      <div className="col-xs-4">
                        <p><b>PA DRIVER</b></p>
                        <span>{this.state.odcModel.TPLSI > 0 ? "(" + this.formatMoney(this.state.odcModel.PADRVRSI,0) + ")" : ""}</span>
                      </div>
                      <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PADRVRPremium5 > 0 ? this.formatMoney(this.state.odcModel.PADRVRPremium5,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PADRVRPremium4 > 0 ? this.formatMoney(this.state.odcModel.PADRVRPremium4,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PADRVRPremium3 > 0 ? this.formatMoney(this.state.odcModel.PADRVRPremium3,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PADRVRPremium2 > 0 ? this.formatMoney(this.state.odcModel.PADRVRPremium2,0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          this.state.isYearTwoExist &&
                        <div
                        className="col-xs-2 pull-right panel-body-list table-objek-left"
                        style={{ textAlign: "right"}}
                      >
                        <p>{this.state.odcModel.PADRVRPremium1 > 0 ? (!this.state.isYearTwoExist && !this.state.isYearThreeExist && !this.state.isYearFourExist && !this.state.isYearFiveExist ? this.formatMoney(this.state.odcModel.PADRVRPremiumRate, 2) + "%" : "") + this.formatMoney(this.state.odcModel.PADRVRPremium1, 0) : "No Cover"}</p>
                      </div>
                        }
                        {
                          !this.state.isYearTwoExist &&
                          <div>

                        <div
                          className="col-xs-6"
                          style={{ textAlign: "right"}}
                        >
                          <p>{this.formatMoney(this.state.odcModel.PADRVRPremiumRate, 2) + "%"}</p>
                        </div>
                        <div
                          className="col-xs-6 panel-body-list"
                          style={{ textAlign: "right"}}
                        >
                          <p>{this.state.odcModel.PADRVRPremium1 > 0 ? this.formatMoney(this.state.odcModel.PADRVRPremium1, 0) : "No Cover"}</p>
                        </div>
                            </div>
                        }
                      </div>
                    </div>

                    <div className="row separatorTop table-objek-bottom">
                      <div className="col-xs-4">
                        <p style={{ display: "inline-block"}}>PA Passenger (0.1 % of Limit)</p>
                      <span style={{ float:"right",display: "inline-block"}}>{this.state.VehicleSitting > 0 ? "(" + this.state.VehicleSitting + " Seat "/*this.formatMoney(this.state.VehicleSitting, 0)*/ + ")" : ""}</span>
                      </div>
                      <div className="col-xs-8 text-right">
                        {this.state.isYearFiveExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PAPASSPremium5 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium5,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearFourExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PAPASSPremium4 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium4,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearThreeExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PAPASSPremium3 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium3,0) : "No Cover"}</p>
                          </div>
                        }
                        {this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PAPASSPremium2 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium2,0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          this.state.isYearTwoExist &&
                          <div
                            className="col-xs-2 pull-right panel-body-list table-objek-left"
                            style={{ textAlign: "right"}}
                          >
                            <p>{this.state.odcModel.PAPASSPremium1 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium1,0) : "No Cover"}</p>
                          </div>
                        }
                        {
                          !this.state.isYearTwoExist &&
                          <div>
                          <div
                            className="col-xs-6"
                            style={{ textAlign: "right"}}
                          >
                            <p>{(!this.state.isYearTwoExist && !this.state.isYearThreeExist && !this.state.isYearFourExist && !this.state.isYearFiveExist ? this.formatMoney(this.state.odcModel.PAPASSPremiumRate, 2) + "%" : "")}</p>
                          </div>
                        <div
                          className="col-xs-6 panel-body-list"
                          style={{ textAlign: "right"}}
                        >
                          <p>{this.state.odcModel.PAPASSPremium1 > 0 ? this.formatMoney(this.state.odcModel.PAPASSPremium1,0) : "No Cover"}</p>
                        </div>
                            </div>
                        }
                      </div>
                    </div>

                  </fieldset>
                  <fieldset class="table-objek-pad">

<div className="row table-objek-bottom">
<div className="col-xs-4">
<p><b>Premi Perluasan</b></p>
</div>
{/*<div className="col-xs-8 text-right">
<p>{this.formatMoney(this.state.odcModel.ExtendPremium1 + this.state.odcModel.ExtendPremium2 +
this.state.odcModel.ExtendPremium3)}</p>
</div> */}
<div className="col-xs-8 text-right">
{this.state.isYearFiveExist &&
<div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
  <p><b>{this.formatMoney(this.state.odcModel.ExtendPremium5)}</b></p>
</div>
}
{this.state.isYearFourExist &&
<div className={(this.state.isYearFiveExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p><b>{this.formatMoney(this.state.odcModel.ExtendPremium4)}</b></p>
</div>
}
{this.state.isYearThreeExist &&
<div className={(this.state.isYearFourExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p><b>{this.formatMoney(this.state.odcModel.ExtendPremium3)}</b></p>
</div>
}
{this.state.isYearTwoExist &&
<div className={(this.state.isYearThreeExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p><b>{this.formatMoney(this.state.odcModel.ExtendPremium2)}</b></p>
</div>
}
<div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-right table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
  <p><b>{this.formatMoney(this.state.odcModel.ExtendPremium1)}</b></p>
</div>
</div>
</div>

<div className="row table-objek-bottom">
<div className="col-xs-4">
<p>Premi Dasar + Perluasan</p>
</div>
<div className="col-xs-8 text-right">
{this.state.isYearFiveExist &&
<div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
  <p>{this.state.odcModel.BExtendPremium5 > 0 ? (this.formatMoney(this.state.odcModel.BExtendPremium5)): "No Cover"}</p>
</div>
}
{this.state.isYearFourExist &&
<div className={(this.state.isYearFiveExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p>{this.state.odcModel.BExtendPremium4 > 0 ? (this.formatMoney(this.state.odcModel.BExtendPremium4)): "No Cover"}</p>
</div>
}
{this.state.isYearThreeExist &&
<div className={(this.state.isYearFourExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p>{this.state.odcModel.BExtendPremium3 > 0 ? (this.formatMoney(this.state.odcModel.BExtendPremium3)): "No Cover"}</p>
</div>
}
{this.state.isYearTwoExist &&
<div className={(this.state.isYearThreeExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p>{this.state.odcModel.BExtendPremium2 > 0 ? (this.formatMoney(this.state.odcModel.BExtendPremium2)) : "No Cover"}</p>
</div>
}
<div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-left table-objek-right" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
<p>{this.state.odcModel.BExtendPremium1 != 0 ? (this.formatMoney(this.state.odcModel.BExtendPremium1)) : "No Cover"}</p>
</div>
</div>
</div>

<div className="row">
<div className={"col-xs-7"}>
<p>Biaya Administrasi</p>
</div>
<div className={"col-xs-5"}>
<p style={(this.state.isYearTwoExist) ? { textAlign: "right", marginLeft: "15%" } : {textAlign: "left", marginLeft: "12%"}}>{this.formatMoney(this.state.datas.AdminFee)}</p>
</div>
</div>

<div class="row separatorTop backgroundgrey table-objek-bottom table-objek-top">
<div className="col-xs-4">
<p><b>TOTAL PREMIUM + Admin</b></p>
</div>
<div className="col-xs-8 text-right">
{this.state.isYearFiveExist &&
<div className="col-xs-2 pull-right panel-body-list" style={{ textAlign: "right"}}>
  <p><b>{this.state.odcModel.TotalPremium5 > 0 ? (this.formatMoney(this.state.odcModel.TotalPremium5)): "No Cover"}</b></p>
</div>
}
{this.state.isYearFourExist &&
<div className={(this.state.isYearFiveExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p><b>{this.state.odcModel.TotalPremium4 > 0 ? (this.formatMoney(this.state.odcModel.TotalPremium4)): "No Cover"}</b></p>
</div>
}
{this.state.isYearThreeExist &&
<div className={ (this.state.isYearFourExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"}style={{ textAlign: "right"}}>
  <p><b>{this.state.odcModel.TotalPremium3 > 0 ? (this.formatMoney(this.state.odcModel.TotalPremium3)) : "No Cover"}</b></p>
</div>
}
{this.state.isYearTwoExist &&
<div className={(this.state.isYearThreeExist) ? "col-xs-2 pull-right panel-body-list table-objek-right" : "col-xs-2 pull-right panel-body-list"} style={{ textAlign: "right"}}>
  <p><b>{this.state.odcModel.TotalPremium2 > 0 ? (this.formatMoney(this.state.odcModel.TotalPremium2)) : "No Cover"}</b></p>
</div>
}
<div className={(this.state.isYearTwoExist) ? "col-xs-2 pull-right panel-body-list table-objek-right table-objek-left" : "col-xs-12 pull-right panel-body-list table-objek-left"} style={{ textAlign: "right"}}>
<p><b>{this.state.odcModel.TotalPremium1 != 0 ? (this.formatMoney(this.state.odcModel.TotalPremium1)) : "No Cover"}</b></p>
</div>
</div>
</div>
</fieldset>
</div>
                  <fieldset>
                    <div className="row backgroundgrey"><h5 className="col-xs-12"><b>FITUR LAYANAN</b></h5></div>
                    {fiturlayanan}
                  </fieldset>
                  <fieldset>
                    <div className="row backgroundgrey"><h5 className="col-xs-12"><b>CATATAN</b></h5></div>
                    <ul style={{ paddingLeft: "20px", paddingRight: "20px" }}>
                      {this.state.datas.ComprePeriod > 0 &&
                        <li>
                          Pertanggungan Comprehensive menjamin resiko terhadap
                          kerugian / kerusakan sebagian maupun total akibat risiko
                          yang dijamin dalam Polis Asuransi Kendaraan Bermotor
                      </li>
                      }
                      {this.state.datas.TLOPeriod > 0 &&
                        <li>
                          Pertanggungan TLO menjamin penggantian apabila terjadi
                          kehilangan atau kerusakan dengan biaya perbaikan minimal
                          75% dari harga kendaraan
                      </li>
                      }
                      <li>
                        Harga pertanggungan dapat berubah sesuai dengan harga
                        pasar yang berlaku
                      </li>
                      <li>
                        Loading dikenakan atas kendaraan dengan usia kendaraan >
                        5 tahun
                      </li>
                      <li>
                        Risiko sendiri untuk kendaraan @basicOwnRisk per
                        kejadian
                      </li>
                      <li>
                        Risiko sendiri untuk perluasan (@coverOwnRisk)
                        @extendOwnRisk
                      </li>
                      <li>
                        Harga pertanggungan sesuai dengan Standar Harga yang
                        berlaku di Asuransi Astra atau Faktur Pembelian
                        Kendaraan unit baru dari Dealer ATPM
                      </li>
                      <li>
                        Penawaran ini berlaku selama 7 hari kalender sejak
                        tanggal diterbitkan, segera hubungi petugas kami untuk
                        Asuransi kendaraan Anda
                      </li>
                      <li>
                        Pertanggungan asuransi berlaku sejak tanggal survei
                        dilakukan atau dokumen lengkap diterima (untuk unit baru
                        dari Dealer ATPM)
                      </li>
                      <li>
                        Pembayaran Premi dilakukan melalui Virtual Account
                        Asuransi Astra paling lambat 14 hari kalender sejak
                        Pertanggungan asuransi berlaku
                      </li>
                      <li>
                        Loading premi dikenakan atas kendaraan dengan
                        pertanggungan comprehensive dan usia kendaraan > 5 tahun
                      </li>
                      <li>
                        Risiko Sendiri untuk kendaraan Rp 300.000,- per Kejadian
                      </li>
                      <li>
                        Risiko Sendiri untuk Perluasan (SRCC, Flood &amp;
                        Windstorm, ETV, TS) 10% dari nilai kerugian minimum Rp
                        500.000,- per Kejadian
                      </li>
                    </ul>
                  </fieldset>
                  <fieldset>
                    <div className="row backgroundgrey"><h5 className="col-xs-12"><b>INFORMASI</b></h5></div>
                    <p>
                      {this.state.BranchData != null &&
                      <span>
                      Untuk informasi lebih lanjut, silakan menghubungi kami di :
                      <br />
                      Asuransi Astra Cabang {this.state.BranchData.Name}
                      <br />
                      {this.state.BranchData.Address}
                      <br />
                      Telp: {this.state.BranchData.Phone + "| "} Fax: {this.state.BranchData.Fax}
                      <br />
                      HP: {JSON.parse(ACCOUNTDATA).UserInfo.User.Phone1}
                      <br />
                      </span>
                      }
                      Garda Akses CALL 24 jam di 1 500 112, email :<br />
                      <span style={{ color: "#0eace6" }}>customer_service@asuransiastra.com</span>
                      <br />
                      Demikian penawaran ini kami sampaikan, atas perhatiannya
                      kami ucapkan terima kasih.
                      <br />
                    </p>
                  </fieldset>
                  <div className="row">
                    <div className="col-md-12">
                      <p className="text-center">Hormat Kami</p>
                      <br />
                      <br />
                      <p className="text-center"><span style={{ borderBottom: "1px solid #333", padding: "0px 0px" }}>{JSON.parse(ACCOUNTDATA).UserInfo.User.Name}</span><br />{this.state.param.Value}</p>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageQuotationDetails);
