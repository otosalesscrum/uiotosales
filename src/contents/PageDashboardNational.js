import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { OtosalesSelect2, OtosalesModalDashboard, OtosalesListDashboard } from "../otosalescomponents";
import BlockUi from "react-block-ui";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { ToastContainer } from "react-toastify";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";


class PageDashboardNational extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ViewCode: "0",
      PeriodCode: "Monthly",
      ViewList: [
        { value: "0", label: "Total Prospect" },
        { value: "1", label: "Total Need FU" },
        { value: "2", label: "Total Deal" },
        { value: "3", label: "Total Potential" },
        { value: "4", label: "Total Call Later" },
        { value: "5", label: "Total Not Deal" },
        { value: "6", label: "Total Send To SA" },
        { value: "7", label: "Total Collect Doc" },
        { value: "8", label: "Total Back To AO" },
        { value: "9", label: "Total Policy Created" }
      ],
      PeriodList: [
        { value: "Monthly", label: "Month to Date" },
        { value: "Yearly", label: "Year to Date" }
      ],
      fuModel: [],
      aabHeads: [],
      isLoading: false,
      datasList: [],
      Param: "",
      showModal: false,
      showModalFilter: false

    }

  }
  componentDidMount() {
    this.getDataDashboard();
  }
  setEmailDefault() {
    var email = "";
    if (this.props.userInfo.Role == "NATIONALMGR" || this.props.userInfo.Role == "BRANCHSUPPORT" || this.props.userInfo.Role == "REGIONALMGR" || this.props.userInfo.User.Role == "BRANCHMGR" || this.props.userInfo.User.Role == "SALESSECHEAD") {
      email = this.props.userInfo.User.Email;
    } else {
      email = this.props.userInfo.User.Email + "; " + this.props.userInfo.ReportTo;
    }
    // console.log("ahay" + email);
    return email;
  }
  onClickListItems = (Param) => {

    secureStorage.setItem("NatStepParam", JSON.stringify(Param));
    secureStorage.setItem("NatStepRole", JSON.stringify("REGIONALMGR"));
    this.props.history.push("/dashboard-Regional");
  }
  onLoading = isLoading => {
    this.setState({ isLoading });
  }
  getDataDashboard = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getDashboard/`, {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "salesofficerid=" + this.props.username + "&periodView=" + this.state.PeriodCode + "&userRole=" + this.props.userInfo.User.Role + "&StateDashboard=" + this.props.userInfo.User.Role
        + "&Channel=" + this.props.userInfo.User.Channel + "&ChannelSource=" + this.props.userInfo.User.ChannelSource + "&param=" + this.state.Param
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          fuModel: jsn.fuModel,
          aabHeads: jsn.aabHeads,
          isLoading: false
        })
      }
      )
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.getDataDashboard();
        }
      });


  };

  setDate = () => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
      "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    const d = new Date();
    return monthNames[d.getMonth()] + " " + d.getFullYear();
  }
  onOptionViewChange = ViewCode => {
    this.setState({ ViewCode });
  }
  onCloseModal = () => {
    this.setState({ showModalFilter: false });
  };
  onModalChange = showModal => {
    this.setState({ showModal });
  }
  onOptionPeriodChange = PeriodCode => {
    secureStorage.setItem("Period", JSON.stringify(PeriodCode));
    this.setState({ PeriodCode }, () => {
      this.getDataDashboard();
    });

  }
  render() {
    return (
      <div>
        <ToastContainer/>
        <Header titles="National Dashboard">
      <ul class="nav navbar-nav navbar-right">
          <li>
            <a 
            className="fa fa-envelope fa-lg"
            onClick={() => this.setState({ showModalFilter: true })}
             />
          </li>
        </ul>
      </Header>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            className="col-xs-12 panel-body-list"
            blocking={this.state.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >
            <OtosalesModalDashboard
              showModal={this.state.showModalFilter}
              onModalChange={this.onModalChange}
              onCloseModal={this.onCloseModal}
              Role={this.props.userInfo.User.Role}
              Email={this.props.userInfo.User.Email}
              ReportTo={this.props.userInfo.ReportTo}
              EmailDefault={this.setEmailDefault()}
              SalesOfficer={this.props.userInfo.User.SalesOfficerID}
              PeriodCode={this.state.PeriodCode}
              Loading={this.onLoading}
            />
            <div className="">
              <div className="panel-heading" style={{ background: "#eaeaea" }}>
                <div class="row">
                  <div class="col-md-4 col-xs-6" style={{ float: "left" }}>
                    <p style={{ textAlign: "left", color: "#606060" }}><b>NEW PROSPECT</b></p>
                  </div>
                  <div class="col-md-4 col-xs-6" style={{ float: "right" }}>
                    <p style={{ textAlign: "right", color: "#606060" }} id="textDate"><b> {this.setDate()}</b></p>
                  </div>
                </div>
              </div>
              <div className="panel-body">
                <div class="row">
                  <div class="col-md-4 col-xs-6" style={{ float: "left" }} >
                    <p>VIEW</p>
                    <div className="input-group">
                      <OtosalesSelect2
                        value={this.state.ViewCode}
                        selectOption={this.state.ViewCode}
                        onOptionsChange={this.onOptionViewChange}
                        options={this.state.ViewList}
                      />
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-6" style={{ float: "right" }} >
                    <p>PERIOD</p>
                    <div className="input-group">
                      <OtosalesSelect2
                        value={this.state.PeriodCode}
                        selectOption={this.state.PeriodCode}
                        onOptionsChange={this.onOptionPeriodChange}
                        options={this.state.PeriodList}
                      />
                    </div>
                  </div>
                </div>
                <div className="row" style={{ marginTop: "20px" }}>
                  {this.state.fuModel.length > 0 &&
                    <OtosalesListDashboard
                      onClickListItems={this.onClickListItems}
                      rowscount={this.state.fuModel[0].length}
                      viewFilter={this.state.ViewCode}
                      dataitems={this.state.fuModel[0]}
                      dataitems2={this.state.aabHeads}
                      Role={this.props.userInfo.User.Role}
                    />
                  }
                </div>
              </div>
            </div>

          </BlockUi>
        </div>
      </div>
    );
  }
}

export default withRouter(PageDashboardNational);
