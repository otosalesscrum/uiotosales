import React, { Component } from "react";
import Header from "../../../components/Header";
import { ToastContainer } from "react-toastify";
import { Util } from "../../../otosalescomponents";
import {
  API_URL,
  ACCOUNTDATA,
  API_VERSION_0213URF2019,
  HEADER_API
} from "../../../config";
import log from "loglevel";
import { withRouter } from "react-router-dom";
import StackTrace from "stacktrace-js";
import * as Config from "../../../config";

window.onerror = function(msg, file, line, col, error) {
  StackTrace.fromError(error).then(err => {
    let errorFrom =  {
      type: "window.onerror",
      url: window.location.href,
      userId: window.userId,
      agent: window.navigator.userAgent,
      date: new Date(),
      msg: msg,
      file,
      line,
      col
    }
    console.log("error raw window.onerror : ", err);
    Util.SomeErrorReportingTool(error, null, err, errorFrom);
  });
};

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      info: "",
      error: ""
    };
  }

  componentDidCatch(error, info) {
    // if(Config.IS_ACTIVE_ERROR_BOUNDARY){
      this.setState({ hasError: true, info, error });
    // }
    console.log(`Error: ${error}`);
    console.log(`ErrorInfo: ${JSON.stringify(info)}`);
    StackTrace.fromError(error).then(err => {
      let errorFrom = {
        type: "ErrorBoundary",
        url: window.location.href,
        userId: window.userId,
        agent: window.navigator.userAgent,
        date: new Date()
      };
      console.log("error raw ErrorBoundary : ", err);
      Util.SomeErrorReportingTool(error, info, err, errorFrom);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.location.pathname != nextProps.location.pathname &&
      this.state.hasError
    ) {
      this.setState({ hasError: false });
    }
  }

  render() {
    return this.state.hasError ? (
      <React.Fragment>
        <Header />
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <div className="panel panel-default">
            <div className="error-container">
              <div className="row error-header">
                <div className="col-md-8 col-xs-7">
                  <span className="error-title">
                    Oops! <br /> Something went wrong <br />{" "}
                    <b>Please refresh page</b>
                  </span>
                </div>
                <div className="col-md-4 col-xs-5">
                  <span className="error-main-icon">
                    <i className="fa fa-meh-o" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    ) : (
      this.props.children
    );
  }
}

export default withRouter(ErrorBoundary);
