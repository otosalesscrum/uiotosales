import React, { Component } from "react";
import Footer from "../../../components/Footer";
import { withRouter } from "react-router-dom";
import { OtosalesListOrderApproval, Util, Log } from "../../../otosalescomponents";
// import { API_URL, API_VERSION, API_V2 } from "../../../config";
import * as Config from "../../../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../../../components/Header";
import { ToastContainer, toast } from "react-toastify";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";

class PageOrderApproval extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isLoadFromAPI: true,
      datas: [
        {
            Id: 1,
            ItemInfo: {
              "OrderNo": "",
              "ApprovalType": "",
              "ApprovalDescription": "",
              "OrderType": "",
              "Name": "",
              "PayToCode": "",
              "PayToName": "",
              "DealerName": "",
              "SalesmanDealerName": "",
              "SalesmanBankname": "",
              "SalesmanAccountNo": 0,
              "VehicleDescription": 0,
              "TSI": 0,
              "Premi": 0,
              "IsPSTB": 1,
              "IsSalesman": 0,
              "IsDelivery":0
            },
            IsSo: 0
        }
    ]
    };
  }

  cleanLastClick = () => {
    secureStorage.removeItem("OrderIdOA");
    secureStorage.removeItem("OrderNoOA");
    secureStorage.removeItem("ApprovalType");
    secureStorage.removeItem("IsSOOA");
  }

  componentDidMount(){
    this.cleanLastClick();
    if(secureStorage.getItem("orderapprovalnotif") != undefined){
      let notifOrderApproval = secureStorage.getItem("orderapprovalnotif").split("-");
      Log.debugGroup("NOTIF ORDER APPROVAL => ", notifOrderApproval);
      Log.debugStr("if success => " + notifOrderApproval[0].includes("success"));
      if(notifOrderApproval[0].includes("success")){
        if(notifOrderApproval[1].toUpperCase() == ("Order Approved").toUpperCase()){
          toast.info("✔️ " + notifOrderApproval[1], {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }else if(notifOrderApproval[1].toUpperCase() == ("Order Revised").toUpperCase()) {
          toast.info("❗" + notifOrderApproval[1], {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.info("❗ Order Rejected", {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        
      }else{
        toast.warning("❗ " + notifOrderApproval[1], {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      }
      secureStorage.removeItem("orderapprovalnotif");
    }
    if (this.state.isLoadFromAPI) {
      this.searchDataHitApi();
    }
  }

  checkStateSO = (type) => {
    return (type == "NEXTLMT")? 0: 1
  }

  searchDataHitApi = () => {
    const { search } = this.state;
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${Config.API_URL+""+Config.API_V2}/OrderApproval/populateOrderApproval`,
      "POST",
      Config.HEADER_API,
      {
        UserID: this.props.username
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.Data.map((data, index) => ({
          Id: `${index}`,
          ItemInfo: data,
          IsSo: this.checkStateSO(data.ApprovalType)
        }))
      )
      .then(datamapping => {
        this.setState({
          datas: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        // console.log("parsing failed", error);
        Log.error("on SearchHitDataApi fetch(/populateOrderApproval): ", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  }

  onclickItem = (data) => {
    // UNUSED ! ! !

    // secureStorage.setItem("OrderIdOA", data.id);
    // secureStorage.setItem("NameOA", data.name);
    // secureStorage.setItem("OrderNoOA", data.orderno);
    // secureStorage.setItem("IsSOOA", data.IsSo);
    // console.log(data);
    // secureStorage.setItem("OrderIdOA", data.Id);
    // secureStorage.setItem("OrderNoOA", data.OrderNo);
    // secureStorage.setItem("ApprovalType", data.ApprovalType);
  }

  
  render() {
    // this.setState({isLoading: true});
    return (
      <div>
        <Header titles="Order Approval"/>
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
                      tag="div"
                      blocking={this.state.isLoading}
                      loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
                      message="Loading, please wait"
                      keepInView
                    >
              <div style={this.state.isLoading? {paddingTop: "35%", paddingBottom: '50%'} : {}}>
                <div className="panel-body panel-body-list" id="Datalist">
                  <OtosalesListOrderApproval
                    onclickItem = {this.onclickItem}
                    name="sampleList"
                    rowscount={this.state.datas.length}
                    dataitems={this.state.datas}
                  />
                </div>
              </div>
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageOrderApproval);
