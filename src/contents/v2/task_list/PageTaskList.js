import React, { Component } from "react";
import Footer from "../../../components/Footer";
import { A2isText, A2isCheckbox, A2isCombo } from "../../../components/a2is";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
// import { TaskListAdapter } from "../../../components/a2is.js";
import { OtosalesModalTaskListFilter, AdapterTaskList, AdapterTaskListLead, Util, Log, OtosalesLoading } from "../../../otosalescomponents";
import { API_URL, API_VERSION, API_VERSION_2, API_RETRY_LIMIT, ACCOUNTDATA } from "../../../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../../../components/Header";
import { VIEW_VERSION } from "../Version"
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";

const PAGE_TITLE = "Task List";
const SIZE_PAGING = 20;
class PageTaskList extends Component {

//flag
  constructor(props) {
    super(props);
    this.state =
      !Util.isNullOrEmpty(secureStorage.getItem("StateTaskList"))
        ? JSON.parse(secureStorage.getItem("StateTaskList"))
        : {
            isSearchActive: false,
            indexOrderByItem: "Status",
            indexOrderByTypeItem: 1,
            chFollowUpMonthlyChecked: false,
            chFollowUpDateChecked: true,
            chNeedFUChecked: true,
            chPotentialChecked: true,
            chCallLaterChecked: true,
            chCollectDocChecked: true,
            chOrderRejectedChecked: true,
            chSentToSAChecked: true,
            chBackToAOChecked: true,
            chNotDealChecked: false,
            chPolicyCreatedChecked: false,
            chNewChecked: true,
            chRenewableChecked: true,
            getFollowUpDate:
              Util.convertDate().getFullYear() +
              "-" +
              (Util.convertDate().getMonth() + 1) +
              "-" +
              Util.convertDate().getDate(),
            tmpsearchterm: "",
            searchterm: "",
            isWaitingSearchResult: false,
            isNewSearchParam: false,
            isLoading: false,
            showModalFilter: false,
            dataItemsLead: [],
            dataItemsAll: [],
            dataItems: [
              // {
              //   id: "1",
              //   name: "Loading Data . . . . .",
              //   lastfollow: "loading . . . .",
              //   nextfollow: "still loading . . . .",
              //   status: "1"
              // },
              // {
              //   id: "2",
              //   name: "Loading Data . . . . .",
              //   lastfollow: "loading . . . .",
              //   nextfollow: "still loading . . . .",
              //   status: "2"
              // }  
            ],
            datatasklist: [],
            historyPenawaranList: [],
            penawaranList: [],
            prospectLeadList: [],

            chIsSearchbyProsName: true,
            chIsSearchbyEngNo: false,
            chIsSearchbyChasNo: false,
            chIsSearchbyPolicyNo: false,
            chType: 1,
            chStrVal: "prn",
            chItems: [ 
              {
                text: "Prospect Name",
                value: "prn"
              },
              {
                text: "Engine No",
                value: "enn"
              },
              {
                text: "Chasis No",
                value: "csn"
              },
              {
                text: "Policy No",
                value: "pcn"
              }
            ],
            currentPage: 1,
            totalPage: 1,
            isLastPage: false,
            isLoadingScroll: false
          };
  }

  componentDidMount() {
    this.searchDataAPI(this.state.searchterm, 0);
    secureStorage.removeItem("tasklist-followupdetailstatusphone");
  }

  onChangeFunction = event => {

    if (event.target.name == "searchterm") {
      this.setState({
        ["tmp"+event.target.name]: event.target.value
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value
      });
    }
  };

  _handleKeyDown = (e) => {
    // console.log(e)
    // handle on click enter keyboard
    if (e.key === 'Enter') {
      console.log('do validate');
      // this.searchDataHitApi(this.state.searchterm, 0);
      var tmpsearchterm = this.state.tmpsearchterm;
      this.setState({
        searchterm: tmpsearchterm
      }, () => {
        this.searchDataByParam(this.state.searchterm, true);
      });
    }
  }

  handleScroll = e => {
    let element = e.target;
    var maxScroll = e.target.scrollHeight - e.target.offsetHeight - 50;
    // console.log(e.nativeEvent);
    // console.log("Max Scroll = " + maxScroll.toString());
    this.scrollPaging(element.scrollTop, maxScroll);

    console.log(element.scrollTop);
  };

  scrollPaging = (scrollVal, maxVal) => {
    if(scrollVal >= maxVal && !this.state.isLoadingScroll && !this.state.isLastPage) {
      var currPage = this.state.currentPage;
      currPage++;
      console.log("getting scroll into Page -> " + currPage.toString());
      this.setState({ isLoadingScroll: true , currentPage: currPage}, () => {
        this.searchDataAPI(this.state.searchterm, 0);
      });
    }
  };

  searchDataByParam = (txtsearch, isNewSearch, trycount = 0) => {
    if(!Util.isNullOrEmpty(txtsearch) && !this.state.isLoading) {
      var param = {
            searchterm: txtsearch,
            chIsSearchbyProsName: false,
            chIsSearchbyEngNo: false,
            chIsSearchbyChasNo: false,
            chIsSearchbyPolicyNo: false,
            chStrVal: this.state.chStrVal,
            // Make data into Default
            datatasklist: [],
            dataItems: [],
            dataItemsLead: [],
            datatasklist: [],
            historyPenawaranList: [],
            penawaranList: [],
            prospectLeadList: [],
            // Paging setting
            currentPage: 1,
            totalPage: 1,
            isLastPage: false,
            isNewSearchParam: isNewSearch
      };
      switch(param.chStrVal) {
        case "prn": {
          param.chIsSearchbyProsName = true;
          break;
        }
        case "enn": {
          param.chIsSearchbyEngNo = true;
          break;
        }
        case "csn": {
          param.chIsSearchbyChasNo = true;
          break;
        }
        case "pcn": {
          param.chIsSearchbyPolicyNo = true;
          break;
        }

      }
      this.setState(param, () => {
        if (!this.state.isWaitingSearchResult) {
          this.searchDataAPI(txtsearch, ++trycount);
        }
      });
    } else return;
  };

  searchDataAPI = (txtsearch, trycount) => {
    if (trycount > API_RETRY_LIMIT) {
        console.log("API '/DataReact/getTaskList/' RETRY LIMIT REACH!!");
        return;
    }
    var isneedretry = false;
    var isloadingdismissneed = false; 
    console.log("fetch API PAGE -> " + this.state.currentPage);
    this.setState({ isLoading: true, isWaitingSearchResult: true});
    Util.fetchAPI(`${API_URL + "" + API_VERSION_2}/DataReact/getTaskList/`, "POST", 
    { // Header
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
    }, { // Body
      salesofficerid: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      allowSearchOthers: JSON.parse(ACCOUNTDATA).UserInfo.User.IsCSO,
      indexOrderByItem: this.state.indexOrderByItem,
      indexOrderByTypeItem: this.state.indexOrderByTypeItem,
      chFollowUpMonthlyChecked: this.state.chFollowUpMonthlyChecked || false,
      chNeedFUChecked: this.state.chNeedFUChecked || false,
      chPotentialChecked: this.state.chPotentialChecked || false,
      chCallLaterChecked: this.state.chCallLaterChecked || false,
      chCollectDocChecked: this.state.chCollectDocChecked || false,
      chOrderRejectedChecked: this.state.chOrderRejectedChecked || false,
      chNotDealChecked: this.state.chNotDealChecked || false, 
      chSentToSAChecked: this.state.chSentToSAChecked || false,
      chBackToAOChecked: this.state.chBackToAOChecked || false,
      chPolicyCreatedChecked: this.state.chPolicyCreatedChecked || false,
      chFollowUpDateChecked: this.state.chFollowUpDateChecked || false,
      getFollowUpDate: this.state.getFollowUpDate +" 23:59:59.725" || false,
      chIsSearchbyEngNo: this.state.chIsSearchbyEngNo || false,
      chIsSearchbyChasNo: this.state.chIsSearchbyChasNo || false,
      chIsSearchbyPolicyNo: this.state.chIsSearchbyPolicyNo || false,
      chIsSearchbyProsName: this.state.chIsSearchbyProsName || false,
      search: txtsearch,
      currentPage: this.state.currentPage,
      PageSize: SIZE_PAGING,
      PageSizeLead: SIZE_PAGING,
      chNewChecked: this.state.chNewChecked || false,
      chRenewableChecked: this.state.chRenewableChecked || false
    })
    .then(res => res.json())
    .then(json => {

        var dtState = this.state.datatasklist;
        dtState.push({
          CurrentPage: json.data.CurrentPage,
          Items: json.data.Items
        });
        var historyState = this.state.historyPenawaranList.concat(json.historyPenawaranList);
        var penawaranState = this.state.penawaranList.concat(json.penawaranList);
        var prospectState = this.state.prospectLeadList.concat(json.prospectLeadList);
        this.setState({
          datatasklist: dtState,
          totalPage: json.data.TotalPages,
          historyPenawaranList: historyState,
          penawaranList: penawaranState,
          prospectLeadList: prospectState,
          isLastPage: (json.data.CurrentPage == json.data.TotalPages)
        });
        // return jsn.data.Items.map((data, i) => ({
        //   id: `${data.FollowUpNo}`,
        //   name: `${data.ProspectName}`,
        //   lastfollow: `${data.LastFollowUpDate}`,
        //   nextfollow: `${data.NextFollowUpDate}`,
        //   status: `${data.FollowUpStatus}`,
        //   CustID: `${data.CustID}`,
        //   datafollowup: data
        // }));
        // return json.data.Items;
        return json
    })
    .then(json => {
        var dtItemState = this.state.dataItems.concat(json.data.Items);
        var dtItemStateLead = this.state.dataItemsLead;
        // if(!Util.isNullOrEmpty(json.prospectLeadList.Items)){
        //   dtItemStateLead = [...dtItemStateLead, json.prospectLeadList.Items];
        // }
        // if(json.RenewalNonRenNotList.Items.length > 0){
        //   dtItemState = [...dtItemState, json.RenewalNonRenNotList.Items];
        // }

        // console.log("current STATE in API");
        // console.log(dtItemState);
        // if (dtItemState.length > 0) {
        //   console.log("CONCATE RESULT FROM API");
        //   console.log(data);
        //   dtItemState.concat(data);
        // } else {
        //   dtItemState = data;
        // }
        isloadingdismissneed = true;
        this.setState({
          dataItems: dtItemState,
          dataItemsLead: dtItemStateLead
        });
    })
    .catch(error => {
      // this.setState({
      //   isLoading: false
      // });

        isloadingdismissneed = true;
      // TODO LCY: is this API set error message?
        isneedretry = true;
      if(!(error+"").toLowerCase().includes("token")){
        isloadingdismissneed = false;
        setTimeout(() => {
          this.searchDataAPI(txtsearch, ++trycount);
        }, 2000);
      }
    })
    .finally( () => {
      // var isloading = true; 
      // // wrong concurency
      // if (isloadingdismissneed && !this.state.isNewSearchParam) {
      //   isloading = false;
      // }
      var isnewsearch = this.state.isNewSearchParam;
      this.setState({ isLoading: false, isLoadingScroll: false, isWaitingSearchResult: false, isNewSearchParam: false }, ()=>{
        Log.debugStr("ISNEEDRETRY => " + isneedretry);
        Log.debugStr("ISNEWSEARCH => " + isnewsearch);
        if(isneedretry && !isnewsearch) {
          this.searchDataByParam(this.state.searchterm, false, ++trycount);
        }
      });
      console.log(this.state.dataItems)
    });

  };

  onClickItem = FollowUpNo => {
    // console.log("Clicked "+FollowUpNo);
    const datataskclicked = this.state.datatasklist.filter((data, i) => {
      return data.FollowUpNo == FollowUpNo;
    });
    // console.log(datataskclicked[0]);
    secureStorage.setItem("StateTaskList", JSON.stringify(this.state));
    secureStorage.setItem("FUSelected", JSON.stringify(datataskclicked[0]));
    this.props.history.push("/tasklist-followupdetail");
  };

  onClickListItem = CustID => {
    // console.log("masuk", CustID);
    // let selectedData = [...this.state.dataItems].filter(data => data.CustID == CustID)[0] ? [...this.state.dataItemsLead].filter(data => data.CustID == CustID)[0] : "";
    let selectedData = [...this.state.dataItems, ...this.state.dataItemsLead].filter(data => data.CustID == CustID)[0];
    
    console.log(selectedData);
    secureStorage.setItem("SelectedTaskDetail", JSON.stringify({
        CustID : selectedData.CustID,
        FollowUpNo : selectedData.FollowUpNo,
        PolicyNo : selectedData.PolicyNo,
        IsRenewalNonRenNot : selectedData.IsRenewalNonRenNot
      }
    ));

    // this.searchProspectCustomer(CustID);

    // console.log(datafollowup);
    // secureStorage.setItem("FUTaskListEdit", JSON.stringify(datafollowup));
    this.props.history.push("/tasklist-details");
  };

  searchProspectCustomer = CustID => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getProspectCustomerEdit/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "custid=" + CustID
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        if (jsn.status == 1) {
          secureStorage.setItem(
            "ProspectCustomerSelected",
            JSON.stringify(jsn.ProspectCustomer)
          );
          secureStorage.setItem(
            "FollowUpSelected",
            JSON.stringify(jsn.FollowUp)
          );
          secureStorage.setItem(
            "OrderSimulationSelected",
            JSON.stringify(jsn.OrderSimulation)
          );
          secureStorage.setItem(
            "OrderSimulationMVSelected",
            JSON.stringify(jsn.OrderSimulationMV)
          );
          secureStorage.setItem(
            "OrderSimulationInterestSelected",
            JSON.stringify(jsn.OrderSimulationInterest)
          );
          secureStorage.setItem(
            "OrderSimulationCoverageSelected",
            JSON.stringify(jsn.OrderSimulationCoverage)
          );
          secureStorage.setItem(
            "ImageDataSelected",
            JSON.stringify(jsn.imageData)
          );
          this.props.history.push("/tasklist-edit");
        } else {
          toast.dismiss();
          toast.error("❗ " + jsn.Message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"").toLowerCase().includes("token")){
         this.searchProspectCustomer(CustID);
        }
      });
  };

  onCloseModal = () => {
    this.setState({ showModalFilter: false });
  };

  onSubmitFilter = filter => {
    const {
      chBackToAOChecked,
      chCallLaterChecked,
      chCollectDocChecked,
      chOrderRejectedChecked,
      chFollowUpDateChecked,
      chFollowUpMonthlyChecked,
      chNeedFUChecked,
      chNotDealChecked,
      chPolicyCreatedChecked,
      chPotentialChecked,
      chSentToSAChecked,
      itemModelsSelected,
      spOrderByTypeItem,
      lastFollowUpDate,
      chNewChecked,
      chRenewableChecked

    } = filter;

    this.setState(
      {
        showModalFilter: false,
        dataItems: [],
        dataItemsLead: [],
        chBackToAOChecked,
        chCallLaterChecked,
        chCollectDocChecked,
        chOrderRejectedChecked,
        chFollowUpDateChecked,
        chFollowUpMonthlyChecked,
        chNeedFUChecked,
        chNotDealChecked,
        chPolicyCreatedChecked,
        chPotentialChecked,
        chNewChecked,
        chRenewableChecked,

        chSentToSAChecked,
        currentPage: 1,
        totalPage: 1,
        isLastPage: false,
        indexOrderByItem: itemModelsSelected,
        indexOrderByTypeItem: spOrderByTypeItem,
        getFollowUpDate:
          lastFollowUpDate.getFullYear() +
          "-" +
          (lastFollowUpDate.getMonth() + 1) +
          "-" +
          lastFollowUpDate.getDate()
      },
      () => {
        // this.searchDataHitApi(this.state.searchterm, 0);
        this.searchDataAPI(this.state.searchterm, 0);
      }
    );
  };

  onChangeCombo = (val) => {
    console.log("search by = " + val);
    this.setState({ chStrVal: val }, () => {
      this.searchDataByParam(this.state.searchterm, true);
    });
  }

  render() {
    return (
      <div onScroll={this.handleScroll}>
        <Header titles={PAGE_TITLE}>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-filter fa-lg"
                onClick={() => this.setState({ showModalFilter: true })}
              />
            </li>
            <li>
              <a
                className="fa fa-search fa-lg"
                onClick={() =>
                  this.setState({ isSearchActive: !this.state.isSearchActive })
                }
              />
            </li>
          </ul>
        </Header>
        <ToastContainer />
        <OtosalesModalTaskListFilter
          showModal={this.state.showModalFilter}
          onCloseModal={this.onCloseModal}
          onSubmitFilter={this.onSubmitFilter}
        />
        <div className="content-wrapper" style={{ padding: "10px" }} version={VIEW_VERSION}>
          {/* <div className="panel panel-primary"> */}
          <div className="panel-body panel-body-list" id="Datalist">
            {this.state.isSearchActive && (
              <div className="col-xs-12 col-md-4 pull-right panel-body-list">
                <div className="col-xs-12 col-md-4 panel-body-list col-lg-12">
                <A2isCombo
                  name="searchType"
                  value="0"
                  onChangeCombo = {this.onChangeCombo}
                  items = {this.state.chItems}
                />
                </div>
                <div className="col-xs-12 col-md-4 panel-body-list col-lg-12">
                <div className="col-xs-10 col-md-4 panel-body-list col-lg-10">
                  <A2isText
                    name="searchterm"
                    caption="Search"
                    onKeyDown={this._handleKeyDown}
                    onChangeText={this.onChangeFunction}
                    minLength={0}
                    maxLength={20}
                    value={this.state.tmpsearchterm}
                    readonly={this.state.readonly}
                    visible={this.state.visible}
                    disabled={this.state.disable}
                    required={this.state.required}
                    hint="Type name prospect"
                  />
                  </div>
                  <div className="col-xs-2 col-md-4 panel-body-list col-lg-2" style={{ padding:"10px" }}>
                  <a onClick={() => 
                    {
                       this.setState({searchterm: this.state.tmpsearchterm}, ()=>{ 
                          this.searchDataByParam(this.state.searchterm, true);
                       });
                    }
                  }>
                     <i className="fa fa-search" style={{ fontSize:"20px" }}></i>
                  </a>
                </div>
                </div>
                </div>
            )}
            <OtosalesLoading
              className="col-xs-12 panel-body-list loadingfixed"
              isLoading={this.state.isLoading}
            >
              {/* <AdapterTaskListLead
                onClickListItem={this.onClickListItem}
                onClickItem={this.onClickItem}
                name="sampleList"
                rowscount={this.state.dataItemsLead.length}
                multipleselect={true}
                dataItems={this.state.dataItemsLead}
              />

              <AdapterTaskList
                onClickListItem={this.onClickListItem}
                onClickItem={this.onClickItem}
                name="sampleList"
                rowscount={this.state.dataItems.length}
                multipleselect={true}
                dataItems={this.state.dataItems}
              /> */}
              <AdapterTaskListLead
                onClickListItem={this.onClickListItem}
                onClickItem={this.onClickItem}
                name="sampleList"
                rowscount={this.state.dataItems.length}
                multipleselect={true}
                dataItems={this.state.dataItems}
              />
            </OtosalesLoading>
          </div>
          {/* </div> */}
        </div>

'
'         {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageTaskList);
