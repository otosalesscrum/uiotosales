import React, { Component } from "react";
import Footer from "../../../components/Footer";
import { withRouter, Link } from "react-router-dom";
import { API_URL, API_VERSION } from "../../../config";
import {
  Tabs,
  OtosalesSelect2,
  OtosalesDropzone,
  OtosalesQuotationHistory,
  OtosalesFloatingButton,
  OtosalesModalSendEmail,
  OtosalesModalSendSMS,
  OtosalesModalAccessories,
  OtosalesModalInfo,
  OtosalesSelectFullview,
  OtosalesModalQuotationHistory,
  HeaderSubContent,
  Util
} from "../../../otosalescomponents";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { ToastContainer, toast } from "react-toastify";
import {
  Call,
  NeedFu,
  HexCarInfo,
  HexDealerInfo,
  HexPolicyDetails,
  HexPolicyInfo,
  HexProspectInfo
} from "../../../assets";
import Modal from "react-responsive-modal";
import imageCompression from "browser-image-compression";
import { DeviceUUID } from "device-uuid";
import Header from "../../../components/Header";
import NumberFormat from "react-number-format";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";

const tabstitle = ["Prospect Info", "Vehicle Info", "Cover", "Summary"];

const PAGE_TITLE_NEW = "New";
const PAGE_TITLE_RENEW = "Renewable";
class PageInputDetailTaskList extends Component {


  constructor(props) {
    super(props);

    let statedefault = {
      fromProspecttoVehicleCheckPhoneExist: true,
      isOpen: false,
      OrderNoQuotationEdit: "",
      FollowUpNoQuotationEdit: "",
      selectedQuotation: null,
      dataquotationhistory: [
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" }
      ],
      showModalInfo: false,
      showModal: false,
      showdialogperiod: false,
      showModalAddCopy: false,
      showModalMakeSure: false,
      showModalSendEmail: false,
      showModalPhoneExist: false,
      showModalAccessories: false,
      showfloatingbutton: (this.props.location.pathname.includes(
              "inputprospect"
            )
              ? false
              : true),
      sendEmailOrSMS: "",

      isPolicyNewRenew: 1,

      isErrProsName: false,
      isErrPhone1: false,
      isErrPhone2: false,
      isErrEmail: false,
      isErrDealer: false,
      isErrSalesman: false,

      isErrVehBrand: false,
      isErrVehYear: false,
      isErrVehType: false,
      isErrVehSeries: false,
      isErrVehUsage: false,
      isErrVehRegion: false,

      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrCovSumInsured: false,
      isLoading: false,
      // activetab: tabstitle[0],
      tabsDisabledState: [false, true, true, true],
      datadealer: [],
      DealerCode: 0,
      datasalesman: [],
      SalesDealer: 0,
      Name: "",
      Phone1: "",
      Phone2: "",
      Email1: "",
      Email2: null,
      CustIDAAB: null,
      SalesOfficerID: this.props.username,
      CustId: "",
      FollowUpNumber: "",
      NextFollowUpDate: "",
      LastFollowUpDate: "",
      FollowUpStatus: "1",
      FollowUpInfo: "1",
      Remark: "",
      BranchCode: "",
      PolicyId: "",
      PolicyNo: "",
      TransactionNo: "",
      SalesAdminID: null,
      SendDocDate: null,
      KTP: "",
      dataKTP: [],
      STNK: "",
      dataSTNK: [],
      SPPAKB: "",
      dataSPPAKB: [],
      BSTB1: "",
      dataBSTB1: [],
      BSTB2: "",
      dataBSTB2: [],
      BSTB3: "",
      dataBSTB3: [],
      BSTB4: "",
      dataBSTB4: [],
      CheckListSurvey1: "",
      dataCheckListSurvey1: [],
      CheckListSurvey2: "",
      dataCheckListSurvey2: [],
      CheckListSurvey3: "",
      dataCheckListSurvey3: [],
      CheckListSurvey4: "",
      dataCheckListSurvey4: [],
      PaymentReceipt1: "",
      dataPaymentReceipt1: [],
      PaymentReceipt2: "",
      dataPaymentReceipt2: [],
      PaymentReceipt3: "",
      dataPaymentReceipt3: [],
      PaymentReceipt4: "",
      dataPaymentReceipt4: [],
      PaymentReceipt1: "",
      PremiumCalculation1: "",
      dataPremiumCalculation1: [],
      PremiumCalculation2: "",
      dataPremiumCalculation2: [],
      PremiumCalculation3: "",
      dataPremiumCalculation3: [],
      PremiumCalculation4: "",
      dataPremiumCalculation4: [],
      FormA1: "",
      dataFormA1: [],
      FormA2: "",
      dataFormA2: [],
      FormA3: "",
      dataFormA3: [],
      FormA4: "",
      dataFormA4: [],
      FormB1: "",
      dataFormB1: [],
      FormB2: "",
      dataFormB2: [],
      FormB3: "",
      dataFormB3: [],
      FormB4: "",
      dataFormB4: [],
      FormB1: "",
      dataFormC1: [],
      FormC2: "",
      dataFormC2: [],
      FormC3: "",
      dataFormC3: [],
      FormC4: "",
      dataFormC4: [],

      VehicleUsedCar: false,
      VehicleCodetemp: "",
      VehicleCode: "",
      VehicleBrand: "",
      datavehiclebrand: [],
      VehicleYear: "",
      datavehicleyear: [],
      VehicleType: "",
      datavehicletype: [],
      VehicleSeries: "",
      datavehicleseries: [],
      VehicleUsage: "",
      datavehicleusage: [],
      VehicleRegion: "",
      datavehicleregion: [],
      VehicleRegistrationNumber: "",
      VehicleChasisNumber: "",
      VehicleEngineNumber: "",

      OrderNo: "",
      AccessSI: "",

      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: "",
      loadingvehicletocover: false,

      MessageAlertCover: "",

      CoverProductType: "0",
      datacoverproducttype: [],
      CoverProductCode: "",
      datacoverproductcode: [],
      CoverBasicCover: "",
      datacoverbasiccover: [],
      CovSumInsured: "",
      CoverTLOPeriod: 0,
      CoverComprePeriod: 0,
      CoverLastInterestNo: 0,
      CoverLastCoverageNo: 0,

      CalculatedPremiItems: [],
      coveragePeriodItems: [],
      coveragePeriodNonBasicItems: [],
      IsTPLEnabled: 0,
      IsTPLChecked: 0,
      IsTPLSIEnabled: 0,
      IsSRCCChecked: 0,
      IsSRCCEnabled: 0,
      IsFLDChecked: 0,
      IsFLDEnabled: 0,
      IsETVChecked: 0,
      IsETVEnabled: 0,
      IsTSChecked: 0,
      IsTSEnabled: 0,
      IsPADRVRChecked: 0,
      IsPADRVREnabled: 0,
      IsPADRVRSIEnabled: 0,
      IsPASSEnabled: 0,
      IsPAPASSSIEnabled: 0,
      IsPAPASSEnabled: 0,
      IsPAPASSChecked: 0,
      IsACCESSChecked: 0,
      IsACCESSSIEnabled: 0,
      IsACCESSEnabled: 0,
      SRCCPremi: 0,
      FLDPremi: 0,
      ETVPremi: 0,
      TSPremi: 0,
      PADRVRPremi: 0,
      PAPASSPremi: 0,
      TPLPremi: 0,
      ACCESSPremi: 0,
      AdminFee: 0,
      TotalPremi: 0,
      Alert: "",

      MultiYearF: false,

      chOne: 0,
      chTwo: 0,
      chThree: 0,
      chOneEnabled: 0,
      chTwoEnable: 0,
      chThreeEnabled: 0,
      chOneEnabledVisible: 0,
      chTwoEnableVisible: 0,
      chThreeEnabledVisible: 0,

      TPLCoverageId: null,
      dataTPLSI: [],
      PASSCOVER: 0,
      PAPASSICOVER: 0,
      PADRVCOVER: 0,
      ACCESSCOVER: 0,
      dataAccessories: [],

      SumName: "Loading...",
      SumPhone1: "",
      SumEmail1: "",
      SumVehicle: "",
      SumCoverageType: "",
      SumTotalPremium: "",
      SumSumInsured: "",
      SumAccessSI: "",
      SumSalesmanDesc: "",
      SumDealerDesc: "",

      isErrorProspect: false,
      isErrorVehicle: false,
      isErrorCover: false,
      message: ""
    };

    
    this.hitratecalculationloading= false;

    this.state = window.location.pathname.includes("tasklist-edit")
      ? JSON.parse(secureStorage.getItem("statetasklistedit")) || statedefault
      : window.location.pathname.includes("inputprospect")
      ? JSON.parse(secureStorage.getItem("stateinput")) || statedefault
      : statedefault;

    if (window.location.pathname.includes("inputprospect")) {
      secureStorage.removeItem("stateinput");
    } else if (window.location.pathname.includes("tasklist-edit")) {
      secureStorage.removeItem("statetasklistedit");
    }
  }


  render() {

    let stylingreload = {
      padding: "10px",
      overscrollBehaviorY: "contain"
    }
    return (
        <div>
          <Header titles={(this.state.isPolicyNewRenew == 1)? PAGE_TITLE_RENEW: PAGE_TITLE_NEW}>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a
                  className="fa fa-history fa-lg"
                  onClick={() =>
                    this.showQuotationHistory(this.state.FollowUpNumber)
                  }
                />
              </li>
              <li>
                <a
                  className="fa fa-plus-square fa-lg"
                  onClick={() =>
                    this.addCopyQuotation(
                      this.state.FollowUpNumber,
                      this.state.OrderNo
                    )
                  }
                />
              </li>
            </ul>
          </Header>
          <div style={{backgroundColor: "#18516c", padding: 5, color: "white", fontSize: "medium"}}>
            Details - { this.state.PolicyNo }
          </div>

          <div className="content-wrapper" style={stylingreload} >
            <ToastContainer />

            <div className="col-xs-12" style={{ backgroundColor: '#f1f1f1', padding: '10px', marginBottom: '5px' }}>
              <div className="col-xs-6" style={{ color: "#18516c" }}>
                Total Premium
              </div>

              <div className="col-xs-6 text-right" style={{ color: 'grey' }}>
                { Util.formatMoney(this.state.TotalPremi) }
              </div>
            </div>

            <HeaderSubContent
              titleText = "DATA KENDARAAN"
            />


          </div>

        </div>
    );
  }


}

export default withRouter(PageInputDetailTaskList);
