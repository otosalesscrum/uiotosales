import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../../../../components/Header";
import { OtosalesLightBox, Util, Log, OtosalesLoading, Otosalesheader } from "../../../../otosalescomponents";
import { BackToAO, Call } from "../../../../assets";
import { API_URL, API_VERSION_2, ACCOUNTDATA } from "../../../../config";
import { secureStorage } from "../../../../otosalescomponents/helpers/SecureWebStorage";

class PageSurveyDocuments extends Component {
  constructor(props) {
    super(props);
    if(secureStorage.getItem("taskdetail-surveyno") == undefined){
      this.props.history.push("/tasklist-details");
    }
    this.state = {
      isLoading: false,
      image1: null,
      dataimage: [
        // { caption: "image 1", image: Util.dataURItoBlob(secureStorage.getItem("dummyimage")) },
        // { caption: "image 2", image: Util.dataURItoBlob(secureStorage.getItem("dummyimage")) },
        // { caption: "image 3", image: Util.dataURItoBlob(secureStorage.getItem("dummyimage")) }
      ],
      dataimages:[],
    };
  }

  componentDidMount() {
    this.getSurveyDocuments(secureStorage.getItem("taskdetail-surveyno"));
    this.intervalgetsurveydocuments = setInterval(() => {  
      this.getSurveyDocuments(secureStorage.getItem("taskdetail-surveyno"));
    }, 10000);
  }

  componentWillUnmount(){
    clearInterval(this.intervalgetsurveydocuments);
  }

  getSurveyDocuments = (SurveyNo) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyDocument`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "SurveyNo=" + SurveyNo
    })
      .then(res => res.json())
      .then(jsn => {
        let dataimages = {};

        let keys = Object.keys(jsn.data);

        keys.map(dataKeys => {
          let dataImage = jsn.data[dataKeys].map(data => {
            return ({
                caption : data.Doc_DESCRIPTION, 
                image: (!Util.isNullOrEmpty(data.Image) ? Util.dataURItoBlob("data:image/"+Util.getExtensionFile(data.File_Name)+";base64," + data.Image) : "")
            });
          });
          
          dataimages = {...dataimages, ...{[dataKeys]: dataImage}};
        });

        this.setState({ dataimages, isLoading: false });
      })
      .catch(error => {
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  getNameDocuments = (name) => {
    name = name.toUpperCase();
    if(name == "ACCEPTANCE"){
      name = "DOCUMENTS";
    }else if(name == "NOCOVER"){
      name = "NO COVER";
    }else if(name == "ORIGINALDEFECT"){
      name = "ORIGINAL DEFECT";
    }else if(name == "ACCESS"){
      name = "ACCESSORIES";
    }

    return name;
  }



  render() {
    let stateDataImages = {...this.state.dataimages};
    let stateImages = Object.keys(stateDataImages).map((nameKey, index) => {
      let stateKey = stateDataImages[nameKey].map(keyImage => {
        return (<div className="col-xs-4 col-md-2 panel-body-list" key={index} style={{ marginBottom: "10px" }}>
        <div className="text-center">
          <OtosalesLightBox src={keyImage.image} caption={keyImage.caption} />
        </div>
      </div>)
      });
      let stateImage = (
        <div key={index} className="row">
          <div className="col-xs-12 backgroundgrey labelspanbold labelfontgrey" style={{marginBottom: "5px", fontSize:"large"}}>
            <center><span>{this.getNameDocuments(nameKey)}</span></center>
          </div>
          <div className="col-xs-12 panel-body-list">
          {stateKey}
          </div>
        </div>
      );
      return stateImage;   
    });


    return (
      <div>
        <Otosalesheader handleBackButton={() => {Util.handleBackButton(this.props)}} titles="Survey Documents" />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            blocking={this.state.isLoading}
          >
            <div className="row paddingside">
              {stateImages}
            </div>
          </OtosalesLoading>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageSurveyDocuments);
