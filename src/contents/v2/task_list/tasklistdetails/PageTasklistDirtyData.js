import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Header from "../../../../components/Header";
import {
  OtosalesLoading,
  OtosalesDropzonev2,
  OtosalesModalInfo,
  Log,
  Util
} from "../../../../otosalescomponents";
import {
  API_VERSION_2,
  API_URL,
  ACCOUNTDATA,
  HEADER_API,
  API_VERSION_0213URF2019,
  API_VERSION_0232URF2019,
  API_VERSION_0008URF2020,
  API_VERSION_0107URF2020
} from "../../../../config";
import imageCompression from "browser-image-compression";
import { DeviceUUID } from "device-uuid";
import { PolicyCreated } from "../../../../assets";
import { ToastContainer } from "react-toastify";
import ReactHtmlParser from "react-html-parser";
import { secureStorage } from "../../../../otosalescomponents/helpers/SecureWebStorage";

class PageTasklistDirtyData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      PolicyNo: null,
      FollowUpNo: null,
      isPTSB: false,
      MessageAlertCover: "",
      showModalInfo: false,

      documentsremarkstosa: "",

      dataFORMRENEWAL: [],
      FORMRENEWAL: "",
      dataRENEWALNOTICE: [],
      RENEWALNOTICE: "",
      dataBUKTIBAYAR: [],
      BUKTIBAYAR: "",
      dataKTP: [],
      KTP: "",
      dataSTNK: [],
      STNK: "",
      dataSURVEY: [],
      SURVEY: "",
      dataDOCNSA1: [],
      DOCNSA1: "",
      dataDOCNSA2: [],
      DOCNSA2: "",
      dataDOCNSA3: [],
      DOCNSA3: "",
      dataDOCNSA4: [],
      DOCNSA4: "",
      dataDOCNSA5: [],
      DOCNSA5: "",
      dataDOCNSA6: [],
      DOCNSA6: "",
      Info: null,
      Ismandatoryssurvey: false,
      Ismandatorysktp: true,
      Ismandatorysrenewalnotice: true,
      Ismandatorysformrenewal: true,
      Ismandatorysremarkstosa: true
    };
  }

  componentDidMount() {
    let SelectedTaskDetail = JSON.parse(
      secureStorage.getItem("SelectedTaskDetail")
    );
    this.setState({
      PolicyNo: SelectedTaskDetail.PolicyNo
    });

    this.hitgetTaskDetailDataKotor(SelectedTaskDetail.PolicyNo);
  }

  onCloseModalInfo = () => {
    this.setState({ showModalInfo: false });
  };

  onShowAlertModalInfo = MessageAlertCover => {
    this.setState({
      showModalInfo: true,
      MessageAlertCover
    });
  };

  hitgetTaskDetailDataKotor = PolicyNo => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getTaskDetailDataKotor`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "PolicyNo=" + PolicyNo
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        if (!Util.isNullOrEmpty(jsn.FollowUpNo)) {
          this.setState({
            FollowUpNo: jsn.FollowUpNo
          });
        }
        if (!Util.isNullOrEmpty(jsn.data)) {
          let data = ReactHtmlParser(jsn.data);
          this.setState({
            Info: data
          });
        }

        if (!Util.isNullOrEmpty(jsn.listImageAAB)) {
          if (jsn.listImageAAB.length > 0) {
            this.checkMandatoryDoc(jsn.listImageAAB, "KTP");
          }
        }
      })
      .catch(error => {
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  onClickSendToSA = (PolicyNo, FollowUpNo, isPTSB) => {
    this.setState({isLoading: true}, () => {
      if (!this.ValidationData()) {
        this.setState({isLoading: false})
        return;
      }
  
      this.sendToSARenewalNonRenNot(PolicyNo, FollowUpNo, isPTSB);
    });
  };

  checkMandatoryDoc = listImageAAB => {
    listImageAAB.forEach(data => {
      if (Util.stringEquals(data.ImageType, "KTP")) {
        this.setState({
          Ismandatorysktp: false
        });
      } else if (Util.stringEquals(data.ImageType, "FORMRENEWAL")) {
        this.setState({
          Ismandatorysformrenewal: false
        });
      } else if (Util.stringEquals(data.ImageType, "RENEWALNOTICE")) {
        this.setState({
          Ismandatorysrenewalnotice: false
        });
      }
    });
  };

  sendToSARenewalNonRenNot = (PolicyNo, FollowUpNo, isPTSB) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_0232URF2019}/DataReact/sendToSARenewalNonRenNot/`,
      // `${API_URL + "" + API_VERSION_0008URF2020}/DataReact/sendToSARenewalNonRenNot/`,
      `${API_URL + "" + API_VERSION_0107URF2020}/DataReact/sendToSARenewalNonRenNot/`,
      "POST",
      {...HEADER_API, 
        "Content-Type": "application/json"
      },
      {
        PolicyNo,
        FollowUpNo,
        RemarksToSA: this.state.documentsremarkstosa,
        SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        isPTSB
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          Util.showToast("Success", "INFO");
          this.props.history.push("/tasklist");
        } else {
          if (!Util.isNullOrEmpty(jsn.data)) {
            if(jsn.data.IsDoubleInsured){
              this.onShowAlertModalInfo(
                "Order Double Insured dengan order/polis aktif " +
                  jsn.data.OrderNo +
                  "/" +
                  jsn.data.PolicyNo +
                  " dengan periode " +
                  Util.formatDate(jsn.data.PeriodFrom, "dd-mm-yyyy") +
                  " s.d " +
                  Util.formatDate(jsn.data.PeriodTo, "dd-mm-yyyy")
              );
            }
          } else {
            Util.showToast("Gagal update :" + jsn.message, "WARNING");
          }
        }
        // if(jsn.status){
        //   if(!Util.isNullOrEmpty(jsn.data)){
        //     if(!Util.isNullOrEmpty(jsn.data.IsDoubleInsured)){
        //         if(jsn.data.IsDoubleInsured){
        //           this.onShowAlertModalInfo(
        //                   "Order Double Insured dengan order/polis aktif " +
        //                     jsn.data.OrderNo +
        //                     "/" +
        //                     jsn.data.PolicyNo +
        //                     " dengan periode " +
        //                     Util.formatDate(jsn.data.PeriodFrom, "dd-mm-yyyy") +
        //                     " s.d " +
        //                     Util.formatDate(jsn.data.PeriodTo, "dd-mm-yyyy")
        //                 );
        //         }else{
        //           Util.showToast("Success", "INFO");
        //           this.props.history.push("/tasklist");
        //         }
        //     }
        //   }else{
        //     Util.showToast("Success", "INFO");
        //     this.props.history.push("/tasklist");
        //   }
        // }else{
        //   Util.showToast("Gagal update :" + jsn.message, "WARNING");
        // }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Util.showToast("error " + error, "WARNING");
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  handleDeleteImage = name => {
    this.setState({
      [name]: "",
      [`data${name}`]: []
    });
  };

  handleUploadImage = (name, value, rejected) => {
    // this.setState({ isLoading: true });
    if (rejected.length > 0) {
      Util.showToast(
        "Please upload file with extension jpg, jpeg or png!!",
        "warning"
      );
      return;
    }

    if (value.length > 0) {
      // Option compress image thumbnail
      var options = {
        maxSizeMB: 0.01, // MB
        useWebWorker: false
      };

      /// Option compress image ori yang diupload
      var optionsori = {
        maxSizeMB: 0.2, // MB
        useWebWorker: false
      };

      // Log.debugGroup("FILE ORI", value[0]);

      this.setState({ [`data${name}`]: value });

      var compressOri = false;
      var compressThumbnail = true;

      imageCompression(value[0], optionsori)
        .then(function(compressedFile) {
          return compressedFile; // write your own logic
        })
        .then(compressImage => {
          // Log.debugGroup("ORI", compressImage);
          this.setDataFoto(name, "ORI", compressImage);
          compressOri = true;
        })
        .catch(function(error) {
          Log.debugGroup(error.message);
        });

      var checkcompressed = setInterval(() => {
        // Log.debugGroup("CHECK COMPRESS");

        if (compressOri && compressThumbnail) {
          /// HIT API UPLOAD IMAGE
          Log.debugGroup("SUDAH COMPRESS DONG!!!");
          clearInterval(checkcompressed);

          // function to convert image to base64 and upload image
          this.getBase64UploadImage(name);
        }
      }, 100);
    }
  };

  setDataFoto = (name, state, data) => {
    var datafototemp = [];
    datafototemp = [...this.state[`data${name}`]];

    if (state == "THUMBNAIL") {
      datafototemp[1] = data;
    } else if (state == "ORI") {
      datafototemp[0] = data;
    }

    if (datafototemp.length > 0) {
      this.setState({ [`data${name}`]: datafototemp }, () => {
        // Log.debugGroup("STATE MASUK", datafototemp);
      });
    }
  };

  getBase64UploadImage = name => {
    // this.setState({ isLoading: true });
    var datafototemp = null;

    datafototemp = [...this.state[`data${name}`]];
    Log.debugGroup(datafototemp);

    // FLAG CHECK PROCESS CONVERT BASE64
    var flagBase64Ori = false;
    var flagBase64Thumb = false;

    // var to save base64 converted
    var database64ori = null;
    var database64thumb = null;

    var reader = new FileReader();
    reader.readAsDataURL(datafototemp[0]);
    reader.onload = function() {
      database64ori = reader.result;
      flagBase64Ori = true;
    };
    reader.onerror = function(error) {
      Log.debugGroup("base64 ori Error: ", error);
    };

    var readerthumb = new FileReader();
    readerthumb.readAsDataURL(datafototemp[0]);
    readerthumb.onload = function() {
      database64thumb = readerthumb.result;
      flagBase64Thumb = true;
    };
    readerthumb.onerror = function(error) {
      Log.debugGroup("base64 thumb Error: ", error);
    };

    var checkconvertbase64 = setInterval(() => {
      if (flagBase64Ori && flagBase64Thumb) {
        Log.debugGroup("SUDAH CONVERT BASE64 DONG!!!");
        clearInterval(checkconvertbase64);
        var ext =
          "." +
          database64ori.substring(
            database64ori.indexOf("image/") + 6,
            database64ori.indexOf(";base64")
          );
        database64ori = database64ori.substring(
          database64ori.indexOf(",") + 1,
          database64ori.length
        );
        database64thumb = database64thumb.substring(
          database64thumb.indexOf(",") + 1,
          database64thumb.length
        );

        this.uploadImage(
          this.base64toHEX(database64ori),
          this.base64toHEX(database64thumb),
          name,
          ext,
          JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID, ///
          this.generateDeviceUUID(),
          this.state.FollowUpNo
        );
      }
    }, 100);
  };

  base64toHEX = base64 => {
    var raw = atob(base64);
    var HEX = "";
    var i = 0;
    for (i = 0; i < raw.length; i++) {
      var _hex = raw.charCodeAt(i).toString(16);
      HEX += _hex.length == 2 ? _hex : "0" + _hex;
    }
    return HEX.toUpperCase();
  };

  generateDeviceUUID = () => {
    var du = new DeviceUUID().parse();
    var dua = [
      du.language,
      du.platform,
      du.os,
      du.cpuCores,
      du.isAuthoritative,
      du.silkAccelerated,
      du.isKindleFire,
      du.isDesktop,
      du.isMobile,
      du.isTablet,
      du.isWindows,
      du.isLinux,
      du.isLinux64,
      du.isMac,
      du.isiPad,
      du.isiPhone,
      du.isiPod,
      du.isSmartTV,
      du.pixelDepth,
      du.isTouchScreen
    ];
    return du.hashMD5(dua.join(":"));
  };

  uploadImage = (
    dataimageori,
    dataimagethumb,
    imageType,
    ext,
    SalesOfficerID,
    DeviceID,
    FollowUpNo
  ) => {
    this.setState({ isLoading: true });

    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/UploadImage/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "ext=" +
        ext +
        "&ImageType=" +
        imageType +
        "&FollowUpNo=" +
        (FollowUpNo || "") +
        "&DeviceID=" +
        DeviceID +
        "&SalesOfficerID=" +
        SalesOfficerID +
        "&Data=" +
        dataimageori +
        "&ThumbnailData=" +
        dataimagethumb
    })
      .then(res => res.json())
      .then(jsn => {
        // Log.debugGroup(jsn);
        this.setState({
          FollowUpNo: jsn.FollowUpNo,
          [imageType]: jsn.data,
          isLoading: false,
          personalneeddocrep: imageType == "DOCREP" ? true : false
        });
      })
      .catch(error => {
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          this.uploadImage(dataimageori, dataimagethumb, imageType);
        }
      });
  };

  FormInfo = () => {
    return (
      <React.Fragment>
        <div
          className="col-xs-12"
          style={{ marginBottom: "5px", marginTop: "10px" }}
        >
          {this.state.Info}
        </div>
      </React.Fragment>
    );
  };

  FormDocuments = () => {
    return (
      <React.Fragment>
        <div className="row">
          <div
            className={
              "col-xs-4 col-md-2 panel-body-list" +
              (this.state.IsNotErrordocumentsformrenewal ? " has-error" : "")
            }
          >
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataFORMRENEWAL}
                name="FORMRENEWAL"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">
              Form Renewal{this.state.Ismandatorysformrenewal ? "*" : ""}
            </center>
          </div>
          <div
            className={
              "col-xs-4 col-md-2 panel-body-list" +
              (this.state.IsNotErrordocumentsrenewalnotice ? " has-error" : "")
            }
          >
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataRENEWALNOTICE}
                name="RENEWALNOTICE"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">
              Renewal Notice{this.state.Ismandatorysrenewalnotice ? "*" : ""}
            </center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataBUKTIBAYAR}
                name="BUKTIBAYAR"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">Bukti Bayar</center>
          </div>
          <div
            className={
              "col-xs-4 col-md-2 panel-body-list" +
              (this.state.IsNotErrordocumentsktp ? " has-error" : "")
            }
          >
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataKTP}
                name="KTP"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">
              KTP{this.state.Ismandatorysktp ? "*" : ""}
            </center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataSTNK}
                name="STNK"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">STNK</center>
          </div>
          <div
            className={
              "col-xs-4 col-md-2 panel-body-list" +
              (this.state.IsNotErrordocumentssurvey ? " has-error" : "")
            }
          >
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataSURVEY}
                name="SURVEY"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            {/* <center className="smallerCheckbox">Survey*</center> */}
            <center className="smallerCheckbox">Survey</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA1}
                name="DOCNSA1"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA2}
                name="DOCNSA2"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA3}
                name="DOCNSA3"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA4}
                name="DOCNSA4"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA5}
                name="DOCNSA5"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
          <div className="col-xs-4 col-md-2 panel-body-list">
            <div className="text-center">
              <OtosalesDropzonev2
                src={this.state.dataDOCNSA6}
                name="DOCNSA6"
                handleDeleteImage={this.handleDeleteImage}
                handleUploadImage={this.handleUploadImage}
              />
            </div>
            <center className="smallerCheckbox">NSA</center>
          </div>
        </div>
        <div className="row" style={{ marginTop: "10px" }}>
          <div className="col-xs-12">
            <label className="col-xs-12">
              <input
                type="checkbox"
                name="isPTSB"
                checked={this.state.isPTSB}
                onChange={this.onChangeFunction}
                style={{ marginRight: "10px" }}
              />
              <span className="smallerCheckbox">
                POLIS TERBIT SEBELUM BAYAR ATAU BAYAR DENGAN CC/DEBIT
              </span>
            </label>
          </div>
        </div>
        <div
          className={
            this.state.IsNotErrordocumentsremarkstosa ? "row has-error" : "row"
          }
          style={{ marginBottom: "150px" }}
        >
          <div className="col-xs-12">
            <span className="col-xs-12 labelspanbold">Remarks to SA</span>
            <div className="col-xs-12">
              <input
                type="text"
                name="documentsremarkstosa"
                maxLength="152"
                value={this.state.documentsremarkstosa}
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  ActionButton = () => {
    return (
      <div
        className="footer navbar-fixed-bottom main-footer"
        style={{ backgroundColor: "#18516c", zIndex: "auto", padding: "0px" }}
      >
        <div className="text-center col-xs-12 panel-body-list">
          <div
            className="col-xs-3 col-md-1 pull-right-md"
            style={{ marginBottom: "5px", marginTop: "5px" }}
            onClick={() =>
              this.onClickSendToSA(
                this.state.PolicyNo,
                this.state.FollowUpNo,
                this.state.isPTSB ? 1 : 0
              )
            }
          >
            <img className="img img-responsive" src={PolicyCreated} />
            <span className="labelfontwhite" style={{ fontSize: "x-small" }}>
              Send to SA
            </span>
          </div>
        </div>
      </div>
    );
  };

  initializeErrorFlagMandatory = () => {
    this.setState({
      IsNotErrordocumentssurvey: false,
      IsNotErrordocumentsktp: false,
      IsNotErrordocumentsrenewalnotice: false,
      IsNotErrordocumentsformrenewal: false,
      IsNotErrordocumentsremarkstosa: false
    });
  };

  validationDocuments = () => {
    let validation = true;
    let messages = "";

    if (
      Util.isNullOrEmpty(this.state.FORMRENEWAL) &&
      this.state.Ismandatorysformrenewal
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Documents Form Renewal mandatory"
        : messages;
      this.setState({
        IsNotErrordocumentsformrenewal: true
      });
    }

    if (
      Util.isNullOrEmpty(this.state.RENEWALNOTICE) &&
      this.state.Ismandatorysrenewalnotice
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Documents Renewal Notice mandatory"
        : messages;
      this.setState({
        IsNotErrordocumentsrenewalnotice: true
      });
    }

    if (Util.isNullOrEmpty(this.state.KTP) && this.state.Ismandatorysktp) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Documents KTP mandatory"
        : messages;
      this.setState({
        IsNotErrordocumentsktp: true
      });
    }

    // if (Util.isNullOrEmpty(this.state.SURVEY)) {
    //   validation = validation ? false : validation;
    //   messages = Util.isNullOrEmpty(messages)
    //     ? "Documents Survey mandatory"
    //     : messages;
    //   this.setState({
    //     IsNotErrordocumentssurvey: true
    //   });
    // }

    if (Util.isNullOrEmpty(this.state.documentsremarkstosa)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Remarks to SA mandatory"
        : messages;
      this.setState({
        IsNotErrordocumentsremarkstosa: true
      });
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  ValidationData = () => {
    let validation = true;
    let messages = "";

    let validationData = { validation: true, messages: "" };

    this.initializeErrorFlagMandatory();

    validationData = this.validationDocuments();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    if (!Util.isNullOrEmpty(messages)) {
      Util.showToast(messages, "WARNING");
    }
    return validation;
  };

  onChangeFunction = event => {
    var nameEvent = event.target.name;
    const value =
      event.target[
        event.target.type === "checkbox"
          ? "checked"
          : event.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = event.target.name;
    let pattern = event.target.pattern;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({ [name]: value });
      }
    } else {
      this.setState({
        [name]: value
      });
    }
  };

  render() {
    return (
      <div>
        <Header titles="Renewal Info" />
        <ToastContainer />

        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfo}
          showModalInfo={this.state.showModalInfo}
        />
        <div className="content-wrapper">
          <OtosalesLoading
            className={"col-xs-12 panel-body-list loadingfixed"+(this.state.isLoading ? " disablediv": "")}
            isLoading={this.state.isLoading}
          >
            <this.FormInfo />
            <this.FormDocuments />
            <this.ActionButton />
          </OtosalesLoading>
        </div>
      </div>
    );
  }
}

export default withRouter(PageTasklistDirtyData);
