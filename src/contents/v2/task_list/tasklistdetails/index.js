import imageCompression from "browser-image-compression";
import { DeviceUUID } from "device-uuid";
import React, { Component } from "react";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import NumberFormat from "react-number-format";
import Modal from "react-responsive-modal";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import {
  CallLater,
  NotDeal,
  PolicyCreated,
  Potential,
  SendQuotation
} from "../../../../assets";
import Header from "../../../../components/Header";
import {
  API_URL,
  API_VERSION_2,
  HEADER_API,
  ACCOUNTDATA,
  API_RETRY_LIMIT,
  API_V0213URF2019,
  API_VERSION_0213URF2019,
  API_VERSION_0232URF2019
} from "../../../../config";
import {
  OtosalesDatePicker,
  OtosalesDropzonev2,
  OtosalesModalInfo,
  OtosalesModalAccessories,
  OtosalesModalMakeSure,
  OtosalesModalQuotationHistory,
  OtosalesModalSendEmail,
  OtosalesModalSendSMS,
  OtosalesModalSendWA,
  OtosalesModalSetPeriod,
  OtosalesModalSetPeriodPremi,
  OtosalesSelectFullview,
  OtosalesSelectFullviewrev,
  OtosalesModalAlert,
  OtosalesSelectFullviewv2,
  OtosalesLoading,
  OtosalesIFrame,
  Util,
  OtosalesFloatingButtonv2,
  Log,
  OtosalesModalSendEmailv2,
  OtosalesModalSendSMSv2,
  OtosalesModalSendEmailCkEditor,
  OtosalesModalSendSMSCkEditor
} from "../../../../otosalescomponents";
import { PageTasklistDirtyData } from "../../..";
import "../../../../otosalescomponents/otosalesdatepickerstyle.css";
import * as Parameterized from "../../../../parameterizedata";
import { secureStorage } from "../../../../otosalescomponents/helpers/SecureWebStorage";
import ActionType from "../../../../redux/reducer/globalActionType";

class PageTasklistDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SalesOfficerID: "",
      SalesOfficerName: "",
      IsRenewal: false,
      roleLogin: "",
      Parameterized: Parameterized,

      SubjectTemplateEmail: "",
      BodyTemplateSendEmail: "",
      SubjectTemplateEmailWA: "",
      BodyTemplateSendEmailWA: "",
      EmailToTemplateWA: "",
      BodyTemplateSendEmailSMSTelerenewal: "",

      dataAttachmentEmail1: [],
      AttachmentEmail1: "",
      typeAttachmentEmail1: "",
      base64AttachmentEmail1: "",
      dataAttachmentEmail2: [],
      AttachmentEmail2: "",
      typeAttachmentEmail2: "",
      base64AttachmentEmail2: "",
      dataAttachmentEmail3: [],
      AttachmentEmail3: "",
      typeAttachmentEmail3: "",
      base64AttachmentEmail3: "",
      dataAttachmentEmail4: [],
      AttachmentEmail4: "",
      typeAttachmentEmail4: "",
      base64AttachmentEmail4: "",
      dataAttachmentEmail5: [],
      AttachmentEmail5: "",
      typeAttachmentEmail5: "",
      base64AttachmentEmail5: "",

      dataphonelist: [
        { value: "089999999999", label: "sjldfsfsdf" },
        { value: "089999999999", label: "sjldfsfsdd" },
        { value: "089999999999", label: "sjldfsfsd3" },
        { value: "089999999999", label: "sjldfsfsd1" }
      ],
      phonelistselect: null,

      followupstatusvisible: [0, 2, 3, 5, 13],
      showModalCheckSurveyResult: false,

      showModalInfo: false,
      showModalButtonSendQuotation: false,
      showModalMakeSure: false,
      showModalSendEmail: false,
      showModalSendWA: false,
      showModalSendEmailWA: false,
      showModal: false,
      showModalAddCopy: false,
      showModalInfoTryAPI: false,
      showModalInfoIsPaid: false,

      IsPaid: false,

      selectedQuotation: "",
      sendEmailOrSMS: "",

      dataquotationhistory: [
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" }
      ],

      isLoading: false,
      dataKTP: [],
      KTP: "",
      dataSTNK: [],
      STNK: "",
      dataBSTB: [],
      BSTB: "",
      dataDOCREP: [],
      DOCREP: "",

      dataNPWP: [],
      NPWP: "",
      dataSIUP: [],
      SIUP: "",

      dataBUKTIBAYAR: [],
      BUKTIBAYAR: "",
      dataBUKTIBAYAR2: [],
      BUKTIBAYAR2: "",
      dataBUKTIBAYAR3: [],
      BUKTIBAYAR3: "",
      dataBUKTIBAYAR4: [],
      BUKTIBAYAR4: "",
      dataBUKTIBAYAR5: [],
      BUKTIBAYAR5: "",

      dataSPPAKB: [],
      SPPAKB: "",
      dataFAKTUR: [],
      FAKTUR: "",
      dataKONFIRMASICUST: [],
      KONFIRMASICUST: "",
      dataDOCNSA1: [],
      DOCNSA1: "",
      dataDOCNSA2: [],
      DOCNSA2: "",
      dataDOCNSA3: [],
      DOCNSA3: "",
      dataDOCNSA4: [],
      DOCNSA3: "",
      dataDOCNSA5: [],
      DOCNSA3: "",
      documentsremarksfromsa: "",
      documentsremarkstosa: "",

      FollowUpNo: "",
      CustID: "",

      Admin: null,
      GrossPremium: null,
      NetPremi: null,
      SurveyNo: null,
      FollowUpStatus: "",
      FollowUpInfo: "",
      PolicyOrderNo: null,
      OldPolicyPeriodTo: null,

      //Renewal
      NoClaimBonus: 500000,
      DiscountPremi: 0,
      OldPolicyNo: "",

      //Default TSI TPL
      defaultTSITPL: 10000000,
      dataextsi: [],

      IsSalesmanDealerEnable: false,

      // personaltanggallahir: Util.convertDate().setFullYear(
      //   Util.convertDate().getFullYear() - 17
      // ),
      personaltanggallahir: null,
      personalJK: "",
      personalname: "",
      dataJK: [
        { value: "M", label: "Laki-Laki" },
        { value: "F", label: "Perempuan" }
      ],
      personalkodepos: "",
      dataKodePos: [],
      personalalamat: "",
      personalemail: "",
      personalhp: "",
      personalnomoridentitas: "",
      personalneeddocrep: false,
      personalamountrep: null,
      personalpayercompany: false,

      isCompany: false,
      isBasicCoverageState: true,

      companyname: "",
      companynpwpnumber: "",
      companynpwpdata: null,
      companynpwpaddress: "",
      companysiupnumber: "",
      companypkpnumber: "",
      companypkpdata: null,
      companyofficeaddress: "",
      companypostalcode: "",
      companyofficenumber: "",
      companypdcname: "",
      companypichp: "",
      companyemail: "",

      vehicleusedcar: false,
      vehiclevehiclecode: "",
      vehiclevehiclecodeyear: null,
      datavehicle: [],
      datavehicleall: [],
      vehiclebasiccoverage: "",
      databasiccover: [],
      databasiccoverall: [],
      vehicleperiodfrom: null,
      vehicleperiodto: null,
      vehicleperiodfrommodalperiod: null,
      vehicleperiodtomodalperiod: null,
      vehicleusage: "",
      datavehicleusage: [],
      vehiclecolor: "",
      datavehicleproducttype: [],
      vehicleproductcode: "",
      datavehicleproductcode: [],
      datavehicleproductcodeall: [],
      Ndays: null,
      vehiclesegmentcode: "",
      datavehiclesegmentcode: [],
      vehiclepolicyno: "",
      vehicleplateno: "",
      vehiclechasisno: "",
      vehicleengineno: "",
      vehicleregion: "",
      datavehicleregion: [],
      vehicledealer: 0,
      datavehicledealer: [],
      vehiclesalesman: 0,
      vehiclesalesmanname: "",
      datavehiclesalesman: [],
      vehiclenamebank: "",
      datavehiclebank: [],
      vehiclenomorrek: "",
      vehicleagency: "",
      vehicleupliner: null,
      vehiclecacatsemula: "",
      vehiclenocover: "",
      vehicleaccesories: "",
      vehicleisordefectrepair: false,
      vehicleisaccessorieschange: false,
      vehicleispolicyissuedbeforpaying: false,
      vehicleremarks: "",

      vehicleAccessSI: 0,
      vehicleBrandCode: "",
      vehicleInsuranceType: 0,
      vehicleModelCode: "",
      vehicleProductTypeCode: "",
      vehicleSeries: "",
      vehicleSitting: 0,
      vehicleType: "",
      vehicleVANumber: "",
      vehicleYear: "",

      MessageAlertCover: "",

      CoverProductType: "0",
      datacoverproducttype: [],
      CoverProductCode: "",
      datacoverproductcode: [],
      CoverBasicCover: "",
      datacoverbasiccover: [],
      vehicletotalsuminsured: 0,
      vehicletotalsuminsuredtemp: 0,
      CoverTLOPeriod: 0,
      CoverComprePeriod: 0,
      CoverLastInterestNo: 0,
      CoverLastCoverageNo: 0,

      CalculatedPremiItems: [],
      coveragePeriodItems: [],
      coveragePeriodNonBasicItems: [],
      IsTPLEnabled: 0,
      IsTPLChecked: 0,
      IsTPLSIEnabled: 0,
      IsSRCCChecked: 0,
      IsSRCCEnabled: 0,
      IsFLDChecked: 0,
      IsFLDEnabled: 0,
      IsETVChecked: 0,
      IsETVEnabled: 0,
      IsTSChecked: 0,
      IsTSEnabled: 0,
      IsPADRVRChecked: 0,
      IsPADRVREnabled: 0,
      IsPADRVRSIEnabled: 0,
      IsPASSEnabled: 0,
      IsPAPASSSIEnabled: 0,
      IsPAPASSEnabled: 0,
      IsPAPASSChecked: 0,
      IsACCESSChecked: 0,
      IsACCESSSIEnabled: 0,
      IsACCESSEnabled: 0,
      SRCCPremi: 0,
      FLDPremi: 0,
      ETVPremi: 0,
      TSPremi: 0,
      PADRVRPremi: 0,
      PAPASSPremi: 0,
      TPLPremi: 0,
      ACCESSPremi: 0,
      AdminFee: 0,
      TotalPremi: 0,
      Alert: "",

      MultiYearF: false,

      IsSRCCCheckedEnable: 1,
      IsETVCheckedEnable: 1,
      IsFLDCheckedEnable: 1,
      IsTRSCheckedEnable: 1,
      IsPADRIVERCheckedEnable: 1,
      IsPAPASSCheckedEnable: 1,

      IsSRCCCheckedEnableDays: 0,
      IsETVCheckedEnableDays: 0,
      IsFLDCheckedEnableDays: 0,

      chItemSetPeriod: false,
      chItemSetPeriodEnable: 0,

      chOne: 0,
      chTwo: 0,
      chThree: 0,
      chOneEnabled: 0,
      chTwoEnable: 0,
      chThreeEnabled: 0,
      chOneEnabledVisible: 0,
      chTwoEnableVisible: 0,
      chThreeEnabledVisible: 0,

      TPLCoverageId: null,
      dataTPLSI: [],
      dataTPLSIall: [],
      TPLSICOVERtemp: 0,
      PASSCOVER: 0,
      PAPASSICOVER: 0,
      PADRVCOVER: 0,
      ACCESSCOVER: 0,
      dataAccessories: [],
      showModalAccessories: false,

      paymentduedate: "",
      paymentvapermata: "",
      paymentvamandiri: "",
      paymentvabca: "",
      paymentnorek: "",
      paymentnova: "",
      paymentisvaactive: true,
      dataBUKTIBAYAR: [],
      BUKTIBAYAR: "",
      dataBUKTIBAYAR2: [],
      BUKTIBAYAR2: "",
      dataBUKTIBAYAR3: [],
      BUKTIBAYAR3: "",
      dataBUKTIBAYAR4: [],
      BUKTIBAYAR4: "",
      dataBUKTIBAYAR5: [],
      BUKTIBAYAR5: "",

      policysentto: "",
      datapolicysentto: [
        { value: "Customer", label: "Customer" },
        {
          value: "Kantor Cabang Asuransi Astra",
          label: "Kantor Cabang Asuransi Astra"
        },
        { value: "Kantor Cabang Dealer", label: "Kantor Cabang Dealer" },
        { value: "Lainnya", label: "Lainnya" }
      ],
      policyname: "",
      datapolicyaddress: [],
      policyaddress: "",
      policykodepos: "",

      surveyneed: false,
      surveynsaskipsurvey: false,
      surveychecklistmanual: false,
      surveykota: "",
      datasurveykota: [],
      datasurveykotaall: [],
      surveylokasi: "",
      datasurveylokasi: [],
      datasurveylokasiall: [],
      surveyalamat: "",
      surveykodepos: "",
      surveytanggal: null,
      datasurveytanggal: [],
      surveywaktu: null,
      datasurveywaktu: [],
      dataKodePosSurvey: [],

      surveydatanocover: "",
      surveydataoriginaldefect: "",
      surveydataaccessories: "",
      surveydatastatus: "",
      surveydatarecommendation: "",
      surveydataremarks: "",
      surveydatasurveyor: ""
    };
  }

  componentDidMount() {
    this.props.addStateTaskDetail(this.state);
    this.setState({
      Parameterized: Parameterized
    });

    let StateTaskDetail = JSON.parse(
      secureStorage.getItem("SelectedTaskDetail")
    );
    if (Util.isNullOrEmpty(StateTaskDetail)) {
      this.props.history.push("/");
    }
    if (StateTaskDetail.IsRenewalNonRenNot == 1) {
      //// HANDLE NONRENOT
    } else {
      this.componentDidMountTaskDetailDefault();
    }
  }

  componentDidMountTaskDetailDefault = () => {
    this.initializeParameterizedData();
      console.log(
        "Masking :",
        Util.maskingPhoneNumberv2("081935944430", ["08"], 4)
      );
      this.state.isflagupdatetaskdetail = true;
      this.abortController = new AbortController();
      this.abortControllerBasicPremium = new AbortController();
      this.abortControllerUpdateTaskDetail = new AbortController();
      this.abortControllerGetMappingPlate = new AbortController();

      // console.log("masking email :", Util.maskingEmail("sdfmklsdfm@jdgfg.com"));

      this.accesoriesInterval = setInterval(() => {
        if (
          this.props.history.action == "POP" &&
          this.state.showModalAccessories
        ) {
          this.setState({ showModalAccessories: false });
        }
      }, 100);

      var eles = document.getElementsByTagName("input");
      for (let i = 0; i < eles.length; i++) {
        eles[i].setAttribute("autocomplete", "off");
      }
      // Log.debugGroup("muncul ya:",Util.sentenceContainsArrayString());
      secureStorage.removeItem("taskdetail-surveyno");

      if (Util.isNullOrEmpty(secureStorage.getItem("SelectedTaskDetail"))) {
        this.props.history.push("/tasklist");
        return;
      }

      Log.debugGroup(this.generateDeviceUUID());
      let SelectedTaskDetail = JSON.parse(
        secureStorage.getItem("SelectedTaskDetail")
      );
      this.setState({
        CustID: SelectedTaskDetail.CustID,
        FollowUpNo: SelectedTaskDetail.FollowUpNo
      });

      this.loadGetTaskDetail = false;
      this.loadPostalCode = false;
      this.loadBasicCover = false;
      this.loadListUsage = false;
      this.loadProductType = false;
      this.loadRegion = false;
      this.loadDealer = false;
      this.loadBank = false;
      this.loadSurveyCity = false;
      this.loadPolicySentTo = false;
      this.loadSurveyLocation = false;
      this.loadpremicalculationfirst = false;

      this.loadallfirstall = setInterval(() => {
        let ListLoading = {
          loadGetTaskDetail: this.loadGetTaskDetail,
          loadPostalCode: this.loadPostalCode,
          loadBasicCover: this.loadBasicCover,
          loadListUsage: this.loadListUsage,
          loadProductType: this.loadProductType,
          loadRegion: this.loadRegion,
          loadDealer: this.loadDealer,
          loadBank: this.loadBank,
          loadSurveyCity: this.loadSurveyCity,
          loadpremicalculationfirst: this.loadpremicalculationfirst,
          loadPolicySentTo: this.loadPolicySentTo
        };
        Log.debugGroupCollapsed("IsLoading Data : ", ListLoading);

        if (
          this.loadGetTaskDetail &&
          this.loadPostalCode &&
          this.loadBasicCover &&
          this.loadListUsage &&
          this.loadProductType &&
          this.loadRegion &&
          this.loadDealer &&
          this.loadBank &&
          this.loadSurveyCity &&
          this.loadpremicalculationfirst &&
          this.loadPolicySentTo
        ) {
          this.setState({
            isLoading: false
          });
          clearInterval(this.loadallfirstall);
        } else {
          this.setState({
            isLoading: true
          });
        }
      }, 10);

      this.getTaskDetail(
        SelectedTaskDetail.CustID,
        SelectedTaskDetail.FollowUpNo
      );
      this.getPostalCode();
      // this.getVehicle();
      this.getBasicCover();
      this.getListUsage();
      // this.getListProductType();
      // this.getListProductCode();
      // this.getUpliner();
      this.getListRegion();
      this.getListDealer();
      this.getListBank();
      this.getSurveyCity();
      this.getPolicySentTo();
      this.setState(
        {
          LastState: {
            PersonalData: this.setModelPersonalData(),
            CompanyData: this.setModelCompanyData(),
            VehicleData: this.setModelVehicleData(),
            PolicyAddress: this.setModelPolicyAddress(),
            OrderSimulation: this.setModelOrderSimulation(),
            OrderSimulationMV: this.setModelOrderSimulationMV(),
            SurveySchedule: this.setModelSurveySchedule(),
            CalculatedPremiItems: this.state.CalculatedPremiItems,
            ImageData: this.setModelImageData()
          }
        },
        () => {
          this.intervalUpdateTaskDetail = setInterval(() => {
            if (!this.state.isflagupdatetaskdetail) {
              return;
            }
            this.onUpdateTaskDetailFuntion();
          }, 10000);
        }
      );

      var account = JSON.parse(ACCOUNTDATA);
      if (!Util.isNullOrEmpty(account)) {
        this.setState({
          role: account.UserInfo.User.Role
        });
      }
  }

  initializeParameterizedData = () => {
    Parameterized.getApplicationParam("PREFIX-LANDLINENO", 0, data => {
      Parameterized.setLandLinePrefix(data, () => {
        import(`../../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });

    Parameterized.getApplicationParam("PREFIX-PHONENO", 0, data => {
      Parameterized.setMobilePrefix(data, () => {
        import(`../../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });

    Parameterized.getApplicationParam("ROLECTI", 0, data => {
      Parameterized.setRoleCTI(data, () => {
        import(`../../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });
  };

  setFlagMandatoryFromLocalStorage = () => {
    if (!Util.isNullOrEmpty(secureStorage.getItem("ErrorFlagMandatory"))) {
      let flagMandatory = JSON.parse(
        secureStorage.getItem("ErrorFlagMandatory")
      );
      this.setState(flagMandatory, () => {
        secureStorage.removeItem("ErrorFlagMandatory");
        secureStorage.removeItem("validationTaskDetail");
      });
    }
  };

  isLoadingDataFirst = () => {
    let returnData = false;

    if (
      // this.loadGetTaskDetail &&
      // this.loadPostalCode &&
      // this.loadBasicCover &&
      // this.loadListUsage &&
      // this.loadProductType &&
      // this.loadRegion &&
      // this.loadDealer &&
      // this.loadBank &&
      // this.loadSurveyCity &&
      // this.loadPolicySentTo
      this.loadGetTaskDetail &&
      this.loadPostalCode &&
      this.loadBasicCover &&
      this.loadListUsage &&
      this.loadProductType &&
      this.loadRegion &&
      this.loadDealer &&
      this.loadBank &&
      this.loadSurveyCity &&
      this.loadpremicalculationfirst &&
      this.loadPolicySentTo
    ) {
      returnData = true;
    }

    return returnData;
  };

  onUpdateTaskDetailFuntion = () => {
    var currentState = {
      PersonalData: this.setModelPersonalData(),
      CompanyData: this.setModelCompanyData(),
      VehicleData: this.setModelVehicleData(),
      PolicyAddress: this.setModelPolicyAddress(),
      OrderSimulation: this.setModelOrderSimulation(),
      OrderSimulationMV: this.setModelOrderSimulationMV(),
      SurveySchedule: this.setModelSurveySchedule(),
      CalculatedPremiItems: this.state.CalculatedPremiItems,
      ImageData: this.setModelImageData()
    };

    if (JSON.stringify(currentState) != JSON.stringify(this.state.LastState)) {
      Log.debugGroupCollapsed("Last State : ", this.state.LastState);
      Log.debugGroupCollapsed("Current State : ", currentState);
      Log.debugGroupCollapsed("JSON UpdateTaskDetail changed");
      if (!Util.isNullOrEmpty(this.state.LastState.PersonalData.Name)) {
        Log.debugGroupCollapsed("hit on UpdateTaskDetail");
        this.onUpdateTaskDetail();
      } else if (
        this.state.isCompany &&
        !Util.isNullOrEmpty(this.state.LastState.CompanyData.CompanyName)
      ) {
        Log.debugGroupCollapsed("hit on UpdateTaskDetail");
        this.onUpdateTaskDetail();
      }
      this.setState({ LastState: currentState });
    } else {
      Log.debugGroupCollapsed("JSON UpdateTaskDetail not changed");
    }
  };

  onDownloadTaskDetail = (callback = () => {}) => {
    var currentState = {
      PersonalData: this.setModelPersonalData(),
      CompanyData: this.setModelCompanyData(),
      VehicleData: this.setModelVehicleData(),
      PolicyAddress: this.setModelPolicyAddress(),
      OrderSimulation: this.setModelOrderSimulation(),
      OrderSimulationMV: this.setModelOrderSimulationMV(),
      SurveySchedule: this.setModelSurveySchedule(),
      CalculatedPremiItems: this.state.CalculatedPremiItems,
      ImageData: this.setModelImageData()
    };

    if (JSON.stringify(currentState) != JSON.stringify(this.state.LastState)) {
      Log.debugGroupCollapsed("Last State : ", this.state.LastState);
      Log.debugGroupCollapsed("Current State : ", currentState);
      Log.debugGroupCollapsed("JSON UpdateTaskDetail changed");
      if (!Util.isNullOrEmpty(this.state.LastState.PersonalData.Name)) {
        Log.debugGroupCollapsed("hit on UpdateTaskDetail");
        this.onUpdateTaskDetail(() => {
          callback();
        });
      } else if (
        this.state.isCompany &&
        !Util.isNullOrEmpty(this.state.LastState.CompanyData.CompanyName)
      ) {
        Log.debugGroupCollapsed("hit on UpdateTaskDetail");
        this.onUpdateTaskDetail(() => {
          callback();
        });
      }
      this.setState({ LastState: currentState });
    } else {
      Log.debugGroupCollapsed("JSON UpdateTaskDetail not changed");
      callback();
    }
  };

  initializeErrorFlagMandatory = (callback = () => {}) => {
    this.setState(
      {
        // Personal data
        IsNotErrorpersonalname: false,
        IsNotErrorpersonaltanggallahir: false,
        IsNotErrorpersonalJK: false,
        IsNotErrorpersonalalamat: false,
        IsNotErrorpersonalkodepos: false,
        IsNotErrorpersonalemail: false,
        IsNotErrorpersonalhp: false,
        IsNotErrorpersonalnomoridentitas: false,
        IsNotErrorpersonalamountrep: false,

        // Company Data
        IsNotErrorcompanyname: false,
        IsNotErrorcompanynpwpnumber: false,
        IsNotErrorcompanynpwpdata: false,
        IsNotErrorcompanynpwpaddress: false,
        IsNotErrorcompanyofficeaddress: false,
        IsNotErrorcompanypostalcode: false,
        IsNotErrorcompanyofficenumber: false,
        IsNotErrorcompanypdcname: false,
        IsNotErrorcompanypichp: false,

        // Vehicle Data
        IsNotErrorvehiclevehiclecode: false,
        IsNotErrorvehiclebasiccoverage: false,
        IsNotErrorvehicleperiodfrom: false,
        IsNotErrorvehicleperiodto: false,
        IsNotErrorvehicleusage: false,
        IsNotErrorvehiclecolor: false,
        IsNotErrorvehicleproductcode: false,
        IsNotErrorvehiclesegmentcode: false,
        IsNotErrorvehicleplateno: false,
        IsNotErrorvehiclechasisno: false,
        IsNotErrorvehicleengineno: false,
        IsNotErrorvehicleregion: false,
        IsNotErrorvehicledealer: false,
        IsNotErrorvehiclesalesman: false,
        IsNotErrorvehiclenamebank: false,
        IsNotErrorvehiclenomorrek: false,
        IsNotErrorvehicletotalsuminsured: false,

        // Policy delivery
        IsNotErrorpolicysentto: false,
        IsNotErrorpolicyname: false,
        IsNotErrorpolicyaddress: false,
        IsNotErrorpolicykodepos: false,

        // Survey schedule
        IsNotErrorsurveykota: false,
        IsNotErrorsurveylokasi: false,
        IsNotErrorsurveyalamat: false,
        IsNotErrorsurveykodepos: false,
        IsNotErrorsurveytanggal: false,
        IsNotErrorsurveywaktu: false,

        // Image Mandatory
        IsNotErrorKTP: false,
        IsNotErrorSTNK: false,
        IsNotErrorBSTB: false,
        IsNotErrorDOCREP: false,
        IsNotErrorNPWP: false,
        IsNotErrorSIUP: false,
        IsNotErrorBUKTIBAYAR: false,
        IsNotErrorBUKTIBAYAR2: false,
        IsNotErrorBUKTIBAYAR3: false,
        IsNotErrorBUKTIBAYAR4: false,
        IsNotErrorBUKTIBAYAR5: false,
        IsNotErrorSPPAKB: false,
        IsNotErrorFAKTUR: false,
        IsNotErrorKONFIRMASICUST: false,
        IsNotErrorDOCNSA1: false,
        IsNotErrorDOCNSA2: false,
        IsNotErrorDOCNSA3: false,
        IsNotErrorDOCNSA4: false,
        IsNotErrorDOCNSA5: false,

        // Premi
        IsNotErrorTPLPremi: false,
        IsNotErrorSRCCPremi: false,
        IsNotErrorFLDPremi: false,
        IsNotErrorETVPremi: false,
        IsNotErrorTSPremi: false,
        IsNotErrorPADriverPremi: false,
        IsNotErrorPAPassangerPremi: false,
        IsNotErrorAccessPremi: false,

        // Document NSA Approval
        IsNotErrordocumentsremarkstosa: false
      },
      () => {
        callback();
      }
    );
  };

  setModelErrorFlagMandatory = () => {
    let ErrorFlagMandatory = {
      // Personal data
      IsNotErrorpersonalname: this.state.IsNotErrorpersonalname,
      IsNotErrorpersonaltanggallahir: this.state.IsNotErrorpersonaltanggallahir,
      IsNotErrorpersonalJK: this.state.IsNotErrorpersonalJK,
      IsNotErrorpersonalalamat: this.state.IsNotErrorpersonalalamat,
      IsNotErrorpersonalkodepos: this.state.IsNotErrorpersonalkodepos,
      IsNotErrorpersonalemail: this.state.IsNotErrorpersonalemail,
      IsNotErrorpersonalhp: this.state.IsNotErrorpersonalhp,
      IsNotErrorpersonalnomoridentitas: this.state
        .IsNotErrorpersonalnomoridentitas,
      IsNotErrorpersonalamountrep: this.state.IsNotErrorpersonalamountrep,

      // Company Data
      IsNotErrorcompanyname: this.state.IsNotErrorcompanyname,
      IsNotErrorcompanynpwpnumber: this.state.IsNotErrorcompanynpwpnumber,
      IsNotErrorcompanynpwpdata: this.state.IsNotErrorcompanynpwpdata,
      IsNotErrorcompanynpwpaddress: this.state.IsNotErrorcompanynpwpaddress,
      IsNotErrorcompanyofficeaddress: this.state.IsNotErrorcompanyofficeaddress,
      IsNotErrorcompanypostalcode: this.state.IsNotErrorcompanypostalcode,
      IsNotErrorcompanyofficenumber: this.state.IsNotErrorcompanyofficenumber,
      IsNotErrorcompanypdcname: this.state.IsNotErrorcompanypdcname,
      IsNotErrorcompanypichp: this.state.IsNotErrorcompanypichp,

      // Vehicle Data
      IsNotErrorvehiclevehiclecode: this.state.IsNotErrorvehiclevehiclecode,
      IsNotErrorvehiclebasiccoverage: this.state.IsNotErrorvehiclebasiccoverage,
      IsNotErrorvehicleperiodfrom: this.state.IsNotErrorvehicleperiodfrom,
      IsNotErrorvehicleperiodto: this.state.IsNotErrorvehicleperiodto,
      IsNotErrorvehicleusage: this.state.IsNotErrorvehicleusage,
      IsNotErrorvehiclecolor: this.state.IsNotErrorvehiclecolor,
      IsNotErrorvehicleproductcode: this.state.IsNotErrorvehicleproductcode,
      IsNotErrorvehiclesegmentcode: this.state.IsNotErrorvehiclesegmentcode,
      IsNotErrorvehicleplateno: this.state.IsNotErrorvehicleplateno,
      IsNotErrorvehiclechasisno: this.state.IsNotErrorvehiclechasisno,
      IsNotErrorvehicleengineno: this.state.IsNotErrorvehicleengineno,
      IsNotErrorvehicleregion: this.state.IsNotErrorvehicleregion,
      IsNotErrorvehicledealer: this.state.IsNotErrorvehicledealer,
      IsNotErrorvehiclesalesman: this.state.IsNotErrorvehiclesalesman,
      IsNotErrorvehiclenamebank: this.state.IsNotErrorvehiclenamebank,
      IsNotErrorvehiclenomorrek: this.state.IsNotErrorvehiclenomorrek,
      IsNotErrorvehicletotalsuminsured: this.state
        .IsNotErrorvehicletotalsuminsured,

      // Policy delivery
      IsNotErrorpolicysentto: this.state.IsNotErrorpolicysentto,
      IsNotErrorpolicyname: this.state.IsNotErrorpolicyname,
      IsNotErrorpolicyaddress: this.state.IsNotErrorpolicyname,
      IsNotErrorpolicykodepos: this.state.IsNotErrorpolicykodepos,

      // Survey schedule
      IsNotErrorsurveykota: this.state.IsNotErrorsurveykota,
      IsNotErrorsurveylokasi: this.state.IsNotErrorsurveylokasi,
      IsNotErrorsurveyalamat: this.state.IsNotErrorsurveyalamat,
      IsNotErrorsurveykodepos: this.state.IsNotErrorsurveykodepos,
      IsNotErrorsurveytanggal: this.state.IsNotErrorsurveytanggal,
      IsNotErrorsurveywaktu: this.state.IsNotErrorsurveywaktu,

      // Image Mandatory
      IsNotErrorKTP: this.state.IsNotErrorKTP,
      IsNotErrorSTNK: this.state.IsNotErrorSTNK,
      IsNotErrorBSTB: this.state.IsNotErrorBSTB,
      IsNotErrorDOCREP: this.state.IsNotErrorDOCREP,
      IsNotErrorNPWP: this.state.IsNotErrorNPWP,
      IsNotErrorSIUP: this.state.IsNotErrorSIUP,
      IsNotErrorBUKTIBAYAR: this.state.IsNotErrorBUKTIBAYAR,
      IsNotErrorBUKTIBAYAR2: this.state.IsNotErrorBUKTIBAYAR2,
      IsNotErrorBUKTIBAYAR3: this.state.IsNotErrorBUKTIBAYAR3,
      IsNotErrorBUKTIBAYAR4: this.state.IsNotErrorBUKTIBAYAR4,
      IsNotErrorBUKTIBAYAR5: this.state.IsNotErrorBUKTIBAYAR5,
      IsNotErrorSPPAKB: this.state.IsNotErrorSPPAKB,
      IsNotErrorFAKTUR: this.state.IsNotErrorFAKTUR,
      IsNotErrorKONFIRMASICUST: this.state.IsNotErrorKONFIRMASICUST,
      IsNotErrorDOCNSA1: this.state.IsNotErrorDOCNSA1,
      IsNotErrorDOCNSA2: this.state.IsNotErrorDOCNSA2,
      IsNotErrorDOCNSA3: this.state.IsNotErrorDOCNSA3,
      IsNotErrorDOCNSA4: this.state.IsNotErrorDOCNSA4,
      IsNotErrorDOCNSA5: this.state.IsNotErrorDOCNSA5,

      // Premi
      IsNotErrorTPLPremi: this.state.IsNotErrorTPLPremi,
      IsNotErrorSRCCPremi: this.state.IsNotErrorSRCCPremi,
      IsNotErrorFLDPremi: this.state.IsNotErrorFLDPremi,
      IsNotErrorETVPremi: this.state.IsNotErrorETVPremi,
      IsNotErrorTSPremi: this.state.IsNotErrorTSPremi,
      IsNotErrorPADriverPremi: this.state.IsNotErrorPADriverPremi,
      IsNotErrorPAPassangerPremi: this.state.IsNotErrorPAPassangerPremi,
      IsNotErrorAccessPremi: this.state.IsNotErrorAccessPremi,

      // Document NSA Approval
      IsNotErrordocumentsremarkstosa: this.state.IsNotErrordocumentsremarkstosa
    };

    return ErrorFlagMandatory;
  };

  setLocalStorageFlagError = () => {
    secureStorage.setItem(
      "ErrorFlagMandatory",
      JSON.stringify(this.setModelErrorFlagMandatory())
    );
  };

  componentWillUnmount() {

    let StateTaskDetail = JSON.parse(
      secureStorage.getItem("SelectedTaskDetail")
    );
    if (Util.isNullOrEmpty(StateTaskDetail)) {
      this.props.history.push("/");
    }
    if (StateTaskDetail.IsRenewalNonRenNot == 1) {
      //// HANDLE NONRENOT
    } else {
      this.componentWillUnmountTaskDetailDefault();
    }
  }

  componentWillUnmountTaskDetailDefault = () => {
    this.abortController.abort();
    this.abortControllerBasicPremium.abort();
    this.abortControllerUpdateTaskDetail.abort();
    this.abortControllerGetMappingPlate.abort();
    clearInterval(this.accesoriesInterval);
    clearInterval(this.loadallfirstall);
    clearInterval(this.intervalUpdateTaskDetail);
    this.setState(
      {
        LastState: {
          PersonalData: this.setModelPersonalData(),
          CompanyData: this.setModelCompanyData(),
          VehicleData: this.setModelVehicleData(),
          PolicyAddress: this.setModelPolicyAddress(),
          OrderSimulation: this.setModelOrderSimulation(),
          OrderSimulationMV: this.setModelOrderSimulationMV(),
          SurveySchedule: this.setModelSurveySchedule(),
          CalculatedPremiItems: this.state.CalculatedPremiItems,
          ImageID: this.setModelImageData()
        }
      },
      () => {
        this.onUpdateTaskDetailFuntion();
      }
    );
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   // this.rateCalculate();
  //   return this.state != nextState;
  // }

  onClickActionButton = (TypeAction = "CALLLATER") => {
    // VALIDASI

    Log.debugGroup("MASUKKKK");

    let FollowUpStatus = "";

    if (TypeAction == "CALLLATER") {
      FollowUpStatus = 3;
      // this.setState({
      //   FollowUpStatus: 3
      // });

      clearInterval(this.intervalUpdateTaskDetail);
      this.checkDoubleInsured(
        this.state.vehiclechasisno,
        this.state.vehicleengineno,
        this.state.selectedQuotation,
        () => {
          this.setState({
            isLoading: true
          });
          // this.onUpdateTaskDetail(() => {
          //   this.props.history.push("/tasklist-followupdetail");
          // }, FollowUpStatus);
          this.onUpdateTaskDetail(() => {
            this.setState({ isLoading: false });
            this.props.history.push("/tasklist-followupdetail");
            secureStorage.setItem("tasklist-followupdetail-FUStatus", 3);
          });
        }
      );
    }
    if (TypeAction == "POTENTIAL") {
      if (
        Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd") ==
        Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd")
      ) {
        Util.showToast("Harap Memilih Period To Yang Sesuai!", "WARNING");
        return;
      }
      if (!this.validationInputState()) {
        return;
      }

      FollowUpStatus = 2;
      // this.setState({
      //   FollowUpStatus: 2
      // });
      if (
        Util.isNullOrEmpty(this.state.vehiclechasisno) ||
        Util.isNullOrEmpty(this.state.vehicleengineno)
      ) {
        Util.showToast("Chasis Number and Engine Number Mandatory", "WARNING");
        return;
      }

      clearInterval(this.intervalUpdateTaskDetail);
      this.checkDoubleInsured(
        this.state.vehiclechasisno,
        this.state.vehicleengineno,
        this.state.selectedQuotation,
        () => {
          this.setState({
            isLoading: true
          });
          // this.onUpdateTaskDetail(() => {
          //   this.props.history.push("/tasklist-followupdetail");
          // }, FollowUpStatus);
          this.onUpdateTaskDetail(() => {
            this.setState({ isLoading: false });
            this.props.history.push("/tasklist-followupdetail");
            secureStorage.setItem("tasklist-followupdetail-FUStatus", 2);
          });
        }
      );
    }
    if (TypeAction == "DEAL") {
      Log.debugGroup(this.validationInputState());
      if (!this.validationInputState()) {
        return;
      }
      FollowUpStatus = 13;
      this.setState({
        isLoading: true
      });
      // this.setState({
      //   FollowUpStatus: 13
      // });
      // this.onUpdateTaskDetail(() => {
      //   this.props.history.push("/tasklist-followupdetail");
      // }, FollowUpStatus);

      clearInterval(this.intervalUpdateTaskDetail);
      this.onUpdateTaskDetail(() => {
        this.setState({ isLoading: false });
        this.props.history.push("/tasklist-followupdetail");
        secureStorage.setItem("tasklist-followupdetail-FUStatus", 13);
      });
    }
    if (TypeAction == "NOTDEAL") {
      FollowUpStatus = 5;
      this.setState({
        isLoading: true
      });

      clearInterval(this.intervalUpdateTaskDetail);
      this.onUpdateTaskDetail(() => {
        this.setState({ isLoading: false });
        this.props.history.push("/tasklist-followupdetail");
        secureStorage.setItem("tasklist-followupdetail-FUStatus", 5);
      });
    }

    // on update task detail
    // this.onUpdateTaskDetail(() => {
    //   this.props.history.push("/tasklist-followupdetail");
    // }, FollowUpStatus);
  };

  checkMaxPriceTSI = (value, callback = () => {}) => {
    if (typeof value == "number") {
      let MessageAlertCover = "";
      let showModalInfo = false;
      let maxPercent = 0;

      if (!this.state.vehicleusedcar) {
        MessageAlertCover = "Sum Insured melebihi +/-10% dari pricelist";
        maxPercent = 0.1;
      }
      if (this.state.vehicleusedcar) {
        MessageAlertCover = "Sum Insured melebihi +/-3% dari pricelist";
        maxPercent = 0.03;
      }

      // if (!this.state.IsRenewal) {
      //   MessageAlertCover = "Sum Insured melebihi +/-10% dari pricelist";
      //   maxPercent = 0.1;
      // } else {
      //   MessageAlertCover = "Sum Insured melebihi +/-3% dari pricelist";
      //   maxPercent = 0.03;
      // }

      if (maxPercent != 0) {
        let maxVehiclePrice =
          this.state.vehicletotalsuminsured +
          this.state.vehicletotalsuminsured * maxPercent;
        let minVehiclePrice =
          this.state.vehicletotalsuminsured -
          this.state.vehicletotalsuminsured * maxPercent;
        if (value > maxVehiclePrice || value < minVehiclePrice) {
          showModalInfo = true;
          this.setState({
            showModalInfo,
            MessageAlertCover
          });
        }
        callback();
      }
    }
  };

  checkDoubleInsured = (ChasisNo, EngineNo, OrderNo, callback = () => {}) => {
    if (Util.isNullOrEmpty(ChasisNo) || Util.isNullOrEmpty(EngineNo)) {
      // Util.showToast("Chasis Number and Engine Number Mandatory", "WARNING");
      callback();
      return;
    }
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/CheckDoubleInsured`,
      "POST",
      HEADER_API,
      {
        ChasisNo: ChasisNo,
        EngineNo: EngineNo,
        OrderNo: OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          isLoading: false
        });
        if (jsn.status) {
          if (jsn.data.IsDoubleInsured) {
            this.onShowAlertModalInfo(
              "Order Double Insured dengan order/polis aktif " +
                jsn.data.OrderNo +
                "/" +
                jsn.data.PolicyNo +
                " dengan periode " +
                Util.formatDate(jsn.data.PeriodFrom, "dd-mm-yyyy") +
                " s.d " +
                Util.formatDate(jsn.data.PeriodTo, "dd-mm-yyyy")
            );
          } else if (!jsn.data.IsDoubleInsured) {
            callback();
          }
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
        });
      });
  };

  checkSurveyResult = (
    OrderNo = this.state.selectedQuotation,
    callback = () => {}
  ) => {
    if (Util.isNullOrEmpty(OrderNo)) {
      Util.showToast("Order Number Mandatory", "WARNING");
      return;
    }
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/CheckSurveyResult`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.isChange) {
          callback();
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
        });
      });
  };

  onUpdateOrder = (
    OrderNo = this.state.selectedQuotation,
    callback = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/UpdateOrder`,
      "POST",
      HEADER_API,
      {
        OrderNo,
        SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        DeviceID: this.generateDeviceUUID()
      }
    )
      .then(res => res.json())
      .then(jsn => {
        callback();
        if (jsn.status) {
          if (!Util.isNullOrEmpty(jsn.data)) {
            this.setStateGetTaskDetail(jsn.data);
          }
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
        });
      });
  };

  getFollowUpStatus = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo,
    IsRenewal = this.state.IsRenewal,
    callback = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpStatus`,
      "POST",
      HEADER_API,
      {
        followupstatus: FollowUpStatus,
        followupstatusinfo: FollowUpInfo,
        followupno: this.state.FollowUpNo,
        isRenewal: IsRenewal ? 1 : 0
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          isLoading: false
        });
        if (jsn.status) {
          let followupstatusvisible = jsn.data.map(data => {
            return data.StatusCode;
          });

          this.setState({
            followupstatusvisible
          });
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
        });
      });
  };

  validationPersonalDocument = () => {
    let validation = true;
    let messages = "";

    if (!this.state.IsRenewal) {
      if (Util.isNullOrEmpty(this.state.KTP)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "Photo KTP/KITAS mandatory"
          : messages;
        this.setState(
          {
            IsNotErrorKTP: true
          },
          () => {
            this.setLocalStorageFlagError();
          }
        );
      }

      if (this.state.vehicleusedcar) {
        if (Util.isNullOrEmpty(this.state.STNK)) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Photo STNK mandatory"
            : messages;
          this.setState(
            {
              IsNotErrorSTNK: true
            },
            () => {
              this.setLocalStorageFlagError();
            }
          );
        }
      }

      if (!this.state.vehicleusedcar) {
        if (Util.isNullOrEmpty(this.state.BSTB)) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Photo BSTB mandatory"
            : messages;
          this.setState(
            {
              IsNotErrorBSTB: true
            },
            () => {
              this.setLocalStorageFlagError();
            }
          );
        }
      }
    }

    if (Util.isNullOrEmpty(this.state.KONFIRMASICUST)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Photo Konfirmasi Customer mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorKONFIRMASICUST: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationPersonalData = () => {
    let validation = true;
    let messages = "";

    if (Util.isNullOrEmpty(this.state.personalname)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Personal name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpersonalname: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personaltanggallahir)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Tanggal lahir mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpersonaltanggallahir: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalJK)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Jenis kelamin mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpersonalJK: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalalamat)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages) ? "Alamat mandatory" : messages;
      this.setState(
        {
          IsNotErrorpersonalalamat: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalkodepos)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages) ? "Kode pos mandatory" : messages;
      this.setState(
        {
          IsNotErrorpersonalkodepos: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalemail)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Email personal mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpersonalemail: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      !Util.isNullOrEmpty(this.state.personalemail) &&
      !Util.validateEmail(this.state.personalemail)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Email personal is not valid"
        : messages;
      this.setState(
        {
          IsNotErrorpersonalemail: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalhp)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages) ? "Nomor HP mandatory" : messages;
      this.setState(
        {
          IsNotErrorpersonalhp: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.personalnomoridentitas)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Nomor Identitas mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpersonalnomoridentitas: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (this.state.personalneeddocrep) {
      if (Util.isNullOrEmpty(this.state.personalamountrep)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "Amount Rep mandatory"
          : messages;
        this.setState(
          {
            IsNotErrorpersonalamountrep: true
          },
          () => {
            this.setLocalStorageFlagError();
          }
        );
      }
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationCompanyData = () => {
    let validation = true;
    let messages = "";

    if (Util.isNullOrEmpty(this.state.companyname)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanyname: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companynpwpnumber)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company NPWP Number mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanynpwpnumber: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companynpwpdata)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company NPWP Date mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanynpwpdata: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companynpwpaddress)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company NPWP Address mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanynpwpaddress: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companypostalcode)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company Postal Code mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanypostalcode: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companypdcname)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company PIC Name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanypdcname: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companypichp)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company PIC Handphone mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanypichp: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.companyemail)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company PIC Email mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorcompanyemail: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      !Util.isNullOrEmpty(this.state.companyemail) &&
      !Util.validateEmail(this.state.companyemail)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Company PIC Email is not valid"
        : messages;
      this.setState(
        {
          IsNotErrorcompanyemail: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationCompanyDocument = () => {
    let validation = true;
    let messages = "";

    if (!this.state.IsRenewal) {
      if (Util.isNullOrEmpty(this.state.NPWP)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "NPWP document mandatory"
          : messages;
        this.setState(
          {
            IsNotErrorNPWP: true
          },
          () => {
            this.setLocalStorageFlagError();
          }
        );
      }

      // if (Util.isNullOrEmpty(this.state.SIUP)) {
      //   validation = validation ? false : validation;
      //   messages = Util.isNullOrEmpty(messages)
      //     ? "SIUP document mandatory"
      //     : messages;
      //     this.setState({
      //       IsNotErrorSIUP: true
      //     }, () => {
      //       this.setLocalStorageFlagError();
      //     });
      // }
    }

    // if (this.state.IsRenewal) {
    if (Util.isNullOrEmpty(this.state.KONFIRMASICUST)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Photo Konfirmasi Customer mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorKONFIRMASICUST: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }
    // }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationVehicleData = () => {
    let validation = true;
    let messages = "";

    if (Util.isNullOrEmpty(this.state.vehiclecolor)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle color on STNK mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclecolor: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehiclevehiclecode)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages) ? "Vehicle mandatory" : messages;
      this.setState(
        {
          IsNotErrorvehiclevehiclecode: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehiclebasiccoverage)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Basic Coverage mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclebasiccoverage: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleperiodto)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Period to basic coverage mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleperiodto: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleperiodfrom)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Period from basic coverage mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleperiodfrom: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleusage)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle usage mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleusage: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehiclesegmentcode)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Segment code mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclesegmentcode: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleproductcode)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Product code mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleproductcode: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleplateno)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle plate no mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleplateno: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehiclechasisno)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle chasis number mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclechasisno: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleengineno)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle engine number mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleengineno: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicleregion)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle region mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicleregion: true,
          IsNotErrorvehicleplateno: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      this.state.IsSalesmanDealerEnable &&
      Util.isNullOrEmpty(this.state.vehicledealer)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Dealer name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicledealer: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      this.state.IsSalesmanDealerEnable &&
      Util.isNullOrEmpty(this.state.vehiclesalesman)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Salesman dealer name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclesalesman: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      this.state.IsSalesmanDealerEnable &&
      Util.isNullOrEmpty(this.state.vehiclenamebank)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Bank name mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclenamebank: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (
      this.state.IsSalesmanDealerEnable &&
      Util.isNullOrEmpty(this.state.vehiclenomorrek)
    ) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Account number mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehiclenomorrek: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.vehicletotalsuminsuredtemp)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Vehicle sum insured mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorvehicletotalsuminsured: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationPolicyDelivery = () => {
    let validation = true;
    let messages = "";

    if (Util.isNullOrEmpty(this.state.policysentto)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Policy sent to mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpolicysentto: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.policyname)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Policy name to mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpolicyname: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (Util.isNullOrEmpty(this.state.policyaddress)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Policy delivery address mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpolicyaddress: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    // if(this.state.policysentto != "GC"){
    if (Util.isNullOrEmpty(this.state.policykodepos)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Policy delivery postal code mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorpolicykodepos: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }
    // }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationSurveySchedule = () => {
    let validation = true;
    let messages = "";

    if (!this.state.surveynsaskipsurvey) {
      if (this.state.surveyneed) {
        if (Util.isNullOrEmpty(this.state.surveykota)) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Survey schedule city mandatory"
            : messages;
          this.setState(
            {
              IsNotErrorsurveykota: true
            },
            () => {
              this.setLocalStorageFlagError();
            }
          );
        }

        if (Util.isNullOrEmpty(this.state.surveylokasi)) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Survey schedule location mandatory"
            : messages;
          this.setState(
            {
              IsNotErrorsurveylokasi: true
            },
            () => {
              this.setLocalStorageFlagError();
            }
          );
        }

        if (Util.isNullOrEmpty(this.state.surveytanggal)) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Survey schedule date mandatory"
            : messages;
          this.setState(
            {
              IsNotErrorsurveytanggal: true
            },
            () => {
              this.setLocalStorageFlagError();
            }
          );
        }

        if (this.state.surveylokasi == "Others") {
          if (Util.isNullOrEmpty(this.state.surveykodepos)) {
            validation = validation ? false : validation;
            messages = Util.isNullOrEmpty(messages)
              ? "Survey schedule postal code mandatory"
              : messages;
            this.setState(
              {
                IsNotErrorsurveykodepos: true
              },
              () => {
                this.setLocalStorageFlagError();
              }
            );
          }

          if (Util.isNullOrEmpty(this.state.surveyalamat)) {
            validation = validation ? false : validation;
            messages = Util.isNullOrEmpty(messages)
              ? "Survey schedule address mandatory"
              : messages;
            this.setState(
              {
                IsNotErrorsurveyalamat: true
              },
              () => {
                this.setLocalStorageFlagError();
              }
            );
          }

          if (Util.isNullOrEmpty(this.state.surveywaktu)) {
            validation = validation ? false : validation;
            messages = Util.isNullOrEmpty(messages)
              ? "Survey schedule time mandatory"
              : messages;
            this.setState(
              {
                IsNotErrorsurveywaktu: true
              },
              () => {
                this.setLocalStorageFlagError();
              }
            );
          }
        }
      }
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationDocuments = () => {
    let validation = true;
    let messages = "";

    if (Util.isNullOrEmpty(this.state.SPPAKB)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Documents SPPAKB mandatory"
        : messages;
      this.setState(
        {
          IsNotErrorSPPAKB: true
        },
        () => {
          this.setLocalStorageFlagError();
        }
      );
    }

    if (this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46) {
      if (
        Util.isNullOrEmpty(this.state.DOCNSA1) &&
        Util.isNullOrEmpty(this.state.DOCNSA2) &&
        Util.isNullOrEmpty(this.state.DOCNSA3) &&
        Util.isNullOrEmpty(this.state.DOCNSA4) &&
        Util.isNullOrEmpty(this.state.DOCNSA5)
      ) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "This Prospect NSA Approval please upload Documents NSA Approval"
          : messages;
        this.setState(
          {
            IsNotErrorDOCNSA1: true,
            IsNotErrorDOCNSA2: true,
            IsNotErrorDOCNSA3: true,
            IsNotErrorDOCNSA4: true,
            IsNotErrorDOCNSA5: true
          },
          () => {
            this.setLocalStorageFlagError();
          }
        );
      }

      if (Util.isNullOrEmpty(this.state.documentsremarkstosa)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "This Prospect NSA Approval, remarks to SA is mandatory"
          : messages;
        this.setState(
          {
            IsNotErrordocumentsremarkstosa: true
          },
          () => {
            this.setLocalStorageFlagError();
          }
        );
      }
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationPremiItems = () => {
    let validation = true;
    let messages = "";

    if (this.state.IsPADRVRChecked == 1 && this.state.PADRVCOVER == 0) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "TSI PA Driver cannot null"
        : messages;
    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  validationInputState = () => {
    let validation = true;
    let messages = "";

    let validationData = { validation: true, messages: "" };

    this.initializeErrorFlagMandatory();

    // set localstorage flagmandatory
    this.setLocalStorageFlagError();

    // validation data personal
    if (!this.state.isCompany) {
      validationData = this.validationPersonalData();
      validation = validationData.validation
        ? validation
        : validationData.validation;
      messages = Util.isNullOrEmpty(messages)
        ? validationData.messages
        : messages;

      validationData = this.validationPersonalDocument();
      validation = validationData.validation
        ? validation
        : validationData.validation;
      messages = Util.isNullOrEmpty(messages)
        ? validationData.messages
        : messages;
    }

    if (this.state.isCompany) {
      validationData = this.validationCompanyData();
      validation = validationData.validation
        ? validation
        : validationData.validation;
      messages = Util.isNullOrEmpty(messages)
        ? validationData.messages
        : messages;

      validationData = this.validationCompanyDocument();
      validation = validationData.validation
        ? validation
        : validationData.validation;
      messages = Util.isNullOrEmpty(messages)
        ? validationData.messages
        : messages;
    }

    validationData = this.validationVehicleData();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    validationData = this.validationPolicyDelivery();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    validationData = this.validationSurveySchedule();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    validationData = this.validationDocuments();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    validationData = this.validationPremiItems();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    // if (!Util.isNullOrEmpty(messages)) {
    //   Util.showToast(messages, "WARNING");
    // }

    // set localstorage validation
    secureStorage.setItem("validationTaskDetail", validation);
    // return validation; // hasil return validation data
    return true;
  };

  onChangeFunction = event => {
    let eventSave = Object.assign({}, event);
    var nameEvent = event.target.name;
    if (Util.stringArrayContains(nameEvent, ["personalpayercompany"])) {
      if (Util.stringEquals(nameEvent, "personalpayercompany")) {
        this.getCheckVAPayment(
          this.state.selectedQuotation,
          () => {
            this.onShowAlertModalInfoIsPaid();
          },
          () => {
            this.setState({
              personalpayercompany: !this.state.personalpayercompany
            });
          }
        );
      }
      return;
    }

    this.onChangeNotAsync(event);
  };

  onChangeNotAsync = event => {
    const value =
      event.target[
        event.target.type === "checkbox"
          ? "checked"
          : event.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = event.target.name;
    let pattern = event.target.pattern;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({ [name]: value });
      }
    } else {
      this.setState(
        {
          [name]: value
        },
        () => {
          if (name == "personalneeddocrep") {
            if (value == false) {
              this.setState({ personalamountrep: null });
            }
          }
          if (name == "surveyneed") {
            this.surveyScheduleResetActive();
          }

          if (name == "vehicleisordefectrepair") {
            if (value) {
              this.handleNeedSurveyChecklist(() => {
                this.surveyScheduleResetActive();
              });
            } else {
              this.handleNeedSurveyUnChecklist();
            }
          }

          if (name == "vehicleusedcar") {
            if (value) {
              this.handleNeedSurveyChecklist(() => {
                this.surveyScheduleResetActive();
              });
            }
          }

          if (name == "vehicleisaccessorieschange") {
            if (value) {
              this.handleNeedSurveyChecklist(() => {
                this.surveyScheduleResetActive();
              });
            } else {
              this.handleNeedSurveyUnChecklist();
            }
          }

          if (name == "surveynsaskipsurvey") {
            // this.setState({
            //   surveyneed: false
            // });
            if (value) {
              this.handleNeedSurveyChecklist(() => {
                this.surveyScheduleResetActive();
              });
            } else {
              this.handleNeedSurveyUnChecklist();
            }
          }

          if (name == "surveychecklistmanual") {
            if (value) {
              this.handleNeedSurveyChecklist(() => {
                this.surveyScheduleResetActive();
              });
            } else {
              this.handleNeedSurveyUnChecklist();
            }
          }
        }
      );
    }

    if (name == "chItemSetPeriod") {
      if (value == true) {
        this.setState({
          vehicleperiodfrommodalperiod: this.state.vehicleperiodfrom,
          vehicleperiodtomodalperiod: this.state.vehicleperiodto
        });
      } else {
        this.setState({
          vehicleperiodfrommodalperiod: null,
          vehicleperiodtomodalperiod: null
        });
      }
    }

    if (name == "vehicleplateno") {
      this.getPlateRegion(value);
    }
  };

  surveyScheduleResetActive = () => {
    this.setState({
      surveykota: "",
      surveylokasi: "",
      surveyalamat: "",
      surveykodepos: "",
      surveytanggal: null,
      surveywaktu: null
    });
  };

  onChangeFunctionReactNumber = (value, name) => {
    this.setState({
      [name]: value
    });
  };

  onChangeFunctionDate = (value, name) => {
    // if (
    //   Util.stringArrayContains(name, ["vehicleperiodfrom", "vehicleperiodto"])
    // ) {

    this.setState({ [name]: value });

    if (name == "vehicleperiodfrom") {
      this.setState({ [name]: value }, () => {
        this.setPeriodToPeriodFrom(this.state.vehiclebasiccoverage, () => {
          this.trigerRateCalculation();
        });
      });
    } else {
      if (name == "vehicleperiodto") {
        this.setState({ [name]: value }, () => {
          this.trigerRateCalculation();
        });
      }
    }

    if (name == "surveytanggal") {
      // this.getSurveyScheduleTime(value, this.state.surveykodepos);
      if (this.state.surveylokasi == "Others") {
        this.getSlotTimeBooking(value, this.state.surveykodepos);
      } else {
        this.getSlotTimeBooking(value);
      }
    }
  };

  onOptionJKChange = personalJK => {
    this.setState({ personalJK });
  };

  onOptionSelect2Change = (value, name) => {
    this.setState({ [name]: value }, () => {
      if (name == "TPLCoverageIdtemp") {
        // this.rateCalculateTPLPAPASSPADRV("TPL");
        let TPLSICOVERtemp = [...this.state.dataTPLSIall].filter(
          data => data.CoverageId == value
        )[0].TSI;
        this.setState({
          TPLSICOVERtemp
        });
      }

      if (
        Util.stringArrayContains(name, [
          "vehiclevehiclecode",
          "vehiclevehiclecodeyear",
          "vehicleregion"
        ])
      ) {
        this.getVehiclePrice(
          this.state.vehiclevehiclecode,
          this.state.vehicleYear,
          this.state.vehicleregion,
          () => {
            this.setState(
              {
                vehicletotalsuminsuredtemp: this.state.vehicletotalsuminsured
              },
              () => {
                this.setPeriodToPeriodFrom(
                  this.state.vehiclebasiccoverage,
                  () => {
                    this.trigerRateCalculation();
                  }
                );
              }
            );
          }
        );
      }

      if (
        Util.stringArrayContains(name, [
          "vehiclevehiclecode",
          "vehiclevehiclecodeyear",
          "vehiclebasiccoverage",
          "vehicleproductcode",
          "vehicleregion",
          "vehicletotalsuminsured",
          "vehicleusage",
          "vehicleusedcar"
        ])
      ) {
        this.setPeriodToPeriodFrom(this.state.vehiclebasiccoverage, () => {
          this.trigerRateCalculation();
        });
      }

      if (name == "policysentto") {
        this.setState(
          {
            policyname: "",
            policyaddress: "",
            policykodepos: ""
          },
          () => {
            if (value == "CS") {
              this.getNameOnPolicyDelivery(this.state.CustID);
            } else if (value == "BR") {
              if (!Util.isNullOrEmpty(this.state.SalesOfficerID)) {
                this.getNameOnPolicyDelivery(this.state.SalesOfficerID);
              }
            }
          }
        );
      }

      if (name == "vehicleproductcode") {
        if (
          this.state.IsRenewal &&
          this.isNSAApproval() &&
          (this.state.vehicledealertemp != this.state.vehicledealer ||
            this.state.vehicleproductcode != this.state.vehicleproductcodetemp)
        ) {
          this.setState(
            {
              vehiclesalesman: this.state.vehiclesalesmantemp,
              vehicledealer: this.state.vehicledealertemp
            },
            () => {
              this.getDealerInformation(this.state.vehiclesalesman);
            }
          );
        }
      }

      if (
        Util.stringArrayContains(name, [
          "vehicleproductcode",
          "vehicleInsuranceType"
        ])
      ) {
        this.getCheckVAPayment(this.state.selectedQuotation, () => {
          this.onShowAlertModalInfoIsPaid();
        });
      }
    });

    if (name == "vehiclesalesman") {
      this.getDealerInformation(value);
    }

    if (name == "vehicledealer") {
      this.getSalesmanDealer(value);
      // this.getDealerInformation(value);
    }

    if (name == "vehiclevehiclecodeyear") {
      let data = [...this.state.datavehicleall].filter(
        data => data.VehicleCode + "-" + data.Year == value
      );

      // this.getListProductCode(
      //   data[0].ProductTypeCode,
      //   data[0].InsuranceType,
      //   this.state.vehicleusedcar ? 1 : 0
      // );

      Log.debugGroup(data);
      this.setState(
        {
          // vehicletotalsuminsured: data[0].Price,
          // vehicletotalsuminsuredtemp: data[0].Price,
          vehicleBrandCode: data[0].BrandCode,
          // vehicleInsuranceType: data[0].InsuranceType,
          vehicleModelCode: data[0].ModelCode,
          vehicleProductTypeCode: data[0].ProductTypeCode,
          vehicleSeries: data[0].Series,
          vehicleSitting: Util.isNullOrEmpty(data[0].Sitting)
            ? 0
            : data[0].Sitting,
          vehicleType: data[0].Type,
          vehicleYear: data[0].Year,
          vehiclevehiclecode: data[0].VehicleCode
        },
        () => {
          this.trigerRateCalculation();
          if (this.state.IsRenewal == true) {
            this.getBasicCover(
              this.state.isBasicCoverageState,
              this.state.vehicleYear
            );
          }
        }
      );

      if (data[0].Year != Util.convertDate().getFullYear()) {
        this.setState(
          {
            vehicleusedcar: true
          },
          () => {
            this.handleNeedSurveyChecklist();
          }
        );
        // this.surveyScheduleResetActive();
      } else {
        this.setState(
          {
            vehicleusedcar: false
          },
          () => {
            // this.handleNeedSurveyUnChecklist();
          }
        );
      }
    }

    if (name == "surveylokasi") {
      this.getSurveyDaysBranch(value); // get survey days by branch
      this.setState({
        surveykodepos: "",
        surveywaktu: null,
        surveytanggal: null
      });

      if (value == "Others") {
        this.getPostalCodeSurvey(this.state.surveykota);
      }
    }

    if (name == "vehicleproductcode") {
      this.setNdays(value, this.state.datavehicleproductcodeall);
      this.getSegmentCode(value);
      this.getEnableDisableSalesmanDealer(value);
    }

    if (name == "surveykota") {
      this.getSurveyLocation(value);
      // this.getSurveyDays(value);
      this.setState({
        surveylokasi: "",
        surveyalamat: "",
        surveykodepos: "",
        surveytanggal: null,
        surveywaktu: null
      });
    }

    if (name == "surveykodepos") {
      if (!Util.isNullOrEmpty(this.state.surveytanggal)) {
        // this.getSurveyScheduleTime(this.state.surveytanggal, value);
        this.getSlotTimeBooking(this.state.surveytanggal, value);
      }
    }

    if (name == "vehicleInsuranceType") {
      this.getListProductCode(
        this.state.ProductTypeCode,
        value,
        this.state.vehicleusedcar ? 0 : 1,
        this.state.IsRenewal ? 1 : 0,
        this.state.SalesOfficerID
      );
    }

    if (name == "policyaddress") {
      let data = [...this.state.datapolicyaddressall].filter(
        data => data.Name == value
      )[0];
      // console.log("Data", data);
      this.setState({
        // policyname: data.Name,
        policyaddress: data.Name,
        policykodepos: Util.valueTrim(data.PostalCode)
      });
    }

    if (name == "vehiclebasiccoverage") {
      this.setPeriodToPeriodFrom(value);
    }

    if (name == "personalpostalcode" || name == "companypostalcode") {
      this.handleSelectPostalCode(value, name);
    }
  };

  handleSelectPostalCode = (value, name) => {
    switch (name) {
      case "companypostalcode": {
        this.setState({
          companypostalcode: value,
          personalkodepos: value
        });
        break;
      }
      case "personalpostalcode": {
        this.setState({ personalkodepos: value });
        break;
      }
      default:
        return 0;
    }
  };

  setPeriodToPeriodFrom = (basiccoverageID, callback = () => {}) => {
    let tempData = [...this.state.databasiccoverall].filter(
      data => data.Id == basiccoverageID
    );
    if (tempData.length > 0) {
      let year = tempData[0].ComprePeriod + tempData[0].TLOPeriod;
      this.setState(
        {
          vehicleperiodfrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
            ? // ? Util.convertDate().setDate(Util.convertDate().getDate() + 1)
              Util.convertDate()
            : this.state.vehicleperiodfrom
        },
        () => {
          let vehicleperiodto = Util.convertDate(
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss")
          );
          vehicleperiodto.setFullYear(vehicleperiodto.getFullYear() + year);
          Log.debugGroup(vehicleperiodto);
          Log.debugGroup(this.state.vehicleperiodfrom);
          this.setState({ vehicleperiodto }, () => {
            callback();
          });
        }
      );
    } else {
      setTimeout(() => {
        Log.debugGroup("Hit timeout : setPeriodToPeriodFrom");
        this.setPeriodToPeriodFrom(basiccoverageID, callback);
      }, 2000);
    }
  };

  setPeriodToPeriodFromTS = (callback = () => {}) => {
    let CalculatedPremiItemsTemp = [...this.state.CalculatedPremiItems].filter(
      data => data.CoverageID.includes("SRC")
    );
    let periodto = null;
    let periodfrom = null;

    CalculatedPremiItemsTemp.forEach((data, index) => {
      if (index == 0) {
        periodfrom = Util.convertDate(data.PeriodFrom);
        periodto = Util.convertDate(data.PeriodTo);
      } else {
        if (periodfrom >= Util.convertDate(data.PeriodFrom)) {
          periodfrom = Util.convertDate(data.PeriodFrom);
        }
        if (periodto <= Util.convertDate(data.PeriodTo)) {
          periodto = Util.convertDate(data.PeriodTo);
        }
      }
    });

    this.setState(
      {
        periodfromTS: periodfrom,
        periodtoTS: periodto
      },
      () => {
        callback();
      }
    );
  };

  setPeriodToPeriodFromExtendedCover = (
    TYPE = "",
    CoverageOrInterest = "",
    callback = () => {}
  ) => {
    let CalculatedPremiItemsTemp = [...this.state.CalculatedPremiItems].filter(
      data => {
        if (CoverageOrInterest.toUpperCase() == "COVERAGE") {
          return data.CoverageID.includes(TYPE);
        } else if (CoverageOrInterest.toUpperCase() == "INTEREST") {
          return data.InterestID.includes(TYPE);
        } else {
          return false;
        }
      }
    );

    let basicCoverage = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.vehiclebasiccoverage
    )[0];

    let periodfrom = Util.convertDate(
      Util.formatDate(this.state.vehicleperiodfrom)
    );
    let periodto = Util.convertDate(
      Util.formatDate(this.state.vehicleperiodfrom)
    ).setFullYear(
      Util.convertDate(
        Util.formatDate(this.state.vehicleperiodfrom)
      ).getFullYear() +
        (parseInt(basicCoverage.ComprePeriod) +
          parseInt(basicCoverage.TLOPeriod))
    );

    if (TYPE == "TPLPER") {
      periodto = Util.convertDate(
        Util.formatDate(this.state.vehicleperiodfrom)
      ).setFullYear(
        Util.convertDate(
          Util.formatDate(this.state.vehicleperiodfrom)
        ).getFullYear() + basicCoverage.ComprePeriod
      );
    }

    if (basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0) {
      periodto = Util.convertDate(Util.formatDate(this.state.vehicleperiodto));
    }

    CalculatedPremiItemsTemp.forEach((data, index) => {
      if (index == 0) {
        periodfrom = Util.convertDate(data.PeriodFrom);
        periodto = Util.convertDate(data.PeriodTo);
      } else {
        if (periodfrom >= Util.convertDate(data.PeriodFrom)) {
          periodfrom = Util.convertDate(data.PeriodFrom);
        }
        if (periodto <= Util.convertDate(data.PeriodTo)) {
          periodto = Util.convertDate(data.PeriodTo);
        }
      }
    });

    this.setState(
      {
        periodfromTemp: periodfrom,
        periodtoTemp: periodto
      },
      () => {
        callback();
      }
    );
  };

  setNdays = (
    ProductCode = this.state.vehicleproductcode,
    datavehicleproductcodeall = this.state.datavehicleproductcodeall,
    callback = () => {}
  ) => {
    if (datavehicleproductcodeall.length > 0) {
      let data = [...datavehicleproductcodeall].filter(
        data => data.ProductCode == ProductCode
      );
      data = data[0];
      Log.debugGroup("setdays", data);
      this.setState(
        {
          Ndays:
            data != null && data != undefined
              ? Util.isNullOrEmpty(data.Ndays)
                ? "0"
                : data.Ndays
              : "0"
        },
        () => callback()
      );
    } else {
      callback();
    }
  };

  setInitialStateTaskDetail = (callback = () => {}) => {
    this.setState(
      {
        FollowUpStatus: "",
        FollowUpInfo: "",
        Admin: "",
        GrossPremium: "",
        NetPremi: "",
        selectedQuotation: "",
        SurveyNo: "",
        PolicyOrderNo: "",
        OldPolicyPeriodTo: null,
        personalalamat: "",
        // personaltanggallahir: Util.convertDate().setFullYear(
        //   Util.convertDate().getFullYear() - 17
        // ),
        personaltanggallahir: null,
        personalJK: "",
        personalemail: "",
        IdentityNo: "",
        personalname: "",
        personalhp: "",
        personalkodepos: "",
        personalnomoridentitas: "",
        companyname: "",
        companynpwpnumber: "",
        companynpwpdata: null,
        companynpwpaddress: "",
        companysiupnumber: "",
        companypkpnumber: "",
        companypkpdata: "",
        companyofficeaddress: "",
        companypostalcode: "",
        companyofficenumber: "",
        companypdcname: "",
        companyemail: "",
        vehicleusedcar: false,
        vehiclevehiclecode: "",
        vehiclebasiccoverage: "",
        vehiclebasiccoverage: "",
        vehicleperiodfrom: null, //
        vehicleperiodto: null, //
        vehicleusage: "",
        vehiclecolor: "",
        vehicleproductcode: "",
        vehiclepolicyno: "",
        vehicleplateno: "",
        vehiclechasisno: "",
        vehicleengineno: "",
        vehicleregion: "",
        vehicledealer: 0,
        vehiclesalesman: 0,
        vehiclenamebank: "",
        vehiclenomorrek: "", //
        vehicletotalsuminsured: 0,
        vehicletotalsuminsuredtemp: 0,
        vehicleispolicyissuedbeforpaying: false,
        vehicleremarks: "", //

        vehiclesegmentcode: "",
        vehicleAccessSI: 0,
        vehicleBrandCode: "",
        vehicleInsuranceType: "",
        vehicleModelCode: "",
        vehicleProductTypeCode: "",
        vehicleSeries: "",
        vehicleSitting: 0,
        vehicleType: "",
        vehicleVANumber: "",
        vehicleYear: "",
        vehicleispolicyissuedbeforpaying: false,
        datavehicle: [],

        paymentduedate: null,
        paymentvapermata: "",
        paymentvamandiri: "",
        paymentvabca: "",
        paymentnorek: "",
        paymentnova: "",
        paymentisvaactive: true,
        dataBUKTIBAYAR: [],
        BUKTIBAYAR: "",
        dataBUKTIBAYAR2: [],
        BUKTIBAYAR2: "",
        dataBUKTIBAYAR3: [],
        BUKTIBAYAR3: "",
        dataBUKTIBAYAR4: [],
        BUKTIBAYAR4: "",
        dataBUKTIBAYAR5: [],
        BUKTIBAYAR5: "",

        surveykota: "",
        surveylokasi: "",
        surveyalamat: "",
        surveykodepos: "",
        KTP: "",
        dataKTP: [],
        BSTB: "",
        dataBSTB: [],
        STNK: "",
        dataSTNK: [],
        DOCREP: "",
        dataDOCREP: [],
        personalneeddocrep: false,
        personalamountrep: null,
        personalpayercompany: false,
        KONFIRMASICUST: "",
        dataKONFIRMASICUST: [],

        dataDOCNSA1: [],
        DOCNSA1: "",
        dataDOCNSA2: [],
        DOCNSA2: "",
        dataDOCNSA3: [],
        DOCNSA3: "",
        dataDOCNSA4: [],
        DOCNSA4: "",
        dataDOCNSA5: [],
        DOCNSA5: "",
        dataFAKTUR: [],
        FAKTUR: "",
        dataSPPAKB: [],
        SPPAKB: "",
        dataNPWP: [],
        NPWP: "",
        dataSIUP: [],
        SIUP: ""
      },
      () => {
        callback();
      }
    );
  };

  setStateTempFirstLoad = (dataGetTaskDetail, CustID = this.state.CustID) => {
    let FieldPremi = dataGetTaskDetail.FieldPremi;
    let PersonalData = dataGetTaskDetail.PersonalData;
    let CompanyData = dataGetTaskDetail.CompanyData;
    let VehicleData = dataGetTaskDetail.VehicleData;
    let PoliciAddress = dataGetTaskDetail.PolicyAddres;
    let PaymentInfo = dataGetTaskDetail.PaymentInfo;
    let SurveySchedule = dataGetTaskDetail.SurveySchedule;
    let PersonalDocument = dataGetTaskDetail.PersonalDocument[0];
    let Document = dataGetTaskDetail.Document[0];
    let FollowUpInfo = Util.isNullOrEmpty(dataGetTaskDetail.FollowUpInfo)
      ? null
      : dataGetTaskDetail.FollowUpInfo[0];
    let CompanyDocument = dataGetTaskDetail.CompanyDocument[0];
    let SurveyData = dataGetTaskDetail.SurveyData;

    if (!Util.isNullOrEmpty(PersonalData)) {
      this.setState({
        personalalamattemp: Util.isNullOrEmpty(PersonalData.CustAddress)
          ? ""
          : PersonalData.CustAddress,
        // personaltanggallahir:
        //   (!Util.isNullOrEmpty(PersonalData.CustBirthDay) &&
        //     Util.convertDate(PersonalData.CustBirthDay)) ||
        //   Util.convertDate().setFullYear(Util.convertDate().getFullYear() - 17),
        personaltanggallahirtemp:
          (!Util.isNullOrEmpty(PersonalData.CustBirthDay) &&
            Util.convertDate(PersonalData.CustBirthDay)) ||
          null,
        personalJKtemp: PersonalData.CustGender,
        personalemailtemp: PersonalData.Email1,
        IdentityNotemp: PersonalData.IdentityNo,
        personalnametemp: PersonalData.Name,
        personalhptemp: PersonalData.Phone1,
        personalkodepostemp: PersonalData.PostalCode,
        personalnomoridentitastemp: Util.isNullOrEmpty(PersonalData.IdentityNo)
          ? ""
          : PersonalData.IdentityNo,
        personalneeddocreptemp: PersonalData.isNeedDocRep,
        personalamountreptemp: PersonalData.AmountRep,
        personalpayercompanytemp: Util.isNullOrEmpty(
          PersonalData.IsPayerCompany
        )
          ? false
          : PersonalData.IsPayerCompany == 0
          ? false
          : true,
        isCompanytemp: Util.isNullOrEmpty(PersonalData.isCompany)
          ? false
          : PersonalData.isCompany == 0
          ? false
          : true
      });
    }

    if (!Util.isNullOrEmpty(CompanyData)) {
      this.setState({
        companynametemp: CompanyData.CompanyName,
        companynpwpnumbertemp: CompanyData.NPWPno,
        companynpwpdatatemp:
          (!Util.isNullOrEmpty(CompanyData.NPWPdate) &&
            Util.convertDate(CompanyData.NPWPdate)) ||
          null,
        companynpwpaddresstemp: CompanyData.NPWPaddres,
        companysiupnumbertemp: CompanyData.SIUPno,
        companypkpnumbertemp: CompanyData.PKPno,
        companypkpdatatemp: CompanyData.PKPdate,
        companyofficeaddresstemp: CompanyData.OfficeAddress,
        companypostalcodetemp: CompanyData.PostalCode,
        companyofficenumbertemp: CompanyData.OfficePhoneNo,
        companypdcnametemp: CompanyData.PICname,
        companypichptemp: CompanyData.PICPhoneNo,
        companyemailtemp: CompanyData.PICEmail
      });
    }

    if (!Util.isNullOrEmpty(FieldPremi)) {
      this.setState({
        Admintemp: FieldPremi.Admin,
        GrossPremiumtemp: FieldPremi.GrossPremium,
        NetPremitemp: FieldPremi.NetPremi,
        selectedQuotationtemp: FieldPremi.OrderNo,
        SurveyNotemp: FieldPremi.SurveyNo,
        PolicyOrderNotemp: FieldPremi.PolicyOrderNo,
        OldPolicyPeriodTotemp: Util.isNullOrEmpty(FieldPremi.OldPolicyPeriodTo)
          ? null
          : Util.convertDate(FieldPremi.OldPolicyPeriodTo)
      });
    }

    if (!Util.isNullOrEmpty(VehicleData)) {
      this.setState({
        vehicleusedcartemp:
          VehicleData.IsNew == null
            ? false
            : VehicleData.IsNew == 1
            ? false
            : true,
        vehiclevehiclecodetemp: VehicleData.VehicleCode,
        vehiclevehiclecodeyeartemp: Util.isNullOrEmpty(VehicleData.VehicleCode)
          ? null
          : VehicleData.VehicleCode + "-" + VehicleData.Year,
        vehiclebasiccoveragetemp: VehicleData.BasicCoverID,
        vehicleperiodfromtemp: Util.isNullOrEmpty(VehicleData.PeriodTo)
          ? null
          : Util.convertDate(VehicleData.PeriodFrom),
        vehicleperiodtotemp: Util.isNullOrEmpty(VehicleData.PeriodTo)
          ? null
          : Util.convertDate(VehicleData.PeriodTo),
        vehicleusagetemp: VehicleData.UsageCode,
        vehiclecolortemp: Util.isNullOrEmpty(VehicleData.ColorOnBPKB)
          ? ""
          : VehicleData.ColorOnBPKB,
        vehicleproductcodetemp: VehicleData.ProductCode,
        vehiclepolicynotemp: Util.isNullOrEmpty(VehicleData.PolicyNo)
          ? ""
          : VehicleData.PolicyNo,
        vehicleplatenotemp: VehicleData.RegistrationNumber,
        vehiclechasisnotemp: VehicleData.ChasisNumber,
        vehicleenginenotemp: VehicleData.EngineNumber,
        vehicleregiontemp: VehicleData.CityCode,
        vehicledealertemp: VehicleData.DealerCode,
        vehiclesalesmantemp: VehicleData.SalesDealer,
        vehicleisaccessorieschangetemp:
          VehicleData.IsAccessoriesChange == 1 ? true : false,
        vehicleisordefectrepairtemp:
          VehicleData.IsORDefectsRepair == 1 ? true : false,
        vehicleispolicyissuedbeforpayingtemp:
          VehicleData.IsPolicyIssuedBeforePaying == 1 ? true : false,
        vehiclecacatsemulatemp: VehicleData.ORDefectsDesc,
        vehiclenocovertemp: VehicleData.NoCover,
        vehicleaccesoriestemp: VehicleData.Accessories,
        vehicleremarkstemp: Util.isNullOrEmpty(VehicleData.Remarks)
          ? ""
          : VehicleData.Remarks,

        vehiclesegmentcodetemp: VehicleData.SegmentCode,
        vehicleAccessSItemp: Util.isNullOrEmpty(VehicleData.AccessSI)
          ? 0
          : VehicleData.AccessSI,
        vehicleBrandCodetemp: Util.valueTrim(VehicleData.BrandCode),
        vehicleInsuranceTypetemp: VehicleData.InsuranceType,
        vehicleModelCodetemp: VehicleData.ModelCode,
        vehicleProductTypeCodetemp: VehicleData.ProductTypeCode,
        vehicleSeriestemp: VehicleData.Series,
        vehicleSittingtemp: VehicleData.Sitting,
        vehicleTypetemp: VehicleData.Type,
        vehicleVANumbertemp: VehicleData.VANumber,
        vehicleYeartemp: VehicleData.Year,
        datavehicletemp: [
          {
            value: VehicleData.VehicleCode + "-" + VehicleData.Year,
            label: VehicleData.Vehicle
          }
        ]
      });
    }
  };

  setStateGetTaskDetail = (dataGetTaskDetail, CustID = this.state.CustID) => {
    this.state.isflagupdatetaskdetail = false;
    this.setStateTempFirstLoad(dataGetTaskDetail);
    this.initializeErrorFlagMandatory(() => {
      let isFlagShowMandatory = secureStorage.getItem(
        "taskdetailshowmandatory"
      );
      if (!Util.isNullOrEmpty(isFlagShowMandatory)) {
        if (JSON.parse(isFlagShowMandatory)) {
          this.setFlagMandatoryFromLocalStorage();
        }
        secureStorage.removeItem("taskdetailshowmandatory");
      }
    });
    let FieldPremi = dataGetTaskDetail.FieldPremi;
    let PersonalData = dataGetTaskDetail.PersonalData;
    let CompanyData = dataGetTaskDetail.CompanyData;
    let VehicleData = dataGetTaskDetail.VehicleData;
    let PoliciAddress = dataGetTaskDetail.PolicyAddres;
    let PaymentInfo = dataGetTaskDetail.PaymentInfo;
    let SurveySchedule = dataGetTaskDetail.SurveySchedule;
    let PersonalDocument = dataGetTaskDetail.PersonalDocument[0];
    let Document = dataGetTaskDetail.Document[0];
    let FollowUpInfo = Util.isNullOrEmpty(dataGetTaskDetail.FollowUpInfo)
      ? null
      : dataGetTaskDetail.FollowUpInfo[0];
    let CompanyDocument = dataGetTaskDetail.CompanyDocument[0];
    let SurveyData = dataGetTaskDetail.SurveyData;
    let Remarks = dataGetTaskDetail.Remaks;

    if (!Util.isNullOrEmpty(FollowUpInfo)) {
      Log.debugGroup(
        "setStateGetTaskDetail = > RUN FollowUpInfo ",
        FollowUpInfo
      );
      this.setState(
        {
          vehicleagency: FollowUpInfo.Agency,
          FollowUpStatus: FollowUpInfo.FollowUpStatus,
          FollowUpInfo: FollowUpInfo.FollowUpInfo,
          SalesOfficerID: FollowUpInfo.SalesOfficerID,
          SalesOfficerName: FollowUpInfo.Name,
          // vehicleupliner: (Util.isNullOrEmpty(FollowUpInfo.Upliner)?"":FollowUpInfo.Upliner),
          IsRenewal: Util.isNullOrEmpty(FollowUpInfo.IsRenewal)
            ? false
            : FollowUpInfo.IsRenewal == 0
            ? false
            : true
        },
        () => {
          this.getListProductType();
          // this.getListProductCode();
          this.getUpliner();

          this.getFollowUpStatus(
            FollowUpInfo.FollowUpStatus,
            FollowUpInfo.FollowUpInfo,
            this.state.IsRenewal
          );

          if (this.state.IsRenewal) {
            if (FollowUpInfo.FollowUpInfo == 46) {
              if (!Util.isNullOrEmpty(VehicleData)) {
                this.getBasicCover(
                  true,
                  // Util.isNullOrEmpty(VehicleData.Year) ? "" : VehicleData.Year
                  ""
                );
              }

              this.setState({
                isBasicCoverageState: true
              });
            } else {
              if (!Util.isNullOrEmpty(VehicleData)) {
                this.getBasicCover(
                  false,
                  Util.isNullOrEmpty(VehicleData.Year) ? "" : VehicleData.Year
                );
              }
              this.setState({
                isBasicCoverageState: false
              });
            }
          }

          if (
            FollowUpInfo.FollowUpStatus == 2 &&
            FollowUpInfo.FollowUpInfo == 59
          ) {
            if (!Util.isNullOrEmpty(FieldPremi.OrderNo)) {
              this.checkSurveyResult(FieldPremi.OrderNo, () => {
                this.setState({
                  showModalCheckSurveyResult: true,
                  selectedQuotation: FieldPremi.OrderNo
                });
              });
            }
          }
        }
      );
    } else {
      this.loadProductType = true;
    }

    if (!Util.isNullOrEmpty(FieldPremi)) {
      this.setState(
        {
          Admin: FieldPremi.Admin,
          GrossPremium: FieldPremi.GrossPremium,
          NetPremi: FieldPremi.NetPremi,
          selectedQuotation: FieldPremi.OrderNo,
          SurveyNo: FieldPremi.SurveyNo,
          NoClaimBonus: FieldPremi.NoClaimBonus,
          OldPolicyNo: Util.isNullOrEmpty(FieldPremi.OldPolicyNo)
            ? ""
            : Util.valueTrim(FieldPremi.OldPolicyNo),
          PolicyOrderNo: FieldPremi.PolicyOrderNo,
          OldPolicyPeriodTo: Util.isNullOrEmpty(FieldPremi.OldPolicyPeriodTo)
            ? null
            : Util.convertDate(FieldPremi.OldPolicyPeriodTo)
        },
        () => {
          if (!Util.isNullOrEmpty(this.state.OldPolicyPeriodTo)) {
            if (this.state.OldPolicyPeriodTo < Util.convertDate()) {
              this.handleNeedSurveyChecklist();
            } else {
              this.handleNeedSurveyUnChecklist();
            }
          }
          this.getCheckVAPayment(FieldPremi.OrderNo);
        }
      );
    }

    if (!Util.isNullOrEmpty(PersonalData)) {
      this.setState({
        personalalamat: Util.isNullOrEmpty(PersonalData.CustAddress)
          ? ""
          : PersonalData.CustAddress,
        // personaltanggallahir:
        //   (!Util.isNullOrEmpty(PersonalData.CustBirthDay) &&
        //     Util.convertDate(PersonalData.CustBirthDay)) ||
        //   Util.convertDate().setFullYear(Util.convertDate().getFullYear() - 17),
        personaltanggallahir:
          (!Util.isNullOrEmpty(PersonalData.CustBirthDay) &&
            Util.convertDate(PersonalData.CustBirthDay)) ||
          null,
        personalJK: PersonalData.CustGender,
        personalemail: PersonalData.Email1,
        IdentityNo: PersonalData.IdentityNo,
        personalname: PersonalData.Name,
        personalhp: PersonalData.Phone1,
        personalkodepos: Util.valueTrim(PersonalData.PostalCode),
        personalnomoridentitas: Util.isNullOrEmpty(PersonalData.IdentityNo)
          ? ""
          : PersonalData.IdentityNo,
        personalneeddocrep: PersonalData.isNeedDocRep,
        personalamountrep: PersonalData.AmountRep,
        personalpayercompany: Util.isNullOrEmpty(PersonalData.IsPayerCompany)
          ? false
          : PersonalData.IsPayerCompany == 0
          ? false
          : true,
        isCompany: Util.isNullOrEmpty(PersonalData.isCompany)
          ? false
          : PersonalData.isCompany == 0
          ? false
          : true
      });
    }

    if (!Util.isNullOrEmpty(CompanyData)) {
      this.setState({
        companyname: CompanyData.CompanyName,
        companynpwpnumber: CompanyData.NPWPno,
        companynpwpdata:
          (!Util.isNullOrEmpty(CompanyData.NPWPdate) &&
            Util.convertDate(CompanyData.NPWPdate)) ||
          null,
        companynpwpaddress: CompanyData.NPWPaddres,
        companysiupnumber: CompanyData.SIUPno,
        companypkpnumber: CompanyData.PKPno,
        companypkpdata: CompanyData.PKPdate,
        companyofficeaddress: CompanyData.OfficeAddress,
        companypostalcode: CompanyData.PostalCode,
        companyofficenumber: CompanyData.OfficePhoneNo,
        companypdcname: CompanyData.PICname,
        companypichp: CompanyData.PICPhoneNo,
        companyemail: CompanyData.PICEmail
      });
    }

    if (!Util.isNullOrEmpty(VehicleData)) {
      this.setState(
        {
          vehicleusedcar:
            VehicleData.IsNew == null
              ? false
              : VehicleData.IsNew == 1
              ? false
              : true,
          vehiclevehiclecode: VehicleData.VehicleCode,
          vehiclevehiclecodeyear: Util.isNullOrEmpty(VehicleData.VehicleCode)
            ? null
            : VehicleData.VehicleCode + "-" + VehicleData.Year,
          vehiclebasiccoverage: VehicleData.BasicCoverID,
          vehicleperiodfrom: Util.isNullOrEmpty(VehicleData.PeriodTo)
            ? null
            : Util.convertDate(VehicleData.PeriodFrom),
          vehicleperiodto: Util.isNullOrEmpty(VehicleData.PeriodTo)
            ? null
            : Util.convertDate(VehicleData.PeriodTo),
          vehicleusage: VehicleData.UsageCode,
          vehiclecolor: Util.isNullOrEmpty(VehicleData.ColorOnBPKB)
            ? ""
            : VehicleData.ColorOnBPKB,
          vehicleproductcode: VehicleData.ProductCode,
          vehiclepolicyno: Util.isNullOrEmpty(VehicleData.PolicyNo)
            ? ""
            : VehicleData.PolicyNo,
          vehicleplateno: VehicleData.RegistrationNumber,
          vehiclechasisno: VehicleData.ChasisNumber,
          vehicleengineno: VehicleData.EngineNumber,
          vehicleregion: VehicleData.CityCode,
          vehicledealer: VehicleData.DealerCode,
          vehiclesalesman: VehicleData.SalesDealer,
          vehicletotalsuminsured: VehicleData.SumInsured,
          vehicletotalsuminsuredtemp: VehicleData.SumInsured,
          vehicleisaccessorieschange:
            VehicleData.IsAccessoriesChange == 1 ? true : false,
          vehicleisordefectrepair:
            VehicleData.IsORDefectsRepair == 1 ? true : false,
          vehicleispolicyissuedbeforpaying:
            VehicleData.IsPolicyIssuedBeforePaying == 1 ? true : false,
          vehiclecacatsemula: VehicleData.ORDefectsDesc,
          vehiclenocover: VehicleData.NoCover,
          vehicleaccesories: VehicleData.Accessories,
          vehicleremarks: Util.isNullOrEmpty(VehicleData.Remarks)
            ? ""
            : VehicleData.Remarks,
          vehiclesegmentcode: VehicleData.SegmentCode,
          vehicleAccessSI: Util.isNullOrEmpty(VehicleData.AccessSI)
            ? 0
            : VehicleData.AccessSI,
          vehicleBrandCode: Util.valueTrim(VehicleData.BrandCode),
          vehicleInsuranceType: VehicleData.InsuranceType,
          vehicleModelCode: VehicleData.ModelCode,
          vehicleProductTypeCode: VehicleData.ProductTypeCode,
          vehicleSeries: VehicleData.Series,
          vehicleSitting: VehicleData.Sitting,
          vehicleType: VehicleData.Type,
          vehicleVANumber: VehicleData.VANumber,
          vehicleYear: VehicleData.Year,
          datavehicle: [
            {
              value: VehicleData.VehicleCode + "-" + VehicleData.Year,
              label: VehicleData.Vehicle
            }
          ],
          PASSCOVER: Util.isNullOrEmpty(VehicleData.Sitting)
            ? this.state.PASSCOVER
            : parseInt(Util.valueTrim(VehicleData.Sitting)) - 1
        },
        () => {
          Log.debugGroup("hit ratecalculation from getTaskDetail");
          if (!Util.isNullOrEmpty(VehicleData.RegistrationNumber)) {
            this.getPlateRegionLoad(
              VehicleData.RegistrationNumber,
              VehicleData
            );
          }
          if (!Util.isNullOrEmpty(VehicleData.ProductCode)) {
            /// HANDLE INI NANTI !!!!!!!!!!!
            this.getListProductCode(
              VehicleData.ProductTypeCode,
              VehicleData.InsuranceType,
              VehicleData.IsNew == null ? 0 : VehicleData.IsNew ? 1 : 0,
              FollowUpInfo.IsRenewal == null
                ? 0
                : FollowUpInfo.IsRenewal
                ? 1
                : 0,
              !Util.isNullOrEmpty(FollowUpInfo.SalesOfficerID)
                ? FollowUpInfo.SalesOfficerID
                : JSON.parse(ACCOUNTDATA.UserInfo.User.SalesOfficerID),
              () => {
                Log.debugGroup(
                  "VehicleData from getTaskDetail => ",
                  VehicleData
                );
                this.setNdays(
                  VehicleData.ProductCode,
                  this.state.datavehicleproductcodeall,
                  () => {
                    this.getVehiclePrice(
                      this.state.vehiclevehiclecode || VehicleData.VehicleCode,
                      this.state.vehicleYear || VehicleData.Year,
                      this.state.vehicleregion || VehicleData.CityCode,
                      () => {
                        this.setState(
                          {
                            vehicletotalsuminsuredtemp: VehicleData.SumInsured
                          },
                          () => {
                            if (!Util.isNullOrEmpty(FieldPremi.OrderNo)) {
                              this.loadPremiumCalculation(
                                FieldPremi.OrderNo,
                                Util.isNullOrEmpty(FollowUpInfo.IsRenewal)
                                  ? false
                                  : FollowUpInfo.IsRenewal == 0
                                  ? false
                                  : true,
                                Util.isNullOrEmpty(FieldPremi.OldPolicyNo)
                                  ? ""
                                  : Util.valueTrim(FieldPremi.OldPolicyNo)
                              );
                            } else {
                              this.basicPremiCalculation();
                            }
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          } else {
            this.state.isflagupdatetaskdetail = true;
            this.loadpremicalculationfirst = true;
          }

          if (VehicleData.Year != Util.convertDate().getFullYear()) {
            this.setState({
              vehicleusedcar: true
            });
            // this.surveyScheduleResetActive();
          }

          if (
            !Util.isNullOrEmpty(VehicleData.BasicCoverID) &&
            Util.isNullOrEmpty(VehicleData.PeriodTo)
          ) {
            this.setPeriodToPeriodFrom(VehicleData.BasicCoverID);
          }

          if (
            !Util.isNullOrEmpty(VehicleData.InsuranceType) &&
            !Util.isNullOrEmpty(VehicleData.ProductTypeCode)
          ) {
            this.getListProductCode(
              VehicleData.ProductTypeCode,
              VehicleData.InsuranceType,
              Util.isNullOrEmpty(VehicleData.IsNew)
                ? 0
                : VehicleData.IsNew
                ? 1
                : 0,
              Util.isNullOrEmpty(FollowUpInfo.IsRenewal)
                ? 0
                : FollowUpInfo.IsRenewal
                ? 1
                : 0,
              !Util.isNullOrEmpty(FollowUpInfo.SalesOfficerID)
                ? FollowUpInfo.SalesOfficerID
                : JSON.parse(ACCOUNTDATA.UserInfo.User.SalesOfficerID)
            );
          }
        }
      );

      if (!Util.isNullOrEmpty(VehicleData.ProductCode)) {
        this.getSegmentCode(VehicleData.ProductCode);
        this.getEnableDisableSalesmanDealer(VehicleData.ProductCode);
      }

      if (!Util.isNullOrEmpty(VehicleData.DealerCode)) {
        // this.getDealerInformation(VehicleData.DealerCode);
        this.getSalesmanDealer(VehicleData.DealerCode);
      }
      if (!Util.isNullOrEmpty(VehicleData.SalesDealer)) {
        this.getDealerInformation(VehicleData.SalesDealer);
      }
    } else {
      this.state.isflagupdatetaskdetail = true;
      this.loadpremicalculationfirst = true;
    }

    if (!Util.isNullOrEmpty(SurveyData)) {
      this.setState({
        surveydatanocover: SurveyData.NoCover,
        surveydataoriginaldefect: SurveyData.OriginalDefect,
        surveydataaccessories: SurveyData.Accessories,
        surveydatarecommendation: SurveyData.SurveyorRecomendation,
        surveydataremarks: SurveyData.Remaks,
        surveydatasurveyor: SurveyData.Surveyor,
        surveydatastatus: SurveyData.SurveyStatus
      });
    }

    if (!Util.isNullOrEmpty(PoliciAddress)) {
      this.setState({
        policysentto: Util.valueTrim(PoliciAddress.SentTo),
        policyname: PoliciAddress.Name,
        datapolicyaddress: Util.stringArrayContains(
          Util.valueTrim(PoliciAddress.SentTo),
          ["BR", "GC"]
        )
          ? [
              {
                value: PoliciAddress.Name,
                label: PoliciAddress.Name
              }
            ]
          : [],
        policyaddress: PoliciAddress.Address,
        policykodepos: Util.valueTrim(PoliciAddress.PostalCode)
      });

      if (!Util.isNullOrEmpty(Util.valueTrim(PoliciAddress.SentTo))) {
        if (Util.valueTrim(PoliciAddress.SentTo) == "CS") {
          this.getNameOnPolicyDelivery(CustID);
        }

        if (Util.valueTrim(PoliciAddress.SentTo) == "BR") {
          // if (
          //   !Util.isNullOrEmpty(
          //     JSON.parse(ACCOUNTDATA).UserInfo.User
          //       .SalesOfficerID
          //   )
          // ) {
          //   this.getNameOnPolicyDelivery(
          //     JSON.parse(ACCOUNTDATA).UserInfo.User
          //       .SalesOfficerID
          //   );
          // }
          if (!Util.isNullOrEmpty(FollowUpInfo.SalesOfficerID)) {
            this.getNameOnPolicyDelivery(FollowUpInfo.SalesOfficerID);
          }
        }

        if (Util.valueTrim(PoliciAddress.SentTo) == "GC") {
          if (!Util.isNullOrEmpty(PoliciAddress.Address)) {
            let datapolicyaddress = [
              { value: PoliciAddress.Address, label: PoliciAddress.Address }
            ];
            this.setState({ datapolicyaddress });
          }
        }
      }
    }

    if (!Util.isNullOrEmpty(PaymentInfo)) {
      // console.log("PaymentInfo : ", PaymentInfo);
      this.setState(
        {
          paymentduedate: Util.isNullOrEmpty(PaymentInfo.DueDate)
            ? null
            : Util.formatDate(
                PaymentInfo.DueDate,
                "ddd, mmm dS, yyyy, hh:MM TT"
              ),
          paymentvapermata: PaymentInfo.VAPermata,
          paymentvamandiri: PaymentInfo.VAMandiri,
          paymentvabca: PaymentInfo.VABCA,
          paymentnorek: PaymentInfo.RekeningPenampung,
          paymentnova: PaymentInfo.VANo,
          // paymentisvaactive: PaymentInfo.IsVAActive == 1 ? true : false,
          dataBUKTIBAYAR: Util.isNullOrEmpty(PaymentInfo.BuktiBayarData)
            ? []
            : [
                Util.dataURItoBlob(
                  "data:image/" +
                    Util.getExtensionFile(PaymentInfo.BuktiBayar) +
                    ";base64," +
                    PaymentInfo.BuktiBayarData
                )
              ],
          BUKTIBAYAR: Util.isNullOrEmpty(PaymentInfo.BuktiBayar)
            ? ""
            : PaymentInfo.BuktiBayar,
          dataBUKTIBAYAR2: Util.isNullOrEmpty(PaymentInfo.BuktiBayar2Data)
            ? []
            : [
                Util.dataURItoBlob(
                  "data:image/" +
                    Util.getExtensionFile(PaymentInfo.BuktiBayar2) +
                    ";base64," +
                    PaymentInfo.BuktiBayar2Data
                )
              ],
          BUKTIBAYAR2: Util.isNullOrEmpty(PaymentInfo.BuktiBayar2)
            ? ""
            : PaymentInfo.BuktiBayar2,
          dataBUKTIBAYAR3: Util.isNullOrEmpty(PaymentInfo.BuktiBayar3Data)
            ? []
            : [
                Util.dataURItoBlob(
                  "data:image/" +
                    Util.getExtensionFile(PaymentInfo.BuktiBayar3) +
                    ";base64," +
                    PaymentInfo.BuktiBayar3Data
                )
              ],
          BUKTIBAYAR3: Util.isNullOrEmpty(PaymentInfo.BuktiBayar3)
            ? ""
            : PaymentInfo.BuktiBayar3,
          dataBUKTIBAYAR4: Util.isNullOrEmpty(PaymentInfo.BuktiBayar4Data)
            ? []
            : [
                Util.dataURItoBlob(
                  "data:image/" +
                    Util.getExtensionFile(PaymentInfo.BuktiBayar4) +
                    ";base64," +
                    PaymentInfo.BuktiBayar4Data
                )
              ],
          BUKTIBAYAR4: Util.isNullOrEmpty(PaymentInfo.BuktiBayar4)
            ? ""
            : PaymentInfo.BuktiBayar4,
          dataBUKTIBAYAR5: Util.isNullOrEmpty(PaymentInfo.BuktiBayar5Data)
            ? []
            : [
                Util.dataURItoBlob(
                  "data:image/" +
                    Util.getExtensionFile(PaymentInfo.BuktiBayar5) +
                    ";base64," +
                    PaymentInfo.BuktiBayar5Data
                )
              ],
          BUKTIBAYAR5: Util.isNullOrEmpty(PaymentInfo.BuktiBayar5)
            ? ""
            : PaymentInfo.BuktiBayar5
        },
        () => {
          // console.log("state paymentvapermata:", this.state.paymentvapermata);
          this.GetPaymentInfo();
        }
      );
    }

    if (!Util.isNullOrEmpty(SurveySchedule)) {
      this.setState(
        {
          surveyneed: SurveySchedule.IsNeedSurvey == 1 ? true : false,
          surveyneedtemp: SurveySchedule.IsNeedSurvey == 1 ? true : false,
          surveynsaskipsurvey:
            SurveySchedule.IsNSASkipSurvey == 1 ? true : false,
          surveychecklistmanual:
            SurveySchedule.IsManualSurvey == 1 ? true : false,
          surveykota: SurveySchedule.CityCode,
          surveylokasi: SurveySchedule.LocationCode,
          surveyalamat: SurveySchedule.SurveyAddress,
          surveykodepos: SurveySchedule.SurveyPostalCode,
          surveytanggal: !Util.isNullOrEmpty(SurveySchedule.SurveyDate)
            ? Util.convertDate(SurveySchedule.SurveyDate)
            : null,
          surveywaktu: SurveySchedule.ScheduleTimeID
        },
        () => {
          if (!Util.isNullOrEmpty(SurveySchedule.CityCode)) {
            if (!Util.isNullOrEmpty(SurveySchedule.LocationCode)) {
              this.getSurveyDaysBranch(SurveySchedule.LocationCode); // get survey days by Branch
              if (SurveySchedule.LocationCode == "Others") {
                this.getPostalCodeSurvey(SurveySchedule.CityCode);
              }
            }
          }
          if (!Util.isNullOrEmpty(SurveySchedule.SurveyDate)) {
            // this.getSurveyScheduleTime(Util.convertDate(SurveySchedule.SurveyDate));

            if (!Util.isNullOrEmpty(SurveySchedule.LocationCode)) {
              if (SurveySchedule.LocationCode == "Others") {
                if (SurveySchedule.SurveyPostalCode) {
                  // this.getSlotTimeBooking(
                  //   Util.convertDate(SurveySchedule.SurveyDate),
                  //   SurveySchedule.SurveyPostalCode,
                  //   SurveySchedule.CityCode
                  // );
                  this.setState({
                    datasurveywaktu: [
                      {
                        value: SurveySchedule.ScheduleTimeID,
                        label: SurveySchedule.ScheduleTimeDesc
                      }
                    ]
                  });
                }
              } else {
                // TODO LCY: check LocationCode = "Others"??
                // this.getSlotTimeBooking(
                //   Util.convertDate(SurveySchedule.SurveyDate),
                //   SurveySchedule.SurveyPostalCode,
                //   SurveySchedule.CityCode
                // );
                this.setState({
                  datasurveywaktu: [
                    {
                      value: SurveySchedule.ScheduleTimeID,
                      label: SurveySchedule.ScheduleTimeDesc
                    }
                  ]
                });
              }
            }
          }

          if (
            Util.isNullOrEmpty(FollowUpInfo.IsRenewal)
              ? false
              : FollowUpInfo.IsRenewal == 0
              ? false
              : true
          ) {
            if (!Util.isNullOrEmpty(VehicleData)) {
              if (VehicleData.IsORDefectsRepair == 1 ? true : false) {
                this.handleNeedSurveyChecklist();
              }

              if (VehicleData.IsAccessoriesChange == 1 ? true : false) {
                this.handleNeedSurveyChecklist();
              }
            }
          }
          if (!Util.isNullOrEmpty(VehicleData)) {
            if (
              !Util.isNullOrEmpty(VehicleData.Year) &&
              VehicleData.Year != Util.convertDate().getFullYear()
            ) {
              this.setState(
                {
                  vehicleusedcar: true
                },
                () => {
                  // this.handleNeedSurveyChecklist(); /// HANDLE PERUBAHAN REQUIREMENT LAGI
                }
              );
            }
          }

          if (this.state.surveynsaskipsurvey) {
            this.setState({
              surveyneed: false
            });
          }
          if (this.state.surveychecklistmanual) {
            this.setState({
              surveyneed: false
            });
          }
        }
      );

      if (!Util.isNullOrEmpty(SurveySchedule.CityCode)) {
        this.getSurveyLocation(SurveySchedule.CityCode);
        // this.getSurveyDays(SurveySchedule.CityCode);
      } else {
        this.loadSurveyLocation = true;
      }
    }

    if (!Util.isNullOrEmpty(PersonalDocument)) {
      this.setState({
        KTP: Util.isNullOrEmpty(PersonalDocument.IdentityCard)
          ? ""
          : PersonalDocument.IdentityCard,
        dataKTP: Util.isNullOrEmpty(PersonalDocument.IdentityCardData)
          ? []
          : [
              Util.dataURItoBlob(
                "data:image/" +
                  Util.getExtensionFile(PersonalDocument.IdentityCard) +
                  ";base64," +
                  PersonalDocument.IdentityCardData
              )
            ],
        BSTB: Util.isNullOrEmpty(PersonalDocument.BSTB)
          ? ""
          : PersonalDocument.BSTB,
        dataBSTB: Util.isNullOrEmpty(PersonalDocument.BSTBData)
          ? []
          : [
              Util.dataURItoBlob(
                "data:image/" +
                  Util.getExtensionFile(PersonalDocument.BSTB) +
                  ";base64," +
                  PersonalDocument.BSTBData
              )
            ],
        STNK: Util.isNullOrEmpty(PersonalDocument.STNK)
          ? ""
          : PersonalDocument.STNK,
        dataSTNK: Util.isNullOrEmpty(PersonalDocument.STNKData)
          ? []
          : [
              Util.dataURItoBlob(
                "data:image/" +
                  Util.getExtensionFile(PersonalDocument.STNK) +
                  ";base64," +
                  PersonalDocument.STNKData
              )
            ],
        DOCREP: Util.isNullOrEmpty(PersonalDocument.DocRep)
          ? ""
          : PersonalDocument.DocRep,
        dataDOCREP: Util.isNullOrEmpty(PersonalDocument.DocRepData)
          ? []
          : [
              Util.dataURItoBlob(
                "data:image/" +
                  Util.getExtensionFile(PersonalDocument.DocRep) +
                  ";base64," +
                  PersonalDocument.DocRepData
              )
            ],
        KONFIRMASICUST: Util.isNullOrEmpty(PersonalDocument.KonfirmasiCust)
          ? ""
          : PersonalDocument.KonfirmasiCust,
        dataKONFIRMASICUST: Util.isNullOrEmpty(
          PersonalDocument.KonfirmasiCustData
        )
          ? []
          : [
              Util.dataURItoBlob(
                "data:image/" +
                  Util.getExtensionFile(PersonalDocument.KonfirmasiCust) +
                  ";base64," +
                  PersonalDocument.KonfirmasiCustData
              )
            ]
      });
    }

    // if (!Util.isNullOrEmpty(Remarks)){
    this.setState({
      documentsremarksfromsa: Document.RemarkFromSA,
      documentsremarkstosa: Document.RemarkToSA
    });
    // }

    this.setState({
      dataDOCNSA1: Util.isNullOrEmpty(Document.DocNSA1Data)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.DocNSA1) +
                ";base64," +
                Document.DocNSA1Data
            )
          ],
      DOCNSA1: Util.isNullOrEmpty(Document.DocNSA1) ? "" : Document.DocNSA1,
      dataDOCNSA2: Util.isNullOrEmpty(Document.DocNSA2Data)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.DocNSA2) +
                ";base64," +
                Document.DocNSA2Data
            )
          ],
      DOCNSA2: Util.isNullOrEmpty(Document.DocNSA2) ? "" : Document.DocNSA2,
      dataDOCNSA3: Util.isNullOrEmpty(Document.DocNSA3Data)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.DocNSA3) +
                ";base64," +
                Document.DocNSA3Data
            )
          ],
      DOCNSA3: Util.isNullOrEmpty(Document.DocNSA3) ? "" : Document.DocNSA3,
      dataDOCNSA4: Util.isNullOrEmpty(Document.DocNSA4Data)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.DocNSA4) +
                ";base64," +
                Document.DocNSA4Data
            )
          ],
      DOCNSA4: Util.isNullOrEmpty(Document.DocNSA4) ? "" : Document.DocNSA4,
      dataDOCNSA5: Util.isNullOrEmpty(Document.DocNSA5Data)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.DocNSA5) +
                ";base64," +
                Document.DocNSA5Data
            )
          ],
      DOCNSA5: Util.isNullOrEmpty(Document.DocNSA5) ? "" : Document.DocNSA5,
      dataFAKTUR: Util.isNullOrEmpty(Document.FAKTURData)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.FAKTUR) +
                ";base64," +
                Document.FAKTURData
            )
          ],
      FAKTUR: Util.isNullOrEmpty(Document.FAKTUR) ? "" : Document.FAKTUR,
      dataSPPAKB: Util.isNullOrEmpty(Document.SPPAKBData)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(Document.SPPAKB) +
                ";base64," +
                Document.SPPAKBData
            )
          ],
      SPPAKB: Util.isNullOrEmpty(Document.SPPAKB) ? "" : Document.SPPAKB,
      dataNPWP: Util.isNullOrEmpty(CompanyDocument.NPWPData)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(CompanyDocument.NPWP) +
                ";base64," +
                CompanyDocument.NPWPData
            )
          ],
      NPWP: Util.isNullOrEmpty(CompanyDocument.NPWP)
        ? ""
        : CompanyDocument.NPWP,
      dataSIUP: Util.isNullOrEmpty(CompanyDocument.SIUPData)
        ? []
        : [
            Util.dataURItoBlob(
              "data:image/" +
                Util.getExtensionFile(CompanyDocument.SIUP) +
                ";base64," +
                CompanyDocument.SIUPData
            )
          ],
      SIUP: Util.isNullOrEmpty(CompanyDocument.SIUP) ? "" : CompanyDocument.SIUP
    });
  };

  getTaskDetail = (CustID, FollowUpNo) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/getTaskDetail`,
      "POST",
      HEADER_API,
      {
        CustID: CustID,
        FollowUpNo: FollowUpNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setStateGetTaskDetail(jsn.data, CustID);
        this.loadGetTaskDetail = true;
      })
      .catch(error => {
        Log.error("parsing failed", error);
        this.loadSurveyLocation = true;
        this.loadGetTaskDetail = true;
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
          // this.getTaskDetail(CustID, FollowUpNo);
        });
      });
  };

  getPostalCode = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetPostalCode`,
      "POST",
      HEADER_API,
      {
        search: ""
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.PostalCode}`,
          label: `${data.PostalCode + " - " + data.PostalDescription}`
        }))
      )
      .then(dataKodePos => {
        this.setState({ dataKodePos, isLoading: false });
        this.loadPostalCode = true;
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadPostalCode = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getPostalCode(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getPostalCodeSurvey = (SurveyCity, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyZipCode`,
      "POST",
      HEADER_API,
      {
        CityID: SurveyCity
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.ZipCode}`,
          label: `${data.ZipCode + " - " + data.ZipCodeDescription}`
        }))
      )
      .then(dataKodePosSurvey => {
        this.setState({ dataKodePosSurvey, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getPostalCodeSurvey(SurveyCity, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSurveyLocation = (idlocation, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyLocation`,
      "POST",
      HEADER_API,
      {
        pageType: "branch",
        type: 0,
        location: idlocation
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ datasurveylokasiall: jsn.data });
        let datas = jsn.data.map(data => ({
          value: `${data.ID}`,
          label: `${data.TNAME + " " + data.NAME + " " + data.ADDRESS}`
        }));
        if (jsn.spesific) {
          datas = [...datas, { value: "Others", label: "Spesific location" }];
        }
        return datas;
      })
      .then(datasurveylokasi => {
        this.loadSurveyLocation = true;
        this.setState({ datasurveylokasi, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadSurveyLocation = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSurveyLocation(idlocation, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSlotTimeBooking = (
    stateTanggal,
    stateKodePos = this.state.surveykodepos,
    stateCityIdSurvey = this.state.surveykota,
    stateBranchId = this.state.surveylokasi
  ) => {
    this.setState({ isLoading: true });

    // GETslotTimeBooking
    // param available date 5-aug-2019
    this.setState({ datasurveywaktuall: [], datasurveywaktu: [] }, () => {
      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/GetSlotTimeBooking`,
        "POST",
        HEADER_API,
        {
          BranchID: stateBranchId == "Others" ? "" : stateBranchId,
          ZipCode: stateKodePos,
          AvailableDate: Util.formatDate(stateTanggal, "dd-mmm-yyyy"),
          CityId: stateCityIdSurvey
        }
      )
        .then(res => res.json())
        .then(jsn => {
          this.setState({ datasurveywaktuall: jsn.data });
          let datas = jsn.data.map(data => ({
            value: `${data.ScheduleTime}`,
            label: `${data.ScheduleTimeTextID}`
          }));
          return datas;
        })
        .then(datasurveywaktu => {
          this.setState({ datasurveywaktu, isLoading: false });
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          // Util.hittingAPIError(error, () => {
          //   this.getSlotTimeBooking(
          //     stateTanggal,
          //     stateKodePos,
          //     stateCityIdSurvey,
          //     stateBranchId
          //   );
          // });
        });
    });
  };

  getSurveyScheduleTime = (
    stateTanggal,
    stateKodePos = this.state.surveykodepos,
    trycount = 0
  ) => {
    this.setState({ isLoading: true });

    // GETslotTimeBooking
    // param available date 5-aug-2019
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetScheduleTime`,
      "POST",
      HEADER_API,
      {
        BranchID: JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode,
        SurveyDate: Util.formatDate(stateTanggal, "yyyy-mm-dd hh:MM:ss"),
        PostalCodeID: stateKodePos,
        Language: "ID"
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ datasurveywaktuall: jsn.data });
        let datas = jsn.data.map(data => ({
          value: `${data.ScheduleTimeID}`,
          label: `${data.SurveyTime}`
        }));
        return datas;
      })
      .then(datasurveywaktu => {
        this.setState({ datasurveywaktu, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSurveyScheduleTime(stateTanggal, stateKodePos, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSurveyCity = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyCity`,
      "POST",
      HEADER_API,
      {
        pageType: "Branch",
        filterType: 0
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ datasurveykotaall: jsn.filterLocation });
        return jsn.filterLocation.map(data => ({
          value: `${data.ID}`,
          label: `${data.NAME}`
        }));
      })
      .then(datasurveykota => {
        this.loadSurveyCity = true;
        this.setState({ datasurveykota, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadSurveyCity = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSurveyCity(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSurveyDays = (CityID = this.state.surveykota, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyDays`,
      "POST",
      HEADER_API,
      {
        CityID /// BranchID
      }
    )
      .then(res => res.json())
      .then(data => {
        // console.log("survey date : ",data);
        if (!Util.isNullOrEmpty(data.data)) {
          if (data.data.length > 0) {
            data = data.data;
            this.setState({
              datasurveytanggal: data
            });
          }
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSurveyDays(CityID, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSurveyDaysBranch = (BranchID = this.state.surveylokasi, trycount = 0) => {
    this.setState({ isLoading: true });
    if (Util.stringEquals(BranchID, "Others")) {
      BranchID = 0;
    }
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSurveyDays`,
      "POST",
      HEADER_API,
      {
        BranchID /// BranchID
      }
    )
      .then(res => res.json())
      .then(data => {
        // console.log("survey date : ",data);
        if (!Util.isNullOrEmpty(data.data)) {
          if (data.data.length > 0) {
            data = data.data;
            this.setState({
              datasurveytanggal: data
            });
          }
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSurveyDaysBranch(BranchID, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  surveyDateExclude = date => {
    let temp = Util.stringArrayElementEquals(
      Util.formatDate(date, "dd-mm-yyyy"),
      this.state.datasurveytanggal
    );
    return temp;
  };

  getBasicCover = (
    isBasic = this.state.isBasicCoverageState,
    VehicleYear = this.state.vehicleYear,
    trycount = 0
  ) => {
    // if (this.state.FollowUpStatus == 13) {
    if (this.isShowBasicCover()) {
      isBasic = true;
      VehicleYear = "";
    }
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getBasicCover`,
      "POST",
      HEADER_API,
      {
        Channel: JSON.parse(ACCOUNTDATA).UserInfo.User.Channel,
        vYear: VehicleYear,
        isBasic: isBasic
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ databasiccoverall: jsn.BasicCover });
        return jsn.BasicCover.map(data => ({
          value: `${data.Id}`,
          label: `${data.Description}`
        }));
      })
      .then(databasiccover => {
        this.setState({ databasiccover, isLoading: false });
        this.loadBasicCover = true;
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadBasicCover = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getBasicCover(isBasic, VehicleYear, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getListUsage = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleUsage/`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => {
          return {
            value: `${data.insurance_code}`,
            label: `${Util.jsUcfirst(data.Description + "")}`
          };
        })
      )
      .then(datamapping => {
        this.setState({
          datavehicleusage: datamapping
        });
        this.setState({
          isLoading: false
        });
        this.loadListUsage = true;
      })
      .catch(error => {
        Log.error("parsing Failed", error);

        this.loadListUsage = true;
        this.setState({
          isLoading: false
        });

        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getListUsage(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getListProductType = (SalesOfficerID = this.state.SalesOfficerID) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getProductType/`,
      "POST",
      HEADER_API,
      {
        salesofficerid:
          SalesOfficerID || JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
        // "Channel=" +
        // JSON.parse(ACCOUNTDATA).UserInfo.User.Channel
      }
    )
      .then(res => res.json())
      .then(jsn => {
        Log.debugGroup(jsn);
        return jsn.data.map(data => ({
          value: `${data.InsuranceType}`,
          label: `${data.Description}`
        }));
      })
      .then(datamapping => {
        this.loadProductType = true;
        this.setState({
          datavehicleproducttype: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        this.loadProductType = true;
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  getListProductCode = (
    ProductTypeCode = "GARDAOTO",
    InsuranceType = 1,
    IsNew = 1,
    IsRenewal = 0,
    SalesOfficerID = this.state.SalesOfficerID ||
      (Util.isNullOrEmpty(accnt.UserInfo.User.SalesOfficerID)
        ? ""
        : accnt.UserInfo.User.SalesOfficerID),
    callback = () => {},
    trycount = 0
  ) => {
    this.setState({ isLoading: true });
    let accnt = JSON.parse(ACCOUNTDATA);
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getProductCode/`,
      "POST",
      HEADER_API,
      {
        producttypecode: ProductTypeCode,
        insurancetype: InsuranceType,
        isVehicleNew: IsNew,
        salesofficerid: SalesOfficerID,
        isRenewal: IsRenewal
        // Channel:
        //   Util.isNullOrEmpty(accnt.UserInfo.User.Channel)?"":accnt.UserInfo.User.Channel
        // "&ChannelSource=" +
        // JSON.parse(ACCOUNTDATA).UserInfo.User.ChannelSource
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datavehicleproductcodeall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.ProductCode}`,
          label: `${data.Description}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datavehicleproductcode: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            // this.getListProductCode();
            this.getListProductCode(
              ProductTypeCode,
              InsuranceType,
              IsNew,
              IsRenewal,
              SalesOfficerID,
              callback,
              trycount
            );
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSegmentCode = (productcode, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSegmentCode`,
      "POST",
      HEADER_API,
      {
        ProductCode: productcode
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ datavehiclesegmentcodeall: jsn.data });
        return jsn.data.map(data => ({
          value: `${data.SOB_ID}`,
          label: `${data.Name}`
        }));
      })
      .then(datavehiclesegmentcode => {
        this.setState({ datavehiclesegmentcode, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            // this.searchDataHitApi();
            this.getSegmentCode(productcode, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getEnableDisableSalesmanDealer = (productcode, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL +
        "" +
        API_VERSION_2}/DataReact/GetEnableDisableSalesmanDealer`,

      "POST",
      HEADER_API,
      {
        ProductCode: productcode,
        BranchCode: JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode
      }
    )
      .then(res => res.json())
      .then(data => {
        this.setState({ isLoading: false });
        if (data.status) {
          this.setState({
            IsSalesmanDealerEnable: data.IsSalesmanDealerEnable
          });
          if (!data.IsSalesmanDealerEnable) {
            this.setState({
              vehiclesalesman: 0,
              vehicledealer: 0,
              vehiclenamebank: null,
              vehiclenomorrek: null,
              vehiclesalesmanname: null
            });
          }
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getEnableDisableSalesmanDealer(productcode, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getRetrieveextsi = (interestId, callback = () => {}) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/retrieveextsi/`,
      "POST",
      HEADER_API,
      {
        InterestId: interestId
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // Log.debugGroup(jsn);
        return jsn.data.map(data => ({
          value: `${data}`,
          label: `${Util.formatMoney(data, 0)}`
        }));
      })
      .then(datamapping => {
        this.data = true;
        this.setState(
          {
            dataextsi: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        // this.loadProductType = true;
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  getVehicle = (search = "") => {
    this.abortController.abort();
    this.abortController = new AbortController();
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetVehicle`,
      "POST",
      HEADER_API,
      {
        moduleName: "QUOTATION",
        IsRenewal: this.state.IsRenewal ? 1 : 0,
        search
      },
      {
        signal: this.abortController.signal
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datavehicleall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.VehicleCode + "-" + data.Year}`,
          label: `${data.VehicleDescription}`
        }));
      })
      .then(datavehicle => {
        this.setState({ datavehicle, isLoading: false });
      })
      .catch(error => {
        if (error.name === "AbortError") {
          return; // Continuation logic has already been skipped, so return normally
        }
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.searchDataHitApi();
        });
      });
  };

  getUpliner = (SalesOfficerID = this.state.SalesOfficerID, trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getUpliner/`,
      "POST",
      HEADER_API,
      {
        SalesOfficerId:
          SalesOfficerID || JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.data.length > 0) {
          this.setState({
            vehicleupliner: jsn.data[0].UplinerName
          });
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({ isLoading: false });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getUpliner(SalesOfficerID, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getListRegion = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleRegion/`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.RegionCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.loadRegion = true;
        this.setState({
          datavehicleregion: datamapping
        });
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadRegion = true;
        this.setState({ isLoading: false });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getListRegion(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getListDealer = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getDealerName/`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.DealerCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.loadDealer = true;
        this.setState({
          datavehicledealer: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadDealer = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getListDealer(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getDealerInformation = (SalesmanCode, trycount = 0) => {
    this.setState({
      vehiclesalesmanname: "",
      vehiclenamebank: "",
      vehiclenomorrek: ""
    });

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetBankInformation/`,
      "POST",
      HEADER_API,
      {
        SalesmanCode: SalesmanCode
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        if (!Util.isNullOrEmpty(jsn.data)) {
          let data = jsn.data;
          this.setState({
            vehiclenamebank: data.BankName,
            vehiclenomorrek: data.AccountNo
            // vehiclesalesman: data.SalesmanCode,
            // vehiclesalesmanname: data.SalesDealerName
          });
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getDealerInformation(SalesmanCode, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getListBank = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetBank/`,
      "POST",
      HEADER_API,
      {
        ParamSearch: ""
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.Bank_Id}`,
          label: `${data.BankName}`
        }))
      )
      .then(datamapping => {
        this.loadBank = true;
        this.setState({
          datavehiclebank: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadBank = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getListBank(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getSalesmanDealer = (DealerCode, trycount = 0) => {
    // this.setState({ vehiclenamebank: null, vehiclenomorrek: null });

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getSalesmanDealerName/`,
      "POST",
      HEADER_API,
      {
        dealercode: DealerCode
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.SalesmanCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehiclesalesman: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getSalesmanDealer(DealerCode, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getPolicySentTo = (trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetPolicySentTo`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.ID}`,
          label: `${data.Description}`
        }))
      )
      .then(datapolicysentto => {
        this.loadPolicySentTo = true;
        this.setState({ datapolicysentto, isLoading: false });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.loadPolicySentTo = true;
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getPolicySentTo(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  getNameOnPolicyDelivery = (paramsearch = "", trycount = 0) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetPolicyDeliveryName/`,
      "POST",
      HEADER_API,
      {
        PolicySentToID: this.state.policysentto,
        ParamSearch: paramsearch
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datapolicyaddressall: jsn.data
        });

        if (jsn.data.length == 1) {
          let datamapping = jsn.data[0];
          // let policyname = !Util.isNullOrEmpty(this.state.policyname)
          //   ? this.state.policyname
          //   : datamapping.Name;
          let policyname = !Util.isNullOrEmpty(this.state.policyname)
            ? this.state.policyname
            : datamapping.Name;

          // if(Util.stringArrayContains(this.state.policysentto, ["BR", "GC"])){
          //   policyname = ""
          // } ///// MASALAH BESAR

          if (this.state.policysentto == "CS") {
            this.setState({
              policyname
            });
          }

          if (this.state.policysentto == "BR") {
            this.setState({
              // policyname : JSON.parse(ACCOUNTDATA).UserInfo.User.Name
              policyname: this.state.SalesOfficerName
            });
          }

          this.setState({
            policyaddress: !Util.isNullOrEmpty(this.state.policyaddress)
              ? this.state.policyaddress
              : datamapping.Address,
            policykodepos: !Util.isNullOrEmpty(this.state.policykodepos)
              ? this.state.policykodepos
              : Util.valueTrim(datamapping.PostalCode)
          });
        }
        return jsn.data.map(data => ({
          value: `${data.Name}`,
          label: `${data.Name}`
        }));
      })
      .then(datamapping => {
        this.setState({
          datapolicyaddress: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.getNameOnPolicyDelivery(paramsearch, trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  setDataFoto = (name, state, data) => {
    var datafototemp = [];
    datafototemp = [...this.state[`data${name}`]];

    if (state == "THUMBNAIL") {
      datafototemp[1] = data;
    } else if (state == "ORI") {
      datafototemp[0] = data;
    }

    if (datafototemp.length > 0) {
      this.setState({ [`data${name}`]: datafototemp }, () => {
        // Log.debugGroup("STATE MASUK", datafototemp);
      });
    }
  };

  handleUploadImage = (name, value, rejected) => {
    // this.setState({ isLoading: true });
    if (rejected.length > 0) {
      Util.showToast(
        "Please upload file with extension jpg, jpeg or png!!",
        "warning"
      );
      return;
    }

    if (
      this.state.FollowUpInfo == 53 ||
      this.state.FollowUpInfo == 54 ||
      this.state.FollowUpInfo == 55
    ) {
      this.setState({
        isLoading: true
      });
    }

    if (this.state.FollowUpStatus == 13) {
      this.setState({
        isLoading: true
      });
    }

    if (value.length > 0) {
      // Option compress image thumbnail
      var options = {
        maxSizeMB: 0.01, // MB
        useWebWorker: true
      };

      /// Option compress image ori yang diupload
      var optionsori = {
        maxSizeMB: 0.2, // MB
        // useWebWorker: true,
        useWebWorker: false
        // maxWidthOrHeight: 1024
      };

      // Log.debugGroup("FILE ORI", value[0]);

      this.setState({ [`data${name}`]: value });

      var compressOri = false;
      var compressThumbnail = true;

      imageCompression(value[0], optionsori)
        .then(function(compressedFile) {
          return compressedFile; // write your own logic
        })
        .then(compressImage => {
          // Log.debugGroup("ORI", compressImage);
          this.setDataFoto(name, "ORI", compressImage);
          compressOri = true;
        })
        .catch(function(error) {
          Log.debugGroup(error.message);
        });

      var checkcompressed = setInterval(() => {
        // Log.debugGroup("CHECK COMPRESS");

        if (compressOri && compressThumbnail) {
          /// HIT API UPLOAD IMAGE
          Log.debugGroup("SUDAH COMPRESS DONG!!!");
          clearInterval(checkcompressed);

          // function to convert image to base64 and upload image
          this.getBase64UploadImage(name);
        }
      }, 100);
    }
  };

  handleUploadFile = (name, value, rejected) => {
    // this.setState({ isLoading: true });
    if (rejected.length > 0) {
      Util.showToast(
        "Please upload file with extension pdf, jpg, jpeg or png!!",
        "warning"
      );
      return;
    }

    if (value.length > 0) {
      this.setState({ [`data${name}`]: value });
      Util.getBase64(value[0], data => {
        // console.log("hasil :", data)
        this.setState({
          [`type${name}`]: Util.base64MimeType(data),
          [`base64${name}`]: data
        }); // set mime type file
        // Util.base64MimeType(data);
        // Util.base64Only(data);
      });
    }
  };

  handleDeleteImage = name => {
    this.setState({
      [name]: "",
      [`data${name}`]: []
    });
  };

  handleDeleteFile = name => {
    this.setState({
      [name]: "",
      [`data${name}`]: [],
      [`type${name}`]: "",
      [`base64${name}`]: ""
    });
  };

  getBase64UploadImage = name => {
    // this.setState({ isLoading: true });
    var datafototemp = null;

    datafototemp = [...this.state[`data${name}`]];

    // FLAG CHECK PROCESS CONVERT BASE64
    var flagBase64Ori = false;
    var flagBase64Thumb = true;

    // var to save base64 converted
    var database64ori = null;
    var database64thumb = null;

    var reader = new FileReader();
    reader.readAsDataURL(datafototemp[0]);
    reader.onload = function() {
      database64ori = reader.result;
      flagBase64Ori = true;
    };
    reader.onerror = function(error) {
      Log.debugGroup("base64 ori Error: ", error);
    };

    // var readerthumb = new FileReader();
    // readerthumb.readAsDataURL(datafototemp[0]);
    // readerthumb.onload = function () {
    //   database64thumb = readerthumb.result;
    //   flagBase64Thumb = true;
    // };
    // readerthumb.onerror = function (error) {
    //   Log.debugGroup("base64 thumb Error: ", error);
    // };

    var checkconvertbase64 = setInterval(() => {
      if (flagBase64Ori && flagBase64Thumb) {
        Log.debugGroup("SUDAH CONVERT BASE64 DONG!!!");
        clearInterval(checkconvertbase64);
        var ext =
          "." +
          database64ori.substring(
            database64ori.indexOf("image/") + 6,
            database64ori.indexOf(";base64")
          );
        database64ori = database64ori.substring(
          database64ori.indexOf(",") + 1,
          database64ori.length
        );
        // database64thumb = database64thumb.substring(
        //   database64thumb.indexOf(",") + 1,
        //   database64thumb.length
        // );

        let base64toHEXtemp = this.base64toHEX(database64ori);

        this.uploadImage(
          base64toHEXtemp,
          "", // base64toHEXtemp,
          name,
          ext,
          this.state.SalesOfficerID ||
            JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID, ///
          this.generateDeviceUUID(),
          this.state.FollowUpNo
        );
      }
    }, 100);
  };

  uploadImage = (
    dataimageori,
    dataimagethumb,
    imageType,
    ext,
    SalesOfficerID,
    DeviceID,
    FollowUpNo
  ) => {
    this.setState({ isLoading: true });

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/UploadImage/`,
      "POST",
      HEADER_API,
      {
        ext: ext,
        ImageType: imageType,
        FollowUpNo: FollowUpNo || "",
        DeviceID: DeviceID,
        SalesOfficerID: SalesOfficerID,
        Data: dataimageori,
        ThumbnailData: dataimagethumb
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // Log.debugGroup(jsn);
        this.setState({
          FollowUpNumber: jsn.FollowUpNo,
          [imageType]: jsn.data,
          isLoading: false
        });
        if (imageType == "DOCREP") {
          this.setState({
            personalneeddocrep: imageType == "DOCREP" ? true : false
          });
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });

        // if (!error + "".toLowerCase().includes("token")) {
        setTimeout(() => {
          this.uploadImage(
            dataimageori,
            dataimagethumb,
            imageType,
            ext,
            SalesOfficerID,
            DeviceID,
            FollowUpNo
          );
        }, 2000);
        // });
      });
  };

  base64toHEX = base64 => {
    var raw = atob(base64);
    var HEX = "";
    var i = 0;
    for (i = 0; i < raw.length; i++) {
      var _hex = raw.charCodeAt(i).toString(16);
      HEX += _hex.length == 2 ? _hex : "0" + _hex;
    }
    return HEX.toUpperCase();
  };

  generateDeviceUUID = () => {
    var du = new DeviceUUID().parse();
    var dua = [
      du.language,
      du.platform,
      du.os,
      du.cpuCores,
      du.isAuthoritative,
      du.silkAccelerated,
      du.isKindleFire,
      du.isDesktop,
      du.isMobile,
      du.isTablet,
      du.isWindows,
      du.isLinux,
      du.isLinux64,
      du.isMac,
      du.isiPad,
      du.isiPhone,
      du.isiPod,
      du.isSmartTV,
      du.pixelDepth,
      du.isTouchScreen
    ];
    return du.hashMD5(dua.join(":"));
  };

  onChangeFunctionChecked = event => {
    let check = event.target.value == 1 ? 0 : 1;
    this.setState({
      [event.target.name]: check
    });
  };

  onChangeFunction2 = e => {
    // Log.debugGroup(event.target.type);
    const value =
      e.target[
        e.target.type === "checkbox"
          ? "checked"
          : e.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = e.target.name;

    this.setState(
      {
        [name]: value
      },
      () => {
        if (name == "IsACCESSChecked") {
          if (value) {
            this.setState({
              IsACCESSSIEnabled: 1
            });
          } else {
            this.setState(
              {
                IsACCESSSIEnabled: 0,
                ACCESSCOVER: 0,
                ACCESSPremi: 0
              },
              () => {
                // let CalculatedPremiItems = [...this.state.CalculatedPremiItems];
                // CalculatedPremiItems = CalculatedPremiItems.filter(
                //   data => data.InterestID != "ACCESS"
                // );

                // this.setState({ CalculatedPremiItems });
                this.rateCalculationNonBasicUnCheckAccesories();
              }
            );
          }
        }
      }
    );
  };

  onShowAlertModalInfo = MessageAlertCover => {
    this.setState({
      showModalInfo: true,
      MessageAlertCover
    });
  };

  onShowAlertModalInfoTryAPI = (
    MessageAlertCover = "Something wrong with server!. Refresh this page or please contacting IT Development!. Are you want to refresh page?"
  ) => {
    this.setState({
      showModalInfoTryAPI: true,
      MessageAlertCover
    });
  };

  onShowAlertModalInfoIsPaid = (
    MessageAlertCover = "This order is already paid"
  ) => {
    this.setState({
      showModalInfoIsPaid: true,
      MessageAlertCover
    });
  };

  checkNSAapproval = (
    callbackTrueNSA = remarks => {},
    callbackFalseNSA = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/CheckNSA/`,
      "POST",
      {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      {
        OrderNo: this.state.selectedQuotation
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        if (jsn.status) {
          if (jsn.data[0].IsNSA == "True") {
            /// KENA NSA
            callbackTrueNSA(jsn.data[0].Remarks);
          } else if (jsn.data[0].IsNSA == "False") {
            /// LOLOS NSA
            callbackFalseNSA();
          }
        }
        // this.setState({
        //  MessageAlertCover : jsn.data[0].Remaks,
        //  nsaResult: jsn.data[0].IsNSA
        // }, () => {
        // if(jsn.data[0].IsNSA == "True"){
        // KENA NSA
        // this.state.isErrorSave = true;
        // this.setState({
        //   statusinfodetail: "46",
        //   FUStatusInfo: "3"
        // }, () => {
        //   // this.save(false);
        //   // this.onShowAlertModalInfo("Order ini terdeteksi memerlukan NSA, harap melakukan approval NSA terlebih dahulu dan menyertakan dokumen pendukung.");
        // });

        // }else if(jsn.data[0].IsNSA == "False"){
        // this.state.isErrorSave = false;
        // this.save(false);

        // FALSE NSA
        // }
        // });
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  trigerRateCalculation = () => {
    if (Util.isNullOrEmpty(this.state.vehicleproductcode)) {
      Log.debugStr("error vehicleproductcode");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Product code is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehiclebasiccoverage)) {
      Log.debugStr("error vehiclebasiccoverage");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Basic coverage is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicletotalsuminsured)) {
      Log.debugStr("error vehicletotalsuminsured");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Sum Insured is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleProductTypeCode)) {
      Log.debugStr("error vehicleProductTypeCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleBrandCode)) {
      Log.debugStr("error vehicleBrandCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehiclevehiclecode)) {
      Log.debugStr("error vehiclevehiclecode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehiclevehiclecodeyear)) {
      Log.debugStr("error vehiclevehiclecodeyear");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleModelCode)) {
      Log.debugStr("error vehicleModelCode");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleType)) {
      Log.debugStr("error vehicleType");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleSeries)) {
      Log.debugStr("error vehicleSeries");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleYear)) {
      Log.debugStr("error vehicleYear");
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleusage)) {
      Log.debugStr("error vehicleusage");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Vehicle usage is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }
    if (Util.isNullOrEmpty(this.state.vehicleregion)) {
      Log.debugStr("error vehicleregion");
      // toast.dismiss();
      // toast.warning(
      //   "❗ Vehicle region is required for calculating rate calculation",
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      return;
    }

    Log.debugStr("Hit ratecalculation from triggerratecalculation");
    // this.rateCalculate(false);
    this.basicPremiCalculation();
  };

  loadPremiumCalculation = (
    OrderNo = "",
    IsRenewal = this.state.IsRenewal,
    OldPolicyNo = this.state.OldPolicyNo
  ) => {
    Log.debugGroup("hit PremiCalculation");

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.vehiclebasiccoverage
    )[0];
    Log.debugGroup("basiccover : ", BasicCover);

    if (!Util.isNullOrEmpty(BasicCover)) {
      var IsNewTemp = !this.state.vehicleusedcar ? 1 : 0;

      this.setState({ isLoading: true });

      let paramAdd = Util.isNullOrEmpty(OrderNo) ? {} : { OrderNo: OrderNo };

      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/PremiumCalculation/`,
        "POST",
        HEADER_API,
        {
          ComprePeriod: BasicCover.ComprePeriod,
          TLOPeriod: BasicCover.TLOPeriod,
          PeriodFrom: Util.formatDate(
            this.state.vehicleperiodfrom,
            "yyyy-mm-dd"
          ),
          PeriodTo: Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd"),
          vtype: this.state.vehicleType,
          vcitycode: this.state.vehicleregion,
          vusagecode: this.state.vehicleusage,
          vyear: this.state.vehicleYear,
          vsitting: this.state.vehicleSitting,
          vProductCode: this.state.vehicleproductcode,
          vTSInterest: this.state.vehicletotalsuminsuredtemp,
          vPrimarySI: this.state.vehicletotalsuminsured,
          vCoverageId: BasicCover.CoverageId,
          // vInterestId:
          // "ALLRIK" , /// Pasti ALLRIK ????
          pQuotationNo: "",
          Ndays: this.state.Ndays,
          vBrand: this.state.vehicleBrandCode,
          vModel: this.state.vehicleModelCode,
          isNew: IsNewTemp,
          OldPolicyNo: IsRenewal ? OldPolicyNo : "",
          ...paramAdd
        }
      )
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
              this.state.isflagupdatetaskdetail = true;
              this.loadpremicalculationfirst = true;
            } else {
              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PADRVR"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PADRVR",
                    "PADDR1"
                  ])
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PAPASS"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PAPASS",
                    "PA24AV"
                  ])
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "ACCESS"
                data => Util.stringArrayContains(data.InterestID, ["ACCESS"])
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "TPLPER"
                data => Util.stringArrayContains(data.InterestID, ["TPLPER"])
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured,
                  PASSCOVER: this.state.vehicleSitting - 1
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0
                  // PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID,
                  TPLSICOVER: TPLCoverageId[0].SumInsured
                });
              } else {
                this.setState({
                  TPLCoverageId: null,
                  TPLSICOVER: 0
                });
              }
              // }

              this.setState(
                {
                  ACCESSPremi: jsn.ACCESSPremi,
                  AdminFee: jsn.AdminFee,
                  Alert: jsn.Alert,
                  CalculatedPremiItems: jsn.CalculatedPremiItems,
                  ETVPremi: jsn.ETVPremi,
                  FLDPremi: jsn.FLDPremi,
                  IsACCESSChecked: jsn.IsACCESSChecked,
                  IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                  IsACCESSEnabled: jsn.IsACCESSEnabled,
                  IsETVChecked: jsn.IsETVChecked,
                  IsETVEnabled: jsn.IsETVEnabled,
                  IsFLDChecked: jsn.IsFLDChecked,
                  IsFLDEnabled: jsn.IsFLDEnabled,
                  IsPADRVRChecked: jsn.IsPADRVRChecked,
                  IsPADRVREnabled: jsn.IsPADRVREnabled,
                  IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                  IsPAPASSChecked: jsn.IsPAPASSChecked,
                  IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                  IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                  IsPASSEnabled: jsn.IsPASSEnabled,
                  IsSRCCChecked: jsn.IsSRCCChecked,
                  IsSRCCEnabled: jsn.IsSRCCEnabled,
                  IsTPLChecked: jsn.IsTPLChecked,
                  IsTPLEnabled: jsn.IsTPLEnabled,
                  IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                  IsTSChecked: jsn.IsTSChecked,
                  IsTSEnabled: jsn.IsTSEnabled,
                  PADRVRPremi: jsn.PADRVRPremi,
                  PAPASSPremi: jsn.PAPASSPremi,
                  SRCCPremi: jsn.SRCCPremi,
                  TPLPremi: jsn.TPLPremi,
                  TSPremi: jsn.TSPremi,
                  TotalPremi: jsn.TotalPremi,
                  coveragePeriodItems: jsn.coveragePeriodItems,
                  coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
                },
                () => {
                  this.state.isflagupdatetaskdetail = true; /// MAKE SURE CALCULATEPREMIITEM LATEST
                  this.loadpremicalculationfirst = true;
                }
              );

              // SET TEMP LAST
              if (this.state.IsRenewal) {
                this.setState({
                  ACCESSPremitemp: jsn.ACCESSPremi,
                  AdminFeetemp: jsn.AdminFee,
                  Alerttemp: jsn.Alert,
                  CalculatedPremiItemstemp: jsn.CalculatedPremiItems,
                  ETVPremitemp: jsn.ETVPremi,
                  FLDPremitemp: jsn.FLDPremi,
                  IsACCESSCheckedtemp: jsn.IsACCESSChecked,
                  IsACCESSSIEnabledtemp: jsn.IsACCESSSIEnabled,
                  IsACCESSEnabledtemp: jsn.IsACCESSEnabled,
                  IsETVCheckedtemp: jsn.IsETVChecked,
                  IsETVEnabledtemp: jsn.IsETVEnabled,
                  IsFLDCheckedtemp: jsn.IsFLDChecked,
                  IsFLDEnabledtemp: jsn.IsFLDEnabled,
                  IsPADRVRCheckedtemp: jsn.IsPADRVRChecked,
                  IsPADRVREnabledtemp: jsn.IsPADRVREnabled,
                  IsPADRVRSIEnabledtemp: jsn.IsPADRVRSIEnabled,
                  IsPAPASSCheckedtemp: jsn.IsPAPASSChecked,
                  IsPAPASSEnabledtemp: jsn.IsPAPASSEnabled,
                  IsPAPASSSIEnabledtemp: jsn.IsPAPASSSIEnabled,
                  IsPASSEnabledtemp: jsn.IsPASSEnabled,
                  IsSRCCCheckedtemp: jsn.IsSRCCChecked,
                  IsSRCCEnabledtemp: jsn.IsSRCCEnabled,
                  IsTPLCheckedtemp: jsn.IsTPLChecked,
                  IsTPLEnabledtemp: jsn.IsTPLEnabled,
                  IsTPLSIEnabledtemp: jsn.IsTPLSIEnabled,
                  IsTSCheckedtemp: jsn.IsTSChecked,
                  IsTSEnabledtemp: jsn.IsTSEnabled,
                  PADRVRPremitemp: jsn.PADRVRPremi,
                  PAPASSPremitemp: jsn.PAPASSPremi,
                  SRCCPremitemp: jsn.SRCCPremi,
                  TPLPremitemp: jsn.TPLPremi,
                  TSPremitemp: jsn.TSPremi,
                  TotalPremitemp: jsn.TotalPremi,
                  coveragePeriodItemstemp: jsn.coveragePeriodItems,
                  coveragePeriodNonBasicItemstemp:
                    jsn.coveragePeriodNonBasicItems
                });
              }

              this.setState({
                // GrossPremium: jsn.TotalPremi - jsn.AdminFee,
                GrossPremium: jsn.GrossPremi,
                Admin: jsn.AdminFee,
                NetPremi: jsn.TotalPremi,
                NoClaimBonus: jsn.NoClaimBonus,
                DiscountPremi: jsn.DiscountPremi
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.vehicleproductcode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null, TPLSICOVER: 0 });
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ Calculate premi error", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
            this.state.isflagupdatetaskdetail = true;
            this.loadpremicalculationfirst = true;
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          this.state.isflagupdatetaskdetail = true;
          this.loadpremicalculationfirst = true;
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Calculate premi error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          Util.hittingAPIError(error, () => {
            this.loadPremiumCalculation(OrderNo, IsRenewal, OldPolicyNo);
          });
        });
    } else {
      this.state.isflagupdatetaskdetail = true;
      this.loadpremicalculationfirst = true;
      setTimeout(() => {
        Log.debugGroup("Hit timeout : premiumcalculation");
        this.loadPremiumCalculation(OrderNo, IsRenewal, OldPolicyNo);
      }, 2000);
    }
  };

  premiumCalculation = () => {
    Log.debugGroup("hit extended premium simulation");

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.vehiclebasiccoverage
    )[0];
    Log.debugGroup("basiccover : ", BasicCover);

    if (!Util.isNullOrEmpty(BasicCover)) {
      let IsNewTemp = !this.state.vehicleusedcar ? 1 : 0;

      let vTSI = this.state.vehicletotalsuminsuredtemp;
      let vsitting = this.state.vehicleSitting;
      let TPLCoverageId = "";
      let vType = this.state.vehicleType;
      let vPrimarySI = this.state.vehicletotalsuminsured;

      if (this.state.chItemTemp == "PAPASS") {
        vsitting = parseInt(this.state.PASSCOVER) + 1;
        vTSI = this.state.PAPASSICOVERtemp;

        if (
          Util.isNullOrEmpty(this.state.PAPASSICOVERtemp) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Passenger is mandatory", "warning");
          return;
        }

        if (Util.isNullOrEmpty(this.state.PASSCOVER)) {
          Util.showToast("Pa Passenger Number is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "PADRVR") {
        vTSI = this.state.PADRVCOVERtemp;

        if (
          Util.isNullOrEmpty(this.state.PADRVCOVERtemp) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Driver is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "ACCESS") {
        vTSI = this.state.ACCESSCOVERtemp;
        vPrimarySI = this.state.vehicletotalsuminsuredtemp;
        if (
          Util.isNullOrEmpty(this.state.ACCESSCOVERtemp) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Accessories is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "TPLPER") {
        TPLCoverageId = this.state.TPLCoverageIdtemp;
        vTSI = this.state.TPLSICOVERtemp;
        if (
          Util.isNullOrEmpty(this.state.TPLSICOVERtemp) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI TPL is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "SRCC") {
        vType = "";
      } else if (this.state.chItemTemp == "ETV") {
        vType = "";
      } else if (this.state.chItemTemp == "FLD") {
        vType = "";
      }

      if (Util.isNullOrEmpty(vTSI)) {
        vTSI = 0;
      }

      // if(Util.isNullOrEmpty(vTSI)){
      //   Util.showToast("Sum Insured is mandatory", "warning");
      //   return;
      // }

      this.setState({
        isLoading: true
      });

      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/PremiumCalculation/`,
        "POST",
        HEADER_API,
        {
          ComprePeriod: BasicCover.ComprePeriod, /// CATATAN
          TLOPeriod: BasicCover.TLOPeriod, /// CATATAN
          PeriodFrom: Util.isNullOrEmpty(
            this.state.vehicleperiodfrommodalperiod
          )
            ? null
            : Util.formatDate(
                this.state.vehicleperiodfrommodalperiod,
                "yyyy-mm-dd"
              ),
          PeriodTo: Util.isNullOrEmpty(this.state.vehicleperiodtomodalperiod)
            ? null
            : Util.formatDate(
                this.state.vehicleperiodtomodalperiod,
                "yyyy-mm-dd"
              ),
          vtype: vType,
          vcitycode: this.state.vehicleregion,
          vusagecode: this.state.vehicleusage,
          vyear: this.state.vehicleYear,
          vsitting: vsitting,
          vProductCode: this.state.vehicleproductcode,
          vTSInterest: vTSI,
          vPrimarySI: vPrimarySI,
          vCoverageId: BasicCover.CoverageId,
          pQuotationNo: "",
          Ndays: this.state.Ndays,
          vBrand: this.state.vehicleBrandCode,
          vModel: this.state.vehicleModelCode,
          isNew: IsNewTemp,
          chItem: this.state.chItemTemp,
          CalculatedPremiItems: JSON.stringify(this.state.CalculatedPremiItems),
          TPLCoverageId: TPLCoverageId,
          OldPolicyNo: this.state.IsRenewal ? this.state.OldPolicyNo : ""
        }
      )
        .then(res => res.json())
        .then(jsn => {
          this.setState({ showdialogperiod: false });
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PADRVR"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PADRVR",
                    "PADDR1"
                  ])
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PAPASS"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PAPASS",
                    "PA24AV"
                  ])
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "ACCESS"
                data => Util.stringArrayContains(data.InterestID, ["ACCESS"])
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "TPLPER"
                data => Util.stringArrayContains(data.InterestID, ["TPLPER"])
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured,
                  PASSCOVER: this.state.vehicleSitting - 1
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0
                  // PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID,
                  TPLSICOVER: TPLCoverageId[0].SumInsured
                });
              } else {
                this.setState({
                  TPLCoverageId: null,
                  TPLSICOVER: 0
                });
              }

              this.setState(
                {
                  ACCESSPremi: jsn.ACCESSPremi,
                  AdminFee: jsn.AdminFee,
                  Alert: jsn.Alert,
                  CalculatedPremiItems: jsn.CalculatedPremiItems,
                  ETVPremi: jsn.ETVPremi,
                  FLDPremi: jsn.FLDPremi,
                  IsACCESSChecked: jsn.IsACCESSChecked,
                  IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                  IsACCESSEnabled: jsn.IsACCESSEnabled,
                  IsETVChecked: jsn.IsETVChecked,
                  IsETVEnabled: jsn.IsETVEnabled,
                  IsFLDChecked: jsn.IsFLDChecked,
                  IsFLDEnabled: jsn.IsFLDEnabled,
                  IsPADRVRChecked: jsn.IsPADRVRChecked,
                  IsPADRVREnabled: jsn.IsPADRVREnabled,
                  IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                  IsPAPASSChecked: jsn.IsPAPASSChecked,
                  IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                  IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                  IsPASSEnabled: jsn.IsPASSEnabled,
                  IsSRCCChecked: jsn.IsSRCCChecked,
                  IsSRCCEnabled: jsn.IsSRCCEnabled,
                  IsTPLChecked: jsn.IsTPLChecked,
                  IsTPLEnabled: jsn.IsTPLEnabled,
                  IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                  IsTSChecked: jsn.IsTSChecked,
                  IsTSEnabled: jsn.IsTSEnabled,
                  PADRVRPremi: jsn.PADRVRPremi,
                  PAPASSPremi: jsn.PAPASSPremi,
                  SRCCPremi: jsn.SRCCPremi,
                  TPLPremi: jsn.TPLPremi,
                  TSPremi: jsn.TSPremi,
                  TotalPremi: jsn.TotalPremi,
                  coveragePeriodItems: jsn.coveragePeriodItems,
                  coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
                },
                () => {
                  if (this.state.IsRenewal) {
                    // Handle check need survey (PENAMBAHAN LIMIT PERLUASAN/PERUBAHAN || HAPUS / PENGURANGAN PERLUASAN)
                    if (this.needSurveyChangeExtendedCoverRenewal()) {
                      this.handleNeedSurveyChecklist();
                    } else {
                      this.handleNeedSurveyUnChecklist();
                    }
                  }
                }
              );

              this.setState({
                // GrossPremium: jsn.TotalPremi - jsn.AdminFee,
                GrossPremium: jsn.GrossPremi,
                Admin: jsn.AdminFee,
                NetPremi: jsn.TotalPremi,
                NoClaimBonus: jsn.NoClaimBonus,
                DiscountPremi: jsn.DiscountPremi
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.vehicleproductcode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null, TPLSICOVER: 0 });
              }

              if (jsn.IsACCESSChecked == 0) {
                this.setState({ ACCESSCOVER: 0 });
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ Calculate premi error", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({ showdialogperiod: false });
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Calculate premi error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          Util.hittingAPIError(error, () => {
            // this.premiumCalculation();
          });
        });
    }
  };

  basicPremiCalculation = () => {
    Log.debugGroup("hit basicPremiCalculation");

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.vehiclebasiccoverage
    )[0];

    if (!Util.isNullOrEmpty(BasicCover)) {
      this.abortControllerBasicPremium.abort();
      this.abortControllerBasicPremium = new AbortController();
      var IsNewTemp = !this.state.vehicleusedcar ? 1 : 0;

      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/BasicPremiCalculation/`,
        "POST",
        HEADER_API,
        {
          ComprePeriod: BasicCover.ComprePeriod,
          TLOPeriod: BasicCover.TLOPeriod,
          PeriodFrom: Util.formatDate(
            this.state.vehicleperiodfrom,
            "yyyy-mm-dd"
          ),
          PeriodTo: Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd"),
          vtype: this.state.vehicleType,
          vcitycode: this.state.vehicleregion,
          vusagecode: this.state.vehicleusage,
          vyear: this.state.vehicleYear,
          vsitting: this.state.vehicleSitting,
          vProductCode: this.state.vehicleproductcode,
          vTSInterest: this.state.vehicletotalsuminsuredtemp,
          vPrimarySI: this.state.vehicletotalsuminsured,
          vCoverageId: BasicCover.CoverageId,
          // vInterestId:
          // "ALLRIK", /// Pasti ALLRIK ????
          pQuotationNo: "",
          Ndays: this.state.Ndays,
          vBrand: this.state.vehicleBrandCode,
          vModel: this.state.vehicleModelCode,
          isNew: IsNewTemp,
          OldPolicyNo: this.state.IsRenewal ? this.state.OldPolicyNo : ""
        },
        {
          signal: this.abortControllerBasicPremium.signal
        }
      )
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PADRVR"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PADRVR",
                    "PADDR1"
                  ])
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PAPASS"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PAPASS",
                    "PA24AV"
                  ])
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "ACCESS"
                data => Util.stringArrayContains(data.InterestID, ["ACCESS"])
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "TPLPER"
                data => Util.stringArrayContains(data.InterestID, ["TPLPER"])
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured,
                  PASSCOVER: this.state.vehicleSitting - 1
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0
                  // PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID,
                  TPLSICOVER: TPLCoverageId[0].SumInsured
                });
              } else {
                this.setState({
                  TPLCoverageId: null,
                  TPLSICOVER: 0
                });
              }

              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                IsACCESSChecked: jsn.IsACCESSChecked,
                IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                IsPADRVREnabled: jsn.IsPADRVREnabled,
                IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                IsTPLEnabled: jsn.IsTPLEnabled,
                IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              // handle data renewal change from TLO to COMPRE need survey
              if (this.state.IsRenewal) {
                if (
                  this.state.IsTPLEnabledtemp == 0 &&
                  this.state.IsTPLEnabled == 1
                ) {
                  this.handleNeedSurveyChecklist();
                }
              }

              this.setState({
                // GrossPremium: jsn.TotalPremi - jsn.AdminFee,
                GrossPremium: jsn.GrossPremi,
                Admin: jsn.AdminFee,
                NetPremi: jsn.TotalPremi,
                NoClaimBonus: jsn.NoClaimBonus,
                DiscountPremi: jsn.DiscountPremi
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.vehicleproductcode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null, TPLSICOVER: 0 });
              }

              if (jsn.IsACCESSChecked == 0) {
                this.setState({ ACCESSCOVER: 0 });
              }
            }
          } else {
            if (jsn.data == "null") {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              toast.dismiss();
              toast.warning("❗ Calculate premi error", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
              });
            }
          }
          this.setState({
            isLoading: false
          });
          this.loadpremicalculationfirst = true;
        })
        .catch(error => {
          this.loadpremicalculationfirst = true;
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          if (!Util.isNotErrorLoopingHittingAPI(error)) {
            toast.dismiss();
            toast.warning("❗ Calculate premi error - " + error, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          //  if(!(error+"").toLowerCase().includes("token")){
          //     this.basicPremiCalculation();
          //   }
        });
    } else {
      setTimeout(() => {
        Log.debugGroup("Hit timeout : basicPremiCalculation");
        this.basicPremiCalculation();
      }, 2000);
    }
  };

  // DIALOG CLICK COVERAGE NON BASIC
  setDialogPeriod = chType => {
    var chItemSetPeriod = false;
    var chItemSetPeriodEnable = 0;

    var chItemTemp = -1;

    this.setState({ chItemTemp }, () => {
      if (chType == 0) {
        chItemTemp = "SRCC";
        if (this.state.IsSRCCCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 1) {
        chItemTemp = "FLD";
        if (this.state.IsFLDCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 2) {
        chItemTemp = "ETV";
        if (this.state.IsETVCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 3) {
        chItemTemp = "TS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
      } else if (chType == 4) {
        chItemTemp = "TPLPER"; // TPLPER
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
        this.getTPLSI(this.state.vehicleproductcode, () => {
          let TPLCoverageIdtemp = "";
          if (
            Util.stringArrayContains(
              this.state.TPLCoverageId + "",
              Util.convertArrayObjectToLinearArray(
                this.state.dataTPLSI,
                "value"
              )
            )
          ) {
            TPLCoverageIdtemp = this.state.TPLCoverageId;
          }
          this.setState({ TPLCoverageIdtemp });
        });
      } else if (chType == 5) {
        chItemTemp = "PADRVR";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;

        this.getRetrieveextsi(chItemTemp, () => {
          let PADRVCOVERtemp = this.state.PADRVCOVER;
          if (
            Util.stringArrayContains(
              this.state.PADRVCOVER + "",
              Util.convertArrayObjectToLinearArray(
                this.state.dataextsi,
                "value"
              )
            )
          ) {
            PADRVCOVERtemp = this.state.PADRVCOVER;
          }
          this.setState({ PADRVCOVERtemp });
        });
      } else if (chType == 6) {
        chItemTemp = "PAPASS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;

        this.getRetrieveextsi(chItemTemp, () => {
          let PAPASSICOVERtemp = this.state.PAPASSICOVER;
          if (
            Util.stringArrayContains(
              this.state.PAPASSICOVER + "",
              Util.convertArrayObjectToLinearArray(
                this.state.dataextsi,
                "value"
              )
            )
          ) {
            PAPASSICOVERtemp = this.state.PAPASSICOVER;
          }
          this.setState({ PAPASSICOVERtemp });
        });

        // HANDLE PASS
        this.setState({
          PASSCOVER: this.state.vehicleSitting - 1
        });
      } else if (chType == 7) {
        chItemTemp = "ACCESS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
        let ACCESSCOVERtemp = null;
        if (
          this.state.ACCESSCOVER != 0 ||
          !Util.isNullOrEmpty(this.state.ACCESSCOVER)
        ) {
          ACCESSCOVERtemp = this.state.ACCESSCOVER;
        }
        this.setState({
          ACCESSCOVERtemp
        });
      } // ACCESS
      this.setState({ chItemTemp, chItemSetPeriod, chItemSetPeriodEnable });

      this.setState(
        {
          vehicleperiodfrommodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodfrom,
          vehicleperiodtomodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodto
        },
        () => {
          this.setState({ showdialogperiod: true });

          if (chItemTemp == "PADRVR") {
            this.setPeriodToPeriodFromExtendedCover("PAD1", "COVERAGE", () => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTemp,
                vehicleperiodfrommodalperiod: this.state.periodfromTemp
              });
            });
          }

          if (chItemTemp == "TPLPER") {
            this.setPeriodToPeriodFromExtendedCover(
              "TPLPER",
              "INTEREST",
              () => {
                this.setState({
                  vehicleperiodtomodalperiod: this.state.periodtoTemp,
                  vehicleperiodfrommodalperiod: this.state.periodfromTemp
                });
              }
            );
          }

          if (chItemTemp == "PAPASS") {
            this.setPeriodToPeriodFromExtendedCover("PAP1", "COVERAGE", () => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTemp,
                vehicleperiodfrommodalperiod: this.state.periodfromTemp
              });
            });
          }

          //// SRCC ETV FLD TS BELUM KE HANDLE LOAD PERIOD FROM PERIOD TO

          // if (chItemTemp == "TPLPER") {
          //   if (this.detectComprePeriod()) {
          //     let basicCoverage = [...this.state.databasiccoverall].filter(
          //       data => data.Id == this.state.vehiclebasiccoverage
          //     )[0];

          //     this.setState(
          //       {
          //         vehicleperiodtomodalperiod: Util.convertDate(
          //           Util.formatDate(this.state.vehicleperiodfrom)
          //         ).setFullYear(
          //           Util.convertDate(
          //             Util.formatDate(this.state.vehicleperiodfrom)
          //           ).getFullYear() + basicCoverage.ComprePeriod
          //         )
          //       },
          //       () => {
          //         if (
          //           basicCoverage.TLOPeriod == 0 &&
          //           basicCoverage.ComprePeriod == 0
          //         ) {
          //           this.setState({
          //             vehicleperiodtomodalperiod: this.state.vehicleperiodto
          //           });
          //         }
          //       }
          //     );
          //   }
          // }

          if (chItemTemp == "TS") {
            this.setPeriodToPeriodFromTS(() => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTS,
                vehicleperiodfrommodalperiod: this.state.periodfromTS
              });
            });
          }
        }
      );
    });
  };

  // Show list accessories
  ShowAccessoryInfo = (trycount = 0) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getAccessoriesList`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      })
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          label: `${data.Description}`,
          value: `${data.MaxSI}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            dataAccessories: datamapping,
            isLoading: false,
            showModalAccessories: true
          },
          () => {
            this.props.history.push(this.props.location.pathname + "#");
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        trycount += 1;
        Util.hittingAPIError(
          error,
          () => {
            this.ShowAccessoryInfo(trycount);
          },
          trycount,
          () => {
            this.onShowAlertModalInfoTryAPI();
          }
        );
      });
  };

  // close modal list accessories
  onCloseModalAccessories = () => {
    this.props.history.goBack();
    this.setState({ showModalAccessories: false });
  };

  // Remove CalculatePremiItems
  removeCalculatePremiItems = (
    data = [
      {
        Name: "",
        Type: "" // INTEREST || COVERAGE
      }
    ],
    CalculatedPremiItems = this.state.CalculatedPremiItems
  ) => {
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
      if (element.Type.toUpperCase().includes("COVERAGE")) {
        CalculatedPremiItems = [...CalculatedPremiItems].filter(
          data =>
            !data.CoverageID.toLowerCase().includes(
              element.Name.toLowerCase().trim()
            )
        );
      } else if (element.Type.toUpperCase().includes("INTEREST")) {
        CalculatedPremiItems = [...CalculatedPremiItems].filter(
          data =>
            !data.InterestID.toLowerCase().includes(
              element.Name.toLowerCase().trim()
            )
        );
      }
      Log.debugGroup("Name : ", element.Name);
      Log.debugGroup("Type : ", element.Type);
      Log.debugGroup("state : ", this.state.CalculatedPremiItems);
      Log.debugGroup("remove : ", CalculatedPremiItems);
    }

    return CalculatedPremiItems;
  };

  // handle bundling cannot check / encheck extended cover
  handleBundling = (CalculatedPremiItems = this.state.CalculatedPremiItems) => {
    this.setState(
      {
        IsSRCCCheckedEnable: 1,
        IsETVCheckedEnable: 1,
        IsFLDCheckedEnable: 1,
        IsTRSCheckedEnable: 1,
        IsPADRIVERCheckedEnable: 1,
        IsPAPASSCheckedEnable: 1
      },
      () => {
        for (let index = 0; index < CalculatedPremiItems.length; index++) {
          const element = CalculatedPremiItems[index];

          if (
            element.CoverageID.toLowerCase().includes("src") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsSRCCCheckedEnable: 0,
              IsSRCCCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("fld") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsFLDCheckedEnable: 0,
              IsFLDCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("etv") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsETVCheckedEnable: 0,
              IsETVCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trs") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trrtlo") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("paddr1") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("padrvr") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("papass") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("pa24av") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          }
        }
      }
    );
  };

  // handle needsurvey checlist
  handleneedsurveychecktrue = (callback = () => {}) => {
    this.setState(
      {
        surveyneed: true
      },
      () => {
        callback();
      }
    );
  };

  // hit senquotationemail
  sendQuotationEmail = (orderNo, Email) => {
    // this.setState({ isLoading: true });
    toast.dismiss();
    toast.info("Send email to " + Email, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
    this.onDownloadTaskDetail(() => {
      this.setState({ showModal: false });
      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationEmail/`,
        "POST",
        HEADER_API,
        {
          Email: Email,
          OrderNo: orderNo
        }
      )
        .then(res => res.json())
        .then(json => {
          if (json.status) {
            this.setState({ showModal: false });
            toast.info("✅ Email has been sent ", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.dismiss();
            toast.warning("❗ Gagal Send Email - " + json.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Gagal Send Email - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          // this.sendQuotationEmail(orderNo, Email);
        });
    });
  };

  // hit sendquotationsms
  sendQuotationSMS = (orderNo, phone) => {
    phone = Util.replacePrefix(phone, ["62", "+62"], "0");
    phone = phone.replace(new RegExp(" ", "gm"), "");

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationSMS/`,
      "POST",
      HEADER_API,
      {
        Phone: phone,
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ SMS has been sent ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send SMS - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false,
          showModal: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send SMS - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, phone);
      });
  };

  // hit sendquotationWA
  sendQuotationWA = (OrderNo = this.state.selectedQuotation) => {
    this.getSendEmailTemplateWA(OrderNo);
  };

  selectedPhoneNumberCTI = () => {
    let phone = this.state.isCompany
      ? this.state.companypichp
      : this.state.personalhp;

    let datas = [...this.state.dataphonelist].filter(data =>
      Util.detectPrefix(data.value, this.state.Parameterized.mobilePrefix)
    );

    if (datas.length > 0) {
      phone = datas[0].value;
    }

    return phone;
  };

  // hit to get template email
  getSendEmailTemplate = orderNo => {
    this.setState({ showModal: false });
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSendEmailTemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState({
              SubjectTemplateEmail: json.data.Subject,
              BodyTemplateSendEmail: json.data.Body.replace(
                "<html><body>",
                "<p>"
              )
                .replace("</body></html>", "</p>")
                .replace(new RegExp("\\<b>", "gm"), "<strong>")
                .replace(new RegExp("\\</b>", "gm"), "</strong>")
                .replace(
                  "#InformasiPembayaran",
                  !Util.isNullOrEmpty(this.state.paymentnorek)
                    ? this.state.paymentnorek
                    : this.state.paymentvapermata
                )
                .trim()
              // BodyTemplateSendEmail: Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")
            });
          }
          this.setState({ showModal: false });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit to get template email WA
  getSendEmailTemplateWA = orderNo => {
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSendWATemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState(
              {
                SubjectTemplateEmailWA: json.data.Subject,
                BodyTemplateSendEmailWA: json.data.Body.replace(
                  "<html><body>",
                  "<p>"
                  // +
                  // Util.stringParse(
                  //   this.state.Parameterized.additionalWA,
                  //   Util.maskingPhoneNumberv2(this.state.phonelistselect, this.state.Parameterized.prefixPhone, 4)
                  // ) +
                  // "<br/>"
                  //+
                  // Util.stringParse(
                  //   this.state.Parameterized.additionalWA,
                  //   this.state.phonelistselect
                  // )
                )
                  .replace("</body></html>", "</p>")
                  .replace(new RegExp("\\<b>", "gm"), "<strong>")
                  .replace(new RegExp("\\</b>", "gm"), "</strong>")
                  .replace(
                    "#InformasiPembayaran",
                    " " +
                      (!Util.isNullOrEmpty(this.state.paymentnorek)
                        ? this.state.paymentnorek
                        : this.state.paymentvapermata)
                  )
                  .trim(),
                EmailToTemplateWA: json.data.EmailTo
                // BodyTemplateSendEmail: Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")
              },
              () => {
                this.setState({
                  showModalSendEmailWA: true
                });
              }
            );
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit to get template email WA
  getSendEmailTemplateSMSTelerenewal = orderNo => {
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetSendSMSTemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState({
              BodyTemplateSendEmailSMSTelerenewal: json.data.Body.replace(
                "<html><body>",
                "<p>"
              )
                .replace("</body></html>", "</p>")
                .replace(new RegExp("\\<b>", "gm"), "<strong>")
                .replace(new RegExp("\\</b>", "gm"), "</strong>")
                .replace(
                  "#InformasiPembayaran",
                  !Util.isNullOrEmpty(this.state.paymentnorek)
                    ? this.state.paymentnorek
                    : this.state.paymentvapermata
                )
                .trim()
            });
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit senquotationemail TELERENEWAL
  sendQuotationEmailTelerenewal = (
    EmailTo,
    Subject,
    Body,
    OrderNo,
    ListAtt = [],
    callback = () => {}
  ) => {
    this.setState({ showModal: false });
    let seq = 0;
    Body = (Body + "")
      .replace(new RegExp("\\<br>", "gm"), "<br/>")
      .replace(new RegExp("<p>|</p>", "gm"), "")
      .replace(new RegExp("\\/n", "gm"), "");

    Body = "<html><body>" + Body;
    Body =
      Body.replace(
        "<html><body>",
        this.state.Parameterized.stylingSendEmailWA
      ) + "</body></html>";

    if (this.state.showModalSendEmailWA) {
      Body = Body
        // .replace("<html><body>", this.state.Parameterized.stylingSendEmailWA)
        .replace(
          this.state.Parameterized.stylingSendEmailWA,
          this.state.Parameterized.stylingSendEmailWA +
            Util.stringParse(
              this.state.Parameterized.additionalWA,
              this.state.phonelistselect
            ) +
            "<br />"
        );
      // .replace(Util.stringParse(
      //   this.state.Parameterized.additionalWA,
      //   Util.maskingPhoneNumberv2(this.state.phonelistselect, this.state.Parameterized.prefixPhone, 4)
      // ), Util.stringParse(
      //   this.state.Parameterized.additionalWA,
      //   this.state.phonelistselect
      // ));
    }

    let sizeAll = 0;

    for (let i = 1; i <= 5; i++) {
      const element = this.state[`dataAttachmentEmail${i}`];
      // if(!Util.isNullOrEmpty(element)){
      if (element.length > 0) {
        // if(element[0].size > this.state.Parameterized.maxSizeAttachmentEmail){
        //   Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING")
        //   return;
        // }

        sizeAll += element[0].size;

        if (sizeAll > this.state.Parameterized.maxSizeAttachmentEmail) {
          Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING");
          return;
        }

        ListAtt.push(
          this.setModelAttachmentEmail(
            seq,
            null,
            this.state[`dataAttachmentEmail${i}`][0].name.substring(
              0,
              this.state[`dataAttachmentEmail${i}`][0].name.indexOf(
                Util.getExtensionFile(
                  this.state[`dataAttachmentEmail${i}`][0].name
                )
              ) - 1
            ), // name file // i,
            Util.base64Only(this.state[`base64AttachmentEmail${i}`]),
            "." +
              Util.getExtensionFile(
                this.state[`dataAttachmentEmail${i}`][0].name
              ), // "." + (this.state[`typeAttachmentEmail${i}`] + "").split("/")[1],
            this.state[`typeAttachmentEmail${i}`]
          )
        );
      }
    }
    this.setState({ isLoading: true });

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendViaEmail/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        EmailTo,
        EmailCC: "",
        EmailBCC: "",
        Subject,
        Body,
        OrderNo,
        ListAtt,
        IsAddAtt: true // flag enable attachment RDL
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ Email berhasil dikirim ! ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send Email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send Email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  // hit senquotationemail TELERENEWAL
  sendQuotationSMSTelerenewal = (PhoneNo, SMSText, callback = () => {}) => {
    // this.setState({ isLoading: true });
    // toast.dismiss();
    // toast.info("Send SMS to " + PhoneNo, {
    //   position: "top-right",
    //   autoClose: 5000,
    //   hideProgressBar: false,
    //   closeOnClick: true,
    //   pauseOnHover: true,
    //   draggable: true
    // });
    this.setState({ showModal: false });
    let seq = 0;
    SMSText = (SMSText + "")
      .replace(/<[^>]*>/g, "")
      .replace(/&nbsp;/g, " ")
      .trim();
    // .replace(/\n/g, "%0a")

    PhoneNo = Util.replacePrefix(PhoneNo, ["62", "+62"], "0");
    PhoneNo = PhoneNo.replace(new RegExp(" ", "gm"), "");

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendViaSMS/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        PhoneNo,
        SMSText
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ SMS berhasil dikirim! ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send SMS - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send SMS - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  getPhoneList = (
    OrderNo = this.state.selectedQuotation,
    showNotification = false,
    callback = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetPhoneList/`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        let dataphonelist = jsn.data.map(data => ({
          value: data.NoHp,
          label: Util.maskingPhoneNumberv2(Util.clearPhoneNo(data.NoHp))
        }));
        // if(jsn.data.length > 0){
        this.setState(
          {
            dataphonelist,
            dataGetPhoneList: jsn.data
          },
          () => {
            if (
              this.state.IsRenewal &&
              showNotification &&
              [...this.state.dataphonelist].filter(data =>
                Util.detectPrefix(
                  data.value,
                  this.state.Parameterized.mobilePrefix
                )
              ).length == 0
            ) {
              Util.showToast(
                "Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.",
                "WARNING"
              );
            }
            callback();
          }
        );
        // }else{
        //   if(showNotification){
        //     Util.showToast("Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.", "WARNING");
        //   }
        // }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  getTPLSI = (ProductCode, callback = () => {}) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getTPLSIList/`,
      "POST",
      HEADER_API,
      {
        ProductCode
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ dataTPLSIall: jsn.data }, () => {
          /// HANDLE DEFAULT TSI
          // let TPLCoverageId = [...this.state.dataTPLSIall].filter(
          //   data => data.TSI == this.state.defaultTSITPL
          // );
          // if (TPLCoverageId.length > 0) {
          //   TPLCoverageId = TPLCoverageId[0];
          //   this.setState({
          //     TPLCoverageId: TPLCoverageId.CoverageId,
          //     TPLSICOVER: TPLCoverageId.TSI
          //   });
          // }
        });
        return jsn.data.map(data => ({
          value: `${data.CoverageId}`,
          label: `${Util.formatMoney(data.TSI, 0)}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            dataTPLSI: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  getVehiclePrice = (
    vehiclecode = this.state.vehiclevehiclecode,
    Year = this.state.vehicleYear,
    citycode = this.state.vehicleregion,
    callback = () => {}
  ) => {
    // this.setState({ isLoading: true });
    if (
      Util.isNullOrEmpty(vehiclecode) ||
      Util.isNullOrEmpty(Year) ||
      Util.isNullOrEmpty(citycode)
    ) {
      callback();
      this.setState({
        isLoading: false
      });
      return;
    }
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/getvehicleprice/`,
      "POST",
      HEADER_API,
      {
        vehiclecode,
        Year,
        citycode
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // if (jsn.status) {
        // if (!Util.isNullOrEmpty(jsn.data)) {
        this.setState(
          {
            vehicletotalsuminsured: jsn.data
          },
          () => {
            callback();
          }
        );
        // }
        // }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  onCloseModalInfo = () => {
    this.setState({ showModalInfo: false });
  };

  onCloseModalInfoTryAPI = () => {
    window.location.reload();
  };

  onCloseModalInfoIsPaid = () => {
    this.setState({ showModalInfoIsPaid: false });
  };

  onDownloadQuotation = OrderNo => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/DownloadQuotation/`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == true) {
          if (jsn.data.FileName == null) {
            this.onShowAlertModalInfo("Nothing quotation for this prospect");
          } else {
            Util.saveByteArrayToPDF(
              jsn.data.FileName,
              Util.base64ToArrayBuffer(jsn.data.Bytes)
            );
          }
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        Util.hittingAPIError(error, () => {
          // this.getListDealer();
        });
      });
  };

  showQuotationHistory = FollowUpNo => {
    Log.debugGroup("showQuotationHistory");
    this.setState({ isLoading: true });
    this.setState({ dataquotationhistory: [] });
    if (FollowUpNo != null) {
      this.setState({ isLoading: true });
      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`,
        "POST",
        HEADER_API,
        {
          followupno: FollowUpNo
        }
      )
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status != false && jsn.data.length > 0) {
            return jsn.data.map(data => ({
              ...data,
              ProspectName: this.state.isCompany
                ? this.state.companyname
                : this.state.personalname,
              FollowUpNo: FollowUpNo
            }));
          } else {
            return [];
          }
        })
        .then(datamapping => {
          this.setState(
            {
              dataquotationhistory: datamapping,
              isLoading: false
            },
            () => {
              if (this.state.dataquotationhistory.length > 0) {
                this.setState({ showModal: true });
              } else {
                toast.dismiss();
                toast.warning(
                  "❗ There is no quotation for this prospect. Please make quotation first.",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                  }
                );
              }
            }
          );
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          Util.hittingAPIError(error, () => {
            // this.showQuotationHistory(FollowUpNo);
          });
        });
    } else {
      Log.debugGroup("FollowUpNo tidak ada");
    }
  };

  onClickQuotationHistory = (OrderNo, FollowUpNo, QuotationNo) => {
    // this.setState({
    //   OrderNoQuotationEdit: OrderNo,
    //   FollowUpNoQuotationEdit: FollowUpNo
    // });
    Log.debugGroup("Masuk");
    this.setState({ selectedQuotation: OrderNo });
  };

  onCloseModalSendEmail = () => {
    this.setState({ showModalSendEmail: false });
  };

  onCloseModalSendSMS = () => {
    this.setState({ showModalSendSMS: false });
  };

  onCloseModalSendWA = () => {
    this.setState({ showModalSendWA: false });
  };

  onCloseModalSendEmailWA = () => {
    this.setState({ showModalSendEmailWA: false });
  };

  setModelPersonalData = () => {
    if (Util.isNullOrEmpty(JSON.parse(ACCOUNTDATA))) {
      return;
    }
    let User = JSON.parse(ACCOUNTDATA).UserInfo.User;
    return {
      Name: this.state.personalname,
      CustBirthDay: Util.formatDate(
        this.state.personaltanggallahir,
        "yyyy-mm-dd hh:MM:ss"
      ),
      CustGender: this.state.personalJK,
      CustAddress: this.state.personalalamat,
      PostalCode: this.state.personalkodepos,
      Email1: this.state.personalemail,
      Phone1: this.state.personalhp,
      IdentityNo: this.state.personalnomoridentitas,
      AmountRep: this.state.personalamountrep,
      IsPayerCompany: this.state.personalpayercompany,
      isNeedDocRep: this.state.personalneeddocrep,
      isCompany: this.state.isCompany,
      SalesOfficerID: this.state.SalesOfficerID || User.SalesOfficerID
    };
  };

  setModelCompanyData = () => {
    return {
      CompanyName: this.state.companyname,
      NPWPno: this.state.companynpwpnumber,
      NPWPdate: Util.isNullOrEmpty(this.state.companynpwpdata)
        ? null
        : Util.formatDate(this.state.companynpwpdata),
      NPWPaddres: this.state.companynpwpaddress,
      SIUPno: this.state.companysiupnumber,
      PKPno: this.state.companypkpnumber,
      PKPdate: this.state.companypkpdata,
      OfficeAddress: this.state.companyofficeaddress,
      PostalCode: this.state.companypostalcode,
      OfficePhoneNo: this.state.companyofficenumber,
      PICPhoneNo: this.state.companypichp,
      PICname: this.state.companypdcname,
      PICEmail: this.state.companyemail
    };
  };

  setModelVehicleData = () => {
    return {
      Vehicle: "",
      ProductTypeCode: this.state.vehicleProductTypeCode,
      ProductCode: this.state.vehicleproductcode,
      VehicleCode: this.state.vehiclevehiclecode,
      BrandCode: this.state.vehicleBrandCode,
      ModelCode: this.state.vehicleModelCode,
      Series: this.state.vehicleSeries,
      Type: this.state.vehicleType,
      Sitting: this.state.vehicleSitting,
      Year: this.state.vehicleYear,
      CityCode: Util.valueTrim(this.state.vehicleregion),
      InsuranceType: Util.isNullOrEmpty(this.state.vehicleInsuranceType)
        ? 0
        : this.state.vehicleInsuranceType,
      SumInsured: this.state.vehicletotalsuminsuredtemp,
      SegmentCode: this.state.vehiclesegmentcode,
      UsageCode: this.state.vehicleusage,
      AccessSI: this.state.vehicleAccessSI,
      RegistrationNumber: (this.state.vehicleplateno + "").toUpperCase(),
      EngineNumber: (this.state.vehicleengineno + "").toUpperCase(),
      ChasisNumber: (this.state.vehiclechasisno + "").toUpperCase(),
      IsNew: !this.state.vehicleusedcar,
      BankID: "",
      VANumber: "",
      Remarks: this.state.vehicleremarks,
      IsPolicyIssuedBeforePaying: this.state.vehicleispolicyissuedbeforpaying
        ? 1
        : 0,
      IsORDefectsRepair: this.state.vehicleisordefectrepair ? 1 : 0,
      IsAccessoriesChange: this.state.vehicleisaccessorieschange ? 1 : 0,
      DealerCode: this.state.vehicledealer,
      SalesDealer: this.state.vehiclesalesman,
      ColorOnBPKB: this.state.vehiclecolor,
      PeriodFrom: Util.formatDate(
        this.state.vehicleperiodfrom,
        "yyyy-mm-dd hh:MM:ss"
      ),
      PeriodTo: Util.formatDate(
        this.state.vehicleperiodto,
        "yyyy-mm-dd hh:MM:ss"
      )
    };
  };

  setModelPolicyAddress = () => {
    return {
      SentTo: this.state.policysentto,
      Name: this.state.policyname,
      Address: this.state.policyaddress,
      PostalCode: this.state.policykodepos
    };
  };

  setModelSurveySchedule = () => {
    return {
      CityCode: this.state.surveykota,
      LocationCode: this.state.surveylokasi,
      SurveyAddress: this.state.surveyalamat,
      SurveyPostalCode: this.state.surveykodepos,
      SurveyDate: Util.formatDate(
        this.state.surveytanggal,
        "yyyy-mm-dd hh:MM:ss"
      ),
      ScheduleTimeID: this.state.surveywaktu,
      IsNeedSurvey: this.state.surveyneed,
      IsNSASkipSurvey: this.state.surveynsaskipsurvey,
      IsManualSurvey: this.state.surveychecklistmanual
    };
  };

  setModelImageData = () => {
    let ImageDataList = [
      { ImageType: "IDENTITYCARD", PathFile: this.state.KTP },
      { ImageType: "STNK", PathFile: this.state.STNK },
      { ImageType: "BSTB", PathFile: this.state.BSTB },
      { ImageType: "FAKTUR", PathFile: this.state.FAKTUR },
      { ImageType: "DOCNSA1", PathFile: this.state.DOCNSA1 },
      { ImageType: "DOCNSA2", PathFile: this.state.DOCNSA2 },
      { ImageType: "DOCNSA3", PathFile: this.state.DOCNSA3 },
      { ImageType: "DOCNSA4", PathFile: this.state.DOCNSA4 },
      { ImageType: "DOCNSA5", PathFile: this.state.DOCNSA5 },
      { ImageType: "KONFIRMASICUST", PathFile: this.state.KONFIRMASICUST },
      { ImageType: "SPPAKB", PathFile: this.state.SPPAKB },
      { ImageType: "DOCREP", PathFile: this.state.DOCREP },
      { ImageType: "NPWP", PathFile: this.state.NPWP },
      { ImageType: "SIUP", PathFile: this.state.SIUP },
      { ImageType: "BUKTIBAYAR", PathFile: this.state.BUKTIBAYAR },
      { ImageType: "BUKTIBAYAR2", PathFile: this.state.BUKTIBAYAR2 },
      { ImageType: "BUKTIBAYAR3", PathFile: this.state.BUKTIBAYAR3 },
      { ImageType: "BUKTIBAYAR4", PathFile: this.state.BUKTIBAYAR4 },
      { ImageType: "BUKTIBAYAR5", PathFile: this.state.BUKTIBAYAR5 }
    ];

    // ImageDataList = (Util.isNullOrEmpty(this.state.selectedQuotation) ? ImageDataList.filter( data => data.ImageType != "BUKTIBAYAR" ) : ImageDataList);

    return ImageDataList;
  };

  setModelRemarks = () => {
    return {
      RemarkToSA: this.state.documentsremarkstosa
      // RemarkFromSA: this.state.documentsremarksfromsa,
    };
  };

  setModelAttachmentEmail = (
    Seq,
    AttachmentID,
    AttachmentDescription,
    FileByte,
    FileExtension,
    FileContentType
  ) => {
    return {
      Seq,
      AttachmentID,
      AttachmentDescription,
      FileByte,
      FileExtension,
      FileContentType
    };
  };

  setModelOrderSimulation = () => {
    if (Util.isNullOrEmpty(JSON.parse(ACCOUNTDATA))) {
      return;
    }
    let User = JSON.parse(ACCOUNTDATA).UserInfo.User;
    Log.debugGroup();
    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.vehiclebasiccoverage
    )[0];

    let MultiYearF = false;
    let YearCoverage = 0;

    if (!Util.isNullOrEmpty(BasicCover)) {
      YearCoverage = BasicCover.TLOPeriod + BasicCover.ComprePeriod;
      MultiYearF = YearCoverage > 1 ? true : false;
    }

    return {
      OrderNo: this.state.selectedQuotation,
      CustID: this.state.CustID,
      FollowUpNo: this.state.FollowUpNo,
      // QuotationNo: "",
      TLOPeriod: Util.isNullOrEmpty(BasicCover) ? 0 : BasicCover.TLOPeriod,
      ComprePeriod: Util.isNullOrEmpty(BasicCover)
        ? 0
        : BasicCover.ComprePeriod,
      BranchCode: User.BranchCode,
      SalesOfficerID: this.state.SalesOfficerID || User.SalesOfficerID,
      PhoneSales: User.Phone1,
      SalesDealer: this.state.vehiclesalesman,
      ProductTypeCode: this.state.vehicleProductTypeCode,
      ProductCode: this.state.vehicleproductcode,
      Phone1: this.state.isCompany
        ? this.state.companyofficenumber
        : this.state.personalhp,
      Email1: this.state.isCompany
        ? this.state.companyemail
        : this.state.personalemail,
      Email2: "",
      InsuranceType: this.state.vehicleInsuranceType,
      ApplyF: "true",
      SendF: "false",
      AdminFee: this.state.AdminFee,
      MultiYearF: MultiYearF,
      TotalPremi: this.state.TotalPremi,
      YearCoverage: YearCoverage,
      LastInterestNo: 0,
      LastCoverageNo: 0
    };
  };

  setModelOrderSimulationMV = () => {
    return {
      OrderNo: this.state.selectedQuotation,
      ObjectNo: 1,
      AccessSI: this.state.ACCESSCOVER,
      ProductTypeCode: this.state.vehicleProductTypeCode,
      VehicleCode: this.state.vehiclevehiclecode,
      BrandCode: this.state.vehicleBrandCode,
      ModelCode: this.state.vehicleModelCode,
      Series: this.state.vehicleSeries,
      Type: this.state.vehicleType,
      Sitting: this.state.vehicleSitting,
      Year: this.state.vehicleYear,
      CityCode: this.state.vehicleregion,
      UsageCode: this.state.vehicleusage,
      SumInsured: this.state.vehicletotalsuminsuredtemp,
      RegistrationNumber: (this.state.vehicleplateno + "").toUpperCase(),
      EngineNumber: (this.state.vehicleengineno + "").toUpperCase(),
      ChasisNumber: (this.state.vehiclechasisno + "").toUpperCase(),
      IsNew: !this.state.vehicleusedcar
    };
  };

  /// SAVE UPDATETASKDETAIL
  onUpdateTaskDetail = (
    callback = () => {},
    FollowUpStatus = this.state.FollowUpStatus
  ) => {
    if (Util.isNullOrEmpty(ACCOUNTDATA)) {
      return;
    }
    Log.debugGroup("hit onUpdateTaskDetail");
    let PersonalData = this.setModelPersonalData();
    let CompanyData = this.setModelCompanyData();
    let VehicleData = this.setModelVehicleData();
    let PolicyAddress = this.setModelPolicyAddress();
    let SurveySchedule = this.setModelSurveySchedule();
    let ImageData = this.setModelImageData();
    let Remarks = this.setModelRemarks();

    let CalculatedPremiItems = {
      ListCalculatePremi: this.state.CalculatedPremiItems,
      OSData: this.setModelOrderSimulation(),
      OSMVData: this.setModelOrderSimulationMV()
    };

    this.abortControllerUpdateTaskDetail.abort();
    this.abortControllerUpdateTaskDetail = new AbortController();

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/UpdateTaskDetail/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        // CustID: this.state.CustID,
        // FollowUpNo: this.state.FollowUpNo,
        // PersonalData: PersonalData,
        // CompanyData: CompanyData,
        // VehicleData: VehicleData,
        // PolicyAddress: PolicyAddress,
        // SurveySchedule: SurveySchedule,
        // ImageData:
        //   // '[{"ImageType":"IDENTITYCARD", "PathFile":"76dc35f5-04b1-4911-8ef5-e6a4cd167014.png" }]',
        //   ImageData,
        // Remarks: Remarks,
        // OSData: Util.isNullOrEmpty(this.state.selectedQuotation)
        //   ? JSON.stringify(this.setModelOrderSimulation())
        //   : "",
        // OSMVData: Util.isNullOrEmpty(this.state.selectedQuotation)
        //   ? JSON.stringify(this.setModelOrderSimulationMV())
        //   : "",
        // calculatedPremiItems: JSON.stringify(CalculatedPremiItems),
        // FollowUpStatus: FollowUpStatus

        CustID: this.state.CustID,
        FollowUpNo: this.state.FollowUpNo,
        PersonalData: PersonalData,
        CompanyData: CompanyData,
        VehicleData: VehicleData,
        PolicyAddress: PolicyAddress,
        SurveySchedule: SurveySchedule,
        ImageData:
          // '[{"ImageType":"IDENTITYCARD", "PathFile":"76dc35f5-04b1-4911-8ef5-e6a4cd167014.png" }]',
          ImageData,
        Remarks: Remarks,
        OSData: Util.isNullOrEmpty(this.state.selectedQuotation)
          ? this.setModelOrderSimulation()
          : "",
        OSMVData: Util.isNullOrEmpty(this.state.selectedQuotation)
          ? this.setModelOrderSimulationMV()
          : "",
        calculatedPremiItems: CalculatedPremiItems,
        FollowUpStatus: FollowUpStatus
      },
      {
        signal: this.abortControllerUpdateTaskDetail.signal
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // this.setState({
        //   isLoading: false
        // });
        if (jsn.status) {
          this.setState({
            selectedQuotation: jsn.data.OrderNo
          });

          let PaymentInfo = jsn.data.PaymentInfo;

          if (!Util.isNullOrEmpty(PaymentInfo)) {
            Log.debugGroup("PaymentInfo : ", PaymentInfo);
            // this.setState({
            //   paymentduedate: Util.isNullOrEmpty(PaymentInfo.DueDate)
            //     ? null
            //     : Util.formatDate(
            //         PaymentInfo.DueDate,
            //         "ddd, mmm dS, yyyy, hh:MM TT"
            //       ),
            //   paymentvapermata: PaymentInfo.VAPermata,
            //   paymentvamandiri: PaymentInfo.VAMandiri,
            //   paymentvabca: PaymentInfo.VABCA,
            //   paymentnorek: PaymentInfo.RekeningPenampung
            // });
            this.GetPaymentInfo();
          }
        }
        callback();
      })
      .catch(error => {
        // this.setState({
        //   isLoading: false
        // });
        Log.error("parsing Failed", error);
        Util.hittingAPIError(error, () => {
          // this.getListDealer();
        });
      });
  };

  GetPaymentInfo = (
    CustID = this.state.CustID,
    FollowUpNo = this.state.FollowUpNo
  ) => {
    // Util.fetchAPIAdditional(`${API_URL+API_V0213URF2019}/DataReact/GetPaymentInfo`, "POST",
    Util.fetchAPIAdditional(
      `${API_URL + API_VERSION_0232URF2019}/DataReact/GetPaymentInfo`,
      "POST",
      HEADER_API,
      {
        CustID,
        FollowUpNo
      }
    )
      .then(res => res.json())
      .then(data => {
        if (!Util.isNullOrEmpty(data.data)) {
          data = data.data;
          this.setState({
            paymentduedate: Util.isNullOrEmpty(data.DueDate)
              ? null
              : Util.formatDate(data.DueDate, "ddd, mmm dS, yyyy, hh:MM TT"),
            paymentvapermata: data.VAPermata,
            paymentvamandiri: data.VAMandiri,
            paymentvabca: data.VABCA,
            paymentnorek: data.RekeningPenampung,
            paymentnova: data.VANo,
            paymentisvaactive: data.IsVAActive,
            dataBUKTIBAYAR: Util.isNullOrEmpty(data.BuktiBayarData)
              ? []
              : [
                  Util.dataURItoBlob(
                    "data:image/" +
                      Util.getExtensionFile(data.BuktiBayar) +
                      ";base64," +
                      data.BuktiBayarData
                  )
                ],
            BUKTIBAYAR: Util.isNullOrEmpty(data.BuktiBayar)
              ? ""
              : data.BuktiBayar,
            dataBUKTIBAYAR2: Util.isNullOrEmpty(data.BuktiBayar2Data)
              ? []
              : [
                  Util.dataURItoBlob(
                    "data:image/" +
                      Util.getExtensionFile(data.BuktiBayar2) +
                      ";base64," +
                      data.BuktiBayar2Data
                  )
                ],
            BUKTIBAYAR2: Util.isNullOrEmpty(data.BuktiBayar2)
              ? ""
              : data.BuktiBayar2,
            dataBUKTIBAYAR3: Util.isNullOrEmpty(data.BuktiBayar3Data)
              ? []
              : [
                  Util.dataURItoBlob(
                    "data:image/" +
                      Util.getExtensionFile(data.BuktiBayar3) +
                      ";base64," +
                      data.BuktiBayar3Data
                  )
                ],
            BUKTIBAYAR3: Util.isNullOrEmpty(data.BuktiBayar3)
              ? ""
              : data.BuktiBayar3,
            dataBUKTIBAYAR4: Util.isNullOrEmpty(data.BuktiBayar4Data)
              ? []
              : [
                  Util.dataURItoBlob(
                    "data:image/" +
                      Util.getExtensionFile(data.BuktiBayar4) +
                      ";base64," +
                      data.BuktiBayar4Data
                  )
                ],
            BUKTIBAYAR4: Util.isNullOrEmpty(data.BuktiBayar4)
              ? ""
              : data.BuktiBayar4,
            dataBUKTIBAYAR5: Util.isNullOrEmpty(data.BuktiBayar5Data)
              ? []
              : [
                  Util.dataURItoBlob(
                    "data:image/" +
                      Util.getExtensionFile(data.BuktiBayar5) +
                      ";base64," +
                      data.BuktiBayar5Data
                  )
                ],
            BUKTIBAYAR5: Util.isNullOrEmpty(data.BuktiBayar5)
              ? ""
              : data.BuktiBayar5
          });
        }
      })
      .catch(error => {
        console.error("error :", error);
      });
  };

  getCheckVAPayment = (
    OrderNo = this.state.selectedQuotation,
    callbackPaid = () => {},
    callbackNotPaid = () => {}
  ) => {
    // Util.fetchAPIAdditional(`${API_URL+API_V0213URF2019}/DataReact/GetPaymentInfo`, "POST",
    if (Util.isNullOrEmpty(OrderNo)) {
      return true;
    }

    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + API_VERSION_0232URF2019}/DataReact/CheckVAPayment`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
      .then(res => res.json())
      .then(data => {
        let IsPaid = data.IsPaid;
        this.setState(
          {
            IsPaid
          },
          () => {
            if (this.state.IsPaid) {
              callbackPaid();
              return false;
            } else {
              callbackNotPaid();
              return true;
            }
          }
        );

        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        this.setState({
          isLoading: false
        });
        // callbackNotPaid();
        console.error("error :", error);
        return true;
      });
  };

  onClickReactivedVA = () => {
    if (Util.isNullOrEmpty(this.state.paymentnova)) {
      Util.showToast("VA Number invalid", "WARNING");
      return;
    }
    this.ReactiveVANumber(
      this.state.paymentnova,
      this.state.PolicyOrderNo || ""
    );
  };

  ReactiveVANumber = (
    VANumber = this.state.paymentnova,
    PolicyOrderNo = this.state.PolicyOrderNo
  ) => {
    this.setState({
      isLoading: true
    });
    // Util.fetchAPIAdditional(`${API_URL+API_V0213URF2019}/DataReact//ReactiveVA`, "POST",
    Util.fetchAPIAdditional(
      `${API_URL + API_VERSION_0232URF2019}/DataReact//ReactiveVA`,
      "POST",
      HEADER_API,
      {
        VANumber,
        PolicyOrderNo
      }
    )
      .then(res => res.json())
      .then(data => {
        // if(data.status){
        Util.showToast("Reactivate VA berhasil !");
        this.GetPaymentInfo();
        // }
        this.setState({
          isLoading: false
        });
      });
  };

  onClickSurveyDocument = () => {
    secureStorage.setItem("dummyimage", "jhsdfjk"); // from dummy image survey
    secureStorage.setItem("taskdetail-surveyno", this.state.SurveyNo); // this.state.SurveyNo
    this.props.history.push("tasklist-surveydocuments");
  };

  editQuotationHistory = () => {
    this.setState({ showModal: false });
    // Log.debugGroup("MASUKKKKK editQuotationHistory");
    // Log.debugGroup("OrderNo", this.state.OrderNoQuotationEdit);
    // Log.debugGroup("FollowUpNo", this.state.FollowUpNoQuotationEdit);
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/editQuotation/`,
      "POST",
      HEADER_API,
      {
        followupno: this.state.FollowUpNo,
        orderNo: this.state.selectedQuotation,
        custid: this.state.CustID
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setInitialStateTaskDetail(() => {
          this.setStateGetTaskDetail(jsn.data, this.state.CustID); /// BERSIHIN MALAM INI
        });
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  addCopyQuotation = (FollowUpNo, OrderNo) => {
    Log.debugGroup("masuk");
    this.setState({ isLoading: true });
    if (FollowUpNo != null) {
      this.setState({ isLoading: true });
      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`,
        "POST",
        HEADER_API,
        {
          followupno: this.state.FollowUpNumber
        }
      )
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.data.length > 0) {
            var isSentToSA = false;
            var isApplyF = false;

            datamapping.data.forEach(data => {
              if (data.SendF == 1) {
                isSentToSA = true;
              }
              if (data.ApplyF) {
                isApplyF = true;
              }
              if (isSentToSA && isApplyF) {
                return;
              }
            });

            Log.debugGroup("isSentToSA", isSentToSA);

            Log.debugGroup("isApplyF", isApplyF);

            if (isSentToSA && this.state.FollowUpStatus == 6) {
              toast.dismiss();
              toast.warning(
                "❗ You must have at least one quotation to add quotation.",
                {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                }
              );
            } else if (!isApplyF) {
              toast.dismiss();
              toast.warning(
                "❗ You must complete your data of vehicle and cover to add more quotation.",
                {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                }
              );
            } else {
              this.setState({ showModalAddCopy: true });
            }
          } else {
            toast.dismiss();
            toast.warning(
              "❗ You must have at least one quotation to add quotation.",
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
              }
            );
          }

          this.setState({ isLoading: false });
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          if (!error + "".toLowerCase().includes("token")) {
            this.showQuotationHistory(FollowUpNo);
          }
        });
    } else {
      Log.debugGroup("FollowUpNo tidak ada");
    }
  };

  clickBtnAddCopyQuotation = clickState => {
    this.setState({ showModalAddCopy: false });
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/addCopyQuotation/`,
      "POST",
      HEADER_API,
      {
        followUpNo: this.state.FollowUpNo,
        orderNo: this.state.selectedQuotation,
        state: clickState
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setInitialStateTaskDetail(() => {
          this.setStateGetTaskDetail(jsn.data, this.state.CustID); /// BERSIHIN MALAM INI
        });
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        // this.clickBtnAddCopyQuotation(clickState);
      });
  };

  isSendToSA = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    // send to sa
    if (FollowUpStatus == 2 && FollowUpInfo == 61) {
      return true;
    }
    return false;
  };

  isSendToSurveyor = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    // send to surveyor
    if (FollowUpStatus == 2 && FollowUpInfo == 58) {
      return true;
    }
    return false;
  };

  isNewData = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    // data baru input
    if (FollowUpStatus == 1) {
      return true;
    }
    return false;
  };

  isBackToAO = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpStatus == 2 && FollowUpInfo == 60) {
      return true;
    }
    return false;
  };

  // is not follow up hasil survey
  hideVehicleOriginalDefect = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    let temp = true;

    if (FollowUpStatus == 2 && FollowUpInfo == 59) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isNeedApproval()) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isBackToAO()) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isFollowUpPayment()) {
      temp = false;
    }

    if (
      !Util.isNullOrEmpty(this.state.SurveyNo) &&
      this.isFollowUpSurveyResult()
    ) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isPolicyApproved()) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isPolicyDelivered()) {
      temp = false;
    }

    if (!Util.isNullOrEmpty(this.state.SurveyNo) && this.isPolicyReceived()) {
      temp = false;
    }

    if (
      !Util.isNullOrEmpty(this.state.SurveyNo) &&
      this.isPolicyApprovedDocRep()
    ) {
      temp = false;
    }

    if (
      !Util.isNullOrEmpty(this.state.SurveyNo) &&
      this.isPolicyDeliveredDocRep()
    ) {
      temp = false;
    }

    if (
      !Util.isNullOrEmpty(this.state.SurveyNo) &&
      this.isPolicyReceivedDocRep()
    ) {
      temp = false;
    }

    return temp;
  };

  // follow up nsa approval
  isNSAApproval = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 46) {
      return true;
    }
    return false;
  };

  isPolicyApproved = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 50) {
      return true;
    }
    return false;
  };

  isPolicyDelivered = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 51) {
      return true;
    }
    return false;
  };

  isPolicyReceived = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 52) {
      return true;
    }
    return false;
  };

  isPolicyApprovedDocRep = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 53) {
      return true;
    }
    return false;
  };

  isPolicyDeliveredDocRep = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 54) {
      return true;
    }
    return false;
  };

  isPolicyReceivedDocRep = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 55) {
      return true;
    }
    return false;
  };

  isNeedApproval = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpInfo == 62) {
      return true;
    }
    return false;
  };

  isFollowUpSurveyResult = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpStatus == 2 && FollowUpInfo == 59) {
      return true;
    }
    return false;
  };

  isFollowUpPayment = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    if (FollowUpStatus == 2 && FollowUpInfo == 57) {
      return true;
    }
    return false;
  };

  isNotDeal = (FollowUpStatus = this.state.FollowUpStatus) => {
    if (FollowUpStatus == 5) {
      return true;
    }
    return false;
  };

  isOrderRejected = (FollowUpStatus = this.state.FollowUpStatus) => {
    if (FollowUpStatus == 14) {
      return true;
    }
    return false;
  };

  isDealNeedDocRep = () => {
    let temp = false;

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }
    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }
    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  disabledEditTaskDetail = (
    FollowUpStatus = this.state.FollowUpStatus,
    FollowUpInfo = this.state.FollowUpInfo
  ) => {
    // send to surveyor
    if (this.isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
      return true;
    }

    // send to sa
    if (this.isSendToSA(FollowUpStatus, FollowUpInfo)) {
      return true;
    }

    return false;
  };

  disabledFieldRenewal = fieldName => {
    let disabled = false;
    // Log.debugStr("IsRenewal = " + this.state.IsRenewal);
    if (this.state.IsRenewal) {
      if (!Util.isNullOrEmpty(this.state[fieldName + "temp"])) {
        disabled = true;
      }
    }

    return disabled;
  };

  disabledProductType = () => {
    let temp = false;

    if (
      this.disabledFieldRenewal("vehicleInsuranceType") ||
      this.isBackToAO()
    ) {
      temp = true;
    }

    if (this.state.IsPaid) {
      temp = true;
    }

    return temp;
  };

  disabledProductCode = () => {
    let temp = false;

    if (
      (this.disabledFieldRenewal("vehicleproductcode") &&
        !this.isNSAApproval()) ||
      this.isBackToAO()
    ) {
      temp = true;
    }

    if (this.state.IsPaid) {
      temp = true;
    }

    return temp;
  };

  disabledUsedCar = () => {
    let temp = false;
    if (!Util.isNullOrEmpty(this.state.vehicleYear)) {
      if (this.state.vehicleYear != Util.convertDate().getFullYear()) {
        temp = true;
      }
    }

    return temp;
  };

  disableDealerVehicle = () => {
    let temp = false;

    if (this.disabledFieldRenewal("vehicledealer")) {
      temp = true;
    }

    // if(
    //   this.state.IsRenewal &&
    //   this.isNSAApproval() &&
    //   (this.state.vehicledealertemp != this.state.vehicledealer ||
    //   this.state.vehicleproductcode != this.state.vehicleproductcodetemp)
    //   ){

    //   temp = false;
    // }

    return temp;
  };

  disableSalesmanVehicle = () => {
    let temp = false;

    if (this.disabledFieldRenewal("vehiclesalesman")) {
      temp = true;
    }

    // if(
    //   this.state.IsRenewal &&
    //   this.isNSAApproval() &&
    //   (this.state.vehiclesalesmantemp != this.state.vehiclesalesman ||
    //   this.state.vehicleproductcode != this.state.vehicleproductcodetemp)
    //   ){
    //   temp = false;
    // }

    return temp;
  };

  disabledNeedSurvey = () => {
    let temp = false;
    if (this.state.surveynsaskipsurvey) {
      temp = true;
    }

    if (this.state.surveychecklistmanual) {
      temp = true;
    }

    if (this.state.FollowUpInfo == 59) {
      temp = true;
    }

    if (this.state.vehicleisordefectrepair) {
      temp = true;
    }

    if (this.state.vehicleisaccessorieschange) {
      temp = true;
    }

    if (!Util.isNullOrEmpty(this.state.OldPolicyPeriodTo)) {
      if (this.state.OldPolicyPeriodTo < Util.convertDate()) {
        temp = true;
      }
      if (this.state.OldPolicyPeriodTo >= Util.convertDate()) {
        temp = true;
      }
    }

    return temp;
  };

  needSurveyChangeExtendedCoverRenewal = () => {
    let temp = false;

    // if(this.state.IsFLDChecked != this.state.IsFLDCheckedtemp ||
    //   this.state.IsETVChecked != this.state.IsETVCheckedtemp ||
    //   this.state.IsSRCCChecked !=
    //     this.state.IsSRCCCheckedtemp ||
    //   this.state.IsTPLChecked != this.state.IsTPLCheckedtemp ||
    //   this.state.IsTSChecked != this.state.IsTSCheckedtemp ||
    //   this.state.SRCCPremi != this.state.SRCCPremitemp ||
    //   this.state.ETVPremi != this.state.ETVPremitemp ||
    //   this.state.FLDPremi != this.state.FLDPremitemp ||
    //   this.state.TPLPremi != this.state.TPLPremitemp ||
    //   this.state.TSPremi != this.state.TSPremitemp){
    //     temp = true;
    // }

    if (
      (this.state.IsFLDCheckedtemp == false &&
        this.state.IsFLDChecked == true) ||
      this.state.FLDPremi > this.state.FLDPremitemp
    ) {
      // Penambahan FLD
      temp = true;
    }

    if (
      (this.state.IsETVCheckedtemp == false &&
        this.state.IsETVChecked == true) ||
      this.state.ETVPremi > this.state.ETVPremitemp
    ) {
      // Penambahan ETV
      temp = true;
    }

    if (
      (this.state.IsSRCCCheckedtemp == false &&
        this.state.IsSRCCChecked == true) ||
      this.state.SRCCPremi > this.state.SRCCPremitemp
    ) {
      // Penambahan SRCC
      temp = true;
    }

    if (
      (this.state.IsTPLCheckedtemp == false &&
        this.state.IsTPLChecked == true) ||
      this.state.TPLPremi > this.state.TPLPremitemp
    ) {
      // Penambahan TPL
      temp = true;
    }

    if (
      (this.state.IsTSCheckedtemp == false && this.state.IsTSChecked == true) ||
      this.state.TSPremi > this.state.TSPremitemp
    ) {
      temp = true;
    }

    if (this.state.ACCESSPremitemp > this.state.ACCESSPremi) {
      temp = true;
    }

    return temp;
  };

  handleNeedSurveyChecklist = (callback = () => {}) => {
    this.setState({
      surveyneedprev: this.state.surveyneed
    });
    if (this.state.FollowUpStatus == 13) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isFollowUpPayment()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isFollowUpSurveyResult()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isNeedApproval()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isBackToAO()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isSendToSA()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isSendToSurveyor()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.state.surveynsaskipsurvey) {
      this.setState({
        surveyneed: false
      });
      return;
    }

    if (this.state.surveychecklistmanual) {
      this.setState({
        surveyneed: false
      });
      return;
    }

    let surveyneed = true;
    this.setState(
      {
        surveyneed
      },
      () => {
        callback();
      }
    );
  };

  handleNeedSurveyUnChecklist = (callback = () => {}) => {
    if (this.isBackToAO()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isSendToSA()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.isSendToSurveyor()) {
      this.setState({
        surveyneed: this.state.surveyneedtemp
      });
      return;
    }
    if (this.state.surveynsaskipsurvey) {
      this.setState({
        surveyneed: false
      });
      return;
    }

    if (this.state.surveychecklistmanual) {
      this.setState({
        surveyneed: false
      });
      return;
    }

    if (!this.state.surveynsaskipsurvey) {
      this.setState({
        // surveyneed: this.state.surveyneedtemp
        surveyneed: this.state.surveyneedprev
      });
    }

    if (!this.state.surveychecklistmanual) {
      this.setState({
        surveyneed: this.state.surveyneedprev
      });
    }

    if (this.state.IsRenewal) {
      let surveyneed = false;
      if (this.state.vehicleisordefectrepair) {
        surveyneed = true;
      } else if (this.state.vehicleisaccessorieschange) {
        surveyneed = true;
      } else if (this.needSurveyChangeExtendedCoverRenewal()) {
        // surveyneed = true;
        // fixing ntar
        this.handleNeedSurveyChecklist();
        return;
      } else {
        surveyneed = false;
      }
      this.setState({
        surveyneed
      });
    }
  };

  ModalButtonSendQuotation = () => (
    <Modal
      styles={{
        modal: {
          width: "80%",
          maxWidth: "500px"
        },
        overlay: {
          zIndex: 1050
        }
      }}
      showCloseIcon={false}
      open={this.state.showModalButtonSendQuotation}
      onClose={() => this.setState({ showModalButtonSendQuotation: false })}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-17px" }}
      >
        <div className="modal-body">
          <div
            onClick={() => {
              this.setState({
                showModalMakeSure: true,
                sendEmailOrSMS: "EMAIL"
              });
            }}
            className="form-group separatorBottom paddingside"
            style={{
              paddingTop: "5px",
              paddingBottom: "5px"
            }}
          >
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list">
                <span
                  className="smalltext labelspanbold"
                  style={{ fontSize: "initial" }}
                >
                  <i
                    className="fa fa-envelope-o"
                    style={{ marginRight: "10px" }}
                  />
                  Send via Email
                </span>
              </div>
            </div>
          </div>
          {Util.stringArrayContains(
            this.state.role,
            this.state.Parameterized.roleCTI
          ) &&
            this.state.IsRenewal && (
              <div
                onClick={() => {
                  this.setState({
                    showModalMakeSure: true,
                    sendEmailOrSMS: "WA"
                  });
                }}
                className="form-group separatorBottom paddingside"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <div
                  className="row panel-body-list"
                  style={{ marginLeft: "0px", marginRight: "0px" }}
                >
                  <div className="col-xs-12 panel-body-list">
                    <span
                      className="smalltext labelspanbold"
                      style={{ fontSize: "initial" }}
                    >
                      <i
                        className="fa fa-envelope-o"
                        style={{ marginRight: "10px" }}
                      />
                      Send via WA
                    </span>
                  </div>
                </div>
              </div>
            )}
          <div
            onClick={() => {
              this.setState({
                showModalMakeSure: true,
                sendEmailOrSMS: "SMS"
              });
            }}
            className="form-group separatorBottom paddingside"
            style={{
              paddingTop: "5px",
              paddingBottom: "5px"
            }}
          >
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list">
                <span
                  className="smalltext labelspanbold"
                  style={{ fontSize: "initial" }}
                >
                  <i
                    className="fa fa-comment-o"
                    style={{ marginRight: "10px" }}
                  />
                  Send via SMS
                </span>
              </div>
            </div>
          </div>
          <div
            className="form-group separatorBottom paddingside"
            onClick={() => {
              this.setState({ isLoading: true });
              this.onDownloadTaskDetail(() => {
                this.onDownloadQuotation(this.state.selectedQuotation);
              });
              this.setState({ showModalButtonSendQuotation: false });
            }}
            style={{
              paddingTop: "5px",
              paddingBottom: "5px"
            }}
          >
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list">
                <span
                  className="smalltext labelspanbold"
                  style={{ fontSize: "initial" }}
                >
                  <i
                    className="fa fa-stack-overflow"
                    style={{ marginRight: "12px" }}
                  />
                  Download
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );

  ModalAddCopyQuotation = () => (
    <Modal
      styles={{
        overlay: {
          zIndex: 1050
        }
      }}
      showCloseIcon={false}
      open={this.state.showModalAddCopy}
      onClose={() => this.setState({ showModalAddCopy: false })}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-18px" }}
      >
        <div className="modal-header panel-heading">
          <h4 className="modal-title pull-left" id="exampleModalLabel">
            <b>Add or Copy?</b>
          </h4>
        </div>
        <div className="modal-body">
          Do you want to add new quotation or copy from existing quotation?
        </div>
        <div className="modal-footer">
          <div>
            <div className="col-xs-6">
              <button
                onClick={() => this.clickBtnAddCopyQuotation("ADD")}
                className="btn btn-warning form-control"
              >
                NEW
              </button>
            </div>
            <div className="col-xs-6">
              <button
                onClick={() => this.clickBtnAddCopyQuotation("COPY")}
                className="btn btn-info form-control"
              >
                COPY
              </button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );

  PersonalData = () => {
    return (
      <div className={this.disabledPersonalData() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#personaldata"
          >
            <span className="pull-left">PERSONAL DATA</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="personaldata"
          className="panel-body panel-padding0 collapse in"
        >
          <div
            className={
              this.state.IsNotErrorpersonalname ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nama Customer</span>
            <div className="col-xs-12">
              <input
                type="text"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                name="personalname"
                maxLength="50"
                disabled={
                  ((this.disabledFieldRenewal("personalname") ||
                    this.isBackToAO()) &&
                    !this.isNSAApproval()) ||
                  this.isDealNeedDocRep()
                }
                value={this.state.personalname}
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row marginTopBottomField">
            <div
              className={
                "col-xs-6 panel-body-list " +
                (this.state.IsNotErrorpersonaltanggallahir ? "has-error" : "")
              }
            >
              <span className="col-xs-12 labelspanbold">Tanggal Lahir</span>
              <div className="col-xs-12">
                <OtosalesDatePicker
                  maxDate={Util.convertDate()}
                  minDate={Util.convertDate().setFullYear(
                    Util.convertDate().getFullYear() - 70
                  )}
                  disabled={
                    this.disabledFieldRenewal("personaltanggallahir") ||
                    this.isBackToAO() ||
                    this.isDealNeedDocRep()
                  }
                  className="form-control"
                  name="personaltanggallahir"
                  selected={this.state.personaltanggallahir}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.onChangeFunctionDate}
                />
              </div>
            </div>
            <div
              className={
                "col-xs-6 panel-body-list " +
                (this.state.IsNotErrorpersonalJK ? "has-error" : "")
              }
            >
              <span className="col-xs-12 labelspanbold">Jenis Kelamin</span>
              <div className="col-xs-12">
                <OtosalesSelectFullviewrev
                  name="personalJK"
                  isDisabled={
                    this.disabledFieldRenewal("personalJK") ||
                    this.isBackToAO() ||
                    this.isDealNeedDocRep()
                  }
                  value={this.state.personalJK}
                  selectOption={this.state.personalJK}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.dataJK}
                  placeholder="Pilih Jenis Kelamin"
                />
              </div>
            </div>
          </div>
          <div
            className={
              "row " + (this.state.IsNotErrorpersonalalamat ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Alamat</span>
            <div className="col-xs-12">
              <input
                name="personalalamat"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                disabled={this.isBackToAO() || this.isDealNeedDocRep()}
                maxLength={152}
                value={this.state.personalalamat}
                onChange={this.onChangeFunction}
                type="text"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " + (this.state.IsNotErrorpersonalkodepos ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Kode Pos</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                name="personalkodepos"
                isDisabled={this.isBackToAO() || this.isDealNeedDocRep()}
                value={this.state.personalkodepos}
                selectOption={this.state.personalkodepos}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.dataKodePos}
                placeholder="Pilih Kode Pos"
              />
            </div>
          </div>
          <div
            className={
              "row " + (this.state.IsNotErrorpersonalemail ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Email</span>
            <div className="col-xs-12">
              <input
                type="email"
                name="personalemail"
                disabled={this.isBackToAO() || this.isDealNeedDocRep()}
                value={
                  this.isNotMaskingPhoneOrEmail(this.state.isFocusEmail) ||
                  this.isRoleCTIandRenewal()
                    ? this.state.personalemail
                    : Util.maskingEmail(this.state.personalemail)
                }
                onBlur={() => {
                  this.setState({
                    isFocusEmail: false
                  });
                }}
                onFocus={() => {
                  this.setState({
                    isFocusEmail: true
                  });
                }}
                onChange={this.onChangeFunction}
                maxLength="50"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " + (this.state.IsNotErrorpersonalhp ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">No Handphone</span>
            <div className="col-xs-12">
              {/* <NumberFormat
                className="form-control"
                disabled={this.isBackToAO() || this.isDealNeedDocRep()}
                onValueChange={value => {
                  Log.debugGroup("value", value);
                  this.setState({ personalhp: value.value });
                }}
                value={Util.maskingPhoneNumberv2(this.state.personalhp)}
                format="#### #### #### ####"
                prefix={""}
              /> */}
              <input
                name="personalhp"
                maxLength="30"
                disabled={
                  this.isBackToAO() ||
                  this.isDealNeedDocRep() ||
                  this.isRoleCTIandRenewal()
                }
                value={
                  this.isNotMaskingPhoneOrEmail(this.state.isFocusPhone)
                    ? this.state.personalhp
                    : Util.maskingPhoneNumberv2(
                        Util.clearPhoneNo(this.state.personalhp)
                      )
                }
                onBlur={() => {
                  this.setState({
                    isFocusPhone: false
                  });
                }}
                onFocus={() => {
                  this.setState({
                    isFocusPhone: true
                  });
                }}
                onChange={this.onChangeFunction}
                type="text"
                pattern="^[0-9]+$"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " +
              (this.state.IsNotErrorpersonalnomoridentitas ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Nomor Identitas</span>
            <div className="col-xs-12">
              {/* <NumberFormat
                maxLength={30}
                className="form-control"
                onValueChange={value => {
                  // Log.debugGroup(value);
                  this.setState({
                    personalnomoridentitas: value.value
                  });
                }}
                value={this.state.personalnomoridentitas}
                prefix={""}
              /> */}
              <input
                name="personalnomoridentitas"
                maxLength="30"
                disabled={
                  this.disabledFieldRenewal("personalnomoridentitas") ||
                  this.isBackToAO() ||
                  this.isDealNeedDocRep()
                }
                value={this.state.personalnomoridentitas}
                onChange={this.onChangeFunction}
                type="text"
                pattern="^[a-zA-Z0-9]+$"
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <input
                name="personalneeddocrep"
                checked={this.state.personalneeddocrep}
                onChange={this.onChangeFunction}
                disabled={this.state.IsRenewal && this.isBackToAO()}
                type="checkbox"
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">NEED DOC REP</label>
            </div>
          </div>
          <div
            className={
              "row " +
              (this.state.IsNotErrorpersonalamountrep ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Amount Rep</span>
            <div className="col-xs-12">
              <NumberFormat
                className="form-control"
                disabled={this.state.IsRenewal && this.isBackToAO()}
                onValueChange={value => {
                  this.setState({
                    personalamountrep:
                      value.floatValue == 0 ? "" : value.floatValue
                  });
                }}
                value={this.state.personalamountrep}
                thousandSeparator=","
                prefix={
                  Util.isNullOrEmpty(this.state.personalamountrep) ? "" : "Rp."
                }
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  PersonalDocuments = () => {
    return (
      <div>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#personaldocument"
          >
            <span className="pull-left">PERSONAL DOCUMENTS</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="personaldocument"
          className="panel-body panel-padding0 collapse in"
        >
          <div>
            {/* {!this.state.IsRenewal && (
              <React.Fragment> */}
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorKTP ? " has-error" : "") +
                (this.disabledPersonalDocuments() ? " disablediv" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataKTP}
                  name="KTP"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                KTP/KITAS{!this.state.IsRenewal ? "*" : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorSTNK ? " has-error" : "") +
                (this.disabledPersonalDocuments() ? " disablediv" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataSTNK}
                  name="STNK"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                STNK
                {!this.state.IsRenewal && this.state.vehicleusedcar ? "*" : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorBSTB ? " has-error" : "") +
                (this.disabledPersonalDocuments() ? " disablediv" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBSTB}
                  name="BSTB"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                BSTB
                {!this.state.IsRenewal && !this.state.vehicleusedcar ? "*" : ""}
              </center>
            </div>
            {/* </React.Fragment>
            )} */}
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCREP ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCREP}
                  name="DOCREP"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">DOC REP</center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorKONFIRMASICUST ? " has-error" : "") +
                (this.disabledPersonalDocuments() ? " disablediv" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataKONFIRMASICUST}
                  name="KONFIRMASICUST"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">KONFIRMASI CUSTOMER*</center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  CompanyData = () => {
    return (
      <div className={this.disabledCompanyData() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#companydata"
          >
            <span className="pull-left">COMPANY DATA</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div id="companydata" className="panel-body panel-padding0 collapse in">
          <div
            className={
              this.state.IsNotErrorcompanyname ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">
              Nama Perusahaan di NPWP
            </span>
            <div className="col-xs-12">
              <input
                name="companyname"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                maxLength={50}
                disabled={
                  this.disabledFieldRenewal("companyname") &&
                  !this.isNSAApproval()
                }
                value={this.state.companyname}
                onChange={this.onChangeFunction}
                type="text"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorcompanynpwpnumber ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nomor NPWP</span>
            <div className="col-xs-12">
              <NumberFormat
                className="form-control"
                onValueChange={value => {
                  this.setState({
                    companynpwpnumber: value.value
                  });
                }}
                disabled={this.disabledFieldRenewal("companynpwpnumber")}
                value={this.state.companynpwpnumber}
                format="##.###.###.#-###.### ###############"
                prefix={""}
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorcompanynpwpdata ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Tanggal NPWP</span>
            <div className="col-xs-12">
              <OtosalesDatePicker
                className="form-control"
                disabled={this.disabledFieldRenewal("companynpwpdata")}
                name="companynpwpdata"
                maxDate={Util.convertDate()}
                selected={this.state.companynpwpdata}
                dateFormat="dd mmm yyyy"
                onChange={this.onChangeFunctionDate}
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorcompanynpwpaddress ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Alamat NPWP</span>
            <div className="col-xs-12">
              <input
                type="text"
                disabled={this.disabledFieldRenewal("companynpwpaddress")}
                maxLength="152"
                value={this.state.companynpwpaddress}
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                name="companynpwpaddress"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          {/* <div className="row">
            <span className="col-xs-12 labelspanbold">SIUP Number</span>
            <div className="col-xs-12">
              <input
                type="text"
                value={this.state.companysiupnumber}
                name="companysiupnumber"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">PKP Number</span>
            <div className="col-xs-12">
              <input
                type="text"
                value={this.state.companypkpnumber}
                name="companypkpnumber"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">PKP Data</span>
            <div className="col-xs-12">
              <OtosalesDatePicker
                className="form-control"
                name="companypkpdata"
                selected={this.state.companypkpdata}
                dateFormat="dd mmm yyyy"
                onChange={this.onChangeFunctionDate}
              />
            </div>
          </div> */}
          <div
            className={
              this.state.IsNotErrorcompanypostalcode ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Kode Pos</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                name="companypostalcode"
                value={this.state.companypostalcode}
                selectOption={this.state.companypostalcode}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.dataKodePos}
                placeholder="Pilih Kode Pos"
              />
            </div>
          </div>
          {/* <div className="row">
            <span className="col-xs-12 labelspanbold">Office Number</span>
            <div className="col-xs-12">
              <NumberFormat
                className="form-control"
                onValueChange={value => {
                  this.setState({
                    companyofficenumber: value.value
                  });
                }}
                value={this.state.companyofficenumber}
                format="#### #### #### ####"
                prefix={""}
              />
            </div>
          </div> */}
          <div
            className={
              this.state.IsNotErrorcompanypdcname ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nama PIC</span>
            <div className="col-xs-12">
              <input
                type="text"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                maxLength={50}
                value={this.state.companypdcname}
                name="companypdcname"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorcompanypichp ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nomor Handphone PIC</span>
            <div className="col-xs-12">
              {/* <NumberFormat
                className="form-control"
                onValueChange={value => {
                  this.setState({
                    companypichp: value.value
                  });
                }}
                value={this.state.companypichp}
                format="#### #### #### ####"
                prefix={""}
              /> */}
              <input
                name="companypichp"
                maxLength="30"
                disabled={this.isRoleCTIandRenewal()}
                value={
                  this.isNotMaskingPhoneOrEmail(this.state.isFocusPhoneCompany)
                    ? this.state.companypichp
                    : Util.maskingPhoneNumberv2(
                        Util.clearPhoneNo(this.state.companypichp)
                      )
                }
                onBlur={() => {
                  this.setState({
                    isFocusPhoneCompany: false
                  });
                }}
                onFocus={() => {
                  this.setState({
                    isFocusPhoneCompany: true
                  });
                }}
                onChange={this.onChangeFunction}
                type="text"
                pattern="^[0-9]+$"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorcompanyemail ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Email PIC</span>
            <div className="col-xs-12">
              <input
                type="email"
                maxLength={50}
                name="companyemail"
                value={this.state.companyemail}
                value={
                  this.isNotMaskingPhoneOrEmail(
                    this.state.isFocusEmailCompany
                  ) || this.isRoleCTIandRenewal()
                    ? this.state.companyemail
                    : Util.maskingEmail(this.state.companyemail)
                }
                onBlur={() => {
                  this.setState({
                    isFocusEmailCompany: false
                  });
                }}
                onFocus={() => {
                  this.setState({
                    isFocusEmailCompany: true
                  });
                }}
                onChange={this.onChangeFunction}
                // pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
                className="form-control"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  CompanyDocuments = () => {
    return (
      <div>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#companydocument"
          >
            <span className="pull-left">COMPANY DOCUMENTS</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="companydocument"
          className="panel-body panel-padding0 collapse in"
        >
          <div>
            {/* {!this.state.IsRenewal && ( */}
            <React.Fragment>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.state.IsNotErrorNPWP ? " has-error" : "") +
                  (this.disabledCompanyDocuments() ? " disablediv" : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.state.dataNPWP}
                    name="NPWP"
                    handleDeleteImage={this.handleDeleteImage}
                    handleUploadImage={this.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  NPWP {this.state.IsRenewal ? "" : "*"}
                </center>
              </div>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.state.IsNotErrorSIUP ? " has-error" : "") +
                  (this.disabledCompanyDocuments() ? " disablediv" : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.state.dataSIUP}
                    name="SIUP"
                    handleDeleteImage={this.handleDeleteImage}
                    handleUploadImage={this.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">SIUP</center>
              </div>
            </React.Fragment>
            {/* )} */}
            {!this.state.IsRenewal && (
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.state.IsNotErrorBSTB ? " has-error" : "") +
                  (this.disabledCompanyDocuments() ? " disablediv" : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.state.dataBSTB}
                    name="BSTB"
                    handleDeleteImage={this.handleDeleteImage}
                    handleUploadImage={this.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">BSTB</center>
              </div>
            )}
            {/* {this.state.IsRenewal && ( */}
            <React.Fragment>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.state.IsNotErrorDOCREP ? " has-error" : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.state.dataDOCREP}
                    name="DOCREP"
                    handleDeleteImage={this.handleDeleteImage}
                    handleUploadImage={this.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">DOC REP</center>
              </div>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.state.IsNotErrorKONFIRMASICUST ? " has-error" : "") +
                  (this.disabledCompanyDocuments() ? " disablediv" : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.state.dataKONFIRMASICUST}
                    name="KONFIRMASICUST"
                    handleDeleteImage={this.handleDeleteImage}
                    handleUploadImage={this.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  KONFIRMASI CUSTOMER *
                </center>
              </div>
            </React.Fragment>
            {/* )} */}
          </div>
        </div>
      </div>
    );
  };

  VehicleData = () => {
    let account = JSON.parse(ACCOUNTDATA);
    return (
      <div className={this.disabledVehicleData() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#datakendaraan"
          >
            <span className="pull-left">DATA KENDARAAN</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="datakendaraan"
          className="panel-body panel-padding0 collapse in"
        >
          <div className="row">
            <div className="col-xs-12">
              <input
                name="personalpayercompany"
                checked={this.state.personalpayercompany}
                onChange={this.onChangeFunction}
                disabled={this.disableChecklistPayerCompany()}
                type="checkbox"
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">
                DIBAYARKAN OLEH PAYER COMPANY
              </label>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <input
                name="vehicleusedcar"
                checked={this.state.vehicleusedcar}
                onChange={this.onChangeFunction}
                type="checkbox"
                style={{ marginRight: "10px" }}
                disabled={this.disabledUsedCar() || this.isBackToAO()}
              />
              <label className="smallerCheckbox">USED CAR</label>
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehiclevehiclecode ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Vehicle</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                minHeight={40}
                isDisabled={
                  this.disabledFieldRenewal("vehiclevehiclecodeyear") ||
                  this.isBackToAO()
                }
                name="vehiclevehiclecodeyear"
                onSearchFunction={this.getVehicle}
                value={this.state.vehiclevehiclecodeyear}
                selectOption={this.state.vehiclevehiclecode}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehicle}
                placeholder="Pilih Kendaraan"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehiclebasiccoverage
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Basic Cover</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehiclebasiccoverage"
                isDisabled={this.isBackToAO()}
                value={this.state.vehiclebasiccoverage}
                selectOption={this.state.vehiclebasiccoverage}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.databasiccover}
                placeholder="Pilih Basic Cover"
              />
            </div>
          </div>
          <div className={"row " + (this.isBackToAO() ? "disablediv" : "")}>
            {(this.handleShowPeriod() || !this.state.IsRenewal) && (
              <div
                className={
                  this.state.IsNotErrorvehicleperiodfrom
                    ? "has-error col-xs-6"
                    : "col-xs-6"
                }
              >
                <span className="labelspanbold">Period From</span>
                <OtosalesDatePicker
                  // maxDate={Util.convertDate().setDate(Util.convertDate().getDate() + 30)}
                  minDate={Util.convertDate().setFullYear(
                    Util.convertDate().getFullYear() - 10
                  )}
                  className="form-control"
                  name="vehicleperiodfrom"
                  selected={this.state.vehicleperiodfrom}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.onChangeFunctionDate}
                />
              </div>
            )}
            {this.handleShowPeriod() && (
              <div
                className={
                  this.state.IsNotErrorvehicleperiodto
                    ? "has-error col-xs-6"
                    : "col-xs-6"
                }
              >
                <span className="labelspanbold">Period To</span>
                <OtosalesDatePicker
                  maxDate={(this.state.vehicleperiodfrom != null
                    ? Util.convertDate(
                        Util.formatDate(this.state.vehicleperiodfrom)
                      )
                    : Util.convertDate()
                  ).setDate(Util.convertDate().getFullYear() + 5)}
                  minDate={(this.state.vehicleperiodfrom != null
                    ? Util.convertDate(
                        Util.formatDate(this.state.vehicleperiodfrom)
                      )
                    : Util.convertDate()
                  ).setDate(Util.convertDate().getDate() + 1)}
                  className="form-control"
                  name="vehicleperiodto"
                  selected={this.state.vehicleperiodto}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.onChangeFunctionDate}
                />
              </div>
            )}
          </div>
          <div
            className={
              this.state.IsNotErrorvehicleusage ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Usage</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehicleusage"
                isDisabled={this.isBackToAO()}
                value={this.state.vehicleusage}
                selectOption={this.state.vehicleusage}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehicleusage}
                placeholder="Pilih Usage"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehiclecolor ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">
              Warna Kendaraan Sesuai BPKB
            </span>
            <div className="col-xs-12">
              <input
                type="text"
                disabled={
                  this.disabledFieldRenewal("vehiclecolor") || this.isBackToAO()
                }
                maxLength="50"
                value={this.state.vehiclecolor}
                name="vehiclecolor"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Product Type</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                isLoading={this.state.isLoading}
                minHeight={50}
                isDisabled={
                  this.disabledProductType()
                  // this.disabledFieldRenewal("vehicleInsuranceType") ||
                  // this.isBackToAO()
                }
                onClick={(callbackShow = () => {}) => {
                  this.getCheckVAPayment(
                    this.state.selectedQuotation,
                    () => {
                      this.onShowAlertModalInfoIsPaid();
                    },
                    () => {
                      callbackShow();
                    }
                  );
                }}
                name="vehicleInsuranceType"
                value={this.state.vehicleInsuranceType}
                selectOption={this.state.vehicleInsuranceType}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehicleproducttype}
                placeholder="Pilih Product Type"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehicleproductcode ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Product Code</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                isLoading={this.state.isLoading}
                minHeight={50}
                isDisabled={
                  this.disabledProductCode()
                  // (this.disabledFieldRenewal("vehicleproductcode") &&
                  //   !this.isNSAApproval()) ||
                  // this.isBackToAO()
                }
                onClick={(callbackShow = () => {}) => {
                  this.getCheckVAPayment(
                    this.state.selectedQuotation,
                    () => {
                      this.onShowAlertModalInfoIsPaid();
                    },
                    () => {
                      callbackShow();
                    }
                  );
                }}
                name="vehicleproductcode"
                value={this.state.vehicleproductcode}
                selectOption={this.state.vehicleproductcode}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehicleproductcode}
                placeholder="Pilih Product Code"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehiclesegmentcode ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Segment Code</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                onClick={callBack => {
                  callBack();
                }}
                minHeight={50}
                isDisabled={this.isBackToAO()}
                name="vehiclesegmentcode"
                value={this.state.vehiclesegmentcode}
                selectOption={this.state.vehiclesegmentcode}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehiclesegmentcode}
                placeholder="Pilih Segment Code"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              <span className="labelspanbold">Policy No</span>
              <input
                type="text"
                value={this.state.vehiclepolicyno}
                name="vehiclepolicyno"
                readOnly={true}
                // onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
            <div
              className={
                this.state.IsNotErrorvehicleplateno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Plate No</span>
              <input
                type="text"
                maxLength="9"
                disabled={this.isBackToAO()}
                value={
                  Util.isNullOrEmpty(this.state.vehicleplateno)
                    ? this.state.vehicleplateno
                    : (this.state.vehicleplateno + "").toUpperCase()
                }
                name="vehicleplateno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <div
              className={
                this.state.IsNotErrorvehiclechasisno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Chasis No</span>
              <input
                type="text"
                value={
                  Util.isNullOrEmpty(this.state.vehiclechasisno)
                    ? this.state.vehiclechasisno
                    : (this.state.vehiclechasisno + "").toUpperCase()
                }
                maxLength="30"
                name="vehiclechasisno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.onChangeFunction}
                className="form-control"
                disabled={
                  this.disabledFieldRenewal("vehiclechasisno") ||
                  this.isBackToAO()
                }
              />
            </div>
            <div
              className={
                this.state.IsNotErrorvehicleengineno
                  ? "has-error col-xs-6"
                  : "col-xs-6"
              }
            >
              <span className="labelspanbold">Engine No</span>
              <input
                type="text"
                value={
                  Util.isNullOrEmpty(this.state.vehicleengineno)
                    ? this.state.vehicleengineno
                    : (this.state.vehicleengineno + "").toUpperCase()
                }
                maxLength="30"
                name="vehicleengineno"
                pattern="^[a-zA-Z0-9]+$"
                onChange={this.onChangeFunction}
                className="form-control"
                disabled={
                  this.disabledFieldRenewal("vehicleengineno") ||
                  this.isBackToAO()
                }
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorvehicleregion ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Region</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="vehicleregion"
                isDisabled={
                  // this.disabledFieldRenewal("vehicleregion") ||
                  // this.isBackToAO()
                  true
                }
                value={this.state.vehicleregion}
                selectOption={this.state.vehicleregion}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datavehicleregion}
                placeholder="Pilih Region"
              />
            </div>
          </div>
          {this.state.IsSalesmanDealerEnable && (
            <React.Fragment>
              <div
                className={
                  (this.state.IsSalesmanDealerEnable
                    ? "row"
                    : "row disablediv") +
                  " " +
                  (this.state.IsNotErrorvehicledealer ? "has-error" : "")
                }
              >
                <span className="col-xs-12 labelspanbold">Dealer Name</span>
                <div className="col-xs-12">
                  <OtosalesSelectFullviewv2
                    isDisabled={this.disableDealerVehicle()}
                    name="vehicledealer"
                    value={this.state.vehicledealer}
                    selectOption={this.state.vehicledealer}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.datavehicledealer}
                    placeholder="Pilih Dealer Name"
                  />
                </div>
              </div>
              <div
                className={
                  (this.state.IsSalesmanDealerEnable
                    ? "row"
                    : "row disablediv") +
                  " " +
                  (this.state.IsNotErrorvehiclesalesman ? "has-error" : "")
                }
              >
                <span className="col-xs-12 labelspanbold">
                  Salesman Dealer Name
                </span>
                <div className="col-xs-12">
                  {/* <input
                  type="text"
                  value={this.state.vehiclesalesmanname}
                  name="vehiclesalesmanname"
                  readOnly
                  className="form-control"
                /> */}
                  <OtosalesSelectFullviewv2
                    isDisabled={this.disableSalesmanVehicle()}
                    name="vehiclesalesman"
                    value={this.state.vehiclesalesman}
                    selectOption={this.state.vehiclesalesman}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.datavehiclesalesman}
                    placeholder="Pilih Salesman Name"
                  />
                </div>
              </div>
              <div className="row">
                <div
                  className={
                    "col-xs-6 " +
                    (this.state.IsNotErrorvehiclenamebank ? "has-error" : "")
                  }
                >
                  <span className="labelspanbold">Nama Bank</span>
                  <input
                    type="text"
                    value={this.state.vehiclenamebank}
                    name="vehiclenamebank"
                    readOnly
                    className="form-control"
                  />
                </div>
                <div
                  className={
                    "col-xs-6 " +
                    (this.state.IsNotErrorvehiclenomorrek ? "has-error" : "")
                  }
                >
                  <span className="labelspanbold">Nomor Rekening</span>
                  <input
                    type="text"
                    value={this.state.vehiclenomorrek}
                    name="vehiclenomorrek"
                    readOnly
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
          {Util.isNullOrEmpty(account.UserInfo.User.Channel) ||
          account.UserInfo.User.Channel != "EXT" ? (
            ""
          ) : (
            <div className="row">
              <span className="col-xs-12 labelspanbold">Agency</span>
              <div className="col-xs-12">
                <input
                  type="text"
                  value={this.state.vehicleagency}
                  name="vehicleagency"
                  readOnly
                  className="form-control"
                />
              </div>
            </div>
          )}
          {Util.isNullOrEmpty(account.UserInfo.User.Channel) ||
          account.UserInfo.User.Channel != "EXT" ? (
            ""
          ) : (
            <div className="row">
              <span className="col-xs-12 labelspanbold">Upliner</span>
              <div className="col-xs-12">
                <input
                  type="text"
                  value={this.state.vehicleupliner}
                  name="vehicleupliner"
                  readOnly
                  className="form-control"
                />
              </div>
            </div>
          )}
          <div
            className={
              this.state.IsNotErrorvehicletotalsuminsured
                ? "has-error row"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Total Sum Insured</span>
            <div className="col-xs-12">
              <NumberFormat
                className="form-control"
                disabled={this.isBackToAO()}
                onValueChange={value => {
                  // if(!Util.isNullOrEmpty(this.state.vehicletotalsuminsuredtemp) &&(this.state.vehicletotalsuminsuredtemp != 0)){
                  //   this.checkMaxPriceTSI(value.floatValue, () => {
                  this.setState(
                    {
                      vehicletotalsuminsuredtemp: value.floatValue
                    },
                    () => {
                      if (this.isLoadingDataFirst()) {
                        this.trigerRateCalculation();
                      }
                      if (
                        !Util.isNullOrEmpty(
                          this.state.vehicletotalsuminsured
                        ) &&
                        this.state.vehicletotalsuminsured != 0
                      ) {
                        setTimeout(() => {
                          this.checkMaxPriceTSI(
                            this.state.vehicletotalsuminsuredtemp
                          );
                        }, 3000);
                      }
                    }
                  );
                  //   })
                  // }
                }}
                value={this.state.vehicletotalsuminsuredtemp}
                thousandSeparator=","
                prefix={"Rp."}
              />
            </div>
          </div>
          {/* {this.state.IsRenewal && this.hideVehicleOriginalDefect() && ( */}
          {this.state.IsRenewal && Util.isNullOrEmpty(this.state.SurveyNo) && (
            <React.Fragment>
              <div className="row">
                <span className="col-xs-12 labelspanbold">Cacat Semula</span>
                <div className="col-xs-12">
                  <textarea
                    class="form-control"
                    rows="5"
                    value={this.state.vehiclecacatsemula}
                    readOnly
                    style={{ resize: "none" }}
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-12 labelspanbold">No Cover</span>
                <div className="col-xs-12">
                  <textarea
                    class="form-control"
                    rows="5"
                    value={this.state.vehiclenocover}
                    readOnly
                    style={{ resize: "none" }}
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-12 labelspanbold">Accesories</span>
                <div className="col-xs-12">
                  <textarea
                    class="form-control"
                    rows="5"
                    value={this.state.vehicleaccesories}
                    readOnly
                    style={{ resize: "none" }}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12">
                  <input
                    name="vehicleisordefectrepair"
                    checked={this.state.vehicleisordefectrepair}
                    onChange={this.onChangeFunction}
                    type="checkbox"
                    disabled={this.isBackToAO()}
                    style={{ marginRight: "10px" }}
                  />
                  <label className="smallerCheckbox">
                    ADA PERBAIKAN CACAT SEMULA
                  </label>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12">
                  <input
                    name="vehicleisaccessorieschange"
                    checked={this.state.vehicleisaccessorieschange}
                    onChange={this.onChangeFunction}
                    disabled={this.isBackToAO()}
                    type="checkbox"
                    style={{ marginRight: "10px" }}
                  />
                  <label className="smallerCheckbox">
                    ADA PENAMBAHAN AKSESORIS TAMBAHAN
                  </label>
                </div>
              </div>
            </React.Fragment>
          )}
          {this.state.IsRenewal && (
            <React.Fragment>
              <div className="row">
                <div className="col-xs-12">
                  <input
                    name="vehicleispolicyissuedbeforpaying"
                    checked={this.state.vehicleispolicyissuedbeforpaying}
                    onChange={this.onChangeFunction}
                    disabled={!this.state.IsRenewal && this.isBackToAO()}
                    type="checkbox"
                    style={{ marginRight: "10px" }}
                  />
                  <label className="smallerCheckbox">
                    POLIS TERBIT SEBELUM BAYAR ATAU BAYAR DENGAN CC/DEBIT
                  </label>
                </div>
              </div>
              <div className="row" style={{ paddingLeft: "20px" }}>
                <span className="col-xs-12 labelspanbold">Remarks</span>
                <div className="col-xs-12">
                  <input
                    type="text"
                    name="vehicleremarks"
                    disabled={this.isBackToAO()}
                    value={this.state.vehicleremarks}
                    onChange={this.onChangeFunction}
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
        </div>
      </div>
    );
  };

  ExtendedCover = () => {
    return (
      <div className={this.disabledExtendedCover() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#extendedcover"
          >
            <span className="pull-left">EXTENDED COVER</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="extendedcover"
          className="panel-body panel-padding0 collapse in"
        >
          <div>
            {this.detectComprePeriod() && this.state.IsTPLEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsTPLChecked}
                        name="IsTPLChecked"
                        value={this.state.IsTPLChecked}
                        onChange={event => {
                          this.setDialogPeriod(4);
                        }}
                      />
                      TJH
                    </label>
                  </div>
                  {/* <OtosalesSelectFullviewv2
                    // isDisabled={this.state.IsTPLSIEnabled == 1 ? false : true}
                    isDisabled={true}
                    name="TPLCoverageId"
                    value={this.state.TPLCoverageId}
                    selectOption={this.state.TPLCoverageId}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.dataTPLSI}
                    placeholder=""
                  /> */}
                  <NumberFormat
                    allowNegative={false}
                    disabled={true}
                    className="form-control"
                    value={this.state.TPLSICOVER}
                    thousandSeparator={true}
                    prefix={""}
                  />
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {Util.formatMoney(this.state.TPLPremi, 0)}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsSRCCEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsSRCCChecked}
                        name="IsSRCCChecked"
                        value={this.state.IsSRCCChecked}
                        onChange={event => {
                          this.setDialogPeriod(0);
                        }}
                        disabled={
                          this.state.IsSRCCCheckedEnable == 1 ? false : true
                        }
                      />
                      STRIKE, RIOT, COMMOTION (SRCC)
                    </label>
                  </div>
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {Util.formatMoney(this.state.SRCCPremi, 0)}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsFLDEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsFLDChecked}
                        name="IsFLDChecked"
                        value={this.state.IsFLDChecked}
                        onChange={event => {
                          this.setDialogPeriod(1);
                        }}
                        disabled={
                          this.state.IsFLDCheckedEnable == 1 ? false : true
                        }
                      />
                      FLOOD &amp; WINDSTROM
                    </label>
                  </div>
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {" "}
                    {Util.formatMoney(this.state.FLDPremi, 0)}{" "}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsETVEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsETVChecked}
                        name="IsETVChecked"
                        value={this.state.IsETVChecked}
                        onChange={event => {
                          this.setDialogPeriod(2);
                        }}
                        disabled={
                          this.state.IsETVCheckedEnable == 1 ? false : true
                        }
                      />
                      EARTHQUAKE, TSUNAMI, VOLCANO ERUPTION
                    </label>
                  </div>
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {Util.formatMoney(this.state.ETVPremi, 0)}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsTSEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        name="IsTSChecked"
                        checked={this.state.IsTSChecked}
                        value={this.state.IsTSChecked}
                        onChange={event => {
                          this.setDialogPeriod(3);
                        }}
                        disabled={
                          this.state.IsTRSCheckedEnable == 1 ? false : true
                        }
                      />
                      TERRORISM &amp; SABOTAGE
                    </label>
                  </div>
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {" "}
                    {Util.formatMoney(this.state.TSPremi, 0)}{" "}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsPADRVREnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsPADRVRChecked}
                        name="IsPADRVRChecked"
                        value={this.state.IsPADRVRChecked}
                        onChange={event => {
                          this.setDialogPeriod(5);
                        }}
                        disabled={
                          this.state.IsPADRIVERCheckedEnable == 1 ? false : true
                        }
                      />
                      PA DRIVER
                    </label>
                  </div>
                  <NumberFormat
                    allowNegative={false}
                    disabled={true}
                    className="form-control"
                    // onValueChange={value => {
                    //   this.setState(
                    //     { PADRVCOVER: value.floatValue },
                    //     () => {
                    //       this.rateCalculateTPLPAPASSPADRV(
                    //         "PADRVCOVER"
                    //       );
                    //     }
                    //   );
                    // }}
                    value={this.state.PADRVCOVER}
                    thousandSeparator={true}
                    prefix={""}
                  />
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {" "}
                    {Util.formatMoney(this.state.PADRVRPremi, 0)}{" "}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsPAPASSEnabled == 0 ? (
              ""
            ) : (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsPAPASSChecked}
                        name="IsPAPASSChecked"
                        value={this.state.IsPAPASSChecked}
                        onChange={event => {
                          this.setDialogPeriod(6);
                        }}
                        disabled={
                          this.state.IsPAPASSCheckedEnable == 1 ? false : true
                        }
                      />
                      PA PASSENGER
                    </label>
                  </div>
                  <div className="row">
                    <div className="col-md-3 col-xs-6 panel-body-list">
                      <div className="col-xs-8" style={{ paddingRight: "0px" }}>
                        <input
                          className="form-control"
                          type="number"
                          name="PASSCOVER"
                          value={this.state.PASSCOVER}
                          // onChange={event => {
                          //   this.onChangeFunction(event);
                          // }}
                          readOnly={true}
                        />
                      </div>
                      <div className="col-xs-4">@</div>
                    </div>
                    <div
                      className="col-md-9 col-xs-6 panel-body-list"
                      style={{
                        marginLeft: "-5px",
                        paddingRight: "10px"
                      }}
                    >
                      <NumberFormat
                        disabled={true}
                        className="form-control"
                        // onValueChange={value => {
                        //   this.setState(
                        //     { PAPASSICOVER: value.floatValue },
                        //     () => {
                        //       this.rateCalculateTPLPAPASSPADRV(
                        //         "PAPASSICOVER"
                        //       );
                        //     }
                        //   );
                        // }}
                        value={this.state.PAPASSICOVER}
                        thousandSeparator={true}
                        prefix={""}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {Util.formatMoney(this.state.PAPASSPremi, 0)}
                  </p>
                </div>
              </div>
            )}
            {this.state.IsACCESSEnabled == 1 && (
              <div className="row">
                <div className="col-xs-8 col-md-10">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.IsACCESSChecked}
                        name="IsACCESSChecked"
                        onChange={event => {
                          this.setDialogPeriod(7);
                        }}
                      />
                      ACCESSORY
                    </label>
                    <i
                      className="fa fa-info-circle fa-2x"
                      style={{
                        marginLeft: "5px",
                        opacity: "0.3"
                      }}
                      onClick={() => {
                        this.ShowAccessoryInfo();
                      }}
                    />
                  </div>
                  <NumberFormat
                    allowNegative={false}
                    disabled={true}
                    className="form-control"
                    // onValueChange={value => {
                    //   this.setState(
                    //     { ACCESSCOVER: value.floatValue },
                    //     () => {
                    //       this.rateCalculateTPLPAPASSPADRV(
                    //         "ACCESSCOVER"
                    //       );
                    //     }
                    //   );
                    // }}
                    value={this.state.ACCESSCOVER}
                    thousandSeparator={true}
                    prefix={""}
                  />
                </div>
                <div className="col-xs-4 col-md-2 text-right">
                  <p style={{ marginTop: "10px" }}>
                    {Util.formatMoney(this.state.ACCESSPremi, 0)}
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  PaymentInfo = () => {
    return (
      <div>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#paymentinfo"
          >
            <span className="pull-left">PAYMENT INFO</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div id="paymentinfo" className="panel-body panel-padding0 collapse in">
          <div className="row">
            <span className="col-xs-4 labelspanbold">Due Date Pembayaran</span>
            <div className="col-xs-8">
              <input
                type="text"
                name="paymentduedate"
                value={this.state.paymentduedate || ""}
                // onChange={this.onChangeFunction}
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
          {Util.isNullOrEmpty(this.state.paymentnorek) && (
            <React.Fragment>
              <div className="row">
                <span className="col-xs-4 labelspanbold">VA Permata</span>
                <div className="col-xs-8">
                  <input
                    type="text"
                    name="paymentvapermata"
                    value={this.state.paymentvapermata}
                    // onChange={this.onChangeFunction}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-4 labelspanbold">VA Mandiri</span>
                <div className="col-xs-8">
                  <input
                    type="text"
                    name="paymentvamandiri"
                    value={this.state.paymentvamandiri}
                    // onChange={this.onChangeFunction}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
              <div className="row">
                <span className="col-xs-4 labelspanbold">VA BCA</span>
                <div className="col-xs-8">
                  <input
                    type="text"
                    name="paymentvabca"
                    value={this.state.paymentvabca}
                    // onChange={this.onChangeFunction}
                    readOnly={true}
                    className="form-control"
                  />
                </div>
              </div>
            </React.Fragment>
          )}
          {!Util.isNullOrEmpty(this.state.paymentnorek) && (
            <div className="row">
              <span className="col-xs-4 labelspanbold">Nomor Rekening</span>
              <div className="col-xs-8">
                <input
                  type="text"
                  name="paymentnorek"
                  value={this.state.paymentnorek}
                  readOnly={true}
                  className="form-control"
                />
              </div>
            </div>
          )}
          {this.state.paymentisvaactive == false && (
            <div className="row" style={{ marginBottom: "5px" }}>
              <div className="col-xs-12">
                <div className="pull-right">
                  <a
                    className="btn btn-success"
                    onClick={() => this.onClickReactivedVA()}
                  >
                    Reactive VA
                  </a>
                </div>
              </div>
            </div>
          )}
          <div className="row">
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBUKTIBAYAR}
                  name="BUKTIBAYAR"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBUKTIBAYAR2}
                  name="BUKTIBAYAR2"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBUKTIBAYAR3}
                  name="BUKTIBAYAR3"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBUKTIBAYAR4}
                  name="BUKTIBAYAR4"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
            <div className="col-xs-4 col-md-2 panel-body-list">
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataBUKTIBAYAR5}
                  name="BUKTIBAYAR5"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">Bukti Bayar</center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  PolicyDelivery2 = () => {
    return (
      <div className={this.disabledPolicyDelivery() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#surveyschedule"
          >
            <span className="pull-left">ALAMAT PENGIRIMAN POLICY</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="surveyschedule"
          className="panel-body panel-padding0 collapse in"
        >
          <div
            className={
              this.state.IsNotErrorpolicysentto ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Dikirim ke</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="policysentto"
                value={this.state.policysentto}
                selectOption={this.state.policysentto}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datapolicysentto}
                placeholder="Pilih Dikirim ke"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicyname ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nama</span>
            <div className="col-xs-12">
              {!Util.stringArrayContains(this.state.policysentto + "", [
                "BR",
                "GC"
              ]) && (
                <input
                  type="text"
                  name="policyname"
                  maxLength="50"
                  value={this.state.policyname}
                  onChange={this.onChangeFunction}
                  className="form-control"
                />
              )}
              {Util.stringArrayContains(this.state.policysentto + "", [
                "GC"
              ]) && (
                <OtosalesSelectFullviewv2
                  minHeight={40}
                  name="policyname"
                  onSearchFunction={this.getNameOnPolicyDelivery}
                  value={this.state.policyname}
                  selectOption={this.state.policyname}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.datapolicyaddress}
                  placeholder="Pilih Name Policy Delivered"
                />
              )}
              {Util.stringArrayContains(this.state.policysentto + "", [
                "BR"
              ]) && (
                <OtosalesSelectFullviewrev
                  minHeight={40}
                  name="policyname"
                  value={this.state.policyname}
                  selectOption={this.state.policyname}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.datapolicyaddress}
                  placeholder="Pilih Name Policy Delivered"
                />
              )}
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicyaddress ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Alamat</span>
            <div className="col-xs-12">
              <input
                type="text"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                name="policyaddress"
                disabled={Util.stringArrayContains(
                  this.state.policysentto + "",
                  ["BR", "GC"]
                )}
                maxLength="152"
                value={this.state.policyaddress}
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicykodepos ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Kode Pos</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                name="policykodepos"
                isDisabled={
                  Util.stringArrayContains(this.state.policysentto + "", [
                    "BR",
                    "GC"
                  ])
                    ? true
                    : false
                }
                value={this.state.policykodepos}
                selectOption={this.state.policykodepos}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.dataKodePos}
                placeholder="Pilih Kode Pos"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  PolicyDelivery = () => {
    return (
      <div className={this.disabledPolicyDelivery() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#surveyschedule"
          >
            <span className="pull-left">ALAMAT PENGIRIMAN POLICY</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="surveyschedule"
          className="panel-body panel-padding0 collapse in"
        >
          <div
            className={
              this.state.IsNotErrorpolicysentto ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Dikirim ke</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewrev
                name="policysentto"
                value={this.state.policysentto}
                selectOption={this.state.policysentto}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.datapolicysentto}
                placeholder="Pilih Dikirim ke"
              />
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicyname ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nama</span>
            <div className="col-xs-12">
              {/* {!Util.stringArrayContains(this.state.policysentto + "", [
                "BR",
                "GC"
              ]) && ( */}
              <input
                type="text"
                name="policyname"
                disabled={Util.stringArrayContains(
                  this.state.policysentto + "",
                  [
                    "CS"
                    // "GC"
                  ]
                )}
                maxLength="50"
                value={this.state.policyname || ""}
                onChange={this.onChangeFunction}
                className="form-control"
              />
              {/* )} */}
              {/* {Util.stringArrayContains(this.state.policysentto + "", [
                "GC"
              ]) && (
                  <OtosalesSelectFullviewv2
                    minHeight={40}
                    name="policyname"
                    onSearchFunction={this.getNameOnPolicyDelivery}
                    value={this.state.policyname}
                    selectOption={this.state.policyname}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.datapolicyaddress}
                    placeholder="Pilih Name Policy Delivered"
                  />
                )}
              {Util.stringArrayContains(this.state.policysentto + "", [
                "BR"
              ]) && (
                  <OtosalesSelectFullviewrev
                    minHeight={40}
                    name="policyname"
                    value={this.state.policyname}
                    selectOption={this.state.policyname}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.datapolicyaddress}
                    placeholder="Pilih Name Policy Delivered"
                  />
                )} */}
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicyaddress ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Alamat</span>
            <div className="col-xs-12">
              {Util.stringArrayContains(this.state.policysentto + "", [
                "BR",
                "CS",
                "OH"
              ]) && (
                <input
                  type="text"
                  pattern="^[a-zA-Z0-9 ,-.\/]+$"
                  name="policyaddress"
                  disabled={Util.stringArrayContains(
                    this.state.policysentto + "",
                    [
                      "BR",
                      "CS"
                      // "GC"
                    ]
                  )}
                  maxLength="152"
                  value={this.state.policyaddress}
                  onChange={this.onChangeFunction}
                  className="form-control"
                />
              )}
              {!Util.stringArrayContains(this.state.policysentto + "", [
                "BR",
                "CS",
                "OH"
              ]) && (
                <OtosalesSelectFullviewv2
                  minHeight={40}
                  name="policyaddress"
                  onSearchFunction={this.getNameOnPolicyDelivery}
                  value={this.state.policyaddress}
                  selectOption={this.state.policyaddress}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.datapolicyaddress}
                  placeholder="Pilih Garda Center"
                />
              )}
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrorpolicykodepos ? "has-error row" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Kode Pos</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                name="policykodepos"
                isDisabled={
                  Util.stringArrayContains(this.state.policysentto + "", [
                    "BR",
                    "GC",
                    "CS"
                  ])
                    ? true
                    : false
                }
                value={this.state.policykodepos}
                selectOption={this.state.policykodepos}
                onOptionsChange={this.onOptionSelect2Change}
                options={this.state.dataKodePos}
                placeholder="Pilih Kode Pos"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  SurveySchedule = () => {
    return (
      <div className={this.disabledSurveySchedule() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#alamatpengirimanpolicy"
          >
            <span className="pull-left">SURVEY SCHEDULE</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="alamatpengirimanpolicy"
          className="panel-body panel-padding0 collapse in"
        >
          <div className="row">
            <div className="col-xs-12">
              <input
                name="surveyneed"
                checked={this.state.surveyneed}
                onChange={this.onChangeFunction}
                type="checkbox"
                disabled={this.disabledNeedSurvey()}
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">NEED SURVEY</label>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <input
                name="surveynsaskipsurvey"
                checked={this.state.surveynsaskipsurvey}
                disabled={this.state.FollowUpInfo == 59 ? true : false}
                onChange={this.onChangeFunction}
                type="checkbox"
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">NSA SKIP SURVEY</label>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <input
                name="surveychecklistmanual"
                checked={this.state.surveychecklistmanual}
                onChange={this.onChangeFunction}
                type="checkbox"
                style={{ marginRight: "10px" }}
              />
              <label className="smallerCheckbox">
                SUDAH SURVEY ADDITIONAL ORDER
              </label>
            </div>
          </div>
          <div className={this.state.surveyneed ? "" : "disablediv"}>
            <div
              className={
                this.state.IsNotErrorsurveykota ? "has-error row" : "row"
              }
            >
              <span className="col-xs-12 labelspanbold">Kota Survei</span>
              <div className="col-xs-12">
                <OtosalesSelectFullviewrev
                  name="surveykota"
                  value={this.state.surveykota}
                  selectOption={this.state.surveykota}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.datasurveykota}
                  placeholder="Pilih Kota Survei"
                />
              </div>
            </div>
            <div
              className={
                this.state.IsNotErrorsurveylokasi ? "has-error row" : "row"
              }
            >
              <span className="col-xs-12 labelspanbold">Lokasi Survei</span>
              <div
                className={
                  Util.isNullOrEmpty(this.state.surveykota)
                    ? "col-xs-12 disablediv"
                    : "col-xs-12"
                }
              >
                <OtosalesSelectFullviewrev
                  name="surveylokasi"
                  value={this.state.surveylokasi}
                  selectOption={this.state.surveylokasi}
                  onOptionsChange={this.onOptionSelect2Change}
                  options={this.state.datasurveylokasi}
                  placeholder="Pilih Lokasi Survei"
                />
              </div>
            </div>
            {this.state.surveylokasi == "Others" && (
              <React.Fragment>
                <div
                  className={
                    this.state.IsNotErrorsurveyalamat ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Alamat Survei</span>
                  <div className="col-xs-12">
                    <input
                      type="text"
                      name="surveyalamat"
                      pattern="^[a-zA-Z0-9 ,-.\/]+$"
                      value={this.state.surveyalamat}
                      onChange={this.onChangeFunction}
                      className="form-control"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.state.IsNotErrorsurveykodepos ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">
                    Kode Pos Survei
                  </span>
                  <div className="col-xs-12">
                    <OtosalesSelectFullviewv2
                      name="surveykodepos"
                      value={this.state.surveykodepos}
                      selectOption={this.state.surveykodepos}
                      onOptionsChange={this.onOptionSelect2Change}
                      options={this.state.dataKodePosSurvey}
                      placeholder="Pilih Kode Pos"
                    />
                  </div>
                </div>
              </React.Fragment>
            )}
            <div className="row">
              <div
                className={
                  this.state.IsNotErrorsurveytanggal
                    ? "has-error col-xs-6 panel-body-list"
                    : "col-xs-6 panel-body-list"
                }
              >
                <span className="col-xs-12 labelspanbold">
                  Tanggal Survei
                  <OtosalesDatePicker
                    className="form-control"
                    minDate={Util.convertDate().setDate(
                      Util.convertDate().getDate() + 1
                    )}
                    maxDate={Util.convertDate().setDate(
                      Util.convertDate().getDate() + 13
                    )}
                    // maxDate={Util.convertDate().setDate(Util.convertDate().getDate() + 16)}
                    // maxDate={Util.addDateWithoutWeekend(Util.convertDate(), 13)}
                    // filterDate={Util.isWeekday}
                    filterDate={this.surveyDateExclude}
                    name="surveytanggal"
                    selected={this.state.surveytanggal}
                    dateFormat="dd/mm/yyyy"
                    onChange={this.onChangeFunctionDate}
                  />
                </span>
                <div className="col-xs-12" />
              </div>
              {/* {this.state.surveytanggal && ( */}
              <div
                className={
                  (this.state.IsNotErrorsurveywaktu
                    ? "has-error col-xs-6 panel-body-list"
                    : "col-xs-6 panel-body-list") +
                  " " +
                  (Util.isNullOrEmpty(this.state.surveytanggal)
                    ? "disablediv"
                    : "")
                }
              >
                <span className="col-xs-12 labelspanbold">Waktu Survei</span>
                <div className="col-xs-12">
                  <OtosalesSelectFullviewrev
                    name="surveywaktu"
                    value={this.state.surveywaktu}
                    selectOption={this.state.surveywaktu}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.state.datasurveywaktu}
                    placeholder="Pilih Waktu Survei"
                  />
                </div>
              </div>
              {/* )} */}
            </div>
          </div>
        </div>
      </div>
    );
  };

  SurveyData = () => {
    return (
      <div>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#surveydata"
          >
            <span className="pull-left">SURVEI DATA</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div id="surveydata" className="panel-body panel-padding0 collapse in">
          <div className="row">
            <span className="col-xs-12 labelspanbold">No Cover</span>
            <div className="col-xs-12">
              <textarea
                value={this.state.surveydatanocover}
                name="surveydatanocover"
                type="text"
                readOnly={true}
                className="form-control"
                rows={3}
                style={{ resize: "none", padding: "0px" }}
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Original Defect</span>
            <div className="col-xs-12">
              <textarea
                value={this.state.surveydataoriginaldefect}
                name="surveydataoriginaldefect"
                type="text"
                readOnly={true}
                className="form-control"
                rows={3}
                style={{ resize: "none", padding: "0px" }}
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Accessories</span>
            <div className="col-xs-12">
              <textarea
                value={this.state.surveydataaccessories}
                name="surveydataaccessories"
                type="text"
                readOnly={true}
                className="form-control"
                rows={3}
                style={{ resize: "none", padding: "0px" }}
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Survey Status</span>
            <div className="col-xs-12">
              <input
                value={this.state.surveydatastatus}
                name="surveydatastatus"
                type="text"
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">
              Surveyor Recommendation
            </span>
            <div className="col-xs-12">
              <input
                value={this.state.surveydatarecommendation}
                name="surveydatarecommendation"
                type="text"
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Remarks</span>
            <div className="col-xs-12">
              <input
                value={this.state.surveydataremarks}
                name="surveydataremarks"
                type="text"
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Surveyor</span>
            <div className="col-xs-12">
              <input
                value={this.state.surveydatasurveyor}
                name="surveydatasurveyor"
                type="text"
                readOnly={true}
                className="form-control"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  SurveyDocuments = () => {
    return (
      <div>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#suveydocuments"
          >
            <span className="pull-left">SURVEY DOCUMENTS</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div
          id="suveydocuments"
          className="panel-body panel-padding0 collapse in"
        >
          <div className="row">
            <div className="col-xs-12">
              <label
                style={{
                  color: "#4ac6e9",
                  width: "100px",
                  lineHeight: "40px"
                }}
                onClick={() => {
                  this.onClickSurveyDocument();
                }}
              >
                VIEW
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  };

  Documents = () => {
    return (
      <div className={this.disabledDocuments() ? "disablediv" : ""}>
        <div className="row backgroundgrey labelspanbold labelfontgrey">
          <div
            className="col-xs-12"
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            data-toggle="collapse"
            data-target="#documents"
          >
            <span className="pull-left">DOCUMENTS</span>
            <span className="pull-right collapsebutton" />
          </div>
        </div>
        <div id="documents" className="panel-body panel-padding0 collapse in">
          <div className="row">
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorSPPAKB ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataSPPAKB}
                  name="SPPAKB"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">SPPAKB/Form Renewal*</center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorFAKTUR ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataFAKTUR}
                  name="FAKTUR"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">FAKTUR</center>
            </div>
          </div>
          <div className="row">
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCNSA1 ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCNSA1}
                  name="DOCNSA1"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                Dokumen NSA
                {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                  ? "*"
                  : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCNSA2 ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCNSA2}
                  name="DOCNSA2"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                Dokumen NSA
                {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                  ? "*"
                  : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCNSA3 ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCNSA3}
                  name="DOCNSA3"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                Dokumen NSA
                {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                  ? "*"
                  : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCNSA4 ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCNSA4}
                  name="DOCNSA4"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                Dokumen NSA
                {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                  ? "*"
                  : ""}
              </center>
            </div>
            <div
              className={
                "col-xs-4 col-md-2 panel-body-list" +
                (this.state.IsNotErrorDOCNSA5 ? " has-error" : "")
              }
            >
              <div className="text-center">
                <OtosalesDropzonev2
                  src={this.state.dataDOCNSA5}
                  name="DOCNSA5"
                  handleDeleteImage={this.handleDeleteImage}
                  handleUploadImage={this.handleUploadImage}
                />
              </div>
              <center className="smallerCheckbox">
                Dokumen NSA
                {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                  ? "*"
                  : ""}
              </center>
            </div>
          </div>
          <div
            className={
              this.state.IsNotErrordocumentsremarkstosa
                ? "row has-error"
                : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">
              Remarks to SA
              {this.state.FollowUpStatus == 3 && this.state.FollowUpInfo == 46
                ? "*"
                : ""}
            </span>
            <div className="col-xs-12">
              <input
                type="text"
                name="documentsremarkstosa"
                maxLength="200"
                pattern="^[a-zA-Z0-9 ,-.\/':;?]+$"
                value={this.state.documentsremarkstosa || ""}
                onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row">
            <span className="col-xs-12 labelspanbold">Remarks from SA</span>
            <div className="col-xs-12">
              <input
                type="text"
                name="documentsremarksfromsa"
                maxLength="200"
                readOnly
                value={this.state.documentsremarksfromsa || ""}
                // onChange={this.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  ActionButton = () => {
    return (
      <div className="row" style={{ backgroundColor: "#18516c" }}>
        <div className="text-center col-xs-12 panel-body-list">
          {this.state.followupstatusvisible.includes(3) && (
            <div
              onClick={() => this.onClickActionButton("CALLLATER")}
              className="col-xs-3 col-md-1 pull-right-md"
              style={{
                marginBottom: "5px",
                marginTop: "5px",
                fontSize: "smaller"
              }}
            >
              <img className="img img-responsive" src={CallLater} />
              <span className="labelfontwhite">Call Later</span>
            </div>
          )}
          {this.state.followupstatusvisible.includes(2) && (
            <div
              onClick={() => this.onClickActionButton("POTENTIAL")}
              className="col-xs-3 col-md-1 pull-right-md"
              style={{
                marginBottom: "5px",
                marginTop: "5px",
                fontSize: "smaller"
              }}
            >
              <img className="img img-responsive" src={Potential} />
              <span className="labelfontwhite">Potential</span>
            </div>
          )}
          {this.state.followupstatusvisible.includes(13) && (
            <div
              onClick={() => this.onClickActionButton("DEAL")}
              className="col-xs-3 col-md-1 pull-right-md"
              style={{
                marginBottom: "5px",
                marginTop: "5px",
                fontSize: "smaller"
              }}
            >
              <img className="img img-responsive" src={PolicyCreated} />
              <span className="labelfontwhite">Deal</span>
            </div>
          )}
          {this.state.followupstatusvisible.includes(5) && (
            <div
              onClick={() => this.onClickActionButton("NOTDEAL")}
              className="col-xs-3 col-md-1 pull-right-md"
              style={{
                marginBottom: "5px",
                marginTop: "5px",
                fontSize: "smaller"
              }}
            >
              <img className="img img-responsive" src={NotDeal} />
              <span className="labelfontwhite">Not Deal</span>
            </div>
          )}
          {/* {this.state.followupstatusvisible.includes(0) && ( */}
          <div
            onClick={() => {
              this.checkDoubleInsured(
                this.state.vehiclechasisno,
                this.state.vehicleengineno,
                this.state.selectedQuotation,
                () => {
                  this.setState({ showModalButtonSendQuotation: true });
                }
              );
            }}
            className="col-xs-3 col-md-1 pull-right-md"
            style={{
              marginBottom: "5px",
              marginTop: "5px",
              fontSize: "smaller"
            }}
          >
            <img className="img img-responsive" src={SendQuotation} />
            <span className="labelfontwhite">Send Quotation</span>
          </div>
          {/* )} */}
        </div>
      </div>
    );
  };

  FieldPremi = () => {
    return (
      <React.Fragment>
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Gross Premium</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.state.GrossPremium, 0)}
          </span>
        </div>
        {this.state.IsRenewal && (
          <React.Fragment>
            {this.state.DiscountPremi > 0 ? (
              <div
                className="col-xs-12 backgroundgrey labelspanbold"
                style={{
                  padding: "0px",
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <span className="pull-left">Discount Premi</span>
                <span className="pull-right">
                  {"Rp." + Util.formatMoney(this.state.DiscountPremi, 0)}
                </span>
              </div>
            ) : (
              <div
                className="col-xs-12 backgroundgrey labelspanbold"
                style={{
                  padding: "0px",
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <span className="pull-left">No Claim Bonus</span>
                <span className="pull-right">
                  {"Rp." + Util.formatMoney(this.state.NoClaimBonus, 0)}
                </span>
              </div>
            )}
            {/* <div
              className="col-xs-12 backgroundgrey labelspanbold"
              style={{
                padding: "0px",
                paddingTop: "5px",
                paddingBottom: "5px"
              }}
            >
              <span className="pull-left">No Claim Bonus</span>
              <span className="pull-right">
                {"Rp." + Util.formatMoney(this.state.NoClaimBonus, 0)}
              </span>
            </div> */}
          </React.Fragment>
        )}
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Admin</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.state.Admin, 0)}
          </span>
        </div>
        <div
          className="col-xs-12 backgroundgrey labelspanbold"
          style={{
            padding: "0px",
            paddingTop: "5px",
            paddingBottom: "5px"
          }}
        >
          <span className="pull-left">Net Premi</span>
          <span className="pull-right">
            {"Rp." + Util.formatMoney(this.state.NetPremi, 0)}
          </span>
        </div>
      </React.Fragment>
    );
  };

  disabledPersonalDocuments = () => {
    let temp = false;

    if (this.isSendToSurveyor()) {
      temp = true;
    }

    // send to sa
    if (this.isSendToSA()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  disabledCompanyDocuments = () => {
    let temp = false;

    if (this.isSendToSurveyor()) {
      temp = true;
    }

    // send to sa
    if (this.isSendToSA()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  disabledDocuments = () => {
    let temp = false;

    if (this.isSendToSurveyor()) {
      temp = true;
    }

    // send to sa
    if (this.isSendToSA()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  disabledPersonalData = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isBackToAO()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    return temp;
  };

  disabledCompanyData = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isBackToAO(this.state.FollowUpStatus, this.state.FollowUpInfo)) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    return temp;
  };

  disabledVehicleData = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isBackToAO()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    return temp;
  };

  disabledExtendedCover = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isBackToAO(this.state.FollowUpStatus, this.state.FollowUpInfo)) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    return temp;
  };

  disabledPolicyDelivery = () => {
    let temp = false;

    if (this.isSendToSA(this.state.FollowUpStatus, this.state.FollowUpInfo)) {
      temp = true;
    }

    if (this.state.IsRenewal) {
      if (this.isFollowUpPayment()) {
        temp = true;
      }
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  disableChecklistPayerCompany = () => {
    let temp = false;

    // new data
    if (!this.isNewData()) {
      temp = true;
    }

    if (this.state.IsPaid) {
      temp = true;
    }

    // // is renewal
    // if(this.state.IsRenewal){
    //   if(this.state.personalpayercompanytemp){
    //     temp = true;
    //   }
    // }

    return temp;
  };

  disabledSurveySchedule = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isBackToAO(this.state.FollowUpStatus, this.state.FollowUpInfo)) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    if (this.isNotDeal()) {
      temp = true;
    }

    if (this.isOrderRejected()) {
      temp = true;
    }

    return temp;
  };

  isShowBasicCover = () => {
    let temp = false;

    if (
      this.disabledEditTaskDetail(
        this.state.FollowUpStatus,
        this.state.FollowUpInfo
      )
    ) {
      temp = true;
    }

    if (this.isNeedApproval()) {
      temp = true;
    }

    if (this.isBackToAO()) {
      temp = true;
    }

    if (this.isFollowUpPayment()) {
      temp = true;
    }

    if (this.isFollowUpSurveyResult()) {
      temp = true;
    }

    if (this.isNSAApproval() && !Util.isNullOrEmpty(this.state.SurveyNo)) {
      temp = true;
    }

    if (this.isPolicyApproved()) {
      temp = true;
    }

    if (this.isPolicyDelivered()) {
      temp = true;
    }

    if (this.isPolicyReceived()) {
      temp = true;
    }

    if (this.isPolicyApprovedDocRep()) {
      temp = true;
    }

    if (this.isPolicyDeliveredDocRep()) {
      temp = true;
    }

    if (this.isPolicyReceivedDocRep()) {
      temp = true;
    }

    return temp;
  };

  detectComprePeriod = () => {
    let temp = false;
    // if (
    //   !Util.isNullOrEmpty(this.state.vehiclebasiccoverage) &&
    //   this.state.databasiccoverall.length > 0
    // ) {
    //   let basicCoverage = [...this.state.databasiccoverall].filter(
    //     data => data.Id == this.state.vehiclebasiccoverage
    //   )[0];
    //   if (!Util.isNullOrEmpty(basicCoverage)) {
    //     if (basicCoverage.ComprePeriod > 0) {
    //       temp = true;
    //     }
    //   }
    // }
    if (this.state.IsTPLEnabled == 1) {
      // if IsTPLEnabled == 1 => compre period
      temp = true;
    }

    return temp;
  };

  disableEditTaskDetailbyRole = () => {
    let temp = false;

    let listUserDisable = ["CCO", "CSO"]; // edit other role task detail

    let account = JSON.parse(ACCOUNTDATA);
    Log.debugStr("SALESOFFICERID => " + this.state.SalesOfficerID);

    if (!Util.isNullOrEmpty(account)) {
      if (
        !Util.stringArrayContains(account.UserInfo.User.Role, listUserDisable)
      ) {
        if (
          !Util.stringEquals(
            account.UserInfo.User.SalesOfficerID,
            this.state.SalesOfficerID
          )
        ) {
          let iscso = Util.isNullOrEmpty(account.UserInfo.User.IsCSO)
            ? 0
            : account.UserInfo.User.IsCSO;
          if (iscso == 0) {
            temp = true;
          }
        }
      }

      if (
        Util.stringArrayContains(Util.valueTrim(account.UserInfo.User.Role), [
          "REGIONALMGR",
          "NATIONALMGR"
        ])
      ) {
        temp = true;
      }
    }

    return temp;
  };

  isUserFollowUp = () => {
    let temp = false;
    if (
      Util.stringEquals(
        this.state.SalesOfficerID,
        JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
      )
    ) {
      temp = true;
    }
    return temp;
  };

  isRoleCTIandRenewal = () => {
    let temp = false;
    if (
      Util.stringArrayContains(
        this.state.role,
        this.state.Parameterized.roleCTI
      ) &&
      this.state.IsRenewal
    ) {
      temp = true;
    }
    return temp;
  };

  isNotMaskingPhoneOrEmail = (isFocus = false) => {
    let temp = false;

    if (this.disableEditTaskDetailbyRole() == false) {
      temp = true;
    }

    if (isFocus || this.isUserFollowUp()) {
      temp = true;
    }

    if (this.isRoleCTIandRenewal()) {
      if (isFocus) {
        temp = true;
      } else {
        temp = false;
      }
    }

    return temp;
  };

  handleShowPeriod = () => {
    let temp = false;
    if (
      !Util.isNullOrEmpty(this.state.vehiclebasiccoverage) &&
      this.state.databasiccoverall.length > 0
    ) {
      let basicCoverage = [...this.state.databasiccoverall].filter(
        data => data.Id == this.state.vehiclebasiccoverage
      )[0];
      if (!Util.isNullOrEmpty(basicCoverage)) {
        if (basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0) {
          temp = true;
        }
      }
    }

    return temp;
  };

  getPlateRegion = (plateNo = this.state.vehicleplateno) => {
    plateNo = plateNo.substring(0, plateNo.length > 2 ? 2 : plateNo.length);
    plateNo = plateNo.replace(/[^a-zA-Z]+/g, "");
    Log.debugStr(plateNo);
    // if (Util.isNullOrEmpty(plateNo)) {
    //   return;
    // }
    this.hitApiGetPlateNo(plateNo);
  };

  getPlateRegionLoad = (
    plateNo = this.state.vehicleplateno,
    VehicleData = null
  ) => {
    plateNo = plateNo.substring(0, plateNo.length > 2 ? 2 : plateNo.length);
    plateNo = plateNo.replace(/[^a-zA-Z]+/g, "");
    Log.debugStr(plateNo);
    if (Util.isNullOrEmpty(plateNo)) {
      return;
    }
    this.hitApiGetPlateNoLoad(plateNo, VehicleData);
  };

  hitApiGetPlateNo = plateRegion => {
    this.setState(
      {
        vehicleregion: ""
      },
      () => {
        this.abortControllerGetMappingPlate.abort();
        this.abortControllerGetMappingPlate = new AbortController();

        Util.fetchAPIAdditional(
          `${API_URL +
            "" +
            API_VERSION_2}/DataReact/GetMappingVehiclePlateGeoArea/`,
          "POST",
          HEADER_API,
          {
            VehiclePlateCode: plateRegion
          },
          {
            signal: this.abortControllerGetMappingPlate.signal
          }
        )
          .then(res => res.json())
          .then(jsn => {
            if (jsn.status) {
              let data = jsn.data[0];
              if (!Util.isNullOrEmpty(data)) {
                this.setState(
                  {
                    vehicleregion: Util.valueTrim(data.RegionCode)
                  },
                  () => {
                    this.getVehiclePrice(
                      this.state.vehiclevehiclecode,
                      this.state.vehicleYear,
                      this.state.vehicleregion,
                      () => {
                        this.setState(
                          {
                            vehicletotalsuminsuredtemp: this.state
                              .vehicletotalsuminsured
                          },
                          () => {
                            this.trigerRateCalculation();
                          }
                        );
                      }
                    );
                  }
                );
              }
            }
          })
          .catch(error => {
            Log.error("parsing Failed", error);
          });
      }
    );
  };

  ModalSendEmail = () => {
    return Util.stringArrayContains(
      this.state.role,
      this.state.Parameterized.roleCTI
    ) && this.state.IsRenewal ? (
      <OtosalesModalSendEmailCkEditor
        state={this.state}
        showModalSendEmail={this.state.showModalSendEmail}
        // bodyEmail={Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")}
        // subject={Util.stringParse("Penawaran Polis Asuransi %s an %s", "[Product Name]", "[Nama Customer]")}
        subject={this.state.SubjectTemplateEmail}
        bodyEmail={this.state.BodyTemplateSendEmail}
        OrderNo={this.state.selectedQuotation}
        maxLengthSubject={100}
        maxLength={3000}
        sendQuotationEmail={this.sendQuotationEmailTelerenewal}
        onCloseModalSendEmail={this.onCloseModalSendEmail}
        handleDeleteImage={this.handleDeleteFile}
        handleUploadImage={this.handleUploadFile}
        state={this.state}
        email={
          this.state.isCompany
            ? this.state.companyemail
            : this.state.personalemail
        }
      />
    ) : (
      <OtosalesModalSendEmailv2
        sendQuotationEmail={this.sendQuotationEmail}
        onCloseModalSendEmail={this.onCloseModalSendEmail}
        state={this.state}
        email={
          this.state.isCompany
            ? this.state.companyemail
            : this.state.personalemail
        }
      />
    );
  };

  ModalSendSMS = () => {
    return Util.stringArrayContains(
      this.state.role,
      this.state.Parameterized.roleCTI
    ) && this.state.IsRenewal ? (
      <OtosalesModalSendSMSCkEditor
        isLoading={this.state.isLoading}
        dataphonelist={[...this.state.dataphonelist].filter(data =>
          Util.detectPrefix(data.value, this.state.Parameterized.mobilePrefix)
        )}
        maxLength={500}
        bodySMS={this.state.BodyTemplateSendEmailSMSTelerenewal}
        phonelistselect={this.state.phonelistselect}
        onOptionSelect2Change={this.onOptionSelect2Change}
        sendQuotationSMS={this.sendQuotationSMSTelerenewal}
        onCloseModalSendSMS={this.onCloseModalSendSMS}
        state={this.state}
        phonenumber={
          this.state.isCompany ? this.state.companypichp : this.state.personalhp
        }
      />
    ) : (
      <OtosalesModalSendSMSv2
        sendQuotationSMS={this.sendQuotationSMS}
        onCloseModalSendSMS={this.onCloseModalSendSMS}
        state={this.state}
        phonenumber={
          this.state.isCompany ? this.state.companypichp : this.state.personalhp
        }
      />
    );
  };
  ModalSendEmailWA = () => {
    return (
      <OtosalesModalSendEmailCkEditor
        state={this.state}
        showModalSendEmail={this.state.showModalSendEmailWA}
        // bodyEmail={Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")}
        // subject={Util.stringParse("Penawaran Polis Asuransi %s an %s", "[Product Name]", "[Nama Customer]")}
        subject={this.state.SubjectTemplateEmailWA}
        bodyEmail={this.state.BodyTemplateSendEmailWA}
        OrderNo={this.state.selectedQuotation}
        maxLengthSubject={100}
        maxLength={3000}
        sendQuotationEmail={this.sendQuotationEmailTelerenewal}
        onCloseModalSendEmail={this.onCloseModalSendEmailWA}
        handleDeleteImage={this.handleDeleteFile}
        handleUploadImage={this.handleUploadFile}
        state={this.state}
        email={
          // this.state.Parameterized.emailAdminWA.join(";") ||
          this.state.EmailToTemplateWA
        }
      />
    );
  };

  hitApiGetPlateNoLoad = (plateRegion, VehicleData = null) => {
    this.setState(
      {
        vehicleregion: ""
      },
      () => {
        Util.fetchAPIAdditional(
          `${API_URL +
            "" +
            API_VERSION_2}/DataReact/GetMappingVehiclePlateGeoArea/`,
          "POST",
          HEADER_API,
          {
            VehiclePlateCode: plateRegion
          }
        )
          .then(res => res.json())
          .then(jsn => {
            if (jsn.status) {
              let data = jsn.data[0];
              if (!Util.isNullOrEmpty(data)) {
                this.setState(
                  {
                    vehicleregion: Util.valueTrim(data.RegionCode)
                  },
                  () => {
                    this.getVehiclePrice(
                      this.state.vehiclevehiclecode,
                      this.state.vehicleYear,
                      this.state.vehicleregion,
                      () => {
                        if (
                          Util.isNullOrEmpty(
                            this.state.vehicletotalsuminsuredtemp
                          ) ||
                          this.state.vehicletotalsuminsuredtemp == 0
                        ) {
                          this.setState({
                            vehicletotalsuminsuredtemp:
                              this.state.vehicletotalsuminsured ||
                              (Util.isNullOrEmpty(VehicleData)
                                ? ""
                                : VehicleData.SumInsured)
                          });
                        }
                      }
                    );
                  }
                );
              }
            }
          })
          .catch(error => {
            Log.error("parsing Failed", error);
          });
      }
    );
  };

  PageTasklistDetails = () => {
    return (
      <div>
        <Header
          titles="New Order"
          additionalDiv={
            <React.Fragment>
              <div className="col-xs-12">
                <span style={{ fontSize: "16px", color: "#fff" }}>
                  Details{" "}
                  {Util.isNullOrEmpty(this.state.PolicyOrderNo)
                    ? ""
                    : this.state.PolicyOrderNo}
                </span>
              </div>
            </React.Fragment>
          }
        >
        </Header>
        <ToastContainer />
        <OtosalesModalInfo
          message="Data order berubah akibat hasil survey!"
          onClose={() => {
            this.onUpdateOrder(this.state.selectedQuotation, () => {
              this.setState({
                showModalCheckSurveyResult: false
              });
            });
          }}
          showModalInfo={this.state.showModalCheckSurveyResult}
          isLoading={this.state.isLoading}
        />
        {this.state.showModalAccessories && (
          <OtosalesModalAccessories
            showModal={this.state.showModalAccessories}
            onCloseModal={this.onCloseModalAccessories}
            dataAccessories={this.state.dataAccessories}
          />
        )}
        <div
          className="content-wrapper"
          style={{
            minHeight: "85vh",
            height: "85vh",
            maxHeight: "85vh",
            overscrollBehaviorY: "contain"
          }}
        >
          <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            isLoading={this.state.isLoading}
          >
            <this.ComponentFunctional />
            {/* <OtosalesFloatingButtonv2>
                <this.ActionButton />
              </OtosalesFloatingButtonv2> */}
            <this.TaskDetailDefault />
            {/* <OtosalesIFrame url="http://localhost" width="100%" height="1000px" /> */}
          </OtosalesLoading>
        </div>
      </div>
    );
  };

  ComponentFunctional = () => {
    return (
      <React.Fragment>
        <this.ModalButtonSendQuotation />
        <this.ModalAddCopyQuotation />
        <OtosalesModalQuotationHistory
          state={this.state}
          onClose={() => this.setState({ showModal: false })}
          editQuotationHistory={() => this.editQuotationHistory()}
          onClickSendEmail={() =>
            this.setState({
              showModalMakeSure: true,
              sendEmailOrSMS: "EMAIL"
            })
          }
          onClickSendSMS={() => {
            this.setState({
              showModalMakeSure: true,
              sendEmailOrSMS: "SMS"
            });
          }}
          onClickQuotationHistory={this.onClickQuotationHistory}
        />
        <this.ModalSendEmail />
        <this.ModalSendSMS />
        <this.ModalSendEmailWA />
        <OtosalesModalSendWA
          dataphonelist={[...this.state.dataphonelist].filter(data =>
            Util.detectPrefix(data.value, this.state.Parameterized.mobilePrefix)
          )}
          phonelistselect={this.state.phonelistselect}
          onOptionSelect2Change={this.onOptionSelect2Change}
          sendQuotationWA={() => {
            this.sendQuotationWA();
          }}
          onCloseModalSendWA={this.onCloseModalSendWA}
          state={this.state}
          phonenumber={
            this.state.isCompany
              ? this.state.companypichp
              : this.state.personalhp
          }
        />
        <OtosalesModalMakeSure
          titles="Confirmation"
          messages="Are you sure to send?"
          showModalMakeSure={this.state.showModalMakeSure}
          onCloseMakeSure={() => {
            this.setState({
              showModalMakeSure: false
            });
          }}
          onSubmitMakeSure={() => {
            this.setState({
              showModalMakeSure: false,
              showModalButtonSendQuotation: false
            });
            this.setState({ isLoading: true });
            this.onDownloadTaskDetail(() => {
              this.setState({ isLoading: false });
              if (this.state.sendEmailOrSMS == "EMAIL") {
                this.setState({
                  showModalSendEmail: true,
                  showModalMakeSure: false,
                  showModal: false
                });
                if (
                  Util.stringArrayContains(
                    this.state.role,
                    this.state.Parameterized.roleCTI
                  )
                ) {
                  this.getSendEmailTemplate(this.state.selectedQuotation);
                }
              } else if (this.state.sendEmailOrSMS == "SMS") {
                if (
                  Util.stringArrayContains(
                    this.state.role,
                    this.state.Parameterized.roleCTI
                  )
                ) {
                  this.getPhoneList(this.state.selectedQuotation, true, () => {
                    this.setState({
                      showModalSendSMS: true,
                      showModalMakeSure: false,
                      showModal: false,
                      phonelistselect: this.selectedPhoneNumberCTI()
                    });
                  });
                  this.getSendEmailTemplateSMSTelerenewal(
                    this.state.selectedQuotation
                  );
                } else {
                  this.setState({
                    showModalSendSMS: true,
                    showModalMakeSure: false,
                    showModal: false,
                    phonelistselect: this.state.isCompany
                      ? this.state.companypichp
                      : this.state.personalhp
                  });
                }
              } else if (this.state.sendEmailOrSMS == "WA") {
                this.getPhoneList(this.state.selectedQuotation, true, () => {
                  this.setState({
                    showModalSendWA: true,
                    showModalMakeSure: false,
                    showModal: false,
                    phonelistselect: this.selectedPhoneNumberCTI()
                  });
                });
              }
            });
          }}
        />
        <OtosalesModalSetPeriodPremi
          state={this.state}
          rateCalculationNonBasic={() => this.premiumCalculation()}
          onChangeFunctionDate={this.onChangeFunctionDate}
          onChangeFunctionChecked={this.onChangeFunctionChecked}
          onChangeFunction={this.onChangeFunction}
          onOptionSelect2Change={this.onOptionSelect2Change}
          onChangeFunctionReactNumber={this.onChangeFunctionReactNumber}
          getTPLSI={this.getTPLSI}
          onClose={() => {
            this.setState({
              showdialogperiod: false
            });
          }}
        />
        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfo}
          showModalInfo={this.state.showModalInfo}
        />
        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfoTryAPI}
          showModalInfo={this.state.showModalInfoTryAPI}
        />
        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfoIsPaid}
          showModalInfo={this.state.showModalInfoIsPaid}
        />
      </React.Fragment>
    );
  };

  TaskDetailDefault = () => {
    return (
      <div
        className={
          "paddingcontent " +
          (this.disableEditTaskDetailbyRole() ? "disablediv" : "")
        }
      >
        <this.FieldPremi />
        {!this.state.isCompany && (
          <React.Fragment>
            <this.PersonalData />
            <this.PersonalDocuments />
          </React.Fragment>
        )}
        {this.state.isCompany && (
          <React.Fragment>
            <this.CompanyData />
            <this.CompanyDocuments />
          </React.Fragment>
        )}
        <this.VehicleData />
        <this.ExtendedCover />
        <this.PaymentInfo />
        <this.PolicyDelivery />
        <this.SurveySchedule />
        {!Util.isNullOrEmpty(this.state.SurveyNo) && (
          <React.Fragment>
            <this.SurveyData />
            <this.SurveyDocuments />
          </React.Fragment>
        )}
        <this.Documents />
        <this.ActionButton />
      </div>
    );
  };

  render() {
    let StateTaskDetail = JSON.parse(
      secureStorage.getItem("SelectedTaskDetail")
    );
    if (Util.isNullOrEmpty(StateTaskDetail)) {
      this.props.history.push("/");
      return <div />;
    }
    if (StateTaskDetail.IsRenewalNonRenNot == 1) {
      //// HANDLE NONRENOT
      clearInterval(this.loadallfirstall);
      return <PageTasklistDirtyData />;
    } else {
      return <this.PageTasklistDetails />;
    }
  }
}

const mapStateToProps = (globalState) =>  {
  return {
    globalState
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addStateTaskDetail : (localState) => dispatch(
      {
        type :ActionType.SELF_FUNC, 
        self_state : localState,
        self_key: "localState"
      }
    ) 
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PageTasklistDetails));
