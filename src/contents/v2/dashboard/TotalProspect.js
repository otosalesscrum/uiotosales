import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { OtosalesSelectFullviewrev, OtosalesProgressbar, Util } from "../../../otosalescomponents";

function mappingDataProgressBar(datalist) {
    return datalist.map(data => {
        return { value: data.value, variant: "info" }
    })
}

function mappingTitleProgressBar(datalist) {
    return datalist.map((data, i) => {
        // return (<label key={i} style={{ marginBottom:"0px" }}>{data.title}</label>);
        return (<label className="col-xs-12" key={i} style={{ marginBottom: "0px", textAlign: "right" }}>{data.value}</label>);
    });
}

export default withRouter(function TotalProspect(props) {
    let maxValue = 0;
    let totVal = 0;
    props.dataTotalProspect.forEach(data => {
        totVal += parseInt(data.listProgress[0].value + "");
        maxValue = parseInt(data.listProgress[0].value) > maxValue ? parseInt(data.listProgress[0].value + "") : maxValue;
    });
    let status = props.dataTotalProspect.map((data, i) => {
        return (
            <div className="col-xs-12 panel-body-list" key={i}>
                <div className="col-md-2 col-xs-12 panel-body-list">{data.name}</div>
                <div className="col-md-6 col-xs-7 panel-body-list">
                    <OtosalesProgressbar
                        // dataitems={mappingDataProgressBar(data.listProgress)}
                        dataitems={[
                            // { value: parseInt(data.listProgress[0].value) / maxValue * 100, variant: "info" },
                            // { value: parseInt(data.listProgress[0].value) / maxValue * 100, variant: "info" }
                            { value: parseInt(data.listProgress[0].value) / totVal * 100, variant: "info" },
                            { value: parseInt(data.listProgress[0].value) / totVal * 100, variant: "info" }
                        ]}
                        max={100}
                        animated={false}
                    />
                </div>
                <div className="col-md-4 col-xs-5 panel-body-list" style={{ fontSize: "10pt" }}>
                    {mappingTitleProgressBar(data.listProgress)}
                </div>
            </div>
        );
    })
    return (
        <React.Fragment>
            <div className="col-md-6 col-xs-6 panel-body-list" style={{ float: "left" }} >
                <p>VIEW</p>
                <div className="input-group" style={{
                    pointerEvents: "none"
                }}>
                    <OtosalesSelectFullviewrev
                        name="viewtotalprospect"
                        value={props.state.viewtotalprospect}
                        onOptionsChange={props.onOptionViewChange}
                        options={props.state.ViewtotalprospectData}
                    />
                </div>
            </div>
            <div className="col-md-6 col-xs-6" style={{ float: "right" }} >
                <p>PERIOD</p>
                <div className="input-group">
                    <OtosalesSelectFullviewrev
                        name="viewperiod"
                        value={props.state.viewperiod}
                        onOptionsChange={props.onOptionViewChange}
                        options={props.state.PeriodView}
                    />
                </div>
            </div>
            <div className="col-xs-12 panel-body-list">{status}</div>
        </React.Fragment>
    )
});
