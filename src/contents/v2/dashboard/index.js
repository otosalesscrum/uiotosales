import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import BlockUi from "react-block-ui";
import { API_URL, API_VERSION_2, HEADER_API, ACCOUNTDATA } from "../../../config";
import { Loader } from "react-loaders";
import Header from "../../../components/Header";
import { ToastContainer } from "react-toastify";
import HeaderDashboard from "./HeaderDashboard";
import TotalProspect from "./TotalProspect";
import ProgressUnit from "./ProgressUnit";
import { OtosalesLoading, Util, Log, Otosalesheader } from "../../../otosalescomponents";

class PageDashboardPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewtotalprospect: "totalprospect",
      viewperiod: "MONTHLY",
      isLoading: false,
      StateDashboard: "", // handle localstorage
      DetailID: "",
      LastDashboard: { StateDashboard: null, DetailID: null },
      TotalPremi: "",
      PeriodView: [
        { value: "MONTHLY", label: "Month to Date" },
        { value: "YEARLY", label: "Year to Date" }
      ],
      ViewtotalprospectData: [
        { value: "totalprospect", label: "Total Prospect" }
      ],
      dataHeader: [
        {
          name: "New",
          listParam: [
            { title: "Total Premi New", value: "Loading ..." },
            { title: "Total Policy New", value: "Loading ..." }
          ]
        },
        {
          name: "Renew",
          listParam: [
            { title: "Total Premi Renew", value: "Loading ..." },
            { title: "Total Policy Renew", value: "Loading ..." },
            { title: "% Renewal Ratio", value: "Loading ..." }
          ]
        },
        {
          name: "Endorse",
          listParam: [
            { title: "Total Premi Endorse", value: "Loading ..." },
            { title: "Total Policy Endorse", value: "Loading ..." }
          ]
        },
        {
          name: "Cancel",
          listParam: [
            { title: "Total Premi Cancel", value: "Loading ..." },
            { title: "Total Policy Cancel", value: "Loading ..." }
          ]
        }
      ],
      dataTotalProspect: [
        {
          name: "Deal",
          listProgress: [
            { title: "Total Order Deal", value: 100 },
            { title: "Total Premi Deal", value: "Loading..." }
          ]
        },
        {
          name: "Potential",
          listProgress: [
            { title: "Total Order Potential", value: 100 },
            { title: "Total Premi Potential", value: "Loading..." }
          ]
        },
        {
          name: "Call Later",
          listProgress: [
            { title: "Total Order Call Later", value: 100 },
            { title: "Total Premi Call Later", value: "Loading..." }
          ]
        },
        {
          name: "Need FU",
          listProgress: [
            { title: "Total Order Call Later", value: 100 },
            { title: "Total Premi Call Later", value: "Loading..." }
          ]
        },
        {
          name: "Not Deal",
          listProgress: [
            { title: "Total Order Call Later", value: 100 },
            { title: "Total Premi Call Later", value: "Loading..." }
          ]
        },
        {
          name: "Order Rejected",
          listProgress: [
            { title: "Total Order Call Later", value: 100 },
            { title: "Total Premi Call Later", value: "Loading..." }
          ]
        }
      ],
      dataProgressUnit: [
        { id: 1, name: "Loading ...", value: "Loading ...", stateDashboard: "" },
        { id: 2, name: "Loading ...", value: "Loading ...", stateDashboard: "" },
        { id: 3, name: "Loading ...", value: "Loading ...", stateDashboard: "" },
        { id: 4, name: "Loading ...", value: "Loading ...", stateDashboard: "" },
        { id: 5, name: "Loading ...", value: "Loading ...", stateDashboard: "" }
      ]
    };
  }

  componentDidMount() {
    let params = new URLSearchParams(this.props.location.search.substring(1));
    Log.debugGroup("id : ", params.get("id"));
    Log.debugGroup("typedashboard : ", params.get("typedashboard"));
    // this.hitGetDashboard();

    this.setState({
      StateDashboard: params.get("typedashboard"),
      DetailID: params.get("id"),
      LastDashboard: { StateDashboard: params.get("typedashboard"), DetailID: params.get("id") },
    }, () => {
      this.checkDataAPi = setInterval(() => {
        params = new URLSearchParams(this.props.location.search.substring(1));
        this.setState({ DetailID: params.get("id"), StateDashboard: params.get("typedashboard") });
        let currentDashboard =
          { DetailID: params.get("id"), StateDashboard: params.get("typedashboard") }

        if (JSON.stringify(this.state.LastDashboard) != JSON.stringify(currentDashboard)) {
          Log.debugGroup("HIT API DISINI");
          this.hitGetDashboard(currentDashboard.StateDashboard, currentDashboard.DetailID, this.state.viewperiod);
          this.setState({
            LastDashboard: currentDashboard
          });
        }
      }, 100);
    });

  }

  setDataHitAPI = (data) => {
    // let jsn = {
    //   "summary": {
    //     "TotalPremi": Math.floor(Math.random() * 100),
    //     "NewTotalPremi": Math.floor(Math.random() * 100),
    //     "NewTotalPolicy": Math.floor(Math.random() * 100),
    //     "RenewTotalPremi": Math.floor(Math.random() * 100),
    //     "RenewTotaiPolicy": Math.floor(Math.random() * 100),
    //     "EndorseTotalPremi": Math.floor(Math.random() * 100),
    //     "EndorseTotalPolicy": Math.floor(Math.random() * 100),
    //     "CancelTotalPremi": Math.floor(Math.random() * 100),
    //     "CancelTotalPolicy": Math.floor(Math.random() * 100),
    //     "RenewalRatio": Math.floor(Math.random() * 100),
    //     "DealTotalPremi": Math.floor(Math.random() * 100),
    //     "DealTotalOrder": Math.floor(Math.random() * 100),
    //     "PotentialTotalPremi": Math.floor(Math.random() * 100),
    //     "PotentialTotalOrder": Math.floor(Math.random() * 100),
    //     "CallLaterTotalPremi": Math.floor(Math.random() * 100),
    //     "CallLaterTotalOrder": Math.floor(Math.random() * 100),
    //     "NeedFUTotalPremi": Math.floor(Math.random() * 100),
    //     "NeedFUTotalOrder": Math.floor(Math.random() * 100),
    //     "NotDealTotalPremi": Math.floor(Math.random() * 100),
    //     "NotDealTotalOrder": Math.floor(Math.random() * 100),
    //     "RejectedTotalPremi": Math.floor(Math.random() * 100),
    //     "RejectedTotalOrder": Math.floor(Math.random() * 100),
    //     "DetailDashboardState": ""
    //   },
    //   "detail": [{
    //     "ID": "1",
    //     "Label": "DKI",
    //     "Value": Math.floor(Math.random() * 100),
    //     "StateDashboard": ""
    //   }, {
    //     "ID": "2",
    //     "Label": "Jabar",
    //     "Value": Math.floor(Math.random() * 100),
    //     "StateDashboard": ""
    //   }, {
    //     "ID": "3",
    //     "Label": "Jateng",
    //     "Value": Math.floor(Math.random() * 100),
    //     "StateDashboard": ""
    //   }, {
    //     "ID": "4",
    //     "Label": "Jatim",
    //     "Value": Math.floor(Math.random() * 100),
    //     "StateDashboard": ""
    //   }, {
    //     "ID": "5",
    //     "Label": "Sumatera",
    //     "Value": Math.floor(Math.random() * 100),
    //     "StateDashboard": ""
    //   }]
    // };

    let jsn = data;

    let summary = jsn.summary;
    let detail = jsn.detail;

    let dataHeader = [
      {
        name: "New",
        listParam: [
          { title: "Total Premi New", value: "Rp." + Util.formatMoney(summary.NewTotalPremi, 0) },
          { title: "Total Policy New", value: summary.NewTotalPolicy }
        ]
      },
      {
        name: "Renew",
        listParam: [
          { title: "Total Premi Renew", value: "Rp." + Util.formatMoney(summary.RenewTotalPremi, 0) },
          { title: "Total Policy Renew", value: summary.RenewTotalPolicy },
          { title: "Renewal Ratio", value: Util.formatMoney((parseFloat(Util.isNullOrEmpty(summary.RenewTotalPolicy) ? 0 : summary.RenewTotalPolicy)/parseFloat(Util.isNullOrEmpty(summary.RenewalRatio) ? 0 : summary.RenewalRatio) * 100),2) + "%" }
        ]
      },
      {
        name: "Endorse",
        listParam: [
          { title: "Total Premi Endorse", value: "Rp." + Util.formatMoney(summary.EndorseTotalPremi, 0) },
          { title: "Total Policy Endorse", value: summary.EndorseTotalPolicy }
        ]
      },
      {
        name: "Cancel",
        listParam: [
          { title: "Total Premi Cancel", value: "Rp." + Util.formatMoney(summary.CancelTotalPremi, 0) },
          { title: "Total Policy Cancel", value: summary.CancelTotalPolicy }
        ]
      }
    ]

    let dataTotalProspect = [
      {
        name: "Deal",
        listProgress: [
          { title: "Total Order Deal", value: summary.DealTotalOrder },
          { title: "Total Premi Deal", value: "Rp." + Util.formatMoney(summary.DealTotalPremi, 0) }
        ]
      },
      {
        name: "Potential",
        listProgress: [
          { title: "Total Order Potential", value: summary.PotentialTotalOrder },
          { title: "Total Premi Potential", value: "Rp." + Util.formatMoney(summary.PotentialTotalPremi, 0) }
        ]
      },
      {
        name: "Call Later",
        listProgress: [
          { title: "Total Order Call Later", value: summary.CallLaterTotalOrder },
          { title: "Total Premi Call Later", value: "Rp." + Util.formatMoney(summary.CallLaterTotalPremi, 0) }
        ]
      },
      {
        name: "Need FU",
        listProgress: [
          { title: "Total Order Need FU", value: summary.NeedFUTotalOrder },
          { title: "Total Premi Need FU", value: "Rp." + Util.formatMoney(summary.NeedFUTotalPremi, 0) }
        ]
      },
      {
        name: "Not Deal",
        listProgress: [
          { title: "Total Order Not Deal", value: summary.NotDealTotalOrder },
          { title: "Total Premi Not Deal", value: "Rp." + Util.formatMoney(summary.NotDealTotalPremi, 0) }
        ]
      },
      {
        name: "Order Rejected",
        listProgress: [
          { title: "Total Order Order Rejected", value: summary.RejectedTotalOrder },
          { title: "Total Premi Order Rejected", value: "Rp." + Util.formatMoney(summary.RejectedTotalPremi, 0) }
        ]
      }
    ];

    let dataProgressUnit = detail.map(data => {
      return { id: data.ID, name: data.Label, value: "Rp." + Util.formatMoney(data.Value, 0), stateDashboard: data.StateDashboard }
    });

    let TotalPremi = "Rp."+Util.formatMoney(summary.TotalPremi, 0);

    this.setState({
      dataHeader,
      dataTotalProspect,
      dataProgressUnit,
      TotalPremi
    });
  }

  hitGetDashboard = (StateDashboard = this.state.StateDashboard, DetailID = this.state.DetailID, PeriodView = "MONTHLY", callback = this.setDataHitAPI) => {
    this.setState({
      isLoading: true
    });

    let account = JSON.parse(ACCOUNTDATA);

    Util.fetchAPIAdditional(`${API_URL + "" + API_VERSION_2}/DataReact/getNewDashboard`, "POST",
      HEADER_API,
      {
        StateDashboard: (Util.isNullOrEmpty(StateDashboard) ? account.UserInfo.User.Role : StateDashboard),
        Role: account.UserInfo.User.Role,
        PeriodView: PeriodView,
        UserID: (account.UserInfo.User.SalesOfficerID + "").toUpperCase(),
        // Channel=" +
        // ChannelSource=" +
        DetailID: DetailID
        // if param get dashboard null => 
        // initialize hit api with above param -> 
        // next hitting -> get param utl set to param API 

      })
      .then(res => res.json())
      .then(jsn => {

        callback(jsn.data);

        this.setState({ isLoading: false })
      })
      .catch(error => {
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
          // this.searchDataHitApi();
        }
      });

  };

  onOptionSelect2Change = (value, name) => {
    this.setState({ [name]: value });

    if (name == "viewperiod") {
      this.hitGetDashboard(this.state.StateDashboard, this.state.DetailID, value);
    }
  };

  hideTotalUnit = () => {
    let temp = true;

    if((JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "SALESOFFICER")){
      temp = false;
    }

    // if((JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "SALESSECHEAD")){
    //   temp = false;
    // }

    if((JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "TELERENEWAL")){
      temp = false;
    }

    if((JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "CSO")){
      temp = false;
    } 
    
    if((JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "COCEN")){
      temp = false;
    }   

    return temp;
  }

  HeaderMonth = () => {
    return (
      <React.Fragment>
        <div className="col-md-4 col-xs-6" style={{ float: "left" }}>
          <p style={{ textAlign: "left", color: "#606060" }}>
            <b>PROSPECT</b>
          </p>
        </div>
        <div className="col-md-4 col-xs-6" style={{ float: "right" }}>
          <p style={{ textAlign: "right", color: "#606060" }} id="textDate">
            <b>{Util.formatDate(Util.convertDate(), "mmmm yyyy")}</b>
          </p>
        </div>
      </React.Fragment>
    );
  };

  SectionTotalPremi = () => {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-xs-12">
            <center>
              <label style={{ fontSize: "14pt" }}>Total Premi : <br/>{this.state.TotalPremi}</label>
            </center>
          </div>
        </div>
        <div className="row" style={{ marginBottom: "10px" }}>
          <HeaderDashboard dataHeader={this.state.dataHeader} />
        </div>
      </React.Fragment>
    );
  };

  handleClickUnit = (DetailID) => {
    Log.debugGroup("ID : ", DetailID);
    let StateDashboard = [...this.state.dataProgressUnit].filter(data => data.id = DetailID)[0];
    StateDashboard = StateDashboard.stateDashboard;
    if (DetailID != this.state.DetailID || StateDashboard != this.state.StateDashboard) {
      this.props.history.push("/dashboard?typedashboard=" + StateDashboard + "&id=" + DetailID);
    }
  };

  PagesRender = () => {
    return (
      <div className="content-wrapper" style={{ padding: "10px" }}>
        <OtosalesLoading
          className="col-xs-12 panel-body-list loadingfixed"
          isLoading={this.state.isLoading}
        >
          <div className="panel-heading" style={{ background: "#eaeaea" }}>
            <div className="row">
              <this.HeaderMonth />
            </div>
          </div>
          <this.SectionTotalPremi />
          <div className="row">
            <div
              className="col-xs-12 col-md-6"
              style={{ marginBottom: "10px" }}
            >
              <div className="box box-solid bg-gray">
                <div className="box-header with-border">
                  <h3 className="box-title"><b style={{ color: "#000" }}>Total Prospect</b></h3>

                  <div className="box-tools pull-right">
                    <button type="button" className="btn btn-box-tool" data-toggle="collapse"
                      data-target="#totalpremi"><i className="fa fa-minus" style={{ color: "#18516c" }}></i>
                    </button>
                  </div>
                </div>
                <div className="box-body collapse in" id="totalpremi">
                  <TotalProspect
                    dataTotalProspect={this.state.dataTotalProspect}
                    onOptionViewChange={this.onOptionSelect2Change}
                    state={this.state}
                  />
                </div>
              </div>
            </div>
            {/* {!(JSON.parse(ACCOUNTDATA).UserInfo.User.Role == "SALESOFFICER" || this.state.StateDashboard=="SALESOFFICER") && this.state.dataProgressUnit.length > 0 && */}
            {this.hideTotalUnit() &&
              <div className="col-xs-12 col-md-6" style={{
                height: "100%",
                overflow: "auto",
                maxHeight: "365px"
              }}>
                <div className="box box-solid bg-light-blue-gradient">
                  {/* <div className="box-header with-border">
                  <h3 className="box-title"><b>Unit</b></h3>
                  <div className="box-tools pull-right">
                    <button type="button" className="btn btn-box-tool" data-toggle="collapse"
                      data-target="#totalunit"><i className="fa fa-minus" style={{ color: "#18516c" }}></i>
                    </button>
                  </div>
                </div> */}
                  <div className="box-body collapse in" id="totalunit">
                    <ProgressUnit dataProgressUnit={this.state.dataProgressUnit} handleClickUnit={this.handleClickUnit} />
                  </div>
                </div>
              </div>
            }
          </div>
        </OtosalesLoading>
      </div>
    );
  };

  render() {
    if(Util.isNullOrEmpty(ACCOUNTDATA)){
      this.props.history.push("/");
      return (<div/>);
    }
    return (
      <React.Fragment>
        {
          Util.isNullOrEmpty(this.state.DetailID) && (
            <Header titles="Dashboard">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  {/* <a className="fa fa-envelope fa-lg" /> */}
                </li>
              </ul>
            </Header>
          )
        }
        {
          !Util.isNullOrEmpty(this.state.DetailID) && (
            <Otosalesheader handleBackButton={() => Util.handleBackButton(this.props)} titles="Dashboard">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  {/* <a className="fa fa-envelope fa-lg" /> */}
                </li>
              </ul>
            </Otosalesheader>
          )
        }

        <this.PagesRender />
      </React.Fragment>
    );
  }
}

export default withRouter(PageDashboardPersonal);
