import React from 'react'
import { withRouter } from "react-router-dom";
import { OtosalesProgressbar } from '../../../otosalescomponents';

export default withRouter(function ProgressUnit(props) {
    let listUnit = props.dataProgressUnit.map((data, i) => {
        return (
            <div className="col-xs-12 panel-body-list" key={i} onClick={() => props.handleClickUnit(data.id)}>
                <div className="col-md-4 col-xs-12 panel-body-list"><b>{data.name}</b></div>
                <div className="col-md-8 col-xs-12 panel-body-list">
                    {/* <OtosalesProgressbar
                        animated={false}
                        dataitems={[
                            { value: data.value, variant: "info" },
                            { value: data.value, variant: "info" }
                        ]}
                        max={100}
                    /> */}
                    {/* {data.value} */}
                    <input
                        type="text"
                        maxLength="50"
                        disabled={true}
                        value={data.value}
                        className="form-control"
                    />
                </div>
            </div>
        );
    })
    return (
        <React.Fragment>
            {listUnit}
        </React.Fragment>
    )
});
