import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { API_URL, API_VERSION, ACCOUNTDATA, HEADER_API } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { Util } from "../otosalescomponents";

class HalChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isError: false,
      isLoading: false,
      messageError: "Please fill all fields",
      newpass: "",
      oldpass: "",
      confirmnewpassword: ""
    };
  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onSubmitChangePass = event => {
    event.preventDefault();
    const { oldpass, newpass, confirmnewpassword } = this.state;

    if (oldpass === "" || newpass === "" || confirmnewpassword === "") {
      this.setState({
        isError: true,
        messageError: "Please fill in all required fields"
      });
      return;
    }

    if (newpass !== confirmnewpassword) {
      this.setState({
        isError: true,
        messageError: "Password confirmation does not match your password"
      });
      return;
    }

    if (!/^(?=.*[A-z])(?=.*\d).{8,20}$/.test(newpass)) {
      this.setState({
        isError: true,
        messageError:
          "Password must be 8 - 20 characters and is a combination of character and number"
      });
      return;
    }

    if (oldpass === newpass) {
      this.setState({
        isError: true,
        messageError: "New password can not be same as old password"
      });
      return;
    }

    this.setState({
      isError: false,
      messageError: "",
      isLoading: true
    });

    let account = JSON.parse(ACCOUNTDATA);

    Util.fetchAPIAdditional(`${API_URL + "" + API_VERSION}/Authentication/ChangePassword/`, "POST",
     HEADER_API,
     {
       username: Util.isNullOrEmpty(account.UserInfo.User.ExtUserID) ? this.props.username : account.UserInfo.User.Email,
       password: newpass,
       oldpassword: oldpass
     })

    // fetch(`${API_URL + "" + API_VERSION}/Authentication/ChangePassword/`, {
    //   method: "POST",
    //   headers: new Headers({
    //     "Content-Type": "application/x-www-form-urlencoded",
    //     Authorization:
    //       "Bearer " + JSON.parse(ACCOUNTDATA).Token
    //   }),
    //   body:
    //     "username=" +
    //     this.props.username +
    //     "&password=" +
    //     newpass +
    //     "&oldpassword=" +
    //     oldpass
    // })
      // .then(res =>{
      //   if(res.status != 200){
      //     console.log("gagal, make popup gagal");
      //     toast.dismiss();toast.error("❗ Server error, password didn't changed", {
      //       position: "top-right",
      //       autoClose: 5000,
      //       hideProgressBar: false,
      //       closeOnClick: true,
      //       pauseOnHover: true,
      //       draggable: true
      //     });
      //   }else{
      //     toast.dismiss();toast.info("✔️ Password has been changed", {
      //       position: "top-right",
      //       autoClose: 5000,
      //       hideProgressBar: false,
      //       closeOnClick: true,
      //       pauseOnHover: true,
      //       draggable: true
      //     });
      //   }

      //   this.setState({
      //     oldpass:"",
      //     newpass:"",
      //     confirmnewpassword:"",
      //     isLoading: false
      //   });
      //   this.props.history.push("/changepassword");
      // })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.Status) {
          toast.dismiss();
          toast.info("✔️ Password has been changed", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning(
            "❗ " + jsn.Message,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            }
          );
        }
        this.setState({
          oldpass: "",
          newpass: "",
          confirmnewpassword: "",
          isLoading: false
        });
        this.props.history.push("/changepassword");
      })
      .catch(error => {
        toast.dismiss();
        toast.warning("❗ Something error, password didn't changed", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        this.setState({ isLoading: false });
        console.log("parsing failed", error);
        this.props.history.push("/changepassword");
      });
  };

  render() {
    const {
      isError,
      messageError,
      newpass,
      oldpass,
      confirmnewpassword,
      isLoading
    } = this.state;
    return (
      <BlockUi
        tag="div"
        blocking={isLoading}
        loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
        message="Loading, please wait"
      >
        <Header />
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <div className="col-md-8 col-md-offset-2 col-xs-12 col-sm-12">
            <div className="panel panel-primary">
              <div className="panel-heading text-center">
                <h4 className="panelheader">Change Password</h4>
              </div>
              <div className="panel-body" id="sampleLayout">
                <div className="row">
                  <div className="col-md-12">
                    <form id="FChaPas" onSubmit={this.onSubmitChangePass}>
                      <div className="form-group hidden">
                        <div className="input-group">
                          <select className="form-control"> </select>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            onChange={this.onChangeFunction}
                            type="password"
                            style={{ marginBottom: "0px" }}
                            name="oldpass"
                            value={oldpass}
                            className="form-control "
                            placeholder="Enter old password"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            onChange={this.onChangeFunction}
                            type="password"
                            name="newpass"
                            value={newpass}
                            className="form-control "
                            placeholder="Enter new password"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            onChange={this.onChangeFunction}
                            type="password"
                            name="confirmnewpassword"
                            value={confirmnewpassword}
                            className="form-control "
                            placeholder="Enter confirm new password"
                          />
                        </div>
                      </div>
                      {isError ? (
                        <div className="col-md-12">
                          <div
                            className="alert alert-warning"
                            id="errorNoteQuestion"
                          >
                            <strong>Warning!</strong> {messageError}.
                          </div>
                        </div>
                      ) : null}
                      <div className="text-center">
                        <button
                          type="submit"
                          className="btn btn-sm btn-success hidden-xs hidden-sm"
                        >
                          <i className="fa fa-check-square" /> Save
                        </button>
                        <button
                          type="submit"
                          className="form-control btn btn-sm btn-success hidden-md hidden-lg"
                        >
                          <i className="fa fa-check-square" /> Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <Footer /> */}
      </BlockUi>
    );
  }
}

export default withRouter(HalChangePassword);
