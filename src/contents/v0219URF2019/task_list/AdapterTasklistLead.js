import React, { Component } from "react";
import "../../../css/a2is.css";
import { Util } from "../../../otosalescomponents";

class AdapterTaskListLead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataItems: this.props.dataItems
      // listId: this.props.listId
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.dataItems != this.props.dataItems) {
      this.setState({ dataItems: this.props.dataItems });
    }
  }

  onClickItem = id => {
    this.props.onClickItem(id);
  };

  render() {
    console.log("On Adapter");
    // console.log(this.state.dataItems);
    console.log("Containt Pages = " + this.props.rowscount);
    let texts = this.state.dataItems.map((data, index) => {
      if (index == 0) {
        // console.log("data index 0 = ");
        // console.log(data);
        // console.log(data);
      }

      if(Util.isNullOrEmpty(data)){
        return <div/>;
      }

      let style = {};
      if (data.isHotProspect == 1) {
        style = { color: "rgb(213, 159, 0)", fontWeight: "bold" };
      }
      return (
        <div
          className="col-md-12 col-xs-12 panel-body-list"
          key={index}
          style={{ fontSize: "10pt" }}
        >
          <div className="panel panel-default">
            <div
              className="panel-body panel-body-list"
              onClick={() => this.props.onClickListItem(data.CustID, data.ReferenceNo, data.PolicyNo)}
            >
              <div
                className="col-xs-12 panel-body-list"
                style={{ paddingBottom: 10 }}
              >
                <div className="col-xs-10">{data.TaskStatus}</div>
                <div
                  className="col-xs-12"
                  style={style}
                  id={"textCustPolNo-" + (index + 1)}
                >
                  {data.ProspectName}
                  {!Util.isNullOrEmpty(data.PolicyNo) ? " - "+data.PolicyNo : ""}
                </div>
                <div className="col-xs-12" id={"textFU-" + (index + 1)}>
                  {data.FollowUpDesc} - {data.FollowUpReason}
                </div>
                { (data.IsRenewal == 1) &&
                  <React.Fragment>
                <div className="col-xs-6">Expired Date</div>
                <div
                  className="col-xs-6 text-right"
                  id={"textValueExpireDate-" + (index + 1)}
                >
                  {Util.formatDate(data.ExpiredDate, "dd mmmm yyyy")}
                </div>
                </React.Fragment>
                }
                <div className="clearfix" />
                <div className="col-xs-6">Call frequency</div>
                <div
                  className="col-xs-6 text-right"
                  id={"textValueCallFreq" + (index + 1)}
                >
                  {data.LastSeqNo}
                </div>
                <div className="clearfix" />
                {!Util.isNullOrEmpty(data.WAStatusDes) && 
                  <React.Fragment>  
                    <div className="col-xs-12">{data.WAStatusDes}</div>
                    <div className="clearfix" />
                  </React.Fragment>
                }
                <div
                  className="col-xs-12"
                  style={{ paddingBottom: 3, paddingTop: 3 }}
                >
                  <div className="col-xs-6" style={{ paddingLeft: 0 }}>
                    LAST FOLLOW UP
                  </div>
                  <div
                    className="col-xs-6 text-right"
                    style={{ paddingRight: 0 }}
                  >
                    {Util.isNullOrEmpty(data.LastFollowUpDate)
                      ? "-"
                      : Util.formatDate(
                          data.LastFollowUpDate,
                          "dd mmm yyyy, hh:MM"
                        )}
                  </div>
                  <div className="clearfix" />
                  <div
                    className="col-xs-12"
                    style={{
                      borderBottomColor: "black",
                      borderBottomWidth: 2,
                      borderBottomStyle: "solid"
                    }}
                  />
                </div>
                <div
                  className="col-xs-12"
                  style={{ paddingBottom: 3, paddingTop: 3 }}
                >
                  <div className="col-xs-6" style={{ paddingLeft: 0 }}>
                    NEXT FOLLOW UP
                  </div>
                  <div
                    className="col-xs-6 text-right"
                    style={{ paddingRight: 0 }}
                  >
                    {Util.isNullOrEmpty(data.NextFollowUpDate)
                      ? "-"
                      : Util.formatDate(
                          data.NextFollowUpDate,
                          "dd mmm yyyy, hh:MM"
                        )}
                  </div>
                  <div className="clearfix" />
                  <div
                    className="col-xs-12"
                    style={{
                      borderBottomColor: "black",
                      borderBottomWidth: 2,
                      borderBottomStyle: "solid"
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default AdapterTaskListLead;
