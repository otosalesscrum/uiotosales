import React from "react";
import { withRouter } from "react-router-dom";
import {
  OtosalesIFrame
} from "../../../../otosalescomponents";


function PageTasklistMVGodig(props) {

    return (
        <React.Fragment>
            <OtosalesIFrame 
                id={props.id || null}
                onLoad={props.onLoad || (() => {})} 
                url={props.url || "http://localhost"} 
                width="100%" 
                getCurrentURL={props.getCurrentURL || (() => {})}
                height={(window.innerHeight*4/5) || 1000} 
                />
        </React.Fragment>
    );
}

export default withRouter(PageTasklistMVGodig);

