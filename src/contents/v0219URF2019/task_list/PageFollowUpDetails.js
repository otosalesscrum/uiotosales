import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  OtosalesSelectFullview,
  OtosalesDatePicker,
  OtosalesTimePicker,
  OtosalesListFollowUpHistory,
  OtosalesModalInfo,
  OtosalesModalSenttoSA,
  OtosalesLoading,
  OtosalesModalInputNumberTelerenewal,
  OtosalesModalSendEmailCkEditor,
  OtosalesModalSendWA,
  OtosalesModalSendSMSCkEditor,
  Util,
  Log
} from "../../../otosalescomponents";
import {
  API_URL,
  API_VERSION,
  API_VERSION_2,
  HEADER_API,
  ACCOUNTDATA,
  AVAYAPIN,
  API_VERSION_0213URF2019,
  API_VERSION_0219URF2019,
  API_VERSION_0008URF2020,
  API_VERSION_0001URF2020
} from "../../../config";
import { ToastContainer, toast } from "react-toastify";
import Header from "../../../components/Header";
import Modal from "react-responsive-modal";
import {
  Call,
  sendEmailv2,
  sendSMS,
  sendWA,
  savedisk,
  checklist,
  hangupphone
} from "../../../assets";
import NumberFormat from "react-number-format";
import * as Parameterized from "../../../parameterizedata";
import moment from "moment";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";

//flag
class PageFollowUpDetails extends Component {
  constructor(props) {
    super(props);

    Log.debugStr(
      "secureStorage TASKLIST:" +
        JSON.parse(secureStorage.getItem("tasklist-followupdetail-FUStatus"))
    );

    var nextFU = Util.convertDate();
    var lastFU = null;

    this.state = {
      width: 0,
      height: 0,

      isMVGodig: 0,

      showModalSendEmail: false,
      showModalSendWA: false,
      showModalSendEmailWA: false,
      showModalSendSMS: false,
      showModalCTICall: false,
      showModalCTICallInput: false,

      connectedModalInputPhoneNo: false,
      modalchecklistrejectreasonInputNo: "",
      modalchecklistrejectreason: "",

      flagCanCallNext: true,

      SubjectTemplateEmail: "",
      BodyTemplateSendEmail: "",
      EmailToTemplateSendEmail: "",
      SubjectTemplateEmailWA: "",
      BodyTemplateSendEmailWA: "",
      BodyTemplateSendEmailSMSTelerenewal: "",

      Parameterized: Parameterized,

      dataAttachmentEmail1: [],
      AttachmentEmail1: "",
      typeAttachmentEmail1: "",
      base64AttachmentEmail1: "",
      dataAttachmentEmail2: [],
      AttachmentEmail2: "",
      typeAttachmentEmail2: "",
      base64AttachmentEmail2: "",
      dataAttachmentEmail3: [],
      AttachmentEmail3: "",
      typeAttachmentEmail3: "",
      base64AttachmentEmail3: "",
      dataAttachmentEmail4: [],
      AttachmentEmail4: "",
      typeAttachmentEmail4: "",
      base64AttachmentEmail4: "",
      dataAttachmentEmail5: [],
      AttachmentEmail5: "",
      typeAttachmentEmail5: "",
      base64AttachmentEmail5: "",

      dataphonelist: [
        // { value: "088390824", label: "sjldfsfsdf" },
        // { value: "028390821", label: "sjldfsfsdd" },
        // { value: "028390822", label: "sjldfsfsd3" },
        // { value: "088390823", label: "sjldfsfsd1" }
      ],
      dataGetPhoneList: [],
      phonelistselect: null,

      isChangeEmail: false,
      emailProspect: "",
      datarelationship: [],
      datarelationsshipraw: [],
      datacallrejectreason: [],
      datacallrejectreasonraw: [],
      phonenumbertype: "",
      ColumnNameInsertUpdatePhone: "",
      titleInputUpdateTelerenewal: "Add Phone Number",
      showModalAddPhone: false,
      showInputPhoneNo: false,
      issetprimarymodal: false,
      phonePicNameModal: "",
      isEnableEditPicNameModal: true,
      userRole: JSON.parse(ACCOUNTDATA).UserInfo.User.Role,
      callCTIPhone: "",
      avayaPassword: "42685",
      CTIStatus: null,
      CTIStatusIsDelivered: null,

      selectedQuotation: null,
      dataquotationhistory: [],
      showModal: false,
      isLoading: false,
      isErrorSave: false,
      messageError: "",
      isErrNextFUDateTime: false,
      isErrFUStatus: false,
      isErrFUStatusInfo: false,
      isErrLastFUPerson: false,
      isErrRemarks: false,
      picWa: "",
      isErrpicWa: false,
      FUStatus: null,
      datafustatus: [],
      FollowUpEditStatus: "",
      FUStatusInfo: "",
      oldFUStatusInfo: "",
      datafustatusinfo: [],
      statusinfodetail: "",
      isErrStatusInfoDetail: false,
      lastFollowUpDate: lastFU,
      lastFollowUpTime: lastFU,
      nextFollowUpDate: nextFU,
      nextFollowUpTime: nextFU,
      lastFUPerson: "",
      remarks: "",

      MessageAlertCover: "",
      showModalInfo: false,
      isRenewal: "",
      OrderNo: "",
      nsaResult: "",
      isMandatory: true,

      statusCodeBefore: "",
      statusInfoCodeBefore: "",
      isRenewal: "",
      needSurvey: "",
      needNSA: "",
      followUpNo: "",

      FollowUpNo: "",
      datafollowup: [],
      FollowUp: [],
      FollowUpNotes: "",
      isErrFollowUpNotes: false,
      datafollowupnotes: [],
      ProspectCustomer: null,
      ProspectCompany: [],
      FUStatus: "",
      isNeedSurvey: false,
      isNeedNSA: false,
      checkDocumentValidation: false,
      datasFUResult: [],
      dataFUHistory: [],
      datas: [
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode: "paytode",
          paytoname: "paytoname",
          accountinfobankname: "accountinfobankname",
          accountinfoaccountno: "accountinfoaccountno"
        }
      ],
      datastatusinfoall: [],
      dataphonelistold: ["Loading . . .", "Loading . . ."]
    };
    console.log(this.state.userRole);
  }

  initializeParameterizedData = () => {
    Parameterized.getApplicationParam("PREFIX-LANDLINENO", 0, data => {
      Parameterized.setLandLinePrefix(data, () => {
        import(`../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });

    Parameterized.getApplicationParam("PREFIX-PHONENO", 0, data => {
      Parameterized.setMobilePrefix(data, () => {
        import(`../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });

    Parameterized.getApplicationParam("ROLECTI", 0, data => {
      Parameterized.setRoleCTI(data, () => {
        import(`../../../parameterizedata`).then(Parameterized =>
          this.setState({ Parameterized })
        );
      });
    });
  };

  componentDidMount() {
    this.setState({
      Parameterized: Parameterized
    });

    if (Util.isNullOrEmpty(secureStorage.getItem("SelectedTaskDetail"))) {
      this.props.history.push("tasklist");
      return;
    }

    this.initializeParameterizedData();

    var SelectedTaskDetail = JSON.parse(
      secureStorage.getItem("SelectedTaskDetail")
    );
    this.setState(
      {
        SelectedTaskDetail
      },
      () => {
        this.getProspectCustomer(SelectedTaskDetail.CustID);
      }
    );
    this.setState(
      {
        account: JSON.parse(ACCOUNTDATA)
      },
      () => {
        if (!Util.isNullOrEmpty(this.state.account)) {
          if (
            Util.stringArrayContains(
              this.state.account.UserInfo.User.Role,
              this.state.Parameterized.roleCTI
            )
          ) {
            this.getAVAYAUserTelerenewal();
          }
        }
      }
    );

    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  getPhoneList = (
    OrderNo = this.state.OrderNo,
    showNotification = false,
    callback = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetPhoneList/`,
      "POST",
      HEADER_API,
      {
        OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        let dataphonelist = jsn.data.map(data => {
          this.setState({
            [`statusphone${data.NoHp}`]: !Util.isNullOrEmpty(
              this.state[`statusphone${data.NoHp}`]
            )
              ? this.state[`statusphone${data.NoHp}`]
              : null // initialize state status phone
          });
          return {
            value: data.NoHp,
            label: Util.maskingPhoneNumberv2(Util.clearPhoneNo(data.NoHp))
          };
        });
        // if (jsn.data.length > 0) {
        this.setState(
          {
            dataphonelist,
            dataGetPhoneList: jsn.data
          },
          () => {
            if (
              showNotification &&
              [...this.state.dataphonelist].filter(data =>
                Util.detectPrefix(
                  Util.clearPhoneNo(data.value),
                  this.state.Parameterized.mobilePrefix
                )
              ).length == 0
            ) {
              Util.showToast(
                "Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.",
                "WARNING"
              );
            }
            callback();
          }
        );
        // }
        // else {
        //   if(showNotification){
        //     Util.showToast(
        //       "Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.",
        //       "WARNING"
        //     );
        //   }
        // }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  endCallTelerenewal = (callCTIPhone = this.state.callCTIPhone) => {
    this.callTrayAPIOnDrop(callCTIPhone);
    setTimeout(() => {
      clearInterval(this.intervalGetStatusCTI);
    }, 5000);
  };

  callTelerenewal = () => {
    if (this.state.dataphonelist.length > 0) {
      this.setState(
        {
          indexPhoneCallCTI: 0
        },
        () => {
          this.getCallRejectReason(() => {
            this.setState({
              showModalCTICall: true
            });
            this.setState(
              {
                callCTIPhone: this.state.dataphonelist[0].value
              },
              () => {
                this.callTrayAPIOnDial(this.state.callCTIPhone);
                this.startIntervalGetStatus();
              }
            );
          });
        }
      );
    } else {
      Util.showToast(
        "Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.",
        "WARNING"
      );
    }
  };

  callTrayAPIOnDial = (PhoneNumber, callback = () => {}) => {
    // Util.fetchAPIAdditional(
    //   `http://localhost:60024/dial/telp=9,${PhoneNumber},${this.state.avayaPassword}`,
    //   "POST",
    //   null
    // )
    PhoneNumber = PhoneNumber.trim();
    PhoneNumber = Util.replacePrefix(PhoneNumber, ["62", "+62"], "0");
    PhoneNumber = PhoneNumber.replace(new RegExp(" ", "gm"), "");
    // PhoneNumber = PhoneNumber.replace(/[^\d]/g, '');
    PhoneNumber = PhoneNumber.replace(/[^+\d]+/g, "");
    fetch(
      `http://localhost:60024/dial/telp=9,${PhoneNumber},${AVAYAPIN ||
        this.state.avayaPassword}`,
      {
        method: "POST"
      }
    )
      .then(res => res.json())
      .then(data => {
        Log.debugGroupCollapsed("CTI Call", data);
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        // if((error+"").toLowerCase().includes("CONNECTION_REFUSED".toLowerCase())){
        Util.showToast("Make sure AVAYA and CTI Tray is running", "WARNING");
        // }
      });
  };

  callTrayAPIOnDrop = (PhoneNumber, callback = () => {}) => {
    // Util.fetchAPIAdditional(
    //   `http://localhost:60024/dial/telp=9,${PhoneNumber},${this.state.avayaPassword}`,
    //   "POST",
    //   null
    // )
    PhoneNumber = PhoneNumber.trim();
    PhoneNumber = Util.replacePrefix(PhoneNumber, ["62", "+62"], "0");
    PhoneNumber = PhoneNumber.replace(new RegExp(" ", "gm"), "");
    // PhoneNumber = PhoneNumber.replace(/[^\d]/g, '');
    PhoneNumber = PhoneNumber.replace(/[^+\d]+/g, "");
    fetch(
      `http://localhost:60024/drop/telp=9,${PhoneNumber},${AVAYAPIN ||
        this.state.avayaPassword}`,
      {
        method: "POST"
      }
    )
      .then(res => res.json())
      .then(data => {
        Log.debugGroupCollapsed("CTI Call", data);
        callback();
        this.setState(
          {
            flagCanCallNext: false
          },
          () => {
            setTimeout(() => {
              this.setState({
                flagCanCallNext: true
              });
            }, 15000);
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        // if((error+"").toLowerCase().includes("net::ERR_CONNECTION_REFUSED".toLowerCase())){
        Util.showToast("Make sure AVAYA and CTI Tray is running", "WARNING");
        // }
      });
  };

  startIntervalGetStatus = () => {
    this.intervalGetStatusCTI = setInterval(() => {
      this.getStatusCTI();
    }, 2000);
  };

  getStatusCTI = () => {
    fetch(`http://localhost:60024/getStatus/`, {
      method: "POST"
    })
      .then(res => res.json())
      .then(data => {
        if (!Util.isNullOrEmpty(data.Message)) {
          this.setState({
            CTIStatus: data.Message,
            CTIStatusIsDelivered: data.isDelivered
          });
        }
        Log.debugGroupCollapsed("CTI Call", data);
      })
      .catch(error => {
        Log.error("Error : ", error);
        clearInterval(this.intervalGetStatusCTI);
      });
  };

  callTrayNext = () => {
    if (Util.isNullOrEmpty(this.state.modalchecklistrejectreason)) {
      Util.showToast(
        "Silahkan pilih checkbox remarks terlebih dahulu",
        "WARNING"
      );
      return;
    }

    let DataPhone = this.searchDataPhoneFromPhonelist(this.state.callCTIPhone);

    let dataReasonReject = this.searchDataReasonReject(
      this.state.modalchecklistrejectreason
    );

    let PhoneNoParam = [
      {
        PhoneNo: Util.clearPhoneNo(this.state.callCTIPhone),
        ColumnName: DataPhone.Column_Name,
        Relation: DataPhone.RelationshipId,
        IsPrimary: false,
        IsWrongNumber: dataReasonReject.IsValid ? false : true,
        IsLandLine: this.detectLandLinePhone(
          Util.clearPhoneNo(this.state.callCTIPhone)
        ),
        isConnected: dataReasonReject.IsValid,
        PICName: DataPhone.PICName,
        CallRejectReasonId: this.state.modalchecklistrejectreason
      }
    ];

    this.InsertUpdatePhoneNo(
      this.state.account.UserInfo.User.SalesOfficerID,
      this.state.OrderNo,
      PhoneNoParam,
      () => {
        this.setState({
          modalchecklistrejectreason: null
        });
        this.callTrayAPIOnDrop(this.state.callCTIPhone, () => {
          if (
            this.state.indexPhoneCallCTI + 1 <
            this.state.dataphonelist.length
          ) {
            this.setState(
              {
                indexPhoneCallCTI: this.state.indexPhoneCallCTI + 1,
                callCTIPhone: this.state.dataphonelist[
                  this.state.indexPhoneCallCTI + 1
                ].value
              },
              () => {
                // this.timeoutNextCall = setTimeout(() => {
                this.intervaNextCall = setInterval(() => {
                  // this.callTrayAPIOnDrop(this.state.callCTIPhone);
                  Log.debugStr("CTI Status : " + this.state.CTIStatus);
                  Log.debugStr(
                    "CTI Status IsDelivered : " +
                      this.state.CTIStatusIsDelivered
                  );
                  // if (
                  //   Util.stringEquals(this.state.CTIStatus, "Ready")
                  // ) {
                  this.callTrayAPIOnDial(this.state.callCTIPhone);
                  clearInterval(this.intervaNextCall);
                  // }
                }, 1000);
                // }, 4000);
              }
            );
          } else {
            Util.showToast("Tidak terdapat nomor untuk dihubungi", "WARNING");
            // clearTimeout(this.timeoutNextCall);
            this.endCallTelerenewal(this.state.callCTIPhone);
            this.onCloseModalCTICall();
          }
        });
      }
    );
  };

  callTrayConnected = () => {
    let DataPhone = this.searchDataPhoneFromPhonelist(this.state.callCTIPhone);

    let PhoneNoParam = [
      {
        PhoneNo: Util.clearPhoneNo(this.state.callCTIPhone),
        ColumnName: DataPhone.Column_Name,
        Relation: DataPhone.RelationshipId,
        IsPrimary: false,
        IsWrongNumber: false,
        IsLandLine: this.detectLandLinePhone(
          Util.clearPhoneNo(this.state.callCTIPhone)
        ),
        isConnected: true,
        PICName: DataPhone.PICName,
        CallRejectReasonId: 0
      }
    ];

    this.InsertUpdatePhoneNo(
      this.state.account.UserInfo.User.SalesOfficerID,
      this.state.OrderNo,
      PhoneNoParam,
      () => {
        this.setState({
          modalchecklistrejectreason: null
        });
        this.onCloseModalCTICall();
      }
    );
  };

  getAVAYAUserTelerenewal = () => {
    // this.setState({ showModal: false });
    // this.setState({
    //   isLoading: true
    // });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetAVAYAUserTelerenewal/`,
      "POST",
      HEADER_API,
      {
        SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          if (json.data.length > 0) {
            this.setState({
              avayaPassword: json.data[0].Password
            });
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get pin - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        // this.setState({
        //   isLoading: false
        // });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        // this.setState({
        //   isLoading: false
        // });
      });
  };

  // hit to get template email
  getSendEmailTemplate = orderNo => {
    this.setState({ showModal: false });
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL +
        "" +
        API_VERSION_0219URF2019}/DataReact/GetButtonEmailTemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({
            showModalSendEmail: true
          });
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState({
              EmailToTemplateSendEmail: json.data.EmailTo,
              SubjectTemplateEmail: json.data.Subject,
              BodyTemplateSendEmail: json.data.Body.replace(
                "<html><body>",
                "<p>"
              )
                .replace("</body></html>", "</p>")
                .replace(new RegExp("\\<b>", "gm"), "<strong>")
                .replace(new RegExp("\\</b>", "gm"), "</strong>")
                .replace(
                  "#InformasiPembayaran",
                  !Util.isNullOrEmpty(this.state.paymentnorek)
                    ? this.state.paymentnorek
                    : this.state.paymentvapermata
                )
                .trim()
              // BodyTemplateSendEmail: Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")
            });
          }
          this.setState({ showModal: false });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit to get template email WA
  getSendEmailTemplateWA = orderNo => {
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetButtonWATemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState(
              {
                SubjectTemplateEmailWA: json.data.Subject,
                BodyTemplateSendEmailWA: json.data.Body.replace(
                  "<html><body>",
                  "<p>" +
                    // Util.stringParse(
                    //   this.state.Parameterized.additionalWA,
                    //   Util.maskingPhoneNumberv2(
                    //     this.state.phonelistselect,
                    //     this.state.Parameterized.prefixPhone,
                    //     4
                    //   )
                    // )+
                    "<br/>"
                  //+
                  // Util.stringParse(
                  //   this.state.Parameterized.additionalWA,
                  //   this.state.phonelistselect
                  // )
                )
                  .replace("</body></html>", "</p>")
                  .replace(new RegExp("\\<b>", "gm"), "<strong>")
                  .replace(new RegExp("\\</b>", "gm"), "</strong>")
                  .replace(
                    "#InformasiPembayaran",
                    " " +
                      (!Util.isNullOrEmpty(this.state.paymentnorek)
                        ? this.state.paymentnorek
                        : this.state.paymentvapermata)
                  )
                  .trim(),
                EmailToTemplateWA: json.data.EmailTo
                // BodyTemplateSendEmail: Util.stringParse(this.state.Parameterized.emailBodyNewCTI, "[Product Name]", "[Product Name]", "[Brand Name] [Model] [Type]", "[Sales-officer-name]", "[sales-officer-email]", "[sales-officer-phone no]")
              },
              () => {
                this.setState({
                  showModalSendEmailWA: true
                });
              }
            );
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit to get template email WA
  getSendEmailTemplateSMSTelerenewal = orderNo => {
    this.setState({
      isLoading: true
    });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetButtonSMSTemplate/`,
      "POST",
      HEADER_API,
      {
        OrderNo: orderNo
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({
            showModalSendSMS: true
          });
          if (!Util.isNullOrEmpty(json.data)) {
            this.setState({
              BodyTemplateSendEmailSMSTelerenewal: json.data.Body.replace(
                "<html><body>",
                "<p>"
              )
                .replace("</body></html>", "</p>")
                .replace(new RegExp("\\<b>", "gm"), "<strong>")
                .replace(new RegExp("\\</b>", "gm"), "</strong>")
                .replace(
                  "#InformasiPembayaran",
                  !Util.isNullOrEmpty(this.state.paymentnorek)
                    ? this.state.paymentnorek
                    : this.state.paymentvapermata
                )
                .trim()
            });
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal get template email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal get template email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  handleUploadFile = (name, value, rejected) => {
    // this.setState({ isLoading: true });
    if (rejected.length > 0) {
      Util.showToast(
        "Please upload file with extension pdf, jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx!!",
        "warning"
      );
      return;
    }

    if (value.length > 0) {
      this.setState({ [`data${name}`]: value });
      Util.getBase64(value[0], data => {
        // console.log("hasil :", data)
        this.setState({
          [`type${name}`]: Util.base64MimeType(data),
          [`base64${name}`]: data
        }); // set mime type file
        // Util.base64MimeType(data);
        // Util.base64Only(data);
      });
    }
  };

  onOptionSelect2Change = (value, name) => {
    this.setState({ [name]: value });
  };

  handleDeleteFile = name => {
    this.setState({
      [name]: "",
      [`data${name}`]: [],
      [`type${name}`]: "",
      [`base64${name}`]: ""
    });
  };

  loadingDot = () => {
    let date = Util.convertDate();
    if (date % 3 == 0) {
      return ". . .";
    } else if (date % 3 == 1) {
      return ". .";
    } else if (date % 3 == 2) {
      return ".";
    }
  };

  selectedPhoneNumberCTI = () => {
    let phone = null;

    let datas = [...this.state.dataphonelist].filter(data =>
      Util.detectPrefix(
        Util.clearPhoneNo(data.value),
        this.state.Parameterized.mobilePrefix
      )
    );

    if (datas.length > 0) {
      phone = datas[0].value;
    }

    return phone;
  };

  onCloseModalSendEmail = () => {
    this.setState({ showModalSendEmail: false });
  };

  onCloseModalSendWA = () => {
    this.setState({ showModalSendWA: false });
  };

  onCloseModalSendEmailWA = () => {
    this.setState({ showModalSendEmailWA: false });
  };

  onCloseModalSendSMS = () => {
    this.setState({ showModalSendSMS: false });
  };

  onCloseModalCTICall = () => {
    this.setState({ showModalCTICall: false });
  };

  onCloseModalCTICallInput = () => {
    this.setState({ showModalCTICallInput: false });
  };

  endCallModalInputPhoneNo = callCTIPhoneInputPhone => {
    // if (Util.isNullOrEmpty(this.state.modalchecklistrejectreason)) {
    //   Util.showToast(
    //     "Silahkan pilih checkbox remarks terlebih dahulu",
    //     "WARNING"
    //   );
    //   return;
    // }
    this.setState({
      modalchecklistrejectreasonInputNo: this.state.modalchecklistrejectreason,
      modalchecklistrejectreason: ""
    });
    // this.endCallTelerenewal(callCTIPhoneInputPhone);
    setTimeout(() => {
      clearInterval(this.intervalGetStatusCTI);
    }, 5000);
    this.onCloseModalCTICallInput();
  };

  connectedModalInputPhoneNo = callCTIPhoneInputPhone => {
    this.setState({
      connectedModalInputPhoneNo: true
    });
    // this.endCallTelerenewal(callCTIPhoneInputPhone);
    setTimeout(() => {
      clearInterval(this.intervalGetStatusCTI);
    }, 5000);
    this.onCloseModalCTICallInput();
  };

  setModelAttachmentEmail = (
    Seq,
    AttachmentID,
    AttachmentDescription,
    FileByte,
    FileExtension,
    FileContentType
  ) => {
    return {
      Seq,
      AttachmentID,
      AttachmentDescription,
      FileByte,
      FileExtension,
      FileContentType
    };
  };

  // hit senquotationemail TELERENEWAL
  sendQuotationSMSTelerenewal = (PhoneNo, SMSText, callback = () => {}) => {
    // this.setState({ isLoading: true });
    // toast.dismiss();
    // toast.info("Send SMS to " + PhoneNo, {
    //   position: "top-right",
    //   autoClose: 5000,
    //   hideProgressBar: false,
    //   closeOnClick: true,
    //   pauseOnHover: true,
    //   draggable: true
    // });
    PhoneNo = Util.replacePrefix(PhoneNo, ["62", "+62"], "0");
    PhoneNo = PhoneNo.replace(new RegExp(" ", "gm"), "");
    this.setState({ showModal: false });
    let seq = 0;
    SMSText = (SMSText + "")
      .replace(/<[^>]*>/g, "")
      .replace(/&nbsp;/g, " ")
      .trim();
    // .replace(/\n/g, "%0a")

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendViaSMS/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        PhoneNo,
        SMSText
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ SMS berhasil dikirim! ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send SMS - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send SMS - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  // hit sendquotationWA
  sendQuotationWA = (OrderNo = this.state.OrderNo) => {
    this.getSendEmailTemplateWA(OrderNo);
  };

  // hit senquotationemail TELERENEWAL
  sendQuotationEmailTelerenewal = (
    EmailTo,
    Subject,
    Body,
    OrderNo,
    ListAtt = [],
    callback = () => {}
  ) => {
    this.setState({ showModal: false });
    let seq = 0;

    Body = (Body + "")
      .replace(new RegExp("\\<br>", "gm"), "<br/>")
      .replace(new RegExp("<p>|</p>", "gm"), "")
      .replace(new RegExp("\\/n", "gm"), "");

    Body = "<html><body>" + Body;
    Body =
      Body.replace(
        "<html><body>",
        this.state.Parameterized.stylingSendEmailWA
      ) + "</body></html>";

    if (this.state.showModalSendEmailWA) {
      Body = Body.replace(
        this.state.Parameterized.stylingSendEmailWA,
        this.state.Parameterized.stylingSendEmailWA +
          Util.stringParse(
            this.state.Parameterized.additionalWA,
            this.state.phonelistselect
          ) +
          "<br/>"
      );
      // .replace("<html><body>", this.state.Parameterized.stylingSendEmailWA)
      // .replace(
      //   Util.stringParse(
      //     this.state.Parameterized.additionalWA,
      //     Util.maskingPhoneNumberv2(
      //       this.state.phonelistselect,
      //       this.state.Parameterized.prefixPhone,
      //       4
      //     )
      //   ),
      //   Util.stringParse(
      //     this.state.Parameterized.additionalWA,
      //     this.state.phonelistselect
      //   )
      // );
    }

    let sizeAll = 0;

    for (let i = 1; i <= 5; i++) {
      const element = this.state[`dataAttachmentEmail${i}`];
      // if(!Util.isNullOrEmpty(element)){
      if (element.length > 0) {
        // if (element[0].size > this.state.Parameterized.maxSizeAttachmentEmail) {
        //   Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING");
        //   return;
        // }

        sizeAll += element[0].size;

        if (sizeAll > this.state.Parameterized.maxSizeAttachmentEmail) {
          Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING");
          return;
        }

        ListAtt.push(
          this.setModelAttachmentEmail(
            seq,
            null,
            this.state[`dataAttachmentEmail${i}`][0].name.substring(
              0,
              this.state[`dataAttachmentEmail${i}`][0].name.indexOf(
                Util.getExtensionFile(
                  this.state[`dataAttachmentEmail${i}`][0].name
                )
              ) - 1
            ), // name file // i,
            Util.base64Only(this.state[`base64AttachmentEmail${i}`]),
            "." +
              Util.getExtensionFile(
                this.state[`dataAttachmentEmail${i}`][0].name
              ), // "." + (this.state[`typeAttachmentEmail${i}`] + "").split("/")[1],
            this.state[`typeAttachmentEmail${i}`]
          )
        );
      }
    }
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/SendViaEmail/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        EmailTo,
        EmailCC: "",
        EmailBCC: "",
        Subject,
        Body,
        OrderNo,
        ListAtt,
        IsAddAtt: false // flag enable attachment RDL
      }
    )
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ Email berhasil dikirim !", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send Email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send Email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  // hit sendquotation WA
  sendQuotationEmailWACTI = (
    EmailTo,
    Subject,
    Body,
    OrderNo,
    ListAtt = [],
    callback = () => {}
  ) => {
    this.setState({ showModal: false });
    let seq = 0;

    Body = (Body + "")
      .replace(new RegExp("\\<br>", "gm"), "<br/>")
      .replace(new RegExp("<p>|</p>", "gm"), "")
      .replace(new RegExp("\\/n", "gm"), "");

    Body = "<html><body>" + Body;
    Body =
      Body.replace(
        "<html><body>",
        this.state.Parameterized.stylingSendEmailWA
      ) + "</body></html>";

    if (this.state.showModalSendEmailWA) {
      Body = Body.replace(
        this.state.Parameterized.stylingSendEmailWA,
        this.state.Parameterized.stylingSendEmailWA +
          Util.stringParse(
            this.state.Parameterized.additionalWA,
            this.state.phonelistselect
          ) +
          "<br/>"
      );
    }

    let sizeAll = 0;

    for (let i = 1; i <= 5; i++) {
      const element = this.state[`dataAttachmentEmail${i}`];
      // if(!Util.isNullOrEmpty(element)){
      if (element.length > 0) {
        // if (element[0].size > this.state.Parameterized.maxSizeAttachmentEmail) {
        //   Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING");
        //   return;
        // }

        sizeAll += element[0].size;

        if (sizeAll > this.state.Parameterized.maxSizeAttachmentEmail) {
          Util.showToast("Attachment tidak dapat melebihi 10 MB", "WARNING");
          return;
        }

        ListAtt.push(
          this.setModelAttachmentEmail(
            seq,
            null,
            this.state[`dataAttachmentEmail${i}`][0].name.substring(
              0,
              this.state[`dataAttachmentEmail${i}`][0].name.indexOf(
                Util.getExtensionFile(
                  this.state[`dataAttachmentEmail${i}`][0].name
                )
              ) - 1
            ), // name file // i,
            Util.base64Only(this.state[`base64AttachmentEmail${i}`]),
            "." +
              Util.getExtensionFile(
                this.state[`dataAttachmentEmail${i}`][0].name
              ), // "." + (this.state[`typeAttachmentEmail${i}`] + "").split("/")[1],
            this.state[`typeAttachmentEmail${i}`]
          )
        );
      }
    }
    this.setState({ isLoading: true });

    let EndPointSendWA = null;

    if (
      Util.stringEquals(JSON.parse(ACCOUNTDATA).UserInfo.User.Role, "TELESALES")
    ) {
      EndPointSendWA = Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SendViaWA/`,
        "POST",
        { ...HEADER_API, "Content-Type": "application/json" },
        {
          WAMessage: Body,
          PhoneNo: this.state.phonelistselect,
          OrderNo,
          ListAtt,
          IsAddAtt: true // flag enable attachment RDL
        }
      );
    } else {
      EndPointSendWA = Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/SendViaEmail/`,
        "POST",
        { ...HEADER_API, "Content-Type": "application/json" },
        {
          EmailTo,
          EmailCC: "",
          EmailBCC: "",
          Subject,
          Body,
          OrderNo,
          ListAtt,
          IsAddAtt: false // flag enable attachment RDL
        }
      );
    }
    EndPointSendWA.then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ Berhasil dikirim !", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send WA - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
        callback();
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send WA - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  getProspectCustomer = CustID => {
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/getProspectCustomerEdit/`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        },
        body: "custid=" + CustID
      }
    )
      .then(res => {
        return res.json();
      })
      .then(jsn => {
        console.log("Data", jsn);
        this.setState({ isLoading: false });
        if (jsn.status == 1) {
          // if(!Util.isNullOrEmpty(jsn.PhonePriorityList)){
          let dataphonelistold = jsn.PhonePriorityList;
          // dataphonelist = [
          //   "981274237843274",
          //   "726347234232342",
          //   "234762834723884"
          // ];
          this.setState({
            dataphonelistold
          });
          // }

          let isMVGodig = Util.isNullOrEmpty(
            secureStorage.getItem("tasklist-followupdetail-isMVGodig")
          )
            ? 0
            : secureStorage.getItem("tasklist-followupdetail-isMVGodig");
          this.setState(
            {
              FollowUp: jsn.FollowUp,
              ProspectCustomer: jsn.ProspectCustomer,
              FUStatusInfo: JSON.parse(
                secureStorage.getItem("tasklist-followupdetail-FUStatus")
              ),
              FollowUpNo: jsn.FollowUp.FollowUpNo,
              ProspectCompany: jsn.ProspectCompany,
              SurveySchedule: !Util.isNullOrEmpty(jsn.SurveySchedule)
                ? jsn.SurveySchedule
                : null,
              isRenewal: jsn.FollowUp.IsRenewal,
              OrderNo: jsn.OrderSimulation.OrderNo,
              FollowUpEditStatus: jsn.FollowUp.FollowUpStatus,
              statusInfoCodeBefore: jsn.FollowUp.FollowUpInfo,
              isNeedSurvey:
                jsn.OrderSimulationSurvey.length > 0
                  ? jsn.OrderSimulationSurvey[0].IsNeedSurvey
                  : 0,
              isNeedNSA:
                jsn.OrderSimulationSurvey.length > 0
                  ? jsn.OrderSimulationSurvey[0].IsNSASkipSurvey
                  : 0,
              remarks: jsn.FollowUp.Remark,
              lastFUPerson: jsn.FollowUp.Receiver,
              picWa: jsn.FollowUp.PICWA,
              isMVGodig
            },
            () => {
              if (!Util.isNullOrEmpty(this.state.FollowUp)) {
                if (this.isCTIComponentRender()) {
                  if (!Util.isNullOrEmpty(this.state.OrderNo)) {
                    this.getPhoneList(this.state.OrderNo, false);
                    if (
                      !Util.isNullOrEmpty(
                        secureStorage.getItem(
                          "tasklist-followupdetailstatusphone"
                        )
                      )
                    ) {
                      let datastatusphone = JSON.parse(
                        secureStorage.getItem(
                          "tasklist-followupdetailstatusphone"
                        )
                      );

                      if (this.state.OrderNo == datastatusphone.OrderNo) {
                        this.setState(datastatusphone.objectTemp);
                      }
                    }
                  }
                }
              }

              if (!Util.isNullOrEmpty(this.state.ProspectCustomer)) {
                this.setState({
                  emailProspect: this.state.ProspectCustomer.Email1
                });
              }
              this.loadProspectCustomer(this.state.FollowUp.CustID);
              this.loadFUHistory(jsn.FollowUp.FollowUpNo);
              this.loadFUStatusInfo(
                this.state.FUStatusInfo,
                this.state.FollowUpEditStatus,
                this.state.statusInfoCodeBefore,
                this.state.isRenewal,
                this.state.isNeedSurvey,
                this.state.isNeedNSA,
                this.state.FollowUpNo,
                this.state.isMVGodig
              );
              this.loadFUStatus(
                this.state.FollowUpEditStatus,
                this.state.statusInfoCodeBefore,
                this.state.isRenewal,
                this.state.FollowUpEditStatus,
                this.state.statusInfoCodeBefore,
                this.state.isMVGodig
              );
              this.loadFUStatusInfoAll();
              this.loadFUnotes();
            }
          );
        } else {
          toast.dismiss();
          toast.error("❗ " + jsn.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          this.searchProspectCustomer(CustID);
        }
      });
  };

  showQuotationHistory = FollowUpNo => {
    // console.log("showQuotationHistory followupno : ", FollowUpNo);
    this.setState({ dataquotationhistory: [], isLoading: true });
    if (FollowUpNo != null) {
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + FollowUpNo
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status != false && jsn.data.length > 0) {
            return jsn.data.map(data => ({
              ...data,
              ProspectName: JSON.parse(secureStorage.getItem("PCSelected"))
                .Name,
              FollowUpNo: JSON.parse(secureStorage.getItem("FUSelected"))
                .FollowUpNo
            }));
          } else {
            return [];
          }
        })
        .then(datamapping => {
          this.setState(
            {
              dataquotationhistory: datamapping,
              selectedQuotation: datamapping[0].OrderNo || null,
              showModal: true
            },
            () => {
              if (this.state.dataquotationhistory.length > 0) {
                this.setState({ showModal: true });
              } else {
                // toast.dismiss();
                toast.warning(
                  "❗ There is no quotation for this prospect. Please make quotation first.",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                  }
                );
              }
              this.setState({ isLoading: false });
            }
          );
        })
        .catch(error => {
          console.log("parsing failed", error);
          // this.showQuotationHistory(FollowUpNo);
          this.setState({ isLoading: false });
        });
    } else {
      console.log("FollowUpNo tidak ada");
    }
  };

  loadFUnotes = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpNotes/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "infocode=" + this.state.statusinfodetail
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.NotesCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datafollowupnotes: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatusInfoAll();
        }
      });
  };

  loadFUStatus = (
    followupstatus,
    followupstatusactivity,
    isRenewal,
    followupstatusbefore,
    followupstatusinfobefore,
    isMVGodig = this.state.isMVGodig
  ) => {
    this.setState({ isLoading: true });
    // fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpStatus/`, {
    //   method: "POST",
    //   headers: new Headers({
    //     "Content-Type": "application/x-www-form-urlencoded",
    //     Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
    //   }),
    //   body:
    //     "followupstatus=" +
    //     followupstatus +
    //     "&followupno=" +
    //     this.state.FollowUpNo +
    //     "&followupstatusinfo=" +
    //     followupstatusactivity +
    //     "&isRenewal=" +
    //     isRenewal
    // })
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getFollowUpStatus/`,
      "POST",
      HEADER_API,
      {
        followupstatus,
        followupno: this.state.FollowUpNo,
        followupstatusinfo: followupstatusactivity,
        isRenewal,
        followupstatusbefore,
        followupstatusinfobefore,
        isMVGodig
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.StatusCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datafustatus: datamapping,
          isLoading: false
        });
        if (followupstatusactivity != null && followupstatusactivity != "") {
          this.setState({ FUStatus: followupstatusactivity }, () => {
            this.loadFUStatusInfo(
              this.state.FUStatusInfo,
              this.state.FollowUpEditStatus,
              this.state.statusInfoCodeBefore,
              this.state.isRenewal,
              this.state.isNeedSurvey,
              this.state.isNeedNSA,
              this.state.FollowUpNo,
              this.state.isMVGodig
            );
          });
          // if (followupstatusactivity != followupstatus) {
          //   this.setState({ FUStatusInfo: "" });
          // }
        }
        // this.onOptionFUStatusChange();
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatus(followupstatus);
        }
      });
  };

  loadFUStatusInfo = (
    FUStatus,
    FollowUpEditStatus,
    statusinfocodebefore,
    isRenewal,
    needSurvey,
    needNSA,
    followupno,
    isMVGodig = this.state.isMVGodig
  ) => {
    this.setState({ isLoading: true });
    // fetch(
    //   `${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpDetailsStatusInfo/`,
    //   {
    //     method: "POST",
    //     headers: new Headers({
    //       "Content-Type": "application/x-www-form-urlencoded",
    //       Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
    //     }),
    //     body:
    //       "statuscode=" +
    //       FUStatus +
    //       "&statuscodebefore=" +
    //       FollowUpEditStatus +
    //       "&statusinfocodebefore=" +
    //       statusinfocodebefore +
    //       "&IsRenewal=" +
    //       isRenewal +
    //       "&NeedSurvey=" +
    //       needSurvey +
    //       "&NeedNSA=" +
    //       needNSA +
    //       "&followUpNo=" +
    //       followupno
    //   }
    // )
    Util.fetchAPIAdditional(
      `${API_URL +
        "" +
        API_VERSION_0219URF2019}/DataReact/getFollowUpDetailsStatusInfo/`,
      "POST",
      HEADER_API,
      {
        statuscode: FUStatus,
        statuscodebefore: FollowUpEditStatus,
        statusinfocodebefore,
        IsRenewal: isRenewal,
        NeedSurvey: needSurvey,
        NeedNSA: needNSA,
        followUpNo: followupno,
        isMVGodig
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.InfoCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        if (FUStatus == 6 && this.state.FollowUpEditStatus != undefined) {
          this.setState({ FUStatusInfo: 27 });
        }

        if (FUStatus == 4 && this.state.FollowUpEditStatus != undefined) {
          this.setState({ FUStatusInfo: 30 });
        }

        this.setState({
          datafustatusinfo:
            FUStatus == 6 && this.state.FollowUpEditStatus != undefined
              ? datamapping.filter(data => data.value == 27)
              : datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatusInfo(FUStatus);
        }
      });
  };

  loadFUHistory = FollowUpNo => {
    this.setState({ datas: [] });
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getFollowUpHistory/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "followupno=" + FollowUpNo
    })
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        if (datamapping.length > 0) {
          this.setState({ datas: datamapping });
        }
        this.setState({
          isLoading: false
        });
        // console.log(datamapping);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUHistory(FollowUpNo);
        }
      });
  };

  loadFUStatusInfoAll = () => {
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/getStatusInfoDescriptionAll/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        })
      }
    )
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        this.setState({
          datastatusinfoall: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadFUStatusInfoAll();
        }
      });
  };

  loadProspectCustomer = custid => {
    this.setState({ datas: [] });
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getProspectCustomer/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "custid=" + custid
    })
      .then(res => res.json())
      .then(jsn => jsn.ProspectCustomer)
      .then(datamapping => {
        secureStorage.setItem("PCSelected", JSON.stringify(datamapping));
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!error + "".toLowerCase().includes("token")) {
          // this.loadProspectCustomer(custid);
        }
      });
  };

  onOptionFUStatusChange = FUStatus => {
    this.setState({ FUStatus });
    this.setState({ FUStatusInfo: null });

    this.loadFUStatusInfo(
      this.state.FUStatusInfo,
      this.state.FollowUpEditStatus,
      this.state.statusInfoCodeBefore,
      this.state.isRenewal,
      this.state.isNeedSurvey,
      this.state.isNeedNSA,
      this.state.FollowUpNo,
      this.state.isMVGodig
    );
  };

  onOptionFUStatusInfoChange = FUStatusInfo => {
    this.setState({ FUStatusInfo });

    this.loadFUStatusInfo(
      FUStatusInfo,
      this.state.FollowUpEditStatus,
      this.state.statusInfoCodeBefore,
      this.state.isRenewal,
      this.state.isNeedSurvey,
      this.state.isNeedNSA,
      this.state.FollowUpNo,
      this.state.isMVGodig
    );
  };

  onOptionFUStatusInfoDetailChange = statusinfodetail => {
    this.setState({ statusinfodetail }, () => {
      if (
        (this.state.FUStatusInfo == "2" &&
          this.state.statusinfodetail == "61") ||
        (this.state.FUStatusInfo == "2" &&
          this.state.statusinfodetail == "62") ||
        this.state.FUStatusInfo == "13" ||
        this.state.FUStatusInfo == "14"
      ) {
        this.state.isMandatory = false;
      }
      this.loadFUnotes();
    });
  };

  onOptionFollowUpNotesChange = FollowUpNotes => {
    this.setState({ FollowUpNotes });
  };

  onChangeFunction = (value, name, pattern = "") => {
    // console.log(value);
    // this.setState({
    //   [name]: value
    // });

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({ [name]: value });
      }
    } else {
      this.setState({
        [name]: value
      });
    }

    if (name == "lastFUPerson") {
      // if(/^0-9a-zA-Z/.test(value)){
      //   this.state.isErrLastFUPerson = true;
      // }else{
      //   this.state.isErrLastFUPerson = false;
      // }
      var isAlphanumeric = require("is-alphanumeric");
      // if(isAlphanumeric(value.blacklist(value,' ')))this.state.isErrLastFUPerson = false;
      // else this.state.isErrLastFUPerson = true;

      if (
        value.split(" ").every(function(value) {
          return isAlphanumeric(value);
        })
      )
        this.state.isErrLastFUPerson = false;
      else this.state.isErrLastFUPerson = true;
    }
  };

  onChangeFunctionEvent = event => {
    let value = event.target.value;
    let name = event.target.name;
    let pattern = event.target.pattern;
    let type = event.target.type;

    let changeState = true;
    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        changeState = true;
      } else {
        changeState = false;
      }
    } else {
      changeState = true;
    }

    if (changeState) {
      if (type == "checkbox") {
        if (this.state[name] == value) {
          value = null;
        }
      }
      this.setState({ [name]: value }, () => {
        if (name == "modalchecklistrelationsip") {
          if (value == 1) {
            this.setState({
              isEnableEditPicNameModal: false,
              phonePicNameModal: this.isCompanyData()
                ? this.state.ProspectCompany.PICName
                : this.state.FollowUp.ProspectName
            });
          } else {
            this.setState({
              isEnableEditPicNameModal: true,
              phonePicNameModal: ""
            });
          }
        }
      });
    }
  };

  onShowAlertModalInfo = MessageAlertCover => {
    this.setState({
      showModalInfo: true,
      MessageAlertCover
    });
  };

  onCloseModalInfo = () => {
    this.setState({ showModalInfo: false }, () => {
      if (!this.state.isErrorSave) {
        this.props.history.push("/tasklist");
        secureStorage.removeItem("SelectedTaskDetail");
      } else {
        this.props.history.push("/tasklist-details");
      }
    });
  };

  onCloseModalAddPhone = () => {
    this.setState({
      showModalAddPhone: false,
      showInputPhoneNo: false,
      issetprimarymodal: false,
      ColumnNameInsertUpdatePhone: "",
      titleInputUpdateTelerenewal: "Add Phone Number",
      phonenumbertype: ""
    });
  };

  onClickSaveRev2 = () => {
    console.log("save");
    const state = this.state;

    this.setState({
      isErrNextFUDateTime: false,
      isErrFollowUpNotes: false,
      isErrFUStatusInfo: false,
      isErrStatusInfoDetail: false,
      isErrLastFUPerson: false,
      isErrRemarks: false
    });

    let errorField = "";

    if (state.FUStatusInfo == "" || state.FUStatusInfo == null) {
      this.setState({ isErrFUStatusInfo: true });
      errorField += "{ Follow Up Result}";
    }

    if (state.statusinfodetail == "" || state.statusinfodetail == null) {
      this.setState({ isErrStatusInfoDetail: true });
      errorField += "{ Follow Up Reason}";
    }

    if (this.state.isMandatory) {
      if (state.FollowUpNotes == "" || state.FollowUpNotes == null) {
        this.setState({ isErrFollowUpNotes: true });
        errorField += "{ Follow Up Notes}";
      }
    }

    if (state.lastFUPerson == "" || state.lastFUPerson == null) {
      this.setState({ isErrLastFUPerson: true });
      errorField += "{ Receiver}";
    }

    if (state.isErrLastFUPerson) {
      this.setState({ isErrLastFUPerson: true });
      errorField += "{Receiver}";
    }

    if (
      (state.statusinfodetail == "61" && state.remarks == "") ||
      state.remarks == null
    ) {
      this.setState({ isErrRemarks: true });
      errorField += "{ Remarks }";
    }

    if (errorField != "") {
      this.setState({
        isErrorSave: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (
      this.state.FUStatusInfo != null ||
      (this.state.FUStatus != "" && this.state.statusinfodetail != null) ||
      (this.state.statusinfodetail != "" && this.state.FollowUpNotes != null) ||
      (this.state.FollowUpNotes != "" && this.state.lastFUPerson != null) ||
      (this.state.lastFUPerson != "" && errorField == "")
    ) {
      if (
        (this.state.FUStatusInfo == "2" &&
          this.state.statusinfodetail == "58") ||
        (this.state.FUStatusInfo == "2" &&
          this.state.statusinfodetail == "61") ||
        (this.state.FUStatusInfo == "2" && this.state.statusinfodetail == "57")
      ) {
        // this.validateTimeSurvey(
        //   () => {
            this.checkVAPayment(
              () => {
                this.processSaveFollowUpStatus();
              },
              () => {
                this.state.isErrorSave = true;
                this.onShowAlertModalInfo("VA Anda sudah expired.");
              }
            );
        //   },
        //   (dataCallback) => {            
        //     let validationTaskDetail = JSON.parse(
        //       secureStorage.getItem("ErrorFlagMandatory")
        //     );
        //     this.state.isErrorSave = true;
        //     this.onShowAlertModalInfo(dataCallback.message);
        //     validationTaskDetail = {
        //       // Make validation on task detail
        //       ...validationTaskDetail,
        //       IsNotErrorsurveytanggal: true,
        //       IsNotErrorsurveywaktu: true
        //     };
        //     secureStorage.setItem(
        //       "ErrorFlagMandatory",
        //       JSON.stringify(validationTaskDetail)
        //     );
        //     secureStorage.setItem("taskdetailshowmandatory", true);
        //   }
        // );
      } else {
        this.setState(
          {
            isErrorSave: false
          },
          () => {
            this.save(false, () => {
              this.onShowAlertModalInfo("Data berhasil disimpan!");
            });
          }
        );
      }
    } else {
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }
  };

  processSaveFollowUpStatus = () => {
    /// 0198-URF-2019 AND 0219-URF-2019
    if (
      !Util.isNullOrEmpty(this.state.account) &&
      !Util.isNullOrEmpty(this.state.FollowUp)
    ) {
      if (this.isCTIComponentRender()) {
        if (!this.onPhoneListHavePrimary()) {
          Util.showToast("Harap memilih nomor telepon primary.", "WARNING");
          return;
        }
      }
    }
    // end 0198-URF-2019 AND 0219-URF-2019

    // if (!JSON.parse(secureStorage.getItem("validationTaskDetail"))) {
    let { res, wording } = this.isValidationData();
    if (!res) {
      this.state.FUStatusInfo = this.state.FollowUpEditStatus;
      this.state.statusinfodetail = this.state.statusInfoCodeBefore;
      this.state.FollowUpNotes = "";
      this.save(false);
      this.state.isErrorSave = true;
      secureStorage.setItem("taskdetailshowmandatory", true); /// flag show mandatory on taskdetail true => show | false => !show
      this.onShowAlertModalInfo(wording);
    } else {
      if (
        this.state.statusinfodetail == "61" ||
        this.state.statusinfodetail == "57" ||
        this.state.statusinfodetail == "58"
      ) {
        //VALIDASI DATA DAN IMAGE MANDATORY NSA APPROVAL
        if (this.handleDocNSA()) {
          this.setState(
            {
              isErrorSave: false
            },
            () => {
              this.save(false, () => {
                this.onShowAlertModalInfo("Data berhasil disimpan!");
              });
            }
          );
        } else {
          this.checkNSAapproval(this.state.OrderNo);
        }
      } else {
        this.setState(
          {
            isErrorSave: false
          },
          () => {
            this.save(false, () => {
              this.onShowAlertModalInfo("Data berhasil disimpan!");
            });
          }
        );
      }
    }
    //VALIDASI DATA SCHEDULE SURVEY, FOTO KTP/NPWP, DATA DAN IMAGE KENDARAAN MANDATORY
  };

  checkVAPayment = (
    callbackNotExpiry = () => {},
    callbackExpiry = () => {}
  ) => {
    this.setState({
      isLoading: true
    });
    if (
      ((this.state.FUStatusInfo == "2" &&
        this.state.statusinfodetail == "58") ||
        (this.state.FUStatusInfo == "2" &&
          this.state.statusinfodetail == "61")) &&
      !(
        this.state.FollowUpEditStatus == 2 &&
        this.state.statusInfoCodeBefore == 60
      )
    ) {
      Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0008URF2020}/DataReact/CheckVAExpiry/`,
        "POST",
        HEADER_API,
        {
          OrderNo: this.state.OrderNo
        }
      )
        .then(res => res.json())
        .then(jsn => {
          this.setState({ isLoading: false });
          if (jsn.status) {
            if (jsn.isVAExpired) {
              if (typeof callbackExpiry === "function") {
                callbackExpiry();
              }
            } else {
              if (typeof callbackNotExpiry === "function") {
                callbackNotExpiry();
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ false - " + jsn.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    } else {
      if (typeof callbackNotExpiry === "function") {
        callbackNotExpiry();
      }
    }
  };

  validateTimeSurvey = (
    callbackNotExpiry = () => {},
    callbackExpiry = (data) => {}
  ) => {
    this.setState({
      isLoading: true
    });
    if (this.state.FUStatusInfo == "2" && this.state.statusinfodetail == "58") {
      Util.fetchAPIAdditional(
        `${API_URL +
          "" +
          API_VERSION_0001URF2020}/DataReact/ValidateTimeSurvey/`,
        "POST",
        HEADER_API,
        {
          CityId: this.state.SurveySchedule.CityCode,
          ScheduleTimeDesc: this.state.SurveySchedule.ScheduleTimeDesc,
          AvailableDate: this.state.SurveySchedule.SurveyDate,
          CustID: this.state.FollowUp.CustID,
          FollowUpNo: this.state.FollowUp.FollowUpNo
        }
      )
        .then(res => res.json())
        .then(jsn => {
          this.setState({ isLoading: false });
          if (jsn.status) {
            if (!jsn.data.isValidTimeSurvey) {
              if (typeof callbackExpiry === "function") {
                callbackExpiry(jsn.data);
              }
            } else {
              if (typeof callbackNotExpiry === "function") {
                callbackNotExpiry();
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ false - " + jsn.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
        })
        .catch(error => {
          Log.error("parsing Failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    } else {
      if (typeof callbackNotExpiry === "function") {
        callbackNotExpiry();
      }
    }
  };

  isValidationData = () => {
    let res = true;
    let wording = "Harap melengkapi data yang wajib diisi!";

    if (
      this.state.statusinfodetail == "61" ||
      this.state.statusinfodetail == "57"
      // || this.state.statusinfodetail == "58"
    ) {
      if (!JSON.parse(secureStorage.getItem("validationTaskDetail"))) {
        res = false;
      }
    }

    //// BERUBAH MANEH !!!!!!!!!!!!!!!!!!!!!!!
    if (this.state.statusinfodetail == 58) {
      let validationSegmentData = JSON.parse(
        secureStorage.getItem("validationSegmentData")
      );
      let validationTaskDetail = JSON.parse(
        secureStorage.getItem("ErrorFlagMandatory")
      );

      // isValidationPersonalData: true,
      // isValidationPersonalDocument: true,
      // isValidationCompanyData: true,
      // isValidationCompanyDocument: true,
      // isValidationVehicleData: true,
      // isValidationPolicyDelivery: true,
      // isValidationSurveySchedule: true,
      // isValidationDocuments: true,
      // isValidationPremiItems: true
      if (!validationSegmentData.isValidationVehicleData) {
        res = false;
      }
      if (!validationSegmentData.isValidationSurveySchedule) {
        res = false;
      }

      if (
        validationTaskDetail.IsNotErrorpersonalname &&
        this.state.ProspectCustomer.isCompany == 0
      ) {
        res = false;
      }
      if (
        validationTaskDetail.IsNotErrorpersonalhp &&
        this.state.ProspectCustomer.isCompany == 0
      ) {
        res = false;
      }
      if (
        validationTaskDetail.IsNotErrorcompanyname &&
        this.state.ProspectCustomer.isCompany == 1
      ) {
        res = false;
      }
      if (
        validationTaskDetail.IsNotErrorcompanypichp &&
        this.state.ProspectCustomer.isCompany == 1
      ) {
        res = false;
      }

      if (!Util.isNullOrEmpty(this.state.SurveySchedule)) {
        /// Penjagaan backdate survey schedule
        let { SurveyDate, ScheduleTimeDesc } = this.state.SurveySchedule;
        if(!Util.isNullOrEmpty(SurveyDate) && !Util.isNullOrEmpty(ScheduleTimeDesc)){
          if (
            Util.convertDate(Util.formatDate(SurveyDate, "dd-mmm-yyyy")) <=
            Util.convertDate()
          ) {
            let datadesc = Util.valueTrim(ScheduleTimeDesc).split("-");
            let data1 = parseInt(datadesc[0].replace(/[^0-9]/g, ""));
            let data2 = parseInt(datadesc[1].replace(/[^0-9]/g, ""));
  
            let datahour = parseInt(
              Util.formatDate(Util.convertDate(), "HH:MM").replace(/[^0-9]/g, "")
            );
  
            if (
              !((data1 <= datahour && data2 >= datahour) || data1 >= datahour)
            ) {
              res = false;
              wording = "Harap mengecek kembali tanggal survey!";
              validationTaskDetail = {
                // Make validation on task detail
                ...validationTaskDetail,
                IsNotErrorsurveytanggal: true,
                IsNotErrorsurveywaktu: true
              };
              secureStorage.setItem(
                "ErrorFlagMandatory",
                JSON.stringify(validationTaskDetail)
              );
            }
  
            if (
              Util.valueTrim(this.state.SurveySchedule.LocationCode) == "Others" && (datahour >= 1400)
            ) {
              res = false;
              wording = "Survey luar on call hari - H tidak dapat dilakukan diatas jam 14.00";
              validationTaskDetail = {
                // Make validation on task detail
                ...validationTaskDetail,
                IsNotErrorsurveytanggal: true,
                IsNotErrorsurveywaktu: true
              };
              secureStorage.setItem(
                "ErrorFlagMandatory",
                JSON.stringify(validationTaskDetail)
              );
            }
          }
        }else{
          res = false;
        }
      }
    }

    return {
      res,
      wording
    };
  };

  checkNSAapproval = OrderNo => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/CheckNSA/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "OrderNo=" + OrderNo
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        this.setState(
          {
            MessageAlertCover: jsn.data[0].Remarks,
            nsaResult: jsn.data[0].IsNSA
          },
          () => {
            if (jsn.data[0].IsNSA == "True") {
              this.state.isErrorSave = true;
              this.setState(
                {
                  statusinfodetail: "46",
                  FUStatusInfo: "3"
                },
                () => {
                  this.save(false, () => {
                    this.onShowAlertModalInfo(
                      "Order ini terdeteksi memerlukan NSA, harap melakukan approval NSA terlebih dahulu dan menyertakan dokumen pendukung."
                    );
                  });
                  // this.onShowAlertModalInfo(jsn.data[0].Remarks);
                }
              );
            } else if (jsn.data[0].IsNSA == "False") {
              this.state.isErrorSave = false;
              this.save(false, () => {
                this.onShowAlertModalInfo("Data berhasil disimpan!");
              });
            } else {
              this.state.isErrorSave = false;
            }
          }
        );
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  getRelationWithInsured = (callback = () => {}) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetRelationshipWithInsured/`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == false) {
          Util.showToast(
            "Oops cannot update email, something wrong !",
            "WARNING"
          );
        } else {
          if (!Util.isNullOrEmpty(jsn.data)) {
            let datarelationship = jsn.data.map(data => ({
              value: data.RelationshipId,
              label: data.RelationshipDes
            }));
            this.setState({
              datarelationsshipraw: jsn.data,
              datarelationship
            });
          }
        }
        this.setState(
          {
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  getCallRejectReason = (callback = () => {}) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/GetCallRejectReason/`,
      "POST",
      HEADER_API
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == false) {
          Util.showToast("Oops something wrong !", "WARNING");
        } else {
          if (!Util.isNullOrEmpty(jsn.data)) {
            let datacallrejectreason = jsn.data.map(data => ({
              value: data.CallRejectReasonId,
              label: data.CallRejectReasonDes
            }));
            this.setState({
              datacallrejectreasonraw: jsn.data,
              datacallrejectreason
            });
          }
        }
        this.setState(
          {
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  handleDocNSA = () => {
    let tempReturn = false;
    let FollowUp = { ...this.state.FollowUp };
    if (
      !Util.isNullOrEmpty(FollowUp.DocNSA1) ||
      !Util.isNullOrEmpty(FollowUp.DocNSA2) ||
      !Util.isNullOrEmpty(FollowUp.DocNSA3) ||
      !Util.isNullOrEmpty(FollowUp.DocNSA4) ||
      !Util.isNullOrEmpty(FollowUp.DocNSA5)
    ) {
      tempReturn = true;
    }
    return tempReturn;
  };

  searchDataPhoneFromPhonelist = (
    PhoneNumber,
    dataphonelist = this.state.dataGetPhoneList
  ) => {
    let temp = null;

    let DataPhone = [...dataphonelist].filter(
      aDataGetPhoneList => aDataGetPhoneList.NoHp == PhoneNumber
    );

    if (DataPhone.length > 0) {
      temp = DataPhone[0];
    }

    return temp;
  };

  searchDataReasonReject = ReasonId => {
    let temp = null;

    let datas = [...this.state.datacallrejectreasonraw].filter(
      data => data.CallRejectReasonId == ReasonId
    );

    if (datas.length > 0) {
      temp = datas[0];
    }
    return temp;
  };

  onPhoneListHavePrimary = () => {
    let temp = false;

    let phonechangeStatus = Object.keys({ ...this.state }).filter(key =>
      key.includes("statusphone") ? key.replace("statusphone", "") : null
    );

    phonechangeStatus.forEach((data, i) => {
      data = data.replace("statusphone", "");
      [...this.state.dataphonelist].forEach((dataphonelist, j) => {
        if (Util.stringEquals(data, dataphonelist.value)) {
          if (this.state[`statusphone${data}`] == "IsPrimary") {
            temp = true;
          }
        }
      });
    });

    return temp;
  };

  onSavePhoneListFollowUp = (callback = () => {}) => {
    console.log("NTAR TAMBAH");
    // let PhoneNoParam = [
    //   {
    //     PhoneNo: this.state.phonenumbertype,
    //     ColumnName: this.props.ColumnName || "",
    //     Relation: this.props.state.modalchecklistrelationsip || "",
    //     IsPrimary: this.state.issetprimarymodal,
    //     IsWrongNumber: false,
    //     IsLandLine: this.detectLandLinePhone(this.state.phonenumbertype),
    //     isConnected: false,
    //     PICName: this.state.picname
    //   }
    // ];

    // throw new Error("shudifjsdf");

    let PhoneNoParam = [];

    let phonechangeStatus = Object.keys({ ...this.state }).filter(key =>
      key.includes("statusphone") ? key.replace("statusphone", "") : null
    );

    phonechangeStatus.forEach((data, i) => {
      data = data.replace("statusphone", "");
      let dataGetPhoneListtemp = [...this.state.dataGetPhoneList];
      [...this.state.dataphonelist].forEach((dataphonelist, j) => {
        if (Util.stringEquals(data, dataphonelist.value)) {
          // let DataPhone = [...this.state.dataGetPhoneList].filter(aDataGetPhoneList => aDataGetPhoneList.NoHp == data);
          let DataPhone = this.searchDataPhoneFromPhonelist(
            data,
            dataGetPhoneListtemp
          );
          dataGetPhoneListtemp = dataGetPhoneListtemp.filter(
            data => data != DataPhone
          );

          if (DataPhone != null) {
            PhoneNoParam.push({
              PhoneNo: Util.clearPhoneNo(data),
              ColumnName: DataPhone.Column_Name,
              Relation: DataPhone.RelationshipId,
              IsPrimary:
                this.state[`statusphone${data}`] == "IsPrimary" ? true : false,
              IsWrongNumber:
                this.state[`statusphone${data}`] == "IsWrongNumber"
                  ? true
                  : false,
              IsLandLine: this.detectLandLinePhone(Util.clearPhoneNo(data)),
              isConnected: false,
              PICName:
                this.state.ProspectCustomer.isCompany == 1
                  ? this.state[`statusphone${data}`] == "IsPrimary"
                    ? this.state.ProspectCompany.PICName
                    : DataPhone.PICName
                  : DataPhone.PICName,
              CallRejectReasonId: 0
            });
          }
        }
      });
    });

    console.log("console ", phonechangeStatus);

    this.InsertUpdatePhoneNo(
      this.state.account.UserInfo.User.SalesOfficerID,
      this.state.OrderNo,
      PhoneNoParam,
      () => {
        callback();
      },
      false
    );
  };

  save = (isSentToSA, callback = () => {}) => {
    if (
      Util.stringArrayContains(
        this.state.account.UserInfo.User.Role,
        this.state.Parameterized.roleCTI
      ) &&
      this.state.FollowUp.IsRenewal == 1
    ) {
      this.onSavePhoneListFollowUp();
    }

    if (
      this.state.FollowUp.FollowUpStatus == 1 &&
      this.state.FollowUp.FollowUpInfo == 34
    ) {
      //////////////// hit MGOBitFeature STATE == MGOBID
      const dateNextFollowUpSend =
        Util.formatDate(this.state.nextFollowUpDate, "yyyy-mm-dd") +
        "T" +
        Util.formatTimeFromDate(this.state.nextFollowUpTime);

      var FollowUpSend = {
        ...this.state.FollowUp,
        // LastFollowUpDate: Util.convertDate(),
        FollowUpInfo: this.state.statusinfodetail,
        FollowUpName: this.state.lastFUPerson,
        Receiver: this.state.lastFUPerson,
        FollowUpNo: this.state.FollowUpNo,
        FollowUpStatus: this.state.FUStatusInfo,
        FollowUpNotes: this.state.FollowUpNotes,
        PICWA: this.state.picWa,
        LastSeqNo: 0,
        Remark: this.state.remarks,
        NextFollowUpDate: dateNextFollowUpSend,
        docnsa1: this.state.FollowUp.DocNSA1,
        docnsa2: this.state.FollowUp.DocNSA2,
        docnsa3: this.state.FollowUp.DocNSA3,
        docnsa4: this.state.FollowUp.DocNSA4,
        docnsa5: this.state.FollowUp.DocNSA5
      };

      FollowUpSend = JSON.stringify(FollowUpSend);
      this.setState({ isLoading: true });

      fetch(
        `${API_URL +
          "" +
          API_VERSION_0001URF2020}/DataReact/SaveFollowUpStatusDetails/`,
        {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "FollowUpLast=" +
            JSON.stringify(this.state.FollowUp) +
            "&FollowUp=" +
            FollowUpSend +
            "&isSentToSA=" +
            isSentToSA +
            "&STATE=MGOBID" +
            "&PICWA=" +
            this.state.picWa +
            "&editor=" +
            JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
        }
      )
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.status == 1) {
            toast.info("✔️ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.warning("❗ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          this.loadFUHistory(this.state.FollowUpNo);
          callback();
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.error("❗ Something error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    } else {
      const dateNextFollowUpSend =
        Util.formatDate(this.state.nextFollowUpDate, "yyyy-mm-dd") +
        "T" +
        Util.formatTimeFromDate(this.state.nextFollowUpTime);

      var FollowUpSend = {
        ...this.state.FollowUp,
        FollowUpInfo: this.state.statusinfodetail,
        FollowUpName: this.state.lastFUPerson,
        Receiver: this.state.lastFUPerson,
        FollowUpNo: this.state.FollowUpNo,
        FollowUpStatus: this.state.FUStatusInfo,
        FollowUpNotes: this.state.FollowUpNotes,
        PICWA: this.state.picWa,
        LastSeqNo: 0,
        Remark: this.state.remarks,
        NextFollowUpDate: dateNextFollowUpSend,
        docnsa1: this.state.FollowUp.DocNSA1,
        docnsa2: this.state.FollowUp.DocNSA2,
        docnsa3: this.state.FollowUp.DocNSA3,
        docnsa4: this.state.FollowUp.DocNSA4,
        docnsa5: this.state.FollowUp.DocNSA5
      };

      FollowUpSend = JSON.stringify(FollowUpSend);
      this.setState({ isLoading: true });

      fetch(
        `${API_URL +
          "" +
          API_VERSION_0001URF2020}/DataReact/SaveFollowUpStatusDetails/`,
        {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "FollowUpLast=" +
            JSON.stringify(this.state.FollowUp) +
            "&FollowUp=" +
            FollowUpSend +
            "&isSentToSA=" +
            isSentToSA +
            "&STATE=" +
            "&editor=" +
            JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
        }
      )
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.status == 1) {
            toast.info("✔️ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          } else {
            toast.warning("❗ " + datamapping.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }

          if (!Util.isNullOrEmpty(datamapping.data)) {
            this.setState({
              FollowUp: datamapping.data,
              lastFollowUpDate: Util.convertDate(
                datamapping.data.LastFollowUpDate + ""
              ),
              lastFollowUpTime: Util.convertDate(
                datamapping.data.LastFollowUpDate + ""
              ),
              nextFollowUpDate: Util.convertDate(
                datamapping.data.NextFollowUpDate + ""
              ),
              nextFollowUpTime: Util.convertDate(
                datamapping.data.NextFollowUpDate + ""
              ),
              FUStatus: datamapping.data.FollowUpStatus,
              oldFUStatusInfo: datamapping.data.FollowUpInfo,
              FUStatusInfo: datamapping.data.FollowUpStatus,
              lastFUPerson: datamapping.data.Receiver,
              remarks: datamapping.data.Remark,
              isLoading: false
            });
          }
          this.loadFUHistory(this.state.FollowUpNo);
          callback();
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.error("❗ Something error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        });
    }
  };

  updateEmailPropspect = (
    Email = this.state.emailProspect,
    OrderNo = this.state.OrderNo,
    callback = () => {}
  ) => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_2}/DataReact/UpdateEmail/`,
      "POST",
      HEADER_API,
      {
        Email,
        OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == false) {
          Util.showToast(
            "Oops cannot update email, something wrong !",
            "WARNING"
          );
        }
        this.setState(
          {
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  addNewPhoneInsertUpdatePhoneNo = (
    SalesOfficerID = JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
    OrderNo = this.state.OrderNo,
    PhoneNoParam = [],
    callback = () => {}
  ) => {
    this.InsertUpdatePhoneNo(SalesOfficerID, OrderNo, PhoneNoParam, () => {
      callback();
      this.setState({
        modalchecklistrejectreason: "",
        modalchecklistrejectreasonInputNo: "",
        connectedModalInputPhoneNo: false
      });
    });
  };

  InsertUpdatePhoneNo = (
    SalesOfficerID = JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
    OrderNo = this.state.OrderNo,
    PhoneNoParam = [],
    callback = () => {},
    controlLoading = true
  ) => {
    if (controlLoading) {
      this.setState({ isLoading: true });
    }
    Util.fetchAPIAdditional(
      `${API_URL +
        "" +
        API_VERSION_0219URF2019}/DataReact/InsertUpdatePhoneNo/`,
      "POST",
      { ...HEADER_API, "Content-Type": "application/json" },
      {
        SalesOfficerID,
        OrderNo,
        PhoneNoParam
      }
    )
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status == false) {
          Util.showToast("Oops something wrong !", "WARNING");
        } else {
          if (!Array.isArray(jsn.data)) {
            Util.showToast("Data ini sudah terdaftar di database.", "WARNING");
          } else {
            let dataphonelist = jsn.data.map(data => ({
              value: data.NoHp,
              label: Util.maskingPhoneNumberv2(Util.clearPhoneNo(data.NoHp))
            }));
            // if (jsn.data.length > 0) {

            if (this.isCompanyData()) {
              if (this.state.showInputPhoneNo) {
                this.setState({
                  ProspectCompany: {
                    ...this.state.ProspectCompany,
                    PICName: PhoneNoParam[0].PICName
                  }
                });
              }
            }

            this.setState(
              {
                dataphonelist,
                dataGetPhoneList: jsn.data
                // modalchecklistrelationsip: null // set null modalchecklistrelationsip
              },
              () => {
                Util.showToast("Data berhasil disimpan.");
                callback();
              }
            );
            // }
          }
        }
        if (controlLoading) {
          this.setState({
            isLoading: false
          });
        }
      })
      .catch(error => {
        Log.error("parsing Failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  addNewPhoneNumber = () => {
    this.getRelationWithInsured(() => {
      this.setState(
        {
          showModalAddPhone: true,
          showInputPhoneNo: true,
          issetprimarymodal: false,
          phonePicNameModal: this.isCompanyData()
            ? this.state.ProspectCompany.PICName
            : "",
          isEnableEditPicNameModal: true,
          ColumnNameInsertUpdatePhone: "",
          titleInputUpdateTelerenewal: "Add Phone Number",
          phonenumbertype: "",

          modalchecklistrelationsip: "",
          modalchecklistrejectreason: ""
        },
        () => {
          if (this.state.modalchecklistrelationsip == 1) {
            this.setState({
              isEnableEditPicNameModal: false,
              phonePicNameModal: this.isCompanyData()
                ? this.state.ProspectCompany.PICName
                : this.state.FollowUp.ProspectName
            });
          }
        }
      );
    });
  };

  onSetPrimaryPhone = PhoneNumber => {
    this.getRelationWithInsured(() => {
      let Data = [...this.state.dataGetPhoneList].filter(
        data => data.NoHp == PhoneNumber
      );

      this.setState(
        {
          modalchecklistrelationsip: Data[0].RelationshipId,
          showModalAddPhone: true,
          showInputPhoneNo: false,
          issetprimarymodal: true,
          phonePicNameModal: this.isCompanyData()
            ? this.state.ProspectCompany.PICName
            : Data[0].PICName,
          isEnableEditPicNameModal: true,
          ColumnNameInsertUpdatePhone: Data[0].Column_Name,
          titleInputUpdateTelerenewal: "Update Set Primary Number",
          phonenumbertype: PhoneNumber
        },
        () => {
          if (this.state.modalchecklistrelationsip == 1) {
            this.setState({
              isEnableEditPicNameModal: false,
              phonePicNameModal: this.isCompanyData()
                ? this.state.ProspectCompany.PICName
                : this.state.FollowUp.ProspectName
            });
          }
        }
      );
    });
  };

  onUpdateSuccessPrimaryPhoneFunction = phoneno => {
    this.setState({
      [`statusphone${phoneno}`]: "IsPrimary"
    });
  };

  onClickCallButtonInputPhone = callCTIPhoneInputPhone => {
    if (!this.state.flagCanCallNext) {
      Util.showToast("wait a minutes for next call session", "WARNING");
      return;
    }
    this.getCallRejectReason(() => {
      this.setState(
        {
          showModalCTICallInput: true,
          callCTIPhoneInputPhone,
          connectedModalInputPhoneNo: false,
          modalchecklistrejectreason: "",
          modalchecklistrejectreasonInputNo: ""
        },
        () => {
          this.callTrayAPIOnDial(callCTIPhoneInputPhone);
          this.startIntervalGetStatus();
        }
      );
    });
  };

  isCompanyData = () => {
    let temp = false;
    if (!Util.isNullOrEmpty(this.state.ProspectCustomer)) {
      if (this.state.ProspectCustomer.isCompany == 1) {
        temp = true;
      }
    }

    return temp;
  };

  detectLandLinePhone = PhoneNumber => {
    let temp = false;
    PhoneNumber += "";
    if (
      Util.detectPrefix(
        Util.clearPhoneNo(PhoneNumber),
        this.state.Parameterized.landLinePrefix
      )
    ) {
      temp = true;
    }

    return temp;
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  PhoneList = () => {
    let phonelist = [...this.state.dataphonelistold].map((data, index) => {
      return (
        <div className="row" key={"phonelistfollowup" + index}>
          <div
            className="col-xs-9 col-lg-10"
            style={{ display: "inline-flex", fontSize: "larger" }}
          >
            <img
              className="img img-responsive"
              src={Call}
              style={{
                width: "100%",
                maxWidth: "40px",
                height: "100%",
                maxHeight: "40px"
              }}
            />
            <span style={{ paddingLeft: "10px", paddingTop: "7px" }}>
              {/* {Util.stringEquals(JSON.parse(ACCOUNTDATA).UserInfo.User.Role, "TELERENEWAL") ? "xxxxxxxxxxxx" : data} */}
              {data}
            </span>
          </div>
          <div className="col-xs-3 col-lg-2 text-right">
            <a
              href={"tel:+" + data}
              className="fa fa-phone fa-2x"
              style={{ height: "50px", weight: "50px", paddingTop: "2px" }}
            >
              {" "}
            </a>
          </div>
        </div>
      );
    });

    return phonelist;
  };

  ModalSendEmailWA = () => {
    return (
      <OtosalesModalSendEmailCkEditor
        title="Send Via WA"
        state={this.state}
        isHideSubject={true}
        showModalSendEmail={this.state.showModalSendEmailWA}
        subject={this.state.SubjectTemplateEmailWA}
        bodyEmail={this.state.BodyTemplateSendEmailWA}
        OrderNo={this.state.OrderNo}
        maxLengthSubject={100}
        maxLength={3000}
        sendQuotationEmail={this.sendQuotationEmailWACTI}
        onCloseModalSendEmail={this.onCloseModalSendEmailWA}
        handleDeleteImage={this.handleDeleteFile}
        handleUploadImage={this.handleUploadFile}
        state={this.state}
        email={
          // this.state.Parameterized.emailAdminWA.join(";") ||
          this.state.EmailToTemplateWA
        }
      />
    );
  };

  ModalCTICallPhone = () => {
    return (
      <Modal
        styles={{
          modal: {
            width: "95%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModalCTICall}
        // onClose={() => this.onCloseModalCTICall()}
        showCloseIcon={false}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px", fontSize: "initial" }}
        >
          <OtosalesLoading isLoading={this.state.isLoading}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title text-center" id="exampleModalLabel">
                <b>
                  Calling {this.loadingDot()}{" "}
                  {Util.maskingPhoneNumberv2(
                    Util.clearPhoneNo(this.state.callCTIPhone)
                  )}
                </b>
              </h4>
            </div>
            <div
              className="modal-body"
              style={{
                fontSize: this.state.width > this.state.height ? "11pt" : "8pt"
              }}
            >
              <this.CallRejectReasonComponent />
            </div>
            <div className="modal-footer">
              <div className="pull-right">
                {
                  // Util.stringEquals(this.state.CTIStatus, "On Call") &&
                  <a
                    className="btn btn-success"
                    onClick={() => {
                      this.callTrayConnected();
                    }}
                  >
                    Connected
                  </a>
                }
                <a
                  className="btn btn-success"
                  onClick={() => {
                    this.callTrayNext();
                  }}
                >
                  Next
                </a>
              </div>
            </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  };

  ModalCTICallPhoneInputPhone = () => {
    return (
      <Modal
        styles={{
          modal: {
            width: "95%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModalCTICallInput}
        // onClose={() => this.onCloseModalCTICall()}
        onClose={() => {
          setTimeout(() => {
            clearInterval(this.intervalGetStatusCTI);
          }, 5000);
          this.endCallModalInputPhoneNo(this.state.callCTIPhoneInputPhone);
        }}
        showCloseIcon={true}
        closeOnOverlayClick={false}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px", fontSize: "initial" }}
        >
          <OtosalesLoading isLoading={this.state.isLoading}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title text-center" id="exampleModalLabel">
                <b>
                  Calling {this.loadingDot()}
                  {Util.maskingPhoneNumberv2(
                    Util.clearPhoneNo(this.state.callCTIPhoneInputPhone)
                  )}
                </b>
              </h4>
            </div>
            <div
              className="modal-body"
              style={{
                fontSize: this.state.width > this.state.height ? "11pt" : "8pt"
              }}
            >
              <this.CallRejectReasonComponent />
            </div>
            <div className="modal-footer">
              <div className="pull-right">
                {
                  // Util.stringEquals(this.state.CTIStatus, "On Call") &&
                  <a
                    className="btn btn-success"
                    onClick={() => {
                      // this.callTrayConnected();
                      setTimeout(() => {
                        clearInterval(this.intervalGetStatusCTI);
                      }, 5000);
                      this.connectedModalInputPhoneNo(
                        this.state.callCTIPhoneInputPhone
                      );
                    }}
                  >
                    Connected
                  </a>
                }
                {/* <a
                  className="btn btn-danger"
                  onClick={() => {
                    this.endCallModalInputPhoneNo(
                      this.state.callCTIPhoneInputPhone
                    );
                  }}
                >
                  End Call
                </a> */}
              </div>
            </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  };

  emailToModalSendEmail = () => {
    let res = this.state.emailProspect;

    if (Util.isNullOrEmpty(res)) {
      res = this.state.EmailToTemplateSendEmail;
    }

    return res;
  };

  CTIComponent = () => {
    return (
      <React.Fragment>
        <OtosalesModalSendEmailCkEditor
          state={this.state}
          showModalSendEmail={this.state.showModalSendEmail}
          subject={this.state.SubjectTemplateEmail}
          bodyEmail={this.state.BodyTemplateSendEmail}
          OrderNo={this.state.OrderNo}
          maxLengthSubject={100}
          maxLength={3000}
          sendQuotationEmail={this.sendQuotationEmailTelerenewal}
          onCloseModalSendEmail={this.onCloseModalSendEmail}
          handleDeleteImage={this.handleDeleteFile}
          handleUploadImage={this.handleUploadFile}
          state={this.state}
          email={this.emailToModalSendEmail()}
        />
        <this.ModalSendEmailWA />
        <OtosalesModalSendWA
          dataphonelist={this.state.dataphonelist.filter(data =>
            Util.detectPrefix(
              Util.clearPhoneNo(data.value),
              this.state.Parameterized.mobilePrefix
            )
          )}
          phonelistselect={this.state.phonelistselect}
          onOptionSelect2Change={this.onOptionSelect2Change}
          sendQuotationWA={() => {
            this.sendQuotationWA();
          }}
          onCloseModalSendWA={this.onCloseModalSendWA}
          state={this.state}
          phonenumber={this.state.phonelistselect}
        />

        <OtosalesModalSendSMSCkEditor
          isLoading={this.state.isLoading}
          dataphonelist={this.state.dataphonelist.filter(data =>
            Util.detectPrefix(
              Util.clearPhoneNo(data.value),
              this.state.Parameterized.mobilePrefix
            )
          )}
          maxLength={500}
          bodySMS={this.state.BodyTemplateSendEmailSMSTelerenewal}
          phonelistselect={this.state.phonelistselect}
          onOptionSelect2Change={this.onOptionSelect2Change}
          sendQuotationSMS={this.sendQuotationSMSTelerenewal}
          onCloseModalSendSMS={this.onCloseModalSendSMS}
          state={this.state}
          phonenumber={this.state.phonelistselect}
        />
        <this.ModalCTICallPhone />
        <this.ModalCTICallPhoneInputPhone />
        <OtosalesModalInputNumberTelerenewal
          title={this.state.titleInputUpdateTelerenewal}
          ColumnName={this.state.ColumnNameInsertUpdatePhone}
          phonenumbertype={this.state.phonenumbertype}
          InsertUpdatePhoneNo={this.addNewPhoneInsertUpdatePhoneNo}
          showInputPhoneNo={this.state.showInputPhoneNo}
          issetprimarymodal={this.state.issetprimarymodal}
          onUpdatePhoneFunction={this.onUpdateSuccessPrimaryPhoneFunction}
          onClickCallButtonInputPhone={this.onClickCallButtonInputPhone}
          searchDataReasonReject={this.searchDataReasonReject}
          showModal={this.state.showModalAddPhone}
          onCloseMakeSure={this.onCloseModalAddPhone}
          onChangeFunction={this.onChangeFunctionEvent}
          phonePicName={this.state.phonePicNameModal}
          isEnableEditPicName={this.state.isEnableEditPicNameModal}
          state={this.state}
          relationComponent={<this.RelationShipWithInsuredComponent />}
        />
        <div className="row">
          <div className="col-xs-12">
            <label>
              Customer Name :{" "}
              {this.isCompanyData()
                ? this.state.ProspectCompany.CompanyName
                : this.state.FollowUp.ProspectName}
            </label>
          </div>
        </div>
        <div className="row" style={{ marginBottom: 10 }}>
          <div
            className="col-md-1 col-xs-3"
            onClick={() => {
              this.getPhoneList(this.state.OrderNo, true, () => {
                if (this.state.dataGetPhoneList.length == 0) {
                  return;
                }
                this.callTrayAPIOnDrop(this.state.callCTIPhone, () => {
                  this.callTelerenewal();
                });
              });
            }}
          >
            <img
              src={Call}
              className="img img-responsive"
              style={{ maxWidth: 60 }}
            />
          </div>
          <div
            className="col-md-1 col-xs-3"
            onClick={() => {
              this.getPhoneList(this.state.OrderNo, true, () => {
                this.setState({
                  showModalSendWA: true,
                  phonelistselect: this.selectedPhoneNumberCTI()
                });
              });
            }}
          >
            <img
              src={sendWA}
              className="img img-responsive"
              style={{ maxWidth: 60 }}
            />
          </div>
          <div
            className="col-md-1 col-xs-3"
            onClick={() => {
              this.getPhoneList(this.state.OrderNo, true, () => {
                this.getSendEmailTemplateSMSTelerenewal(this.state.OrderNo);
                this.setState({
                  phonelistselect: this.selectedPhoneNumberCTI()
                });
              });
            }}
          >
            <img
              src={sendSMS}
              className="img img-responsive"
              style={{ maxWidth: 60 }}
            />
          </div>
          <div
            className="col-md-1 col-xs-3"
            onClick={() => {
              this.getSendEmailTemplate(this.state.OrderNo);
            }}
          >
            <img
              src={sendEmailv2}
              className="img img-responsive"
              style={{ maxWidth: 60 }}
            />
          </div>
        </div>
        <div className="row">
          <this.CTIPhoneList />
          <div className="row">
            <div className="col-xs-12">
              <div
                className="col-xs-12 checkbox"
                onClick={() => {
                  this.addNewPhoneNumber();
                }}
              >
                <i className="fa fa-plus" style={{ marginRight: 2 }} />
                <span>add new phone number</span>
              </div>
              <div className="col-xs-12 col-md-4">
                <label style={{ width: "20%" }}>Email :</label>
                {this.state.isChangeEmail ? (
                  <input
                    type="email"
                    maxLength="50"
                    name="emailProspect"
                    value={this.state.emailProspect}
                    onChange={this.onChangeFunctionEvent}
                    style={{
                      width: "80%"
                    }}
                  />
                ) : (
                  <a
                    style={{
                      width: "80%"
                    }}
                  >
                    {this.state.emailProspect}
                  </a>
                )}
              </div>
              <div className="col-xs-12 col-md-3">
                {this.state.isChangeEmail ? (
                  <a
                    onClick={() => {
                      this.updateEmailPropspect(
                        this.state.emailProspect,
                        this.state.OrderNo,
                        () => {
                          this.setState({
                            isChangeEmail: false
                          });
                        }
                      );
                    }}
                  >
                    Save
                  </a>
                ) : (
                  <a
                    onClick={() => {
                      this.setState({
                        isChangeEmail: true
                      });
                    }}
                  >
                    Edit Data?
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  CTIPhoneList = () => {
    let phonelist = [...this.state.dataphonelist].map((data, index) => {
      return (
        <div
          className="row"
          key={"CTIPhone" + data.value + "|" + index}
          style={{
            fontSize: this.state.width < this.state.height ? "8pt" : "11pt",
            marginBottom: "15px"
          }}
        >
          <div className="col-xs-12">
            <div
              className="col-xs-3 col-md-2"
              style={{
                wordBreak: "break-word",
                paddingRight: "0px"
              }}
            >
              {Util.maskingPhoneNumberv2(Util.clearPhoneNo(data.value))}
            </div>
            <div className="col-xs-6 col-md-4 panel-body-list">
              <div className="col-md-6 col-xs-12">
                <div class="checkbox" style={{ marginTop: 0 }}>
                  <label>
                    <input
                      type="checkbox"
                      name={`statusphone${data.value}`}
                      checked={
                        this.state[`statusphone${data.value}`] ==
                        "IsWrongNumber"
                      }
                      value={"IsWrongNumber"}
                      onChange={event => this.onChangeFunctionEvent(event)}
                    />
                    Salah Sambung
                  </label>
                </div>
              </div>
              {Util.detectPrefix(
                Util.clearPhoneNo(data.value),
                this.state.Parameterized.mobilePrefix
              ) && (
                <div className="col-md-6 col-xs-12">
                  <div
                    class="checkbox"
                    style={{ marginTop: 0, marginBottom: 0 }}
                  >
                    <label>
                      <input
                        type="checkbox"
                        name={`statusphone${data.value}`}
                        disabled={
                          this.state[`statusphone${data.value}`] ==
                          "IsWrongNumber"
                        }
                        checked={
                          this.state[`statusphone${data.value}`] == "IsPrimary"
                        }
                        value={"IsPrimary"}
                        // onChange={event => this.onChangeFunctionEvent(event)}
                        onClick={() => {
                          if (
                            Util.detectPrefix(
                              Util.clearPhoneNo(data.value),
                              this.state.Parameterized.mobilePrefix
                            )
                          ) {
                            this.onSetPrimaryPhone(data.value);
                          } else {
                            Util.showToast(
                              "Nomor ini tidak dapat menjadi primary number",
                              "WARNING"
                            );
                          }
                        }}
                      />
                      Set Primary
                    </label>
                  </div>
                </div>
              )}
            </div>
            {data.value == this.state.callCTIPhone &&
              !Util.stringEquals(this.state.CTIStatus, "Ready") && (
                <div
                  className="col-xs-3 col-md-6 panel-body-list"
                  style={{
                    fontSize:
                      this.state.width < this.state.height ? "8pt" : "11pt"
                  }}
                >
                  <div
                    className="col-xs-12 col-md-4 panel-body-list"
                    style={{ display: "flex" }}
                  >
                    <img
                      className="img img-responsive"
                      src={checklist}
                      style={{
                        maxWidth: "20px",
                        maxHeight: "20px",
                        marginRight: 2
                      }}
                    />
                    <span>Now Connecting</span>
                  </div>
                  {/* <div
                  className="col-xs-12 col-md-4 panel-body-list"
                  style={{ display: "flex" }}
                  onClick={() => {
                    this.endCallTelerenewal(this.state.callCTIPhone);
                  }}
                >
                  <img
                    className="img img-responsive"
                    src={hangupphone}
                    style={{
                      maxWidth: "20px",
                      maxHeight: "20px",
                      marginRight: 2
                    }}
                  />
                </div> */}
                </div>
              )}
          </div>
        </div>
      );
    });

    return phonelist;
  };

  RelationShipWithInsuredComponent = () => {
    let datas = this.state.datarelationship;
    let temp =
      datas.length > 0
        ? [...datas].map((data, i) => (
            <div className="col-xs-6" key={"indexRL" + i}>
              <div
                className="checkbox"
                style={{ marginTop: 0, marginBottom: 0 }}
              >
                <label>
                  <input
                    type="checkbox"
                    name="modalchecklistrelationsip"
                    value={data.value}
                    checked={this.state.modalchecklistrelationsip == data.value}
                    onChange={this.onChangeFunctionEvent}
                  />
                  {data.label}
                </label>
              </div>
            </div>
          ))
        : null;

    return <div className="col-xs-12">{temp}</div>;
  };

  CallRejectReasonComponent = () => {
    let datas = this.state.datacallrejectreason;
    let temp =
      datas.length > 0
        ? [...datas].map((data, i) => (
            <div className="col-xs-12" key={"indexCRR" + i}>
              <div
                className="checkbox"
                style={{ marginTop: 0, marginBottom: 0 }}
              >
                <label>
                  <input
                    type="checkbox"
                    name="modalchecklistrejectreason"
                    value={data.value}
                    checked={
                      this.state.modalchecklistrejectreason == data.value
                    }
                    onChange={this.onChangeFunctionEvent}
                  />
                  {data.label}
                </label>
              </div>
            </div>
          ))
        : null;

    return <div className="col-xs-12">{temp}</div>;
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
    // clearInterval(this.intervalGetStatusCTI);
    // clearInterval(this.intervaNextCall);
    // console.log("statusphone :",this.setModelPhoneStatus());
    secureStorage.setItem(
      "tasklist-followupdetailstatusphone",
      JSON.stringify(this.setModelPhoneStatus())
    );
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  };

  setModelPhoneStatus = () => {
    let objectTemp = this.modelPhoneStatusChanged();

    return {
      OrderNo: this.state.OrderNo,
      objectTemp
    };
  };

  modelPhoneStatusChanged = () => {
    let objectTemp = [];

    objectTemp = Object.keys({ ...this.state })
      .filter(key => key.includes("statusphone"))
      .reduce((obj, key) => {
        obj[key] = this.state[key];
        return obj;
      }, {});

    return objectTemp;
  };

  // Handle CTI Component render
  isCTIComponentRender = () => {
    let res = false;

    if (
      Util.stringArrayContains(
        this.state.account.UserInfo.User.Role,
        this.state.Parameterized.roleCTI
      )
      // &&
      // this.state.FollowUp.IsRenewal == 1
    ) {
      res = true;
    }

    return res;
  };

  // handle disable follow up result
  isDisabledFollowUpResult = () => {
    let res = false;

    if (
      !Util.isNullOrEmpty(
        secureStorage.getItem("tasklist-followupdetail-FUStatus")
      )
    ) {
      res = true;
    }

    return res;
  };

  PageRender = () => (
    <div className="panel-body panel-body-list" id="Datalist">
      <div className="form-group">
        {!Util.isNullOrEmpty(this.state.account) &&
          !Util.isNullOrEmpty(this.state.FollowUp) &&
          (this.isCTIComponentRender() ? (
            <this.CTIComponent /> // CTI Component
          ) : (
            <this.PhoneList /> // Phone number
          ))}
        {/* {(this.state.userRole.toLowerCase() == "telesales" ||
          this.state.userRole.toLowerCase() == "telerenewal") &&
          !Util.isNullOrEmpty(this.state.FollowUp) &&
          this.state.FollowUp.IsRenewal != 1 && (
            <div
              className={
                this.state.isErrpicWa ? "form-group has-error" : "form-group"
              }
            >
              <label>SEND TO PIC WA</label>
              <div className="input-group">
                <div>
                  <NumberFormat
                    className="form-control"
                    onValueChange={value => {
                      this.setState({ picWa: value.value });
                    }}
                    value={this.state.picWa}
                    format="#### #### #### ####"
                    prefix={""}
                  />
                </div>
              </div>
            </div>
          )} */}
        <div
          className={
            this.state.isErrFUStatusInfo ? "form-group has-error" : "form-group"
          }
        >
          <label>FOLLOW UP RESULT </label>
          <div className="input-group">
            <OtosalesSelectFullview
              placeholder="Choose follow up result"
              error={this.state.isErrFUStatusInfo}
              value={this.state.FUStatusInfo}
              selectOption={this.state.datasFUResult}
              onOptionsChange={this.onOptionFUStatusInfoChange}
              options={this.state.datafustatus}
              isDisabled={this.isDisabledFollowUpResult()}
            />
          </div>
        </div>
        <div
          className={
            this.state.isErrStatusInfoDetail
              ? "form-group has-error"
              : "form-group"
          }
        >
          {/* <label>REASON / EXPLANATION * </label> */}
          <label>FOLLOW UP REASON </label>
          <div className="input-group">
            <OtosalesSelectFullview
              placeholder="Choose follow up reason"
              error={this.state.isErrStatusInfoDetail}
              value={this.state.statusinfodetail}
              selectOption={this.state.statusinfodetail}
              onOptionsChange={this.onOptionFUStatusInfoDetailChange}
              options={this.state.datafustatusinfo}
            />
          </div>
        </div>
        <div
          className={
            this.state.isErrFollowUpNotes
              ? "form-group has-error"
              : "form-group"
          }
        >
          <label>FOLLOW UP NOTES </label>
          <div className="input-group">
            <OtosalesSelectFullview
              placeholder="Choose follow up notes"
              error={this.state.isErrFollowUpNotes}
              value={this.state.FollowUpNotes}
              selectOption={this.state.FollowUpNotes}
              onOptionsChange={this.onOptionFollowUpNotesChange}
              options={this.state.datafollowupnotes}
            />
          </div>
        </div>
        <div
          className={
            this.state.isErrLastFUPerson ? "form-group has-error" : "form-group"
          }
        >
          <label>RECEIVER </label>
          <div className="input-group">
            <div>
              <input
                type="text"
                name="lastFUPerson"
                value={this.state.lastFUPerson}
                onChange={event =>
                  this.onChangeFunction(
                    event.target.value,
                    event.target.name,
                    "^[a-zA-Z0-9 ,-./]+$"
                  )
                }
                className="form-control"
                maxLength="50"
              />
            </div>
          </div>
        </div>
        {this.state.FUStatusInfo != "5" && (
          <div className="form-group">
            <label>PREFERRED CALL </label>
            <div className="row">
              <div className="col-xs-6">
                <div
                  className={
                    this.state.isErrNextFUDateTime
                      ? "form-group has-error"
                      : "form-group"
                  }
                >
                  <div className="input-group">
                    <OtosalesDatePicker
                      minDate={Util.convertDate()}
                      className={
                        this.state.isErrNextFUDateTime
                          ? "form-control has-error"
                          : "form-control"
                      }
                      selected={this.state.nextFollowUpDate}
                      dateFormat="dd-mm-yyyy"
                      onChange={this.onChangeFunction}
                      name="nextFollowUpDate"
                    />
                  </div>
                </div>
              </div>
              <div className="col-xs-6">
                <div className="form-group">
                  <div className="input-group">
                    <OtosalesTimePicker
                      // minTime={Util.calculateMinTime(Util.convertDate())}
                      // maxTime={moment()
                      //   .endOf("day")
                      //   .toDate()} // set to 23:59 pm today
                      selected={this.state.nextFollowUpTime}
                      className={
                        this.state.isErrNextFUDateTime
                          ? "form-control has-error"
                          : "form-control"
                      }
                      onChange={this.onChangeFunction}
                      name="nextFollowUpTime"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        <div
          className={
            this.state.isErrRemarks ? "form-group has-error" : "form-group"
          }
        >
          <div className="form-group">
            <label>REMARKS </label>
            <div className="input-group">
              <textarea
                className="form-control"
                rows="5"
                name="remarks"
                value={this.state.remarks}
                onChange={event =>
                  this.onChangeFunction(
                    event.target.value,
                    event.target.name,
                    "^[a-zA-Z0-9 ,-./':;?]+$"
                  )
                }
                maxLength="100"
                style={{ resize: "none" }}
              />
            </div>
          </div>
        </div>
        <div className="form-group">
          {/* <legend>FOLLOW UP HISTORY</legend> */}
          <legend style={{ color: "grey" }}>FOLLOW UP RESULT</legend>
          <div className="row">
            <OtosalesListFollowUpHistory
              name="sampleList"
              rowscount={this.state.datas.length}
              dataitems={this.state.datas}
              datastatusinfoall={this.state.datastatusinfoall}
            />
          </div>
        </div>
      </div>
    </div>
  );

  UtilComponents = () => {
    return (
      <React.Fragment>
        <ToastContainer />
        <OtosalesModalSenttoSA
          onclickSendToSAValid={this.onclickSendToSAValid}
          showModal={this.state.showModal}
          closeModal={this.closeModal}
          FollowUpNo={this.state.FollowUpNo}
          showModal={this.state.showModal}
          dataquotationhistory={this.state.dataquotationhistory}
          selectedQuotation={this.state.selectedQuotation}
        />
        <OtosalesModalInfo
          message={this.state.MessageAlertCover}
          onClose={this.onCloseModalInfo}
          showModalInfo={this.state.showModalInfo}
        />
      </React.Fragment>
    );
  };

  render() {
    return (
      <div>
        <Header titles="Follow Up Data">
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-save fa-lg"
                onClick={() => this.onClickSaveRev2()}
              />
            </li>
          </ul>
        </Header>
        <this.UtilComponents />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <OtosalesLoading
            className="col-xs-12 loadingfixed"
            isLoading={this.state.isLoading}
          >
            <this.PageRender />
          </OtosalesLoading>
        </div>
      </div>
    );
  }
}

export default withRouter(PageFollowUpDetails);
