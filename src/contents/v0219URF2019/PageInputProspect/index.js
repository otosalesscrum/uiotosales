import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import {
  API_URL,
  API_VERSION,
  API_VERSION_2,
  ACCOUNTDATA,
  HEADER_API,
  API_VERSION_0219URF2019
} from "../../../config";
import {
  Tabs,
  OtosalesSelect2,
  OtosalesDropzonev2,
  OtosalesQuotationHistory,
  OtosalesFloatingButton,
  OtosalesModalSendEmail,
  OtosalesModalSendSMS,
  OtosalesModalAccessories,
  OtosalesModalInfo,
  OtosalesSelectFullview,
  OtosalesModalQuotationHistory,
  OtosalesSelectFullviewv2,
  // OtosalesDatePicker,
  OtosalesModalSetPeriodPremi2,
  OtosalesLoading,
  Util,
  Log,
  OtosalesSelectFullviewrev
} from "../../../otosalescomponents";
import OtosalesDatePicker from "../../../otosalescomponents/otosalesdatepicker";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { ToastContainer, toast } from "react-toastify";
import {
  Call,
  NeedFu,
  HexCarInfo,
  HexDealerInfo,
  HexPolicyDetails,
  HexPolicyInfo,
  HexProspectInfo
} from "../../../assets";
import Modal from "react-responsive-modal";
import imageCompression from "browser-image-compression";
import { DeviceUUID } from "device-uuid";
import Header from "../../../components/Header";
import NumberFormat from "react-number-format";
import { secureStorage } from "../../../otosalescomponents/helpers/SecureWebStorage";
// import { isThisSecond } from "date-fns";
const tabstitle = ["Prospect Info", "Vehicle Info", "Cover", "Summary"];
class PageInputProspect extends Component {
  constructor(props) {
    super(props);

    let statedefault = {
      fromProspecttoVehicleCheckPhoneExist: true,
      isOpen: false,
      OrderNoQuotationEdit: "",
      FollowUpNoQuotationEdit: "",
      selectedQuotation: null,
      dataquotationhistory: [
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" },
        { name: "asdad", hasil: "shdbfsdf" }
      ],
      showModalInfo: false,
      showModal: false,
      showdialogperiod: false,
      showModalAddCopy: false,
      showModalMakeSure: false,
      showModalSendEmail: false,
      showModalPhoneExist: false,
      showModalAccessories: false,
      showfloatingbutton: this.props.location.pathname.includes("inputprospect")
        ? false
        : true,
      sendEmailOrSMS: "",

      flagPrevTabs: "",

      isDisableIsMvGodig: false,

      isErrIsMvGodig: false,

      isErrProsName: false,
      isErrPhone1: false,
      isErrPhone2: false,
      isErrEmail: false,
      isErrDealer: false,
      isErrSalesman: false,

      isErrVehBrand: false,
      isErrVehYear: false,
      isErrVehType: false,
      isErrVehSeries: false,
      isErrVehUsage: false,
      isErrVehRegion: false,
      isErrVehRegistrationNumber: false,
      isErrSearch: false,

      IsSRCCCheckedEnable: 1,
      IsETVCheckedEnable: 1,
      IsFLDCheckedEnable: 1,
      IsTRSCheckedEnable: 1,
      IsPADRIVERCheckedEnable: 1,
      IsPAPASSCheckedEnable: 1,

      IsSRCCCheckedEnableDays: 0,
      IsETVCheckedEnableDays: 0,
      IsFLDCheckedEnableDays: 0,

      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrCovSumInsured: false,
      isErrPeriodFrom: false,
      isErrPeriodTo: false,
      isLoading: false,
      activetab: tabstitle[0],
      tabsDisabledState: [false, true, true, true],
      datadealer: [],
      DealerCode: "0",
      datasalesman: [],
      SalesDealer: "0",
      isCompany: false,
      isMvGodig: false,
      isNonMvGodig: false,
      vehicleperiodfrom: null,
      vehicleperiodto: null,
      vehicleperiodfrommodalperiod: null,
      vehicleperiodtomodalperiod: null,
      Name: "",
      Phone1: "",
      Phone2: "",
      Email1: "",
      Email2: null,
      ChannelSource: "",
      CustIDAAB: null,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      PolicyNo: "",
      CustId: "",
      GuidTempPenawaran: "",
      FollowUpNumber: "",
      NextFollowUpDate: "",
      LastFollowUpDate: "",
      FollowUpStatus: "1",
      FollowUpInfo: "1",
      Remark: "",
      BranchCode: "",
      PolicyId: "",
      PolicyNo: "",
      TransactionNo: "",
      SalesAdminID: null,
      SendDocDate: null,
      KTP: "",
      dataKTP: [],
      STNK: "",
      dataSTNK: [],
      SPPAKB: "",
      dataSPPAKB: [],
      BSTB1: "",
      dataBSTB1: [],
      BSTB2: "",
      dataBSTB2: [],
      BSTB3: "",
      dataBSTB3: [],
      BSTB4: "",
      dataBSTB4: [],
      CheckListSurvey1: "",
      dataCheckListSurvey1: [],
      CheckListSurvey2: "",
      dataCheckListSurvey2: [],
      CheckListSurvey3: "",
      dataCheckListSurvey3: [],
      CheckListSurvey4: "",
      dataCheckListSurvey4: [],
      BUKTIBAYAR: "",
      dataBUKTIBAYAR: [],
      PaymentReceipt2: "",
      dataPaymentReceipt2: [],
      PaymentReceipt3: "",
      dataPaymentReceipt3: [],
      PaymentReceipt4: "",
      dataPaymentReceipt4: [],
      BUKTIBAYAR: "",
      PremiumCalculation1: "",
      dataPremiumCalculation1: [],
      PremiumCalculation2: "",
      dataPremiumCalculation2: [],
      PremiumCalculation3: "",
      dataPremiumCalculation3: [],
      PremiumCalculation4: "",
      dataPremiumCalculation4: [],
      FormA1: "",
      dataFormA1: [],
      FormA2: "",
      dataFormA2: [],
      FormA3: "",
      dataFormA3: [],
      FormA4: "",
      dataFormA4: [],
      FormB1: "",
      dataFormB1: [],
      FormB2: "",
      dataFormB2: [],
      FormB3: "",
      dataFormB3: [],
      FormB4: "",
      dataFormB4: [],
      FormB1: "",
      dataFormC1: [],
      FormC2: "",
      dataFormC2: [],
      FormC3: "",
      dataFormC3: [],
      FormC4: "",
      dataFormC4: [],

      flagWilayah: false,

      VehicleUsedCar: false,
      VehiclePrice: "",
      VehicleCodetemp: "",
      VehicleCode: "",
      VehicleBrand: "",
      datavehiclebrand: [],
      vehicleModelCode: "",
      VehicleYear: "",
      datavehicleyear: [],
      VehicleType: "",
      datavehicletype: [],
      VehicleSeries: "",
      datavehicleseries: [],
      VehicleUsage: "",
      datavehicleusage: [],
      VehicleRegion: "",
      datavehicleregion: [],
      VehicleRegistrationNumber: "",
      VehicleChasisNumber: "",
      VehicleEngineNumber: "",
      searchVehicle: "",
      datasearch: [],
      datavehicleall: [],
      dataMixCover: [
        "33",
        "34",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42"
      ],
      datacoverproductcodeall: [],

      OrderNo: "",
      AccessSI: "",
      Ndays: null,
      dataTPLSIall: [],
      tempCovSumInsured: "",

      VehicleProductTypeCode: "",
      VehicleTypeType: "",
      VehicleSitting: "",
      VehicleSumInsured: "",
      loadingvehicletocover: false,

      dataListProductType: [],
      databasiccoverall: [],

      MessageAlertCover: "",

      CoverProductType: "0",
      datacoverproducttype: [],
      CoverProductCode: "",
      datacoverproductcode: [],
      CoverBasicCover: "",
      datacoverbasiccover: [],
      CovSumInsured: "",
      CoverTLOPeriod: 0,
      CoverComprePeriod: 0,
      CoverLastInterestNo: 0,
      CoverLastCoverageNo: 0,

      CalculatedPremiItems: [],
      coveragePeriodItems: [],
      coveragePeriodNonBasicItems: [],
      IsTPLEnabled: 0,
      IsTPLChecked: 0,
      IsTPLSIEnabled: 0,
      IsSRCCChecked: 0,
      IsSRCCEnabled: 0,
      IsFLDChecked: 0,
      IsFLDEnabled: 0,
      IsETVChecked: 0,
      IsETVEnabled: 0,
      IsTSChecked: 0,
      IsTSEnabled: 0,
      IsPADRVRChecked: 0,
      IsPADRVREnabled: 0,
      IsPADRVRSIEnabled: 0,
      IsPASSEnabled: 0,
      IsPAPASSSIEnabled: 0,
      IsPAPASSEnabled: 0,
      IsPAPASSChecked: 0,
      IsACCESSChecked: 0,
      IsACCESSSIEnabled: 0,
      IsACCESSEnabled: 0,
      SRCCPremi: 0,
      FLDPremi: 0,
      ETVPremi: 0,
      TSPremi: 0,
      PADRVRPremi: 0,
      PAPASSPremi: 0,
      TPLPremi: 0,
      ACCESSPremi: 0,
      AdminFee: 0,
      TotalPremi: 0,
      Alert: "",

      PremiumModel: null,

      MultiYearF: false,
      dataextsi: [],

      chOne: 0,
      chTwo: 0,
      chThree: 0,
      chOneEnabled: 0,
      chTwoEnable: 0,
      chThreeEnabled: 0,
      chOneEnabledVisible: 0,
      chTwoEnableVisible: 0,
      chThreeEnabledVisible: 0,

      TPLCoverageId: null,
      dataTPLSI: [],
      PASSCOVER: 0,
      PAPASSICOVER: 0,
      PADRVCOVER: 0,
      ACCESSCOVER: 0,
      dataAccessories: [],

      SumName: "Loading...",
      SumPhone1: "",
      SumEmail1: "",
      SumVehicle: "",
      SumCoverageType: "",
      SumTotalPremium: "",
      SumSumInsured: "",
      SumAccessSI: "",
      SumSalesmanDesc: "",
      SumDealerDesc: "",

      isErrorProspect: false,
      isErrorVehicle: false,
      isErrorCover: false,
      message: "",

      odcModel: {
        CascoSI1: 0,
        CascoSI2: 0,
        CascoSI3: 0,
        CascoSI4: 0,
        CascoSI5: 0,
        CascoAccess1: 0,
        CascoAccess2: 0,
        CascoAccess3: 0,
        CascoAccess4: 0,
        CascoAccess5: 0,
        AccessSI1: 0,
        AccessSI2: 0,
        AccessSI3: 0,
        AccessSI4: 0,
        AccessSI5: 0,
        CascoRate1: 0,
        CascoRate2: 0,
        CascoRate3: 0,
        CascoRate4: 0,
        CascoRate5: 0,
        LoadingRate1: 0,
        LoadingRate2: 0,
        LoadingRate3: 0,
        LoadingRate4: 0,
        LoadingRate5: 0,
        BasicPremium1: 0,
        BasicPremium2: 0,
        BasicPremium3: 0,
        BasicPremium4: 0,
        BasicPremium5: 0,
        BundlingPremium1: 0,
        BundlingPremium2: 0,
        BundlingPremium3: 0,
        BundlingPremium4: 0,
        BundlingPremium5: 0,
        IsBundling1: 0,
        IsBundling2: 0,
        IsBundling3: 0,
        IsBundling4: 0,
        IsBundling5: 0,
        TSPremium1: 0,
        TSPremium2: 0,
        TSPremium3: 0,
        TSPremium4: 0,
        TSPremium5: 0,
        TPLPremium1: 0,
        TPLPremium2: 0,
        TPLPremium3: 0,
        TPLPremium4: 0,
        TPLPremium5: 0,
        PADRVRPremium1: 0,
        PADRVRPremium2: 0,
        PADRVRPremium3: 0,
        PADRVRPremium4: 0,
        PADRVRPremium5: 0,
        PAPASSPremium1: 0,
        PAPASSPremium2: 0,
        PAPASSPremium3: 0,
        PAPASSPremium4: 0,
        PAPASSPremium5: 0,
        ExtendPremium1: 0,
        ExtendPremium2: 0,
        ExtendPremium3: 0,
        ExtendPremium4: 0,
        ExtendPremium5: 0,
        BExtendPremium1: 0,
        BExtendPremium2: 0,
        BExtendPremium3: 0,
        BExtendPremium4: 0,
        BExtendPremium5: 0,
        TotalPremium1: 0,
        TotalPremium2: 0,
        TotalPremium3: 0,
        TotalPremium4: 0,
        TotalPremium5: 0,
        TPLSI: 0,
        PADRVRSI: 0,
        PASS: 0,
        PAPASSSI: 0,
        AdminFee: 0,

        // 0283/URF/2015 BSY
        BundlingPremiumRate: 0,
        TSPremiumRate: 0,
        TPLPremiumRate: 0,
        PADRVRPremiumRate: 0,
        PAPASSPremiumRate: 0
        // END
      }
    };

    this.hitratecalculationloading = false;

    this.state = window.location.pathname.includes("tasklist-edit")
      ? JSON.parse(secureStorage.getItem("statetasklistedit")) || statedefault
      : window.location.pathname.includes("inputprospect")
      ? JSON.parse(secureStorage.getItem("stateinput")) || statedefault
      : statedefault;

    if (window.location.pathname.includes("inputprospect")) {
      secureStorage.removeItem("stateinput");
    } else if (window.location.pathname.includes("tasklist-edit")) {
      secureStorage.removeItem("statetasklistedit");
    }
  }

  formatDate = dates => {
    var dd = dates.getDate();
    var mm = dates.getMonth(); //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }

    return yyyy + "-" + mm + "-" + dd;
  };

  formatTimeFromDate = dates => {
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    return hh + ":" + mm;
  };

  setOrderDetailCoverage = odlcModels => {
    var odcModelState = { ...this.state.odcModel };
    for (let i = 0; i < odlcModels.length; i++) {
      const item = odlcModels[i];
      Log.debugGroup("odlcModels: ", odlcModels);
      if (item.Year == 1) {
        this.setState(
          {
            isYearTwoExist: false,
            isYearThreeExist: false,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "1";
          }
        );

        odcModelState.CascoSI1 = item.VehiclePremi;
        odcModelState.AccessSI1 = item.AccessPremi;
        odcModelState.CascoAccess1 = item.VehicleAccessPremi;
        odcModelState.CascoRate1 = item.Rate;
        odcModelState.LoadingRate1 = item.LoadingPremi;
        odcModelState.BasicPremium1 = item.BasicPremi;
        odcModelState.BundlingPremium1 = item.BasiCoverage;
        odcModelState.TSPremium1 = item.TSCoverage;
        odcModelState.TPLPremium1 = item.TPLCoverage;
        odcModelState.PADRVRPremium1 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium1 = item.PAPASSCoverage;
        odcModelState.ExtendPremium1 = item.PremiPerluasan;
        odcModelState.BExtendPremium1 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium1 = item.TotalPremi;
      } else if (item.Year == 2) {
        odcModelState.CascoSI2 = item.VehiclePremi;
        odcModelState.AccessSI2 = item.AccessPremi;
        odcModelState.CascoAccess2 = item.VehicleAccessPremi;
        odcModelState.CascoRate2 = item.Rate;
        odcModelState.LoadingRate2 = item.LoadingPremi;
        odcModelState.BasicPremium2 = item.BasicPremi;
        odcModelState.BundlingPremium2 = item.BasiCoverage;
        odcModelState.TSPremium2 = item.TSCoverage;
        odcModelState.TPLPremium2 = item.TPLCoverage;
        odcModelState.PADRVRPremium2 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium2 = item.PAPASSCoverage;
        odcModelState.ExtendPremium2 = item.PremiPerluasan;
        odcModelState.BExtendPremium2 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium2 = item.TotalPremi;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: false,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "2";
          }
        );
      } else if (item.Year == 3) {
        odcModelState.CascoSI3 = item.VehiclePremi;
        odcModelState.AccessSI3 = item.AccessPremi;
        odcModelState.CascoAccess3 = item.VehicleAccessPremi;
        odcModelState.CascoRate3 = item.Rate;
        odcModelState.LoadingRate3 = item.LoadingPremi;
        odcModelState.BasicPremium3 = item.BasicPremi;
        odcModelState.BundlingPremium3 = item.BasiCoverage;
        odcModelState.TSPremium3 = item.TSCoverage;
        odcModelState.TPLPremium3 = item.TPLCoverage;
        odcModelState.PADRVRPremium3 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium3 = item.PAPASSCoverage;
        odcModelState.ExtendPremium3 = item.PremiPerluasan;
        odcModelState.BExtendPremium3 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium3 = item.TotalPremi;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: false,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "3";
          }
        );
      } else if (item.Year == 4) {
        odcModelState.CascoSI4 = item.VehiclePremi;
        odcModelState.AccessSI4 = item.AccessPremi;
        odcModelState.CascoAccess4 = item.VehicleAccessPremi;
        odcModelState.CascoRate4 = item.Rate;
        odcModelState.LoadingRate4 = item.LoadingPremi;
        odcModelState.BasicPremium4 = item.BasicPremi;
        odcModelState.BundlingPremium4 = item.BasiCoverage;
        odcModelState.TSPremium4 = item.TSCoverage;
        odcModelState.TPLPremium4 = item.TPLCoverage;
        odcModelState.PADRVRPremium4 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium4 = item.PAPASSCoverage;
        odcModelState.ExtendPremium4 = item.PremiPerluasan;
        odcModelState.BExtendPremium4 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium4 = item.TotalPremi;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: true,
            isYearFiveExist: false
          },
          () => {
            this.state.TotalYear = "4";
          }
        );
      } else if (item.Year == 5) {
        odcModelState.CascoSI5 = item.VehiclePremi;
        odcModelState.AccessSI5 = item.AccessPremi;
        odcModelState.CascoAccess5 = item.VehicleAccessPremi;
        odcModelState.CascoRate5 = item.Rate;
        odcModelState.LoadingRate5 = item.LoadingPremi;
        odcModelState.BasicPremium5 = item.BasicPremi;
        odcModelState.BundlingPremium5 = item.BasiCoverage;
        odcModelState.TSPremium5 = item.TSCoverage;
        odcModelState.TPLPremium5 = item.TPLCoverage;
        odcModelState.PADRVRPremium5 = item.PADRVRCoverage;
        odcModelState.PAPASSPremium5 = item.PAPASSCoverage;
        odcModelState.ExtendPremium5 = item.PremiPerluasan;
        odcModelState.BExtendPremium5 = item.PremiDasarPerluasan;
        odcModelState.TotalPremium5 = item.TotalPremi;

        this.setState(
          {
            isYearTwoExist: true,
            isYearThreeExist: true,
            isYearFourExist: true,
            isYearFiveExist: true
          },
          () => {
            this.state.TotalYear = "5";
          }
        );
      }
    }

    this.setState({ odcModel: odcModelState });

    // console.log(odcModelState);
  };

  ShowAccessoryInfo = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getAccessoriesList`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      })
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          label: `${data.Description}`,
          value: `${data.MaxSI}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            dataAccessories: datamapping,
            isLoading: false,
            showModalAccessories: true
          },
          () => {
            if (this.props.location.pathname.includes("inputprospect")) {
              this.props.history.push("/inputprospect#");
            } else {
              this.props.history.push("/tasklist-edit#");
            }
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.ShowAccessoryInfo();
        }
      });
  };

  // handle disable checklist mv godig
  isDisableMVGodig = () => {
    let res = false;

    if (this.state.isDisableIsMvGodig) {
      res = true;
    }

    return res;
  };

  // this function use for handle change data by any condition
  // if return true, successfull passed handling
  // if return false, error handle change data
  handlePassSubmitByCondition = () => {
    if (this.state.FollowUpStatus == 6 && this.state.FollowUpInfo == 28) {
      toast.dismiss();
      toast.warning(
        "❗ You can't change this data because policy status is waiting approval",
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        }
      );
      // this.onClickTabs("Prospect Info");
      return false;
    }

    if (this.state.FollowUpStatus == 7) {
      toast.dismiss();
      toast.warning(
        "❗ You can't change this data because policy has been created",
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        }
      );
      // this.onClickTabs("Prospect Info");
      return false;
    }

    return true;
  };

  onSubmitProspect = event => {
    event.preventDefault();
    this.setState({
      isErrProsName: false,
      isErrIsMvGodig: false,
      isErrPhone1: false,
      isErrDealer: false,
      isErrSalesman: false,
      isErrEmail: false,
      isErrPhone2: false
    });
    const state = this.state;
    let errorField = "";
    if (state.Name === "") {
      this.setState({
        isErrProsName: true
      });
      errorField += "{Prospect Name} ";
    }

    if (state.Phone1 == null) {
      this.setState({
        isErrPhone1: true
      });
      errorField += "{Prospect Phone Number 1 must cannot empty} ";
      toast.dismiss();
      toast.warning("Prospect phone number tidak boleh kosong!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    } else {
      if (state.Phone1.length < 9 || state.Phone1.length > 13) {
        this.setState({
          isErrPhone1: true
        });
        errorField +=
          "{Prospect Phone Number 1 must be filled with 9 - 13 characters} ";
        toast.dismiss();
        toast.warning("Prospect phone number harus 9 - 13 karakter", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }
    }

    if (state.Phone2 != null && state.Phone2 != "") {
      if (state.Phone2.length < 9 || state.Phone2.length > 13) {
        this.setState({
          isErrPhone2: true
        });
        errorField +=
          "{Prospect Phone Number 2 must be filled with 9 - 13 characters} ";
        toast.dismiss();
        toast.warning("Prospect phone number harus 9 - 13 karakter", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return;
      }
    }

    if (
      state.Email1 != "" &&
      !Util.validateEmail(state.Email1)
    ) {
      this.setState({
        isErrEmail: true
      });
      errorField += "{Email is not valid} ";
      toast.dismiss();
      toast.warning("❗ Email is not valid", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (errorField != "") {
      this.setState({
        isErrorProspect: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (!this.handlePassSubmitByCondition()) {
      return;
    }

    if (this.state.Name.length > 100) {
      toast.warning("Panjang maksimum 100 karakter!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }

    var tabsDisabledState = [...this.state.tabsDisabledState];
    tabsDisabledState[1] = false;
    this.setState({
      tabsDisabledState: tabsDisabledState,
      isErrorProspect: false
    });
    this.onClickTabs(tabstitle[1]);
    // console.log("Dealer Code : " + this.state.DealerCode + " | Salesman Dealer : " + this.state.SalesDealer);
  };

  onSubmitVehicle = e => {
    e.preventDefault();
    this.setState({
      isErrVehCode: false,
      isErrVehBrand: false,
      isErrVehYear: false,
      isErrVehType: false,
      isErrVehSeries: false,
      isErrVehUsage: false,
      isErrVehRegion: false,
      isErrVehRegistrationNumber: false,
      loadingvehicletocover: false,
      isErrSearch: false
    });

    const state = this.state;
    let errorField = "";

    if (state.searchVehicle === "") {
      this.setState({
        isErrSearch: true
      });
      errorField += "{Vehicle Search} ";
    }

    if (state.VehicleUsage === "") {
      this.setState({
        isErrVehUsage: true
      });
      errorField += "{Vehicle Usage} ";
    }

    if (state.VehicleRegion === "") {
      this.setState({
        isErrVehRegion: true
      });
      errorField += "{Vehicle Region} ";
    }

    if (errorField != "") {
      this.setState({
        isErrorVehicle: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (!Util.isNullOrEmpty(this.state.VehicleRegistrationNumber)) {
      if (this.isNotValidSaveVehicle()) {
        return;
      }
    }

    var tabsDisabledState = [...this.state.tabsDisabledState];
    tabsDisabledState[2] = false;
    this.setState({
      tabsDisabledState: tabsDisabledState,
      isErrorVehicle: false
    });
    this.onClickTabs(tabstitle[2]);
  };

  onSubmitCover = e => {
    e.preventDefault();

    this.setState({
      isErrCovProductType: false,
      isErrCovProductCode: false,
      isErrCovBasicCover: false,
      isErrPeriodFrom: false,
      isErrPeriodTo: false
    });

    const state = this.state;
    let errorField = "";

    if (state.CoverProductType === "") {
      this.setState({
        isErrCovProductType: true
      });
      errorField += "{Product Type} ";
    }

    if (state.CoverProductCode === "" || state.CoverProductCode == null) {
      this.setState({
        isErrCovProductCode: true
      });
      errorField += "{Product Code} ";
    }

    if (state.CoverBasicCover === "") {
      this.setState({
        isErrCovBasicCover: true
      });
      errorField += "{Basic Cover} ";
    }

    if (state.CovSumInsured === "") {
      this.setState({
        isErrCovSumInsured: true
      });
      errorField += "{Sum Insured} ";
    }

    if (state.vehicleperiodfrom === "") {
      this.setState({
        isErrPeriodFrom: true
      });
      errorField += "{Period From} ";
    }

    if (state.vehicleperiodto === "") {
      this.setState({
        isErrPeriodTo: true
      });
      errorField += "{Period To} ";
    }

    if (errorField != "") {
      this.setState({
        isErrorCover: true,
        messageError: errorField + " is required!"
      });
      toast.dismiss();
      toast.warning("Harap mengisi semua field yang dibutuhkan!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (this.state.CalculatedPremiItems.length == 0) {
      toast.dismiss();
      toast.warning("Harap Memilih Period To Yang Sesuai!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (this.state.CovSumInsured.length == 0) {
      this.onShowAlertModalInfo("Please insert vehicle Sum Insured");
      return;
    }

    if (this.state.IsPADRVRChecked == 1 && this.state.PADRVCOVER == 0) {
      this.onShowAlertModalInfo("Please insert PA Driver Sum Insured");
      return;
    }

    if (this.state.IsPASSEnabled == 1 && this.state.PAPASSICOVER == 0) {
      this.onShowAlertModalInfo("Please insert PA Passanger Sum Insured");
      return;
    }

    if (this.state.IsPAPASSSIEnabled == 1 && this.state.PASSCOVER == 0) {
      this.onShowAlertModalInfo("Please insert number of Passanger");
      return;
    }

    if (
      parseInt(this.state.PADRVCOVER + "") >
      parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "PA Driver Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    if (
      parseInt(this.state.PAPASSICOVER + "") >
      parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "PA Passanger Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    if (this.state.IsACCESSChecked == 1 && this.state.ACCESSCOVER == 0) {
      this.onShowAlertModalInfo("Please insert Accessory Sum Insured");
      return;
    }

    if (
      this.state.IsACCESSChecked == 1 &&
      parseInt(this.state.ACCESSCOVER + "") >
        parseInt(this.state.CovSumInsured + "")
    ) {
      this.onShowAlertModalInfo(
        "Accessory Sum Insured can't be more than vehicle Sum Insured"
      );
      return;
    }

    if (this.hitratecalculationloading == true) {
      toast.dismiss();
      toast.warning("❗ Please wait calculating premi!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (state.isErrCovSumInsured) {
      this.onShowAlertModalInfo("Sum Insured melebihi +/-3% dari pricelist");
      this.setState({
        isErrCovSumInsured: true,
        CovSumInsured: 0
      });
      this.onClickTabs(tabstitle[2]);
    } else {
      var tabsDisabledState = [...this.state.tabsDisabledState];
      tabsDisabledState[3] = false;
      this.setState({
        tabsDisabledState: tabsDisabledState,
        isErrorCover: false,
        flagPrevTabs: "Summary"
      });
      this.onClickTabs(tabstitle[3]);
      console.log("masuk submit cover");
    }
  };

  componentWillUnmount() {
    this.abortControllerBasicPremium.abort();
    this.abortController.abort();
    clearInterval(this.accesoriesInterval);
  }

  componentDidMount() {
    this.abortControllerBasicPremium = new AbortController();
    this.abortController = new AbortController();
    this.accesoriesInterval = setInterval(() => {
      if (
        this.props.history.action == "POP" &&
        this.state.showModalAccessories
      ) {
        this.setState({ showModalAccessories: false });
      }
    }, 100);

    var BranchCode = JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode;
    this.state.ChannelSource = JSON.parse(
      ACCOUNTDATA
    ).UserInfo.User.ChannelSource;
    this.setState({ BranchCode });
    // // this.getListDealer();
    // console.log(window.location.pathname);
    if (window.location.pathname == "/tasklist-edit") {
      console.log("haja");
      this.setState({ tabsDisabledState: [false, false, true, true] });

      const FUTaskList = JSON.parse(secureStorage.getItem("FollowUpSelected"));
      const PCTaskList = JSON.parse(
        secureStorage.getItem("ProspectCustomerSelected")
      );
      const OSTaskLIst = JSON.parse(
        secureStorage.getItem("OrderSimulationSelected") || null
      );
      const OSMVTaskLIst = JSON.parse(
        secureStorage.getItem("OrderSimulationMVSelected") || null
      );
      const OSIITaskList = JSON.parse(
        secureStorage.getItem("OrderSimulationInterestSelected") || null
      );
      const OSICTaskList = JSON.parse(
        secureStorage.getItem("OrderSimulationCoverageSelected") || null
      );
      const ImageDataTaskList = JSON.parse(
        secureStorage.getItem("ImageDataSelected") || null
      );

      if (FUTaskList == null) {
        this.props.history.push("/");
        return;
      }

      this.setState(
        {
          DealerCode: PCTaskList.DealerCode,
          SalesDealer: PCTaskList.SalesDealer,
          isCompany: PCTaskList.isCompany,
          Name: FUTaskList.ProspectName,
          Phone1: PCTaskList.Phone1,
          Phone2: PCTaskList.Phone2,
          Email1: PCTaskList.Email1,
          Email2: PCTaskList.Email2,
          SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
          PolicyNo: FUTaskList.PolicyNo,
          // isCompany: false,
          CustId: FUTaskList.CustID,
          FollowUpNumber: FUTaskList.FollowUpNo,
          NextFollowUpDate: FUTaskList.NextFollowUpDate,
          LastFollowUpDate: FUTaskList.LastFollowUpDate,
          FollowUpStatus: FUTaskList.FollowUpStatus,
          FollowUpInfo: FUTaskList.FollowUpInfo,
          Remark: FUTaskList.Remark,
          BranchCode: FUTaskList.BranchCode,
          PolicyId: FUTaskList.PolicyId,
          PolicyNo: FUTaskList.PolicyNo,
          TransactionNo: FUTaskList.TransactionNo,
          SalesAdminID: FUTaskList.SalesAdminID,
          SendDocDate: FUTaskList.SendDocDate,
          KTP: FUTaskList.IdentityCard,
          dataKTP: [], // FROM DB API
          STNK: FUTaskList.STNK,
          dataSTNK: [], // FROM DB API
          SPPAKB: FUTaskList.SPPAKB,
          dataSPPAKB: [], // FROM DB API
          BSTB1: FUTaskList.BSTB1,
          dataBST1: [],
          BSTB2: FUTaskList.BSTB2,
          dataBST2: [],
          BSTB3: FUTaskList.BSTB3,
          dataBST3: [],
          BSTB4: FUTaskList.BSTB4,
          dataBST4: [],
          CheckListSurvey1: FUTaskList.CheckListSurvey1,
          dataCheckListSurvey1: [],
          CheckListSurvey2: FUTaskList.CheckListSurvey2,
          dataCheckListSurvey2: [],
          CheckListSurvey3: FUTaskList.CheckListSurvey3,
          dataCheckListSurvey3: [],
          CheckListSurvey4: FUTaskList.CheckListSurvey4,
          dataCheckListSurvey4: [],
          BUKTIBAYAR: FUTaskList.BUKTIBAYAR,
          dataBUKTIBAYAR: [],
          PaymentReceipt2: FUTaskList.PaymentReceipt2,
          dataPaymentReceipt2: [],
          PaymentReceipt3: FUTaskList.PaymentReceipt3,
          dataPaymentReceipt3: [],
          PaymentReceipt4: FUTaskList.PaymentReceipt4,
          dataPaymentReceipt4: [],
          PremiumCalculation1: FUTaskList.PremiumCalculation1,
          dataPremiumCalculation1: [],
          PremiumCalculation2: FUTaskList.PremiumCalculation2,
          dataPremiumCalculation2: [],
          PremiumCalculation3: FUTaskList.PremiumCalculation3,
          dataPremiumCalculation3: [],
          PremiumCalculation4: FUTaskList.PremiumCalculation4,
          dataPremiumCalculation4: [],
          FormA1: FUTaskList.FormA1,
          dataFormA1: [],
          FormA2: FUTaskList.FormA2,
          dataFormA2: [],
          FormA3: FUTaskList.FormA3,
          dataFormA3: [],
          FormA4: FUTaskList.FormA4,
          dataFormA4: [],
          FormB1: FUTaskList.FormB1,
          dataFormB1: [],
          FormB2: FUTaskList.FormB2,
          dataFormB2: [],
          FormB3: FUTaskList.FormB3,
          dataFormB3: [],
          FormB4: FUTaskList.FormB4,
          dataFormB4: [],
          FormC1: FUTaskList.FormC1,
          dataFormC1: [],
          FormC2: FUTaskList.FormC2,
          dataFormC2: [],
          FormC3: FUTaskList.FormC3,
          dataFormC3: [],
          FormC4: FUTaskList.FormC4,
          dataFormC4: [],
          FUBidStatus: FUTaskList.FUBidStatus,

          CalculatedPremiItems: [],
          coveragePeriodItems: [],
          coveragePeriodNonBasicItems: [],
          IsTPLEnabled: 0,
          IsTPLChecked: 0,
          IsTPLSIEnabled: 0,
          IsSRCCChecked: 0,
          IsFLDChecked: 0,
          IsETVChecked: 0,
          IsTSChecked: 0,
          IsTSEnabled: 0,
          IsPADRVRChecked: 0,
          IsPADRVRSIEnabled: 0,
          IsPASSEnabled: 0,
          IsPAPASSSIEnabled: 0,
          IsPAPASSEnabled: 0,
          IsPAPASSChecked: 0,
          SRCCPremi: 0,
          FLDPremi: 0,
          ETVPremi: 0,
          TSPremi: 0,
          PADRVRPremi: 0,
          PAPASSPremi: 0,
          TPLPremi: 0,
          AdminFee: 0,
          TotalPremi: 0,
          Alert: "",

          chOne: 0,
          chTwo: 0,
          chThree: 0,
          chOneEnabled: 0,
          chTwoEnable: 0,
          chThreeEnabled: 0,
          chOneEnabledVisible: 0,
          chTwoEnableVisible: 0,
          chThreeEnabledVisible: 0,

          MultiYearF: false,

          SumName: "Loading ...",
          SumPhone1: "",
          SumEmail1: "",
          SumVehicle: "",
          SumCoverageType: "",
          SumTotalPremium: "",
          SumSumInsured: "",
          SumAccessSI: "",
          SumSalesmanDesc: "",
          SumDealerDesc: "",

          isErrorProspect: false,
          isErrorVehicle: false,
          isErrorCover: false,
          messageError: ""
        },
        () => {
          this.onOptionDealerChange(this.state.DealerCode);
          this.setState({ SalesDealer: PCTaskList.SalesDealer });
          if (ImageDataTaskList.length > 0) {
            //////////////////////////////////////////////////////////////
            var dataimage = ImageDataTaskList.map(data => ({
              ori: data.Data,
              thumbnail: data.ThumbnailData,
              namefile: data.ImageID,
              ext:
                "data:image/" +
                data.PathFile.substring(
                  data.ImageID.length + 1,
                  data.PathFile.length
                ) +
                ";base64,"
            }));

            ImageDataTaskList.forEach(data => {
              var tempimage = [];

              tempimage[0] =
                "data:image/" +
                data.PathFile.substring(
                  data.ImageID.length + 1,
                  data.PathFile.length
                ) +
                ";base64," +
                data.Data;

              tempimage[1] =
                "data:image/" +
                data.PathFile.substring(
                  data.ImageID.length + 1,
                  data.PathFile.length
                ) +
                ";base64," +
                data.ThumbnailData;

              // // console.log(blobThumb);
              // tempimage[0] = blobOri;
              // tempimage[1] = blobThumb;

              // console.log(tempimage);
              tempimage[0] = this.dataURItoBlob(tempimage[0]);
              tempimage[1] = this.dataURItoBlob(tempimage[1]);

              if (data.PathFile == this.state.KTP) {
                this.setState({ dataKTP: tempimage });
              } else if (data.PathFile == this.state.STNK) {
                this.setState({ dataSTNK: tempimage });
              } else if (data.PathFile == this.state.SPPAKB) {
                this.setState({ dataSPPAKB: tempimage });
              } else if (data.PathFile == this.state.BSTB1) {
                this.setState({ dataBSTB1: tempimage });
              } else if (data.PathFile == this.state.BSTB2) {
                this.setState({ dataBSTB2: tempimage });
              } else if (data.PathFile == this.state.BSTB3) {
                this.setState({ dataBSTB3: tempimage });
              } else if (data.PathFile == this.state.BSTB4) {
                this.setState({ dataBSTB4: tempimage });
              } else if (data.PathFile == this.state.CheckListSurvey1) {
                this.setState({ dataCheckListSurvey1: tempimage });
              } else if (data.PathFile == this.state.CheckListSurvey2) {
                this.setState({ dataCheckListSurvey2: tempimage });
              } else if (data.PathFile == this.state.CheckListSurvey3) {
                this.setState({ dataCheckListSurvey3: tempimage });
              } else if (data.PathFile == this.state.CheckListSurvey4) {
                this.setState({ dataCheckListSurvey4: tempimage });
              } else if (data.PathFile == this.state.BUKTIBAYAR) {
                this.setState({ dataBUKTIBAYAR: tempimage });
              } else if (data.PathFile == this.state.PaymentReceipt2) {
                this.setState({ dataPaymentReceipt2: tempimage });
              } else if (data.PathFile == this.state.PaymentReceipt3) {
                this.setState({ dataPaymentReceipt3: tempimage });
              } else if (data.PathFile == this.state.PaymentReceipt4) {
                this.setState({ dataPaymentReceipt4: tempimage });
              } else if (data.PathFile == this.state.PremiumCalculation1) {
                this.setState({ dataPremiumCalculation1: tempimage });
              } else if (data.PathFile == this.state.PremiumCalculation2) {
                this.setState({ dataPremiumCalculation2: tempimage });
              } else if (data.PathFile == this.state.PremiumCalculation3) {
                this.setState({ dataPremiumCalculation3: tempimage });
              } else if (data.PathFile == this.state.PremiumCalculation4) {
                this.setState({ dataPremiumCalculation4: tempimage });
              } else if (data.PathFile == this.state.FormA1) {
                this.setState({ dataFormA1: tempimage });
              } else if (data.PathFile == this.state.FormA2) {
                this.setState({ dataFormA2: tempimage });
              } else if (data.PathFile == this.state.FormA3) {
                this.setState({ dataFormA3: tempimage });
              } else if (data.PathFile == this.state.FormA4) {
                this.setState({ dataFormA4: tempimage });
              } else if (data.PathFile == this.state.FormB1) {
                this.setState({ dataFormB1: tempimage });
              } else if (data.PathFile == this.state.FormB2) {
                this.setState({ dataFormB2: tempimage });
              } else if (data.PathFile == this.state.FormB3) {
                this.setState({ dataFormB3: tempimage });
              } else if (data.PathFile == this.state.FormB4) {
                this.setState({ dataFormB4: tempimage });
              } else if (data.PathFile == this.state.FormC1) {
                this.setState({ dataFormC1: tempimage });
              } else if (data.PathFile == this.state.FormC2) {
                this.setState({ dataFormC2: tempimage });
              } else if (data.PathFile == this.state.FormC3) {
                this.setState({ dataFormC3: tempimage });
              } else if (data.PathFile == this.state.FormC4) {
                this.setState({ dataFormC4: tempimage });
              }
            });

            // console.log(dataimage);
          }
        }
      );

      if (OSTaskLIst != null) {
        this.setState({
          OrderNo: OSTaskLIst.OrderNo,
          QuotationNo: OSTaskLIst.QuotationNo,
          PhoneSales: this.props.userInfo.User.Phone1,
          SendStatus: OSTaskLIst.SendStatus,
          SendDate: OSTaskLIst.SendDate,
          ApplyF: OSTaskLIst.ApplyF,
          SendF: OSTaskLIst.SendF,
          LastInterestNo: OSTaskLIst.LastInterestNo,
          LastCoverageNo: OSTaskLIst.LastCoverageNo,

          CoverProductType: OSTaskLIst.InsuranceType,
          CoverProductCode: OSTaskLIst.ProductCode,
          CoverBasicCover: "",
          CoverTLOPeriod: OSTaskLIst.TLOPeriod,
          CoverComprePeriod: OSTaskLIst.ComprePeriod,
          CoverLastInterestNo: OSTaskLIst.LastInterestNo,
          CoverLastCoverageNo: OSTaskLIst.LastCoverageNo
        });
        this.setState({ selectedQuotation: OSTaskLIst.OrderNo });
        this.setState({
          OrderNoQuotationEdit: OSTaskLIst.OrderNo,
          FollowUpNoQuotationEdit: FUTaskList.FollowUpNo
        });
      }

      if (OSMVTaskLIst.length > 0) {
        this.setState({
          VehicleCode: OSMVTaskLIst[0].VehicleCode,
          VehicleBrand: OSMVTaskLIst[0].BrandCode,
          VehicleYear: OSMVTaskLIst[0].Year,
          VehicleType: OSMVTaskLIst[0].ModelCode,
          VehicleSeries: OSMVTaskLIst[0].Series,
          VehicleProductTypeCode: OSMVTaskLIst[0].ProductTypeCode,
          VehicleTypeType: OSMVTaskLIst[0].Type,
          VehicleSitting: OSMVTaskLIst[0].Sitting,
          VehicleSumInsured: OSMVTaskLIst[0].SumInsured,
          VehicleUsage: OSMVTaskLIst[0].UsageCode,
          VehicleRegion: OSMVTaskLIst[0].CityCode,
          AccessSI: OSMVTaskLIst[0].AccessSI,
          searchVehicle: OSMVTaskLIst[0].searchVehicle,
          vehicleModelCode: OSMVTaskLIst[0].ModelCode
        });
      }
    }

    this.onClickTabs(this.state.activetab);

    if (window.location.pathname == "/tasklist-edit") {
      if (JSON.parse(secureStorage.getItem("tasklist-edit-state")) != null) {
        this.setState(JSON.parse(secureStorage.getItem("tasklist-edit-state")));
        secureStorage.removeItem("tasklist-edit-state");
      }
    }

    if (window.location.pathname == "/inputprospect") {
      if (JSON.parse(secureStorage.getItem("inputprospect-state")) != null) {
        this.setState(JSON.parse(secureStorage.getItem("inputprospect-state")));
        secureStorage.removeItem("inputprospect-state");
      }
    }

    // Handle default checklist isMVGodig
    this.defaultChecklistMVGodig();
  }

  // Handle default checklist isMVGodig
  defaultChecklistMVGodig = (callback = () => {}) => {
    let { isMvGodig, isNonMvGodig } = { ...this.state };
    if (this.isRoleTelesales()) {
      isMvGodig = true;
      isNonMvGodig = false;
    } else {
      isMvGodig = false;
      isNonMvGodig = true;
    }

    this.setState(
      {
        isMvGodig,
        isNonMvGodig
      },
      () => {
        callback();
      }
    );
  };

  dataURItoBlob = dataURI => {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(",")[1]);

    // separate out the mime component
    var mimeString = dataURI
      .split(",")[0]
      .split(":")[1]
      .split(";")[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab]);
    return bb;
  };

  setModelInputProspect = (
    FUTaskList1,
    PCTaskList1,
    OSTaskLIst1,
    OSMVTaskLIst1,
    OSIITaskList1,
    OSICTaskList1
  ) => {
    console.log("============================================");
    const FUTaskList =
      FUTaskList1 == null
        ? JSON.parse(secureStorage.getItem("FollowUpSelected"))
        : FUTaskList1;
    const PCTaskList =
      PCTaskList1 == null
        ? JSON.parse(secureStorage.getItem("ProspectCustomerSelected"))
        : PCTaskList1;
    const OSTaskLIst =
      OSTaskLIst1 == null
        ? JSON.parse(secureStorage.getItem("OrderSimulationSelected"))
        : OSTaskLIst1;
    const OSMVTaskLIst =
      OSMVTaskLIst1 == null
        ? JSON.parse(secureStorage.getItem("OrderSimulationMVSelected"))
        : OSMVTaskLIst1;
    const OSIITaskList =
      OSIITaskList1 == null
        ? JSON.parse(secureStorage.getItem("OrderSimulationInterestSelected"))
        : OSIITaskList1;
    const OSICTaskList =
      OSICTaskList1 == null
        ? JSON.parse(secureStorage.getItem("OrderSimulationCoverageSelected"))
        : OSICTaskList1;

    console.log("Masuk - setModelInputProspect");

    this.setState(
      {
        DealerCode: PCTaskList.DealerCode,
        SalesDealer: PCTaskList.SalesDealer,
        Name: FUTaskList.ProspectName,
        Phone1: PCTaskList.Phone1,
        Phone2: PCTaskList.Phone2,
        Email1: PCTaskList.Email1,
        Email2: PCTaskList.Email2,
        SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        PolicyNo: FUTaskList.PolicyNo,
        isCompany: false,
        CustId: FUTaskList.CustID,
        FollowUpNumber: FUTaskList.FollowUpNo,
        NextFollowUpDate: FUTaskList.NextFollowUpDate,
        LastFollowUpDate: FUTaskList.LastFollowUpDate,
        FollowUpStatus: FUTaskList.FollowUpStatus,
        FollowUpInfo: FUTaskList.FollowUpInfo,
        Remark: FUTaskList.Remark,
        BranchCode: FUTaskList.BranchCode,
        PolicyId: FUTaskList.PolicyId,
        PolicyNo: FUTaskList.PolicyNo,
        TransactionNo: FUTaskList.TransactionNo,
        SalesAdminID: FUTaskList.SalesAdminID,
        SendDocDate: FUTaskList.SendDocDate,
        KTP: FUTaskList.IdentityCard,
        dataKTP: [], // FROM DB API
        STNK: FUTaskList.STNK,
        dataSTNK: [], // FROM DB API
        SPPAKB: FUTaskList.SPPAKB,
        dataSPPAKB: [], // FROM DB API
        BSTB1: FUTaskList.BSTB1,
        dataBST1: [],
        BSTB2: FUTaskList.BSTB2,
        dataBST2: [],
        BSTB3: FUTaskList.BSTB3,
        dataBST3: [],
        BSTB4: FUTaskList.BSTB4,
        dataBST4: [],
        CheckListSurvey1: FUTaskList.CheckListSurvey1,
        dataCheckListSurvey1: [],
        CheckListSurvey2: FUTaskList.CheckListSurvey2,
        dataCheckListSurvey2: [],
        CheckListSurvey3: FUTaskList.CheckListSurvey3,
        dataCheckListSurvey3: [],
        CheckListSurvey4: FUTaskList.CheckListSurvey4,
        dataCheckListSurvey4: [],
        BUKTIBAYAR: FUTaskList.BUKTIBAYAR,
        dataBUKTIBAYAR: [],
        PaymentReceipt2: FUTaskList.PaymentReceipt2,
        dataPaymentReceipt2: [],
        PaymentReceipt3: FUTaskList.PaymentReceipt3,
        dataPaymentReceipt3: [],
        PaymentReceipt4: FUTaskList.PaymentReceipt4,
        dataPaymentReceipt4: [],
        PremiumCalculation1: FUTaskList.PremiumCalculation1,
        dataPremiumCalculation1: [],
        PremiumCalculation2: FUTaskList.PremiumCalculation2,
        dataPremiumCalculation2: [],
        PremiumCalculation3: FUTaskList.PremiumCalculation3,
        dataPremiumCalculation3: [],
        PremiumCalculation4: FUTaskList.PremiumCalculation4,
        dataPremiumCalculation4: [],
        FormA1: FUTaskList.FormA1,
        dataFormA1: [],
        FormA2: FUTaskList.FormA2,
        dataFormA2: [],
        FormA3: FUTaskList.FormA3,
        dataFormA3: [],
        FormA4: FUTaskList.FormA4,
        dataFormA4: [],
        FormB1: FUTaskList.FormB1,
        dataFormB1: [],
        FormB2: FUTaskList.FormB2,
        dataFormB2: [],
        FormB3: FUTaskList.FormB3,
        dataFormB3: [],
        FormB4: FUTaskList.FormB4,
        dataFormB4: [],
        FormC1: FUTaskList.FormC1,
        dataFormC1: [],
        FormC2: FUTaskList.FormC2,
        dataFormC2: [],
        FormC3: FUTaskList.FormC3,
        dataFormC3: [],
        FormC4: FUTaskList.FormC4,
        dataFormC4: [],
        FUBidStatus: FUTaskList.FUBidStatus,

        CalculatedPremiItems: [],
        coveragePeriodItems: [],
        coveragePeriodNonBasicItems: [],
        IsTPLEnabled: 0,
        IsTPLChecked: 0,
        IsTPLSIEnabled: 0,
        IsSRCCChecked: 0,
        IsFLDChecked: 0,
        IsETVChecked: 0,
        IsTSChecked: 0,
        IsTSEnabled: 0,
        IsPADRVRChecked: 0,
        IsPADRVRSIEnabled: 0,
        IsPASSEnabled: 0,
        IsPAPASSSIEnabled: 0,
        IsPAPASSEnabled: 0,
        IsPAPASSChecked: 0,
        SRCCPremi: 0,
        FLDPremi: 0,
        ETVPremi: 0,
        TSPremi: 0,
        PADRVRPremi: 0,
        PAPASSPremi: 0,
        TPLPremi: 0,
        AdminFee: 0,
        TotalPremi: 0,
        Alert: "",

        chOne: 0,
        chTwo: 0,
        chThree: 0,
        chOneEnabled: 0,
        chTwoEnable: 0,
        chThreeEnabled: 0,
        chOneEnabledVisible: 0,
        chTwoEnableVisible: 0,
        chThreeEnabledVisible: 0,

        MultiYearF: false,

        SumName: "sdnjhbj",
        SumPhone1: "",
        SumEmail1: "",
        SumVehicle: "",
        SumCoverageType: "",
        SumTotalPremium: "",
        SumSumInsured: "",
        SumAccessSI: "",
        SumSalesmanDesc: "",
        SumDealerDesc: "",

        isErrorProspect: false,
        isErrorVehicle: false,
        isErrorCover: false,
        messageError: ""
      },
      () => {
        this.onOptionDealerChange(this.state.DealerCode);
        this.setState({ SalesDealer: PCTaskList.SalesDealer });
      }
    );

    if (OSTaskLIst != null) {
      this.setState({
        OrderNo: OSTaskLIst.OrderNo,
        QuotationNo: OSTaskLIst.QuotationNo,
        PhoneSales: this.props.userInfo.User.Phone1,
        SendStatus: OSTaskLIst.SendStatus,
        SendDate: OSTaskLIst.SendDate,
        ApplyF: OSTaskLIst.ApplyF,
        SendF: OSTaskLIst.SendF,
        LastInterestNo: OSTaskLIst.LastInterestNo,
        LastCoverageNo: OSTaskLIst.LastCoverageNo,

        CoverProductType: OSTaskLIst.InsuranceType || "",
        CoverProductCode: OSTaskLIst.ProductCode,
        CoverBasicCover: OSTaskLIst.ProductTypeCode,
        CoverTLOPeriod: OSTaskLIst.TLOPeriod,
        CoverComprePeriod: OSTaskLIst.ComprePeriod,
        CoverLastInterestNo: OSTaskLIst.LastInterestNo,
        CoverLastCoverageNo: OSTaskLIst.LastCoverageNo
      });
      this.setState({ selectedQuotation: OSTaskLIst.OrderNo });
      this.setState({
        OrderNoQuotationEdit: OSTaskLIst.OrderNo,
        FollowUpNoQuotationEdit: FUTaskList.FollowUpNo
      });
    }

    Log.debugGroup("OSMV TASKLIST: ", OSMVTaskLIst);

    if (OSMVTaskLIst != null) {
      Log.debugStr("MASUK OSMV TASKLIST");
      this.setState({
        VehicleCodetemp: OSMVTaskLIst.VehicleCode || "",
        VehicleCode: OSMVTaskLIst.VehicleCode || "",
        VehicleBrand: OSMVTaskLIst.BrandCode || "",
        VehicleYear: OSMVTaskLIst.Year || "",
        VehicleType: OSMVTaskLIst.ModelCode || "",
        VehicleSeries: OSMVTaskLIst.Series || "",
        VehicleProductTypeCode: OSMVTaskLIst.ProductTypeCode || "",
        VehicleTypeType: OSMVTaskLIst.Type || "",
        VehicleSitting: OSMVTaskLIst.Sitting || "",
        VehicleSumInsured: OSMVTaskLIst.SumInsured || "",
        CovSumInsured: OSMVTaskLIst.SumInsured || "",
        VehicleUsage: OSMVTaskLIst.UsageCode || "",
        VehicleRegion: OSMVTaskLIst.CityCode || "",
        AccessSI: OSMVTaskLIst.AccessSI,
        searchVehicle: OSMVTaskLIst.searchVehicle || "",
        vehicleModelCode: OSMVTaskLIst.ModelCode
      });
    }
  };

  getListDealer = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getDealerName/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      })
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.DealerCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datadealer: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListDealer();
        }
      });
  };

  getListBrand = (isLoadingBefore = false) => {
    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleBrand/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      })
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.BrandCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehiclebrand: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({
            isLoading: false
          });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({
            isLoading: false
          });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListBrand(isLoadingBefore);
        }
      });
  };

  isValidSaveVehicle = () => {
    let res = false;

    if (
      !Util.isNullOrEmpty(this.state.VehicleRegion) &&
      !Util.isNullOrEmpty(this.state.VehicleCode) &&
      !Util.isNullOrEmpty(this.state.VehicleUsage)
    ) {
      res = true;
    }

    return res;
  };

  getListUsage = (isLoadingBefore = false, callback = () => {}) => {
    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getVehicleUsage/`,
      "POST",
      HEADER_API,
      {
        isMvGodig: this.state.isMvGodig
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.insurance_code}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState(
          {
            datavehicleusage: datamapping,
            VehicleUsage:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.VehicleUsage,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.VehicleUsage
                : ""
          },
          () => {
            this.onLoadDataFormCover();
          }
        );
        if (isLoadingBefore == false) {
          this.setState({
            isLoading: false
          });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({
            isLoading: false
          });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListUsage(isLoadingBefore, callback());
        }
      });
  };

  getListRegion = (isLoadingBefore = false) => {
    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleRegion/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      })
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.RegionCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicleregion: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListRegion(isLoadingBefore);
        }
      });
  };

  getVehiclePrice = (vehiclecode, year, citycode) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehiclePrice/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "vehiclecode=" + vehiclecode + "&year=" + year + "&citycode=" + citycode
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          CovSumInsured: jsn.data,
          tempCovSumInsured: jsn.data,
          VehiclePrice: jsn.data,
          VehicleSumInsured: jsn.data,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getVehiclePrice(vehiclecode, year, citycode);
        }
      });
  };

  getListProductType = () => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getProductType/`,
      "POST",
      HEADER_API,
      {
        Channel: JSON.parse(ACCOUNTDATA).UserInfo.User.Channel,
        salesofficerid: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        isMvGodig: this.state.isMvGodig
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({ dataListProductType: jsn.data });
        console.log(jsn);
        return jsn.data.map(data => ({
          value: `${data.InsuranceType}`,
          label: `${data.Description}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datacoverproducttype: datamapping,
            isLoading: false
          },
          () => {
            // if(!Util.isNullOrEmpty(this.state.CoverProductType) && this.state.CoverProductType != 0){
            //   this.onOptionCovProductTypeChange(this.state.CoverProductType);
            // }
          }
        );
        if (JSON.parse(ACCOUNTDATA).UserInfo.User.Channel == "EXT") {
          this.setState(
            {
              CoverProductType: datamapping[0].value
            },
            () => {
              this.onOptionCovProductTypeChange(datamapping[0].value);
            }
          );
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListProductType();
        }
      });
  };

  getListBasicCover = () => {
    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getBasicCover/`,
      "POST",
      HEADER_API,
      {
        Channel: JSON.parse(ACCOUNTDATA).UserInfo.User.Channel,
        salesofficerid: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        isBasic: true,
        vyear: "",
        isMvGodig: this.state.isMvGodig
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          databasiccoverall: jsn.BasicCover
        });
        return jsn.BasicCover.map(data => ({
          value: `${data.Id}`,
          label: `${data.Description}`,
          ComprePeriod: `${data.ComprePeriod}`,
          TLOPeriod: `${data.TLOPeriod}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datacoverbasiccover: datamapping,
            isLoading: false,
            CoverBasicCover:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.CoverBasicCover,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.CoverBasicCover
                : ""
          },
          () => {
            let CoverBasicCover = this.state.datacoverbasiccover.filter(
              data =>
                data.ComprePeriod == this.state.CoverComprePeriod &&
                data.TLOPeriod == this.state.CoverTLOPeriod
            );
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getListProductType();
        }
      });
  };

  getSummaryResult = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getSummaryResult/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "followupno=" + this.state.FollowUpNumber
    })
      .then(res => res.json())
      .then(jsn => {
        var datatemp = jsn.data;
        if (datatemp.length > 0) {
          this.setState({
            SumName: datatemp[0].Name,
            SumPhone1: datatemp[0].Phone1,
            SumEmail1: datatemp[0].Email1,
            SumVehicle: datatemp[0].Vehicle,
            SumCoverageType: datatemp[0].CoverageType,
            SumTotalPremium: datatemp[0].TotalPremium,
            SumSumInsured: datatemp[0].SumInsured,
            SumAccessSI: datatemp[0].AccessSI,
            SumSalesmanDesc: datatemp[0].SalesmanDesc,
            SumDealerDesc: datatemp[0].DealerDesc,
            isLoading: false
          });
        } else {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.getSummaryResult();
        }
      });
  };

  SaveProspectAction = () => {
    this.onSaveProspect();

    this.onLoadDataFormVehicleInfo();
  };

  onLoadDataFormVehicleInfo = () => {
    let {
      VehicleBrand,
      VehicleYear,
      VehicleType,
      VehicleSeries,
      searchVehicle
    } = {
      ...this.state
    };
    this.getListBrand(true);
    this.getListUsage(true);
    this.getListRegion(true);
    // this.getListSearch(true);
    // this.getVehicleList(true);

    if (searchVehicle != "") {
      this.onOptionSearch(searchVehicle, true);
    }

    if (VehicleBrand != "") {
      this.onOptionVehBrandChange(VehicleBrand, true);
    }
    if (VehicleYear != "") {
      this.onOptionVehYearChange(VehicleYear, true);
    }
    if (VehicleType != "") {
      this.onOptionVehTypeChange(VehicleType, true);
    }
    if (VehicleSeries != "") {
      this.onOptionVehSeriesChange(VehicleSeries, true);
    }
  };

  onLoadDataFormCover = () => {
    if (
      Util.isNullOrEmpty(this.state.OrderNo) ||
      Util.isNullOrEmpty(this.state.CoverBasicCover) ||
      this.state.flagWilayah
    ) {
      this.basicPremiCalculation();
    } else {
      this.premiumCalculation();
    }
    this.setState({
      fromProspecttoVehicleCheckPhoneExist: false,
      flagPrevTabs: "Cover"
    });
    let { CoverProductCode } = { ...this.state };
    if (this.isValidSaveVehicle()) {
      this.onSaveVehicle();
    }
    this.getListProductType();
    // this.onOptionCovProductTypeChange(this.state.CoverProductType);
    this.setState({ CoverProductCode });
    this.getListBasicCover();
    this.setState({ CovSumInsured: this.state.VehicleSumInsured });
  };

  onChangeMVGodigLoadDataForm = () => {
    if (
      this.state.activetab == tabstitle[1] ||
      this.state.activetab == tabstitle[2]
    ) {
      this.onLoadDataFormVehicleInfo();
    }

    // if(this.state.activetab == tabstitle[2]){
    //   this.onLoadDataFormCover();
    // }
  };

  CheckPhoneExist = () => {
    this.setState({
      isLoading: true
    });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/CheckPhoneExist/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "Phone=" + this.state.Phone1 + "&CustId=" + this.state.CustId
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.code == 1 && this.state.fromProspecttoVehicleCheckPhoneExist) {
          this.setState({
            isLoading: false,
            showModalPhoneExist: true,
            message: jsn.message,
            fromProspecttoVehicleCheckPhoneExist: false
          });
        } else {
          this.setState({ isLoading: false });
          this.SaveProspectAction();
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (!(error + "").toLowerCase().includes("token")) {
          this.CheckPhoneExist();
        }
        this.setState({
          isLoading: false
        });
      });
  };

  onSaveProspect = () => {
    this.setState(
      {
        isLoading: true
      },
      () => {
        if (this.state.ChannelSource === "MGO") {
          this.state.FollowUpStatus = "9";
        }
      }
    );

    const { state, props } = { ...this };

    const ProspectCustomer = {
      CustID: state.CustId || "",
      Name: state.Name,
      Phone1: state.Phone1,
      Phone2: state.Phone2,
      Email1: state.Email1,
      Email2: state.Email2,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      DealerCode: state.DealerCode,
      SalesDealer: state.SalesDealer,
      BranchCode: props.userInfo.User.BranchCode,
      isCompany: state.isCompany
    };

    const FollowUp = {
      FollowUpNo: state.FollowUpNumber || "",
      LastSeqNo: "0",
      CustID: state.CustId || "",
      ProspectName: state.Name,
      Phone1: state.Phone1,
      Phone2: state.Phone2,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      FollowUpName: state.Name,
      NextFollowUpDate: state.NextFollowUpDate,
      LastFollowUpDate: state.LastFollowUpDate,
      FollowUpStatus: state.FollowUpStatus,
      FollowUpInfo: state.FollowUpInfo,
      Remark: state.Remark,
      BranchCode: state.BranchCode,
      PolicyId: state.PolicyId,
      PolicyNo: state.PolicyNo,
      TransactionNo: state.TransactionNo,
      SalesAdminID: state.SalesAdminID,
      SendDocDate: state.SendDocDate,
      IdentityCard: state.KTP,
      STNK: state.STNK,
      SPPAKB: state.SPPAKB,
      BSTB1: state.BSTB1,
      BUKTIBAYAR: state.BUKTIBAYAR
    };

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SaveProspect/`,
      "POST",
      HEADER_API,
      {
        State: "PROSPECT",
        GuidTempPenawaran: this.state.GuidTempPenawaran,
        isMvGodig: this.state.isMvGodig,
        isNonMvGodig: this.state.isNonMvGodig,
        ProspectCustomer: JSON.stringify(ProspectCustomer),
        FollowUp: JSON.stringify(FollowUp)
      }
    )
      .then(res => res.json())
      //before using datamapping
      .then(jsn => {
        if (jsn.status) {
          secureStorage.setItem("FollowUpSelected", JSON.stringify(jsn.FU));
          secureStorage.setItem(
            "ProspectCustomerSelected",
            JSON.stringify(jsn.PC)
          );

          this.setState({
            CustId: jsn.FU.CustID || jsn.CustId || this.state.CustID,
            FollowUpNumber:
              jsn.FU.FollowUpNo || jsn.FollowUpNo || this.state.FollowUpNumber,
            GuidTempPenawaran: jsn.GuidTempPenawaran,
            isLoading: false
          });

          this.setState({ showfloatingbutton: true });
        } else {
          this.onShowAlertModalInfo(jsn.message);
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (!(error + "").toLowerCase().includes("token")) {
          this.onSaveProspect();
        }
        this.setState({
          isLoading: false
        });
      });

    console.log("save");
  };

  onSaveVehicle = () => {
    this.setState({
      isLoading: true
    });

    const { state, props } = this;

    const OrderSimulation = {
      OrderNo: this.state.OrderNo,
      CustID: this.state.CustId || "",
      FollowUpNo: this.state.FollowUpNumber || "",
      QuotationNo: this.state.QuotationNo,
      TLOPeriod: this.state.CoverTLOPeriod,
      ComprePeriod: this.state.CoverComprePeriod,
      BranchCode: this.state.BranchCode,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      PhoneSales: this.state.PhoneSales,
      DealerCode: this.state.DealerCode,
      SalesDealer: this.state.SalesDealer,
      ProductTypeCode: this.state.VehicleProductTypeCode,
      ProductCode: this.state.CoverProductCode,
      Phone1: this.state.Phone1,
      Phone2: this.state.Phone2,
      Email1: this.state.Email1,
      Email2: "",
      SendStatus: this.state.SendStatus,
      SendDate: this.state.SendDate,
      InsuranceType: parseInt(this.state.CoverProductType),
      ApplyF: "true",
      SendF: "false",
      LastInterestNo: 0,
      LastCoverageNo: 0,
      PeriodFrom: this.state.vehicleperiodfrom,
      PeriodTo: this.state.vehicleperiodto
    };

    const OrderSimulationMV = {
      OrderNo: state.OrderNo,
      ObjectNo: "1",
      ProductTypeCode: state.VehicleProductTypeCode,
      VehicleCode: state.VehicleCode,
      BrandCode: state.VehicleBrand,
      ModelCode: state.VehicleType,
      Series: state.VehicleSeries,
      Type: state.VehicleTypeType,
      Sitting: state.VehicleSitting,
      Year: state.VehicleYear,
      CityCode: state.VehicleRegion,
      UsageCode: state.VehicleUsage,
      SumInsured: state.VehicleSumInsured,
      RegistrationNumber: state.VehicleRegistrationNumber,
      EngineNumber: state.VehicleEngineNumber,
      ChasisNumber: state.VehicleChasisNumber,
      IsNew: !state.VehicleUsedCar,
      searchVehicle: state.searchVehicle,
      ModelCode: state.vehicleModelCode
    };

    // console.log(OrderSimulation);

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SaveProspect/`,
      "POST",
      HEADER_API,
      {
        State: "VEHICLE",
        GuidTempPenawaran: this.state.GuidTempPenawaran,
        isMvGodig: this.state.isMvGodig,
        isNonMvGodig: this.state.isNonMvGodig,
        OrderSimulation: JSON.stringify(OrderSimulation),
        OrderSimulationMV: JSON.stringify(OrderSimulationMV)
      }
    )
      .then(res => res.json())
      //datamapping
      .then(jsn => {
        if (jsn.status) {
          this.setState({
            isLoading: false,
            OrderNo: jsn.OrderNo,
            selectedQuotation: jsn.OrderNo,
            GuidTempPenawaran: jsn.GuidTempPenawaran
          });
        } else {
          this.onShowAlertModalInfo(jsn.message);
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (!(error + "").toLowerCase().includes("token")) {
          this.onSaveVehicle();
        }
        this.setState({
          isLoading: false
        });
      });

    console.log("save");
  };

  onSaveCover = () => {
    this.setState({
      isLoading: true
    });

    const { state, props } = this;

    let MultiYearFTemp = state.MultiYearF;
    if (state.CoverComprePeriod > 1 || state.CoverTLOPeriod > 1) {
      this.setState({
        MultiYearF: true
      });
      MultiYearFTemp = true;
    }

    const OrderSimulation = {
      OrderNo: this.state.OrderNo,
      CustID: this.state.CustId || "",
      FollowUpNo: this.state.FollowUpNumber || "",
      QuotationNo: this.state.QuotationNo,
      MultiYearF: MultiYearFTemp,
      YearCoverage: this.state.CoverTLOPeriod + this.state.CoverComprePeriod,
      TLOPeriod: this.state.CoverTLOPeriod,
      ComprePeriod: this.state.CoverComprePeriod,
      BranchCode: this.state.BranchCode,
      SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
      PhoneSales: this.state.PhoneSales,
      DealerCode: this.state.DealerCode,
      SalesDealer: this.state.SalesDealer,
      ProductTypeCode: this.state.VehicleProductTypeCode,
      ProductCode: this.state.CoverProductCode,
      AdminFee: this.state.AdminFee,
      TotalPremium: this.state.TotalPremi,
      Phone1: this.state.Phone1,
      Phone2: this.state.Phone2,
      Email1: this.state.Email1,
      Email2: "",
      SendStatus: this.state.SendStatus,
      SendDate: this.state.SendDate,
      InsuranceType: parseInt(this.state.CoverProductType),
      ApplyF: "true",
      SendF: "false",
      LastInterestNo: 0,
      LastCoverageNo: 0,
      PeriodTo: Util.isNullOrEmpty(this.state.vehicleperiodto)
        ? null
        : Util.formatDate(this.state.vehicleperiodto),
      PeriodFrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
        ? null
        : Util.formatDate(this.state.vehicleperiodfrom)
    };

    const OrderSimulationMV = {
      OrderNo: state.OrderNo,
      ObjectNo: "1",
      ProductTypeCode: state.VehicleProductTypeCode,
      VehicleCode: state.VehicleCode,
      BrandCode: state.VehicleBrand,
      ModelCode: state.VehicleType,
      Series: state.VehicleSeries,
      Type: state.VehicleTypeType,
      Sitting: state.VehicleSitting,
      Year: state.VehicleYear,
      CityCode: state.VehicleRegion,
      UsageCode: state.VehicleUsage,
      SumInsured: state.CovSumInsured,
      RegistrationNumber: state.VehicleRegistrationNumber,
      EngineNumber: state.EngineNumber,
      ChasisNumber: state.ChasisNumber,
      IsNew: !state.VehicleUsedCar,
      AccessSI: state.ACCESSCOVER,
      searchVehicle: state.searchVehicle
    };

    // console.log(OrderSimulation);

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SaveProspect/`,
      "POST",
      HEADER_API,
      {
        State: "COVER",
        GuidTempPenawaran: this.state.GuidTempPenawaran,
        isMvGodig: this.state.isMvGodig,
        isNonMvGodig: this.state.isNonMvGodig,
        OrderSimulation: JSON.stringify(OrderSimulation),
        OrderSimulationMV: JSON.stringify(OrderSimulationMV),
        calculatedPremiItems: JSON.stringify(this.state.CalculatedPremiItems),
        PremiumModel: JSON.stringify(this.state.PremiumModel)
      }
    )
      .then(res => res.json())
      //datamaping
      .then(jsn => {
        if (jsn.status) {
          this.setState({
            isLoading: false,
            OrderNo: jsn.OrderNo,
            GuidTempPenawaran: jsn.GuidTempPenawaran
          });

          this.getSummaryResult();
        } else {
          this.onShowAlertModalInfo(jsn.message);
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (!(error + "").toLowerCase().includes("token")) {
          this.onSaveCover();
        }
        this.setState({
          isLoading: false
        });
      });

    console.log("save Cover");
  };

  isValidChecklistMvGodig = () => {
    let res = true;
    if (!this.state.isMvGodig && !this.state.isNonMvGodig) {
      // Not Checked both IsMvGodig or IsNonMvGodig
      this.setState({
        isErrIsMvGodig: true
      });
      res = false;
    }

    return res;
  };

  isRoleTelesales = () => {
    let res = false;

    let account = JSON.parse(ACCOUNTDATA);

    if (Util.stringEquals(account.UserInfo.User.Role, "TELESALES")) {
      res = true;
    }

    return res;
  };

  onClickTabs = activetab => {
    if (!navigator.onLine) {
      Util.showToast("Check your connection!", "ERROR");
      return;
    }
    this.setState({
      chItemTemp: ""
    });

    this.setState({
      isErrIsMvGodig: false
    });

    if (activetab != tabstitle[0]) {
      if (!this.isValidChecklistMvGodig()) {
        Util.showToast(
          "Please tick MV Go Digital or MV Non Go Digital!",
          "WARNING"
        );
        return;
      }
    }

    if (activetab != tabstitle[0]) {
      if (!this.handlePassSubmitByCondition()) {
        return;
      }
    }

    if (activetab != tabstitle[2]) {
      this.setState({ loadingvehicletocover: false });
    }

    this.setState({ activetab });

    if (activetab == tabstitle[0]) {
      // console.log("Prospect");
      this.setState({ fromProspecttoVehicleCheckPhoneExist: true });
      this.getListDealer();
    }

    if (activetab == tabstitle[1]) {
      this.CheckPhoneExist();
    }

    if (activetab == tabstitle[2]) {
      this.onLoadDataFormCover();
    }

    if (activetab == tabstitle[3]) {
      this.setState({
        isDisableIsMvGodig: true
      });
      this.setState({ fromProspecttoVehicleCheckPhoneExist: false });
      this.onSaveCover();
      this.state.flagWilayah = false;
    }
  };

  isNotValidSaveVehicle = () => {
    let res = false;

    if (!Util.isNullOrEmpty(this.state.VehicleRegistrationNumber)) {
      let patt = new RegExp("^[A-Za-z]{1,2}[0-9]{1,4}[A-Za-z]{0,3}$");

      if (!patt.test(this.state.VehicleRegistrationNumber)) {
        Util.showToast("Registration number not valid!", "WARNING");
        this.setState({
          isErrVehRegistrationNumber: true
        });
        res = true;
      }
    }

    return res;
  };

  onOptionDealerChange = DealerCode => {
    this.setState({ DealerCode });
    this.setState({ SalesDealer: null });

    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getSalesmanDealerName/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "dealercode=" + DealerCode
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.SalesmanCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datasalesman: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionDealerChange(DealerCode);
        }
      });
  };

  onOptionSalesmanChange = SalesDealer => {
    this.setState({ SalesDealer });
  };

  getVehicle = (search = "") => {
    this.abortController.abort();
    this.abortController = new AbortController();
    this.setState({ isLoading: true });

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetVehicle/`,
      "POST",
      HEADER_API,
      {
        moduleName: "QUOTATION",
        search: search,
        isMvGodig: this.state.isMvGodig
      },
      {
        signal: this.abortController.signal
      }
    )
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datavehicleall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.VehicleDescription}`,
          label: `${data.VehicleDescription}`
        }));
      })
      .then(datasearch => {
        this.setState({ datasearch, isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          // this.searchDataHitApi();
        }
      });
  };

  onOptionSearch = (searchVehicle, name) => {
    this.setState({ [name]: searchVehicle });
    console.log("masuk search");

    let data = [...this.state.datavehicleall].filter(
      data => data.VehicleDescription == searchVehicle
    );

    // console.log(data);
    this.setState({
      VehicleProductTypeCode: data[0].ProductTypeCode,
      VehicleBrand: data[0].BrandCode,
      VehicleCode: data[0].VehicleCode,
      VehicleType: data[0].Type,
      VehicleTypeType: data[0].Type,
      VehicleSeries: data[0].Series,
      VehicleYear: data[0].Year,
      VehicleSitting: data[0].Sitting,
      VehicleUsedCar: !data[0].IsNew,
      vehicleModelCode: data[0].ModelCode,
      PASSCOVER: parseInt(data[0].Sitting) - 1,
      isErrCovSumInsured: false
    });

    if (this.state.VehicleYear != Util.convertDate().getFullYear()) {
      this.state.VehicleUsedCar = true;
    }

    if (
      !(this.state.VehicleRegion === "") ||
      !(this.state.VehicleDescription == null)
    ) {
      this.getVehiclePrice(
        data[0].VehicleCode,
        data[0].Year,
        this.state.VehicleRegion
      );
    }
  };

  getVehicleList = (isLoadingBefore = false) => {
    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }

    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetVehicle/`,
      "POST",
      HEADER_API,
      {
        moduleName: "QUOTATION",
        search: this.state.searchVehicle,
        isMvGodig: this.state.isMvGodig
      },
      {
        signal: this.abortController.signal
      }
    )
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.VehicleDescription}`,
          label: `${data.VehicleDescription}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datasearch: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.getVehicleList(isLoadingBefore);
        }
      });
  };

  onOptionVehBrandChange = (VehicleBrand, isLoadingBefore = false) => {
    this.setState({ VehicleBrand });
    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }

    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleYear/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "brandCode=" + VehicleBrand
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.Year}`,
          label: `${data.Year}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicleyear: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehBrandChange(VehicleBrand, isLoadingBefore);
        }
      });
  };

  onOptionVehYearChange = (VehicleYear, isLoadingBefore) => {
    this.setState({ VehicleYear });
    // console.log("Year : "+VehicleYear);
    var now = Util.convertDate();
    // this.setState({
    //   VehicleUsedCar: false
    // });

    if (now.getFullYear() - parseInt(VehicleYear.trim()) > 1) {
      this.setState({ VehicleUsedCar: true });
    }

    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleType/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "brandCode=" + this.state.VehicleBrand + "&year=" + VehicleYear
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.ModelCode}`,
          label: `${data.Description}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicletype: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehYearChange(VehicleYear, isLoadingBefore);
        }
      });
  };

  onOptionVehTypeChange = (VehicleType, isLoadingBefore = false) => {
    this.setState({ VehicleType });

    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleSeries/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "brandCode=" +
        this.state.VehicleBrand +
        "&year=" +
        this.state.VehicleYear +
        "&modelCode=" +
        VehicleType
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          value: `${data.Series}`,
          label: `${data.Series}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datavehicleseries: datamapping
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionVehTypeChange(VehicleType, isLoadingBefore);
        }
      });
  };

  onOptionVehSeriesChange = (VehicleSeries, isLoadingBefore = false) => {
    this.setState({ VehicleSeries });

    if (isLoadingBefore == false) {
      this.setState({ isLoading: true });
    }
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getVehicleCode/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "brandCode=" +
        this.state.VehicleBrand +
        "&year=" +
        this.state.VehicleYear +
        "&modelCode=" +
        this.state.VehicleType +
        "&series=" +
        VehicleSeries
    })
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          VehicleCodetemp: datamapping.data[0].VehicleCode,
          VehicleCode: datamapping.data[0].VehicleCode,
          VehicleProductTypeCode: datamapping.data[0].ProductTypeCode,
          VehicleTypeType: datamapping.data[0].Type,
          VehicleSitting: datamapping.data[0].Sitting,
          VehicleSumInsured: datamapping.data[0].Price
        });
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }

        // console.log(this.state.VehicleCode);
      })
      .catch(error => {
        console.log("parsing failed", error);
        if (isLoadingBefore == false) {
          this.setState({ isLoading: false });
        }
        // if(!(error+"").toLowerCase().includes("token")){
        //   this.onOptionVehSeriesChange(VehicleSeries, isLoadingBefore);
        // }
      });
  };

  onOptionVehUsageChange = VehicleUsage => {
    this.setState({ VehicleUsage });
  };

  onOptionVehRegionChange = VehicleRegion => {
    this.setState({ VehicleRegion, flagWilayah: true });

    if (
      !(this.state.searchVehicle === "") ||
      this.state.searchVehicle != null
    ) {
      this.getVehiclePrice(
        this.state.VehicleCode,
        this.state.VehicleYear,
        VehicleRegion
      );
    }
  };

  isValidHitBasicPremiCalculation = () => {
    let res = false;

    if (
      !Util.isNullOrEmpty(this.state.CoverProductType) &&
      !Util.isNullOrEmpty(this.state.CoverProductCode) &&
      !Util.isNullOrEmpty(this.state.CoverBasicCover) &&
      !Util.isNullOrEmpty(this.state.CovSumInsured)
    ) {
      res = true;
    }

    return res;
  };

  onOptionCovProductTypeChange = CoverProductType => {
    this.setState({ CoverProductType }, () => {
      if (this.isValidHitBasicPremiCalculation()) {
        this.basicPremiCalculation();
      }
    });

    // let VehicleProductTypeCode = [...this.state.dataListProductType].filter( data => data.InsuranceType == CoverProductType);
    // VehicleProductTypeCode = VehicleProductTypeCode[0].ProductTypeCode;

    this.setState({ isLoading: true });
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getProductCode/`,
      "POST",
      HEADER_API,
      {
        insurancetype: CoverProductType,
        salesofficerid: this.state.SalesOfficerID,
        isMvGodig: this.state.isMvGodig,
        isNew: !this.state.VehicleUsedCar
      }
    )

      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datacoverproductcodeall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.ProductCode}`,
          label: `${data.Description}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            datacoverproductcode: datamapping,
            isLoading: false,
            CoverProductCode:
              datamapping.length == 1
                ? datamapping[0].value
                : Util.stringArrayElementEquals(
                    this.state.CoverProductCode,
                    Util.arrayObjectToSingleDimensionArray(datamapping, "value")
                  )
                ? this.state.CoverProductCode
                : ""
          },
          () => {
            this.checkRateCalculatePremi();
          }
        );
        // if (
        //   JSON.parse(ACCOUNTDATA).UserInfo.User.Channel ==
        //   "EXT"
        // ) {
        //   this.setState(
        //     {
        //       CoverProductCode: datamapping[0].value
        //     },
        //     () => {
        //       this.checkRateCalculatePremi();
        //     }
        //   );
        // // } else {
        //   this.checkRateCalculatePremi();
        // }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.onOptionCovProductTypeChange(CoverProductType);
        }
      });
  };

  onOptionCovProductCodeChange = CoverProductCode => {
    this.setState({ CoverProductCode }, () => {
      this.hitratecalculationsimulation = true;
      this.basicPremiCalculation();
      this.setNdays(CoverProductCode, this.state.datacoverproductcodeall);
    });
    // console.log(CoverProductCode);
  };

  onOptionCovBasicCoverChange = CoverBasicCover => {
    this.setState({ CoverBasicCover });
    var datatemp = this.state.datacoverbasiccover.filter(
      data => data.value == CoverBasicCover
    );

    Log.debugStr("TEST MASUK DATATEMP BASIC COVER");
    if (datatemp != null) {
      let year =
        parseInt(datatemp[0].ComprePeriod) + parseInt(datatemp[0].TLOPeriod);
      Log.debugStr("TEST MASUK SET YEAR BASIC COVER");
      this.setState(
        {
          CoverComprePeriod: parseInt(datatemp[0].ComprePeriod),
          CoverTLOPeriod: parseInt(datatemp[0].TLOPeriod),
          vehicleperiodfrom: Util.isNullOrEmpty(this.state.vehicleperiodfrom)
            ? Util.convertDate()
            : this.state.vehicleperiodfrom
        },
        () => {
          Log.debugStr("TEST MASUK SET COMPRE TLO BASIC COVER");
          this.state.vehicleperiodto = Util.convertDate(
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss")
          );

          // Added FAY fixing

          let vehicleperiodto = Util.convertDate(
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss")
          );
          vehicleperiodto.setFullYear(vehicleperiodto.getFullYear() + year);

          this.hitratecalculationsimulation = true;

          this.setState(
            {
              vehicleperiodto
            },
            () => {
              this.basicPremiCalculation();
            }
          );
          // Ended FAY
        }
      );
    }

    // console.log(datatemp);
  };

  onOptionTPLSIChange = TPLCoverageId => {
    this.setState({ TPLCoverageId }, () => {
      this.rateCalculateTPLPAPASSPADRV("TPL");
    });
    // console.log(TPLCoverageId);
  };

  checkTotalSumInsured = () => {
    var min = parseInt(
      this.state.tempCovSumInsured - (this.state.tempCovSumInsured * 3) / 100
    );
    var max = parseInt(
      this.state.tempCovSumInsured + (this.state.tempCovSumInsured * 3) / 100
    );
    Log.debugStr("var min: " + min);
    Log.debugStr("var max: " + max);
    Log.debugStr(this.state.CovSumInsured > max);
    Log.debugStr(this.state.CovSumInsured);
    Log.debugStr(this.state.tempCovSumInsured);
    if (this.state.CovSumInsured < min || this.state.CovSumInsured > max) {
      this.setState({
        isErrCovSumInsured: true
      });
      return false;
    } else {
      this.setState({
        isErrCovSumInsured: false
      });
    }

    return true;
  };

  checkRateCalculatePremi = () => {
    if (this.state.VehiclePrice === "" || this.state.VehiclePrice == null) {
      this.setState({
        tempCovSumInsured: this.state.CovSumInsured
      });
    }
    let min = parseInt(
      this.state.tempCovSumInsured - (this.state.tempCovSumInsured * 3) / 100
    );
    let max = parseInt(
      this.state.tempCovSumInsured + (this.state.tempCovSumInsured * 3) / 100
    );
    Log.debugStr("MIN SUM INSURED: " + min);
    Log.debugStr("MAX SUM INSURED: " + max);
    Log.debugStr("SUM INSURED: " + this.state.CovSumInsured);
    Log.debugStr("TEMP SUM INSURED: " + this.state.tempCovSumInsured);
    if (
      this.state.CovSumInsured < min ||
      (this.state.CovSumInsured > max && this.state.VehicleDetailsTemp != "")
    ) {
      this.setState({
        isErrCovSumInsured: true
      });
    } else {
      this.setState({
        isErrCovSumInsured: false
      });
    }

    if (
      this.state.CoverBasicCover != null &&
      this.state.CoverBasicCover != "" &&
      this.state.CoverProductCode != null &&
      this.state.CoverProductCode != "" &&
      this.state.CoverProductType != null &&
      this.state.CoverProductType != "" &&
      !this.state.isErrCovSumInsured
    ) {
      this.setState(
        {
          isErrCovBasicCover: false,
          isErrCovProductCode: false,
          isErrCovProductType: false,
          isErrorCover: false
        },
        () => {
          if (this.state.CovSumInsured != null) {
            this.basicPremiCalculation();
          }
        }
      );
      this.setState({
        IsACCESSChecked: 0,
        IsACCESSSIEnabled: 0,
        ACCESSCOVER: 0
      });
    } else {
      console.log("belum hit rate calculate");
    }

    this.basicPremiCalculation();

    return this.state.isErrCovSumInsured;
  };

  onChangeFunctionDate = (value, name) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        if (
          this.state.vehicleperiodfrom.getDate() >
          this.state.vehicleperiodto.getDate()
        ) {
          this.state.isErrPeriodTo = true;
        }

        if (name == "vehicleperiodto") {
          this.basicPremiCalculation();
        }

        if (name == "vehicleperiodfrom") {
          this.basicPremiCalculation();
        }

        // this.getOrderDetailCoverage();
      }
    );
  };

  getOrderDetailCoverage = () => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailCoverage/`, {
      method: "POST",

      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "calculatedPremi=" +
        JSON.stringify(this.state.CalculatedPremiItems) +
        "&AdminFee=" +
        this.state.AdminFee
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState(
          {
            dataorderdetailcoverageall: jsn.data
          },
          () => {
            this.setOrderDetailCoverage(jsn.data.dtlCoverage);
          }
        );
      })
      .then(datamapping => {
        this.setState({
          // datas: datamapping.data[0],
          datacoverage: datamapping.dtlCoverage,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "".toLowerCase().includes("token"))) {
        }
      });
  };

  onChangeFunction = event => {
    var nameEvent = event.target.name;
    let value =
      event.target[
        event.target.type === "checkbox"
          ? "checked"
          : event.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = event.target.name;
    let pattern = event.target.pattern;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        value = value;
      } else {
        return;
      }
    }

    this.setState(
      {
        [name]: value
      },
      () => {
        if (nameEvent == "CovSumInsured") {
          this.checkRateCalculatePremi();
        }
        if (
          nameEvent == "PADRVCOVER" ||
          nameEvent == "PAPASSICOVER" ||
          nameEvent == "PASSCOVER" ||
          nameEvent == "ACCESSCOVER"
        ) {
          this.rateCalculateTPLPAPASSPADRV(nameEvent);
        }

        if (nameEvent == "VehicleCodetemp") {
          console.log("masuk");
          this.onChangeVehicleCode();
        }

        if (name == "chItemSetPeriod") {
          if (value == true) {
            this.setState({
              vehicleperiodfrommodalperiod: this.state.vehicleperiodfrom,
              vehicleperiodtomodalperiod: this.state.vehicleperiodto
            });
          } else {
            this.setState({
              vehicleperiodfrommodalperiod: null,
              vehicleperiodtomodalperiod: null
            });
          }
        }

        if (nameEvent === "isMvGodig" || nameEvent === "isNonMvGodig") {
          this.handleChecklistIsMvGodig(nameEvent, () => {
            this.onChangeMVGodigLoadDataForm();
          });
        }
      }
    );
  };

  handleChecklistIsMvGodig = (nameEvent, callback = () => {}) => {
    let { isNonMvGodig, isMvGodig } = { ...this.state };

    if (nameEvent === "isMvGodig") {
      isNonMvGodig = false;
    }

    if (nameEvent === "isNonMvGodig") {
      isMvGodig = false;
    }

    this.setState(
      {
        isNonMvGodig,
        isMvGodig
      },
      () => {
        callback();
      }
    );
  };

  onChangeVehicleCode = () => {
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/ResetVehicleCode/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "vehicleCode=" + this.state.VehicleCodetemp
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          this.setState(
            {
              VehicleCode: jsn.data.VehicleCode,
              VehicleCodetemp: jsn.data.VehicleCode,
              VehicleBrand: jsn.data.BrandCode,
              VehicleProductTypeCode: jsn.data.ProductTypeCode,
              VehicleType: jsn.data.ModelCode,
              VehicleSeries: jsn.data.Series,
              VehicleTypeType: jsn.data.Type,
              VehicleSitting: jsn.data.Sitting,
              VehicleYear: jsn.data.Year,
              VehicleSumInsured: jsn.data.Price
            },
            () => {
              if (jsn.data.VehicleBrand != "") {
                this.onOptionVehBrandChange(this.state.VehicleBrand);
              }
              if (jsn.data.VehicleYear != "") {
                this.onOptionVehYearChange(this.state.VehicleYear);
              }
              if (jsn.data.VehicleType != "") {
                this.onOptionVehTypeChange(this.state.VehicleType);
              }
              if (jsn.data.VehicleSeries != "") {
                this.onOptionVehSeriesChange(this.state.VehicleSeries);
              }
            }
          );
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
      });
  };

  onChangeFunctionChecked = event => {
    let check = event.target.value == 1 ? 0 : 1;
    this.setState({
      [event.target.name]: check
    });
  };

  onChangeFunction2 = e => {
    // console.log(event.target.type);
    const value =
      e.target[
        e.target.type === "checkbox"
          ? "checked"
          : e.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = e.target.name;

    this.setState(
      {
        [name]: value
      },
      () => {
        if (name == "IsACCESSChecked") {
          if (value) {
            this.setState({
              IsACCESSSIEnabled: 1
            });
          } else {
            this.setState(
              {
                IsACCESSSIEnabled: 0,
                ACCESSCOVER: 0,
                ACCESSPremi: 0
              },
              () => {
                // let CalculatedPremiItems = [...this.state.CalculatedPremiItems];
                // CalculatedPremiItems = CalculatedPremiItems.filter(
                //   data => data.InterestID != "ACCESS"
                // );

                // this.setState({ CalculatedPremiItems });
                this.rateCalculationNonBasicUnCheckAccesories();
              }
            );
          }
        }
      }
    );
  };

  handleUploadImage = (name, value, rejected) => {
    if (rejected.length > 0) {
      Util.showToast(
        "Please upload file with extension jpg, jpeg or png!!",
        "warning"
      );
      return;
    }

    this.setState({ isLoading: true });

    if (value.length > 0) {
      // Option compress image thumbnail
      var options = {
        maxSizeMB: 0.01, // MB
        useWebWorker: true
      };

      /// Option compress image ori yang diupload
      var optionsori = {
        maxSizeMB: 0.2, // MB
        useWebWorker: false,
        maxWidthOrHeight: 640
      };

      // console.log("FILE ORI", value[0]);

      this.setState({ [`data${name}`]: value });

      var compressOri = false;
      var compressThumbnail = true;

      imageCompression(value[0], optionsori)
        .then(function(compressedFile) {
          return compressedFile; // write your own logic
        })
        .then(compressImage => {
          // console.log("ORI", compressImage);
          Log.debugGroup("COMPRESSED IMAGE " + name + " => ", compressImage);
          Log.debugGroup("Check State on COMPRESSED IMAGE => ", [
            ...this.state.dataBUKTIBAYAR
          ]);
          Log.debugGroup("CHECK STATE KTP on COMPRESSED IMAGE => ", [
            ...this.state.dataKTP
          ]);
          this.setDataFoto(name, "ORI", compressImage);
          compressOri = true;
        })
        .catch(function(error) {
          console.log(error.message);
        });

      var checkcompressed = setInterval(() => {
        // console.log("CHECK COMPRESS");

        if (compressOri && compressThumbnail) {
          /// HIT API UPLOAD IMAGE
          console.log("SUDAH COMPRESS DONG!!!");
          clearInterval(checkcompressed);

          // function to convert image to base64 and upload image
          this.getBase64UploadImage(name);
        }
      }, 100);
    }
  };

  getBase64UploadImage = name => {
    this.setState({ isLoading: true });
    var datafototemp = null;
    Log.debugStr("CHECK NAME => " + name);
    Log.debugGroup("CHECK STATE KTP => ", [...this.state.dataKTP]);
    if (name == "KTP") {
      datafototemp = [...this.state.dataKTP];
    } else if (name == "STNK") {
      datafototemp = [...this.state.dataSTNK];
    } else if (name == "SPPAKB") {
      datafototemp = [...this.state.dataSPPAKB];
    } else if (name == "BSTB1") {
      datafototemp = [...this.state.dataBSTB1];
    } else if (name == "BSTB2") {
      datafototemp = [...this.state.dataBSTB2];
    } else if (name == "BSTB3") {
      datafototemp = [...this.state.dataBSTB3];
    } else if (name == "BSTB4") {
      datafototemp = [...this.state.dataBSTB4];
    } else if (name == "CheckListSurvey1") {
      datafototemp = [...this.state.dataCheckListSurvey1];
    } else if (name == "CheckListSurvey2") {
      datafototemp = [...this.state.dataCheckListSurvey2];
    } else if (name == "CheckListSurvey3") {
      datafototemp = [...this.state.dataCheckListSurvey3];
    } else if (name == "CheckListSurvey4") {
      datafototemp = [...this.state.dataCheckListSurvey4];
    } else if (name == "BUKTIBAYAR") {
      Log.debugStr("In Name");
      datafototemp = [...this.state.dataBUKTIBAYAR];
    } else if (name == "PaymentReceipt2") {
      datafototemp = [...this.state.dataPaymentReceipt2];
    } else if (name == "PaymentReceipt3") {
      datafototemp = [...this.state.dataPaymentReceipt3];
    } else if (name == "PaymentReceipt4") {
      datafototemp = [...this.state.dataPaymentReceipt4];
    } else if (name == "PremiumCalculation1") {
      datafototemp = [...this.state.dataPremiumCalculation1];
    } else if (name == "PremiumCalculation2") {
      datafototemp = [...this.state.dataPremiumCalculation2];
    } else if (name == "PremiumCalculation3") {
      datafototemp = [...this.state.dataPremiumCalculation3];
    } else if (name == "PremiumCalculation4") {
      datafototemp = [...this.state.dataPremiumCalculation4];
    } else if (name == "FormA1") {
      datafototemp = [...this.state.dataFormA1];
    } else if (name == "FormA2") {
      datafototemp = [...this.state.dataFormA2];
    } else if (name == "FormA3") {
      datafototemp = [...this.state.dataFormA3];
    } else if (name == "FormA4") {
      datafototemp = [...this.state.dataFormA4];
    } else if (name == "FormB1") {
      datafototemp = [...this.state.dataFormB1];
    } else if (name == "FormB2") {
      datafototemp = [...this.state.dataFormB2];
    } else if (name == "FormB3") {
      datafototemp = [...this.state.dataFormB3];
    } else if (name == "FormB4") {
      datafototemp = [...this.state.dataFormB4];
    } else if (name == "FormC1") {
      datafototemp = [...this.state.dataFormC1];
    } else if (name == "FormC2") {
      datafototemp = [...this.state.dataFormC2];
    } else if (name == "FormC3") {
      datafototemp = [...this.state.dataFormC3];
    } else if (name == "FormC4") {
      datafototemp = [...this.state.dataFormC4];
    }

    Log.debugGroup("DATAFOTOTEMP: ", datafototemp[0]);
    Log.debugStr("NAME: " + name);
    Log.debugStr("DATAPAYMENTRECEIPT: " + this.state.dataBUKTIBAYAR);
    // console.log("name : ", name);

    // FLAG CHECK PROCESS CONVERT BASE64
    var flagBase64Ori = false;
    var flagBase64Thumb = true;

    // var to save base64 converted
    var database64ori = null;
    var database64thumb = null;

    var reader = new FileReader();
    reader.readAsDataURL(datafototemp[0]);
    reader.onload = function() {
      // console.log("base64 ori", reader.result);
      database64ori = reader.result;
      flagBase64Ori = true;
    };
    reader.onerror = function(error) {
      console.log("base64 ori Error: ", error);
    };

    var checkconvertbase64 = setInterval(() => {
      // console.log("CHECK CONVERT BASE64");

      if (flagBase64Ori && flagBase64Thumb) {
        // console.log("SUDAH CONVERT BASE64 DONG!!!");
        clearInterval(checkconvertbase64);
        // console.log("var base64 ori : ", database64ori);
        // console.log("var base64 thumb : ", database64thumb);
        // console.log(database64ori.indexOf(";base64"));

        // console.log(database64ori.indexOf(","));
        var ext =
          "." +
          database64ori.substring(
            database64ori.indexOf("image/") + 6,
            database64ori.indexOf(";base64")
          );
        // console.log("ext", ext);
        // console.log("name", name);
        // console.log("FollowUpNumber", this.state.FollowUpNumber);
        // console.log("device id", new DeviceUUID().get());
        // console.log("username", this.state.SalesOfficerID);
        database64ori = database64ori.substring(
          database64ori.indexOf(",") + 1,
          database64ori.length
        );
        // database64thumb = database64thumb.substring(
        //   database64thumb.indexOf(",") + 1,
        //   database64thumb.length
        // );
        // console.log("dataori", database64ori);
        // console.log("datathumbs", database64thumb);

        // HIT API UPLOAD IMAGE
        ///!!!!!!!!!!!!!

        // console.log(this.base64toHEX(database64ori));

        let base64toHEXtemp = Util.base64toHEX(database64ori);

        this.uploadImage(
          base64toHEXtemp,
          "", //base64toHEXtemp,
          name,
          ext,
          this.state.SalesOfficerID,
          new DeviceUUID().get(),
          this.state.FollowUpNumber
        );
      }
    }, 100);
  };

  uploadImage = (
    dataimageori,
    dataimagethumb,
    imageType,
    ext,
    SalesOfficerID,
    DeviceID,
    FollowUpNo
  ) => {
    this.setState({ isLoading: true });

    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/UploadImage/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "ext=" +
        ext +
        "&ImageType=" +
        imageType +
        "&FollowUpNo=" +
        (FollowUpNo || "") +
        "&DeviceID=" +
        DeviceID +
        "&SalesOfficerID=" +
        SalesOfficerID +
        "&Data=" +
        dataimageori +
        "&ThumbnailData=" +
        dataimagethumb
    })
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn);
        this.setState({
          FollowUpNumber: jsn.FollowUpNo,
          [imageType]: jsn.data,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.uploadImage(dataimageori, dataimagethumb, imageType);
        }
      });
  };

  setDataFoto = (name, state, data) => {
    var datafototemp = [];
    if (name == "KTP") {
      datafototemp = [...this.state.dataKTP];
    } else if (name == "STNK") {
      datafototemp = [...this.state.dataSTNK];
    } else if (name == "SPPAKB") {
      datafototemp = [...this.state.dataSPPAKB];
    } else if (name == "BSTB1") {
      datafototemp = [...this.state.dataBSTB1];
    } else if (name == "BSTB2") {
      datafototemp = [...this.state.dataBSTB2];
    } else if (name == "BSTB3") {
      datafototemp = [...this.state.dataBSTB3];
    } else if (name == "BSTB4") {
      datafototemp = [...this.state.dataBSTB4];
    } else if (name == "CheckListSurvey1") {
      datafototemp = [...this.state.dataCheckListSurvey1];
    } else if (name == "CheckListSurvey2") {
      datafototemp = [...this.state.dataCheckListSurvey2];
    } else if (name == "CheckListSurvey3") {
      datafototemp = [...this.state.dataCheckListSurvey3];
    } else if (name == "CheckListSurvey4") {
      datafototemp = [...this.state.dataCheckListSurvey4];
    } else if (name == "BuktiBayar") {
      datafototemp = [...this.state.dataBUKTIBAYAR];
    } else if (name == "PaymentReceipt2") {
      datafototemp = [...this.state.dataPaymentReceipt2];
    } else if (name == "PaymentReceipt3") {
      datafototemp = [...this.state.dataPaymentReceipt3];
    } else if (name == "PaymentReceipt4") {
      datafototemp = [...this.state.dataPaymentReceipt4];
    } else if (name == "PremiumCalculation1") {
      datafototemp = [...this.state.dataPremiumCalculation1];
    } else if (name == "PremiumCalculation2") {
      datafototemp = [...this.state.dataPremiumCalculation2];
    } else if (name == "PremiumCalculation3") {
      datafototemp = [...this.state.dataPremiumCalculation3];
    } else if (name == "PremiumCalculation4") {
      datafototemp = [...this.state.dataPremiumCalculation4];
    } else if (name == "FormA1") {
      datafototemp = [...this.state.dataFormA1];
    } else if (name == "FormA2") {
      datafototemp = [...this.state.dataFormA2];
    } else if (name == "FormA3") {
      datafototemp = [...this.state.dataFormA3];
    } else if (name == "FormA4") {
      datafototemp = [...this.state.dataFormA4];
    } else if (name == "FormB1") {
      datafototemp = [...this.state.dataFormB1];
    } else if (name == "FormB2") {
      datafototemp = [...this.state.dataFormB2];
    } else if (name == "FormB3") {
      datafototemp = [...this.state.dataFormB3];
    } else if (name == "FormB4") {
      datafototemp = [...this.state.dataFormB4];
    } else if (name == "FormC1") {
      datafototemp = [...this.state.dataFormC1];
    } else if (name == "FormC2") {
      datafototemp = [...this.state.dataFormC2];
    } else if (name == "FormC3") {
      datafototemp = [...this.state.dataFormC3];
    } else if (name == "FormC4") {
      datafototemp = [...this.state.dataFormC4];
    }

    if (state == "THUMBNAIL") {
      datafototemp[1] = data;
    } else if (state == "ORI") {
      datafototemp[0] = data;
    }

    if (datafototemp.length > 0) {
      this.setState({ [`data${name}`]: datafototemp }, () => {
        // console.log("STATE MASUK", datafototemp);
      });
    }
  };

  handleDeleteImage = name => {
    this.setState({
      [name]: "",
      [`data${name}`]: []
    });
  };

  showQuotationHistory = FollowUpNo => {
    console.log("showQuotationHistory");
    this.setState({ isLoading: true });
    this.setState({ dataquotationhistory: [] });
    if (FollowUpNo != null) {
      this.setState({ isLoading: true });
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + this.state.FollowUpNumber
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status != false && jsn.data.length > 0) {
            return jsn.data.map(data => ({
              ...data,
              ProspectName: this.state.Name,
              FollowUpNo: this.state.FollowUpNumber
            }));
          } else {
            return [];
          }
        })
        .then(datamapping => {
          this.setState(
            {
              dataquotationhistory: datamapping,
              isLoading: false
            },
            () => {
              if (this.state.dataquotationhistory.length > 0) {
                this.setState({ showModal: true });
              } else {
                toast.dismiss();
                toast.warning(
                  "❗ There is no quotation for this prospect. Please make quotation first.",
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                  }
                );
              }
            }
          );
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          if (!(error + "").toLowerCase().includes("token")) {
            this.showQuotationHistory(FollowUpNo);
          }
        });
    } else {
      console.log("FollowUpNo tidak ada");
    }
  };

  addCopyQuotation = (FollowUpNo, OrderNo) => {
    console.log("masuk");
    this.setState({ isLoading: true });
    if (FollowUpNo != null) {
      this.setState({ isLoading: true });
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getQuotationHistory/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "followupno=" + this.state.FollowUpNumber
      })
        .then(res => res.json())
        .then(datamapping => {
          if (datamapping.data.length > 0) {
            var isSentToSA = false;
            var isApplyF = false;

            datamapping.data.forEach(data => {
              if (data.SendF == 1) {
                isSentToSA = true;
              }
              if (data.ApplyF) {
                isApplyF = true;
              }
              if (isSentToSA && isApplyF) {
                return;
              }
            });

            console.log("isSentToSA", isSentToSA);

            console.log("isApplyF", isApplyF);

            if (isSentToSA && this.state.FollowUpStatus == 6) {
              toast.dismiss();
              toast.warning(
                "❗ You must have at least one quotation to add quotation.",
                {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                }
              );
            } else if (!isApplyF) {
              toast.dismiss();
              toast.warning(
                "❗ You must complete your data of vehicle and cover to add more quotation.",
                {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                }
              );
            } else {
              this.setState({ showModalAddCopy: true });
            }
          } else {
            toast.dismiss();
            toast.warning(
              "❗ You must have at least one quotation to add quotation.",
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
              }
            );
          }

          this.setState({ isLoading: false });
        })
        .catch(error => {
          console.log("parsing failed", error);
          this.setState({
            isLoading: false
          });
          if (!(error + "").toLowerCase().includes("token")) {
            this.showQuotationHistory(FollowUpNo);
          }
        });
    } else {
      console.log("FollowUpNo tidak ada");
    }
  };

  rateCalculate = () => {
    this.loadingproduct = true;
    console.log("hit rateCalculate");
    var param = "";
    if (this.state.loadingvehicletocover == false) {
      param = "&OrderNo=" + this.state.OrderNo;
      this.setState({ isLoading: true });
    }

    this.hitratecalculationloading = true;

    var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/rateCalculation/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "ProductCode=" +
        this.state.CoverProductCode +
        "&TLOPeriod=" +
        this.state.CoverTLOPeriod +
        "&ComprePeriod=" +
        this.state.CoverComprePeriod +
        "&SumInsured=" +
        parseInt(this.state.CovSumInsured) +
        "&ProductTypeCode=" +
        this.state.VehicleProductTypeCode +
        "&BrandCode=" +
        this.state.VehicleBrand +
        "&VehicleCode=" +
        this.state.VehicleCode +
        "&ModelCode=" +
        this.state.VehicleType +
        "&Type=" +
        this.state.VehicleTypeType +
        "&Series=" +
        this.state.VehicleSeries +
        "&Year=" +
        this.state.VehicleYear +
        "&UsageCode=" +
        this.state.VehicleUsage +
        "&CityCode=" +
        this.state.VehicleRegion +
        "&IsNew=" +
        IsNewTemp +
        param
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          ////////////////LANJUTKAN COVER
          // console.log(jsn);

          if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
            this.onShowAlertModalInfo(jsn.Alert);
          } else {
            // if (jsn.Alert != "") {
            //   this.onShowAlertModalInfo(jsn.Alert);
            // }
            if (this.state.loadingvehicletocover == false) {
              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                data => data.InterestID == "PADRVR"
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                data => data.InterestID == "PAPASS"
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                data => data.InterestID == "ACCESS"
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                data => data.InterestID == "TPLPER"
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured,
                  PASSCOVER: PAPASSICOVER[0].CoverageID.trim().replace("C", "")
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0,
                  PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID
                });
              } else {
                this.setState({
                  TPLCoverageId: null
                });
              }
            }

            this.setState({
              loadingvehicletocover: true,
              ACCESSPremi: jsn.ACCESSPremi,
              AdminFee: jsn.AdminFee,
              Alert: jsn.Alert,
              CalculatedPremiItems: jsn.CalculatedPremiItems,
              ETVPremi: jsn.ETVPremi,
              FLDPremi: jsn.FLDPremi,
              // IsACCESSChecked: jsn.IsACCESSChecked,
              // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
              IsACCESSEnabled: jsn.IsACCESSEnabled,
              IsETVChecked: jsn.IsETVChecked,
              IsETVEnabled: jsn.IsETVEnabled,
              IsFLDChecked: jsn.IsFLDChecked,
              IsFLDEnabled: jsn.IsFLDEnabled,
              IsPADRVRChecked: jsn.IsPADRVRChecked,
              IsPADRVREnabled: jsn.IsPADRVREnabled,
              IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
              IsPAPASSChecked: jsn.IsPAPASSChecked,
              IsPAPASSEnabled: jsn.IsPAPASSEnabled,
              IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
              IsPASSEnabled: jsn.IsPASSEnabled,
              IsSRCCChecked: jsn.IsSRCCChecked,
              IsSRCCEnabled: jsn.IsSRCCEnabled,
              IsTPLChecked: jsn.IsTPLChecked,
              IsTPLEnabled: jsn.IsTPLEnabled,
              IsTPLSIEnabled: jsn.IsTPLSIEnabled,
              IsTSChecked: jsn.IsTSChecked,
              IsTSEnabled: jsn.IsTSEnabled,
              PADRVRPremi: jsn.PADRVRPremi,
              PAPASSPremi: jsn.PAPASSPremi,
              SRCCPremi: jsn.SRCCPremi,
              TPLPremi: jsn.TPLPremi,
              TSPremi: jsn.TSPremi,
              TotalPremi: jsn.TotalPremi,
              coveragePeriodItems: jsn.coveragePeriodItems,
              coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
            });

            if (jsn.IsTPLChecked == 1) {
              this.getTPLSI(this.state.CoverProductCode);
            }

            if (jsn.IsPADRVRChecked == 0) {
              this.setState({ PADRVCOVER: 0 });
            }

            if (jsn.IsPAPASSChecked == 0) {
              this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
            }

            if (jsn.IsTPLChecked == 0) {
              this.setState({ TPLCoverageId: null });
            }
          }
        } else {
          toast.dismiss();
          toast.warning("❗ Calculate premi error", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.loadingproduct = false;
        this.hitratecalculationloading = false;
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        this.loadingproduct = false;
        this.hitratecalculationloading = false;
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculate();
        }
      });
  };

  // hit calculate premi when check TPL / PA PASSENGER / PA DRIVER / ACCESSORY
  rateCalculateTPLPAPASSPADRV = jenisCheckList => {
    console.log("Masuk rateCalculateTPLPAPASSPADRV");
    var param = "";
    if (
      jenisCheckList != null &&
      jenisCheckList != "" &&
      this.loadingproduct == false
    ) {
      console.log("Hit rateCalculateTPLPAPASSPADRV");
      if (jenisCheckList == "TPL") {
        param = "&TPLCoverageID=" + this.state.TPLCoverageId;
      } else if (
        jenisCheckList == "PAPASSICOVER" ||
        jenisCheckList == "PASSCOVER"
      ) {
        param =
          "&PASS=" +
          parseInt(this.state.PASSCOVER + "") +
          "&PAPASSSI=" +
          parseInt(this.state.PAPASSICOVER + "");
      } else if (jenisCheckList == "PADRVCOVER") {
        param = "&PADRVRSI=" + parseInt(this.state.PADRVCOVER + "");
      } else if (jenisCheckList == "ACCESSCOVER") {
        param = "&AccessSI=" + parseInt(this.state.ACCESSCOVER + "");
      }

      // this.setState({ isLoading: true });

      this.hitratecalculationloading = true;

      var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;
      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/rateCalculation/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "CityCode=" +
          this.state.VehicleRegion +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&TLOPeriod=" +
          this.state.CoverTLOPeriod +
          "&ComprePeriod=" +
          this.state.CoverComprePeriod +
          "&SumInsured=" +
          parseInt(this.state.CovSumInsured) +
          "&ProductTypeCode=" +
          this.state.VehicleProductTypeCode +
          "&CalculatedPremi=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems) +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          param
      })
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            ////////////////LANJUTKAN COVER
            // console.log(jsn);
            if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
              toast.dismiss();
              toast.warning("❗ " + jsn.Alert, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
              });
            } else {
              if (jsn.Alert != "") {
                toast.dismiss();
                toast.warning("❗ " + jsn.Alert, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
                });
              }
              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                // IsACCESSChecked: jsn.IsACCESSChecked,
                // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                // IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                // IsETVEnabled: jsn.IxsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                // IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                // IsPADRVREnabled: jsn.IsPADRVREnabled,
                // IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                // IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                // IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                // IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                // IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                // IsTPLEnabled: jsn.IsTPLEnabled,
                // IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                // IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }
            }
          }
          this.hitratecalculationloading = false;
        })
        .catch(error => {
          this.hitratecalculationloading = false;
          console.log("parsing failed", error);
          if (!(error + "").toLowerCase().includes("token")) {
            this.rateCalculateTPLPAPASSPADRV(jenisCheckList);
          }
        });
    }
  };

  // hit senquotationemail
  sendQuotationEmail = (orderNo, Email) => {
    // this.setState({ isLoading: true });
    toast.dismiss();
    toast.info("Send email to " + Email, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
    this.setState({ showModal: false });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationEmail/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "Email=" + Email + "&OrderNo=" + orderNo
    })
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("Email sudah berhasil dikirimkan! ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          let FollowUpSelected = JSON.parse(
            secureStorage.getItem("FollowUpSelected")
          );
          FollowUpSelected = {
            ...FollowUpSelected,
            FollowUpInfo: 3,
            FollowUpStatus: 2
          };
          this.setState({
            FollowUpStatus: 2,
            FollowUpInfo: 3
          });
          secureStorage.setItem(
            "FollowUpSelected",
            JSON.stringify(FollowUpSelected)
          );
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send Email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send Email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, Email);
      });
  };

  // hit sendquotationsms
  sendQuotationSMS = (orderNo, phone) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationSMS/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "Phone=" + phone + "&OrderNo=" + orderNo
    })
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          this.setState({ showModal: false });
          toast.info("✅ SMS has been sent ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.dismiss();
          toast.warning("❗ Gagal Send SMS - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({
          isLoading: false,
          showModal: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Gagal Send SMS - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendQuotationEmail(orderNo, phone);
      });
  };

  setDialogPeriod = chType => {
    var chItemSetPeriod = false;
    var chItemSetPeriodEnable = 0;

    var chItemTemp = -1;

    this.setState({ chItemTemp }, () => {
      if (chType == 0) {
        chItemTemp = "SRCC";
        if (this.state.IsSRCCCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 1) {
        chItemTemp = "FLD";
        if (this.state.IsFLDCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 2) {
        chItemTemp = "ETV";
        if (this.state.IsETVCheckedEnable == 0) {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriod = true;
          chItemSetPeriodEnable = 1;
        }
      } else if (chType == 3) {
        chItemTemp = "TS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
      } else if (chType == 4) {
        chItemTemp = "TPLPER"; // TPLPER
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
        this.getTPLSI(this.state.CoverProductCode);
      } else if (chType == 5) {
        chItemTemp = "PADRVR";
        chItemSetPeriod = true;
        if (this.state.IsPAPASSChecked == 1) {
          chItemSetPeriodEnable = 0;
        } else {
          chItemSetPeriodEnable = 1;
        }

        this.getRetrieveextsi(chItemTemp);
      } else if (chType == 6) {
        chItemTemp = "PAPASS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;

        this.getRetrieveextsi(chItemTemp);

        // HANDLE PASS
        this.setState({
          PASSCOVER: this.state.VehicleSitting - 1
        });
      } else if (chType == 7) {
        chItemTemp = "ACCESS";
        chItemSetPeriod = true;
        chItemSetPeriodEnable = 1;
      } // ACCESS
      this.setState({ chItemTemp, chItemSetPeriod, chItemSetPeriodEnable });

      this.setState(
        {
          vehicleperiodfrommodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodfrom,
          vehicleperiodtomodalperiod:
            chItemSetPeriod == false ? null : this.state.vehicleperiodto
        },
        () => {
          this.setState({ showdialogperiod: true });

          if (chItemTemp == "TPLPER") {
            if (this.detectComprePeriod()) {
              let basicCoverage = [...this.state.databasiccoverall].filter(
                data => data.Id == this.state.CoverBasicCover
              )[0];

              this.setState(
                {
                  vehicleperiodtomodalperiod: Util.convertDate(
                    Util.formatDate(this.state.vehicleperiodfrom)
                  ).setFullYear(
                    Util.convertDate(
                      Util.formatDate(this.state.vehicleperiodfrom)
                    ).getFullYear() + basicCoverage.ComprePeriod
                  )
                },
                () => {
                  if (
                    basicCoverage.TLOPeriod == 0 &&
                    basicCoverage.ComprePeriod == 0
                  ) {
                    this.setState({
                      vehicleperiodtomodalperiod: this.state.vehicleperiodto
                    });
                  }
                }
              );
            }
          }

          if (chItemTemp == "TS") {
            this.setPeriodToPeriodFromTS(() => {
              this.setState({
                vehicleperiodtomodalperiod: this.state.periodtoTS,
                vehicleperiodfrommodalperiod: this.state.periodfromTS
              });
            });
          }
        }
      );
    });
  };

  detectComprePeriod = () => {
    let temp = false;
    if (this.state.IsTPLEnabled == 1) {
      temp = true;
    }

    return temp;
  };

  onOptionSelect2Change = (value, name) => {
    this.setState({ [name]: value }, () => {
      if (name == "TPLCoverageId") {
        // this.rateCalculateTPLPAPASSPADRV("TPL");
        let TPLSICOVER = [...this.state.dataTPLSIall].filter(
          data => data.CoverageId == value
        )[0].TSI;
        this.setState({
          TPLSICOVER
        });
      }

      if (
        Util.stringArrayContains(name, [
          "vehiclevehiclecode",
          "vehiclevehiclecodeyear",
          "CoverBasicCover",
          "vehicleproductcode",
          "vehicleregion",
          "vehicletotalsuminsured",
          "vehicleusage",
          "vehicleusedcar"
        ])
      ) {
        this.trigerRateCalculation();
      }
    });
    if (name == "vehiclevehiclecodeyear") {
      let data = [...this.state.datavehicleall].filter(
        data => data.VehicleCode + "-" + data.Year == value
      );

      // this.getListProductCode(
      //   data[0].ProductTypeCode,
      //   data[0].InsuranceType,
      //   this.state.vehicleusedcar ? 1 : 0
      // );

      console.log(data);
      this.setState(
        {
          vehicletotalsuminsured: data[0].Price,
          vehicletotalsuminsuredtemp: data[0].Price,
          vehicleBrandCode: data[0].BrandCode,
          // vehicleInsuranceType: data[0].InsuranceType,
          vehicleModelCode: data[0].ModelCode,
          vehicleProductTypeCode: data[0].ProductTypeCode,
          vehicleSeries: data[0].Series,
          vehicleSitting: Util.isNullOrEmpty(data[0].Sitting)
            ? 0
            : data[0].Sitting,
          vehicleType: data[0].Type,
          vehicleYear: data[0].Year,
          vehiclevehiclecode: data[0].VehicleCode
        },
        () => this.trigerRateCalculation()
      );

      if (data[0].Year != Util.convertDate().getFullYear()) {
        this.setState({
          vehicleusedcar: true
        });
      }
    }

    if (name == "vehicleproductcode") {
      this.setNdays(value, this.state.datavehicleproductcodeall);
      this.getSegmentCode(value);
      this.getEnableDisableSalesmanDealer(value);
    }

    if (name == "vehicleInsuranceType") {
      this.getListProductCode(
        this.state.ProductTypeCode,
        value,
        this.state.vehicleusedcar ? 1 : 0
      );
    }
  };

  onChangeFunctionReactNumber = (value, name, callback = () => {}) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        callback();
      }
    );
  };

  rateCalculationNonBasic = () => {
    console.log("masukkkkk");
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/rateCalculationNonBasic/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "chItem=" +
          this.state.chItemTemp +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&SumInsured=" +
          this.state.CovSumInsured +
          "&chPAPASS=" +
          this.state.IsPAPASSCheckedtemp +
          "&chPADRVR=" +
          this.state.IsPADRVRCheckedtemp +
          "&chTPL=" +
          this.state.IsTPLCheckedtemp +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&CityCode=" +
          this.state.VehicleRegion +
          "&isExtentedBundling=" +
          this.state.isExtentedBundlingtemp +
          "&chOne=" +
          this.state.chOne +
          "&chTwo=" +
          this.state.chTwo +
          "&chThree=" +
          this.state.chThree +
          "&ischOneEnabled=" +
          this.state.chOneEnabled +
          "&ischTwoEnabled=" +
          this.state.chTwoEnable +
          "&ischThreeEnabled=" +
          this.state.chThreeEnabled +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems) +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&PADRVRSI=" +
          this.state.PADRVCOVER +
          "&PAPASSSI=" +
          this.state.PAPASSICOVER +
          "&PASS=" +
          this.state.PASSCOVER +
          "&AccessSI=" +
          this.state.ACCESSCOVER
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn);
        if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
          toast.dismiss();
          toast.warning("❗ " + jsn.Alert, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          this.setState({ showdialogperiod: true });
        } else {
          if (jsn.Alert != "") {
            toast.dismiss();
            toast.warning("❗ " + jsn.Alert, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
            // this.onShowAlertModalInfo(jsn.Alert);
          }

          if (jsn.IsPADRVRChecked == 0) {
            this.setState({ PADRVCOVER: 0 });
          }

          if (jsn.IsPAPASSChecked == 0) {
            this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
          }

          if (jsn.IsTPLChecked == 0) {
            this.setState({ TPLCoverageId: null });
          }

          this.setState({
            ACCESSPremi: jsn.ACCESSPremi,
            // AdminFee: jsn.AdminFee,
            Alert: jsn.Alert,
            CalculatedPremiItems: jsn.CalculatedPremiItems,
            ETVPremi: jsn.ETVPremi,
            FLDPremi: jsn.FLDPremi,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            IsETVChecked: jsn.IsETVChecked,
            IsFLDChecked: jsn.IsFLDChecked,
            IsPADRVRChecked: jsn.IsPADRVRChecked,
            IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
            IsPAPASSChecked: jsn.IsPAPASSChecked,
            IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
            IsPASSEnabled: jsn.IsPAPASSSIEnabled,
            IsSRCCChecked: jsn.IsSRCCChecked,
            IsTPLChecked: jsn.IsTPLChecked,
            IsTPLSIEnabled: jsn.IsTPLSIEnabled,
            IsTSChecked: jsn.IsTSChecked,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            PADRVRPremi: jsn.PADRVRPremi,
            PAPASSPremi: jsn.PAPASSPremi,
            SRCCPremi: jsn.SRCCPremi,
            TPLPremi: jsn.TPLPremi,
            TSPremi: jsn.TSPremi,
            ACCESSPremi: jsn.ACCESSPremi,
            TotalPremi: jsn.TotalPremi,
            coveragePeriodItems: jsn.coveragePeriodItems,
            coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems,
            showdialogperiod: false
          });
          if (jsn.IsTPLChecked == 1) {
            this.getTPLSI(this.state.CoverProductCode);
            this.setState({ showdialogperiod: false });
          }
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculationNonBasic();
        }
      });
  };

  rateCalculationNonBasicUnCheckAccesories = () => {
    console.log("rateCalculationNonBasicUnCheckAccesories");
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION_2}/DataReact/rateCalculationNonBasic/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "chItem=access" +
          "&ProductCode=" +
          this.state.CoverProductCode +
          "&SumInsured=" +
          this.state.CovSumInsured +
          "&type=" +
          this.state.VehicleTypeType +
          "&UsageCode=" +
          this.state.VehicleUsage +
          "&CityCode=" +
          this.state.VehicleRegion +
          "&coveragePeriodItems=" +
          JSON.stringify(this.state.coveragePeriodItems) +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&coveragePeriodNonBasicItems=" +
          JSON.stringify(this.state.coveragePeriodNonBasicItems)
      }
    )
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn);
        if (jsn.Alert != "" && jsn.CalculatedPremiItems == undefined) {
          toast.dismiss();
          toast.warning("❗ " + jsn.Alert, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          if (jsn.Alert != "") {
            toast.dismiss();
            toast.warning("❗ " + jsn.Alert, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
            // this.onShowAlertModalInfo(jsn.Alert);
          }

          if (jsn.IsPADRVRChecked == 0) {
            this.setState({ PADRVCOVER: 0 });
          }

          if (jsn.IsPAPASSChecked == 0) {
            this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
          }

          if (jsn.IsTPLChecked == 0) {
            this.setState({ TPLCoverageId: null });
          }

          this.setState({
            ACCESSPremi: jsn.ACCESSPremi,
            // AdminFee: jsn.AdminFee,
            Alert: jsn.Alert,
            CalculatedPremiItems: jsn.CalculatedPremiItems,
            ETVPremi: jsn.ETVPremi,
            FLDPremi: jsn.FLDPremi,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            IsETVChecked: jsn.IsETVChecked,
            IsFLDChecked: jsn.IsFLDChecked,
            IsPADRVRChecked: jsn.IsPADRVRChecked,
            IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
            IsPAPASSChecked: jsn.IsPAPASSChecked,
            IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
            IsPASSEnabled: jsn.IsPAPASSSIEnabled,
            IsSRCCChecked: jsn.IsSRCCChecked,
            IsTPLChecked: jsn.IsTPLChecked,
            IsTPLSIEnabled: jsn.IsTPLSIEnabled,
            IsTSChecked: jsn.IsTSChecked,
            // IsACCESSChecked: jsn.IsACCESSChecked,
            // IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
            PADRVRPremi: jsn.PADRVRPremi,
            PAPASSPremi: jsn.PAPASSPremi,
            SRCCPremi: jsn.SRCCPremi,
            TPLPremi: jsn.TPLPremi,
            TSPremi: jsn.TSPremi,
            ACCESSPremi: jsn.ACCESSPremi,
            TotalPremi: jsn.TotalPremi,
            coveragePeriodItems: jsn.coveragePeriodItems,
            coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems,
            showdialogperiod: false
          });
          if (jsn.IsTPLChecked == 1) {
            this.getTPLSI(this.state.CoverProductCode);
          }
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Calculate premi error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        if (!(error + "").toLowerCase().includes("token")) {
          this.rateCalculationNonBasic();
        }
      });
  };

  getTPLSI = (ProductCode, callback = () => {}) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/getTPLSIList/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "ProductCode=" + ProductCode
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          dataTPLSIall: jsn.data
        });
        return jsn.data.map(data => ({
          value: `${data.CoverageId}`,
          label: `${this.formatMoney(data.TSI, 0)}`
        }));
      })
      .then(datamapping => {
        this.setState(
          {
            dataTPLSI: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        toast.dismiss();
        toast.warning("❗ Error - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      });
  };

  setPeriodToPeriodFromTS = (callback = () => {}) => {
    let CalculatedPremiItemsTemp = [
      ...this.state.CalculatedPremiItems
    ].filter(data => data.CoverageID.includes("SRC"));
    let periodto = null;
    let periodfrom = null;

    CalculatedPremiItemsTemp.forEach((data, index) => {
      if (index == 0) {
        periodfrom = Util.convertDate(data.PeriodFrom);
        periodto = Util.convertDate(data.PeriodTo);
      } else {
        if (periodfrom >= Util.convertDate(data.PeriodFrom)) {
          periodfrom = Util.convertDate(data.PeriodFrom);
        }
        if (periodto <= Util.convertDate(data.PeriodTo)) {
          periodto = Util.convertDate(data.PeriodTo);
        }
      }
    });

    this.setState(
      {
        periodfromTS: periodfrom,
        periodtoTS: periodto
      },
      () => {
        callback();
      }
    );
  };

  setNdays = (
    ProductCode = this.state.CoverProductCode,
    datacoverproductcodeall = this.state.datacoverproductcodeall,
    callback = () => {}
  ) => {
    if (datacoverproductcodeall.length > 0) {
      let data = [...datacoverproductcodeall].filter(
        data => data.ProductCode == ProductCode
      );
      data = data[0];
      console.log("setdays", data);
      this.setState({
        Ndays: data.Ndays
      });
    }
  };

  handleBundling = (CalculatedPremiItems = this.state.CalculatedPremiItems) => {
    this.setState(
      {
        IsSRCCCheckedEnable: 1,
        IsETVCheckedEnable: 1,
        IsFLDCheckedEnable: 1,
        IsTRSCheckedEnable: 1,
        IsPADRIVERCheckedEnable: 1,
        IsPAPASSCheckedEnable: 1
      },
      () => {
        for (let index = 0; index < CalculatedPremiItems.length; index++) {
          const element = CalculatedPremiItems[index];

          if (
            element.CoverageID.toLowerCase().includes("src") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsSRCCCheckedEnable: 0,
              IsSRCCCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("fld") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsFLDCheckedEnable: 0,
              IsFLDCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("etv") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsETVCheckedEnable: 0,
              IsETVCheckedEnableDays:
                Util.daysBetween(
                  Util.convertDate(element.PeriodFrom),
                  Util.convertDate(element.PeriodTo)
                ) - 1
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trs") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("trrtlo") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsTRSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("paddr1") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("padrvr") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPADRIVERCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("papass") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          } else if (
            element.CoverageID.toLowerCase().includes("pa24av") &&
            element.IsBundling == 1
          ) {
            this.setState({
              IsPAPASSCheckedEnable: 0
            });
          }
        }
      }
    );
  };

  basicPremiCalculation = () => {
    Log.debugGroup("hit basicPremiCalculation");

    if (Util.isNullOrEmpty(this.state.CoverProductCode)) {
      return;
    }

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.CoverBasicCover
    )[0];
    this.abortControllerBasicPremium.abort();
    this.abortControllerBasicPremium = new AbortController();

    if (!Util.isNullOrEmpty(BasicCover)) {
      var IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;

      fetch(
        `${API_URL + "" + API_VERSION_2}/DataReact/BasicPremiCalculation/`,
        {
          method: "POST",
          signal: this.abortControllerBasicPremium.signal,
          headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
          }),
          body:
            "ComprePeriod=" +
            BasicCover.ComprePeriod +
            "&TLOPeriod=" +
            BasicCover.TLOPeriod +
            "&PeriodFrom=" +
            Util.formatDate(this.state.vehicleperiodfrom, "yyyy-mm-dd") +
            "&PeriodTo=" +
            Util.formatDate(this.state.vehicleperiodto, "yyyy-mm-dd") +
            "&vtype=" +
            this.state.VehicleType +
            "&vcitycode=" +
            this.state.VehicleRegion +
            "&vusagecode=" +
            this.state.VehicleUsage +
            "&vyear=" +
            this.state.VehicleYear +
            "&vsitting=" +
            this.state.VehicleSitting +
            "&vProductCode=" +
            this.state.CoverProductCode +
            "&vTSInterest=" +
            this.state.CovSumInsured +
            "&vPrimarySI=" +
            this.state.tempCovSumInsured +
            "&vCoverageId=" +
            BasicCover.CoverageId +
            // "&vInterestId=" +
            // "ALLRIK" + /// Pasti ALLRIK ????
            "&pQuotationNo=" +
            "&Ndays=" +
            this.state.Ndays +
            "&vBrand=" +
            this.state.VehicleBrand +
            "&vModel=" +
            this.state.vehicleModelCode +
            "&isNew=" +
            IsNewTemp
        }
      )
        .then(res => res.json())
        .then(jsn => {
          if (jsn.status) {
            if (
              jsn.message != "" &&
              jsn.CalculatedPremiItems == undefined &&
              jsn.status != undefined
            ) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              // if (jsn.Alert != "") {
              //   this.onShowAlertModalInfo(jsn.Alert);
              // }

              // if (isLoading == true) {
              //   let PADRVCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PADRVR"
              //   );
              //   let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "PAPASS"
              //   );
              //   let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "ACCESS"
              //   );
              //   let TPLCoverageId = jsn.CalculatedPremiItems.filter(
              //     data => data.InterestID == "TPLPER"
              //   );

              //   if (PADRVCOVER.length > 0) {
              //     this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              //   } else {
              //     this.setState({ PADRVCOVER: 0 });
              //   }

              //   if (PAPASSICOVER.length > 0) {
              //     this.setState({
              //       PAPASSICOVER: PAPASSICOVER[0].SumInsured,
              //       PASSCOVER: PAPASSICOVER[0].CoverageID.trim().replace(
              //         "C",
              //         ""
              //       )
              //     });
              //   } else {
              //     this.setState({
              //       PAPASSICOVER: 0,
              //       PASSCOVER: 0
              //     });
              //   }

              //   if (ACCESSCOVER.length > 0) {
              //     this.setState({
              //       IsACCESSChecked: 1,
              //       IsACCESSSIEnabled: 1,
              //       ACCESSCOVER: ACCESSCOVER[0].SumInsured
              //     });
              //   } else {
              //     this.setState({
              //       IsACCESSChecked: 0,
              //       IsACCESSSIEnabled: 0,
              //       ACCESSCOVER: 0
              //     });
              //   }

              //   if (TPLCoverageId.length > 0) {
              //     this.setState({
              //       TPLCoverageId: TPLCoverageId[0].CoverageID
              //     });
              //   } else {
              //     this.setState({
              //       TPLCoverageId: null
              //     });
              //   }
              // }

              this.setState({
                PremiumModel: jsn
              });

              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                IsACCESSChecked: jsn.IsACCESSChecked,
                IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                IsPADRVREnabled: jsn.IsPADRVREnabled,
                IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                IsTPLEnabled: jsn.IsTPLEnabled,
                IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              this.setState({
                // GrossPremium: jsn.TotalPremi - jsn.AdminFee,
                AdminFee: jsn.AdminFee
                // NetPremi: jsn.TotalPremi
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.CoverProductCode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }

              this.setState({
                ACCESSCOVER: 0
              });
            }
          } else {
            // toast.dismiss();
            // toast.warning("❗ Calculate premi error", {
            //   position: "top-right",
            //   autoClose: 5000,
            //   hideProgressBar: false,
            //   closeOnClick: true,
            //   pauseOnHover: true,
            //   draggable: true
            // });
            this.onShowAlertModalInfo(jsn.message);
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.debugGroup("parsing failed", error);
          this.setState({
            isLoading: false
          });
          // toast.dismiss();
          // toast.warning("❗ Calculate premi error - " + error, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
          //  if(!(error+"").toLowerCase().includes("token")){
          //     this.basicPremiCalculation();
          //   }
        });
    } else {
      setTimeout(() => {
        Log.debugGroup("Hit timeout : basicPremiCalculation");
        this.basicPremiCalculation();
      }, 2000);
    }
  };
  getRetrieveextsi = (interestId, callback = () => {}) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/retrieveextsi/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "InterestId=" + interestId
    })
      .then(res => res.json())
      .then(jsn => {
        // Log.debugGroup(jsn);
        return jsn.data.map(data => ({
          value: `${data}`,
          label: `${Util.formatMoney(data, 0)}`
        }));
      })
      .then(datamapping => {
        this.data = true;
        this.setState(
          {
            dataextsi: datamapping,
            isLoading: false
          },
          () => {
            callback();
          }
        );
      })
      .catch(error => {
        // this.loadProductType = true;
        Log.debugGroup("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  premiumCalculation = () => {
    Log.debugGroup("hit extended premium simulation");

    let addtional = "";

    if (
      !Util.isNullOrEmpty(this.state.OrderNo) &&
      Util.isNullOrEmpty(this.state.chItemTemp)
    ) {
      addtional = "&OrderNo=" + this.state.OrderNo;
    }

    var BasicCover = [...this.state.databasiccoverall].filter(
      data => data.Id == this.state.CoverBasicCover
    )[0];
    Log.debugGroup("basiccover : ", BasicCover);

    if (!Util.isNullOrEmpty(BasicCover)) {
      let IsNewTemp = !this.state.VehicleUsedCar ? 1 : 0;

      let vTSI = this.state.CovSumInsured;
      let vsitting = this.state.VehicleSitting;
      let TPLCoverageId = "";
      let vType = this.state.VehicleType;
      let vPrimarySI = this.state.tempCovSumInsured;

      if (this.state.chItemTemp == "PAPASS") {
        // vsitting = this.state.PASSCOVER;
        vTSI = this.state.PAPASSICOVER;

        if (
          Util.isNullOrEmpty(this.state.PAPASSICOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Passenger is mandatory", "warning");
          return;
        }

        if (Util.isNullOrEmpty(this.state.PASSCOVER)) {
          Util.showToast("Pa Passenger Number is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "PADRVR") {
        vTSI = this.state.PADRVCOVER;

        if (
          Util.isNullOrEmpty(this.state.PADRVCOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Pa Driver is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "ACCESS") {
        vTSI = this.state.ACCESSCOVER;
        vPrimarySI = this.state.CovSumInsured;
        if (
          Util.isNullOrEmpty(this.state.ACCESSCOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI Accessories is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "TPLPER") {
        TPLCoverageId = this.state.TPLCoverageId;
        vTSI = this.state.TPLSICOVER;
        if (
          Util.isNullOrEmpty(this.state.TPLSICOVER) &&
          this.state.chItemSetPeriod
        ) {
          Util.showToast("TSI TPL is mandatory", "warning");
          return;
        }
      } else if (this.state.chItemTemp == "SRCC") {
        vType = "";
      } else if (this.state.chItemTemp == "ETV") {
        vType = "";
      } else if (this.state.chItemTemp == "FLD") {
        vType = "";
      }

      // if(Util.isNullOrEmpty(vTSI)){
      //   Util.showToast("Sum Insured is mandatory", "warning");
      //   return;
      // }

      this.setState({
        isLoading: true
      });

      fetch(`${API_URL + "" + API_VERSION_2}/DataReact/PremiumCalculation/`, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "ComprePeriod=" +
          BasicCover.ComprePeriod + /// CATATAN
          "&TLOPeriod=" +
          BasicCover.TLOPeriod + /// CATATAN
          "&PeriodFrom=" +
          (Util.isNullOrEmpty(this.state.vehicleperiodfrommodalperiod)
            ? null
            : Util.formatDate(
                this.state.vehicleperiodfrommodalperiod,
                "yyyy-mm-dd"
              )) +
          "&PeriodTo=" +
          (Util.isNullOrEmpty(this.state.vehicleperiodtomodalperiod)
            ? null
            : Util.formatDate(
                this.state.vehicleperiodtomodalperiod,
                "yyyy-mm-dd"
              )) +
          "&vtype=" +
          vType +
          "&vcitycode=" +
          this.state.VehicleRegion +
          "&vusagecode=" +
          this.state.VehicleUsage +
          "&vyear=" +
          this.state.VehicleYear +
          "&vsitting=" +
          vsitting +
          "&vProductCode=" +
          this.state.CoverProductCode +
          "&vTSInterest=" +
          vTSI +
          "&vPrimarySI=" +
          vPrimarySI +
          "&vCoverageId=" +
          BasicCover.CoverageId +
          "&pQuotationNo=" +
          "&Ndays=" +
          this.state.Ndays +
          "&vBrand=" +
          this.state.VehicleBrand +
          "&vModel=" +
          this.state.vehicleModelCode +
          "&isNew=" +
          IsNewTemp +
          "&chItem=" +
          this.state.chItemTemp +
          "&CalculatedPremiItems=" +
          JSON.stringify(this.state.CalculatedPremiItems) +
          "&TPLCoverageId=" +
          TPLCoverageId +
          addtional
      })
        .then(res => res.json())
        .then(jsn => {
          this.setState({ showdialogperiod: false });
          if (jsn.status) {
            if (jsn.message != "" && jsn.CalculatedPremiItems == undefined) {
              this.onShowAlertModalInfo(jsn.message);
            } else {
              // if (jsn.Alert != "") {
              //   this.onShowAlertModalInfo(jsn.Alert);
              // }
              // if (isLoading == true) {
              let PADRVCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PADRVR"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PADRVR",
                    "PADDR1"
                  ])
              );
              let PAPASSICOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "PAPASS"
                data =>
                  Util.stringArrayContains(data.InterestID, [
                    "PAPASS",
                    "PA24AV"
                  ])
              );
              let ACCESSCOVER = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "ACCESS"
                data => Util.stringArrayContains(data.InterestID, ["ACCESS"])
              );
              let TPLCoverageId = jsn.CalculatedPremiItems.filter(
                // data => data.InterestID == "TPLPER"
                data => Util.stringArrayContains(data.InterestID, ["TPLPER"])
              );

              if (PADRVCOVER.length > 0) {
                this.setState({ PADRVCOVER: PADRVCOVER[0].SumInsured });
              } else {
                this.setState({ PADRVCOVER: 0 });
              }

              if (PAPASSICOVER.length > 0) {
                this.setState({
                  PAPASSICOVER: PAPASSICOVER[0].SumInsured
                  // PASSCOVER: this.state.vehicleSitting - 1
                });
              } else {
                this.setState({
                  PAPASSICOVER: 0
                  // PASSCOVER: 0
                });
              }

              if (ACCESSCOVER.length > 0) {
                this.setState({
                  IsACCESSChecked: 1,
                  IsACCESSSIEnabled: 1,
                  ACCESSCOVER: ACCESSCOVER[0].SumInsured
                });
              } else {
                this.setState({
                  IsACCESSChecked: 0,
                  IsACCESSSIEnabled: 0,
                  ACCESSCOVER: 0
                });
              }

              if (TPLCoverageId.length > 0) {
                this.setState({
                  TPLCoverageId: TPLCoverageId[0].CoverageID,
                  TPLSICOVER: TPLCoverageId[0].SumInsured
                });
              } else {
                this.setState({
                  TPLCoverageId: null,
                  TPLSICOVER: 0
                });
              }

              this.setState({
                PremiumModel: jsn
              });

              this.setState({
                ACCESSPremi: jsn.ACCESSPremi,
                AdminFee: jsn.AdminFee,
                Alert: jsn.Alert,
                CalculatedPremiItems: jsn.CalculatedPremiItems,
                ETVPremi: jsn.ETVPremi,
                FLDPremi: jsn.FLDPremi,
                IsACCESSChecked: jsn.IsACCESSChecked,
                IsACCESSSIEnabled: jsn.IsACCESSSIEnabled,
                IsACCESSEnabled: jsn.IsACCESSEnabled,
                IsETVChecked: jsn.IsETVChecked,
                IsETVEnabled: jsn.IsETVEnabled,
                IsFLDChecked: jsn.IsFLDChecked,
                IsFLDEnabled: jsn.IsFLDEnabled,
                IsPADRVRChecked: jsn.IsPADRVRChecked,
                IsPADRVREnabled: jsn.IsPADRVREnabled,
                IsPADRVRSIEnabled: jsn.IsPADRVRSIEnabled,
                IsPAPASSChecked: jsn.IsPAPASSChecked,
                IsPAPASSEnabled: jsn.IsPAPASSEnabled,
                IsPAPASSSIEnabled: jsn.IsPAPASSSIEnabled,
                IsPASSEnabled: jsn.IsPASSEnabled,
                IsSRCCChecked: jsn.IsSRCCChecked,
                IsSRCCEnabled: jsn.IsSRCCEnabled,
                IsTPLChecked: jsn.IsTPLChecked,
                IsTPLEnabled: jsn.IsTPLEnabled,
                IsTPLSIEnabled: jsn.IsTPLSIEnabled,
                IsTSChecked: jsn.IsTSChecked,
                IsTSEnabled: jsn.IsTSEnabled,
                PADRVRPremi: jsn.PADRVRPremi,
                PAPASSPremi: jsn.PAPASSPremi,
                SRCCPremi: jsn.SRCCPremi,
                TPLPremi: jsn.TPLPremi,
                TSPremi: jsn.TSPremi,
                TotalPremi: jsn.TotalPremi,
                coveragePeriodItems: jsn.coveragePeriodItems,
                coveragePeriodNonBasicItems: jsn.coveragePeriodNonBasicItems
              });

              this.setState({
                Admin: jsn.AdminFee
              });

              if (jsn.CalculatedPremiItems.length > 0) {
                this.handleBundling(jsn.CalculatedPremiItems);
              }

              if (jsn.IsTPLChecked == 1) {
                this.getTPLSI(this.state.CoverProductCode);
              }

              if (jsn.IsPADRVRChecked == 0) {
                this.setState({ PADRVCOVER: 0 });
              }

              if (jsn.IsPAPASSChecked == 0) {
                // this.setState({ PASSCOVER: 0, PAPASSICOVER: 0 });
                this.setState({ PAPASSICOVER: 0 });
              }

              if (jsn.IsTPLChecked == 0) {
                this.setState({ TPLCoverageId: null });
              }
            }
          } else {
            toast.dismiss();
            toast.warning("❗ Calculate premi error", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          this.setState({
            isLoading: false
          });
        })
        .catch(error => {
          Log.debugGroup("parsing failed", error);
          this.setState({ showdialogperiod: false });
          this.setState({
            isLoading: false
          });
          toast.dismiss();
          toast.warning("❗ Calculate premi error - " + error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          if (!(error + "").toLowerCase().includes("token")) {
            // this.premiumCalculation();
          }
        });
    }
  };

  onClickQuotationHistory = (OrderNo, FollowUpNo, QuotationNo) => {
    // console.log(OrderNo);
    // console.log(FollowUpNo);
    this.setState({
      OrderNoQuotationEdit: OrderNo,
      FollowUpNoQuotationEdit: FollowUpNo
    });
    this.setState({ selectedQuotation: OrderNo });
  };

  editQuotationHistory = () => {
    this.setState({ showModal: false });
    // console.log("MASUKKKKK editQuotationHistory");
    // console.log("OrderNo", this.state.OrderNoQuotationEdit);
    // console.log("FollowUpNo", this.state.FollowUpNoQuotationEdit);
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/editQuotation/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "followupno=" +
        this.state.FollowUpNoQuotationEdit +
        "&orderNo=" +
        this.state.OrderNoQuotationEdit
    })
      .then(res => res.json())
      .then(datamapping => {
        // console.log(datamapping);
        this.setState({
          isLoading: false
        });
        secureStorage.setItem(
          "OrderSimulationSelected",
          JSON.stringify(datamapping.OrderSimulation)
        );
        secureStorage.setItem(
          "OrderSimulationMVSelected",
          JSON.stringify(datamapping.OrderSimulationMV)
        );
        secureStorage.setItem(
          "OrderSimulationInterestSelected",
          JSON.stringify(datamapping.OrderSimulationInterest)
        );
        secureStorage.setItem(
          "OrderSimulationCoverageSelected",
          JSON.stringify(datamapping.OrderSimulationCoverage)
        );

        var OS = JSON.parse(secureStorage.getItem("OrderSimulationSelected"));
        var OSMV = JSON.parse(
          secureStorage.getItem("OrderSimulationMVSelected")
        );
        var OSI = JSON.parse(
          secureStorage.getItem("OrderSimulationInterestSelected")
        );
        var OSC = JSON.parse(
          secureStorage.getItem("OrderSimulationCoverageSelected")
        );

        this.setModelInputProspect(null, null, OS, OSMV, OSI, OSC);

        //   activetab: tabstitle[0],
        // tabsDisabledState: [false, true, true, true],

        this.setState({ activetab: tabstitle[0] });

        let tabsDisabledState = [...this.state.tabsDisabledState];

        if (OSMV.length == 0) {
          tabsDisabledState[1] = false;
          tabsDisabledState[2] = true;
          tabsDisabledState[3] = true;
          this.setState({ tabsDisabledState });
        }
        if (OSC.length == 0) {
          tabsDisabledState[1] = false;
          tabsDisabledState[2] = false;
          tabsDisabledState[3] = true;
          this.setState({ tabsDisabledState });
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
      });
  };

  clickBtnAddCopyQuotation = clickState => {
    this.setState({ showModalAddCopy: false });
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/DataReact/addCopyQuotation/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "followUpNo=" +
        this.state.FollowUpNumber +
        "&orderNo=" +
        this.state.OrderNo +
        "&state=" +
        clickState
    })
      .then(res => res.json())
      .then(datamapping => {
        // console.log(datamapping);
        if (datamapping.status == 1) {
          this.setState({
            isLoading: false
          });
          secureStorage.setItem(
            "OrderSimulationSelected",
            JSON.stringify(datamapping.data.orderSimulation)
          );
          secureStorage.setItem(
            "OrderSimulationMVSelected",
            JSON.stringify(datamapping.data.orderSimulationMV)
          );
          secureStorage.setItem(
            "OrderSimulationInterestSelected",
            JSON.stringify(datamapping.data.orderSimulationInterest)
          );
          secureStorage.setItem(
            "OrderSimulationCoverageSelected",
            JSON.stringify(datamapping.data.orderSimulationCoverage)
          );

          var OS = JSON.parse(secureStorage.getItem("OrderSimulationSelected"));
          var OSMV = JSON.parse(
            secureStorage.getItem("OrderSimulationMVSelected")
          );
          var OSI = JSON.parse(
            secureStorage.getItem("OrderSimulationInterestSelected")
          );
          var OSC = JSON.parse(
            secureStorage.getItem("OrderSimulationCoverageSelected")
          );

          this.setModelInputProspect(null, null, OS, OSMV, OSI, OSC);

          //   activetab: tabstitle[0],
          // tabsDisabledState: [false, true, true, true],

          this.setState({ activetab: tabstitle[0] });

          let tabsDisabledState = [...this.state.tabsDisabledState];

          if (OSMV.length == 0) {
            tabsDisabledState[1] = false;
            tabsDisabledState[2] = true;
            tabsDisabledState[3] = true;
            this.setState({ tabsDisabledState });
          }
          if (OSC.length == 0) {
            tabsDisabledState[1] = false;
            tabsDisabledState[2] = false;
            tabsDisabledState[3] = true;
            this.setState({ tabsDisabledState });
          }
        }
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        // this.clickBtnAddCopyQuotation(clickState);
      });
  };

  formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  };

  onCloseModalSendEmail = () => {
    this.setState({ showModalSendEmail: false });
  };

  onCloseModalSendSMS = () => {
    this.setState({ showModalSendSMS: false });
  };

  onCloseModalAccessories = () => {
    this.props.history.goBack();
    this.setState({ showModalAccessories: false });
  };

  onCloseModalInfo = () => {
    this.setState({ showModalInfo: false });
    if (this.state.activetab == tabstitle[3]) {
      this.onClickTabs("Cover");
    }
  };

  onShowAlertModalInfo = MessageAlertCover => {
    this.setState({
      showModalInfo: true,
      MessageAlertCover
    });
  };

  handleScroll = e => {
    let element = e.target;
    // console.log(element.clientHeight);

    this.valueScrollTop = element.scrollTop;
    if (this.state.CustId === "") {
      this.setState({ showfloatingbutton: false });
    } else {
      if (this.valueScrollTop > 200) {
        this.setState({ showfloatingbutton: false });
      } else {
        this.setState({ showfloatingbutton: true });
      }
    }

    console.log(element.scrollTop);
  };

  handleShowPeriod = () => {
    let temp = false;
    if (
      !Util.isNullOrEmpty(this.state.CoverBasicCover) &&
      this.state.databasiccoverall.length > 0
    ) {
      let basicCoverage = [...this.state.databasiccoverall].filter(
        data => data.Id == this.state.CoverBasicCover
      )[0];
      if (!Util.isNullOrEmpty(basicCoverage)) {
        if (basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0) {
          temp = true;
        }
      }
    }

    return temp;
  };

  render() {
    const ModalSendSMS = () => (
      <Modal
        styles={{
          modal: {
            width: "80%"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.state.showModalSendSMS}
        onClose={() => this.setState({ showModalSendSMS: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send SMS</b>
            </h4>
            <div className="dropdown pull-right">
              <i
                className="fa fa-send fa-lg"
                onClick={() => {
                  if (/^[0-9-]{2,}[0-9]$/.test(this.sendtosms.value)) {
                    this.sendQuotationSMS(
                      this.state.selectedQuotation,
                      this.sendtosms.value
                    );
                    this.setState({ showModalSendSMS: false });
                  } else {
                    toast.dismiss();
                    toast.warning("❗ Phone number invalid", {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true
                    });
                  }
                }}
              />
            </div>
          </div>
          <div className="modal-body">
            <input
              type="number"
              ref={input => (this.sendtosms = input)}
              placeholder="enter phone number"
              className="form-control"
            />
          </div>
          <div className="modal-footer" />
        </div>
      </Modal>
    );
    const ModalConfirmPhoneExist = () => (
      <Modal
        styles={{
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModalPhoneExist}
        onClose={() => this.setState({ showModalPhoneExist: false })}
        showCloseIcon={false}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Confirmation</b>
            </h4>
          </div>
          <div className="modal-body">{this.state.message}</div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    this.setState({ showModalPhoneExist: false });
                    this.onClickTabs("Prospect Info");
                  }}
                  className="btn btn-warning form-control"
                >
                  No
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    this.setState({ showModalPhoneExist: false });
                    this.SaveProspectAction();
                  }}
                  className="btn btn-info form-control"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
    const ModalConfirmSendEmailSMS = () => (
      <Modal
        styles={{
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModalMakeSure}
        onClose={() => this.setState({ showModalMakeSure: false })}
        showCloseIcon={false}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Confirmation</b>
            </h4>
          </div>
          <div className="modal-body">Are you sure to send?</div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.setState({ showModalMakeSure: false })}
                  className="btn btn-warning form-control"
                >
                  No
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    if (this.state.sendEmailOrSMS == "EMAIL") {
                      this.setState({
                        showModalSendEmail: true,
                        showModalMakeSure: false,
                        showModal: false
                      });
                    } else if (this.state.sendEmailOrSMS == "SMS") {
                      this.setState({
                        showModalSendSMS: true,
                        showModalMakeSure: false,
                        showModal: false
                      });
                    }
                  }}
                  className="btn btn-info form-control"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
    const ModalComponentQuotationHistory = (
      <OtosalesModalQuotationHistory
        state={this.state}
        onClose={() => this.setState({ showModal: false })}
        editQuotationHistory={() => this.editQuotationHistory()}
        onClickSendEmail={() =>
          this.setState({
            showModalMakeSure: true,
            sendEmailOrSMS: "EMAIL"
          })
        }
        onClickSendSMS={() => {
          this.setState({
            showModalMakeSure: true,
            sendEmailOrSMS: "SMS"
          });
        }}
        onClickQuotationHistory={this.onClickQuotationHistory}
      />
    );
    const ModalComponent = () => (
      <Modal
        styles={{
          modal: {
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModal}
        showCloseIcon={false}
        onClose={() => this.setState({ showModal: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <BlockUi
            tag="div"
            blocking={this.state.isLoading && this.state.showModal}
            loader={
              <Loader active type="ball-spin-fade-loader" color="#02a17c" />
            }
            message="Loading, please wait"
          >
            <div className="modal-header panel-heading">
              <h4 className="modal-title pull-left" id="exampleModalLabel">
                <b>Quotation History</b>
              </h4>
              <div className="dropdown pull-right">
                <button
                  className="btn btn-primary dropdown-toggle"
                  type="button"
                  style={{ borderColor: "#18516c", backgroundColor: "#18516c" }}
                  data-toggle="dropdown"
                >
                  <i
                    style={{
                      marginTop: "5px",
                      marginBottom: "5px",
                      marginLeft: "10px",
                      marginRight: "10px"
                    }}
                    className="fa fa-ellipsis-v fa-lg"
                  />
                </button>
                <ul
                  className="dropdown-menu container-shadow-box"
                  style={{ marginTop: "0px" }}
                >
                  <li>
                    <a
                      style={{ color: "#000", fontSize: "12pt" }}
                      onClick={this.editQuotationHistory}
                    >
                      Edit Quotation
                    </a>
                    <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                  </li>
                  <li>
                    <a
                      style={{ color: "#000", fontSize: "12pt" }}
                      onClick={() =>
                        this.setState({
                          showModalMakeSure: true,
                          sendEmailOrSMS: "EMAIL"
                        })
                      }
                    >
                      Send Email
                    </a>
                    <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                  </li>
                  <li>
                    <a
                      style={{ color: "#000", fontSize: "12pt" }}
                      onClick={() =>
                        this.setState({
                          showModalMakeSure: true,
                          sendEmailOrSMS: "SMS"
                        })
                      }
                    >
                      Send Messages
                    </a>
                    <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                  </li>
                </ul>
              </div>
            </div>
            <div className="modal-body">
              {/* {this.state.selectedQuotation != null && ( */}
              <OtosalesQuotationHistory
                onClickQuotationHistory={this.onClickQuotationHistory}
                selectedQuotation={this.state.selectedQuotation}
                dataitems={this.state.dataquotationhistory}
              />
              {/* )} */}
            </div>
            <div className="modal-footer">
              {/* <div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.setState({ showModal: false })}
                  className="btn btn-warning form-control"
                >
                  Cancel
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => console.log("Clicked")}
                  className="btn btn-info form-control"
                >
                  Approve
                </button>
              </div>
            </div> */}
            </div>
          </BlockUi>
        </div>
      </Modal>
    );

    const ModalAddCopyQuotation = () => (
      <Modal
        styles={{
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.state.showModalAddCopy}
        onClose={() => this.setState({ showModalAddCopy: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-18px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Add or Copy?</b>
            </h4>
          </div>
          <div className="modal-body">
            Do you want to add new quotation or copy from existing quotation?
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.clickBtnAddCopyQuotation("ADD")}
                  className="btn btn-warning form-control"
                >
                  NEW
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.clickBtnAddCopyQuotation("COPY")}
                  className="btn btn-info form-control"
                >
                  COPY
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );

    let stylingreload = {
      padding: "10px",
      overscrollBehaviorY: "contain"
    };

    return (
      <div onScroll={this.handleScroll}>
        <Header titles="Input Prospect">
          {/* <ul className="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-history fa-lg"
                onClick={() =>
                  this.showQuotationHistory(this.state.FollowUpNumber)
                }
              />
            </li>
            <li>
              <a
                className="fa fa-plus-square fa-lg"
                onClick={() =>
                  this.addCopyQuotation(
                    this.state.FollowUpNumber,
                    this.state.OrderNo
                  )
                }
              />
            </li>
          </ul> */}
        </Header>
        <div className="content-wrapper" style={stylingreload}>
          <ToastContainer />
          {this.state.showModalAccessories && (
            <OtosalesModalAccessories
              showModal={this.state.showModalAccessories}
              onCloseModal={this.onCloseModalAccessories}
              dataAccessories={this.state.dataAccessories}
            />
          )}
          <OtosalesModalInfo
            message={this.state.MessageAlertCover}
            onClose={this.onCloseModalInfo}
            showModalInfo={this.state.showModalInfo}
          />
          <ModalAddCopyQuotation />
          {/* <ModalComponent /> */}
          <OtosalesModalQuotationHistory
            state={this.state}
            onClose={() => this.setState({ showModal: false })}
            editQuotationHistory={() => this.editQuotationHistory()}
            onClickSendEmail={() =>
              this.setState({
                showModalMakeSure: true,
                sendEmailOrSMS: "EMAIL"
              })
            }
            onClickSendSMS={() => {
              this.setState({
                showModalMakeSure: true,
                sendEmailOrSMS: "SMS"
              });
            }}
            onClickQuotationHistory={this.onClickQuotationHistory}
          />
          {/* <ModalDialogPeriod /> */}
          <ModalConfirmSendEmailSMS />
          <ModalConfirmPhoneExist />
          {this.state.showModalSendSMS && (
            <OtosalesModalSendSMS
              sendQuotationSMS={this.sendQuotationSMS}
              onCloseModalSendSMS={this.onCloseModalSendSMS}
              state={this.state}
            />
          )}
          {this.state.showModalSendEmail && (
            <OtosalesModalSendEmail
              sendQuotationEmail={this.sendQuotationEmail}
              onCloseModalSendEmail={this.onCloseModalSendEmail}
              state={this.state}
            />
          )}
          <OtosalesModalSetPeriodPremi2
            state={this.state}
            rateCalculationNonBasic={() => this.premiumCalculation()}
            onChangeFunctionDate={this.onChangeFunctionDate}
            onChangeFunctionChecked={this.onChangeFunctionChecked}
            onChangeFunction={this.onChangeFunction}
            onOptionSelect2Change={this.onOptionSelect2Change}
            onChangeFunctionReactNumber={this.onChangeFunctionReactNumber}
            getTPLSI={this.getTPLSI}
            onClose={() => {
              this.setState({
                showdialogperiod: false
              });
            }}
          />

          <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            isLoading={this.state.isLoading}
          >
            <div className={"panel-body panel-body-list"}>
              <div
                className={
                  "row" + (this.state.isErrIsMvGodig ? " has-error" : "")
                }
              >
                <div className="col-xs-12 col-md-3">
                  <div>
                    <label>
                      <input
                        type="checkbox"
                        disabled={this.isDisableMVGodig()}
                        style={{ marginRight: "10px" }}
                        onChange={this.onChangeFunction}
                        checked={this.state.isMvGodig}
                        name="isMvGodig"
                      />
                      MV GO DIGITAL
                    </label>
                  </div>
                </div>
                <div className="col-xs-12 col-md-3">
                  <div>
                    <label>
                      <input
                        type="checkbox"
                        disabled={this.isDisableMVGodig()}
                        style={{ marginRight: "10px" }}
                        onChange={this.onChangeFunction}
                        checked={this.state.isNonMvGodig}
                        name="isNonMvGodig"
                      />
                      MV NON GO DIGITAL
                    </label>
                  </div>
                </div>
              </div>
              <div className="mask" />
            </div>
            <div className="panel-body panel-body-list" id="Datalist">
              <Tabs
                activetab={this.state.activetab}
                onClickTabs={this.onClickTabs}
              >
                <div
                  label={tabstitle[0]}
                  disabled={this.state.tabsDisabledState[0]}
                >
                  <form onSubmit={this.onSubmitProspect} autoComplete="off">
                    {!this.state.isMvGodig && (
                      <div>
                        <input
                          type="checkbox"
                          style={{ marginRight: "10px" }}
                          onChange={this.onChangeFunction}
                          checked={this.state.isCompany}
                          name="isCompany"
                        />
                        <label>COMPANY</label>
                      </div>
                    )}
                    <div
                      className={
                        this.state.isErrProsName
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT NAME * </label>
                      <div className="input-group">
                        <input
                          type="text"
                          maxLength="50"
                          style={{ marginBottom: "0px" }}
                          onChange={this.onChangeFunction}
                          pattern="^[a-zA-Z0-9 ,-.\/]+$"
                          value={this.state.Name}
                          name="Name"
                          className="form-control "
                          placeholder="Enter new prospect name"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrPhone1
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT PHONE NUMBER 1 *</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            this.setState({ Phone1: value.value });
                            if (this.state.Phone1.length > 13)
                              this.state.isErrPhone1 = true;
                            else if (this.state.Phone1.length < 9)
                              this.state.isErrPhone1 = true;
                            else this.state.isErrPhone1 = false;
                          }}
                          value={this.state.Phone1}
                          format="#### #### #### ####"
                          prefix={""}
                          isNumericString={true}
                        />
                        <div
                          className="input-group-addon "
                          style={{ border: "0px" }}
                          onClick={() => {
                            if (this.state.Phone1 != "") {
                              window.location.href = "tel:" + this.state.Phone1;
                            }
                          }}
                        >
                          <img src={Call} style={{ width: "20px" }} />
                        </div>
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrPhone2
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT PHONE NUMBER 2</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            this.setState({ Phone2: value.value });
                            if (this.state.Phone2.length > 13)
                              this.state.isErrPhone2 = true;
                            else if (this.state.Phone2.length < 9)
                              this.state.isErrPhone2 = true;
                            else this.state.isErrPhone2 = false;
                          }}
                          value={this.state.Phone2}
                          format="#### #### #### ####"
                          prefix={""}
                          isNumericString={true}
                        />
                        <div
                          className="input-group-addon "
                          style={{ border: "0px" }}
                          onClick={() => {
                            if (this.state.Phone2 != "") {
                              window.location.href = "tel:" + this.state.Phone2;
                            }
                          }}
                        >
                          <img src={Call} style={{ width: "20px" }} />
                        </div>
                      </div>
                    </div>
                    {window.location.pathname == "/tasklist-edit" &&
                      JSON.parse(secureStorage.getItem("FollowUpSelected")) &&
                      JSON.parse(secureStorage.getItem("FollowUpSelected"))
                        .FollowUpStatus == 9 && (
                        <div className="form-group">
                          <label>PREFERED CALLING TIME (PCT) </label>
                          <div className="input-group">
                            <input
                              type="text"
                              style={{ marginBottom: "0px" }}
                              value={
                                secureStorage.getItem(
                                  "followUpDateTimeSelected"
                                ) != "null"
                                  ? this.formatDate(
                                      Util.convertDate(
                                        secureStorage.getItem(
                                          "followUpDateTimeSelected"
                                        )
                                      )
                                    ) +
                                    " " +
                                    this.formatTimeFromDate(
                                      Util.convertDate(
                                        secureStorage.getItem(
                                          "followUpDateTimeSelected"
                                        )
                                      )
                                    )
                                  : " "
                              }
                              className="form-control "
                              placeholder="Enter new prospect name"
                            />
                          </div>
                        </div>
                      )}
                    <div
                      className={
                        this.state.isErrEmail
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PROSPECT EMAIL</label>
                      <div className="input-group">
                        <input
                          type="email"
                          style={{ marginBottom: "0px" }}
                          onChange={this.onChangeFunction}
                          value={this.state.Email1}
                          name="Email1"
                          className="form-control"
                          placeholder="Enter prospect email"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrDealer
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>DEALER NAME</label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrDealer}
                          value={this.state.DealerCode}
                          selectOption={this.state.DealerCode}
                          onOptionsChange={this.onOptionDealerChange}
                          options={this.state.datadealer}
                          placeholder="Select dealer name"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrSalesman
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>SALESMAN DEALER NAME </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrSalesman}
                          value={this.state.SalesDealer}
                          selectOption={this.state.SalesDealer}
                          onOptionsChange={this.onOptionSalesmanChange}
                          options={this.state.datasalesman}
                          placeholder="Select salesman dealer name"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>POLICY NUMBER</label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={this.onChangeFunction}
                          value={this.state.PolicyNo}
                          disabled={true}
                          name="PolicyNo"
                          className="form-control "
                          placeholder="Enter policy number"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <legend>
                        <b>ATTACHMENT</b>
                      </legend>
                    </div>
                    <div className="form-group">
                      <div className="col-xs-3 panel-body-list">
                        <label>KTP/KITAS/SIM</label>
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.state.dataKTP}
                            name="KTP"
                            handleDeleteImage={this.handleDeleteImage}
                            handleUploadImage={this.handleUploadImage}
                          />
                        </div>
                      </div>
                      <div
                        className="col-xs-3 text-center panel-body-list"
                        style={{ marginLeft: "25px" }}
                      >
                        <label>STNK</label>
                        <OtosalesDropzonev2
                          src={this.state.dataSTNK}
                          name="STNK"
                          handleDeleteImage={this.handleDeleteImage}
                          handleUploadImage={this.handleUploadImage}
                        />
                      </div>
                      <div
                        className="col-xs-3 text-center panel-body-list"
                        style={{ marginLeft: "25px" }}
                      >
                        <label>SPPAKB</label>
                        <OtosalesDropzonev2
                          src={this.state.dataSPPAKB}
                          name="SPPAKB"
                          handleDeleteImage={this.handleDeleteImage}
                          handleUploadImage={this.handleUploadImage}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-xs-12">
                        <label>BSTB</label>
                      </div>
                      <div className="col-xs-12 text-center">
                        <div className="col-xs-3 panel-body-list">
                          <OtosalesDropzonev2
                            src={this.state.dataBSTB1}
                            name="BSTB1"
                            handleDeleteImage={this.handleDeleteImage}
                            handleUploadImage={this.handleUploadImage}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                        <br />
                        <br />
                        <br />
                      </div>
                    </div>
                  </form>
                </div>

                <div
                  label={tabstitle[1]}
                  disabled={this.state.tabsDisabledState[1]}
                >
                  <form onSubmit={this.onSubmitVehicle} autoComplete="off">
                    <div
                      className={
                        this.state.isErrVehCode
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <input
                        type="checkbox"
                        style={{ marginRight: "10px" }}
                        onChange={this.onChangeFunction}
                        checked={this.state.VehicleUsedCar}
                        name="VehicleUsedCar"
                        disabled={
                          this.state.VehicleYear <
                          Util.convertDate().getFullYear()
                            ? true
                            : false
                        }
                      />
                      <label>USED CAR</label>
                    </div>
                    <div
                      className={
                        this.state.isErrSearch
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>VEHICLE DETAILS *</label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          minHeight={40}
                          name="searchVehicle"
                          onSearchFunction={this.getVehicle}
                          value={this.state.searchVehicle}
                          selectOption={this.state.searchVehicle}
                          onOptionsChange={this.onOptionSearch}
                          options={this.state.datasearch}
                          placeholder="Pilih Kendaraan"
                        />
                      </div>
                    </div>

                    <div
                      className={
                        this.state.isErrVehUsage
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>USAGE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrVehUsage}
                          value={this.state.VehicleUsage}
                          selectOption={this.state.VehicleUsage}
                          onOptionsChange={this.onOptionVehUsageChange}
                          options={this.state.datavehicleusage}
                          placeholder="Select usage"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrVehRegion
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>REGION * </label>
                      <div className="input-group">
                        <OtosalesSelectFullview
                          error={this.state.isErrVehRegion}
                          value={this.state.VehicleRegion}
                          selectOption={this.state.VehicleRegion}
                          onOptionsChange={this.onOptionVehRegionChange}
                          options={this.state.datavehicleregion}
                          placeholder="Select region"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        "form-group" +
                        (this.state.isErrVehRegistrationNumber
                          ? " has-error"
                          : "")
                      }
                    >
                      <label>REGISTRATION NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          pattern="^[a-zA-Z0-9]+$"
                          onChange={this.onChangeFunction}
                          value={Util.valueTrim(
                            this.state.VehicleRegistrationNumber
                          ).toUpperCase()}
                          name="VehicleRegistrationNumber"
                          className="form-control "
                          maxLength="9"
                          placeholder="Type registration number"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>CHASIS NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={this.onChangeFunction}
                          pattern="^[a-zA-Z0-9]+$"
                          value={Util.valueTrim(
                            this.state.VehicleChasisNumber
                          ).toUpperCase()}
                          name="VehicleChasisNumber"
                          className="form-control "
                          placeholder="Type chasis number"
                          maxLength="30"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>ENGINE NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={this.onChangeFunction}
                          pattern="^[a-zA-Z0-9]+$"
                          value={Util.valueTrim(
                            this.state.VehicleEngineNumber
                          ).toUpperCase()}
                          name="VehicleEngineNumber"
                          className="form-control "
                          placeholder="Type engine number"
                          maxLength="30"
                        />
                      </div>
                    </div>

                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                      </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                  </form>
                </div>
                <div
                  label={tabstitle[2]}
                  disabled={this.state.tabsDisabledState[2]}
                >
                  <form onSubmit={this.onSubmitCover} autoComplete="off">
                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <div className="row separatorTop backgroundgrey">
                          <h5 className="col-xs-8 col-md-10">
                            <b>PRODUCT INFO</b>
                          </h5>
                        </div>
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrCovProductType
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PRODUCT TYPE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrCovProductType}
                          value={this.state.CoverProductType}
                          selectOption={this.state.CoverProductType}
                          onOptionsChange={this.onOptionCovProductTypeChange}
                          options={this.state.datacoverproducttype}
                          placeholder="Select product type"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        this.state.isErrCovProductCode
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>PRODUCT CODE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrCovProductCode}
                          value={this.state.CoverProductCode}
                          selectOption={this.state.CoverProductCode}
                          onOptionsChange={this.onOptionCovProductCodeChange}
                          options={this.state.datacoverproductcode}
                          placeholder="Select product code"
                        />
                      </div>
                    </div>
                    {/* )} */}

                    <div
                      className={
                        this.state.isErrCovBasicCover
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>BASIC COVER * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={this.state.isErrCovBasicCover}
                          value={this.state.CoverBasicCover}
                          selectOption={this.state.CoverBasicCover}
                          onOptionsChange={this.onOptionCovBasicCoverChange}
                          options={this.state.datacoverbasiccover}
                          placeholder="Select basic cover"
                        />
                      </div>
                    </div>
                    {this.handleShowPeriod() && (
                      <div className="row">
                        <div className="col-xs-6">
                          <span className="labelspanbold">Period From</span>
                          <OtosalesDatePicker
                            minDate={Util.convertDate()}
                            maxDate={Util.convertDate().setDate(
                              Util.convertDate().getDate() + 30
                            )}
                            className="form-control"
                            name="vehicleperiodfrom"
                            selected={this.state.vehicleperiodfrom}
                            dateFormat="dd/mm/yyyy"
                            onChange={this.onChangeFunctionDate}
                          />
                        </div>
                        <div className="col-xs-6">
                          <span className="labelspanbold">Period To</span>
                          <OtosalesDatePicker
                            maxDate={(this.state.vehicleperiodfrom != null
                              ? Util.convertDate(
                                  Util.formatDate(this.state.vehicleperiodfrom)
                                )
                              : Util.convertDate()
                            ).setDate(Util.convertDate().getFullYear() + 5)}
                            minDate={
                              this.state.vehicleperiodfrom != null
                                ? Util.convertDate(
                                    Util.formatDate(
                                      this.state.vehicleperiodfrom
                                    )
                                  )
                                : Util.convertDate()
                            }
                            className="form-control"
                            name="vehicleperiodto"
                            selected={this.state.vehicleperiodto}
                            dateFormat="dd/mm/yyyy"
                            onChange={this.onChangeFunctionDate}
                          />
                        </div>
                      </div>
                    )}

                    <div
                      className={
                        this.state.isErrCovSumInsured
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>SUM INSURED *</label>
                      <div className="input-group">
                        <NumberFormat
                          className="form-control"
                          onValueChange={value => {
                            this.setState(
                              { CovSumInsured: value.floatValue },
                              () => {
                                this.state.isErrCovSumInsured = this.checkRateCalculatePremi();
                              }
                            );
                          }}
                          value={this.state.CovSumInsured}
                          thousandSeparator={true}
                          prefix={""}
                        />
                      </div>
                    </div>
                    <div>
                      <div className="row backgroundgrey labelspanbold labelfontgrey">
                        <div
                          className="col-xs-12"
                          style={{ paddingTop: "5px", paddingBottom: "5px" }}
                          data-toggle="collapse"
                          data-target="#extendedcover"
                        >
                          <span className="pull-left">EXTENDED COVER</span>
                          <span className="pull-right collapsebutton" />
                        </div>
                      </div>
                      <div
                        id="extendedcover"
                        className="panel-body panel-padding0 collapse in"
                      >
                        <div>
                          {this.state.IsTPLEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsTPLChecked}
                                      name="IsTPLChecked"
                                      value={this.state.IsTPLChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(4);
                                      }}
                                    />
                                    TJH
                                  </label>
                                </div>
                                <NumberFormat
                                  allowNegative={false}
                                  disabled={true}
                                  className="form-control"
                                  value={this.state.TPLSICOVER}
                                  thousandSeparator={true}
                                  prefix={""}
                                />
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(this.state.TPLPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsSRCCEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsSRCCChecked}
                                      name="IsSRCCChecked"
                                      value={this.state.IsSRCCChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(0);
                                      }}
                                      disabled={
                                        this.state.IsSRCCCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    STRIKE, RIOT, COMMOTION (SRCC)
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(this.state.SRCCPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsFLDEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsFLDChecked}
                                      name="IsFLDChecked"
                                      value={this.state.IsFLDChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(1);
                                      }}
                                      disabled={
                                        this.state.IsFLDCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    FLOOD &amp; WINDSTROM
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(
                                    this.state.FLDPremi,
                                    0
                                  )}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsETVEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsETVChecked}
                                      name="IsETVChecked"
                                      value={this.state.IsETVChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(2);
                                      }}
                                      disabled={
                                        this.state.IsETVCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    EARTHQUAKE, TSUNAMI, VOLCANO ERUPTION
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(this.state.ETVPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsTSEnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      name="IsTSChecked"
                                      checked={this.state.IsTSChecked}
                                      value={this.state.IsTSChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(3);
                                      }}
                                      disabled={
                                        this.state.IsTRSCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    TERRORISM &amp; SABOTAGE
                                  </label>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(this.state.TSPremi, 0)}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsPADRVREnabled == 1 && (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsPADRVRChecked}
                                      name="IsPADRVRChecked"
                                      value={this.state.IsPADRVRChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(5);
                                      }}
                                      disabled={
                                        this.state.IsPADRIVERCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    PA DRIVER
                                  </label>
                                </div>
                                <NumberFormat
                                  allowNegative={false}
                                  disabled={true}
                                  className="form-control"
                                  value={this.state.PADRVCOVER}
                                  thousandSeparator={true}
                                  prefix={""}
                                />
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {" "}
                                  {Util.formatMoney(
                                    this.state.PADRVRPremi,
                                    0
                                  )}{" "}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsPAPASSEnabled == 0 ? (
                            ""
                          ) : (
                            <div className="row">
                              <div className="col-xs-8 col-md-10">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      type="checkbox"
                                      checked={this.state.IsPAPASSChecked}
                                      name="IsPAPASSChecked"
                                      value={this.state.IsPAPASSChecked}
                                      onChange={event => {
                                        this.setDialogPeriod(6);
                                      }}
                                      disabled={
                                        this.state.IsPAPASSCheckedEnable == 1
                                          ? false
                                          : true
                                      }
                                    />
                                    PA PASSENGER
                                  </label>
                                </div>
                                <div className="row">
                                  <div className="col-md-3 col-xs-6 panel-body-list">
                                    <div
                                      className="col-xs-8"
                                      style={{ paddingRight: "0px" }}
                                    >
                                      <input
                                        className="form-control"
                                        type="number"
                                        name="PASSCOVER"
                                        value={this.state.PASSCOVER}
                                        readOnly={true}
                                      />
                                    </div>
                                    <div className="col-xs-4">@</div>
                                  </div>
                                  <div
                                    className="col-md-9 col-xs-6 panel-body-list"
                                    style={{
                                      marginLeft: "-5px",
                                      paddingRight: "10px"
                                    }}
                                  >
                                    <NumberFormat
                                      disabled={true}
                                      className="form-control"
                                      value={this.state.PAPASSICOVER}
                                      thousandSeparator={true}
                                      prefix={""}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-xs-4 col-md-2 text-right">
                                <p style={{ marginTop: "10px" }}>
                                  {Util.formatMoney(this.state.PAPASSPremi, 0)}
                                </p>
                              </div>
                            </div>
                          )}
                          {this.state.IsACCESSEnabled == 1 &&
                            !this.state.isMvGodig && (
                              <div className="row">
                                <div className="col-xs-8 col-md-10">
                                  <div className="checkbox">
                                    <label>
                                      <input
                                        type="checkbox"
                                        checked={this.state.IsACCESSChecked}
                                        name="IsACCESSChecked"
                                        onChange={event => {
                                          this.setDialogPeriod(7);
                                        }}
                                      />
                                      ACCESSORY
                                    </label>
                                    <i
                                      className="fa fa-info-circle fa-2x"
                                      style={{
                                        marginLeft: "5px",
                                        opacity: "0.3"
                                      }}
                                      onClick={() => {
                                        this.ShowAccessoryInfo();
                                      }}
                                    />
                                  </div>
                                  <NumberFormat
                                    allowNegative={false}
                                    disabled={true}
                                    className="form-control"
                                    // onValueChange={value => {
                                    //   this.setState(
                                    //     { ACCESSCOVER: value.floatValue },
                                    //     () => {
                                    //       this.rateCalculateTPLPAPASSPADRV(
                                    //         "ACCESSCOVER"
                                    //       );
                                    //     }
                                    //   );
                                    // }}
                                    value={this.state.ACCESSCOVER}
                                    thousandSeparator={true}
                                    prefix={""}
                                  />
                                </div>
                                <div className="col-xs-4 col-md-2 text-right">
                                  <p style={{ marginTop: "10px" }}>
                                    {Util.formatMoney(
                                      this.state.ACCESSPremi,
                                      0
                                    )}
                                  </p>
                                </div>
                              </div>
                            )}
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                      </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                  </form>
                </div>
                <div
                  label={tabstitle[3]}
                  disabled={this.state.tabsDisabledState[3]}
                >
                  <div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img
                            src={HexProspectInfo}
                            style={{ width: "100%" }}
                          />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>
                            <strong>{this.state.SumName}</strong>
                          </span>
                          <p className="list-group-item-text">
                            {this.state.SumPhone1}
                          </p>
                          <p className="list-group-item-text">
                            {this.state.SumEmail1}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexCarInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{this.state.SumVehicle}</span>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexPolicyInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{this.state.SumCoverageType}</span>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img
                            src={HexPolicyDetails}
                            style={{ width: "100%" }}
                          />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>Total Premi </span>
                          <p className="">
                            <strong style={{ color: "#D59F00" }}>
                              {this.formatMoney(this.state.SumTotalPremium, 0)}
                            </strong>
                          </p>
                          <span>TSI CASCO</span>
                          <p className="">
                            {this.formatMoney(this.state.SumSumInsured, 0)}
                          </p>
                          <span>TSI Accesory </span>
                          <p className="">
                            {this.formatMoney(this.state.SumAccessSI, 0)}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexDealerInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{this.state.SumSalesmanDesc}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Tabs>
            </div>
          </OtosalesLoading>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageInputProspect);
