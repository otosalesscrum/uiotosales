import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { OtosalesModalAlert, Util } from "../otosalescomponents";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Modal from "react-responsive-modal";
import Header from "../components/Header";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";

class PageOrderApprovalDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showModalAlertRevise: false,
      remarksAlertRevise: "",
      datas: [],
      datasobject: [],
      isLoading: false,
      donehitOrderApprovalDetail: false,
      donehitOrderApprovalDetailObject: false,
      OrderID: "",
      Name: "",
      OrderNo: "",
      year: [],
      objects: [],
      interests: [],
      allObjects: [],
      remarksrevise: "",
      type: ""
    };
  }

  onOpenModal = type => {
    // console.log(type);
    this.setState({ showModal: true, type: type });
  };

  onCloseModal = () => {
    this.setState({ showModal: false });
  };

  onApprove = () => {
    this.ProcessOrderApproval("Approve", "");
  };

  onReject = () => {
    this.ProcessOrderApproval("Reject", "");
  };

  onSubmitRemarksRevise = event => {
    event.preventDefault();
    if (this.textInput.value == "") {
      toast.dismiss();
      toast.warning("❗ Please Input Remarks for SA", {
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }
    // console.log(this.textInput.value);
    this.setState({
      showModalAlertRevise: true,
      remarksAlertRevise: this.textInput.value+""
    });

    // this.ProcessOrderApproval("Revise", this.textInput.value);
  };

  ProcessOrderApproval = (submitOrder, remarks) => {
    this.setState({ isLoading: true });
    fetch(
      `${API_URL + "" + API_VERSION}/Synchronization/ProcessOrderApproval/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body:
          "OrderId=" +
          this.state.OrderID +
          "&OrderNo=" +
          this.state.OrderNo +
          "&userID=" +
          this.props.username +
          "&submitOrder=" +
          submitOrder +
          "&remarksOrder=" +
          remarks
      }
    )
      .then(res => res.json())
      .then(datamapping => {
        this.setState({
          isLoading: false
        });
        if (datamapping.Status == 1) {
          secureStorage.setItem(
            "orderapprovalnotif",
            "success-" + datamapping.Message
          );
          toast.dismiss();
          toast.success("✔️ " + datamapping.Message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          secureStorage.setItem(
            "orderapprovalnotif",
            "failed-" + datamapping.Message
          );
          toast.dismiss();
          toast.warning("❗ " + datamapping.Message, {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.props.history.push("/orderapproval");
        secureStorage.removeItem("NameOA");
        secureStorage.removeItem("OrderIdOA");
        secureStorage.removeItem("OrderNoOA");
        secureStorage.removeItem("IsSOOA");
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ Something error", {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        // this.props.history.push("/orderapproval");
        // secureStorage.removeItem("NameOA");
        // secureStorage.removeItem("OrderIdOA");
        // this.ProcessOrderApproval(submitOrder);
      });
  };

  formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  };

  closeAlertRevise = () => {
    this.setState({ showModalAlertRevise: false });
  };

  onSubmitAlertRevise = () => {
    this.setState({
      showModalAlertRevise: false,
      showModal: false
    });
    this.ProcessOrderApproval("Revise", this.textInput.value);
  };

  componentDidMount() {
    const OrderID = secureStorage.getItem("OrderIdOA");
    const Name = secureStorage.getItem("NameOA");
    const OrderNo = secureStorage.getItem("OrderNoOA");

    this.setState({ OrderID, Name });

    if (this.state.Name == undefined) {
      this.props.history.push("/");
    }
    if (OrderID == null) {
      this.props.history.push("/");
    }
    this.searchDataHitApi(OrderID);

    this.orderobjectApi(OrderNo);
  }

  searchDataHitApi = OrderID => {
    this.setState({
      datas: [],
      donehitOrderApprovalDetail: false,
      isLoading: true
    });
    fetch(`${API_URL + "" + API_VERSION}/Replication/OrderApprovalDetail/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "OrderID=" + OrderID
    })
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        this.setState({
          datas: datamapping,
          isLoading:
            !this.state.donehitOrderApprovalDetail &&
            !this.state.donehitOrderApprovalDetailObject,
          donehitOrderApprovalDetail: true,
          OrderNo: datamapping.OrderNo
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading:
            !this.state.donehitOrderApprovalDetail &&
            !this.state.donehitOrderApprovalDetailObject,
          donehitOrderApprovalDetail: true
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  };

  orderobjectApi = OrderNo => {
    this.setState({
      datasobject: [],
      isLoading: true,
      donehitOrderApprovalDetailObject: false
    });
    fetch(
      `${API_URL + "" + API_VERSION}/Replication/OrderApprovalDetailObject/`,
      {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Bearer " + JSON.parse(ACCOUNTDATA).Token
        }),
        body: "OrderNo=" + OrderNo
      }
    )
      .then(res => res.json())
      .then(jsn => jsn.data)
      .then(datamapping => {
        this.setState({
          datasobject: datamapping,
          isLoading:
            !this.state.donehitOrderApprovalDetail &&
            !this.state.donehitOrderApprovalDetailObject,
          donehitOrderApprovalDetailObject: true
        });

        // console.log(datamapping);
        this.loadOrderObjects(datamapping);
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading:
            !this.state.donehitOrderApprovalDetail &&
            !this.state.donehitOrderApprovalDetailObject,
          donehitOrderApprovalDetailObject: true
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.orderobjectApi(OrderNo);
        }
      });
  };

  loadOrderObjects = datamapping => {
    if (datamapping === undefined) {
      toast.dismiss();
      toast.warning("❗ Something error, Network Connection is Not Connected", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }

    if (datamapping === null) {
      toast.dismiss();
      toast.warning(
        "❗ Something error, The server is down or the network connection is not connected to the Internet.",
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        }
      );
    } else {
      datamapping.map(data => {
        var date = Util.convertDate(data.Begin_Date);
        if (!this.state.year.includes(date.getFullYear() + "")) {
          this.setState({
            year: [...this.state.year, date.getFullYear() + ""]
          });
        }

        if (!this.state.objects.includes(data.Object_No)) {
          this.setState({ objects: [...this.state.objects, data.Object_No] });
        }

        if (!this.state.interests.includes(data.Interest_Code)) {
          this.setState({
            interests: [...this.state.interests, data.Interest_Code]
          });
        }
      });

      this.state.year.map(year => {
        var objectYearly = {
          Year: "",
          Object: []
        };

        this.state.objects.map(object => {
          var objectVehicle = {
            ObjectName: "",
            PoliceNumber: "",
            Year: "",
            Interests: []
          };

          this.state.interests.map(interest => {
            var objectInterest = {
              InterestName: "",
              SumInsured: "",
              Coverages: []
            };

            datamapping.map(data => {
              var date = Util.convertDate(data.Begin_Date);
              if (
                year == date.getFullYear() + "" &&
                data.Object_No == object &&
                data.Interest_Code == interest
              ) {
                //////////////Order Approval 350
                let objectCoverage = {
                  Coverage: "",
                  Premium: "",
                  LoadingRate: ""
                };

                if (data.LoadingRate != 0) {
                  let objectCoveragetemp = {
                    Coverage: "Loading",
                    Premium: "",
                    LoadingRate: data.LoadingRate
                  };
                  objectCoverage = objectCoveragetemp;

                  let objectInteresttemp = {
                    InterestName: { ...objectInterest.InterestName },
                    SumInsured: { ...objectInterest.SumInsured },
                    Coverages: [...objectInterest.Coverages, objectCoverage]
                  };
                  objectInterest = objectInteresttemp;
                }

                objectCoverage = {
                  Coverage: data.Coverage_Description,
                  Premium: data.Cover_Premium,
                  LoadingRate: ""
                };

                let objectInteresttemp = {
                  InterestName: data.Interest_Description,
                  SumInsured: data.Sum_Insured,
                  Coverages: [...objectInterest.Coverages, objectCoverage]
                };
                objectInterest = objectInteresttemp;

                var objectVehicletemp = {
                  ObjectName: data.VehicleName,
                  PoliceNumber: data.PoliceNumber,
                  Year: data.Year,
                  Interests: [...objectVehicle.Interests]
                };

                objectVehicle = objectVehicletemp;

                var objectYearlytemp = {
                  Year: Util.convertDate(data.Begin_Date).getFullYear() + "",
                  Object: []
                };

                objectYearly = objectYearlytemp;
              }
            });

            if (objectInterest.InterestName != null) {
              var objectVehicleTemp = {
                ObjectName: objectVehicle.ObjectName,
                PoliceNumber: objectVehicle.PoliceNumber,
                Year: objectVehicle.Year,
                Interests: [...objectVehicle.Interests, objectInterest]
              };

              objectVehicle = objectVehicleTemp;
            }
          });

          var objectYearlytemp = {
            Year: objectYearly.Year,
            Object: [...objectYearly.Object, objectVehicle]
          };

          objectYearly = objectYearlytemp;
        });
        this.setState({ allObjects: [...this.state.allObjects, objectYearly] });
      });

      // console.log(this.state.year);
      // console.log(this.state.objects);
      // console.log(this.state.interests);
      // console.log(this.state.allObjects);
      // secureStorage.setItem("allObjects", JSON.stringify(this.state.allObjects));
    }
  };

  render() {
    console.log(this.state.allObjects);
    const ModalComponent = () => {
      if (this.state.type == "APPROVE") {
        return (
          <Modal
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <div
              className="modal-content panel panel-primary"
              style={{ margin: "-17px" }}
            >
              <div className="modal-header panel-heading">
                <h5 className="modal-title" id="exampleModalLabel">
                  Order Approval
                </h5>
              </div>
              <div className="modal-body">Do you want to approve?</div>
              <div className="modal-footer">
                <div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onCloseModal}
                      className="btn btn-warning form-control"
                    >
                      Cancel
                    </button>
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onApprove}
                      className="btn btn-info form-control"
                    >
                      Approve
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        );
      } else if (this.state.type == "REVISE") {
        return (
          <Modal
            styles={{
              modal: {
                width: "80%"
              }
            }}
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <form onSubmit={this.onSubmitRemarksRevise}>
              <div
                className="modal-content panel panel-primary"
                style={{ margin: "-22px" }}
              >
                <div className="modal-header panel-heading">
                  <h5 className="pull-left" id="exampleModalLabel">
                    <b>Send to SA</b>
                  </h5>

                  {/* <button className="btn btn-primary" type="button"> */}
                  <button
                    type="submit"
                    style={{
                      backgroundColor: "#18516c",
                      borderColor: "#18516c"
                    }}
                    className="btn btn-danger pull-right"
                  >
                    <i className="fa fa-send" />
                  </button>
                  {/* </button> */}
                </div>
                <div className="modal-body">
                  <p>Remarks</p>
                  <textarea
                    type="text"
                    style={{ marginBottom: "0px", resize: "none" }}
                    name="remarksrevise"
                    className="form-control"
                    placeholder="Remarks"
                    ref={input => (this.textInput = input)}
                  />
                </div>
                <div className="modal-footer">
                  {/* <div>
                    <div className="col-xs-6">
                      <button
                        onClick={this.onCloseModal}
                        className="btn btn-warning form-control"
                      >
                        Cancel
                      </button>
                    </div>
                    <div className="col-xs-6">
                      
                    </div>
                  </div> */}
                </div>
              </div>
            </form>
          </Modal>
        );
      } else if (this.state.type == "REJECT") {
        return (
          <Modal
            open={this.state.showModal}
            showCloseIcon={false}
            onClose={this.onCloseModal}
            center
          >
            <div
              className="modal-content panel panel-primary"
              style={{ margin: "-17px" }}
            >
              <div className="modal-header panel-heading">
                <h5 className="modal-title" id="exampleModalLabel">
                  Order Approval
                </h5>
              </div>
              <div className="modal-body">Do you want to reject?</div>
              <div className="modal-footer">
                <div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onCloseModal}
                      className="btn btn-warning form-control"
                    >
                      Cancel
                    </button>
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={this.onReject}
                      className="btn btn-danger form-control"
                    >
                      Reject
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        );
      }

      return null;
    };

    var detailsobject = null;

    if (this.state.allObjects != []) {
      // console.log(this.state.datasobject)
      detailsobject = this.state.allObjects.map((data, index) => {
        return (
          <fieldset key={index}>
            <div className="row separatorTop backgroundgrey">
              <h4 style={{ marginLeft: "15px", marginTop: "15px" }}>
                <b>TAHUN {index + 1}</b>
              </h4>
            </div>
            <div className="row separatorTop">
              <div className="col-xs-12">
                <p>
                  <b>
                    {data.Object[0].ObjectName +
                      " " +
                      data.Object[0].Year +
                      " " +
                      data.Object[0].PoliceNumber}
                  </b>
                </p>
              </div>
              {data.Object[0].Interests.map((interest, index) => {
                return (
                  <div
                    className="col-xs-12 panel-body-list separatorTop"
                    key={index + 908}
                  >
                    <p style={{ marginLeft: "15px", marginRight: "15px" }}>
                      <b>{interest.InterestName.toUpperCase()}</b>
                      <br />
                      {this.formatMoney(interest.SumInsured, 2)}
                    </p>
                    {interest.Coverages.map((coverage, index) => {
                      return (
                        <div
                          key={index + 19032}
                          className="col-xs-12 separatorTop panel-body-list"
                        >
                          <div
                            style={{ marginLeft: "15px", marginRight: "15px" }}
                          >
                            <div className="col-xs-6 panel-body-list">
                              <p className="pull-left">
                                {coverage.Coverage.toUpperCase()}
                              </p>
                            </div>
                            {
                              coverage.Coverage.toUpperCase().includes("LOADING") ? 
                              <div className="col-xs-6 panel-body-list">
                                <p className="pull-right">
                                  <b>{this.formatMoney(coverage.LoadingRate, 2)}%</b>
                                </p>
                              </div> : <div className="col-xs-6 panel-body-list">
                                <p className="pull-right">
                                  <b>{this.formatMoney(coverage.Premium, 2)}</b>
                                </p>
                              </div>
                            }
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </div>
          </fieldset>
        );
      });
    }

    const datas = this.state.datas;
    return (
      <div>
        <Header />
        <OtosalesModalAlert
          message="Do you want to revise?"
          onClose={this.closeAlertRevise}
          onSubmit={this.onSubmitAlertRevise}
          showModalInfo={this.state.showModalAlertRevise}
        />
        <ModalComponent />
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            blocking={this.state.isLoading}
            loader={
              <Loader active type="ball-spin-fade-loader" color="#02a17c" />
            }
            message="Loading, please wait"
          >
            <div
              className="panel panel-default"
              style={{ paddingBottom: "10px" }}
            >
              <div className="panel-heading">
                <h3 className="panel-title"> Order Approval Details </h3>
              </div>
              <div className="panel-body">
                <h3>
                  <strong>{this.state.Name}</strong>
                </h3>
                <div className="row">
                  <div className="col-xs-5">
                    <p>PAY TO</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>
                        {datas.PayToCode != null &&
                        datas.PayToName != "" &&
                        datas.PayToCode != "" &&
                        datas.PayToName != null
                          ? datas.PayToCode + " " + datas.PayToName
                          : ""}
                      </strong>
                    </p>
                  </div>
                </div>
                {
                  datas.Dealer != null && datas.Dealer != ""
                  ? 
                  <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>DEALER</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>
                        {datas.Dealer != null && datas.Dealer != ""
                          ? datas.Dealer
                          : ""}
                      </strong>
                    </p>
                  </div>
                </div>
                  : ""
                }
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>ACCOUNT INFO</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>
                        {datas.Account_Info_BankName +
                          " " +
                          (datas.Account_Info_AccountNo != null &&
                          datas.Account_Info_AccountNo != ""
                            ? datas.Account_Info_AccountNo
                            : "")}
                        <br />
                        {datas.Account_Info_AccountHolder != null &&
                        datas.Account_Info_AccountHolder != ""
                          ? "an " + datas.Account_Info_AccountHolder
                          : ""}
                      </strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>POLICY NUMBER</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{datas.Policy_No}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>PRODUCT</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{datas.Description}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>GROSS PREMIUM</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{this.formatMoney(datas.GrossPremium, 2)}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>DISCOUNT</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{datas.Discount}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>PREMIUM</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{this.formatMoney(datas.Premium, 2)}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>ADMIN FEE</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{this.formatMoney(datas.AdminFee, 2)}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>TOTAL PREMIUM</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>{this.formatMoney(datas.TotalPremium, 2)}</strong>
                    </p>
                  </div>
                </div>
                <div className="row separatorTop">
                  <div className="col-xs-5">
                    <p>TOTAL COMMISSION ({this.formatMoney(datas.CommissionPercentage,2)}%)</p>
                  </div>
                  <div className="col-xs-7">
                    <p className="text-right">
                      <strong>
                        {this.formatMoney(datas.TotalCommission, 2)}
                      </strong>
                    </p>
                  </div>
                </div>
                {detailsobject}
                <div className="row">
                  <div className="col-xs-6">
                    {secureStorage.getItem("IsSOOA") == 1 ? (
                      <button
                        onClick={() => this.onOpenModal("REVISE")}
                        className="btn btn-warning form-control"
                      >
                        Revise
                      </button>
                    ) : (
                      <button
                        onClick={() => this.onOpenModal("REJECT")}
                        className="btn btn-warning form-control"
                      >
                        Reject
                      </button>
                    )}
                  </div>
                  <div className="col-xs-6">
                    <button
                      onClick={() => this.onOpenModal("APPROVE")}
                      className="btn btn-info form-control"
                    >
                      Approve
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageOrderApprovalDetails);
