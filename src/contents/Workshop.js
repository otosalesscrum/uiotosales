import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { A2isListAdapter, A2isText } from "../components/a2is.js";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import Header from "../components/Header";
import { OtosalesLoading } from "../otosalescomponents";

class Workshop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSearchActive: false,
      search: "",
      isLoading: true,
      datas: [
        {
          id: "1",
          namelokasi: "Loading . . . . .",
          alamat: "Loading . . . . .",
          phone: "Loading . . . . .",
          fax: "Loading . . . . ."
        },
        {
          id: "2",
          namelokasi: "Loading . . . . .",
          alamat: "Loading . . . . .",
          phone: "Loading . . . . .",
          fax: "Loading . . . . ."
        }
      ]
    };
  }

  componentDidMount() {
    this.searchDataHitApi();
  }

  searchDataHitApi = () => {
    const { search } = this.state;
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getWorkshopList/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "search=" + search
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          id: `${data.WorkshopId}`,
          namelokasi: `${data.WorkshopName}`,
          alamat: `${data.Address}`,
          phone: `${data.Phone}`,
          fax: `${data.Fax}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datas: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  };

  onSearchData = event => {
    const search = event.target.value;
    this.setState({ search });
    this.searchDataHitApi();
  };

  render() {
    const { datas } = this.state;
    return (
      <div>
        <Header titles="Workshop List">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-search fa-lg"
                onClick={() =>
                  this.setState({ isSearchActive: !this.state.isSearchActive })
                }
              />
            </li>
          </ul>
        </Header>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <div className="panel-body panel-body-list" id="Datalist">
            {this.state.isSearchActive && (
              <div className="col-xs-12 col-md-4 pull-right panel-body-list">
                <A2isText
                  name="txtsample"
                  caption="Search"
                  onChangeText={this.onSearchData}
                  onKeyDown={(event) => {}}
                  minLength={0}
                  maxLength={20}
                  value={this.state.value}
                  readonly={this.state.readonly}
                  visible={this.state.visible}
                  disabled={this.state.disable}
                  required={this.state.required}
                  hint="Type address or name location"
                />
              </div>
            )}        
            <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            isLoading={this.state.isLoading}
            >
              <A2isListAdapter
                name="sampleList"
                rowscount={datas.length}
                multipleselect={true}
                dataitems={datas}
              />
            </OtosalesLoading>
          </div>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(Workshop);
