import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import {
  OtosalesListProspectLead,
  OtosalesModalInfo,
  OtosalesLoading
} from "../otosalescomponents";
import { API_URL, API_VERSION_2, ACCOUNTDATA, API_VERSION_0090URF2020 } from "../config";
import Header from "../components/Header";

class PageProspectLead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalAlert: false,
      messageAlert: "",
      titleAlert: null,
      isLoading: false,
      datas: [
        {
          PolicyID: "Loading .......",
          OrderNo: "Loading ....... ",
          customerName: "Loading ....... ",
          vehicleDescription: "Loading ....... ",
          followUpDateTime: "Loading ......."
        },
        {
          PolicyID: "Loading .......",
          OrderNo: "Loading ....... ",
          customerName: "Loading ....... ",
          vehicleDescription: "Loading ....... ",
          followUpDateTime: "Loading ......."
        }
      ]
    };
  }

  componentDidMount() {
    this.searchDataHitApi();
  }

  onCloseModal = () => {
    this.setState({ showModalAlert: false });
  };

  showModal = (messageAlert, titleAlert) => {
    this.setState({
      showModalAlert: true,
      messageAlert: messageAlert,
      titleAlert: titleAlert || null
    });
  };

  searchDataHitApi = () => {
    this.setState({ isLoading: true });
    // fetch(`${API_URL + "" + API_VERSION_2}/replication/penawaran`, {
    fetch(`${API_URL + "" + API_VERSION_0090URF2020}/datareact/penawaran`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body: "userID=" + JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.Data == null) {
          toast.dismiss();
          toast.error("❗ " + jsn.Message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          return (jsn.Data = []);
        }
        return jsn.Data;
      })
      .then(datamapping => {
        this.setState({
          datas: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  };

  onclickItem = policyID => {
    // console.log(policyID);
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/Synchronization/GetItMGOBid`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "PolicyID=" +
        policyID +
        "&OrderNo=" +
        (this.state.OrderNo || "") +
        "&SalesOfficerID=" +
        JSON.parse(ACCOUNTDATA).UserInfo.User
          .SalesOfficerID +
        "&UserPhone=" +
        JSON.parse(ACCOUNTDATA).UserInfo.User.Phone1 +
        "&BranchCode=" +
        JSON.parse(ACCOUNTDATA).UserInfo.User.BranchCode
    })
      .then(res => res.json())
      .then(jsn => {
        // console.log(jsn)
        if (jsn.Message == "'Get It' success") {
          toast.info("Prospect added to Task List", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
          
        } else if (jsn.Message != "") {
          // toast.dismiss();
          // toast.info(jsn.Message, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
          this.showModal(jsn.Message, null);
        }
        if (jsn.Status) {
          // this.props.history.push("/tasklist");
          // toast.info("Prospect added to Task List", {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
        }

        this.setState({
          isLoading: false
        });
        this.searchDataHitApi();
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  };

  render() {
    return (
      <div>
        <Header titles="Prospect Lead" />
        <OtosalesModalInfo
          message={this.state.messageAlert}
          onClose={this.onCloseModal}
          showModalInfo={this.state.showModalAlert}
          title={this.state.titleAlert}
        />
        <ToastContainer />
        <div className="content-wrapper" style={{ padding: "10px" }}>
        <OtosalesLoading
            className="col-xs-12 panel-body-list loadingfixed"
            isLoading={this.state.isLoading}
          >
            <div className="panel-body panel-body-list" id="Datalist">
              <OtosalesListProspectLead
                onClick={this.onclickItem}
                name="sampleList"
                rowscount={this.state.datas.length}
                dataitems={this.state.datas}
              />
            </div>
          </OtosalesLoading>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageProspectLead);
