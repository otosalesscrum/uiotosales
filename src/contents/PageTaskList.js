import React, { Component } from "react";
import Footer from "../components/Footer";
import { A2isText } from "../components/a2is";
import { withRouter } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { TaskListAdapter } from "../components/a2is.js";
import { OtosalesModalTaskListFilter, Util } from "../otosalescomponents";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";

class PageTaskList extends Component {
  constructor(props) {
    super(props);
    this.state =
      secureStorage.getItem("StateTaskList") != undefined
        ? JSON.parse(secureStorage.getItem("StateTaskList"))
        : {
            isSearchActive: false,
            indexOrderByItem: "Status",
            indexOrderByTypeItem: 1,
            chFollowUpMonthlyChecked: false,
            chFollowUpDateChecked: true,
            chNeedFUChecked: true,
            chPotentialChecked: true,
            chCallLaterChecked: true,
            chCollectDocChecked: true,
            chSentToSAChecked: true,
            chBackToAOChecked: true,
            chNotDealChecked: false,
            chPolicyCreatedChecked: false,
            SalesOfficerID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
            getFollowUpDate:
              Util.convertDate().getFullYear() +
              "-" +
              (Util.convertDate().getMonth() + 1) +
              "-" +
              Util.convertDate().getDate(),

            searchterm: "",
            isLoading: false,
            showModalFilter: false,
            datas: [
              {
                id: "1",
                name: "Loading Data . . . . .",
                lastfollow: "loading . . . .",
                nextfollow: "still loading . . . .",
                status: "1"
              },
              {
                id: "2",
                name: "Loading Data . . . . .",
                lastfollow: "loading . . . .",
                nextfollow: "still loading . . . .",
                status: "2"
              }
            ],
            datatasklist: [],
            historyPenawaranList: [],
            penawaranList: []
          };
  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });

    if (event.target.name == "searchterm") {
      this.searchDataHitApi(event.target.value);
    }
  };

  _handleKeyDown = (e) => {
    // console.log(e)
    // handle on click enter keyboard
    if (e.key === 'Enter') {
      console.log('do validate');
      // this.searchDataHitApi(this.state.searchterm);
    }
  }

  componentDidMount() {
    this.searchDataHitApi(this.state.searchterm);
  }

  searchDataHitApi = searchterm => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getTaskList/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body:
        "salesofficerid=" +
        this.state.SalesOfficerID +
        "&search=" +
        searchterm +
        "&indexOrderByItem=" +
        this.state.indexOrderByItem +
        "&indexOrderByTypeItem=" +
        this.state.indexOrderByTypeItem +
        "&chFollowUpMonthlyChecked=" +
        this.state.chFollowUpMonthlyChecked +
        "&chNeedFUChecked=" +
        this.state.chNeedFUChecked +
        "&chPotentialChecked=" +
        this.state.chPotentialChecked +
        "&chCallLaterChecked=" +
        this.state.chCallLaterChecked +
        "&chCollectDocChecked=" +
        this.state.chCollectDocChecked +
        "&chNotDealChecked=" +
        this.state.chNotDealChecked +
        "&chSentToSAChecked=" +
        this.state.chSentToSAChecked +
        "&chBackToAOChecked=" +
        this.state.chBackToAOChecked +
        "&chPolicyCreatedChecked=" +
        this.state.chPolicyCreatedChecked +
        "&chFollowUpDateChecked=" +
        this.state.chFollowUpDateChecked +
        "&getFollowUpDate=" +
        this.state.getFollowUpDate +" 23:59:59.725"
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({
          datatasklist: jsn.data,
          historyPenawaranList: jsn.historyPenawaranList,
          penawaranList: jsn.penawaranList
        });
        return jsn.data.map((data, i) => ({
          id: `${data.FollowUpNo}`,
          name: `${data.ProspectName}`,
          lastfollow: `${data.LastFollowUpDate}`,
          nextfollow: `${data.NextFollowUpDate}`,
          status: `${data.FollowUpStatus}`,
          CustID: `${data.CustID}`,
          datafollowup: data
        }));
      })
      .then(datamapping => {
        // console.log(datamapping);
        this.setState({
          datas: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi(searchterm);
        }
      });
  };

  onClickItem = FollowUpNo => {
    // console.log("Clicked "+FollowUpNo);
    const datataskclicked = this.state.datatasklist.filter((data, i) => {
      return data.FollowUpNo == FollowUpNo;
    });
    // console.log(datataskclicked[0]);
    secureStorage.setItem("StateTaskList", JSON.stringify(this.state));
    secureStorage.setItem("FUSelected", JSON.stringify(datataskclicked[0]));
    this.props.history.push("/tasklist-followupdetail");
  };

  onClickListItem = CustID => {
    // console.log("masuk", CustID);
    secureStorage.setItem("StateTaskList", JSON.stringify(this.state));
    this.searchProspectCustomer(CustID);

    // console.log(datafollowup);
    // secureStorage.setItem("FUTaskListEdit", JSON.stringify(datafollowup));
    // this.props.history.push("/tasklist-edit");
  };

  searchProspectCustomer = CustID => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/getProspectCustomerEdit/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      },
      body: "custid=" + CustID
    })
      .then(res => res.json())
      .then(jsn => {
        this.setState({ isLoading: false });
        if (jsn.status == 1) {
          secureStorage.setItem(
            "ProspectCustomerSelected",
            JSON.stringify(jsn.ProspectCustomer)
          );
          secureStorage.setItem(
            "FollowUpSelected",
            JSON.stringify(jsn.FollowUp)
          );
          secureStorage.setItem(
            "OrderSimulationSelected",
            JSON.stringify(jsn.OrderSimulation)
          );
          secureStorage.setItem(
            "OrderSimulationMVSelected",
            JSON.stringify(jsn.OrderSimulationMV)
          );
          secureStorage.setItem(
            "OrderSimulationInterestSelected",
            JSON.stringify(jsn.OrderSimulationInterest)
          );
          secureStorage.setItem(
            "OrderSimulationCoverageSelected",
            JSON.stringify(jsn.OrderSimulationCoverage)
          );
          secureStorage.setItem(
            "ImageDataSelected",
            JSON.stringify(jsn.imageData)
          );
          this.props.history.push("/tasklist-edit");
        } else {
          toast.dismiss();
          toast.error("❗ " + jsn.Message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
      })
      .catch(error => {
        toast.dismiss();
        toast.error("❗ " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
        //  this.searchProspectCustomer(CustID);
        }
      });
  };

  onCloseModal = () => {
    this.setState({ showModalFilter: false });
  };

  onSubmitFilter = filter => {
    const {
      chBackToAOChecked,
      chCallLaterChecked,
      chCollectDocChecked,
      chFollowUpDateChecked,
      chFollowUpMonthlyChecked,
      chNeedFUChecked,
      chNotDealChecked,
      chPolicyCreatedChecked,
      chPotentialChecked,
      chSentToSAChecked,
      itemModelsSelected,
      spOrderByTypeItem,
      lastFollowUpDate
    } = filter;

    this.setState(
      {
        showModalFilter: false,
        chBackToAOChecked,
        chCallLaterChecked,
        chCollectDocChecked,
        chFollowUpDateChecked,
        chFollowUpMonthlyChecked,
        chNeedFUChecked,
        chNotDealChecked,
        chPolicyCreatedChecked,
        chPotentialChecked,
        chSentToSAChecked,
        indexOrderByItem: itemModelsSelected,
        indexOrderByTypeItem: spOrderByTypeItem,
        getFollowUpDate:
          lastFollowUpDate.getFullYear() +
          "-" +
          (lastFollowUpDate.getMonth() + 1) +
          "-" +
          lastFollowUpDate.getDate()
      },
      () => {
        this.searchDataHitApi(this.state.searchterm);
      }
    );
  };

  render() {
    return (
      <div>
        <Header titles="Task List">
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a
                className="fa fa-filter fa-lg"
                onClick={() => this.setState({ showModalFilter: true })}
              />
            </li>
            <li>
              <a
                className="fa fa-search fa-lg"
                onClick={() =>
                  this.setState({ isSearchActive: !this.state.isSearchActive })
                }
              />
            </li>
          </ul>
        </Header>
        <ToastContainer />
        <OtosalesModalTaskListFilter
          showModal={this.state.showModalFilter}
          onCloseModal={this.onCloseModal}
          onSubmitFilter={this.onSubmitFilter}
        />
        <div className="content-wrapper" style={{ padding: "10px" }}>
          {/* <div className="panel panel-primary"> */}
          <div className="panel-body panel-body-list" id="Datalist">
            {this.state.isSearchActive && (
                <A2isText
                  name="searchterm"
                  caption="Search"
                  onKeyDown={this._handleKeyDown}
                  onChangeText={this.onChangeFunction}
                  minLength={0}
                  maxLength={20}
                  value={this.state.searchterm}
                  readonly={this.state.readonly}
                  visible={this.state.visible}
                  disabled={this.state.disable}
                  required={this.state.required}
                  hint="Type name prospect"
                />
            )}
            <BlockUi
              tag="div"
              className="col-xs-12 panel-body-list loadingtop"
              blocking={this.state.isLoading}
              loader={
                <Loader active type="ball-spin-fade-loader" color="#02a17c" />
              }
              message="Loading, please wait"
            >
              <TaskListAdapter
                onClickListItem={this.onClickListItem}
                onClickItem={this.onClickItem}
                name="sampleList"
                rowscount={this.state.datas.length}
                multipleselect={true}
                dataitems={this.state.datas}
                penawaranList={this.state.penawaranList}
                historyPenawaranList={this.state.historyPenawaranList}
              />
            </BlockUi>
          </div>
          {/* </div> */}
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageTaskList);
