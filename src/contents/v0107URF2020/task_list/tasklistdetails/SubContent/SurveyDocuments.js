import React, { Component } from 'react'
import { OtosalesSegmentDropdown } from "./../../../../../otosalescomponents"

export default class SurveyDocuments extends Component {
    render() {
        return (
            <div>
              <OtosalesSegmentDropdown title="SURVEY DOCUMENTS" divid="suveydocuments">
                <div className="row">
                  <div className="col-xs-12">
                    <label
                      style={{
                        color: "#4ac6e9",
                        width: "100px",
                        lineHeight: "40px"
                      }}
                      onClick={() => {
                        this.props.functions.onClickSurveyDocument();
                      }}
                    >
                      VIEW
                    </label>
                  </div>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
