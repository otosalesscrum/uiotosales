import React, { Component } from 'react'
import {
  OtosalesSelectFullviewrev,
  OtosalesSelectFullviewv2,
  Util,
  OtosalesSegmentDropdown
} from "../../../../../otosalescomponents";

export default class PolicyDelivery extends Component {
    render() {
        return (
            <div className={this.props.functions.disabledPolicyDelivery() ? "disablediv" : ""}>
              <OtosalesSegmentDropdown title="ALAMAT PENGIRIMAN POLICY" divid="alamatpengirimanpolicy">
              <div
                  className={
                    this.props.state.IsNotErrorpolicysentto ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Dikirim ke</span>
                  <div className="col-xs-12">
                    <OtosalesSelectFullviewrev
                      name="policysentto"
                      value={this.props.state.policysentto}
                      selectOption={this.props.state.policysentto}
                      onOptionsChange={this.props.functions.onOptionSelect2Change}
                      options={this.props.state.datapolicysentto}
                      placeholder="Pilih Dikirim ke"
                    />
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorpolicyname ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Nama</span>
                  <div className="col-xs-12">
                    {/* {!Util.stringArrayContains(this.props.state.policysentto + "", [
                      "BR",
                      "GC"
                    ]) && ( */}
                    <input
                      type="text"
                      name="policyname"
                      disabled={Util.stringArrayContains(
                        this.props.state.policysentto + "",
                        [
                          "CS"
                          // "GC"
                        ]
                      )}
                      maxLength="50"
                      value={this.props.state.policyname || ""}
                      onChange={this.props.functions.onChangeFunction}
                      className="form-control"
                    />
                    {/* )} */}
                    {/* {Util.stringArrayContains(this.props.state.policysentto + "", [
                      "GC"
                    ]) && (
                        <OtosalesSelectFullviewv2
                          minHeight={40}
                          name="policyname"
                          onSearchFunction={this.props.functions.getNameOnPolicyDelivery}
                          value={this.props.state.policyname}
                          selectOption={this.props.state.policyname}
                          onOptionsChange={this.props.functions.onOptionSelect2Change}
                          options={this.props.state.datapolicyaddress}
                          placeholder="Pilih Name Policy Delivered"
                        />
                      )}
                    {Util.stringArrayContains(this.props.state.policysentto + "", [
                      "BR"
                    ]) && (
                        <OtosalesSelectFullviewrev
                          minHeight={40}
                          name="policyname"
                          value={this.props.state.policyname}
                          selectOption={this.props.state.policyname}
                          onOptionsChange={this.props.functions.onOptionSelect2Change}
                          options={this.props.state.datapolicyaddress}
                          placeholder="Pilih Name Policy Delivered"
                        />
                      )} */}
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorpolicyaddress ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Alamat</span>
                  <div className="col-xs-12">
                    {Util.stringArrayContains(this.props.state.policysentto + "", [
                      "BR",
                      "CS",
                      "OH"
                    ]) && (
                      <input
                        type="text"
                        pattern="^[a-zA-Z0-9 ,-.\/]+$"
                        name="policyaddress"
                        disabled={Util.stringArrayContains(
                          this.props.state.policysentto + "",
                          [
                            "BR",
                            "CS"
                            // "GC"
                          ]
                        )}
                        maxLength="152"
                        value={this.props.state.policyaddress}
                        onChange={this.props.functions.onChangeFunction}
                        className="form-control"
                      />
                    )}
                    {!Util.stringArrayContains(this.props.state.policysentto + "", [
                      "BR",
                      "CS",
                      "OH"
                    ]) && (
                      <OtosalesSelectFullviewv2
                        minHeight={40}
                        name="policyaddress"
                        onSearchFunction={this.props.functions.getNameOnPolicyDelivery}
                        value={this.props.state.policyaddress}
                        selectOption={this.props.state.policyaddress}
                        onOptionsChange={this.props.functions.onOptionSelect2Change}
                        options={this.props.state.datapolicyaddress}
                        placeholder="Pilih Garda Center"
                      />
                    )}
                  </div>
                </div>
                <div
                  className={
                    this.props.state.IsNotErrorpolicykodepos ? "has-error row" : "row"
                  }
                >
                  <span className="col-xs-12 labelspanbold">Kode Pos</span>
                  <div className="col-xs-12">
                    <OtosalesSelectFullviewv2
                      name="policykodepos"
                      isDisabled={
                        Util.stringArrayContains(this.props.state.policysentto + "", [
                          "BR",
                          "GC",
                          "CS"
                        ])
                          ? true
                          : false
                      }
                      value={this.props.state.policykodepos}
                      selectOption={this.props.state.policykodepos}
                      onOptionsChange={this.props.functions.onOptionSelect2Change}
                      options={this.props.state.dataKodePos}
                      placeholder="Pilih Kode Pos"
                    />
                  </div>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
