import React, { Component } from "react";
import {
  Util,
  OtosalesDatePicker,
  OtosalesSelectFullviewrev,
  OtosalesSelectFullviewv2,
  OtosalesSegmentDropdown
} from "../../../../../otosalescomponents";
import NumberFormat from "react-number-format";

export default class PersonalData extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        className={
          this.props.functions.disabledPersonalData() ? "disablediv" : ""
        }
      >
        <OtosalesSegmentDropdown title="PERSONAL DATA" divid="personaldata">
          <div
            className={
              this.props.state.IsNotErrorpersonalname ? "row has-error" : "row"
            }
          >
            <span className="col-xs-12 labelspanbold">Nama Customer</span>
            <div className="col-xs-12">
              <input
                type="text"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                name="personalname"
                maxLength="50"
                disabled={this.props.functions.disabledPersonalDataName()}
                value={this.props.state.personalname}
                onChange={this.props.functions.onChangeFunction}
                className="form-control"
              />
            </div>
          </div>
          <div className="row marginTopBottomField">
            <div
              className={
                "col-xs-6 panel-body-list " +
                (this.props.state.IsNotErrorpersonaltanggallahir
                  ? "has-error"
                  : "")
              }
            >
              <span className="col-xs-12 labelspanbold">Tanggal Lahir</span>
              <div className="col-xs-12">
                <OtosalesDatePicker
                  maxDate={Util.convertDate()}
                  minDate={Util.convertDate().setFullYear(
                    Util.convertDate().getFullYear() - 70
                  )}
                  disabled={this.props.functions.disabledPersonalDataTanggalLahir()}
                  className="form-control"
                  name="personaltanggallahir"
                  selected={this.props.state.personaltanggallahir}
                  dateFormat="dd/mm/yyyy"
                  onChange={this.props.functions.onChangeFunctionDate}
                />
              </div>
            </div>
            <div
              className={
                "col-xs-6 panel-body-list " +
                (this.props.state.IsNotErrorpersonalJK ? "has-error" : "")
              }
            >
              <span className="col-xs-12 labelspanbold">Jenis Kelamin</span>
              <div className="col-xs-12">
                <OtosalesSelectFullviewrev
                  name="personalJK"
                  isDisabled={this.props.functions.disabledPersonalDataJenisKelamin()}
                  value={this.props.state.personalJK}
                  selectOption={this.props.state.personalJK}
                  onOptionsChange={this.props.functions.onOptionSelect2Change}
                  options={this.props.state.dataJK}
                  placeholder="Pilih Jenis Kelamin"
                />
              </div>
            </div>
          </div>
          <div
            className={
              "row " +
              (this.props.state.IsNotErrorpersonalalamat ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Alamat</span>
            <div className="col-xs-12">
              <input
                name="personalalamat"
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                disabled={this.props.functions.disabledPersonalDataAlamat()}
                maxLength={152}
                value={this.props.state.personalalamat}
                onChange={this.props.functions.onChangeFunction}
                type="text"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " +
              (this.props.state.IsNotErrorpersonalkodepos ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Kode Pos</span>
            <div className="col-xs-12">
              <OtosalesSelectFullviewv2
                name="personalkodepos"
                isDisabled={this.props.functions.disabledPersonalDataKodePos()}
                value={this.props.state.personalkodepos}
                selectOption={this.props.state.personalkodepos}
                onOptionsChange={this.props.functions.onOptionSelect2Change}
                options={this.props.state.dataKodePos}
                placeholder="Pilih Kode Pos"
              />
            </div>
          </div>
          <div
            className={
              "row " +
              (this.props.state.IsNotErrorpersonalemail ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Email</span>
            <div className="col-xs-12">
              <input
                type="email"
                name="personalemail"
                disabled={this.props.functions.disabledPersonalDataEmail()}
                value={
                  this.props.functions.isNotMaskingPhoneOrEmail(
                    this.props.state.isFocusEmail
                  ) || this.props.functions.isRoleCTIandRenewal()
                    ? this.props.state.personalemail
                    : Util.maskingEmail(this.props.state.personalemail)
                }
                onBlur={() => {
                  this.props.functions.setState({
                    isFocusEmail: false
                  });
                }}
                onFocus={() => {
                  this.props.functions.setState({
                    isFocusEmail: true
                  });
                }}
                onChange={this.props.functions.onChangeFunction}
                maxLength="50"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " +
              (this.props.state.IsNotErrorpersonalhp ? "has-error" : "")
            }
          >
            <span className="col-xs-12 labelspanbold">No Handphone</span>
            <div className="col-xs-12">
              {/* <NumberFormat
                          className="form-control"
                          disabled={this.props.functions.isBackToAO() || this.props.functions.isDealNeedDocRep()}
                          onValueChange={value => {
                          Log.debugGroup("value", value);
                          this.props.functions.setState({ personalhp: value.value });
                          }}
                          value={Util.maskingPhoneNumberv2(this.props.state.personalhp)}
                          format="#### #### #### ####"
                          prefix={""}
                      /> */}
              <input
                name="personalhp"
                maxLength="16"
                disabled={this.props.functions.disabledPersonalDataPhoneNo()}
                value={
                  this.props.functions.isNotMaskingPhoneOrEmail(
                    this.props.state.isFocusPhone
                  )
                    ? this.props.state.personalhp
                    : Util.maskingPhoneNumberv2(
                        Util.clearPhoneNo(this.props.state.personalhp)
                      )
                }
                onBlur={() => {
                  this.props.functions.setState({
                    isFocusPhone: false
                  });
                }}
                onFocus={() => {
                  this.props.functions.setState({
                    isFocusPhone: true
                  });
                }}
                onChange={this.props.functions.onChangeFunction}
                type="text"
                pattern="^[0-9]+$"
                className="form-control"
              />
            </div>
          </div>
          <div
            className={
              "row " +
              (this.props.state.IsNotErrorpersonalnomoridentitas
                ? "has-error"
                : "")
            }
          >
            <span className="col-xs-12 labelspanbold">Nomor Identitas</span>
            <div className="col-xs-12">
              {/* <NumberFormat
                          maxLength={30}
                          className="form-control"
                          onValueChange={value => {
                          // Log.debugGroup(value);
                          this.props.functions.setState({
                              personalnomoridentitas: value.value
                          });
                          }}
                          value={this.props.state.personalnomoridentitas}
                          prefix={""}
                      /> */}
              <input
                name="personalnomoridentitas"
                maxLength="30"
                disabled={this.props.functions.disabledPersonalDataIdentityNo()}
                value={this.props.state.personalnomoridentitas}
                onChange={this.props.functions.onChangeFunction}
                type="text"
                pattern="^[a-zA-Z0-9]+$"
                className="form-control"
              />
            </div>
          </div>
          {!this.props.functions.isHideDocRep() && (
            <React.Fragment>
              <div className="row">
                <div className="col-xs-12">
                  <input
                    name="personalneeddocrep"
                    checked={this.props.state.personalneeddocrep}
                    onChange={this.props.functions.onChangeFunction}
                    disabled={this.props.functions.disabledNeedDocRep()}
                    type="checkbox"
                    style={{ marginRight: "10px" }}
                  />
                  <label className="smallerCheckbox">NEED DOC REP</label>
                </div>
              </div>
              <div
                className={
                  "row " +
                  (this.props.state.IsNotErrorpersonalamountrep
                    ? "has-error"
                    : "")
                }
              >
                <span className="col-xs-12 labelspanbold">Amount Rep</span>
                <div className="col-xs-12">
                  <NumberFormat
                    className="form-control"
                    disabled={this.props.functions.disabledNeedDocRep()}
                    onValueChange={value => {
                      this.props.functions.setState({
                        personalamountrep:
                          value.floatValue == 0 ? "" : value.floatValue
                      });
                    }}
                    value={this.props.state.personalamountrep}
                    thousandSeparator=","
                    prefix={
                      Util.isNullOrEmpty(this.props.state.personalamountrep)
                        ? ""
                        : "Rp."
                    }
                  />
                </div>
              </div>
            </React.Fragment>
          )}
        </OtosalesSegmentDropdown>
      </div>
    );
  }
}
