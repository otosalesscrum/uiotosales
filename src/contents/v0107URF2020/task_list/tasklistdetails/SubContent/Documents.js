import React, { Component } from 'react'
import {
    OtosalesDropzonev2,
    OtosalesLoadingSegment,
    OtosalesSegmentDropdown
  } from "../../../../../otosalescomponents";

export default class Documents extends Component {
    render() {
        return (
            <div className={this.props.functions.disabledDocuments() ? "disablediv" : ""}>
              <OtosalesSegmentDropdown title="DOCUMENTS" divid="documents">
              <OtosalesLoadingSegment
                  isLoading={this.props.state.isLoadingDocumentProspect}
                >
                  <React.Fragment>
                    <div className="row">
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorSPPAKB ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataSPPAKB}
                            name="SPPAKB"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          SPPAKB/Form Renewal*
                        </center>
                      </div>
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorFAKTUR ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataFAKTUR}
                            name="FAKTUR"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">FAKTUR</center>
                      </div>
                    </div>
                    <div className="row">
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorDOCNSA1 ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataDOCNSA1}
                            name="DOCNSA1"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          Dokumen NSA
                          {this.props.state.FollowUpStatus == 3 &&
                          this.props.state.FollowUpInfo == 46
                            ? "*"
                            : ""}
                        </center>
                      </div>
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorDOCNSA2 ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataDOCNSA2}
                            name="DOCNSA2"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          Dokumen NSA
                          {this.props.state.FollowUpStatus == 3 &&
                          this.props.state.FollowUpInfo == 46
                            ? "*"
                            : ""}
                        </center>
                      </div>
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorDOCNSA3 ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataDOCNSA3}
                            name="DOCNSA3"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          Dokumen NSA
                          {this.props.state.FollowUpStatus == 3 &&
                          this.props.state.FollowUpInfo == 46
                            ? "*"
                            : ""}
                        </center>
                      </div>
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorDOCNSA4 ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataDOCNSA4}
                            name="DOCNSA4"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          Dokumen NSA
                          {this.props.state.FollowUpStatus == 3 &&
                          this.props.state.FollowUpInfo == 46
                            ? "*"
                            : ""}
                        </center>
                      </div>
                      <div
                        className={
                          "col-xs-4 col-md-2 panel-body-list" +
                          (this.props.state.IsNotErrorDOCNSA5 ? " has-error" : "")
                        }
                      >
                        <div className="text-center">
                          <OtosalesDropzonev2
                            src={this.props.state.dataDOCNSA5}
                            name="DOCNSA5"
                            handleDeleteImage={this.props.functions.handleDeleteImage}
                            handleUploadImage={this.props.functions.handleUploadImage}
                          />
                        </div>
                        <center className="smallerCheckbox">
                          Dokumen NSA
                          {this.props.state.FollowUpStatus == 3 &&
                          this.props.state.FollowUpInfo == 46
                            ? "*"
                            : ""}
                        </center>
                      </div>
                    </div>
                    <div
                      className={
                        this.props.state.IsNotErrordocumentsremarkstosa
                          ? "row has-error"
                          : "row"
                      }
                    >
                      <span className="col-xs-12 labelspanbold">
                        Remarks to SA
                        {this.props.state.FollowUpStatus == 3 &&
                        this.props.state.FollowUpInfo == 46
                          ? "*"
                          : ""}
                      </span>
                      <div className="col-xs-12">
                        <input
                          type="text"
                          name="documentsremarkstosa"
                          maxLength="200"
                          pattern="^[a-zA-Z0-9 ,-.\/':;?]+$"
                          value={this.props.state.documentsremarkstosa || ""}
                          onChange={this.props.functions.onChangeFunction}
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="row">
                      <span className="col-xs-12 labelspanbold">Remarks from SA</span>
                      <div className="col-xs-12">
                        <input
                          type="text"
                          name="documentsremarksfromsa"
                          maxLength="200"
                          readOnly
                          value={this.props.state.documentsremarksfromsa || ""}
                          // onChange={this.props.functions.onChangeFunction}
                          className="form-control"
                        />
                      </div>
                    </div>
                  </React.Fragment>
                </OtosalesLoadingSegment>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
