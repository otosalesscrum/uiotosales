import React, { Component } from 'react'
import {
  OtosalesDatePicker,
  OtosalesSelectFullviewrev,
  OtosalesSelectFullviewv2,
  OtosalesSegmentDropdown,
  Util
} from "../../../../../otosalescomponents";

export default class SurveySchedule extends Component {
    render() {
        return (
            <div className={this.props.functions.disabledSurveySchedule() ? "disablediv" : ""}>
              <OtosalesSegmentDropdown title="SURVEY SCHEDULE" divid="schedulesurvey">
                <div className="row">
                  <div className="col-xs-12">
                    <input
                      name="surveyneed"
                      checked={this.props.state.surveyneed}
                      onChange={this.props.functions.onChangeFunction}
                      type="checkbox"
                      disabled={this.props.functions.disabledNeedSurvey()}
                      style={{ marginRight: "10px" }}
                    />
                    <label className="smallerCheckbox">NEED SURVEY</label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12">
                    <input
                      name="surveynsaskipsurvey"
                      checked={this.props.state.surveynsaskipsurvey}
                      disabled={this.props.state.FollowUpInfo == 59 ? true : false}
                      onChange={this.props.functions.onChangeFunction}
                      type="checkbox"
                      style={{ marginRight: "10px" }}
                    />
                    <label className="smallerCheckbox">NSA SKIP SURVEY</label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12">
                    <input
                      name="surveychecklistmanual"
                      checked={this.props.state.surveychecklistmanual}
                      onChange={this.props.functions.onChangeFunction}
                      type="checkbox"
                      style={{ marginRight: "10px" }}
                    />
                    <label className="smallerCheckbox">
                      SUDAH SURVEY ADDITIONAL ORDER
                    </label>
                  </div>
                </div>
                <div className={this.props.state.surveyneed ? "" : "disablediv"}>
                  <div
                    className={
                      this.props.state.IsNotErrorsurveykota ? "has-error row" : "row"
                    }
                  >
                    <span className="col-xs-12 labelspanbold">Kota Survei</span>
                    <div className="col-xs-12">
                      <OtosalesSelectFullviewrev
                        name="surveykota"
                        value={this.props.state.surveykota}
                        selectOption={this.props.state.surveykota}
                        onOptionsChange={this.props.functions.onOptionSelect2Change}
                        options={this.props.state.datasurveykota}
                        placeholder="Pilih Kota Survei"
                      />
                    </div>
                  </div>
                  <div
                    className={
                      this.props.state.IsNotErrorsurveylokasi ? "has-error row" : "row"
                    }
                  >
                    <span className="col-xs-12 labelspanbold">Lokasi Survei</span>
                    <div
                      className={
                        Util.isNullOrEmpty(this.props.state.surveykota)
                          ? "col-xs-12 disablediv"
                          : "col-xs-12"
                      }
                    >
                      <OtosalesSelectFullviewrev
                        name="surveylokasi"
                        value={this.props.state.surveylokasi}
                        selectOption={this.props.state.surveylokasi}
                        onOptionsChange={this.props.functions.onOptionSelect2Change}
                        options={this.props.state.datasurveylokasi}
                        placeholder="Pilih Lokasi Survei"
                      />
                    </div>
                  </div>
                  {this.props.state.surveylokasi == "Others" && (
                    <React.Fragment>
                      <div
                        className={
                          this.props.state.IsNotErrorsurveyalamat ? "has-error row" : "row"
                        }
                      >
                        <span className="col-xs-12 labelspanbold">Alamat Survei</span>
                        <div className="col-xs-12">
                          <input
                            type="text"
                            name="surveyalamat"
                            maxLength="152"
                            pattern="^[a-zA-Z0-9 ,-.\/]+$"
                            value={this.props.state.surveyalamat}
                            onChange={this.props.functions.onChangeFunction}
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div
                        className={
                          this.props.state.IsNotErrorsurveykodepos ? "has-error row" : "row"
                        }
                      >
                        <span className="col-xs-12 labelspanbold">
                          Kode Pos Survei
                        </span>
                        <div className="col-xs-12">
                          <OtosalesSelectFullviewv2
                            name="surveykodepos"
                            value={this.props.state.surveykodepos}
                            selectOption={this.props.state.surveykodepos}
                            onOptionsChange={this.props.functions.onOptionSelect2Change}
                            options={this.props.state.dataKodePosSurvey}
                            placeholder="Pilih Kode Pos"
                          />
                        </div>
                      </div>
                    </React.Fragment>
                  )}
                  <div className="row">
                    <div
                      className={
                        this.props.state.IsNotErrorsurveytanggal
                          ? "has-error col-xs-6 panel-body-list"
                          : "col-xs-6 panel-body-list"
                      }
                    >
                      <span className="col-xs-12 labelspanbold">
                        Tanggal Survei
                        <OtosalesDatePicker
                          className="form-control"
                          // minDate={Util.convertDate().setDate(
                          //   Util.convertDate().getDate()
                          // )}
                          // maxDate={Util.convertDate().setDate(
                          //   Util.convertDate().getDate() + 13
                          // )}
                          // maxDate={Util.convertDate().setDate(Util.convertDate().getDate() + 16)}
                          // maxDate={Util.addDateWithoutWeekend(Util.convertDate(), 13)}
                          // filterDate={Util.isWeekday}
                          filterDate={this.props.functions.surveyDateExclude}
                          name="surveytanggal"
                          selected={this.props.state.surveytanggal}
                          dateFormat="dd/mm/yyyy"
                          onChange={this.props.functions.onChangeFunctionDate}
                        />
                      </span>
                      <div className="col-xs-12" />
                    </div>
                    {/* {this.props.state.surveytanggal && ( */}
                    <div
                      className={
                        (this.props.state.IsNotErrorsurveywaktu
                          ? "has-error col-xs-6 panel-body-list"
                          : "col-xs-6 panel-body-list") +
                        " " +
                        (Util.isNullOrEmpty(this.props.state.surveytanggal)
                          ? "disablediv"
                          : "")
                      }
                    >
                      <span className="col-xs-12 labelspanbold">Waktu Survei</span>
                      <div className="col-xs-12">
                        <OtosalesSelectFullviewrev
                          name="surveywaktu"
                          value={this.props.state.surveywaktu}
                          selectOption={this.props.state.surveywaktu}
                          onOptionsChange={this.props.functions.onOptionSelect2Change}
                          options={this.props.state.datasurveywaktu}
                          placeholder="Pilih Waktu Survei"
                        />
                      </div>
                    </div>
                    {/* )} */}
                  </div>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
