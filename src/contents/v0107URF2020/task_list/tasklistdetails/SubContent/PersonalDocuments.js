import React, { Component } from "react";
import {
  OtosalesDropzonev2,
  Util,
  OtosalesLoadingSegment,
  OtosalesSegmentDropdown
} from "../../../../../otosalescomponents";

export default class PersonalDocuments extends Component {
  render() {
    return (
      <div>
        <OtosalesSegmentDropdown
          title="PERSONAL DOCUMENTS"
          divid="personaldocument"
        >
          <OtosalesLoadingSegment
            isLoading={this.props.state.isLoadingPersonalDocument}
          >
            <div>
              {/* {!this.props.state.IsRenewal && (
                    <React.Fragment> */}
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.props.state.IsNotErrorKTP ? " has-error" : "") +
                  (this.props.functions.disabledPersonalDocuments()
                    ? " disablediv"
                    : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.props.state.dataKTP}
                    name="KTP"
                    handleDeleteImage={this.props.functions.handleDeleteImage}
                    handleUploadImage={this.props.functions.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  KTP/KITAS{!this.props.state.IsRenewal ? "*" : ""}
                </center>
              </div>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.props.state.IsNotErrorSTNK ? " has-error" : "") +
                  (this.props.functions.disabledPersonalDocuments()
                    ? " disablediv"
                    : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.props.state.dataSTNK}
                    name="STNK"
                    handleDeleteImage={this.props.functions.handleDeleteImage}
                    handleUploadImage={this.props.functions.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  STNK
                  {!this.props.state.IsRenewal &&
                  this.props.state.vehicleusedcar
                    ? "*"
                    : ""}
                </center>
              </div>
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.props.state.IsNotErrorBSTB ? " has-error" : "") +
                  (this.props.functions.disabledPersonalDocuments()
                    ? " disablediv"
                    : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.props.state.dataBSTB}
                    name="BSTB"
                    handleDeleteImage={this.props.functions.handleDeleteImage}
                    handleUploadImage={this.props.functions.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  BSTB
                  {!this.props.state.IsRenewal &&
                  !this.props.state.vehicleusedcar
                    ? "*"
                    : ""}
                </center>
              </div>
              {/* </React.Fragment>
                  )} */}
              {!this.props.functions.isHideDocRep() && (
                <React.Fragment>
                  <div
                    className={
                      "col-xs-4 col-md-2 panel-body-list" +
                      (this.props.state.IsNotErrorDOCREP ? " has-error" : "")
                    }
                  >
                    <div className="text-center">
                      <OtosalesDropzonev2
                        src={this.props.state.dataDOCREP}
                        name="DOCREP"
                        handleDeleteImage={
                          this.props.functions.handleDeleteImage
                        }
                        handleUploadImage={
                          this.props.functions.handleUploadImage
                        }
                      />
                    </div>
                    <center className="smallerCheckbox">DOC REP</center>
                  </div>
                </React.Fragment>
              )}
              <div
                className={
                  "col-xs-4 col-md-2 panel-body-list" +
                  (this.props.state.IsNotErrorKONFIRMASICUST
                    ? " has-error"
                    : "") +
                  (this.props.functions.disabledPersonalDocuments()
                    ? " disablediv"
                    : "")
                }
              >
                <div className="text-center">
                  <OtosalesDropzonev2
                    src={this.props.state.dataKONFIRMASICUST}
                    name="KONFIRMASICUST"
                    handleDeleteImage={this.props.functions.handleDeleteImage}
                    handleUploadImage={this.props.functions.handleUploadImage}
                  />
                </div>
                <center className="smallerCheckbox">
                  KONFIRMASI CUSTOMER*
                </center>
              </div>
            </div>
          </OtosalesLoadingSegment>
        </OtosalesSegmentDropdown>
      </div>
    );
  }
}
