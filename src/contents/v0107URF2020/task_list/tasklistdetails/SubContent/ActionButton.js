import React, { Component } from 'react'
import CallLater from "../../../../../assets/call_later_180.png";
import NotDeal from "../../../../../assets/not_deal_180.png";
import PolicyCreated from "../../../../../assets/policycreated_180.png";
import Potential from "../../../../../assets/potential_180.png";
import SendQuotation from "../../../../../assets/send_180.png";

export default class ActionButton extends Component {
    render() {
        return (
            <div
              className={"row" + (this.props.functions.disableActionButton() ? " disablediv" : "")}
              style={{ backgroundColor: "#18516c" }}
            >
              <div className="text-center col-xs-12 panel-body-list">
                {this.props.functions.isShowButtonCall() && (
                  <div
                    onClick={() => this.props.functions.onClickActionButton("CALLLATER")}
                    className="col-xs-3 col-md-1 pull-right-md"
                    style={{
                      marginBottom: "5px",
                      marginTop: "5px",
                      fontSize: "smaller"
                    }}
                  >
                    <img className="img img-responsive" src={CallLater} />
                    <span className="labelfontwhite">Call Later</span>
                  </div>
                )}
                {this.props.functions.isShowButtonPotential() && (
                  <div
                    onClick={() => this.props.functions.onClickActionButton("POTENTIAL")}
                    className="col-xs-3 col-md-1 pull-right-md"
                    style={{
                      marginBottom: "5px",
                      marginTop: "5px",
                      fontSize: "smaller"
                    }}
                  >
                    <img className="img img-responsive" src={Potential} />
                    <span className="labelfontwhite">Potential</span>
                  </div>
                )}
                {this.props.functions.isShowButtonDeal() && (
                  <div
                    onClick={() => this.props.functions.onClickActionButton("DEAL")}
                    className="col-xs-3 col-md-1 pull-right-md"
                    style={{
                      marginBottom: "5px",
                      marginTop: "5px",
                      fontSize: "smaller"
                    }}
                  >
                    <img className="img img-responsive" src={PolicyCreated} />
                    <span className="labelfontwhite">Deal</span>
                  </div>
                )}
                {this.props.functions.isShowButtonNotDeal() && (
                  <div
                    onClick={() => this.props.functions.onClickActionButton("NOTDEAL")}
                    className="col-xs-3 col-md-1 pull-right-md"
                    style={{
                      marginBottom: "5px",
                      marginTop: "5px",
                      fontSize: "smaller"
                    }}
                  >
                    <img className="img img-responsive" src={NotDeal} />
                    <span className="labelfontwhite">Not Deal</span>
                  </div>
                )}
                <div
                  onClick={() => {
                    this.props.functions.checkDoubleInsured(
                      this.props.state.vehiclechasisno,
                      this.props.state.vehicleengineno,
                      this.props.state.selectedQuotation,
                      () => {
                        if (this.props.state.isMVGodig) {
                          this.props.functions.onGetOrderNoMVGodig(this.props.state.GuidPenawaran, () => {
                            this.props.functions.setState({ showModalButtonSendQuotation: true });
                          });
                        } else {
                          this.props.functions.setState({ showModalButtonSendQuotation: true });
                        }
                      }
                    );
                  }}
                  className="col-xs-3 col-md-1 pull-right-md"
                  style={{
                    marginBottom: "5px",
                    marginTop: "5px",
                    fontSize: "smaller"
                  }}
                >
                  <img className="img img-responsive" src={SendQuotation} />
                  <span className="labelfontwhite">Send Quotation</span>
                </div>
                {/* )} */}
              </div>
            </div>
          );
    }
}
