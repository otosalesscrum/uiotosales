import React, { Component } from 'react'
import { OtosalesSegmentDropdown } from "./../../../../../otosalescomponents"

export default class SurveyData extends Component {
    render() {
        return (
            <div>
              <OtosalesSegmentDropdown title="SURVEI DATA" divid="surveydata">
                <div className="row">
                  <span className="col-xs-12 labelspanbold">No Cover</span>
                  <div className="col-xs-12">
                    <textarea
                      value={this.props.state.surveydatanocover}
                      name="surveydatanocover"
                      type="text"
                      readOnly={true}
                      className="form-control"
                      rows={3}
                      style={{ resize: "none", padding: "0px" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Original Defect</span>
                  <div className="col-xs-12">
                    <textarea
                      value={this.props.state.surveydataoriginaldefect}
                      name="surveydataoriginaldefect"
                      type="text"
                      readOnly={true}
                      className="form-control"
                      rows={3}
                      style={{ resize: "none", padding: "0px" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Accessories</span>
                  <div className="col-xs-12">
                    <textarea
                      value={this.props.state.surveydataaccessories}
                      name="surveydataaccessories"
                      type="text"
                      readOnly={true}
                      className="form-control"
                      rows={3}
                      style={{ resize: "none", padding: "0px" }}
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Survey Status</span>
                  <div className="col-xs-12">
                    <input
                      value={this.props.state.surveydatastatus}
                      name="surveydatastatus"
                      type="text"
                      readOnly={true}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">
                    Surveyor Recommendation
                  </span>
                  <div className="col-xs-12">
                    <input
                      value={this.props.state.surveydatarecommendation}
                      name="surveydatarecommendation"
                      type="text"
                      readOnly={true}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Remarks</span>
                  <div className="col-xs-12">
                    <input
                      value={this.props.state.surveydataremarks}
                      name="surveydataremarks"
                      type="text"
                      readOnly={true}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="row">
                  <span className="col-xs-12 labelspanbold">Surveyor</span>
                  <div className="col-xs-12">
                    <input
                      value={this.props.state.surveydatasurveyor}
                      name="surveydatasurveyor"
                      type="text"
                      readOnly={true}
                      className="form-control"
                    />
                  </div>
                </div>
              </OtosalesSegmentDropdown>
            </div>
          );
    }
}
