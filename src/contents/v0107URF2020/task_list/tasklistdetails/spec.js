
import * as LogicalActions from "./Actions";
import { Util } from "./../../../../otosalescomponents";

describe('should basic cover', () => {
    it('disable if Follow up survey result', () => {
        let res = false;
        if(LogicalActions.disableChecklistPayerCompany(false, 2, 59, '237648264')){
            res = true;
        }
        expect(res).toBe(true);
    })

    it('enable if Data baru input', () => {
        let res = false;
        if(LogicalActions.disableChecklistPayerCompany(false, 1, 1, null)){
            res = true;
        }
        expect(res).toBe(false);
    })
})

describe('checklist polis terbit sebelum bayar', () => {
    it('should enable if follow up survey result ', () => {
        let test = LogicalActions.disabledPolisTerbitSebelumBayar(2, 59, '232345435');
        expect(test).toBe(false);
    })
    it('should disable if send to surveyor ', () => {
        let test = LogicalActions.disabledPolisTerbitSebelumBayar(2, 58, '232345435');
        expect(test).toBe(true);
    })
    
    it('should disable if send to SA', () => {
        let test = LogicalActions.disabledPolisTerbitSebelumBayar(2, 61, '232345435');
        expect(test).toBe(true);
    })

    it('testing validate  : aee@hit-leadmaster.com is valid', () => {
        let isValid = Util.validateEmail("aee@hit-leadmaster.com");
        expect(isValid).toBe(true);
    })
})
