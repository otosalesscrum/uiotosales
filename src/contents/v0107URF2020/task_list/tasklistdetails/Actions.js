import * as DataSource from "./DataSource";
import { ACCOUNTDATA, URL_GEN5 } from "../../../../config";
import { Util, Log } from "../../../../otosalescomponents";
import { secureStorage } from "../../../../otosalescomponents/helpers/SecureWebStorage";

export const isNotRunAutoSave = state => {
  let res = false;

  if (!state.isflagupdatetaskdetail) {
    res = true;
  }

  if (state.isLoadingCompanyDocument) {
    res = true;
  }

  if (state.isLoadingPersonalDocument) {
    res = true;
  }

  if (state.isLoadingDocumentProspect) {
    res = true;
  }

  return res;
};

export const hideDocRep = state => {
  let temp = false;

  temp = true; // FAY - Always hide by Sprint 4 Otosales (0063-URF-2020)

  return temp;
}

export const getTaskDetailDocument = (
  CustID,
  FollowUpNo,
  Type,
  callback = data => {}
) => {
  DataSource.getTaskDetailDocument(CustID, FollowUpNo, Type)
    .then(res => res.json())
    .then(jsn => {
      if (jsn.status) {
        callback(jsn);
      } else {
        Util.showToast("Gagal fetching : getTaskDetailDocument", "WARNING");
      }
    })
    .catch(error => {
      Log.error("parsing failed", error);
      Util.hittingAPIError(error, () => {});
    });
};

export const setModelPersonalData = state => {
  if (Util.isNullOrEmpty(JSON.parse(ACCOUNTDATA))) {
    return;
  }
  let User = JSON.parse(ACCOUNTDATA).UserInfo.User;
  return {
    Name: state.personalname,
    CustBirthDay: Util.formatDate(
      state.personaltanggallahir,
      "yyyy-mm-dd hh:MM:ss"
    ),
    CustGender: state.personalJK,
    CustAddress: state.personalalamat,
    PostalCode: state.personalkodepos,
    Email1: state.personalemail,
    Phone1: state.personalhp,
    IdentityNo: state.personalnomoridentitas,
    AmountRep: state.personalamountrep,
    IsPayerCompany: state.personalpayercompany,
    isNeedDocRep: state.personalneeddocrep,
    isCompany: state.isCompany,
    SalesOfficerID: state.SalesOfficerID || User.SalesOfficerID
  };
};

export const setModelCompanyData = state => {
  return {
    CompanyName: state.companyname,
    NPWPno: state.companynpwpnumber,
    NPWPdate: Util.isNullOrEmpty(state.companynpwpdata)
      ? null
      : Util.formatDate(state.companynpwpdata),
    NPWPaddres: state.companynpwpaddress,
    SIUPno: state.companysiupnumber,
    PKPno: state.companypkpnumber,
    PKPdate: state.companypkpdata,
    OfficeAddress: state.companyofficeaddress,
    PostalCode: state.companypostalcode,
    OfficePhoneNo: state.companyofficenumber,
    PICPhoneNo: state.companypichp,
    PICname: state.companypdcname,
    PICEmail: state.companyemail
  };
};

export const setModelVehicleData = state => {
  return {
    Vehicle: "",
    ProductTypeCode: state.vehicleProductTypeCode,
    ProductCode: state.vehicleproductcode,
    MouID: state.vMouID, 
    VehicleCode: state.vehiclevehiclecode,
    BrandCode: state.vehicleBrandCode,
    ModelCode: state.vehicleModelCode,
    Series: state.vehicleSeries,
    Type: state.vehicleType,
    Sitting: state.vehicleSitting,
    Year: state.vehicleYear,
    CityCode: Util.valueTrim(state.vehicleregion),
    InsuranceType: Util.isNullOrEmpty(state.vehicleInsuranceType)
      ? 0
      : state.vehicleInsuranceType,
    SumInsured: state.vehicletotalsuminsuredtemp,
    SegmentCode: state.vehiclesegmentcode,
    UsageCode: state.vehicleusage,
    AccessSI: state.vehicleAccessSI,
    RegistrationNumber: (state.vehicleplateno + "").toUpperCase(),
    EngineNumber: (state.vehicleengineno + "").toUpperCase(),
    ChasisNumber: (state.vehiclechasisno + "").toUpperCase(),
    IsNew: !state.vehicleusedcar,
    BankID: "",
    VANumber: "",
    Remarks: state.vehicleremarks,
    IsPolicyIssuedBeforePaying: state.vehicleispolicyissuedbeforpaying ? 1 : 0,
    IsORDefectsRepair: state.vehicleisordefectrepair ? 1 : 0,
    IsAccessoriesChange: state.vehicleisaccessorieschange ? 1 : 0,
    DealerCode: state.vehicledealer,
    SalesDealer: state.vehiclesalesman,
    ColorOnBPKB: state.vehiclecolor,
    PeriodFrom: Util.formatDate(state.vehicleperiodfrom, "yyyy-mm-dd hh:MM:ss"),
    PeriodTo: Util.formatDate(state.vehicleperiodto, "yyyy-mm-dd hh:MM:ss")
  };
};

export const setModelPolicyAddress = state => {
  return {
    SentTo: state.policysentto,
    Name: state.policyname,
    Address: state.policyaddress,
    PostalCode: state.policykodepos
  };
};

export const setModelSurveySchedule = state => {
  return {
    CityCode: state.surveykota,
    LocationCode: state.surveylokasi,
    SurveyAddress: state.surveyalamat,
    SurveyPostalCode: state.surveykodepos,
    SurveyDate: Util.formatDate(state.surveytanggal, "yyyy-mm-dd hh:MM:ss"),
    ScheduleTimeID: state.surveywaktu,
    IsNeedSurvey: state.surveyneed,
    IsNSASkipSurvey: state.surveynsaskipsurvey,
    IsManualSurvey: state.surveychecklistmanual
  };
};

export const setModelImageData = state => {
  let ImageDataList = [
    { ImageType: "IDENTITYCARD", PathFile: state.KTP },
    { ImageType: "STNK", PathFile: state.STNK },
    { ImageType: "BSTB", PathFile: state.BSTB },
    { ImageType: "FAKTUR", PathFile: state.FAKTUR },
    { ImageType: "DOCNSA1", PathFile: state.DOCNSA1 },
    { ImageType: "DOCNSA2", PathFile: state.DOCNSA2 },
    { ImageType: "DOCNSA3", PathFile: state.DOCNSA3 },
    { ImageType: "DOCNSA4", PathFile: state.DOCNSA4 },
    { ImageType: "DOCNSA5", PathFile: state.DOCNSA5 },
    { ImageType: "KONFIRMASICUST", PathFile: state.KONFIRMASICUST },
    { ImageType: "SPPAKB", PathFile: state.SPPAKB },
    { ImageType: "DOCREP", PathFile: state.DOCREP },
    { ImageType: "NPWP", PathFile: state.NPWP },
    { ImageType: "SIUP", PathFile: state.SIUP },
    { ImageType: "BUKTIBAYAR", PathFile: state.BUKTIBAYAR },
    { ImageType: "BUKTIBAYAR2", PathFile: state.BUKTIBAYAR2 },
    { ImageType: "BUKTIBAYAR3", PathFile: state.BUKTIBAYAR3 },
    { ImageType: "BUKTIBAYAR4", PathFile: state.BUKTIBAYAR4 },
    { ImageType: "BUKTIBAYAR5", PathFile: state.BUKTIBAYAR5 }
  ];

  // ImageDataList = (Util.isNullOrEmpty(state.selectedQuotation) ? ImageDataList.filter( data => data.ImageType != "BUKTIBAYAR" ) : ImageDataList);

  return ImageDataList;
};

export const setModelRemarks = state => {
  return {
    RemarkToSA: state.documentsremarkstosa
    // RemarkFromSA: state.documentsremarksfromsa,
  };
};

export const setModelAttachmentEmail = (
  Seq,
  AttachmentID,
  AttachmentDescription,
  FileByte,
  FileExtension,
  FileContentType
) => {
  return {
    Seq,
    AttachmentID,
    AttachmentDescription,
    FileByte,
    FileExtension,
    FileContentType
  };
};

export const setModelOrderSimulation = state => {
  if (Util.isNullOrEmpty(JSON.parse(ACCOUNTDATA))) {
    return;
  }
  let User = JSON.parse(ACCOUNTDATA).UserInfo.User;
  Log.debugGroup();
  var BasicCover = [...state.databasiccoverall].filter(
    data => data.Id == state.vehiclebasiccoverage
  )[0];

  let MultiYearF = false;
  let YearCoverage = 0;

  if (!Util.isNullOrEmpty(BasicCover)) {
    YearCoverage = BasicCover.TLOPeriod + BasicCover.ComprePeriod;
    MultiYearF = YearCoverage > 1 ? true : false;
  }

  return {
    OrderNo: state.selectedQuotation,
    CustID: state.CustID,
    FollowUpNo: state.FollowUpNo,
    // QuotationNo: "",
    TLOPeriod: Util.isNullOrEmpty(BasicCover) ? 0 : BasicCover.TLOPeriod,
    ComprePeriod: Util.isNullOrEmpty(BasicCover) ? 0 : BasicCover.ComprePeriod,
    BranchCode: User.BranchCode,
    SalesOfficerID: state.SalesOfficerID || User.SalesOfficerID,
    PhoneSales: User.Phone1,
    SalesDealer: state.vehiclesalesman,
    ProductTypeCode: state.vehicleProductTypeCode,
    ProductCode: state.vehicleproductcode,
    Phone1: state.isCompany ? state.companyofficenumber : state.personalhp,
    Email1: state.isCompany ? state.companyemail : state.personalemail,
    Email2: "",
    InsuranceType: state.vehicleInsuranceType,
    ApplyF: "true",
    SendF: "false",
    AdminFee: state.AdminFee,
    MultiYearF: MultiYearF,
    TotalPremi: state.TotalPremi,
    YearCoverage: YearCoverage,
    LastInterestNo: 0,
    LastCoverageNo: 0
  };
};

export const setModelOrderSimulationMV = state => {
  return {
    OrderNo: state.selectedQuotation,
    ObjectNo: 1,
    AccessSI: state.ACCESSCOVER,
    ProductTypeCode: state.vehicleProductTypeCode,
    VehicleCode: state.vehiclevehiclecode,
    BrandCode: state.vehicleBrandCode,
    ModelCode: state.vehicleModelCode,
    Series: state.vehicleSeries,
    Type: state.vehicleType,
    Sitting: state.vehicleSitting,
    Year: state.vehicleYear,
    CityCode: state.vehicleregion,
    UsageCode: state.vehicleusage,
    SumInsured: state.vehicletotalsuminsuredtemp,
    RegistrationNumber: (state.vehicleplateno + "").toUpperCase(),
    EngineNumber: (state.vehicleengineno + "").toUpperCase(),
    ChasisNumber: (state.vehiclechasisno + "").toUpperCase(),
    IsNew: !state.vehicleusedcar
  };
};

export const setModelErrorFlagMandatory = state => {
  let ErrorFlagMandatory = {
    // Personal data
    IsNotErrorpersonalname: state.IsNotErrorpersonalname,
    IsNotErrorpersonaltanggallahir: state.IsNotErrorpersonaltanggallahir,
    IsNotErrorpersonalJK: state.IsNotErrorpersonalJK,
    IsNotErrorpersonalalamat: state.IsNotErrorpersonalalamat,
    IsNotErrorpersonalkodepos: state.IsNotErrorpersonalkodepos,
    IsNotErrorpersonalemail: state.IsNotErrorpersonalemail,
    IsNotErrorpersonalhp: state.IsNotErrorpersonalhp,
    IsNotErrorpersonalnomoridentitas: state.IsNotErrorpersonalnomoridentitas,
    IsNotErrorpersonalamountrep: state.IsNotErrorpersonalamountrep,

    // Company Data
    IsNotErrorcompanyname: state.IsNotErrorcompanyname,
    IsNotErrorcompanynpwpnumber: state.IsNotErrorcompanynpwpnumber,
    IsNotErrorcompanynpwpdata: state.IsNotErrorcompanynpwpdata,
    IsNotErrorcompanynpwpaddress: state.IsNotErrorcompanynpwpaddress,
    IsNotErrorcompanyofficeaddress: state.IsNotErrorcompanyofficeaddress,
    IsNotErrorcompanypostalcode: state.IsNotErrorcompanypostalcode,
    IsNotErrorcompanyofficenumber: state.IsNotErrorcompanyofficenumber,
    IsNotErrorcompanypdcname: state.IsNotErrorcompanypdcname,
    IsNotErrorcompanypichp: state.IsNotErrorcompanypichp,

    // Vehicle Data
    IsNotErrorvehiclevehiclecode: state.IsNotErrorvehiclevehiclecode,
    IsNotErrorvehiclebasiccoverage: state.IsNotErrorvehiclebasiccoverage,
    IsNotErrorvehicleperiodfrom: state.IsNotErrorvehicleperiodfrom,
    IsNotErrorvehicleperiodto: state.IsNotErrorvehicleperiodto,
    IsNotErrorvehicleusage: state.IsNotErrorvehicleusage,
    IsNotErrorvehiclecolor: state.IsNotErrorvehiclecolor,
    IsNotErrorvehicleproducttype: state.IsNotErrorvehicleproducttype,
    IsNotErrorvehicleproductcode: state.IsNotErrorvehicleproductcode,
    IsNotErrorvehiclesegmentcode: state.IsNotErrorvehiclesegmentcode,
    IsNotErrorvehicleplateno: state.IsNotErrorvehicleplateno,
    IsNotErrorvehiclechasisno: state.IsNotErrorvehiclechasisno,
    IsNotErrorvehicleengineno: state.IsNotErrorvehicleengineno,
    IsNotErrorvehicleregion: state.IsNotErrorvehicleregion,
    IsNotErrorvehicledealer: state.IsNotErrorvehicledealer,
    IsNotErrorvehiclesalesman: state.IsNotErrorvehiclesalesman,
    IsNotErrorvehiclenamebank: state.IsNotErrorvehiclenamebank,
    IsNotErrorvehiclenomorrek: state.IsNotErrorvehiclenomorrek,
    IsNotErrorvehicletotalsuminsured: state.IsNotErrorvehicletotalsuminsured,

    // Policy delivery
    IsNotErrorpolicysentto: state.IsNotErrorpolicysentto,
    IsNotErrorpolicyname: state.IsNotErrorpolicyname,
    IsNotErrorpolicyaddress: state.IsNotErrorpolicyname,
    IsNotErrorpolicykodepos: state.IsNotErrorpolicykodepos,

    // Survey schedule
    IsNotErrorsurveykota: state.IsNotErrorsurveykota,
    IsNotErrorsurveylokasi: state.IsNotErrorsurveylokasi,
    IsNotErrorsurveyalamat: state.IsNotErrorsurveyalamat,
    IsNotErrorsurveykodepos: state.IsNotErrorsurveykodepos,
    IsNotErrorsurveytanggal: state.IsNotErrorsurveytanggal,
    IsNotErrorsurveywaktu: state.IsNotErrorsurveywaktu,

    // Image Mandatory
    IsNotErrorKTP: state.IsNotErrorKTP,
    IsNotErrorSTNK: state.IsNotErrorSTNK,
    IsNotErrorBSTB: state.IsNotErrorBSTB,
    IsNotErrorDOCREP: state.IsNotErrorDOCREP,
    IsNotErrorNPWP: state.IsNotErrorNPWP,
    IsNotErrorSIUP: state.IsNotErrorSIUP,
    IsNotErrorBUKTIBAYAR: state.IsNotErrorBUKTIBAYAR,
    IsNotErrorBUKTIBAYAR2: state.IsNotErrorBUKTIBAYAR2,
    IsNotErrorBUKTIBAYAR3: state.IsNotErrorBUKTIBAYAR3,
    IsNotErrorBUKTIBAYAR4: state.IsNotErrorBUKTIBAYAR4,
    IsNotErrorBUKTIBAYAR5: state.IsNotErrorBUKTIBAYAR5,
    IsNotErrorSPPAKB: state.IsNotErrorSPPAKB,
    IsNotErrorFAKTUR: state.IsNotErrorFAKTUR,
    IsNotErrorKONFIRMASICUST: state.IsNotErrorKONFIRMASICUST,
    IsNotErrorDOCNSA1: state.IsNotErrorDOCNSA1,
    IsNotErrorDOCNSA2: state.IsNotErrorDOCNSA2,
    IsNotErrorDOCNSA3: state.IsNotErrorDOCNSA3,
    IsNotErrorDOCNSA4: state.IsNotErrorDOCNSA4,
    IsNotErrorDOCNSA5: state.IsNotErrorDOCNSA5,

    // Premi
    IsNotErrorTPLPremi: state.IsNotErrorTPLPremi,
    IsNotErrorSRCCPremi: state.IsNotErrorSRCCPremi,
    IsNotErrorFLDPremi: state.IsNotErrorFLDPremi,
    IsNotErrorETVPremi: state.IsNotErrorETVPremi,
    IsNotErrorTSPremi: state.IsNotErrorTSPremi,
    IsNotErrorPADriverPremi: state.IsNotErrorPADriverPremi,
    IsNotErrorPAPassangerPremi: state.IsNotErrorPAPassangerPremi,
    IsNotErrorAccessPremi: state.IsNotErrorAccessPremi,

    // Document NSA Approval
    IsNotErrordocumentsremarkstosa: state.IsNotErrordocumentsremarkstosa
  };

  return ErrorFlagMandatory;
};

export const isSendToSA = (FollowUpStatus, FollowUpInfo) => {
  // send to sa
  if (FollowUpStatus == 2 && FollowUpInfo == 61) {
    return true;
  }
  return false;
};

export const isSendToSurveyor = (FollowUpStatus, FollowUpInfo) => {
  // send to surveyor
  if (FollowUpStatus == 2 && FollowUpInfo == 58) {
    return true;
  }
  return false;
};

export const isNewData = (FollowUpStatus, FollowUpInfo) => {
  // data baru input
  if (FollowUpStatus == 1) {
    return true;
  }
  return false;
};

export const isBackToAO = (FollowUpStatus, FollowUpInfo) => {
  if (FollowUpStatus == 2 && FollowUpInfo == 60) {
    return true;
  }
  return false;
};

// follow up nsa approval
export const isNSAApproval = FollowUpInfo => {
  if (FollowUpInfo == 46) {
    return true;
  }
  return false;
};

export const isPolicyApproved = FollowUpInfo => {
  if (FollowUpInfo == 50) {
    return true;
  }
  return false;
};

export const isPolicyDelivered = FollowUpInfo => {
  if (FollowUpInfo == 51) {
    return true;
  }
  return false;
};

export const isPolicyReceived = FollowUpInfo => {
  if (FollowUpInfo == 52) {
    return true;
  }
  return false;
};

export const isPolicyCancelled = FollowUpInfo => {
  if (FollowUpInfo == 92) {
    return true;
  }
  return false;
};

export const isPolicyEndorse = FollowUpInfo => {
  if (FollowUpInfo == 93) {
    return true;
  }
  return false;
};

export const isPolicyApprovedDocRep = FollowUpInfo => {
  if (FollowUpInfo == 53) {
    return true;
  }
  return false;
};

export const isPolicyDeliveredDocRep = FollowUpInfo => {
  if (FollowUpInfo == 54) {
    return true;
  }
  return false;
};

export const isPolicyReceivedDocRep = FollowUpInfo => {
  if (FollowUpInfo == 55) {
    return true;
  }
  return false;
};

export const isNeedApproval = FollowUpInfo => {
  if (FollowUpInfo == 62) {
    return true;
  }
  return false;
};

export const isFollowUpSurveyResult = (FollowUpStatus, FollowUpInfo) => {
  if (FollowUpStatus == 2 && FollowUpInfo == 59) {
    return true;
  }
  return false;
};

export const isFollowUpPayment = (FollowUpStatus, FollowUpInfo) => {
  if (FollowUpStatus == 2 && FollowUpInfo == 57) {
    return true;
  }
  return false;
};

export const isNotDeal = FollowUpStatus => {
  if (FollowUpStatus == 5) {
    return true;
  }
  return false;
};

export const isOrderRejected = FollowUpStatus => {
  if (FollowUpStatus == 14) {
    return true;
  }
  return false;
};

export const disableChecklistPayerCompany = (
  IsPaid,
  FollowUpStatus,
  FollowUpInfo,
  surveyNo
) => {
  let res = false;
  if (!isNewData(FollowUpStatus)) {
    res = true;
  }

  if (IsPaid) {
    res = true;
  }

  if (disabledVehicleData(FollowUpStatus, FollowUpInfo, surveyNo)) {
    res = true;
  }

  if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(surveyNo)) {
    res = true;
  }

  if (isFollowUpSurveyResult(FollowUpStatus, FollowUpInfo)) {
    res = true;
  }

  return res;
};

export const disabledVehicleData = (FollowUpStatus, FollowUpInfo, surveyNo) => {
  let temp = false;

  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // send to sa
  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // if (isFollowUpSurveyResult()) {
  //   temp = true;
  // }

  // if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(surveyNo)) {
  //   temp = true;
  // }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledEditTaskDetail = (FollowUpStatus, FollowUpInfo) => {
  // send to surveyor
  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    return true;
  }

  // send to sa
  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    return true;
  }

  return false;
};

export const disabledSurveySchedule = (
  FollowUpStatus,
  FollowUpInfo,
  surveyNo
) => {
  let temp = false;

  if (disabledEditTaskDetail(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpSurveyResult(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(surveyNo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const isDontHitUpdateTaskdetail = (FollowUpStatus, FollowUpInfo) => {
  let temp = false;

  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }
  // if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
  //   temp = true;
  // }
  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  return temp;
};

export const disabledPolisTerbitSebelumBayar = (
  FollowUpStatus,
  FollowUpInfo,
  surveyNo
) => {
  let temp = false;

  if (disabledVehicleData(FollowUpStatus, FollowUpInfo, surveyNo)) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  return temp;
};

export const disabledExtendedCover = (
  FollowUpStatus,
  FollowUpInfo,
  SurveyNo
) => {
  let temp = false;
  if (disabledEditTaskDetail(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpSurveyResult(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(SurveyNo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledFieldRenewal = (fieldName, state) => {
  let disabled = false;
  if (state.IsRenewal) {
    if (!Util.isNullOrEmpty(state[fieldName + "temp"])) {
      disabled = true;
    }
  }

  return disabled;
};

export const disableActionButton = (isMVGodig, MVGodidSalesOfficer) => {
  let res = false;

  if (isMVGodig) {
    if (
      !Util.stringEquals(
        MVGodidSalesOfficer,
        JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
      )
    ) {
      res = true;
    }
  }

  return res;
};

export const disabledProductType = (state) => {
  let temp = false;

  if (
    disabledFieldRenewal("vehicleInsuranceType", state) ||
    isBackToAO(state.FollowUpStatus, state.FollowUpInfo)
  ) {
    temp = true;
  }

  if(isNSAApproval(state.FollowUpInfo) && !Util.isNullOrEmpty(state.PolicyOrderNo)){
    temp = true;
  }

  if (state.IsPaid) {
    temp = true;
  }

  return temp;
};

export const disabledProductCode = (state = {
  FollowUpStatus: 1,
  FollowUpInfo: 1,
  fieldName: "vehicleproductcode",
  IsPaid: 0
}) => {
  let temp = false;

  // if (
  //   (disabledFieldRenewal("vehicleproductcode", state) &&
  //     !isNSAApproval(state.FollowUpInfo)) ||
  //   isBackToAO(state.FollowUpStatus, state.FollowUpInfo)
  // ) {
  //   temp = true;
  // }

  if (
    disabledFieldRenewal("vehicleproductcode", state) ||
    isBackToAO(state.FollowUpStatus, state.FollowUpInfo)
  ) {
    temp = true;
  }

  if(isNSAApproval(state.FollowUpInfo) && !Util.isNullOrEmpty(state.PolicyOrderNo)){
    temp = true;
  }

  if (state.IsPaid) {
    temp = true;
  }

  return temp;
};

export const isDealNeedDocRep = (FollowUpInfo) => {
  let temp = false;

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }
  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  return temp;
};

export const disabledSegmentCode = (FollowUpStatus, FollowUpInfo, state) => {
  let temp = false;

  if(isBackToAO(FollowUpStatus, FollowUpInfo)){
    temp = true;
  }

  if(isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(state.PolicyOrderNo)){
    temp = true;
  }

  if(disabledFieldRenewal("vehiclesegmentcode", state)){ // Penjagaan Sprint 3 Otosales Scrum
    temp = true;
  }

  return temp;
}

export const disabledPersonalData = (FollowUpStatus, FollowUpInfo, surveyNo) => {
  let temp = false;

  if (
    disabledEditTaskDetail(
      FollowUpStatus,
      FollowUpInfo
    )
  ) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // if (isFollowUpSurveyResult()) {
  //   temp = true;
  // }

  if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(surveyNo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledPersonalDocuments = (FollowUpStatus, FollowUpInfo) => {
  let temp = false;

  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // send to sa
  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledCompanyDocuments = (FollowUpStatus, FollowUpInfo) => {
  let temp = false;

  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // send to sa
  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledDocuments = (FollowUpStatus, FollowUpInfo) => {
  let temp = false;

  if (isSendToSurveyor(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // send to sa
  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledCompanyData = (FollowUpStatus, FollowUpInfo, SurveyNo) => {
  let temp = false;

  if (
    disabledEditTaskDetail(
      FollowUpStatus,
      FollowUpInfo
    )
  ) {
    temp = true;
  }

  if (isBackToAO(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  // if (isFollowUpSurveyResult()) {
  //   temp = true;
  // }

  if (isNSAApproval(FollowUpInfo) && !Util.isNullOrEmpty(SurveyNo)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const disabledPolicyDelivery = (FollowUpStatus, FollowUpInfo, IsRenewal) => {
  let temp = false;

  if (isSendToSA(FollowUpStatus, FollowUpInfo)) {
    temp = true;
  }

  if (IsRenewal) {
    if (isFollowUpPayment(FollowUpStatus, FollowUpInfo)) {
      temp = true;
    }
  }

  if (isNeedApproval(FollowUpInfo)) {
    temp = true;
  }

  if (isNotDeal(FollowUpStatus)) {
    temp = true;
  }

  if (isOrderRejected(FollowUpStatus)) {
    temp = true;
  }

  if (isPolicyApproved(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDelivered(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceived(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyApprovedDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyDeliveredDocRep(FollowUpInfo)) {
    temp = true;
  }

  if (isPolicyReceivedDocRep(FollowUpInfo)) {
    temp = true;
  }

  // 0063 URF 2020 Sprint 4 Otosales
  if(isPolicyCancelled(FollowUpInfo)){
    temp = true;
  }
  if(isPolicyEndorse(FollowUpInfo)){
    temp = true;
  }
  // End Of 0063 URF 2020 Sprint 4 Otosales

  return temp;
};

export const setLocalStorageFlagError = (state) => {
  secureStorage.setItem(
    "ErrorFlagMandatory",
    JSON.stringify(setModelErrorFlagMandatory(state))
  );
};

export const validationPersonalDocument = (state, setState, setLocalStorageFlagError) => {
  let validation = true;
  let messages = "";

  if (!state.IsRenewal) {
    if (Util.isNullOrEmpty(state.KTP)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Photo KTP/KITAS mandatory"
        : messages;
      setState(
        {
          IsNotErrorKTP: true
        },
        () => {
          setLocalStorageFlagError();
        }
      );
    }

    if (state.vehicleusedcar) {
      if (Util.isNullOrEmpty(state.STNK)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "Photo STNK mandatory"
          : messages;
        setState(
          {
            IsNotErrorSTNK: true
          },
          () => {
            setLocalStorageFlagError();
          }
        );
      }
    }

    if (!state.vehicleusedcar) {
      if (Util.isNullOrEmpty(state.BSTB)) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "Photo BSTB mandatory"
          : messages;
        setState(
          {
            IsNotErrorBSTB: true
          },
          () => {
            setLocalStorageFlagError();
          }
        );
      }
    }
  }

  if (Util.isNullOrEmpty(state.KONFIRMASICUST)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Photo Konfirmasi Customer mandatory"
      : messages;
    setState(
      {
        IsNotErrorKONFIRMASICUST: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  let returnvalidation = { validation: validation, messages: messages };
  return returnvalidation;
};

export const validationPersonalData = (state, setState, setLocalStorageFlagError) => {
  let validation = true;
  let messages = "";

  if (Util.isNullOrEmpty(state.personalname)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Personal name mandatory"
      : messages;
    setState(
      {
        IsNotErrorpersonalname: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personaltanggallahir)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Tanggal lahir mandatory"
      : messages;
    setState(
      {
        IsNotErrorpersonaltanggallahir: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personalJK)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Jenis kelamin mandatory"
      : messages;
    setState(
      {
        IsNotErrorpersonalJK: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personalalamat)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages) ? "Alamat mandatory" : messages;
    setState(
      {
        IsNotErrorpersonalalamat: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personalkodepos)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages) ? "Kode pos mandatory" : messages;
    setState(
      {
        IsNotErrorpersonalkodepos: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  let IsNotErrorpersonalemail = false;

  if (Util.isNullOrEmpty(state.personalemail)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Email personal mandatory"
      : messages;
    IsNotErrorpersonalemail = true;
  }

  if (
    !Util.isNullOrEmpty(state.personalemail) &&
    !Util.validateEmail(state.personalemail)
  ) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Email personal is not valid"
      : messages;
    IsNotErrorpersonalemail = true;
  }

  if(IsNotErrorpersonalemail == true){
    setState(
      {
        IsNotErrorpersonalemail: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personalhp)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages) ? "Nomor HP mandatory" : messages;
    setState(
      {
        IsNotErrorpersonalhp: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.personalnomoridentitas)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Nomor Identitas mandatory"
      : messages;
    setState(
      {
        IsNotErrorpersonalnomoridentitas: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (state.personalneeddocrep) {
    if (Util.isNullOrEmpty(state.personalamountrep)) {
      validation = validation ? false : validation;
      messages = Util.isNullOrEmpty(messages)
        ? "Amount Rep mandatory"
        : messages;
      setState(
        {
          IsNotErrorpersonalamountrep: true
        },
        () => {
          setLocalStorageFlagError();
        }
      );
    }
  }

  let returnvalidation = { validation: validation, messages: messages };
  return returnvalidation;
};

export const validationCompanyData = (state, setState, setLocalStorageFlagError) => {
  let validation = true;
  let messages = "";

  if (Util.isNullOrEmpty(state.companyname)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company name mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanyname: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companynpwpnumber)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company NPWP Number mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanynpwpnumber: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companynpwpdata)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company NPWP Date mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanynpwpdata: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companynpwpaddress)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company NPWP Address mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanynpwpaddress: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companypostalcode)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company Postal Code mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanypostalcode: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companypdcname)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company PIC Name mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanypdcname: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  if (Util.isNullOrEmpty(state.companypichp)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company PIC Handphone mandatory"
      : messages;
    setState(
      {
        IsNotErrorcompanypichp: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  let IsNotErrorcompanyemail = false;

  if (Util.isNullOrEmpty(state.companyemail)) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company PIC Email mandatory"
      : messages;
    IsNotErrorcompanyemail = true;
  }

  if (
    !Util.isNullOrEmpty(state.companyemail) &&
    !Util.validateEmail(state.companyemail)
  ) {
    validation = validation ? false : validation;
    messages = Util.isNullOrEmpty(messages)
      ? "Company PIC Email is not valid"
      : messages;
    IsNotErrorcompanyemail = true;
  }

  if(IsNotErrorcompanyemail == true){
    setState(
      {
        IsNotErrorcompanyemail: true
      },
      () => {
        setLocalStorageFlagError();
      }
    );
  }

  let returnvalidation = { validation: validation, messages: messages };
  return returnvalidation;
};

export const disableEditTaskDetailbyRole = (state) => {
  let temp = false;

  let listUserDisable = ["CCO", "CSO"]; // edit other role task detail

  let account = JSON.parse(ACCOUNTDATA);
  Log.debugStr("SALESOFFICERID => " + state.SalesOfficerID);

  if (!Util.isNullOrEmpty(account)) {
    if (
      !Util.stringArrayContains(account.UserInfo.User.Role, listUserDisable)
    ) {
      if (
        !Util.stringEquals(
          account.UserInfo.User.SalesOfficerID,
          state.SalesOfficerID
        )
      ) {
        let iscso = Util.isNullOrEmpty(account.UserInfo.User.IsCSO)
          ? 0
          : account.UserInfo.User.IsCSO;
        if (iscso == 0) {
          temp = true;
        }
      }
    }

    if (
      Util.stringArrayContains(Util.valueTrim(account.UserInfo.User.Role), [
        "REGIONALMGR",
        "NATIONALMGR"
      ])
    ) {
      temp = true;
    }
  }

  if (state.isMVGodig) {
    temp = false;
  }

  return temp;
};

export const getVehiclePrice = (
  vehiclecode,
  Year,
  citycode,
  setState,
  callback = () => {}
) => {
  if (
    Util.isNullOrEmpty(vehiclecode) ||
    Util.isNullOrEmpty(Year) ||
    Util.isNullOrEmpty(citycode)
  ) {
    callback();
    setState({
      isLoading: false
    });
    return;
  }
  DataSource.getvehicleprice(vehiclecode, Year, citycode)
    .then(res => res.json())
    .then(jsn => {
      setState(
        {
          vehicletotalsuminsured: jsn.data
        },
        () => {
          callback();
        }
      );
      setState({
        isLoading: false
      });
    })
    .catch(error => {
      Log.error("parsing Failed", error);
      setState({
        isLoading: false
      });
      Util.showToast("❗ Error - " + error, "WARNING");
    });
};

export const getTPLSI = (ProductCode, callback = () => {}, setState) => {
  setState({ isLoading: true });
  DataSource.getTPLSIList(ProductCode)
    .then(res => res.json())
    .then(jsn => {
      setState({ dataTPLSIall: jsn.data }, () => {
      });
      return jsn.data.map(data => ({
        value: `${data.CoverageId}`,
        label: `${Util.formatMoney(data.TSI, 0)}`
      }));
    })
    .then(datamapping => {
      setState(
        {
          dataTPLSI: datamapping,
          isLoading: false
        },
        () => {
          callback();
        }
      );
    })
    .catch(error => {
      Log.error("parsing Failed", error);
      setState({
        isLoading: false
      });
      Util.showToast("❗ Error - " + error, "WARNING");
    });
};

export const getPhoneList = (
  OrderNo,
  showNotification,
  callback = () => {},
  state,
  setState
) => {
  setState({ isLoading: true });
  DataSource.getPhoneList(OrderNo)
    .then(res => res.json())
    .then(jsn => {
      let dataphonelist = jsn.data.map(data => ({
        value: data.NoHp,
        label: Util.maskingPhoneNumberv2(Util.clearPhoneNo(data.NoHp))
      }));
      // if(jsn.data.length > 0){
      setState(
        {
          dataphonelist,
          dataGetPhoneList: jsn.data
        },
        () => {
          if (
            state.IsRenewal &&
            showNotification &&
            [...state.dataphonelist].filter(data =>
              Util.detectPrefix(
                data.value,
                state.Parameterized.mobilePrefix
              )
            ).length == 0
          ) {
            Util.showToast(
              "Tidak terdapat nomor yang valid. Harap melakukan penambahan data nomor telepon.",
              "WARNING"
            );
          }
          callback();
        }
      );
      setState({
        isLoading: false
      });
    })
    .catch(error => {
      Log.error("parsing Failed", error);
      setState({
        isLoading: false
      });
      Util.showToast("❗ Please submit data first" + error, "WARNING");
    });
};

export const getFollowUpStatus = (
  FollowUpStatus,
  FollowUpInfo,
  IsRenewal,
  state,
  setState,
  callback = () => {}
) => {
  setState({ isLoading: true });
  DataSource.getFollowUpStatus(
    FollowUpStatus,
    FollowUpInfo,
    state.FollowUpNo,
    IsRenewal ? 1 : 0
  )
    .then(res => res.json())
    .then(jsn => {
      setState({
        isLoading: false
      });
      if (jsn.status) {
        let followupstatusvisible = jsn.data.map(data => {
          return data.StatusCode;
        });

        setState({
          followupstatusvisible
        });
      }
    })
    .catch(error => {
      Log.error("parsing Failed", error);
      setState({
        isLoading: false
      });
      Util.hittingAPIError(error, () => {
        // searchDataHitApi();
      });
    });
};
