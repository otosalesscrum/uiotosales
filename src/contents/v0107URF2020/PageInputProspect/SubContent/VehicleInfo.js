import React from 'react'
import {
  OtosalesSelectFullview,
  OtosalesSelectFullviewv2,
  Util
} from "../../../../otosalescomponents";

export default function VehicleInfo(props) {
    return (
        <React.Fragment>
            <form onSubmit={props.functions.onSubmitVehicle} autoComplete="off">
                    <div
                      className={
                        props.state.isErrVehCode
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <input
                        type="checkbox"
                        style={{ marginRight: "10px" }}
                        onChange={props.functions.onChangeFunction}
                        checked={props.state.VehicleUsedCar}
                        name="VehicleUsedCar"
                        disabled={
                          props.state.VehicleYear <
                          Util.convertDate().getFullYear()
                            ? true
                            : false
                        }
                      />
                      <label>USED CAR</label>
                    </div>
                    <div
                      className={
                        props.state.isErrSearch
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>VEHICLE DETAILS *</label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          minHeight={40}
                          name="searchVehicle"
                          onSearchFunction={props.functions.getVehicle}
                          value={props.state.searchVehicle}
                          selectOption={props.state.searchVehicle}
                          onOptionsChange={props.functions.onOptionSearch}
                          options={props.state.datasearch}
                          placeholder="Pilih Kendaraan"
                        />
                      </div>
                    </div>

                    <div
                      className={
                        props.state.isErrVehUsage
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>USAGE * </label>
                      <div className="input-group">
                        <OtosalesSelectFullviewv2
                          error={props.state.isErrVehUsage}
                          value={props.state.VehicleUsage}
                          selectOption={props.state.VehicleUsage}
                          onOptionsChange={props.functions.onOptionVehUsageChange}
                          options={props.state.datavehicleusage}
                          placeholder="Select usage"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        props.state.isErrVehRegion
                          ? "form-group has-error"
                          : "form-group"
                      }
                    >
                      <label>REGION * </label>
                      <div className="input-group">
                        <OtosalesSelectFullview
                          error={props.state.isErrVehRegion}
                          value={props.state.VehicleRegion}
                          selectOption={props.state.VehicleRegion}
                          onOptionsChange={props.functions.onOptionVehRegionChange}
                          options={props.state.datavehicleregion}
                          placeholder="Select region"
                        />
                      </div>
                    </div>
                    <div
                      className={
                        "form-group" +
                        (props.state.isErrVehRegistrationNumber
                          ? " has-error"
                          : "")
                      }
                    >
                      <label>REGISTRATION NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          pattern="^[a-zA-Z0-9]+$"
                          onChange={props.functions.onChangeFunction}
                          value={Util.valueTrim(
                            props.state.VehicleRegistrationNumber
                          ).toUpperCase()}
                          name="VehicleRegistrationNumber"
                          className="form-control "
                          maxLength="9"
                          placeholder="Type registration number"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>CHASIS NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          pattern="^[a-zA-Z0-9]+$"
                          value={Util.valueTrim(
                            props.state.VehicleChasisNumber
                          ).toUpperCase()}
                          name="VehicleChasisNumber"
                          className="form-control "
                          placeholder="Type chasis number"
                          maxLength="30"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>ENGINE NUMBER </label>
                      <div className="input-group">
                        <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          pattern="^[a-zA-Z0-9]+$"
                          value={Util.valueTrim(
                            props.state.VehicleEngineNumber
                          ).toUpperCase()}
                          name="VehicleEngineNumber"
                          className="form-control "
                          placeholder="Type engine number"
                          maxLength="30"
                        />
                      </div>
                    </div>

                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right">
                          Next <i className="fa fa-chevron-circle-right" />
                        </button>
                      </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                  </form>
        </React.Fragment>
    )
}
