import React from 'react'
import HexCarInfo from "../../../../assets/hex_car_info.png";
import HexDealerInfo from "../../../../assets/hex_dealer_info.png";
import HexPolicyDetails from "../../../../assets/hex_policy_details.png";
import HexPolicyInfo from "../../../../assets/hex_policy_info.png";
import HexProspectInfo from "../../../../assets/hex_prospek_info.png";

export default function SummaryInfo(props) {
    return (
        <div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img
                            src={HexProspectInfo}
                            style={{ width: "100%" }}
                          />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>
                            <strong>{props.state.SumName}</strong>
                          </span>
                          <p className="list-group-item-text">
                            {props.state.SumPhone1}
                          </p>
                          <p className="list-group-item-text">
                            {props.state.SumEmail1}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexCarInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{props.state.SumVehicle}</span>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexPolicyInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{props.state.SumCoverageType}</span>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img
                            src={HexPolicyDetails}
                            style={{ width: "100%" }}
                          />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>Total Premi </span>
                          <p className="">
                            <strong style={{ color: "#D59F00" }}>
                              {props.functions.formatMoney(props.state.SumTotalPremium, 0)}
                            </strong>
                          </p>
                          <span>TSI CASCO</span>
                          <p className="">
                            {props.functions.formatMoney(props.state.SumSumInsured, 0)}
                          </p>
                          <span>TSI Accesory </span>
                          <p className="">
                            {props.functions.formatMoney(props.state.SumAccessSI, 0)}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="well">
                      <div
                        className="row"
                        style={{ alignItems: "center", display: "flex" }}
                      >
                        <div className="col-xs-3 col-md-1">
                          <img src={HexDealerInfo} style={{ width: "100%" }} />
                        </div>
                        <div className="col-xs-9 col-md-11">
                          <span>{props.state.SumSalesmanDesc}</span>
                        </div>
                      </div>
                    </div>
                  </div>
    )
}
