import { Util } from "../../../otosalescomponents"
import {
    API_URL,
    API_VERSION,
    API_VERSION2,
    API_VERSION_2,
    ACCOUNTDATA,
    HEADER_API,
    API_VERSION_0219URF2019,
    API_VERSION_0063URF2020,
    API_VERSION_0090URF2020,
    API_VERSION_0107URF2020
  } from "../../../config";

export const getOrderDetailHeader = async (followupno, branchcode, channelsource) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailHeader/`,
        "POST",
        HEADER_API,
        {
            followupno,
            branchcode,
            channelsource
        }
    )
  }
  
  export const getOrderDetailCoverage = async (orderno) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getOrderDetailCoverage/`,
        "POST",
        HEADER_API,
        {
            orderno
        }
    )
  }
  
  export const SendQuotationEmail = async (Email, OrderNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0107URF2020}/DataReact/SendQuotationEmail/`,
        "POST",
        HEADER_API,
        {
            Email,
            OrderNo
        }
    )
  }
  
  export const SendQuotationSMS = async (Phone, OrderNo) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/SendQuotationSMS/`,
        "POST",
        HEADER_API,
        {
            Phone,
            OrderNo
        }
    )
  }
  
  export const SaveProspect = async (paramReq) => {
    return Util.fetchAPIAdditional(
      // `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/SaveProspect/`,
      // `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/SaveProspect/`,
      `${API_URL + "" + API_VERSION_0107URF2020}/DataReact/SaveProspect/`,
        "POST",
        HEADER_API,
        paramReq
      )
  }
  
  export const getDealerName = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getDealerName/`,
        "POST",
        HEADER_API
    )
  }
  
  export const getVehicleBrand = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleBrand/`,
        "POST",
        HEADER_API
    )
  }
  
  export const getVehicleTypeall = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleTypeall/`,
        "POST",
        HEADER_API
    )
  }
  
  export const getVehicleType = async (brandCode, year) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleType/`,
        "POST",
        HEADER_API,
        {
            brandCode,
            year
        }
    )
  }
  
  export const getVehicleUsage = async (isMvGodig) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/getVehicleUsage/`,
        "POST",
        HEADER_API,
        {
          isMvGodig
        }
      )
  }
  
  export const getVehicleRegion = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleRegion/`,
        "POST",
        HEADER_API
    )
  }
  
  export const getVehiclePrice = async (vehiclecode, year, citycode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehiclePrice/`,
        "POST",
        HEADER_API,
        {
            vehiclecode,
            year,
            citycode
        }
    )
  }
  
  export const getProductType = async (Channel, salesofficerid, isMvGodig) => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0063URF2020}/DataReact/getProductType/`,
        "POST",
        HEADER_API,
        {
          Channel,
          salesofficerid,
          isMvGodig
        }
      )
  }
  
  export const getBasicCover = async (Channel, salesofficerid, isBasic, vyear, isMvGodig, productCode = "") => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0090URF2020}/DataReact/getBasicCover/`,
        "POST",
        HEADER_API,
        {
          Channel,
          salesofficerid,
          isBasic,
          vyear,
          isMvGodig,
          productCode
        }
      )
  }
  
  export const GetVehicle = async (moduleName, search, isMvGodig, signal) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/GetVehicle/`,
        "POST",
        HEADER_API,
        {
          moduleName,
          search,
          isMvGodig
        },
        {
          signal
        }
      )
  }
  
  export const ResetVehicleCode = async (vehicleCode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION2}/DataReact/ResetVehicleCode/`,
        "POST",
        HEADER_API,
        {
            vehicleCode
        }
    )
  }
  
  export const getVehicleYear = async (brandCode) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleYear/`,
        "POST",
        HEADER_API,
        {
            brandCode
        }
    )
  }
  
  export const getVehicleCode = async (brandCode, year, modelCode, series) => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getVehicleCode/`,
        "POST",
        HEADER_API,
        {
            brandCode,
            year,
            modelCode,
            series
        }
    )
  }
  
  export const getProductCode = async (insurancetype, salesofficerid, isMvGodig, isNew, searchParam = "") => {
    return Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0107URF2020}/DataReact/getProductCode/`,
        "POST",
        HEADER_API,
        {
          insurancetype,
          salesofficerid,
          isMvGodig,
          isNew,
          searchParam
        }
      )
  }
  
  export const getAccessoriesList = async () => {
    return Util.fetchAPIAdditional(
        `${API_URL + "" + API_VERSION_2}/DataReact/getAccessoriesList`,
        "POST",
        HEADER_API
    )
  }
  
  export const usdfpsdjnkfliebsdf2sdf = async () => {
    return 
  }
  
  export const usdfpsdjnkfldf1iebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkfli234ebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkdg123fliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkfdg45liebsdf = async () => {
    return 
  }
  
  export const usdf345dgpsdjnkfliebsdf = async () => {
    return 
  }
  
  export const usdfpsdj134nkfliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnk756fliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnk123dfliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnsf12kfliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnksdfs123fliebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkflsdf1231iebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkflsdfs1231iebsdf = async () => {
    return 
  }
  
  export const usdfpsdjnkfliebsdf123sdf = async () => {
    return 
  }