import React from "react";
import { OtosalesSelectFullviewv2, Util } from "../../../../otosalescomponents";
import HexCarInfo from "../../../../assets/hex_car_info.png";
import HexDealerInfo from "../../../../assets/hex_dealer_info.png";
import HexPolicyDetails from "../../../../assets/hex_policy_details.png";
import HexPolicyInfo from "../../../../assets/hex_policy_info.png";
import HexProspectInfo from "../../../../assets/hex_prospek_info.png";
import NumberFormat from "react-number-format";

export default function SummaryDetails(props) {
  return (
    <React.Fragment>
      <form onSubmit={props.functions.onSubmitProspect} autoComplete="off">
        <div>
          <div className="well">
            <div
              className="row"
              style={{ alignItems: "center", display: "flex" }}
            >
              <div className="col-xs-3 col-md-1">
                <img src={HexCarInfo} style={{ width: "100%" }} />
              </div>
              <div className="col-xs-9 col-md-11">
                <span>
                  {props.state.activetab == "Summary Details" &&
                    // props.state.VehicleBrand != "" &&
                    // props.state.VehicleType != "" &&
                    // props.state.vehicleModelCode != "" &&
                    // props.state.datavehiclebrand.length > 0 &&
                    // props.state.datavehicletype.length > 0 &&
                    // props.state.datacoverbasiccover.length > 0 &&
                    // [...props.state.datavehiclebrand].filter(
                    //   data => data.value == props.state.VehicleBrand
                    // )[0].label +
                    //   " " +
                    //   props.state.datavehicletype.filter(
                    //     data => data.value == props.state.vehicleModelCode
                    //   )[0].label +
                    //   " " +
                    //   props.state.VehicleSeries +
                    //   " " +
                    //   props.state.VehicleYear
                    props.state.VehicleDetailsTemp != "" &&
                    props.state.VehicleDetailsTemp}
                </span>
              </div>
            </div>
          </div>
          <div className="well">
            <div
              className="row"
              style={{ alignItems: "center", display: "flex" }}
            >
              <div className="col-xs-3 col-md-1">
                <img src={HexPolicyInfo} style={{ width: "100%" }} />
              </div>
              <div className="col-xs-9 col-md-11">
                <span>
                  {props.state.CoverBasicCover != "" &&
                    props.state.datacoverbasiccover.length > 0 &&
                    props.state.datacoverbasiccover.filter(
                      data => data.value == props.state.CoverBasicCover
                    )[0].label}
                </span>
              </div>
            </div>
          </div>
          <div className="well">
            <div
              className="row"
              style={{ alignItems: "center", display: "flex" }}
            >
              <div className="col-xs-3 col-md-1">
                <img src={HexPolicyDetails} style={{ width: "100%" }} />
              </div>
              <div className="col-xs-9 col-md-11">
                <span>Total Premi </span>
                <p className="">
                  <strong style={{ color: "#D59F00" }}>
                    {props.functions.formatMoney(props.state.TotalPremi)}
                  </strong>
                </p>
                <span>TSI CASCO</span>
                <p className="">{props.functions.formatMoney(props.state.CovSumInsured)}</p>
                <span>TSI Accesory </span>
                <p className="">{props.functions.formatMoney(props.state.ACCESSCOVER)}</p>
                {/* <Link
                            onClick={() => {
                              secureStorage.setItem(
                                "premiumsimulation",
                                JSON.stringify(props.state)
                              );
                              secureStorage.setItem(
                                "flagquotationdetailspremiumsimulation",
                                true
                              );
                            }}
                            to="/premiumsimulation-quotationdetails"
                          >
                            {" "}
                            View Quotation Details
                          </Link> */}
              </div>
            </div>
          </div>

          {!props.state.isMvGodig && (
            <div
              className={
                props.state.isErrCompany ? "form-group has-error" : "form-group"
              }
            >
              <input
                type="checkbox"
                style={{ marginRight: "10px" }}
                onChange={props.functions.onChangeFunction}
                checked={props.state.isCompany}
                name="isCompany"
              />
              <label>Company</label>
            </div>
          )}

          <div
            className={
              props.state.isErrProsName ? "form-group has-error" : "form-group"
            }
          >
            <label>PIC / CUSTOMER NAME * </label>
            <div className="input-group">
              {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
              <input
                type="text"
                style={{ marginBottom: "0px" }}
                pattern="^[a-zA-Z0-9 ,-.\/]+$"
                onChange={props.functions.onChangeFunction}
                value={props.state.ProspectName}
                name="ProspectName"
                className="form-control "
                maxLength="50"
                // readOnly={true}
              />
            </div>
          </div>
          <div
            className={
              props.state.isErrPhone1 ? "form-group has-error" : "form-group"
            }
          >
            <label>PIC / CUSTOMER PHONE NUMBER * </label>
            <div className="input-group">
              {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
              {/* <input
                          type="number"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          value={props.state.Phone1}
                          name="Phone1"
                          className="form-control "
                          // readOnly={true}
                        /> */}
              <NumberFormat
                className="form-control"
                onValueChange={value => {
                  props.functions.setState({
                    ProspectPhone1: value.value,
                    Phone1: value.value
                  });
                  if (props.state.Phone1.length > 13)
                    props.state.isErrPhone1 = true;
                  else if (props.state.Phone1.length < 9)
                    props.state.isErrPhone1 = true;
                  else props.state.isErrPhone1 = false;
                }}
                value={props.state.ProspectPhone1}
                format="#### #### #### ####"
                prefix={""}
                maxLength="50"
                isNumericString={true}
              />
            </div>
          </div>

          <div
            className={
              props.state.isErrEmail ? "form-group has-error" : "form-group"
            }
          >
            <label>PIC / CUSTOMER EMAIL ADDRESS *</label>
            <div className="input-group">
              {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
              <input
                type="text"
                style={{ marginBottom: "0px" }}
                onChange={props.functions.onChangeFunction}
                value={props.state.ProspectEmail}
                name="ProspectEmail"
                className="form-control "
                maxLength="50"
                // readOnly={true}
              />
            </div>
          </div>

          <div
            className={
              props.state.isErrVehDetails ? "form-group has-error" : "form-group"
            }
          >
            <label>VEHICLE DETAILS *</label>
            <div className="input-group">
              {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
              {/* <input
                          type="text"
                          style={{ marginBottom: "0px" }}
                          onChange={props.functions.onChangeFunction}
                          value={props.state.VehicleDetailsTemp}
                          name="VehicleDetailsTemp"
                          className="form-control "
                          placeholder="Type vehicle details"
                          // readOnly={true}
                        /> */}
              <OtosalesSelectFullviewv2
                minHeight={40}
                name="VehicleDetailsTemp"
                onSearchFunction={props.functions.getVehicle}
                value={props.state.VehicleDetailsTemp}
                selectOption={props.state.VehicleDetailsTemp}
                onOptionsChange={props.functions.onChangeVehicleDetails}
                options={props.state.datavehicledetails}
                placeholder="Type Vehicle Details"
              />
            </div>
          </div>

          <div
            className={
              props.state.isErrVehRegistrationNumber
                ? "form-group has-error"
                : "form-group"
            }
          >
            <label>REGISTRATION NUMBER </label>
            <div className="input-group">
              {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
              <input
                type="text"
                style={{ marginBottom: "0px" }}
                pattern="^[a-zA-Z0-9]+$"
                value={Util.valueTrim(
                  props.state.RegistrationNumber
                ).toUpperCase()}
                onChange={props.functions.onChangeFunction}
                name="RegistrationNumber"
                className="form-control "
                placeholder="Type registration number"
                maxLength="9"
                // readOnly={true}
              />
            </div>
          </div>

          <label>CHASIS NUMBER </label>
          <div className="input-group">
            {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
            <input
              type="text"
              style={{ marginBottom: "0px" }}
              pattern="^[a-zA-Z0-9]+$"
              value={Util.valueTrim(props.state.ChasisNumber).toUpperCase()}
              onChange={props.functions.onChangeFunction}
              name="ChasisNumber"
              className="form-control "
              placeholder="Type chasis number"
              maxLength="30"
              // readOnly={true}
            />
          </div>

          <label>ENGINE NUMBER</label>
          <div className="input-group">
            {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
            <input
              type="text"
              style={{ marginBottom: "0px" }}
              pattern="^[a-zA-Z0-9]+$"
              value={Util.valueTrim(props.state.EngineNumber).toUpperCase()}
              onChange={props.functions.onChangeFunction}
              name="EngineNumber"
              className="form-control "
              placeholder="Type engine number"
              maxLength="30"
              // readOnly={true}
            />
          </div>
        </div>
        <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
      </form>
    </React.Fragment>
  );
}
