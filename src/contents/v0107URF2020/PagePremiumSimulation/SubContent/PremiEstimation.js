import React from 'react'

export default function PremiEstimation(props) {
    return (
        <React.Fragment>
            <form onSubmit={props.functions.onSubmitCover} autoComplete="off">
                    {/* FLAG PREMI ESTIMATION*/}
                    <div className="row">
                      <div className="col-xs-3">
                        <p>Basic Cover</p>
                      </div>
                      <div className="col-xs-9">
                        <p style={{ textAlign: "left" }}>
                          {props.state.activetab == "Premi Estimation" &&
                            props.state.CoverBasicCover != "" &&
                            props.state.datacoverbasiccover.length > 0 &&
                            ": " +
                              props.state.datacoverbasiccover.filter(
                                data => data.value == props.state.CoverBasicCover
                              )[0].label +
                              " "}
                        </p>
                      </div>
                    </div>
                    {(props.state.VehicleDetailsTemp != "" ||
                      props.state.VehicleDetailsTemp != null) && (
                      <div className="row">
                        <div className="col-xs-3">
                          <p>Vehicle</p>
                        </div>
                        <div className="col-xs-9">
                          <p style={{ textAlign: "left" }}>
                            {props.state.activetab == "Premi Estimation" &&
                              props.state.VehicleDetailsTemp != "" &&
                              (": "+ props.state.VehicleDetailsTemp)}
                          </p>
                        </div>
                      </div>
                    )}

                    <div className="row">
                      <div className="col-xs-3">
                        <p>Region / Plat</p>
                      </div>
                      <div className="col-xs-9">
                        <p style={{ textAlign: "left" }}>
                          {props.state.activetab == "Premi Estimation" &&
                            props.state.VehicleRegion != "" &&
                            props.state.datavehicleregion.length > 0 &&
                            ": " +
                              props.state.datavehicleregion.filter(
                                data => data.value == props.state.VehicleRegion
                              )[0].label +
                              " "}
                        </p>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-xs-3">
                        <p>Usage</p>
                      </div>
                      <div className="col-xs-9">
                        <p style={{ textAlign: "left" }}>
                          {props.state.activetab == "Premi Estimation" &&
                            props.state.VehicleUsage != "" &&
                            props.state.datavehicleusage.length > 0 &&
                            ": " +
                              props.state.datavehicleusage.filter(
                                data => data.value == props.state.VehicleUsage
                              )[0].label +
                              " "}
                        </p>
                      </div>
                    </div>
                    <div
                      style={{
                        overflowX: "scroll",
                        overflowY: "scroll",
                        border: "1px solid black"
                      }}
                    >
                      <table className="table-objekP">
                        {props.state.datas && (
                          <React.Fragment>
                            {/* TABLE OBJECT PERTANGGUNGAN START*/}
                            <fieldset class="table-objek-pad">
                              <div className="row separatorTop backgroundgrey table-objek-top">
                                <div
                                  className="col-xs-4 text-center"
                                  style={
                                    props.state.isYearTwoExist
                                      ? { width: "45.41%" }
                                      : { marginRight: "15px" }
                                  }
                                >
                                  <h5 className="text-center">
                                    <b>Objek Pertanggungan</b>
                                  </h5>
                                </div>
                                <div
                                  className={
                                    props.state.isYearTwoExist
                                      ? "col-xs-6 text-center"
                                      : "col-xs-7 text-center table-objek-left"
                                  }
                                >
                                  <h5 className="col-xs-12 text-center">
                                    <b>Perhitungan</b>
                                  </h5>{" "}
                                </div>
                              </div>
                              {!props.state.isYearTwoExist &&
                              !props.state.isYearThreeExist &&
                              !props.state.isYearFourExist &&
                              !props.state.isYearFiveExist ? (
                                ""
                              ) : (
                                <div className="row separatorTop backgroundgrey table-objek-top">
                                  <div className="col-xs-4">
                                    <p />
                                  </div>
                                  <div className="col-xs-8 text-center">
                                    {props.state.isYearFiveExist && (
                                      <div
                                        className="col-xs-2 pull-right panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          <b>Tahun 5</b>
                                        </p>
                                      </div>
                                    )}
                                    {props.state.isYearFourExist && (
                                      <div
                                        className={
                                          props.state.isYearFiveExist
                                            ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                            : "col-xs-2 pull-right panel-body-list"
                                        }
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          <b>Tahun 4</b>
                                        </p>
                                      </div>
                                    )}
                                    {props.state.isYearThreeExist && (
                                      <div
                                        className={
                                          props.state.isYearFourExist
                                            ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                            : "col-xs-2 pull-right panel-body-list"
                                        }
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          <b>Tahun 3</b>
                                        </p>
                                      </div>
                                    )}
                                    {props.state.isYearTwoExist && (
                                      <div
                                        className={
                                          props.state.isYearThreeExist
                                            ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                            : "col-xs-2 pull-right panel-body-list"
                                        }
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          <b>Tahun 2</b>
                                        </p>
                                      </div>
                                    )}
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-right table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>Tahun 1</b>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              )}

                              <div className="row separatorTop table-objek-top">
                                <div className="col-xs-4">
                                  <p>Kendaraan</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <span>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoSI5,
                                          0
                                        )}
                                      </span>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <span>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoSI4,
                                          0
                                        )}
                                      </span>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <span>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoSI3,
                                          0
                                        )}
                                      </span>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <span>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoSI2,
                                          0
                                        )}
                                      </span>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <span>
                                      {props.functions.formatMoney(
                                        props.state.odcModel.CascoSI1,
                                        0
                                      )}
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>Aksesoris Non Standard</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.AccessSI5 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.AccessSI5,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.AccessSI4 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.AccessSI4,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.AccessSI3 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.AccessSI3,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.AccessSI2 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.AccessSI2,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      {props.state.odcModel.AccessSI1 != 0
                                        ? props.functions.formatMoney(
                                            props.state.odcModel.AccessSI1,
                                            0
                                          )
                                        : "No Cover"}
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>
                                    <b>Kendaraan + Aksesoris Non Standard</b>
                                  </p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.CascoAccess5 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.CascoAccess5
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.CascoAccess4 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.CascoAccess4
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.CascoAccess3 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.CascoAccess3
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.CascoAccess2 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.CascoAccess2
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      <b>
                                        {props.state.odcModel.CascoAccess1 != 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.CascoAccess1
                                            )
                                          : "No Cover"}
                                      </b>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              {/* {(!props.state.isYearTwoExist && !props.state.isYearThreeExist && !props.state.isYearFourExist && !props.state.isYearFiveExist) ? "" : */}
                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>Tarif Rate</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoRate5,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoRate4,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoRate3,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.CascoRate2,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      {props.functions.formatMoney(
                                        props.state.odcModel.CascoRate1,
                                        2
                                      ) + "%"}
                                    </p>
                                  </div>
                                </div>
                              </div>
                              {/* } */}

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>Loading Premi</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.LoadingRate5,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.LoadingRate4,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.LoadingRate3,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.LoadingRate2,
                                          2
                                        ) + "%"}
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      {props.functions.formatMoney(
                                        props.state.odcModel.LoadingRate1,
                                        2
                                      ) + "%"}
                                    </p>
                                  </div>
                                </div>
                              </div>

                              {/* PREMI DASAR */}
                              <div className="row separatorTop table-objek-top-bottom ">
                                <div className="col-xs-4">
                                  <p>
                                    <b>Premi Dasar</b>
                                  </p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.BasicPremium5 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .BasicPremium5
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className={
                                        props.state.isYearFiveExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.BasicPremium4 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .BasicPremium4
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className={
                                        props.state.isYearFourExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.BasicPremium3 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .BasicPremium3
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className={
                                        props.state.isYearThreeExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.BasicPremium2 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .BasicPremium2
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left table-objek-right"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      <b>
                                        {props.state.odcModel.BasicPremium1 != 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.BasicPremium1
                                            )
                                          : "No Cover"}
                                      </b>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </fieldset>

                            <fieldset class="table-objek-pad">
                              <div className="row separatorTop backgroundgrey table-objek-top-bottom">
                                <h5 className="col-xs-12 text-center">
                                  <b>
                                    {!props.state.isYearTwoExist &&
                                    !props.state.isYearThreeExist &&
                                    !props.state.isYearFourExist &&
                                    !props.state.isYearFiveExist
                                      ? "TARIF & PREMI " +
                                        props.state.jenisperlindungan.toUpperCase()
                                      : "Perluasan Pertanggungan (optional)"}
                                  </b>
                                </h5>
                              </div>
                              {props.state.jenisperlindungan && (
                                <div>
                                  {!props.state.isYearTwoExist &&
                                  !props.state.isYearThreeExist &&
                                  !props.state.isYearFourExist &&
                                  !props.state.isYearFiveExist ? (
                                    <div className="row separatorTop  table-objek-top">
                                      <div className="col-xs-4"></div>
                                      <div className="col-xs-8">
                                        <div
                                          className="col-xs-6 text-center"
                                          style={{ textAlign: "center" }}
                                        >
                                          TARIF
                                        </div>
                                        <div
                                          className="col-xs-6 text-center panel-body-list"
                                          style={{ textAlign: "center" }}
                                        >
                                          PREMI
                                        </div>
                                      </div>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              )}

                              <div className="row separatorTop table-objek-top">
                                <div className="col-xs-4">
                                  <p>SRCC, FLOOD &amp; WINDSTORM, ETV</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.BundlingPremium5 != "No Cover"  ? "Include" : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundling5 == 0
                                          ? props.state.odcModel.SFEPremi5 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.SFEPremi5,
                                                0
                                              )
                                            : props.state.odcModel
                                                .BundlingPremium5
                                          : props.state.odcModel
                                              .BundlingPremium5}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.BundlingPremium4 != "No Cover" ? "Include" : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundling4 == 0
                                          ? props.state.odcModel.SFEPremi4 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.SFEPremi4,
                                                0
                                              )
                                            : props.state.odcModel
                                                .BundlingPremium4
                                          : props.state.odcModel
                                              .BundlingPremium4}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.BundlingPremium3 != "No Cover" ? "Include" : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundling3 == 0
                                          ? props.state.odcModel.SFEPremi3 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.SFEPremi3,
                                                0
                                              )
                                            : props.state.odcModel
                                                .BundlingPremium3
                                          : props.state.odcModel
                                              .BundlingPremium3}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.BundlingPremium2 != "No Cover" ? "Include" : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundling2 == 0
                                          ? props.state.odcModel.SFEPremi2 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.SFEPremi2,
                                                0
                                              )
                                            : props.state.odcModel
                                                .BundlingPremium2
                                          : props.state.odcModel
                                              .BundlingPremium2}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.BundlingPremium1 != "No Cover" ?  "Include"  : "No Cover"} </p> */}
                                      <p>
                                        {props.state.odcModel.IsBundling1 == 0
                                          ? props.state.odcModel.SFEPremi1 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.SFEPremi1,
                                                0
                                              )
                                            : props.state.odcModel
                                                .BundlingPremium1
                                          : props.state.odcModel
                                              .BundlingPremium1}
                                      </p>
                                    </div>
                                  )}
                                  {!props.state.isYearTwoExist && (
                                    <div>
                                      <div
                                        className="col-xs-6  table-objek-left"
                                        style={{ textAlign: "center" }}
                                      >
                                        {/* <p>{props.state.odcModel.BundlingPremium1 != "No Cover" ?  "Include" : "No Cover"} </p> */}
                                        <p>
                                          {props.state.odcModel.IsBundling1 == 0
                                            ? props.state.odcModel.SFEPremi1 > 0
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel.SFERate1,
                                                  2
                                                ) + "%"
                                              : props.state.odcModel
                                                  .BundlingPremium1
                                            : props.state.odcModel
                                                .BundlingPremium1}
                                        </p>
                                      </div>
                                      <div
                                        className="col-xs-6 panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        {/* <p>{props.state.odcModel.BundlingPremium1 != "No Cover" ?  "Include" : "No Cover"} </p> */}
                                        <p>
                                          {props.state.odcModel.IsBundling1 == 0
                                            ? props.state.odcModel.SFEPremi1 > 0
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel.SFEPremi1,
                                                  0
                                                )
                                              : props.state.odcModel
                                                  .BundlingPremium1
                                            : props.state.odcModel
                                                .BundlingPremium1}
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>Terorisme &amp; Sabotase</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.TSPremium5 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSPremium5,0) : "No Cover"}</p> */}
                                      {/* <p>{props.state.odcModel.TSPremium5 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage5,0) : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundlingTRS5 == 0
                                          ? props.state.odcModel.TSPremium5 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TSPremium5,
                                                0
                                              )
                                            : props.state.odcModel.TSCoverage5
                                          : props.state.odcModel.TSCoverage5}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.TSPremium4 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSPremium4,0) : "No Cover"}</p> */}
                                      {/* <p>{props.state.odcModel.TSPremium4 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage4,0) : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundlingTRS4 == 0
                                          ? props.state.odcModel.TSPremium4 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TSPremium4,
                                                0
                                              )
                                            : props.state.odcModel.TSCoverage4
                                          : props.state.odcModel.TSCoverage4}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.TSPremium3 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSPremium3,0) : "No Cover"}</p> */}
                                      {/* <p>{props.state.odcModel.TSPremium3 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage3,0) : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundlingTRS3 == 0
                                          ? props.state.odcModel.TSPremium3 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TSPremium3,
                                                0
                                              )
                                            : props.state.odcModel.TSCoverage3
                                          : props.state.odcModel.TSCoverage3}
                                      </p>
                                    </div>
                                  )}

                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.TSPremium2 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSPremium2,0) : "No Cover"}</p> */}
                                      {/* <p>{props.state.odcModel.TSPremium2 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage2,0) : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundlingTRS2 == 0
                                          ? props.state.odcModel.TSPremium2 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TSPremium2,
                                                0
                                              )
                                            : props.state.odcModel.TSCoverage2
                                          : props.state.odcModel.TSCoverage2}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      {/* <p>{props.state.odcModel.TSPremium1 > 0 ? (!props.state.isYearTwoExist ? props.functions.formatMoney(props.state.odcModel.TSPremiumRate, 2) + "% " : "") + props.functions.formatMoney(props.state.odcModel.TSPremium1, 0) : "No Cover"}</p> */}
                                      {/* <p>{props.state.odcModel.TSPremium1 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage1,0) : "No Cover"}</p> */}
                                      <p>
                                        {props.state.odcModel.IsBundlingTRS1 == 0
                                          ? props.state.odcModel.TSPremium1 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TSPremium1,
                                                0
                                              )
                                            : props.state.odcModel.TSCoverage1
                                          : props.state.odcModel.TSCoverage1}
                                      </p>
                                    </div>
                                  )}
                                  {!props.state.isYearTwoExist && (
                                    <div>
                                      <div
                                        className="col-xs-6  table-objek-left"
                                        style={{ textAlign: "center" }}
                                      >
                                        {/* <p>{props.functions.formatMoney(props.state.odcModel.TSRate1, 2) + "%"}</p> */}
                                        <p>
                                          {props.state.odcModel.IsBundlingTRS1 ==
                                          0
                                            ? props.state.odcModel.TSPremium1 > 0
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel.TSRate1,
                                                  2
                                                ) + "%"
                                              : props.state.odcModel.TSCoverage1
                                            : props.state.odcModel.TSCoverage1}
                                        </p>
                                      </div>
                                      <div
                                        className="col-xs-6 panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        {/* <p>{props.state.odcModel.TSPremium1 > 0 ? props.functions.formatMoney(props.state.odcModel.TSPremium1, 0) : "No Cover"}</p> */}
                                        {/* <p>{props.state.odcModel.TSPremium1 > 0 ?  props.functions.formatMoney(props.state.odcModel.TSCoverage1,0) : "No Cover"}</p> */}
                                        <p>
                                          {props.state.odcModel.IsBundlingTRS1 ==
                                          0
                                            ? props.state.odcModel.TSPremium1 > 0
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel
                                                    .TSPremium1,
                                                  0
                                                )
                                              : props.state.odcModel.TSCoverage1
                                            : props.state.odcModel.TSCoverage1}
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p style={{ display: "inline-block" }}>
                                    TJH Pihak ke 3
                                  </p>
                                  <span
                                    style={{
                                      float: "right",
                                      display: "inline-block"
                                    }}
                                  >
                                    {props.state.TPLSICOVER > 0
                                      ? props.functions.formatMoney(
                                          props.state.TPLSICOVER,
                                          0
                                        )
                                      : ""}
                                  </span>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.TPLPremium5 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.TPLPremium5,
                                              0
                                            )
                                          : props.state.odcModel.TPLRate5 == "-1"
                                          ? "Include"
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.TPLPremium4 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.TPLPremium4,
                                              0
                                            )
                                          : props.state.odcModel.TPLRate4 == "-1"
                                          ? "Include"
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.TPLPremium3 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.TPLPremium3,
                                              0
                                            )
                                          : props.state.odcModel.TPLRate3 == "-1"
                                          ? "Include"
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}

                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.TPLPremium2 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.TPLPremium2,
                                              0
                                            )
                                          : props.state.odcModel.TPLRate2 == "-1"
                                          ? "Include"
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.TPLPremium1 > 0
                                          ? (!props.state.isYearTwoExist &&
                                            !props.state.isYearThreeExist &&
                                            !props.state.isYearFourExist &&
                                            !props.state.isYearFiveExist
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel
                                                    .TPLPremiumRate,
                                                  2
                                                ) + "%"
                                              : "") +
                                            props.functions.formatMoney(
                                              props.state.odcModel.TPLPremium1,
                                              0
                                            )
                                          : props.state.odcModel.TPLRate1 == "-1"
                                          ? "Include"
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {!props.state.isYearTwoExist && (
                                    <div>
                                      <div
                                        className="col-xs-6  table-objek-left"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.TPLRate1,
                                            2
                                          ) + "%"}
                                        </p>
                                      </div>
                                      <div
                                        className="col-xs-6 panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {props.state.odcModel.TPLPremium1 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.TPLPremium1,
                                                0
                                              )
                                            : props.state.odcModel.TPLRate1 ==
                                              "-1"
                                            ? "Include"
                                            : "No Cover"}
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>

                              <div className="row separatorTop">
                                <div className="col-xs-4">
                                  <p>PA Driver (0.5 % of Limit)</p>
                                  <span>
                                    {props.state.odcModel.TPLSI > 0
                                      ? "(" +
                                        props.functions.formatMoney(
                                          props.state.odcModel.PADRVRSI,
                                          0
                                        ) +
                                        ")"
                                      : ""}
                                  </span>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PADRVRPremium5 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PADRVRPremium5,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PADRVRPremium4 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PADRVRPremium4,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PADRVRPremium3 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PADRVRPremium3,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PADRVRPremium2 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PADRVRPremium2,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PADRVRPremium1 > 0
                                          ? (!props.state.isYearTwoExist &&
                                            !props.state.isYearThreeExist &&
                                            !props.state.isYearFourExist &&
                                            !props.state.isYearFiveExist
                                              ? props.functions.formatMoney(
                                                  props.state.odcModel
                                                    .PADRVRPremiumRate,
                                                  2
                                                ) + "%"
                                              : "") +
                                            props.functions.formatMoney(
                                              props.state.odcModel
                                                .PADRVRPremium1,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {!props.state.isYearTwoExist && (
                                    <div>
                                      <div
                                        className="col-xs-6  table-objek-left"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.PADRVRRate1,
                                            2
                                          ) + "%"}
                                        </p>
                                      </div>
                                      <div
                                        className="col-xs-6 panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {props.state.odcModel.PADRVRPremium1 >
                                          0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .PADRVRPremium1,
                                                0
                                              )
                                            : "No Cover"}
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>

                              <div className="row separatorTop table-objek-bottom">
                                <div className="col-xs-4">
                                  <p style={{ display: "inline-block" }}>
                                    PA Passenger (0.1 % of Limit)
                                  </p>
                                  <span
                                    style={{
                                      float: "right",
                                      display: "inline-block"
                                    }}
                                  >
                                    {props.state.VehicleSitting > 0
                                      ? +props.state.PASSCOVER +
                                        " Seat " /*props.functions.formatMoney(props.state.VehicleSitting, 0)*/
                                      : ""}
                                  </span>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PAPASSPremium5 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PAPASSPremium5,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PAPASSPremium4 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PAPASSPremium4,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PAPASSPremium3 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PAPASSPremium3,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PAPASSPremium2 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PAPASSPremium2,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list table-objek-left"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.PAPASSPremium1 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .PAPASSPremium1,
                                              0
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {!props.state.isYearTwoExist && (
                                    <div>
                                      <div
                                        className="col-xs-6  table-objek-left"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {!props.state.isYearTwoExist &&
                                          !props.state.isYearThreeExist &&
                                          !props.state.isYearFourExist &&
                                          !props.state.isYearFiveExist
                                            ? props.functions.formatMoney(
                                                props.state.odcModel.PAPASSRate1,
                                                2
                                              ) + "%"
                                            : ""}
                                        </p>
                                      </div>
                                      <div
                                        className="col-xs-6 panel-body-list"
                                        style={{ textAlign: "center" }}
                                      >
                                        <p>
                                          {props.state.odcModel.PAPASSPremium1 >
                                          0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .PAPASSPremium1,
                                                0
                                              )
                                            : "No Cover"}
                                        </p>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </fieldset>
                            <fieldset class="table-objek-pad">
                              <div className="row table-objek-bottom">
                                <div className="col-xs-4">
                                  <p>
                                    <b>Premi Perluasan</b>
                                  </p>
                                </div>
                                {/*<div className="col-xs-8 text-right">
                        <p>{props.functions.formatMoney(props.state.odcModel.ExtendPremium1 + props.state.odcModel.ExtendPremium2 +
                          props.state.odcModel.ExtendPremium3)}</p>
                      </div> */}
                                <div className="col-xs-8 text-right">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.ExtendPremium5
                                          )}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className={
                                        props.state.isYearFiveExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.ExtendPremium4
                                          )}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className={
                                        props.state.isYearFourExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.ExtendPremium3
                                          )}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className={
                                        props.state.isYearThreeExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.functions.formatMoney(
                                            props.state.odcModel.ExtendPremium2
                                          )}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-right table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      <b>
                                        {props.functions.formatMoney(
                                          props.state.odcModel.ExtendPremium1
                                        )}
                                      </b>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="row table-objek-bottom">
                                <div className="col-xs-4">
                                  <p>Premi Dasar + Perluasan</p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.BExtendPremium5 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .BExtendPremium5
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className={
                                        props.state.isYearFiveExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.BExtendPremium4 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .BExtendPremium4
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className={
                                        props.state.isYearFourExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.BExtendPremium3 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .BExtendPremium3
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className={
                                        props.state.isYearThreeExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        {props.state.odcModel.BExtendPremium2 > 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel
                                                .BExtendPremium2
                                            )
                                          : "No Cover"}
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-left table-objek-right"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      {props.state.odcModel.BExtendPremium1 != 0
                                        ? props.functions.formatMoney(
                                            props.state.odcModel.BExtendPremium1
                                          )
                                        : "No Cover"}
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className={"col-xs-7"}>
                                  <p>Biaya Administrasi</p>
                                </div>
                                <div className={"col-xs-5"}>
                                  <p
                                    style={
                                      props.state.isYearTwoExist
                                        ? {
                                            textAlign: "center",
                                            marginLeft: "15%"
                                          }
                                        : {
                                            textAlign: "left",
                                            marginLeft: "12%"
                                          }
                                    }
                                  >
                                    {props.functions.formatMoney(
                                      props.state.datas.AdminFee
                                    )}
                                  </p>
                                </div>
                              </div>

                              <div class="row separatorTop backgroundgrey table-objek-bottom table-objek-top">
                                <div className="col-xs-4">
                                  <p>
                                    <b>TOTAL PREMIUM + Admin</b>
                                  </p>
                                </div>
                                <div className="col-xs-8 text-center">
                                  {props.state.isYearFiveExist && (
                                    <div
                                      className="col-xs-2 pull-right panel-body-list"
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.TotalPremium5 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .TotalPremium5
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearFourExist && (
                                    <div
                                      className={
                                        props.state.isYearFiveExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.TotalPremium4 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .TotalPremium4
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearThreeExist && (
                                    <div
                                      className={
                                        props.state.isYearFourExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.TotalPremium3 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .TotalPremium3
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  {props.state.isYearTwoExist && (
                                    <div
                                      className={
                                        props.state.isYearThreeExist
                                          ? "col-xs-2 pull-right panel-body-list table-objek-right"
                                          : "col-xs-2 pull-right panel-body-list"
                                      }
                                      style={{ textAlign: "center" }}
                                    >
                                      <p>
                                        <b>
                                          {props.state.odcModel.TotalPremium2 > 0
                                            ? props.functions.formatMoney(
                                                props.state.odcModel
                                                  .TotalPremium2
                                              )
                                            : "No Cover"}
                                        </b>
                                      </p>
                                    </div>
                                  )}
                                  <div
                                    className={
                                      props.state.isYearTwoExist
                                        ? "col-xs-2 pull-right panel-body-list table-objek-right table-objek-left"
                                        : "col-xs-12 pull-right panel-body-list table-objek-left"
                                    }
                                    style={{ textAlign: "center" }}
                                  >
                                    <p>
                                      <b>
                                        {props.state.odcModel.TotalPremium1 != 0
                                          ? props.functions.formatMoney(
                                              props.state.odcModel.TotalPremium1
                                            )
                                          : "No Cover"}
                                      </b>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                            {/* TABLE OBJECT PERTANGGUNGAN END*/}
                          </React.Fragment>
                        )}
                      </table>
                    </div>

                    <div className="row">
                      <div
                        className="form-group"
                        style={{
                          marginRight: "15px",
                          marginLeft: "15px"
                        }}
                      >
                        <button className="btn btn-info pull-right" style={{marginTop: 20}}>
                          Next
                        </button>
                      </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                  </form>
        </React.Fragment>
    )
}
