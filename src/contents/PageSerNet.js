import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter, Link } from "react-router-dom";
import branch from "../assets/branch.png";
import gardacenter from "../assets/gardacenter.png";
import workshop from "../assets/workshop.png";
import Header from "../components/Header";

class PageSerNet extends Component {
  render() {
    return (
      <div>
        <Header/>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <div className="row">
            <div
              className="col-md-4 col-xs-6"
              style={{ padding: "10px" }}
            >
              <Link to="/servicenetworks-branch">
                <img
                  src={branch}
                  className="img-rounded"
                  style={{ width: "100%" }}
                  alt=""
                />
              </Link>
            </div>
            <div
              className="col-md-4 col-xs-6"
              style={{ padding: "10px" }}
            >
              <Link to="/servicenetworks-gardacenter">
                <img
                  src={gardacenter}
                  className="img-rounded"
                  style={{ width: "100%" }}
                  alt=""
                />
              </Link>
            </div>
            <div
              className="col-md-4 col-xs-6"
              style={{ padding: "10px" }}
            >
              <Link to="/servicenetworks-workshop">
                <img
                  src={workshop}
                  className="img-rounded"
                  style={{ width: "100%" }}
                  alt=""
                />
              </Link>
            </div>
          </div>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageSerNet);
