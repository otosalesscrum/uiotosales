import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter, Redirect } from "react-router-dom";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../components/Header";


class PageDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading:false
    }
  }
  componentDidMount() {
    this.checkRole();
  }
checkRole=()=>{
  var role=this.props.userInfo.User.Role;
  this.setState.isLoading=true;
  // console.log("Role "+role);
  var endurl='';
  if(role=="NATIONALMGR"){
    endurl='dashboard-National';
  }
  else if(role=="REGIONALMGR"){
    endurl='dashboard-Regional';
  }
  else if((role=="BRANCHMGR")||(role=="SALESSECHEAD")){
    endurl='dashboard-Branch';
  }
  else if((role=="SALESOFFICER")||(role=="SESALESOFFICER")||(role=="SESALESEXECUTIVE")||
          (role=="SALESEXECUTIVE")){
    endurl='dashboard-Personal';
  }
  
  this.setState.isLoading=false;
  this.props.history.push(endurl);
}
  
  render() {
    return (
      <div>
        <Header/>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <BlockUi
            tag="div"
            blocking={this.setState.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageDashboard);
