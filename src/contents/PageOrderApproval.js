import React, { Component } from "react";
import Footer from "../components/Footer";
import { withRouter } from "react-router-dom";
import { OtosalesListOrderApproval } from "../otosalescomponents";
import { API_URL, API_VERSION } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Header from "../components/Header";
import { ToastContainer, toast } from "react-toastify";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";

class PageOrderApproval extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      datas: [
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode:"paytode",
          paytoname:"paytoname",
          accountinfobankname:"accountinfobankname",
          accountinfoaccountno:"accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode:"paytode",
          paytoname:"paytoname",
          accountinfobankname:"accountinfobankname",
          accountinfoaccountno:"accountinfoaccountno"
        },
        {
          id: "1",
          name: "Loading Data . . . . .",
          paytocode:"paytode",
          paytoname:"paytoname",
          accountinfobankname:"accountinfobankname",
          accountinfoaccountno:"accountinfoaccountno"
        }
      ]
    };
  }

  componentDidMount(){
    if(secureStorage.getItem("orderapprovalnotif") != undefined){
      let notifOrderApproval = secureStorage.getItem("orderapprovalnotif").split("-");
      if(notifOrderApproval[0].includes("success")){
        if(notifOrderApproval[1] == "Order approved"){
          toast.info("✔️ " + notifOrderApproval[1], {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }else{
          toast.info("❗ Order rejected", {
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        
      }else{
        toast.warning("❗ " + notifOrderApproval[1], {
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
      }
      secureStorage.removeItem("orderapprovalnotif");
    }
    this.searchDataHitApi();
  }

  searchDataHitApi = () => {
    const { search } = this.state;
    this.setState({ isLoading: true });
    fetch(`${API_URL+""+API_VERSION}/replication/orderapproval`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer "+JSON.parse(ACCOUNTDATA).Token
      }),
      body: "userID=" + this.props.username
    })
      .then(res => res.json())
      .then(jsn =>
        jsn.data.map(data => ({
          id: `${data.Order_ID}`,
          name: `${data.Name}`,
          paytocode:`${data.PayToCode}`,
          paytoname:`${data.PayToName}`,
          accountinfobankname:`${data.Account_Info_BankName}`,
          accountinfoaccountno:`${data.Account_Info_AccountNo}`,
          orderno: `${data.OrderNo}`,
          isso: `${data.isSO}`
        }))
      )
      .then(datamapping => {
        this.setState({
          datas: datamapping,
          isLoading: false
        });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          // this.searchDataHitApi();
        }
      });
  }

  onclickItem = (data) => {
    secureStorage.setItem("OrderIdOA", data.id);
    secureStorage.setItem("NameOA", data.name);
    secureStorage.setItem("OrderNoOA", data.orderno);
    secureStorage.setItem("IsSOOA", data.isso);
  }

  
  render() {
    return (
      <div>
        <Header titles="Order Approval"/>
        <div className="content-wrapper" style={{ padding: "10px" }}>
        <ToastContainer />
        <BlockUi
            tag="div"
            blocking={this.state.isLoading}
            loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
            message="Loading, please wait"
          >
              <div className="panel-body panel-body-list" id="Datalist">
                <OtosalesListOrderApproval
                  onclickItem = {this.onclickItem}
                  name="sampleList"
                  rowscount={this.state.datas.length}
                  dataitems={this.state.datas}
                />
              </div>
          </BlockUi>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default withRouter(PageOrderApproval);
