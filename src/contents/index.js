import { lazy } from "react";
import { Util } from "../otosalescomponents";
const PageSerNet = lazy(() => Util.retry(() => import("./PageSerNet")));
const Branch = lazy(() => Util.retry(() => import("./Branch")));
const Gardacenter = lazy(() => Util.retry(() => import("./Gardacenter")));
const Workshop = lazy(() => Util.retry(() => import("./Workshop")));
// const PageTaskList = lazy(() => Util.retry(() => import("./v0219URF2019/task_list/PageTaskList")));
const PageTaskList = lazy(() => Util.retry(() => import("./v0043URF2020/task_list/PageTaskList")));
// const PageTaskList = lazy(() => Util.retry(() => import("./PageTaskList")));
const PageInputDetailTaskList = lazy(() => Util.retry(() => import("./v2/task_list/PageInputDetailTaskList")));
// const PageInputProspect = lazy(() => Util.retry(() => import("./v0219URF2019/PageInputProspect")));
// const PageInputProspect = lazy(() => Util.retry(() => import("./v0043URF2020/PageInputProspect")));
const PageInputProspect = lazy(() => Util.retry(() => import("./v0107URF2020/PageInputProspect")));
// const PageOrderApproval = lazy(() => Util.retry(() => import("./PageOrderApproval")));
// const PageOrderApproval = lazy(() => Util.retry(() => import("./v2/order_approval/PageOrderApproval")));
const PageOrderApproval = lazy(() => Util.retry(() => import("./v0090URF2020/order_approval/PageOrderApproval")));
// const PageProspectLead = lazy(() => Util.retry(() => import("./PageProspectLead")));
const PageProspectLead = lazy(() => Util.retry(() => import("./v0090URF2020/PageProspectLead")));
const PageQuotationDetails = lazy(() => Util.retry(() => import("./PageQuotationDetails")));
// const PageOrderApprovalDetail = lazy(() => Util.retry(() => import("./PageOrderApprovalDetail")));
// const PageOrderApprovalDetail = lazy(() => Util.retry(() => import("./v2/order_approval/PageOrderApprovalDetail")));
const PageOrderApprovalDetail = lazy(() => Util.retry(() => import("./v0090URF2020/order_approval/PageOrderApprovalDetail")));
// const PageFollowUpDetails = lazy(() => Util.retry(() => import("./PageFollowUpDetails"));
// const PageFollowUpDetails = lazy(() => Util.retry(() => import("./v2/task_list/PageFollowUpDetails")));
// const PageFollowUpDetails = lazy(() => Util.retry(() => import("./v0219URF2019/task_list/PageFollowUpDetails")));
// const PageFollowUpDetails = lazy(() => Util.retry(() => import("./v0043URF2020/task_list/PageFollowUpDetails")));
const PageFollowUpDetails = lazy(() => Util.retry(() => import("./v0090URF2020/task_list/PageFollowUpDetails")));
// const PagePremiumSimulation = lazy(() => Util.retry(() => import("./v0219URF2019/PagePremiumSimulation")));
// const PagePremiumSimulation = lazy(() => Util.retry(() => import("./v0043URF2020/PagePremiumSimulation")));
const PagePremiumSimulation = lazy(() => Util.retry(() => import("./v0107URF2020/PagePremiumSimulation")));
const PageQuotationDetailsSimulation = lazy(() => Util.retry(() => import("./PageQuotationDetailsSimulation")));
const PageDashboard = lazy(() => Util.retry(() => import("./PageDashboard")));
const PageDashboardNational = lazy(() => Util.retry(() => import("./PageDashboardNational")));
const PageDashboardRegional = lazy(() => Util.retry(() => import("./PageDashboardRegional")));
const PageDashboardBranch = lazy(() => Util.retry(() => import("./PageDashboardBranch")));
const PageDashboardPersonal = lazy(() => Util.retry(() => import("./PageDashboardPersonal")));
// const PageTasklistDetails = lazy(() => Util.retry(() => import("./v0219URF2019/task_list/tasklistdetails")));
// const PageTasklistDetails = lazy(() => Util.retry(() => import("./v0043URF2020/task_list/tasklistdetails")));
const PageTasklistDetails = lazy(() => Util.retry(() => import("./v0107URF2020/task_list/tasklistdetails")));
const PageSurveyDocuments = lazy(() => Util.retry(() => import("./v2/task_list/tasklistdetails/PageSurveyDocuments")));
// const PageDashboardv2 = lazy(() => Util.retry(() => import("./v2/dashboard")));
const PageDashboardv2 = lazy(() => Util.retry(() => import("./v0043URF2020/dashboard")));
const PageTasklistDirtyData = lazy(() => Util.retry(() => import("./v2/task_list/tasklistdetails/PageTasklistDirtyData")));
const PageTasklistMVGodig = lazy(() => Util.retry(() => import("./v0219URF2019/task_list/tasklistdetails/PageTasklistMVGodig")));
const ErrorBoundary = lazy(() => Util.retry(() => import("./v2/error_handling")));


const  PageChangePassword = lazy(() => Util.retry(() =>  import ("./PageChangePassword")));

export {
    PageChangePassword,
    PageSerNet,
    Branch,
    Gardacenter,
    Workshop,
    PageTaskList,
    PageInputDetailTaskList,
    PageInputProspect,
    PageOrderApproval,
    PageProspectLead,
    PageQuotationDetails,
    PageOrderApprovalDetail,
    PageFollowUpDetails,
    PageTasklistDetails,
    PagePremiumSimulation,
    PageQuotationDetailsSimulation,
    PageDashboard,
    PageDashboardNational,
    PageDashboardRegional,
    PageDashboardBranch,
    PageDashboardPersonal,
    PageDashboardv2,
    // PageTasklistDetails,
    PageSurveyDocuments,
    PageTasklistDirtyData,
    PageTasklistMVGodig,
    ErrorBoundary
}