import React, {Component} from 'react';
import Footer from './Footer'; 
import { withRouter } from "react-router-dom";

class Content extends Component {
     render(){
        return (
            <div>
            <div className="content-wrapper" style={{padding:"10px"}}>
                    {this.props.layout}
            </div>
            <Footer />
            </div>
        )
    }
}

export default withRouter (Content);