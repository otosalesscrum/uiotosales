import React, { Component } from 'react';  
import { isNullOrUndefined } from 'util'; 

class a2isbutton extends Component {
   render() {   
    return (
        <button type="button" 
          className={(!isNullOrUndefined(this.props.buttonstyle)) ?"btn btn-sm btn-" + this.props.buttonstyle:"btn btn-sm btn-primary"} 
          style={{margin:"3px", visibility:(this.props.visible===false)?"hidden":""}} 
          onClick={this.props.onclick}
          name = {this.props.name} 
          disabled={this.props.disabled || false}
          readOnly={this.props.readonly || false}
          data-toggle={this.props.toggle || ""} 
          data-target={this.props.target || ""}
          >
            <span className={(!isNullOrUndefined(this.props.icons)) ? 'fa fa-'+ this.props.icons:""}></span>  
            {this.props.caption}  
        </button>  
    );
  }
}

export default a2isbutton;
