import React, { Component } from 'react';  
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'font-awesome/css/font-awesome.min.css';

class a2ispager extends Component {
  
  constructor(props)
  {
    super()
    this.state={
      pageno:props.pageno||1,
      pagecount:props.pagecount||1
    }
    // this.onBackAction=this.onBackAction.bind(this);
    // this.onNextAction=this.onNextAction.bind(this);
    // this.onFirstPage=this.onFirstPage.bind(this);
    // this.onLastPage=this.onLastPage.bind(this);
  } 

  onNextAction=()=>{ 
    this.setState(
      {
        pageno: (this.state.pageno+1)
      },()=>{
         this.props.onGetPaging(this.state);
      }
    );  
  }

  onBackAction=()=>
  {
    this.setState(
      {
        pageno: (this.state.pageno-1)
      },()=>{
         this.props.onGetPaging(this.state);
      }
    );
  }

  onLastPage=()=>
  {
    this.setState(
      {
        pageno: this.state.pagecount
      },()=>{
         this.props.onGetPaging(this.state);
      }
    );
  }

  onFirstPage=()=>
  {
    this.setState(
      {
        pageno: 1
      },()=>{
         this.props.onGetPaging(this.state);
      }
    );
  } 

  render() { 
    return (
      <div className="btn-group"
        name = {this.props.name}
        disabled={this.props.disabled || false}
        readOnly={this.props.readOnly || false}  
        style={{visibility:(this.props.visible===false)?"hidden":"" }}
      > 
        <button className="btn btn-sm btn-default" onClick={this.onFirstPage} disabled={(this.state.pageno===1) ? true : false}>
        <span className="fa fa-step-backward"></span>
        </button>
        <button className="btn btn-sm btn-default" onClick={this.onBackAction} disabled={(this.state.pageno===1) ? true : false}>
          <span className="fa fa-backward"></span>
        </button>
        <button className="btn btn-sm btn-default disabled" style={{cursor:"default"}}>{this.state.pageno} of {this.state.pagecount}</button>
        <button className="btn btn-sm btn-default" onClick={this.onNextAction} disabled={(this.state.pageno===this.state.pagecount) ? true : false}>
        <span className="fa fa-forward"></span>
        </button>
        <button className="btn btn-sm btn-default" onClick={this.onLastPage} disabled={(this.state.pageno===this.state.pagecount) ? true : false}>
          <span className="fa fa-step-forward"></span>
        </button>
      </div>
    );
  }
}

export default a2ispager;
