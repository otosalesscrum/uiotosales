import React, { Component } from 'react';  
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import '../../css/a2is.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'font-awesome/css/font-awesome.min.css';

class a2isdatetimepicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
          startDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this);
      }
     
      handleChange(date) {
        this.setState({
          startDate: date
        });
      }
      isRequired=(required)=> {
        if (required) { 
            return <span className="required"> *</span>;
        }
        return null;
      }

  render() {
    var visibility ="";
    if(this.props.visible===false) visibility="hidden";
    return (
        <div className="form-group row" style={{visibility:visibility}}>
            <label   className="col-xs-12 col-sm-3 col-form-label text-right">{this.props.caption || "Date time picker"}
                {this.isRequired(this.props.required)}
            </label>
            <div className="col-xs-12 col-sm-9">
                <div className="input-group input-group-sm datetime"> 
                <DatePicker style={{width:"300px"}}
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    dateFormat="MMMM d, yyyy h:mm aa"
                    timeCaption="time"
                    name={this.props.name}
                    disabled={this.props.disabled || false}
                    readonly={this.props.readonly || false}
                />
                </div>
            </div>
        </div>
 
    );
  }
}

export default a2isdatetimepicker;
