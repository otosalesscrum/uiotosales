import React, { Component } from 'react';  
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import '../../css/a2is.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'font-awesome/css/font-awesome.min.css';

class a2istimepicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
          startDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this);
      }
     
      handleChange(date) {
        this.setState({
          startDate: date
        });
      }
      isRequired=(required)=> {
        if (required) { 
            return <span className="required"> *</span>;
        }
        return null;
      }

  render() {
    var visibility ="";
    if(this.props.visible===false) visibility="hidden";


    return (
        <div className="form-group row" style={{visibility:visibility}} >
            <label   className="col-xs-12 col-sm-3 col-form-label text-right">{this.props.caption || "Timepicker"}
                {this.isRequired(this.props.required)}
            </label>
            <div className="col-xs-12 col-sm-9">
                <div className="input-group input-group-sm date"> 
                    <DatePicker
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                        showTimeSelect
                        showTimeSelectOnly 
                        dateFormat={this.props.format||"HH:mm"}
                        timeFormat={this.props.format||"HH:mm"}
                        timeCaption="Time"
                        // minTime={this.props.minTime}
                        // maxTime={this.props.maxTime}
                        //locale="en-GB"
                        timeIntervals={this.props.intervals|| 15}
                        name={this.props.name}
                        disabled={this.props.disabled || false}
                        readonly={this.props.readonly || false}
                    /> 
                </div>
            </div>
        </div>
 
    );
  }
}

export default a2istimepicker;
