import React, { Component } from 'react';   
class a2islabel extends Component {
  componentDidUpdate(paramprops)
  {
    // if(paramprops.visible) console.log("if props");
    // else console.log("else props");
  }
  render() {  
    return (
        <div className="row"> 
          <label 
              className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right"
              name = {this.props.name}
              id={this.props.name}
              value={this.props.caption || "a2islabel"}   
              style={{
                  visibility:(this.props.visible===false)?"hidden":"",
                  textAlign:(this.props.align!=="")?this.props.align:"",
              }} 
              >
              {this.props.caption}
          </label>
        </div>
    );
  }
}

export default a2islabel;
