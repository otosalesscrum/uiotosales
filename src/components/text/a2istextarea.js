import React, { Component } from 'react';  
import "../../css/a2is.css";

class a2istextarea extends Component {
  constructor(props)
  {
    super()
    this.state={
      value:props.value||""
    }
  } 

  onChange=(event)=> {
    this.setState(
        {
          value: event.target.value
        },()=>{
          if(typeof(this.props.onChangeText)==="function")
            {
              this.props.onChangeText(this.state.value);
            }
        }
      );
  } 
  isRequired=(required)=> {
    if (required) { 
        return <span className="required"> *</span>;
    }
    return null;
  }
  render() {
     return (
      <div className="form-group" style={{visibility:(this.props.visible===false) ?"hidden":""}}>
        <label   className="col-xs-12 col-sm-3 col-form-label text-right">{this.props.caption || "Text area"}{this.isRequired(this.props.required)}</label>
        <div className="col-xs-12 col-sm-9">
          <textarea className="form-control"
              name = {this.props.name}
              value={this.state.value}
              onChange={this.onChange}
              disabled={this.props.disabled || false}
              readOnly={this.props.readonly || false}
              minLength={this.props.minLength}
              maxLength={this.props.maxLength}
              placeholder={this.props.hint} 
              rows={this.props.rows || 2}
          ></textarea>
        </div>
      </div>
    );
  }
}

export default a2istextarea;
