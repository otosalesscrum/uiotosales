import React, { Component } from 'react';   
import "../../css/a2is.css";
class a2istext extends Component {

  onChange=(event)=> {
    console.log("test change");
  } 
  //has-error // for validate error

  isRequired=(required)=> {
      if (required) { 
          return <span className="required"> *</span>;
      }
      return null;
    }


  render() { 
    return (
      <div className="form-group " style={{visibility:(this.props.visible===false)?"hidden":""}} > 
        <label className="col-xs-12 col-sm-3 col-form-label text-right">{this.props.caption || "Textbox"} {this.isRequired(this.props.required)}</label>
        <div className="col-xs-12 col-sm-9">
          <input 
          autoFocus
              autoComplete="off"
              type="text" 
              className="form-control"   
              //props
              name = {this.props.name}
              id={this.props.name}
              value={this.props.value}
              disabled={this.props.disabled || false}
              readOnly={this.props.readonly || false}
              minLength={this.props.minLength}
              maxLength={this.props.maxLength}
              placeholder={this.props.hint}  
              //event handler
            onChange={((event) => {this.props.onChangeText(event)})|| this.onChange}
            onKeyDown={((event) => {this.props.onKeyDown(event)}) || this.onChange}


              />
        </div>
      </div>
    );
  }
}

export default a2istext;
