import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

class NavLink extends React.Component {
  render() {
    var path = this.context.router.route.location.pathname + "";
    var isActive = path.includes(this.props.to);
    var className = isActive ? "active" : "";

    return (
      <li style={{listStyle:"none"}} className={className}>
        <Link {...this.props}>{this.props.children}</Link>
      </li>
    );
  }
}

NavLink.contextTypes = {
  router: PropTypes.object
};

export default NavLink;
