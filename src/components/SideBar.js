import React, { Component } from "react";
import NavLink from "./NavLink";
import OtosalesModalAlert from "../otosalescomponents/otosalesmodalalert";
import {
  API_URL,
  API_V2,
  ACCOUNTDATA,
  HEADER_API,
  API_VERSION_0219URF2019,
  API_VERSION_0090URF2020,
} from "../config";

import * as Util from "../otosalescomponents/helpers/Util.js";
import * as Log from "../otosalescomponents/helpers/Logger.js";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";

export default class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalLogOut: false,
      countOrderApproval: secureStorage.getItem("countOrderApproval") || 0,
      countOrderApprovalOnly: 0,
      countPaymentApprovalOnly: 0,
      isDonePopulateOrderApproval: false,
      isDonePopulatePaymentApproval: false
    };
  }

  componentDidMount() {
    this.searchDataHitApi();
    // if(Util.isNullOrEmpty(secureStorage.getItem("nexthittingorderapproval"))){
    //   secureStorage.setItem("nexthittingorderapproval", Util.convertDate());
    // }
    this.orderapproval = setInterval(() => {
      let countOrderApproval = Util.isNullOrEmpty(
        secureStorage.getItem("countOrderApproval")
      )
        ? 0
        : secureStorage.getItem("countOrderApproval");
      if (countOrderApproval != this.state.countOrderApproval) {
        this.setState({ countOrderApproval });
      }
      this.searchDataHitApi();
    }, 2 * 60 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.orderapproval);
  }

  searchDataHitApi = () => {
    if (
      !Util.isNullOrEmpty(secureStorage.getItem("nexthittingorderapproval"))
    ) {
      if (
        Util.convertDate() -
          Util.convertDate(secureStorage.getItem("nexthittingorderapproval")) <=
        2 * 60 * 1000
      ) {
        return;
      }
    }

    const {userInfo} = this.props;
    if(!Util.isNullOrEmpty(userInfo.User.Role)){
      if(Util.stringEquals(userInfo.User.Role, "TELESALES") || Util.stringEquals(userInfo.User.Role, "TELERENEWAL")){
        this.hitIsActive();
      }
    }
    const { search } = this.state;
    // this.setState({ isLoading: true });
    this.countOrderApprovalOnly();
    this.countPaymentApprovalOnly();
  };

  countOrderApprovalOnly = () => {
    Util.fetchAPIAdditional(
      `${
        API_URL + "" + API_VERSION_0090URF2020
      }/DataReact/populateOrderApproval`,
      "POST",
      {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token,
      },
      {
        userID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        ReqType: 2,
      }
    )
      .then((res) => res.json())
      .then((jsn) => {
        this.setState(
          {
            countOrderApprovalOnly: Util.isNullOrEmpty(jsn.Data.Count)
              ? 0
              : jsn.Data.Count,
            isDonePopulateOrderApproval : true
          },
          () => {
            this.joinReturnOrderApproval();
          }
        );
      })
      .catch((error) => {
        Log.error(
          "Error SideBar.js on searchDataHitApi from /populateOrderApproval",
          error
        );
        this.setState({
          isLoading: false,
        });
      });
  };

  countPaymentApprovalOnly = () => {
    Util.fetchAPIAdditional(
      `${
        API_URL + "" + API_VERSION_0090URF2020
      }/DataReact/populatePaymentApproval`,
      "POST",
      {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Bearer " + JSON.parse(ACCOUNTDATA).Token,
      },
      {
        userID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID,
        ReqType: 2,
      }
    )
      .then((res) => res.json())
      .then((jsn) => {
        this.setState(
          {
            countPaymentApprovalOnly: Util.isNullOrEmpty(jsn.Data.Count)
              ? 0
              : jsn.Data.Count,
              isDonePopulatePaymentApproval : true
          },
          () => {
            this.joinReturnOrderApproval();
          }
        );
      })
      .catch((error) => {
        Log.error(
          "Error SideBar.js on searchDataHitApi from /populatePaymentApproval",
          error
        );
        this.setState({
          isLoading: false,
        });
      });
  };

  joinReturnOrderApproval = () => {
    const { isDonePopulateOrderApproval, isDonePopulatePaymentApproval, countOrderApprovalOnly, countPaymentApprovalOnly } = this.state;
    if(isDonePopulateOrderApproval && isDonePopulatePaymentApproval){
      secureStorage.setItem(
        "countOrderApproval",
        countOrderApprovalOnly + countPaymentApprovalOnly || null
      );
      this.setState({
        countOrderApproval: secureStorage.getItem("countOrderApproval") || 0,
      });
      secureStorage.setItem("nexthittingorderapproval", Util.convertDate());
    }
  };

  hitIsActive = () => {
    let account = JSON.parse(ACCOUNTDATA);
    Util.fetchAPIAdditional(
      `${API_URL + "" + API_VERSION_0219URF2019}/DataReact/updateUserIsActive`,
      "POST",
      HEADER_API,
      {
        SalesOfficerID: account.UserInfo.User.SalesOfficerID,
        IsLogin: 1,
      }
    );
  };

  onCloseModalLogOut = () => {
    this.setState({ showModalLogOut: false });
  };

  handleIsCSOSALESOFFICER = () => {
    let temp = false;
    let accnt = JSON.parse(ACCOUNTDATA);
    if (!Util.isNullOrEmpty(accnt)) {
      accnt = accnt.UserInfo.User;
      if (accnt.Role == "SALESOFFICER" && accnt.IsCSO == 1) {
        temp = true;
      }
    }

    return temp;
  };

  handleSignOut(e) {
    e.preventDefault();
    this.setState({ showModalLogOut: true });
  }

  render() {
    const { menu } = this.props;
    Log.debugStr(
      "RENDER COUNTER ORDER APPROVAL => " + this.state.countOrderApproval
    );
    return (
      <aside
        className="main-sidebar"
        style={{ backgroundColor: "black", paddingTop: "50px" }}
      >
        <OtosalesModalAlert
          message="Are you sure want to logout?"
          onClose={this.onCloseModalLogOut}
          onSubmit={this.props.onSignOut}
          showModalInfo={this.state.showModalLogOut}
        />
        <section className="sidebar">
          <ul className="sidebar-menu" data-widget="tree">
            <li className="header">MAIN NAVIGATION</li>
            {menu.filter((menu) => menu.FormID === "PROSPECTLEAD").length >
              0 && (
              <NavLink
                onClick={() => this.props.clickSideBarMenu()}
                to="/prospectlead"
              >
                <i className="fa fa-users" /> <span>Prospect Lead</span>
              </NavLink>
            )}
            {menu.filter((menu) => menu.FormID === "TASKLIST").length > 0 && (
              <NavLink
                onClick={() => this.props.clickSideBarMenu()}
                to="/tasklist"
              >
                <i className="fa fa-calendar-o" /> <span>Task List</span>
              </NavLink>
            )}
            {menu.filter((menu) => menu.FormID === "INPUTPROSPECT").length >
              0 && (
              <NavLink
                onClick={() => this.props.clickSideBarMenu()}
                to="/inputprospect"
              >
                <i className="fa fa-plus-square" /> <span>Input Prospect</span>
                <span className="pull-right-container" />
              </NavLink>
            )}
            {menu.filter((menu) => menu.FormID === "ORDERSIMULATION").length >
              0 && (
              <NavLink
                onClick={() => this.props.clickSideBarMenu()}
                to="/premiumsimulation"
              >
                <i className="fa fa-files-o" /> <span>Premium Simulation</span>
                <span className="pull-right-container" />
              </NavLink>
            )}
            {!this.handleIsCSOSALESOFFICER() &&
              menu.filter((menu) => menu.FormID === "APPROVALORDER").length >
                0 && (
                <NavLink
                  onClick={() => this.props.clickSideBarMenu()}
                  to="/orderapproval"
                >
                  <i className="fa fa-money" />{" "}
                  <span>
                    Order Approval{" "}
                    {secureStorage.getItem("countOrderApproval") != "null" && (
                      <span className="badge" style={{ marginLeft: "5px" }}>
                        {this.state.countOrderApproval}
                      </span>
                    )}
                  </span>
                  <span className="pull-right-container" />
                </NavLink>
              )}
            {!this.handleIsCSOSALESOFFICER() &&
              menu.filter((menu) => menu.FormID === "DASHBOARD").length > 0 && (
                <NavLink
                  onClick={() => this.props.clickSideBarMenu()}
                  to="/dashboard"
                >
                  <i className="fa fa-signal" /> <span>Dashboard</span>
                  <span className="pull-right-container" />
                </NavLink>
              )}
            {menu.filter((menu) => menu.FormID === "INFO").length > 0 && (
              <NavLink
                onClick={() => this.props.clickSideBarMenu()}
                to="/servicenetworks"
              >
                <i className="fa fa-info-circle" /> <span>Service Network</span>
                <span className="pull-right-container" />
              </NavLink>
            )}
            <NavLink
              onClick={() => this.props.clickSideBarMenu()}
              to="/changepassword"
            >
              <i className="fa fa-lock" /> <span>Change Password</span>
              <span className="pull-right-container" />
            </NavLink>
            <li>
              <a href="#" onClick={this.handleSignOut.bind(this)}>
                <i className="fa fa-sign-out" /> <span>Logout</span>
              </a>
            </li>
          </ul>
        </section>
      </aside>
    );
  }
}
