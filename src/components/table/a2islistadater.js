import React, { Component } from "react";
import "../../css/a2is.css";

class a2islistadapter extends Component {
  constructor(props) {
    super();
  }
  render() {
    // console.log(this.props.dataitems);
    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map(function(data, index) {
      if (index < pg) {
        return (
          <div className="col-md-12 col-xs-12 panel-body-list" key={index}>
            <div className="panel panel-default">
              <div className="panel-body panel-body-list">
                <div className="col-xs-12">
                  <h4>
                    <b>{data.namelokasi}</b>
                  </h4>
                </div>
                <div className="col-xs-12 col-md-12 panel-body-list">
                  <div className="col-xs-12 col-md-12">
                      <p>
                        <i className="fa fa-university" />
                        {" " + data.alamat}
                      </p>
                    </div>
                    <div className="col-xs-12">
                      <p>
                        <i className="fa fa-phone-square" /> Phone:{" "}
                        {data.phone}
                      </p>
                    </div>
                    <div className="col-xs-12">
                      <p>
                        <i className="fa fa-fax" /> Fax: {data.fax}
                      </p>
                    </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default a2islistadapter;
