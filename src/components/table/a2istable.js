import React, { Component } from 'react';  
import '../../css/a2is.css'; 

class a2istable extends Component {
  constructor(props)
  {
    super()
    this.state={
      items:props.dataitems,
      header:props.header || [],
      rowcount:props.rowscount||0
    }
  }
 
  render() {
      
    var pg=this.state.rowcount;
    let data= this.state.items.map(function(text, index){
      if(index<pg)
      {   
        return <tr key={index}> 
                {
                    text.map(function(colValue, id){
                        return <td key={id}>{colValue}</td>;
                    })
                }
         </tr>;
      } 
    },this.pg);

    let header=this.state.header.map(function(name, index){
        return <th key={index} nowrap="" style={{height:"26px"}}  ><span >{name}</span><button  style={{margin:"-5px 5px -5px 2px", display: "none"}} className="btn btn-xs btn-default"><span  className="fa fa-sort"></span></button></th>;
    });

    return (
      <div className="col-xs-12">
          <div className="panel panel-default " style={{overflow: "scroll auto",margin: "0px 10px 15px -1px",padding: "0px",minHeight: "257px"}}>
                <table className="table table-striped table-condensed fixed_header"   style={{marginBottom:"0",width:"auto"}}>
                  <thead>
                      <tr>
                          {/* <th style={{textAlign:"center",minWidth:"28px",width:"28px"}}><input type="checkbox"/></th> */}
                          {header}
                      </tr>
                  </thead>
                  <tbody style={{height:(this.state.rowcount*26)+"px"}} >{data}</tbody>
              </table>
              <div className="btn-group-vertical btn-group-xs a2is-datatable-menu">
                  <button className="btn btn-default "  ><span className="fa fa-plus" data-toggle="tooltip" data-placement="left" title="" data-original-title="Create new"></span></button>
                  <button className="btn btn-default "  ><span className="fa fa-pencil" data-toggle="tooltip" data-placement="left" title="" data-original-title="Edit"></span></button>
                  <button className="btn btn-default "  ><span className="fa fa-minus" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete"></span></button>
              </div>
          </div>
      </div>
    );
  }
}

export default a2istable;
