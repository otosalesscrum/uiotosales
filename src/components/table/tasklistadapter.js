import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../../css/a2is.css";
import {
  NeedFu,
  Potential,
  CallLater,
  CollectDoc,
  NotDeal,
  SendToSA,
  PolicyCreated,
  BackToAO
} from "../../assets";
import { Util } from "../../otosalescomponents";
import { secureStorage } from "../../otosalescomponents/helpers/SecureWebStorage";

class tasklistadapter extends Component {
  constructor(props) {
    super(props);
  }

  onClickItem = id => {
    this.props.onClickItem(id);
  };

  formatDate = dates => {
    var dd = dates.getDate();
    var mm = dates.getMonth(); //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }

    var mL = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    var mS = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ];
    return dd + " " + mS[mm] + " " + yyyy;
  };

  formatTimeFromDate = dates => {
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    var ss = dates.getMinutes();

    return hh + ":" + mm;
  };

  render() {
    const iconStatus = status => {
      if (status == 1) {
        return NeedFu;
      } else if (status == 2) {
        return Potential;
      } else if (status == 3) {
        return CallLater;
      } else if (status == 4) {
        return CollectDoc;
      } else if (status == 5) {
        return NotDeal;
      } else if (status == 6) {
        return SendToSA;
      } else if (status == 7) {
        return PolicyCreated;
      } else if (status == 8) {
        return BackToAO;
      } else if (status == 9) {
        return NeedFu;
      } else {
        return null;
      }
    };

    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((data, index) => {
      var expired = "";
      var timeIn = "";
      var diff = "";

      if (data.status == 9) {
        diff =
          (Util.convertDate(this.props.historyPenawaranList[index].ExpiredDate) -
            Util.convertDate()) /
          60000;

        if (diff >= 60) {
          timeIn = "hour";
          diff /= 60;
        } else {
          timeIn = "minute";
        }

        expired = diff > 0 ? diff : 0;
        timeIn = diff > 1 ? timeIn + "s" : timeIn;
      }

      if (index < pg) {
        return data.status != 9 ? (
          <div
            className="col-md-12 col-xs-12"
            key={index}
            style={{ fontSize: "9pt" }}
          >
            <div className="panel panel-default">
              <div className="panel-body panel-body-list">
                <div className="col-xs-12 panel-body-list">
                  <div
                    className="col-xs-10"
                    onClick={() => this.props.onClickListItem(data.CustID)}
                  >
                    <img
                      src={iconStatus(data.status)}
                      className="pull-left"
                      style={{ width: "35px", marginRight: "5px" }}
                      alt=""
                    />
                    <h4>
                      <b>{data.name}</b>
                    </h4>
                  </div>
                  <div
                    className="col-xs-2"
                    onClick={() => this.onClickItem(data.id)}
                  >
                    <i
                      className="pull-right fa fa-bars fa-lg"
                      style={{ marginTop: "10px" }}
                    />
                  </div>
                </div>
                <div
                  className="col-xs-12 col-md-12 panel-body-list"
                  onClick={() => this.props.onClickListItem(data.CustID)}
                >
                  <div className="col-xs-12 col-md-12 panel-body-list">
                    <div className="col-xs-12">
                      <p className="pull-left">
                        <i className="fa fa-university" /> Last Follow Up
                      </p>
                      <p className="pull-right">
                        <i className="fa fa-calendar-o" />
                        <b>
                          {" "}
                          {data.lastfollow == "null"
                            ? "No Data"
                            : Util.convertDate(data.lastfollow) == "Invalid Date"
                            ? data.lastfollow
                            : this.formatDate(Util.convertDate(data.lastfollow)) +
                              " " +
                              this.formatTimeFromDate(
                                Util.convertDate(data.lastfollow)
                              )}
                        </b>
                      </p>
                    </div>
                  </div>
                  <div className="col-xs-12 col-md-12 panel-body-list">
                    <div className="col-xs-12">
                      <p className="pull-left">
                        <i className="fa fa-phone-square" /> Next Follow Up
                      </p>
                      <p className="pull-right">
                        <i className="fa fa-calendar" />{" "}
                        <b>
                          {" "}
                          {data.nextfollow == "null"
                            ? "No Data"
                            : Util.convertDate(data.nextfollow) == "Invalid Date"
                            ? data.nextfollow
                            : this.formatDate(Util.convertDate(data.nextfollow)) +
                              " " +
                              this.formatTimeFromDate(
                                Util.convertDate(data.nextfollow)
                              )}
                        </b>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="col-md-12 col-xs-12" key={index}>
            <div className="panel panel-default">
              <div className="panel-body panel-body-list">
                <div className="col-xs-12 panel-body-list">
                  <div
                    className="col-xs-10"
                    onClick={() => {
                      this.props.onClickListItem(data.CustID);
                      secureStorage.setItem(
                        "followUpDateTimeSelected",
                        this.props.penawaranList[index].followUpDateTime
                      );
                    }}
                  >
                    <img
                      src={iconStatus(data.status)}
                      className="pull-left"
                      style={{ width: "35px", marginRight: "5px" }}
                      alt=""
                    />
                    <h4>
                      {this.props.penawaranList[index].isHotProspect == 1 ? (
                        <b style={{ color: "#D59F00" }}>{data.name}</b>
                      ) : (
                        <b>{data.name}</b>
                      )}
                    </h4>
                  </div>
                  <div
                    className="col-xs-2"
                    onClick={() => this.onClickItem(data.id)}
                  >
                    <i
                      className="pull-right fa fa-bars"
                      style={{ marginTop: "10px" }}
                    />
                  </div>
                </div>
                <div
                  className="col-xs-12 col-md-12 panel-body-list"
                  onClick={() => {
                    this.props.onClickListItem(data.CustID);
                    secureStorage.setItem(
                      "followUpDateTimeSelected",
                      this.props.penawaranList[index].followUpDateTime
                    );
                  }}
                >
                  <div className="col-xs-12 col-md-12 panel-body-list">
                    <div className="col-xs-12">
                      <p className="pull-left">
                        Expired in {Math.round(expired) + " " + timeIn} <br />
                        {this.props.penawaranList[index].followUpDateTime !=
                          null && (
                          <span>
                            PCT{" "}
                            {this.formatDate(
                              Util.convertDate(
                                this.props.penawaranList[index].followUpDateTime
                              )
                            ) +
                              " " +
                              this.formatTimeFromDate(
                                Util.convertDate(
                                  this.props.penawaranList[
                                    index
                                  ].followUpDateTime
                                )
                              )}
                            <br />
                          </span>
                        )}
                        {this.props.penawaranList[index].vehicleDescription}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default tasklistadapter;
