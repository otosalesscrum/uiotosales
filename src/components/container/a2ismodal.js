import React, { Component } from 'react';  

class A2isModal extends Component {
  constructor(props)
  {
    super()
    this.state=({
      modal:true
    })
  }
  componentDidMount(props) {
    console.log(this); 
  }

  render() {
    return (
      <div className="modal fade" id={this.props.id} data-backdrop={this.props.backdrop} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">{this.props.title}</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {this.props.body}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary btn-sm">Save changes</button>
            </div>
          </div>
        </div>
    </div>
    );
  }
}

export default A2isModal;
