import React, { Component } from 'react';
import '../../css/a2is.css';
import PropTypes from 'prop-types';

import Tab from '../container/Tab';

class a2istabs extends Component {
    static propTypes = {
        children: PropTypes.instanceOf(Array).isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            activeTab: this.props.children[0].props.label,
        };
    }

    onClickTabItem = (tab) => {
        this.setState({ activeTab: tab });
    }

    render() {
        const {
            onClickTabItem,
            props: {
                children,
            },
            state: {
                activeTab,
            }
        } = this;

        let styleCustomWidth = {};
        if(children.length == 4){
            styleCustomWidth = {
                width: "25%",
                minWidth: "0px",
            }
        }else if(children.length == 3){
            styleCustomWidth = {
                width: "33.34%",
                minWidth: "0px",
            }
        }else if(children.length == 2){
            styleCustomWidth = {
                width: "50%",
                minWidth: "0px",
            }
        }else if(children.length == 1){
            styleCustomWidth = {
                width: "100%",
                minWidth: "0px",
            }
        }

        return (
            <div className="tabs">
                <ul className="nav nav-tabs">
                    {children.map((child) => {
                        const { label } = child.props;

                        return (
                            <Tab
                                activeTab={activeTab}
                                styleCustomWidth = {styleCustomWidth}
                                key={label}
                                label={label}
                                onClick={onClickTabItem}
                            />
                        );
                    })}
                </ul>
                <div className="tab-content">
                    {children.map((child) => {
                        if (child.props.label !== activeTab) return undefined;
                        return child.props.children;
                    })}
                </div>
            </div>
        );
    }
}

export default a2istabs;