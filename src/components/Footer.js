import React, { Component } from 'react';  
import '../css/a2is.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

class Footer extends Component {
  render() {
    return ( 
        <div className="navbar-fixed-bottom row footerContent otosalesTheme" style={{fontSize:"9pt"}}>
            {/* <span className="col-md-1 hidden-xs hidden-sm otosalesTheme"></span> */}
            <span className="col-xs-6 col-sm-6 col-md-6 text-left otosalesTheme">© {(new Date().getFullYear())} - AsuransiAstra.Com</span>
            <span className="col-xs-6 col-sm-6 col-md-6 text-right otosalesTheme" style={{float:"right", paddingRight:"25px"}}>Service Desk <span className="fa fa-phone"></span> 17800</span>
        </div> 
    );
  }
}

export default Footer;
