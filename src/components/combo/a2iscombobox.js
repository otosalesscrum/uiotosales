import React, { Component } from 'react';  
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'font-awesome/css/font-awesome.min.css';

class a2iscombobox extends Component {
  constructor(props)
  {
    super()
    this.state={
      value:props.value||""
    }
  } 
  onChange=(event)=> {
    this.setState(
        {
          value: event.target.value
        },()=>{
          if(typeof(this.props.onChangeCombo)==="function")
            {
              this.props.onChangeCombo(this.state.value);
            }
        }
      );
  } 
  isRequired=(required)=> {
    if (required) { 
        return <span className="required"> *</span>;
    }
    return null;
  }
  



  render() {
  return (
      <div className="form-group" style={{visibility:(this.props.visible===false) ?"hidden":""}}>
        <label   className="col-xs-12 col-sm-3 col-form-label text-right">{this.props.caption || "Combobox"}{this.isRequired(this.props.required)}</label>
        <div className="col-xs-12 col-sm-9">
          <select className="form-control"
              onChange={this.props.onChange}
              name = {this.props.name}
              value={this.state.value}
              disabled={this.props.disabled || false}
              readOnly={this.props.readonly || false} 
              onChange={this.onChange}
          >
            {
              (this.props.items !=null) ? 
                this.props.items.map(value => 
                  <option key={value.text} value={value.value}>{value.text}</option>
              ) : <option>Please select</option>}
          </select>
        </div>
      </div>
    );
  }
}

export default a2iscombobox;
