import React, { Component } from 'react';  
import "../../css/a2is.css";
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'font-awesome/css/font-awesome.min.css';

class a2isradio extends Component {
  constructor(props)
  {
    super()
    this.state={
      items:props.dataitems || [],
      name:props.name || "",
      readonly:props.readonly||false,
      disabled:props.disabled||false

    };
  }
  isRequired=(required)=> {
    if (required) { 
        return <span className="required"> *</span>;
    }
    return null;
  }
  render() {  
    let data=this.state.items.map(function(description, index){
        return <div key={index} style={{overflow: "hidden"}} className="col-lg-2 col-sm-4">
                  <input type="radio" 
                    disabled={this.state.disabled} 
                    readOnly={this.state.readOnly} 
                    name={this.state.name} 
                    />&nbsp;
                    <span className="smalltext">{description}</span>
              </div>;
    }, this);


    return ( 
      <div style={{padding: "0px", visibility:(this.props.visible===false) ?"hidden":""}} className="form-group form-group-sm col-sm-12">
          <label className="control-label col-sm-3  text-right">
                  <span>{this.props.caption || "Radio buttons"}</span> 
                  {this.isRequired(this.props.required)}
          </label>
          <div className="col-sm-9">
                  <div style={{padding:"0"}}>{data}  </div> 
                  <div className="clearfix"></div>
          </div>
      </div>
    );
  }
}

export default a2isradio;
