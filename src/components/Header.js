import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import "../css/a2is.css";
import { Util, Log } from "../otosalescomponents";
import { API_URL, API_VERSION, API_V2, ACCOUNTDATA } from "../config";
import { secureStorage } from "../otosalescomponents/helpers/SecureWebStorage";

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNameShow: false
    };
  }

  componentDidMount() {
    this.checkname = setInterval(() => {
      if (ACCOUNTDATA != undefined) {
        if (document.body.classList.contains("sidebar-open")) {
          this.setState({ isNameShow: true });
        } else {
          this.setState({ isNameShow: false });
        }
      } else {
        clearInterval(this.checkname);
      }
    }, 100);
  }

  componentWillUnmount(){
    clearInterval(this.checkname);
  }

  // searchDataHitApi = () => {
  //   const { search } = this.state;
  //   this.setState({ isLoading: true });
  //   Util.fetchAPIAdditional(
  //       `${API_URL+""+API_V2}/OrderApproval/populateOrderApproval`, "POST",
  //       {
  //         "Content-Type": "application/x-www-form-urlencoded",
  //         "Authorization": "Bearer "+JSON.parse(ACCOUNTDATA).Token
  //       },
  //       {userID: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID}
  //   )
  //     .then(res => res.json())
  //     .then(jsn =>{
  //       secureStorage.setItem("countOrderApproval", jsn.Data.length || 0);
  //     }
  //     )
  //     .catch(error => {
  //       Log.error("Error Header.js On searchDataHitApi from /populateOrderApproval => ", error);
  //       // console.log("parsing failed", error);
  //       this.setState({
  //         isLoading: false
  //       });
  //       if(!(error+"".toLowerCase().includes("token"))){
  //         // this.searchDataHitApi();
  //       }
  //     });
  // }

  render() {
    return (
      <header className="main-header">
        <Link to="/" className="logo hidden-xs">
          <span className="logo-mini">
            <img
              src="img/logo.bak.png"
              className="img-circle"
              style={{ height: "40px" }}
            />
          </span>
          <span className="logo-lg">
            <img
              src="img/logo.bak.png"
              className="img-circle"
              style={{ height: "30px" }}
            />
            <img
              src="img/otosales.png"
              style={{ height: "40px", marginLeft: "10px" }}
            />
          </span>
        </Link>
        <nav className="navbar navbar-static-top">
          <a
            // onClick={() => this.searchDataHitApi()}
            href="#"
            className="sidebar-toggle"
            data-toggle="push-menu"
            role="button"
          >
            <span className="sr-only">Toggle navigation</span>
          </a>
          <a className="navbar-brand hidden-xs" style={{marginLeft:"-5px"}}>
              <img
                src="img/mr_smile.png"
                className="img pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              <span style={{verticalAlign:"sub"}}>{JSON.parse(ACCOUNTDATA).UserInfo.User.Name}</span>
            </a>
          {this.state.isNameShow == true ? (
            <a className="navbar-brand hidden-lg" style={{marginTop:"-5px", marginLeft:"-5px"}}>
              <img
                src="img/mr_smile.png"
                className="img pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              <span style={{verticalAlign:"sub"}}>{JSON.parse(ACCOUNTDATA).UserInfo.User.Name}</span>
            </a>
          ) : (
            <a className="navbar-brand hidden-lg hidden-sm hidden-md" style={{marginLeft:"-5px"}}>
              <img
                src="img/logoonly.png"
                className="img pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              {
                this.props.titles != undefined ? <span style={{fontSize:"medium"}}>{this.props.titles }</span>:  
                (<img
                  src="img/otosales.png"
                  className="img pull-left"
                  style={{ height: "30px", marginRight: "10px" }}
                />)
              }
            </a>
          )}
          <div className="navbar-custom-menu" style={{marginRight:"20px"}}>
          {this.state.isNameShow == true ? "":this.props.children}
          </div>
          {!this.state.isNameShow && (this.props.additionalDiv != undefined) && this.props.additionalDiv}
        </nav>
      </header>
    );
  }
}


Header.propTypes = {
  additionalDiv: PropTypes.object,
  titles: PropTypes.string
};
