// import A2isLabel from './text/a2islabel'; 
// import A2isText from './text/a2istext.js';
// import A2isTextArea from './text/a2istextarea';
// import A2isCombo from './combo/a2iscombobox';
// import A2isRadio from './combo/a2isradio';
// import A2isCheckbox from './combo/a2ischeckbox';
// import A2isButton from './button/a2isbutton';
// import A2isPager from './button/a2ispager';
// import A2isTable from './table/a2istable';
// import A2isList from './table/a2islist';
// import A2isDate from './picker/datepicker';
// import A2isTime from './picker/timepicker';
// import A2isDateTime from './picker/datetimepicker';
// import A2isTabs from './container/a2istabs';
// import A2isModal from './container/a2ismodal';
// import A2isListAdapter from "./table/a2islistadater";
// import TaskListAdapter from "./table/tasklistadapter";
// import A2isProgressBar from './loader/a2isprogressbar';


import { lazy } from "react";
import * as Util from "./../otosalescomponents/helpers/Util";

const A2isLabel = lazy(() => Util.retry(() => import("./text/a2islabel")));
const A2isText = lazy(() => Util.retry(() => import("./text/a2istext.js")));
const A2isTextArea = lazy(() => Util.retry(() => import("./text/a2istextarea")));
const A2isCombo = lazy(() => Util.retry(() => import("./combo/a2iscombobox")));
const A2isRadio = lazy(() => Util.retry(() => import("./combo/a2isradio")));
const A2isCheckbox = lazy(() => Util.retry(() => import("./combo/a2ischeckbox")));
const A2isButton = lazy(() => Util.retry(() => import("./button/a2isbutton")));
const A2isPager = lazy(() => Util.retry(() => import("./button/a2ispager")));
const A2isTable = lazy(() => Util.retry(() => import("./table/a2istable")));
const A2isList = lazy(() => Util.retry(() => import("./table/a2islist")));
const A2isDate = lazy(() => Util.retry(() => import("./picker/datepicker")));
const A2isTime = lazy(() => Util.retry(() => import("./picker/timepicker")));
const A2isDateTime = lazy(() => Util.retry(() => import("./picker/datetimepicker")));
const A2isTabs = lazy(() => Util.retry(() => import("./container/a2istabs")));
const A2isModal = lazy(() => Util.retry(() => import("./container/a2ismodal")));
const A2isListAdapter = lazy(() => Util.retry(() => import("./table/a2islistadater")));
const TaskListAdapter = lazy(() => Util.retry(() => import("./table/tasklistadapter")));
const A2isProgressBar = lazy(() => Util.retry(() => import("./loader/a2isprogressbar")));
 
export {
  A2isLabel,
  A2isText,
  A2isTextArea,
  A2isCombo,
  A2isRadio,
  A2isCheckbox,
  A2isButton,
  A2isPager,
  A2isTable,
  A2isList,
  A2isListAdapter,
  A2isDate,
  A2isDateTime,
  A2isTime,
  A2isTabs,
  A2isModal,
  A2isProgressBar,
  TaskListAdapter
}; 
