import React,{Component} from "react";
import ReactLoading from "react-loading";
import { Section, Article } from "./generic";


class a2isloader extends Component {
    render(){
        return <Section>
            <Article key={this.props.name}>
                <ReactLoading type={this.props.name} color="#fff" />
            </Article> 
        </Section>
    }
}

export default a2isloader;
