import React, {Component} from 'react'
import ProgressBar from 'react-bootstrap/ProgressBar';

//https://react-bootstrap.github.io/components/progress/
class a2isprogressbar extends Component
{ 
    render(){
        return <div className="form-group" > 
                    <div className="col-xs-12" style={{paddingLeft:"0px", paddingRight: "0px", marginBottom:"0px"}}>
                        <ProgressBar 
                            now={(this.props.now && this.props.now>0 )?this.props.now:0}  
                            min={(this.props.min && this.props.min<0 )?0:this.props.min}  
                            max={(this.props.max && this.props.max>100 )?100:this.props.max} 
                            label={(this.props.label)?this.props.label : ""}
                            variant={(this.props.variant)?this.props.variant : "info"}
                            striped={(this.props.striped)?this.props.striped : false}
                            animated={(this.props.animated)?this.props.animated : false} 
                            />
                    </div>
                </div>
    }
}
export default a2isprogressbar


