import React from "react";
import App from "./App";

import "react-toastify/dist/ReactToastify.css";
import "./css/a2is.css";
import "react-block-ui/style.css";
import "loaders.css/loaders.min.css";
import "react-image-lightbox/style.css";
import "./otosalescomponents/style.css";
import "font-awesome/css/font-awesome.min.css";
import "./otosalescomponents/otosalesdatepickerstyle.css";
import { Util, OtosalesModalPWAPrompt, OtosalesModalAlert } from "./otosalescomponents";

class UIOtosales extends App {
  constructor(props) {
    super(props);
    this.state = {
      networkstatus: true,
      showPWAPrompt: false
    };
  }

  componentDidMount() {
    window.addEventListener("online", this.updateIndicator.bind(this));
    window.addEventListener("offline", this.updateIndicator.bind(this));
    this.updateIndicator();

    // window.addEventListener("keydown", this.logKey);
    window.addEventListener("keyup", this.logKey);
    // window.addEventListener("keypress", this.logKey);

    ///// FORCE AND CUSTOM BANNER PWA
    // window.addEventListener("beforeinstallprompt", e => {
    //   // Stash the event so it can be triggered later.
    //   this.deferredPrompt = e;
    //   if(this.isMobileOS()){
    //     this.setState({ showPWAPrompt: true });
    //   }
    // });
  }

  addToHomeScreen = () => {
    this.deferredPrompt.prompt(); // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the A2HS prompt");
        this.deferredPrompt = null;
      } else {
        window.addEventListener("beforeinstallprompt", e => {
          // Stash the event so it can be triggered later.
          this.deferredPrompt = e;
          if(this.isMobileOS()){
            this.setState({ showPWAPrompt: true });
          }
        });
      }

      this.deferredPrompt = null;
    }).catch(error => {
      console.error("error pwa : "+error);
      window.addEventListener("beforeinstallprompt", e => {
        // Stash the event so it can be triggered later.
        this.deferredPrompt = e;
        if(this.isMobileOS()){
          this.setState({ showPWAPrompt: true });
        }
      });
    });
  };

  isMobileOS = () => {
    console.log("OS Detection :",navigator.appVersion);
    var result = true;
    if (Util.stringArrayContains((navigator.appVersion+""), ["win", "macintosh"])){
      result = false;
    }
    return result;
  }

  logKey = ev => {
    var a;
    a = ev.keyCode;
    if (a == 44) {
      alert("Access restricted print screen!!");
      this.stopPrntScr();
    }
    return false;
  };

  stopPrntScr = () => {
    var inpFld = document.createElement("input");
    inpFld.setAttribute("value", ".");
    inpFld.setAttribute("width", "0");
    inpFld.style.height = "0px";
    inpFld.style.width = "0px";
    inpFld.style.border = "0px";
    document.body.appendChild(inpFld);
    inpFld.select();
    document.execCommand("copy");
    inpFld.remove(inpFld);
  };

  updateIndicator = () => {
    this.setState({ networkstatus: navigator.onLine });
  };

  render() {
    return (
      <div>
        {!this.state.networkstatus && (
          <div className="connectioninformationnetwork">
            Please check your internet connection{" "}
          </div>
        )}

        {this.state.showPWAPrompt && (
          <OtosalesModalPWAPrompt
            title="Garda Mobile OtoSales PWA"
            btnno="No"
            btnyes="Yes"
            showModalInfo={this.state.showPWAPrompt}
            onClose={() => {
              this.setState({
                showPWAPrompt: false
              })
            }}
            onSubmit={() => {
              this.addToHomeScreen();
              this.setState({
                showPWAPrompt: false
              })
            }}
            message="Add Garda Mobile OtoSales to your home screen ?"
          />
        )}
        <App
          //region layout
          networkstatus={this.state.networkstatus}
          menu={this.props.menu}
          onSignOut={this.props.onSignOut}
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      </div>
    );
  }
}

export default UIOtosales;
