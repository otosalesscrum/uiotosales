import { secureStorage } from "./otosalescomponents/helpers/SecureWebStorage";


let hostname = window.location.href+"";
let API_URL_Temp = "https://cisadane-dev.asuransiastra.com/Otosales-Dev";
API_URL_Temp = "https://cisadane-qc.asuransiastra.com/Otosales";
// API_URL_Temp = "https://publicapi-qc.gardaoto.com/Otosales";
API_URL_Temp = "http://localhost/Otosales";
// API_URL_Temp = "http://172.16.92.36/Otosales";
// API_URL_Temp = "http://172.16.92.36/Otosales";
// API_URL_Temp = "https://publicapi.gardaoto.com/Otosales";
// API_URL_Temp = 'https://publicapi.gardaoto.com/Otosales';
// API_URL_Temp = "http://10.1.2.76/Otosales";
let IS_DEBUG_Temp = false;

let TOKENAPI ="";

let ACCOUNTDATA_TEMP = secureStorage.getItem("account");
let AVAYAPIN_TEMP = "42685";

let isActiveErrorBoundary = false;

let URL_GEN5_TEMP = "https://cisadane-dev.asuransiastra.com/OtosalesGen5";

URL_GEN5_TEMP = "https://172.16.92.62/retail/UI/Root/home?trxID=";

URL_GEN5_TEMP = "https://localhost/retail/UI/Root/home?trxID=";
URL_GEN5_TEMP = "https://otosalesgen5-fay.gardaoto.com/retail/UI/root/home?trxID=";


if(hostname.includes("otosales-qc.gardaoto.com")){
    API_URL_Temp = 'https://publicapi-qc.gardaoto.com/Otosales';
    IS_DEBUG_Temp = false;
    AVAYAPIN_TEMP = null;
    URL_GEN5_TEMP = "https://otosalesgen5-qc.gardaoto.com/retail/UI/root/home?trxID=";
    isActiveErrorBoundary = true;
}

if(hostname.includes("otosales-centos-qc.gardaoto.com")){
    API_URL_Temp = 'https://publicapi-qc.gardaoto.com/Otosales';
    IS_DEBUG_Temp = false;
    AVAYAPIN_TEMP = null;
    URL_GEN5_TEMP = "https://otosalesgen5-qc.gardaoto.com/retail/UI/root/home?trxID=";
    isActiveErrorBoundary = true;
}

if(hostname.includes("otosales.gardaoto.com")){
    API_URL_Temp = 'https://publicapi.gardaoto.com/Otosales';
    IS_DEBUG_Temp = false;
    AVAYAPIN_TEMP = null; 
    URL_GEN5_TEMP = "https://otosalesgen5.gardaoto.com/retail/UI/root/home?trxID=";
    isActiveErrorBoundary = true;
}

if(hostname.includes("otosales-centos.gardaoto.com")){
    API_URL_Temp = 'https://publicapi.gardaoto.com/Otosales';
    IS_DEBUG_Temp = false;
    AVAYAPIN_TEMP = null;
    URL_GEN5_TEMP = "https://otosalesgen5.gardaoto.com/retail/UI/root/home?trxID=";
    isActiveErrorBoundary = true;
}

export const API_URL = API_URL_Temp;
export const IS_ACTIVE_ERROR_BOUNDARY = isActiveErrorBoundary;

if(ACCOUNTDATA_TEMP == null){
    var intervalRun = setInterval(() => {
        if(secureStorage.getItem("account") != undefined){
            TOKENAPI = JSON.parse(secureStorage.getItem("account")).Token;
            ACCOUNTDATA_TEMP = secureStorage.getItem("account");
            clearInterval(intervalRun);
        }
    });
}

if(secureStorage.getItem("account") != undefined){
    TOKENAPI = JSON.parse(secureStorage.getItem("account")).Token;
    ACCOUNTDATA_TEMP = secureStorage.getItem("account");
}

export const HEADER_API = {
    "Content-Type": "application/x-www-form-urlencoded",
    Authorization:
      "Bearer " + TOKENAPI
  };
export const API_VERSION = "/api/vWeb";
export const API_VERSION2 = "/api/vWeb2";
export const API_VERSION_2 = "/api/vWeb2";
export const API_VERSION_0213URF2019 = "/api/v0213URF2019";
export const API_VERSION_0232URF2019 = "/api/v0232URF2019";
export const API_VERSION_0219URF2019 = "/api/v0219URF2019";
export const API_VERSION_0008URF2020 = "/api/v0008URF2020";
export const API_VERSION_0001URF2020 = "/api/v0001URF2020";
export const API_VERSION_0063URF2020 = "/api/v0063URF2020";
export const API_VERSION_0090URF2020 = "/api/v0090URF2020";
export const API_VERSION_0107URF2020 = "/api/v0107URF2020";
export const API_VERSION_FAYTEST = "/api/FAYTest";
export const API_V2 = "/api/v2";
export const API_V0213URF2019 = "/api/v0213URF2019";
export const FCMmessagingSenderId = "643082169723";
export const FCMKey = "AIzaSyD3gVS_MwJZFWrnCMjRjYK66Bp4p9_n7o8";
export const DeviceType = 2;
export const APP_VERSION = "1.0.0";

export const URL_GEN5 = URL_GEN5_TEMP;

export const ACCOUNTDATA = ACCOUNTDATA_TEMP;

export const API_RETRY_LIMIT = 5;

export const IS_DEBUG = IS_DEBUG_Temp;

export const AVAYAPIN = AVAYAPIN_TEMP;
