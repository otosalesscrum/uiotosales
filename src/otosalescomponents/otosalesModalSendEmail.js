import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";
import { Util, Log } from "../otosalescomponents";

export default class otosalesModalSendEmail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: this.props.state.Email1
    }

    
    Log.debugStr("CONSTRUCTORthis.props.state.Email1 on Render ModalSendEmail => " + this.props.state.Email1);
    Log.debugStr("this.state.email on Render ModalSendEmail => " + this.state.email);

  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  componentDidUpdate(prevProps){
    if(this.props.state.Email1 !== prevProps.state.Email1){
      this.state.email = this.props.state.Email1
    }
  }

  render() {
    return (
      <div>
        <Modal
          styles={{
            modal:{
              width: "80%",
              maxWidth: "500px"
            },
            overlay: {
              zIndex: 1050
            }
          }}
          showCloseIcon={false}
          open={this.props.state.showModalSendEmail}
          onClose={() => this.props.onCloseModalSendEmail()}
          center
        >
          <div className="modal-content panel panel-primary" style={{ margin: "-15px" }}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title pull-left" id="exampleModalLabel">
                <b>Send Email</b>
              </h4>
              <div className="dropdown pull-right">
                {/* <button className="btn btn-primary" type="button"> */}
                  <i
                    className="fa fa-send fa-lg"
                    onClick={() => {
                      if (Util.validateEmail(this.props.state.Email1) || Util.validateEmail(this.state.email)) {
                        this.props.sendQuotationEmail(
                          this.props.state.selectedQuotation,
                          this.state.email
                        );
                        this.props.onCloseModalSendEmail();
                      } else {
                        Log.debugStr("EMAIL TIDAK VALID: " + this.props.state.Email1);
                        toast.warning("❗ Email Tidak Valid", {
                          position: "top-right",
                          autoClose: 5000,
                          hideProgressBar: false,
                          closeOnClick: true,
                          pauseOnHover: true,
                          draggable: true
                        });
                      }
                    }}
                  />
                {/* </button> */}
              </div>
            </div>
            <div className="modal-body">
              <textarea
                type="email"
                name="email"
                onChange={event => this.onChangeFunction(event)}
                value={this.state.email}
                placeholder="enter email"
                className="form-control"
                style={{resize:"none"}}
              />
              <p style={{textAlign: "right"}}>email1@domain.com; email2@domain.com{}</p>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
