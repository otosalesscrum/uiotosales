import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { withRouter } from "react-router-dom";
class otosalesmodalaccessories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      action: null
    };
  }

  formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  };
  render() {
    var pg = this.props.dataAccessories.length || 0;
    let texts = this.props.dataAccessories.map((data, index) => {
      if (index < pg) {
        return (
          <div
            className="form-group separatorBottom"
            style={{ marginTop: "10px", marginBottom: "10px" }}
          >
            <div
              className="row"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-7">
                <span className="smalltext" style={{ fontSize: "10pt" }}>
                  {data.label}
                </span>
              </div>
              <div className="col-xs-5">
                <span
                  style={{
                    fontSize: "10pt",
                    fontWeight: "500",
                    float: "right"
                  }}
                >
                  {this.formatMoney(data.value, 0)}
                </span>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <Modal
        styles={{
          modal: { width: "100%" },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.props.showModal}
        onClose={() => this.props.onCloseModal()}
        center
        showCloseIcon={false}
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Max Insured Value of Accessories</b>
            </h4>
          </div>
          <div
            className="modal-body"
            style={{ overflowY: "auto", maxHeight: "calc(100vh - 210px)" }}
          >
            {texts}
          </div>
        </div>
      </Modal>
    );
  }
}

export default withRouter(otosalesmodalaccessories);
