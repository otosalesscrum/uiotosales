import React from "react";
import PropTypes from "prop-types";
import Modal from "react-responsive-modal";
import OtosalesLoading from "./v2/loading/otosalesloading";

export default function otosalesmodalinfo(props) {
  return (
    <Modal
      styles={{
        modal: {
          maxWidth: "500px",
          width: "80%"
        },
        overlay: {
          zIndex: 1050
        }
      }}
      open={props.showModalInfo}
      onClose={() => props.onClose()}
      closeOnOverlayClick={false}
      showCloseIcon={false}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-15px" }}
      >
        <OtosalesLoading isLoading={props.isLoading || false}>
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>{props.title || "Information"}</b>
            </h4>
          </div>
          <div
            className="modal-body"
            style={{ fontWeight: "1000", textAlign: "center" }}
          >
            {props.message}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-12">
                <button
                  onClick={() => props.onClose()}
                  className="btn btn-info form-control"
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </OtosalesLoading>
      </div>
    </Modal>
  );
}

otosalesmodalinfo.propTypes = {
  showModalInfo: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string,
  title: PropTypes.string,
  isLoading: PropTypes.bool
};
