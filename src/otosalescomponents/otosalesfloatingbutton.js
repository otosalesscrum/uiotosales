import React from "react";
import { Container, Button, Link } from "react-floating-action-button";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import { toast } from "react-toastify";
import { withRouter } from "react-router-dom";
import {
  NeedFu,
  CallLater,
  Potential,
  CollectDoc,
  SendToSA,
  NotDeal,
  PolicyCreated,
  BackToAO
} from "../assets";
import { secureStorage } from "./helpers/SecureWebStorage";

function updateStatusFollowUp(followstatus, props) {
  if(window.location.pathname.includes("inputprospect")){
    secureStorage.setItem("stateinput", JSON.stringify(props.state));
  }else if(window.location.pathname.includes("tasklist-edit")){
    secureStorage.setItem("statetasklistedit", JSON.stringify(props.state));
  }
  if (followstatus == 6) {
    console.log("SendtoSA");
    isValidatedSendToSA(props.state, followstatus, props);
  }else{ 
    secureStorage.setItem("FollowUpEditStatus-statusInfo", followstatus);
    setPCFUSelected(props);
    props.history.push("/tasklist-followupdetail");
  }
}

function isValidatedSendToSA(state, followstatus, props) {
  if (
    JSON.parse(secureStorage.getItem("FollowUpSelected")).IdentityCard == null ||
    JSON.parse(secureStorage.getItem("FollowUpSelected")).IdentityCard == ""
  ) {
    toast.dismiss();
    toast.warning("❗ Please complete photo of KTP/KITAS/SIM first.", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
    return false;
  }

  if (
    (JSON.parse(secureStorage.getItem("FollowUpSelected")).STNK == null ||
    JSON.parse(secureStorage.getItem("FollowUpSelected")).STNK == "") &&
    (JSON.parse(secureStorage.getItem("FollowUpSelected")).BSTB1 == null ||
    JSON.parse(secureStorage.getItem("FollowUpSelected")).BSTB1 == "")
  ) {
    toast.dismiss();
    toast.warning("❗ Please complete photo of STNK or BSTB first.", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
    return false;
  }

  if (JSON.parse(secureStorage.getItem("FollowUpSelected")).SPPAKB == null || JSON.parse(secureStorage.getItem("FollowUpSelected")).SPPAKB == "") {
    toast.dismiss();
    toast.warning("❗ Please complete photo of SPPAKB first.", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
    return false;
  }

  if (JSON.parse(secureStorage.getItem("FollowUpSelected")).FollowUpStatus == "28" || JSON.parse(secureStorage.getItem("FollowUpSelected")).FollowUpStatus == 28) {
    toast.dismiss();
    toast.warning(
      "❗ You could not send document of this follow up because the policy status on follow up is already awaiting approval.",
      {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      }
    );
    return false;
  }

  fetch(`${API_URL + "" + API_VERSION}/DataReact/isValidatedSendToSA/`, {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization:
        "Bearer " + JSON.parse(ACCOUNTDATA).Token
    }),
    body: "followupno=" + state.FollowUpNumber
  })
    .then(res => res.json())
    .then(jsn => {
      if (jsn.vaildToSA == 1) {
        // toast.info("✔️ NEXT CHANGE STEP READ SEND TO SA", {
        //   position: "top-right",
        //   autoClose: 5000,
        //   hideProgressBar: false,
        //   closeOnClick: true,
        //   pauseOnHover: true,
        //   draggable: true
        // });

        //////////// NEXT SEND TO SA
        secureStorage.setItem("FollowUpEditStatus-statusInfo", followstatus);
        setPCFUSelected(props);
        props.history.push("/tasklist-followupdetail");
        return true;
      } else {
        toast.dismiss()
        toast.warning("❗ " + jsn.toast, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        return false;
      }
    })
    .catch(error => {
      console.log("parsing failed", error);
      toast.dismiss();
      toast.error("❗ Something error - " + error, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return false;
    });
}

function setPCFUSelected(props) {
  secureStorage.setItem(
    "PCSelected",
    secureStorage.getItem("ProspectCustomerSelected")
  );
  let FollowUpSelected = JSON.parse(secureStorage.getItem("FollowUpSelected"));
  FollowUpSelected = {...FollowUpSelected, FollowUpNo : props.state.FollowUpNumber};

  secureStorage.setItem("FUSelected", JSON.stringify(FollowUpSelected));
}

export default withRouter(function otosalesfloatingbutton(props) {
  var isNeedFu = true;
  var isPotential = true;
  var isCallLater = true;
  var isCollectDoc = true;
  var isNotDeal = true;
  var isSendToSA = true;
  var iconStatus = null;

  if (props.FollowUpStatus == 1) {
    iconStatus = NeedFu;
  } else if (props.FollowUpStatus == 2) {
    isNeedFu = false;
    iconStatus = Potential;
  } else if (props.FollowUpStatus == 3) {
    isNeedFu = false;
    iconStatus = CallLater;
  } else if (props.FollowUpStatus == 4) {
    isNeedFu = false;
    iconStatus = CollectDoc;
  } else if (props.FollowUpStatus == 5) {
    isNeedFu = false;
    iconStatus = NotDeal;
  } else if (props.FollowUpStatus == 6) {
    isNeedFu = false;
    isCallLater = false;
    isNotDeal = false;
    isCollectDoc = false;
    isPotential = false;
    iconStatus = SendToSA;
  } else if (props.FollowUpStatus == 7) {
    isNeedFu = false;
    isCallLater = false;
    isNotDeal = false;
    isCollectDoc = false;
    isPotential = false;
    isSendToSA = false;
    iconStatus = PolicyCreated;
  } else if (props.FollowUpStatus == 8) {
    isNeedFu = false;
    isCallLater = false;
    isNotDeal = false;
    isCollectDoc = false;
    isPotential = false;
    iconStatus = BackToAO;
  } else {
    iconStatus = NeedFu;
  }

  return (
    <Container styles={{ zIndex: "1049" }}>
      {isNeedFu && (
        <Link
        
          tooltip="Need Follow Up"
          styles={{ backgroundColor: "transparent" }}
        >
          <img
            onClick={() => updateStatusFollowUp(1, props)}
            src={NeedFu}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {isCallLater && (
        <Link tooltip="Call Later">
          <img
            onClick={() => updateStatusFollowUp(3, props)}
            src={CallLater}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {isPotential && (
        <Link tooltip="Potential">
          <img
            onClick={() => updateStatusFollowUp(2, props)}
            src={Potential}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {isCollectDoc && (
        <Link tooltip="Collect Doc">
          <img
            onClick={() => updateStatusFollowUp(4, props)}
            src={CollectDoc}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {isSendToSA && (
        <Link tooltip="Send to SA">
          <img
            onClick={() => updateStatusFollowUp(6, props)}
            src={SendToSA}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {isNotDeal && (
        <Link tooltip="Not Deal">
          <img
            onClick={() => updateStatusFollowUp(5, props)}
            src={NotDeal}
            style={{ width: "100%" }}
          />
        </Link>
      )}
      {/* <Link
                tooltip="Add user link"
                icon="fa fa-user-plus"
                className="fab-item btn btn-link btn-lg text-white"
                /> */}
      <Button
        // tooltip="The big plus button!"
        // icon="fa fa-plus"
        // rotate={true}
        styles={{ backgroundColor: "transparent" }}
      >
        <img src={iconStatus} style={{ width: "100%" }} />
      </Button>
    </Container>
  );
});
