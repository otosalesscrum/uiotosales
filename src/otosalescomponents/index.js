import { lazy } from "react";
import * as Util from "./helpers/Util.js"
import * as Log from "./helpers/Logger.js";

const AdapterTaskList = lazy(() => Util.retry(() => import( "./v2/list/AdapterTaskList")));
const AdapterTaskListLead = lazy(() => Util.retry(() => import( "./v2/list/AdapterTaskListLead")));
const Tabs = lazy(() => Util.retry(() => import( "./Tabs")));
const OtosalesSelect2 = lazy(() => Util.retry(() => import( "./otosalesselect2")));
const OtosalesList = lazy(() => Util.retry(() => import( "./otosaleslist")));
const OtosalesListProspectLead = lazy(() => Util.retry(() => import( "./otosaleslistprospectlead")));
// const OtosalesListOrderApproval = lazy(() => Util.retry(() => import( "./otosaleslistorderapproval")));
const OtosalesListOrderApproval = lazy(() => Util.retry(() => import( "./v2/list/OtoSalesOrderApproval")));
const OtosalesListFollowUpHistory = lazy(() => Util.retry(() => import( "./otosaleslistfollowuphistory")));
const OtosalesDatePicker = lazy(() => Util.retry(() => import( "./otosalesdatepicker")));
const OtosalesTimePicker = lazy(() => Util.retry(() => import( "./otosalestimepicker")));
const OtosalesDropzone = lazy(() => Util.retry(() => import( "./otosalesdropzone")));
const OtosalesDropzonev2 = lazy(() => Util.retry(() => import( "./v2/dropzone")));
const OtosalesQuotationHistory = lazy(() => Util.retry(() => import( "./otosalesquotationhistory")));
const OtosalesModalSenttoSA = lazy(() => Util.retry(() => import( "./otosalesmodalsenttosa")));
// const OtosalesModalTaskListFilter = lazy(() => Util.retry(() => import( "./otosalesmodaltasklistfilter")));
const OtosalesModalTaskListFilter = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalTaskListFilter")));
const OtosalesModalPremiumSimulation = lazy(() => Util.retry(() => import( "./otosalesmodalpremiumsimulation")));
const OtosalesListDashboard = lazy(() => Util.retry(() => import( "./otosaleslistdashboard")));
const OtosalesDashboardTabsHandler = lazy(() => Util.retry(() => import( "./otosalesdashboardtabshandler")));
const OtosalesModalDashboard = lazy(() => Util.retry(() => import( "./otosalesmodaldashboard")));
const OtosalesDashboardPersonal = lazy(() => Util.retry(() => import( "./otosalesdashboardpersonal")));
const OtosalesFloatingButton = lazy(() => Util.retry(() => import( "./otosalesfloatingbutton")));
const OtosalesModalSendEmail = lazy(() => Util.retry(() => import( "./otosalesModalSendEmail")));
const OtosalesModalSendSMS = lazy(() => Util.retry(() => import( "./otosalesmodalsendsms")));
// const OtosalesNotification = lazy(() => Util.retry(() => import( "./otosalesnotification")));
const OtosalesModalAccessories = lazy(() => Util.retry(() => import( "./otosalesmodalaccessories")));
const OtosalesModalInfo = lazy(() => Util.retry(() => import( "./otosalesmodalinfo")));
const OtosalesSelectFullview = lazy(() => Util.retry(() => import( "./otosalesselectfullview")));
const OtosalesModalAlert = lazy(() => Util.retry(() => import( "./otosalesmodalalert")));
const OtosalesModalQuotationHistory = lazy(() => Util.retry(() => import( "./otosalesmodalquotationhistory")));
const Otosalesheader = lazy(() => Util.retry(() => import( "./otosalesheader")));
const HeaderSubContent = lazy(() => Util.retry(() => import( "./v2/header/HeaderSubContent")));
const OtosalesFloatingButtonv2 = lazy(() => Util.retry(() => import( "./v2/floatingbutton")));
// const OtosalesSelectFullviewv2 = lazy(() => Util.retry(() => import( "./v2/select/otosalesselectfullview")));
const OtosalesSelectFullviewv2 = lazy(() => Util.retry(() => import( "./v3/select/otosalesselectfullview")));
// const OtosalesSelectFullviewrev = lazy(() => Util.retry(() => import( "./v2/select/otosalesselectfullviewrev")));
const OtosalesSelectFullviewrev = lazy(() => Util.retry(() => import( "./v3/select/otosalesselectfullviewrev")));
const OtosalesSelect2v2 = lazy(() => Util.retry(() => import( "./v2/select/otosalesselect2")));
const OtosalesModalSetPeriod = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSetPeriod")));
const OtosalesModalSetPeriodPremi = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSetPeriodPremi")));
const OtosalesModalMakeSure = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalMakeSure")));
const OtosalesLightBox = lazy(() => Util.retry(() => import( "./v2/imagelightbox/otosalesllightbox")));
const OtosalesProgressbar = lazy(() => Util.retry(() => import( "./v2/progressbar/otosalesprogressbar")));
const OtosalesModalSendEmailv2 = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSendEmail")));
const OtosalesModalSendSMSv2 = lazy(() => Util.retry(() => import( "./v2/modal/otosalesmodalsendsms")));
const OtosalesLoading = lazy(() => Util.retry(() => import( "./v2/loading/otosalesloading")));
const OtosalesModalSetPeriodPremi2 = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSetPeriodPremi2")));
const OtosalesModalPWAPrompt = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalPWAPrompt")));
const OtosalesModalSendEmailCkEditor = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSendEmailCkEditor")));
const OtosalesModalSendSMSCkEditor = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSendSMSCkEditor")));
const OtosalesModalSendWA = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalSendWA")));
const OtosalesModalInputNumberTelerenewal = lazy(() => Util.retry(() => import( "./v2/modal/OtosalesModalInputNumberTelerenewal")));
const OtosalesDropzoneFile = lazy(() => Util.retry(() => import( "./v2/dropzone/dropzoneFile")));
const OtosalesIFrame = lazy(() => Util.retry(() => import( "./v2/iframe")));
const OtosalesLoadingSegment = lazy(() => Util.retry(() => import( "./v2/loading/otosalesloadingsegment")));
const OtosalesSegmentDropdown = lazy(() => Util.retry(() => import( "./v2/segmentdropdown")));
const OtosalesRadioButton = lazy(() => Util.retry(() => import( "./v2/radiobutton/otosalesradiobutton")));

export {
    AdapterTaskList,
    AdapterTaskListLead,
    Tabs,
    OtosalesSelect2,
    OtosalesList,
    OtosalesListProspectLead,
    OtosalesListOrderApproval,
    OtosalesListFollowUpHistory,
    OtosalesDatePicker,
    OtosalesTimePicker,
    OtosalesDropzone, 
    OtosalesDropzonev2,
    OtosalesQuotationHistory,
    OtosalesModalSenttoSA,
    OtosalesModalTaskListFilter,
    OtosalesModalPremiumSimulation,
    OtosalesListDashboard,
    OtosalesDashboardTabsHandler,
    OtosalesModalDashboard,
    OtosalesDashboardPersonal,
    OtosalesFloatingButton,
    OtosalesModalSendEmail,
    OtosalesModalSendEmailv2,
    OtosalesModalSendSMS,
    OtosalesModalSendSMSv2,
    // OtosalesNotification,
    OtosalesModalAccessories,
    OtosalesModalInfo,
    OtosalesSelectFullview,
    OtosalesModalQuotationHistory,
    OtosalesModalAlert,
    Otosalesheader,
    HeaderSubContent,
    OtosalesFloatingButtonv2,
    OtosalesSelectFullviewv2,
    OtosalesSelectFullviewrev,
    OtosalesModalSetPeriod,
    OtosalesSelect2v2,
    OtosalesModalMakeSure,
    OtosalesLightBox,
    OtosalesProgressbar,
    OtosalesLoading,
    OtosalesModalSetPeriodPremi,
    OtosalesModalSetPeriodPremi2,
    OtosalesModalPWAPrompt,
    OtosalesModalSendEmailCkEditor,
    OtosalesModalSendSMSCkEditor,
    OtosalesModalSendWA,
    OtosalesIFrame,
    OtosalesDropzoneFile,
    OtosalesModalInputNumberTelerenewal,
    OtosalesLoadingSegment,
    OtosalesSegmentDropdown,
    OtosalesRadioButton,
    Util,
    Log
};