import React, { Component } from "react";
import A2isProgressBar from "../components/loader/a2isprogressbar";

class otosalesdashboardpersonal extends Component {
  constructor(props) {
    super(props);
  }

  handleInputChange = newValue => {
    this.props.onOptionsChange(newValue.value);
  };

  render() {
    const maxValue = data => {
      var Max = 0;
      if (data.TotalNeedFu > Max) {
        Max = data.TotalNeedFu;
      }
      if (data.TotalPotential > Max) {
        Max = data.TotalPotential;
      }
      if (data.TotalCallLater > Max) {
        Max = data.TotalCallLater;
      }
      if (data.TotalCollectDoc > Max) {
        Max = data.TotalCollectDoc;
      }
      if (data.TotalNotDeal > Max) {
        Max = data.TotalNotDeal;
      }
      if (data.TotalSendToSA > Max) {
        Max = data.TotalSendToSA;
      }
      if (data.TotalPolicyCreated > Max) {
        Max = data.TotalPolicyCreated;
      }
      if (data.TotalBackToAO > Max) {
        Max = data.TotalBackToAo;
      }
      if (data.TotalDeal > Max) {
        Max = data.TotalDeal;
      }
      return Max;
    };

    var pg = this.props.dataitems[0].length || 0;
    console.log("panjang "+pg);
    let texts = this.props.dataitems[0].map((data, index) => {

      if (index < pg) {
      
    var Max = maxValue(data);
    return (
      <div>
        <legend style={{ fontSize: "12pt" }}>
          {data.TotalProspect} TOTAL PROSPECT
        </legend>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              NOT DEAL
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalNotDeal}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalNotDeal}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              NEED FU
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalNeedFu}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalNeedFu}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              CALL LATER
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalCallLater}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalCallLater}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              POTENTIAL
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalPotential}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalPotential}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              DEAL
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotaDeal}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalDeal}
            </span>
          </div>
        </div>
        <br />
        <legend style={{ fontSize: "12pt" }}>DEAL PROSPECT BREAKDOWN</legend>
        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              COLLECT DOC
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalCollectDoc}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalCollectDoc}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              SENT TO SA
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalSendToSA}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalSendToSA}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              BACK TO AO
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalBackToAo}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalBackToAo}
            </span>
          </div>
        </div>

        <div className="row" style={{ marginLeft: "0px", marginRight: "0px" }}>
          <div className="col-xs-5 col-md-2" style={{textAlign:"right"}}>
            <span className="smalltext" style={{ fontSize: "10pt" }}>
              POLICY CREATED
            </span>
          </div>
          <div className="col-xs-7 col-md-10 panel-body-list">
            {
              <div className="col-xs-10 panel-body-list">
                {" "}
                <A2isProgressBar
                  variant="warning"
                  now={data.TotalPolicyCreated}
                  min={0}
                  max={Max}
                  striped={false}
                  caption=" "
                />
              </div>
            }
            <span
              style={{ fontSize: "10pt", fontWeight: "500", paddingLeft: "10px" }}
            >
              {data.TotalPolicyCreated}
            </span>
          </div>
        </div>
        <br />
        <br />
      </div>
        );
      }
    }, this.pg);
    return (
      <div>
        {texts}
      </div>
    );
  }
  
}

export default otosalesdashboardpersonal;
