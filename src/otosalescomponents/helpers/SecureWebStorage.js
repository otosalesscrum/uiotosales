var SecureStorage = require("secure-web-storage");

var CryptoJS = require("crypto-js");
 
let SECRET_KEY = '0t054l35k3y';
 
let secureStorageTemp = new SecureStorage(localStorage, {
    hash: function hash(key) {
        key = CryptoJS.SHA256(key, SECRET_KEY);
 
        return key.toString();
    },
    encrypt: function encrypt(data) {
        data = CryptoJS.AES.encrypt(data, SECRET_KEY);
 
        data = data.toString();
 
        return data;
    },
    decrypt: function decrypt(data) {
        data = CryptoJS.AES.decrypt(data, SECRET_KEY);
 
        data = data.toString(CryptoJS.enc.Utf8);
 
        return data;
    }
});

if((window.location.href+"").includes("localhost")){
    secureStorageTemp = localStorage;
}


function decryptCryptoJS(data, SECRET_KEY='0t054l35k3y') {
    data = CryptoJS.AES.decrypt(data, SECRET_KEY);

    data = data.toString(CryptoJS.enc.Utf8);

    return data;
}

function encryptCryptoJS(data, SECRET_KEY='0t054l35k3y') {
    data = CryptoJS.AES.encrypt(data, SECRET_KEY);

    data = data.toString();

    return data;
}

let secureStorage = secureStorageTemp;
secureStorage = localStorage;


export {
    secureStorage,
    decryptCryptoJS,
    encryptCryptoJS
}