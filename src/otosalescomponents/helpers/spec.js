import StringTranslations from "./stringTranslation";
import * as Utils from "./Util";

it("string translation", () => {
  expect(StringTranslations("testing")).toBe("You have entered invalid bank account");
});

it("validate email with util 'hdsnfjdsfn@ndfjkdgf.com' tobe true", () => {
  expect(Utils.validateEmail("hdsnfjdsfn@ndfjkdgf.com")).toBe(true);
});
