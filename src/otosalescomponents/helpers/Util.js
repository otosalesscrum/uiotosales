/** Util.js
 *
 * CREATED ON 01/07/2019
 * CREATED BY LCY
 *
 *
 **/

/** Util.js
 *
 * MODIFIED BY FAY
 *
 **/

import { toast } from "react-toastify";
import { func } from "prop-types";
import * as Parameterized from "../../parameterizedata";
import { API_RETRY_LIMIT, API_URL, API_VERSION_0213URF2019, ACCOUNTDATA, HEADER_API, FCMKey } from "../../config";
import StackTrace from "stacktrace-js";
import { secureStorage } from "./SecureWebStorage";

function fetchAPI(URL, method, header, body) {
  if (!isNullOrEmpty(method)) {
    var parsedBody = toFormData(body);
    if (method.toString() == "POST") {
      return fetchPostAPI(URL, header, parsedBody);
    } else if (method.toString() == "GET") {
      return fetchGetAPI(URL, header, parsedBody);
    } else {
      return null;
    }
  } else {
    return null;
  }
}

function fetchPostAPI(URL, header, body) {
  return fetch(URL, {
    method: "POST",
    headers: header,
    body: body
  });
}

function fetchGetAPI(URL, header, body) {
  return fetch(URL, {
    method: "GET",
    headers: header,
    body: body
  });
}

function isNullOrEmpty(str) {
  if (str === undefined || str == "undefined") {
    return true;
  }
  if (str == null) {
    return true;
  }
  if (str == "") {
    return true;
  }
  if (str == "null") {
    return true;
  }
  return false;
}

function valueTrim(str){
  if(isNullOrEmpty(str)){
    return str;
  }

  if(typeof str === "string"){
    return str.trim();
  }

  return str;
}

function isEmptyFunction(f){
  if(/^function [^(]*\(\)[ ]*{(.*)}$/.exec(
    f.toString().replace(/\n/g, "")
    )[1].trim() === ""){
      return true;
  }else{
    return false
  }
}

function toFormData(obj) {
  var form_data = "";
  if (obj != null) {
    if (typeof obj == "object") {
      form_data = Object.keys(obj)
        .map(function(key) {
          return key + "=" + obj[key];
        })
        .join("&");
    }
  }
  return form_data;
}

/**
 * ADDED START BY FAY
 **/
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(
      (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
    ).toString();
    let j = i.length > 3 ? i.length % 3 : 0;

    return (
      negativeSign +
      (j ? i.substr(0, j) + thousands : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
      (decimalCount
        ? decimal +
          Math.abs(amount - i)
            .toFixed(decimalCount)
            .slice(2)
        : "")
    );
  } catch (e) {
    console.log(e);
  }
}

function replaceAll(string, strReplace, strWith) {
  var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  var reg = new RegExp(esc, 'ig');
  return string.replace(reg, strWith);
};

function handleBackButton(props) {
  props.history.goBack();
}

function formatDate(date, format = "yyyy-mm-dd hh:MM:ss") {
  // format dddd, mmmm dS, yyyy, h:MM:ss TT
  var dateFormat = require("dateformat");
  if (isNullOrEmpty(date)) {
    return date;
  }
  if(typeof date == 'string'){
    // if(stringArrayContains((navigator.appVersion+"").toLowerCase(), ["mac", "iphone"])){
    //   date = date.replace(" ", "T");
    //   date = date.includes("T") ? date+"+07:00" : date;
    // }
    date = convertDate(date);
  }
  // console.log("OS Detection :",navigator.appVersion);
  return dateFormat(date, format);
}

 function getTimezoneOffsetByHour() {
  let hour = (new Date().getTimezoneOffset() / 60) * -1;
  if(hour < 10 && hour > 0){
    return "+0"+hour+":00";
  }else if(hour == 0){
    return "00:00";
  }else if(hour > 10){
    return "+"+hour+":00";
  }else{
    return hour+":00";
  }
};

function convertDate(datestring=""){
  if(isNullOrEmpty(datestring)){
    return new Date();
  }else{
    if(stringArrayContains((navigator.appVersion+"").toLowerCase(), ["mac", "iphone"])){
      datestring = datestring.trim().split(" ").length == 2 ? datestring.trim().replace(" ", "T") : datestring.trim();
      datestring = datestring.replace("Z", "");
      // datestring = datestring.includes("T") ? datestring+"+07:00" : datestring;
      datestring = datestring.split("T").length == 2 ? datestring+getTimezoneOffsetByHour() : datestring;
      // datestring = datestring.includes("T") ? datestring+getTimezoneOffsetByHour() : datestring;
    }
    return new Date(datestring);
  }
}

function calculateMinTime(date) {
  var moment = require("moment");
  let isToday = moment(date).isSame(moment(), "day");
  if (isToday) {
    let nowAddOneHour = moment(new Date())
      .add({ hours: 1 })
      .toDate();
    return nowAddOneHour;
  }
  return moment()
    .startOf("day")
    .toDate(); // set to 12:00 am today
}

function formatTimeFromDate(dates) {
  var hh = dates.getHours();
  if (hh < 10) {
    hh = "0" + hh;
  }

  var mm = dates.getMinutes();
  if (mm < 10) {
    mm = "0" + mm;
  }

  var ss = dates.getMinutes();
  if (ss < 10) {
    ss = "0" + ss;
  }

  return hh + ":" + mm + ":" + ss;
}

function jsUcfirst(string) {
  string = string + "";
  return string.charAt(0).toUpperCase() + string.slice(1).toLocaleLowerCase();
}

function base64ToArrayBuffer(base64) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

function saveByteArrayToPDF(reportName, byte, type = "application/pdf") {
  var blob = new Blob([byte], { type });
  var link = document.createElement("a");
  link.href = window.URL.createObjectURL(blob);
  var fileName = reportName;
  link.download = fileName;
  link.click();
}

function stringArrayContains(item, arrayItems) {
  var filterstrings = arrayItems;
  var regex = new RegExp(filterstrings.join("|"), "i");
  return regex.test(item);
}

function stringArrayElementEquals(item, arrayItems) {
  var temp = false;

  if(Array.isArray(arrayItems)){
    for (let i = 0; i < arrayItems.length; i++) {
      const element = arrayItems[i];
      if(stringEquals(item, element)){
        temp = true;
      }  
    }
  }

  return temp;
}

function arrayObjectToSingleDimensionArray(arrayObject, key){
  let res = [];

  if(Array.isArray(arrayObject)){
    for (let i = 0; i < arrayObject.length; i++) {
      const element = arrayObject[i];
      if(!isNullOrEmpty(element[`${key}`])){
        res.push(element[`${key}`])
      }
    }
  }

  return res;
}


// export const stringTrans = {
//   keys : {
//     id : "sdfjlksf",
//     en : "sdfhsdjf",
//   }
// }

// function const stringSt

function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(",")[1]);

  // separate out the mime component
  var mimeString = dataURI
    .split(",")[0]
    .split(":")[1]
    .split(";")[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var bb = new Blob([ab]);
  return bb;
}

function getExtensionFile(FileNamewithextension) {
  return (FileNamewithextension + "").split(".").pop();
}

function showToast(messages, type = "INFO", dismissToast = true, position = "top-right") {
  type = type.toUpperCase();
  if (type == "WARNING") {
    if(dismissToast){
      toast.dismiss();
    }
    toast.warning("❗ " + messages, {
      position: position,
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  } else if (type == "SUCCESS") {
    if(dismissToast){
      toast.dismiss();
    }
    toast.success("" + messages, {
      position: position,
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  } else if (type == "INFO") {
    if(dismissToast){
      toast.dismiss();
    }
    toast.info("" + messages, {
      position: position,
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  } else if (type == "ERROR") {
    if(dismissToast){
      toast.dismiss();
    }
    toast.error("❗ " + messages, {
      position: position,
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  }
}

function daysBetween(date1, date2) {
  //Get 1 day in milliseconds
  var one_day = 1000 * 60 * 60 * 24;

  // Convert both dates to milliseconds
  var date1_ms = date1.getTime();
  var date2_ms = date2.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;

  // Convert back to days and return
  return Math.round(difference_ms / one_day);
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function sentenceContainsArrayString(
  yourstring = "foo barya",
  substrings = ["foo", "bar"]
) {
  let length = substrings.length;
  let tempReturn = true;
  while (length--) {
    if (tempReturn) {
      if (
        (yourstring + "")
          .toLowerCase()
          .includes((substrings[length] + "").toLowerCase())
      ) {
        tempReturn = true;
      } else {
        tempReturn = false;
      }
    }
  }

  return tempReturn;
}

async function fetchAPIAdditional(
  URL, // site URL
  method = "POST", // Method API
  headers, // Headers hitting API
  body = null, // object body
  additional = {} // addtional
) {
  body = JSON.stringify(headers).includes("application/x-www-form-urlencoded")
    ? toFormData(body)
    : JSON.stringify(body);
  
    if(!isNullOrEmpty(secureStorage.getItem("isStopHitAPI"))){
      return {
        status: "STOPHITTING",
        json: () => ({
          status: false,
          data: null,
          message: "session expired!"
        })
      };
    }

    var fetching = await fetch(URL, {
      method: method.toUpperCase(),
      headers,
      body,
      ...additional
    }).then(checkFetchBeforeMapping);

  return fetching;
}

function checkFetchBeforeMapping (res) {
  if(res.status == "440"){
    secureStorage.setItem("isStopHitAPI", true);
    throw new Error("token expired!");
  }

  return res;
}

function hittingAPIError(error, callbackTrue = () => {}, trycount = 0, callbackMaxHit = () => {}) {
  if(trycount > API_RETRY_LIMIT){
    callbackMaxHit();
    errorTrace(error);
    return;
  }
  if (!isNotErrorLoopingHittingAPI(error)) {
    setTimeout(() => {
      callbackTrue();
    }, 2000);
  }
}

function stringEquals(str1, str2) {
  let temp = false;
  if ((str1 + "").trim().toUpperCase() === (str2 + "").trim().toUpperCase()) {
    temp = true;
  }
  return temp;
}

function stringContains(sentence, str){
  let temp = false;
  if ((sentence + "").trim().toUpperCase().includes((str + "").trim().toUpperCase())) {
    temp = true;
  }
  return temp;
}

function isWeekday(date) {
  const day = date.getDay();
  return day !== 0 && day !== 6;
}

function addDateWithoutWeekend(date, added) {
  var dateTemp = new Date(formatDate(date, "dd mmmm yyyy"));

  var tempadditional = 0;
  for (let i = 0; i < added; i++) {
    var dateTemploop = new Date(formatDate(dateTemp, "dd mmmm yyyy")).setDate(
      new Date(formatDate(dateTemp, "dd mmmm yyyy")).getDate() + i
    );
    if (!isWeekday(new Date(formatDate(dateTemploop, "dd mmmm yyyy")))) {
      tempadditional += 1;
    }
  }

  added += tempadditional;

  return new Date(formatDate(date, "dd mmmm yyyy")).setDate(
    new Date(formatDate(date, "dd mmmm yyyy")).getDate() + added
  );
}

function convertArrayObjectToLinearArray(arrayOfObject, keyObjectArray) {
  var array = [...arrayOfObject];
  var returnArray = [];

  array.forEach((data, i) => {
    if (typeof data == "object") {
      returnArray.push(data[keyObjectArray]);
    }
  });

  return returnArray;
}

function isNotErrorLoopingHittingAPI(error) {
  var result = false;

  error = (error + "").toLowerCase();

  if (error.includes("token")) {
    showToast("Your session expired! Please login again", "ERROR");
    result = true;
  }

  // if (error.includes("map")) {
  //   result = true;
  // }

  if (error.includes("aborterror")) {
    result = true;
  }

  return result;
}

function stripHtml(html) {
  var tmp = document.createElement("DIV");
  tmp.innerHTML = html;
  return tmp.textContent || tmp.innerText || "";
}

function stringParse(str) {
  str += "";
  var args = [].slice.call(arguments, 1),
    i = 0;

  return str.replace(/%s/g, () => args[i++]);
}

function maskingPhoneNumber(
  value,
  prefix = ["628", "08", "02", "622"],
  lastDigit = 4
) {
  var temp = value + "";
  for (var i = 0; i < prefix.length; i++) {
    var element = prefix[i];
    // detect landline change phone lastdigit
    if (element != "08" || element == "622") {
      lastDigit = 3;
    }
    if (stringEquals(element, temp.substring(0, element.length))) {
      temp =
        element +
        temp
          .substring(element.length, temp.length - lastDigit)
          .replace(/[0-9]/g, "X") +
        temp.substring(temp.length - lastDigit, temp.length);
    }
  }

  return temp;
}

function maskingPhoneNumberv2(value) {
  value = value+"";
  value = value.trim();
  var temp = value + "";

  var prefixValue = "";

  var lastDigit = 4;

  if (detectPrefix(value, Parameterized.mobilePrefix)) {
    for (var i = 0; i < Parameterized.mobilePrefix.length; i++) {
      var element = Parameterized.mobilePrefix[i];
      if (stringEquals(element, temp.substring(0, element.length))) {
        prefixValue = element;
      }
    }
  } else {
    lastDigit = 3;
    if (detectPrefix(value, Parameterized.landLinePrefix)) {
      for (var i = 0; i < Parameterized.landLinePrefix.length; i++) {
        var element = Parameterized.landLinePrefix[i];
        if (stringEquals(element, temp.substring(0, element.length))) {
          prefixValue = element;
        }
      }
    } else {
      prefixValue = temp.substring(0, 2);
    }
  }

  temp =
    prefixValue +
    temp
      .substring(prefixValue.length, temp.length - lastDigit)
      .replace(/[0-9]/g, "X") +
    temp.substring(temp.length - lastDigit, temp.length);

  return temp;
}

function maskingEmail(value){
  value = value+"";
  
  value = value.substring( 0, value.indexOf("@") ).replace(/./g, "X") + value.substring( value.indexOf("@"), value.length)
  return value;
}

async function getBase64(file, callback) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function() {
    console.log(reader.result);
    callback(reader.result);
  };
  reader.onerror = function(error) {
    console.log("Error: ", error);
  };
}

function base64toHEX (base64){
  var raw = atob(base64);
  var HEX = "";
  var i = 0;
  for (i = 0; i < raw.length; i++) {
    var _hex = raw.charCodeAt(i).toString(16);
    HEX += _hex.length == 2 ? _hex : "0" + _hex;
  }
  return HEX.toUpperCase();
};

function base64MimeType(encoded) {
  var result = null;

  if (typeof encoded !== "string") {
    return result;
  }

  var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

  if (mime && mime.length) {
    result = mime[1];
  }

  console.log("mime types :", mime);

  return result;
}

function base64Only(encoded) {
  var result = null;

  if (typeof encoded !== "string") {
    return result;
  }

  var mime = encoded.split(",");

  if (mime && mime.length) {
    result = mime[1];
  }

  console.log("base64Only :", mime);

  return result;
}

function detectPrefix(str, array) {
  str = str + "";
  let temp = false;
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      const element = array[i] + "";
      if (str.startsWith(element)) {
        temp = true;
      }
    }
  }

  return temp;
}

function replacePrefix(str, array, replacewith){
  str = str + "";
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      const element = array[i] + "";
      if (str.startsWith(element)) {
        str = replacewith + str.substring(element.length, str.length)
      }
    }
  }

  return str;
}


function clearPhoneNo (PhoneNo) {
  PhoneNo = (PhoneNo+"").trim();
  PhoneNo = PhoneNo.replace(new RegExp(" ", "gm"), "");

  return PhoneNo;
}

function isSelectOptionContainValue(array, value) {
  var temp = false;
  if (Array.isArray(array)) {
    array.forEach(data => {
      if (stringEquals(data.value, value)) {
        temp = true;
      }
    });
  }

  return temp;
}

function retry(fn, retriesLeft = 5, interval = 500) {
  return new Promise((resolve, reject) => {
    fn()
      .then(resolve)
      .catch((error) => {
        setTimeout(() => {
          if (retriesLeft === 1) {
            // reject('maximum retries exceeded');
            reject(error);
            return;
          }

          // Passing on "reject" is the important part
          retry(fn, retriesLeft - 1, interval).then(resolve, reject);
        }, interval);
      });
  });
}

function SomeErrorReportingTool (error, info, errorStackTrace, errorFrom) {
  let jsonError = {
    url: window.location.href,
    error: (error+""),
    info: info,
    errorStackTrace,
    errorFrom
  };
  fetchAPIAdditional(
    `${API_URL + API_VERSION_0213URF2019}/DataReact/logErrorUI/`,
    "POST",
    HEADER_API,
    {
      ErrorInfo: JSON.stringify(jsonError),
      UserLogin: JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
    }
  ).then(res => res.json());
};

function errorTrace(error, info = "") {
  StackTrace.fromError(error).then(err => {
    let errorFrom = {
      type: "error from Catch API",
      url: window.location.href,
      userId: window.userId,
      agent: window.navigator.userAgent,
      date: new Date()
    };
    console.log("error raw from Catch API : ", err);
    SomeErrorReportingTool(error, info, err, errorFrom);
  });
}


const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)


const asyncLocalStorage = {
  setItem: function (key, value) {
      return Promise.resolve().then(function () {
          secureStorage.setItem(key, value);
      });
  },
  getItem: function (key) {
      return Promise.resolve().then(function () {
          return secureStorage.getItem(key);
      });
  }
};

function getAddress (latitude, longitude) {
  return new Promise(function (resolve, reject) {
      var request = new XMLHttpRequest();

      var method = 'GET';
      var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true&key='+FCMKey;
      var async = true;

      request.open(method, url, async);
      request.onreadystatechange = function () {
          if (request.readyState == 4) {
              if (request.status == 200) {
                  var data = JSON.parse(request.responseText);
                  var address = data.results[0];
                  resolve(address);
              }
              else {
                  reject(request.status);
              }
          }
      };
      request.send();
  });
};

function getBrowserLanguage () {
  var nav = window.navigator,
  browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'],
  i,
  language;

  // support for HTML 5.1 "navigator.languages"
  if (Array.isArray(nav.languages)) {
    for (i = 0; i < nav.languages.length; i++) {
      language = nav.languages[i];
      if (language && language.length) {
        return language;
      }
    }
  }

  // support for other well known properties in browsers
  for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
    language = nav[browserLanguagePropertyKeys[i]];
    if (language && language.length) {
      return language;
    }
  }

  return null;
};

function copyToClipboard ( str ) {
  const el = document.createElement('textarea');
  el.value = str;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
};

function getLocation(callbackSuccess = showPosition, callbackError = showError ) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(callbackSuccess, callbackError);
  } else { 
    // x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  // x.innerHTML = "Latitude: " + position.coords.latitude + 
  // "<br>Longitude: " + position.coords.longitude;
}

function showError(error) {
  // switch(error.code) {
  //   case error.PERMISSION_DENIED:
  //     x.innerHTML = "User denied the request for Geolocation."
  //     break;
  //   case error.POSITION_UNAVAILABLE:
  //     x.innerHTML = "Location information is unavailable."
  //     break;
  //   case error.TIMEOUT:
  //     x.innerHTML = "The request to get user location timed out."
  //     break;
  //   case error.UNKNOWN_ERROR:
  //     x.innerHTML = "An unknown error occurred."
  //     break;
  // }
}


/**
 * ADDED END BY FAY
 **/

function generateErrorEncodedCode(obj) {
      var result = "";
      try {
        result = btoa(JSON.stringify(obj));
      } catch(ex) {
        console.log(ex);
      } finally{
        return result;
      }
}

export {
  fetchAPI,
  isNullOrEmpty,
  formatMoney,
  formatDate,
  formatTimeFromDate,
  jsUcfirst,
  daysBetween,
  base64ToArrayBuffer,
  saveByteArrayToPDF,
  stringArrayContains,
  dataURItoBlob,
  getExtensionFile,
  sentenceContainsArrayString,
  validateEmail,
  handleBackButton,
  showToast,
  fetchAPIAdditional,
  hittingAPIError,
  stringEquals,
  isWeekday,
  addDateWithoutWeekend,
  convertArrayObjectToLinearArray,
  isNotErrorLoopingHittingAPI,
  stripHtml,
  stringParse,
  maskingPhoneNumber,
  maskingPhoneNumberv2,
  maskingEmail,
  getBase64,
  base64Only,
  base64MimeType,
  calculateMinTime,
  isSelectOptionContainValue,
  stringArrayElementEquals,
  arrayObjectToSingleDimensionArray,
  detectPrefix,
  retry,
  clearPhoneNo,
  replacePrefix,
  convertDate,
  getTimezoneOffsetByHour,
  getAddress,
  getLocation,
  errorTrace,
  valueTrim,
  base64toHEX,
  scrollToRef,
  copyToClipboard,
  stringContains,
  isEmptyFunction,
  asyncLocalStorage,
  replaceAll,
  getBrowserLanguage,
  SomeErrorReportingTool,
  generateErrorEncodedCode
};
