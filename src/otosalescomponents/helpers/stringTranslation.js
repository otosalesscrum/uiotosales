import {
    isNullOrEmpty,
    getBrowserLanguage,
    stringContains
} from "../helpers/Util";

const stringTranslate = {
    keyString : {
        id : "ID String",
        en : "EN String"
    },
    testing : {
        id : "Rekening payroll salah",
        en : "You have entered invalid bank account"
    },
    FUSurveyResultAlert : {
        id : "Data order berubah akibat hasil survey!",
        en : "Data order berubah akibat hasil survey!"
    },
    IsLoadingPleaseWait : {
        id : "Sedang memuat, silahkan menunggu",
        en : "Loading, please wait"
    }
}


export default function translate(key) {
    if(!isNullOrEmpty(key)){
        if(typeof(key) === "string"){
            if(stringTranslate[`${key}`] !== undefined){
                let stringTemp = Object.keys(stringTranslate[`${key}`]).filter(data => stringContains(getBrowserLanguage(), data));
                if(stringTemp.length > 0){
                    return stringTranslate[`${key}`][`${stringTemp[0]}`]
                }else{
                    return stringTranslate[`${key}`][`en`];
                }
            }
        }
    }
    return key;
}