import React, { Component } from 'react'
import Modal from "react-responsive-modal";

export default class otosalesmodalsenttosamakesure extends Component {
    render() {
        return (
            <Modal
          styles={{
            overlay: {
              zIndex: 1050
            }
          }}
          showCloseIcon={false}
          open={this.props.state.showModalMakeSure}
          onClose={() => this.props.onClose()}
          center
        >
          <div className="modal-content panel panel-primary" style={{margin:"-25px"}}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title pull-left" id="exampleModalLabel">
                <b>Confirmation</b>
              </h4>
            </div>
            <div className="modal-body">
              Are you sure to send?
            </div>
            <div className="modal-footer">
              <div>
                <div className="col-xs-6">
                  <button
                    onClick={() => this.props.onclose()}
                    className="btn btn-warning form-control"
                  >
                    No
                  </button>
                </div>
                <div className="col-xs-6">
                  <button
                    onClick={() => this.props.yesSendToSA()}
                    className="btn btn-info form-control"
                  >
                    Yes
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal>
        )
    }
}
