import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { A2isText } from "../components/a2is.js";
import { toast } from "react-toastify";
import { OtosalesDatePicker } from "../otosalescomponents";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";

export default class otosalesmodaldashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: this.props.EmailDefault,
      period: new Date(),
      isLoading: false
    };
  }

  getDatePicker = (date, name) => {
    this.setState({ period: date });

    console.log("value" + this.state.period);
  };
  onChange = event => {
    const text = event.target.value;
    this.setState({ email: text });
    //this.props.EmailDefault=text;
  };
  sendReportEmail = (email, SalesOfficerID, Role) => {
    var ReportType = "";
    if (Role == "SALESOFFICER") {
      ReportType = "REPORTOFFICER";
    } else {
      ReportType =
        this.props.PeriodCode == 0 ? "REPORTMANAGER" : "REPORTMANAGERYTD";
    }
    var mm = String(this.state.period.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = this.state.period.getFullYear();
    var perioddate = yyyy + "-" + mm + "-01 00:00:00.000";

    // this.setState({ isLoading: true }, () => {
    //   this.props.Loading(this.state.isLoading);
    // });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/SendReportEmail/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "Email=" +
        email +
        "&SalesOfficerId=" +
        SalesOfficerID +
        "&ReportType=" +
        ReportType +
        "&period=" +
        perioddate
    })
      .then(res => res.json())
      .then(json => {
        if (json.status) {
          toast.info("✅ Email has been sent ", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          toast.warning("❗ Gagal Send Email - " + json.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState(
          {
            isLoading: false
          },
          () => {
            this.props.Loading(this.state.isLoading);
          }
        );
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState(
          {
            isLoading: false
          },
          () => {
            this.props.Loading(this.state.isLoading);
          }
        );
        toast.warning("❗ Gagal Send Email - " + error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });
        // this.sendReportEmail(email, SalesOfficerID, ReportType);
      });
  };
  render() {
    return (
      <Modal
        styles={{
          modal: { width: "100%" },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.props.showModal}
        onClose={() => this.props.onCloseModal()}
        center
        showCloseIcon={false}
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send Email</b>
            </h4>
            <div className="dropdown pull-right">
              <button
                className="btn btn-primary"
                style={{ borderColor: "#18516c", backgroundColor: "#18516c" }}
                type="button"
              >
                <i
                  className="fa fa-send fa-lg"
                  onClick={() => {
                    this.props.onCloseModal();
                    if (/\S+@\S+\.\S+/.test(this.state.email)) {
                      this.sendReportEmail(
                        this.state.email,
                        this.props.SalesOfficer,
                        this.props.Role
                      );
                      this.setState({ showModal: false });
                    } else {
                      toast.warning("❗ Email Tidak Valid", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                      });
                    }
                  }}
                />
              </button>
            </div>
          </div>
          <div className="modal-body">
            <div class="col-xs-12">
              <label>PROSPECT INPUT PERIOD</label>
            </div>
            <div class="col-xs-12">
              <OtosalesDatePicker
                className="form-control"
                name="txtdatesample"
                selected={this.state.period}
                dateFormat="dd/MM/yyyy"
                readOnly={false}
                onChange={this.getDatePicker}
              />
            </div>

            <div class="col-xs-12" style={{ marginTop: "10px" }}>
              <label>SEND TO</label>
            </div>
            <div className="col-xs-12">
              <input
                type="text"
                className="form-control"
                name="txtleft"
                id="txtleft"
                minlength="0"
                maxlength="10000"
                onChange={evt => {
                  this.setState({ email: evt.target.value });
                }}
                defaultValue={this.props.EmailDefault}
              />
            </div>
            <div className="col-xs-12 text-right ">
              <label>email1@domain.com; email2@domain.com</label>
            </div>
          </div>

          <div className="modal-footer">
            <div>
              <div className="col-xs-6" />
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
