import React, { Component } from 'react';
import Select from 'react-select/dist/react-select.esm';

class otosalesselect2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    }
  }
  
    handleInputChange = (newValue) => {
        this.props.onOptionsChange(newValue.value);
    };

  render() {
    let customStyles = (this.props.error||false) ? {
      control: (provided) => ({
        ...provided,
        border: '1px solid #dd4b39',
        borderRadius: '0px',
        height: '1px'
      })
    }:{
      control: (provided) => ({
        ...provided,
        borderRadius: '0px',
        height: '1px'
      })
    };
    
    customStyles = {
      ...customStyles,
      menuList: (base) => ({
        ...base,
        minHeight: "75vh",
    }),
    }


    let defaultValue = [];

    if(this.props.options != undefined){ 
      defaultValue = this.props.options.filter(option => option.value == this.props.value);
    }
      // console.log(defaultValue);
    return (
      <Select
      autoFocus={this.props.autoFocus || false}
      onMenuOpen={() => {
        // document.getElementById("react-select-4-input").blur()
        // console.log(document.activeElement);
        // document.activeElement.blur();
        document.activeElement.scrollIntoView(true); // Top
      }}
      isDisabled={this.props.isDisabled || false}
      styles={customStyles}
        value={defaultValue}
        selectOption={this.props.selectOption}
        options={this.props.options}
        onChange={this.handleInputChange}
        placeholder={this.props.placeholder || "Select..."}
      />
    );
  }
}

export default otosalesselect2;