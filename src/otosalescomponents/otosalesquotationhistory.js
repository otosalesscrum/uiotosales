import React, { Component } from "react";
import { Util } from ".";

class otosalesquotationhistory extends Component {
  constructor(props) {
    super(props);
  }

  formatDate = dates => {
    dates = Util.convertDate(dates);
    var day = dates.getDay();
    var dd = dates.getDate();
    var mm = dates.getMonth(); //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }

    var mL = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    var mS = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ];

    var dayName = [
      "Sun",
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat"
    ];
    return dayName[day]+" "+ dd + " " + mS[mm] + " " + yyyy;
  };

  formatTimeFromDate = dates => {
    dates = Util.convertDate(dates);
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    return hh + ":" + mm;
  };

  formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
  
      const negativeSign = amount < 0 ? "-" : "";
  
      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;
  
      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      console.log(e)
    }
  };

  render() {
    //   console.log(this.props.selectedQuotation);
    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((data, index) => {
        var checked = false;
        if(this.props.selectedQuotation == data.OrderNo){
            checked = true;
        }
      return (
        <div className="panel panel-default" key={index} onClick={() => this.props.onClickQuotationHistory(data.OrderNo, data.FollowUpNo, data.QuotationNo)}>
          <div className="panel-body">
            <div className="col-xs-1 panel-body-list">
              <input type="radio" checked={checked} onChange={() => {}} style={{marginTop:"10px"}}/>
            </div>
            <div className="col-xs-11 panel-body-list">
              <h4><b>{data.ProspectName}</b></h4>
            </div>
            <div className="row panel-body-list">
            
            <p className="col-xs-12">{data.Vehicle+" - "+data.VehicleYear}</p>
                <div className="col-xs-12">
                  <span className="pull-left">TSI CASCO</span>
                  <span className="pull-right">{this.formatMoney(data.SumInsured,0)}</span>
                </div>
                <div className="col-xs-12 separatorTop">
                  <span className="pull-left">TSI ACCESSORIES</span>
                  <span className="pull-right">{this.formatMoney(data.AccessSI,0)}</span>
                </div>
                <div className="col-xs-12 separatorTop">
                  <span className="pull-left">QUOTATION DATE</span>
                  <span className="pull-right"><b>{this.formatDate(data.EntryDate)}</b>{", "+this.formatTimeFromDate(data.EntryDate)}</span>
                </div>
                <div className="col-xs-12 separatorTop">
                  <span className="pull-left">TOTAL PREMIUM</span>
                  <span className="pull-right"><b style={{ color: "#D59F00" }}>{this.formatMoney(data.TotalPremium,0)}</b></span>
                </div>
              </div>
          </div>
        </div>
      );
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default otosalesquotationhistory;
