import React from "react";
import PropTypes from "prop-types";

export default function SegmentDropdown(props) {
  return (
    <React.Fragment>
      <div className="row backgroundgrey labelspanbold labelfontgrey">
        <div
          className="col-xs-12"
          style={{ paddingTop: "5px", paddingBottom: "5px" }}
          data-toggle="collapse"
          data-target={"#"+props.divid}
        >
          <span className="pull-left">{props.title || ""}</span>
          <span className="pull-right collapsebutton" />
        </div>
      </div>
      <div
        id={props.divid}
        className="panel-body panel-padding0 collapse in"
      >
          {props.children || null}
      </div>
    </React.Fragment>
  );
}

SegmentDropdown.propTypes = {
  title: PropTypes.string.isRequired,
  divid: PropTypes.string.isRequired
};
