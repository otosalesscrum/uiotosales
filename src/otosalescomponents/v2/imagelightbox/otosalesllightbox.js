import React, { Component } from "react";
import { PlaceHolderImage, PlaceholderLoading } from "../../../assets";
import Lightbox from "react-image-lightbox";
import { Util } from "../../../otosalescomponents";
import PropTypes from "prop-types";

class otosaleslightbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  render() {
    const img = !Util.isNullOrEmpty(this.props.src)
      ? URL.createObjectURL(this.props.src)
      : PlaceholderLoading;

    return (
      <div>
        {this.state.isOpen && (
          <Lightbox
            reactModalStyle={{
              overlay: {
                zIndex: 1050
              }
            }}
            mainSrc={img}
            onCloseRequest={() => this.setState({ isOpen: false })}
          />
        )}
        <div>
          <div
            className="squareimg"
            onClick={() => this.setState({ isOpen: true })}
          >
            <img
              className="contentimg"
              style={{ width: "100%", imageOrientation: "from-image" }}
              src={img}
            />
          </div>
          {this.props.caption && <center style={{wordWrap: "break-word", paddingLeft: "2px", paddingRight: "2px"}}>{this.props.caption}</center>}
        </div>
      </div>
    );
  }
}

otosaleslightbox.propTypes = {
  src: PropTypes.string,
  caption: PropTypes.string
};

export default otosaleslightbox;
