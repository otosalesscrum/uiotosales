import Iframe from 'react-iframe'
import React, { useEffect } from 'react'
import PropTypes from "prop-types"

export default function index(props) {
    useEffect(() => {
        // props.getCurrentURL(document.getElementById(props.id).contentWindow.location+"");
    });

    return (
        <Iframe url={props.url || "https://otosales.gardaoto.com"}
            width={props.width || "450px"}
            height={props.height || "100vh"}
            id={props.id || ""}
            className={props.className || ""}
            display="initial"
            position="relative"
            onLoad={props.onLoad || (() => {})}
        />
    )
}

index.propTypes = {
    url: PropTypes.string.isRequired,
    width: PropTypes.string,
    height: PropTypes.string,
    className: PropTypes.string,
    id: PropTypes.string,
    onLoad: PropTypes.func,
    getCurrentURL: PropTypes.func
  };
