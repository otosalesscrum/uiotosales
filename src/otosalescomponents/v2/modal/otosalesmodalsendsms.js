import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";
import { Util } from "../..";

export default class otosalesmodalsendsms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: this.props.phonenumber
    };
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (nextProps.phonenumber !== this.state.phonenumber && Util.isNullOrEmpty(this.state.phonenumber)) {
      this.setState({ phonenumber: nextProps.phonenumber });
    }
  }

  onChangeFunction = event => {
    let pattern = "^[0-9]*$";
    let value = event.target.value;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({
          [event.target.name]: event.target.value
        });
      }
    }
  };

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "80%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.props.state.showModalSendSMS || false}
        onClose={() => this.props.onCloseModalSendSMS()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send SMS</b>
            </h4>
            <div className="dropdown pull-right">
              <i
                className="fa fa-send fa-lg"
                onClick={() => {
                  if (/^[0-9-]{2,}[0-9]$/.test(this.state.phonenumber.replace("+",""))) {
                    this.props.sendQuotationSMS(
                      this.props.state.selectedQuotation,
                      this.state.phonenumber
                    );
                    this.props.onCloseModalSendSMS();
                  } else {
                    toast.dismiss();
                    toast.warning("❗ Phone number invalid", {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true
                    });
                  }
                }}
              />
            </div>
          </div>
          <div className="modal-body">
            <textarea
              type="number"
              maxLength="16"
              name="phonenumber"
              value={this.state.phonenumber}
              onChange={event => this.onChangeFunction(event)}
              placeholder="enter phone number"
              className="form-control"
              style={{ resize: "none" }}
            />
          </div>
          <div className="modal-footer" />
        </div>
      </Modal>
    );
  }
}
