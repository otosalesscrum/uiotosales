import React, { Component } from "react";
import PropTypes from "prop-types";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";
import { Util } from "../..";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import { sendEmail } from "../../../assets";
import OtosalesDropzoneFile from "../dropzone/dropzoneFile";
import OtosalesLoading from "../loading/otosalesloading";
import OtosalesPDFViewer from "../pdfviewer";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

export default class otosalesModalSendEmailCkEditor extends Component {
  static propTypes = {
    title: PropTypes.string,
    maxLength: PropTypes.string.isRequired,
    isNotHideSubject: PropTypes.bool,
    bodyEmail: PropTypes.string.isRequired,
    showModalSendEmail: PropTypes.bool.isRequired,
    email: PropTypes.string,
    subject: PropTypes.string,
    handleUploadFile: PropTypes.func,
    maxLengthSubject: PropTypes.number
  };

  constructor(props) {
    super(props);

    let editorState = EditorState.createEmpty();
    if (!Util.isNullOrEmpty(this.props.bodyEmail)) {
      const blocksFromHtml = htmlToDraft(this.props.bodyEmail);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(
        contentBlocks,
        entityMap
      );
      editorState = EditorState.createWithContent(contentState);
    }

    this.state = {
      email: this.props.email,
      subject: this.props.subject,
      charTypeLength: 0,
      editorState
    };
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (
      nextProps.email !== this.props.email
    ) {
      this.setState({ email: nextProps.email });
    }

    if (
      nextProps.subject !== this.props.subject
    ) {
      this.setState({
        subject: nextProps.subject
      });
    }

    if (this.props.bodyEmail != nextProps.bodyEmail) {
      let editorState = EditorState.createEmpty();
      const blocksFromHtml = htmlToDraft(nextProps.bodyEmail);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(
        contentBlocks,
        entityMap
      );
      editorState = EditorState.createWithContent(contentState);
      const currentContent = editorState.getCurrentContent();
      const currentContentLength = currentContent.getPlainText("").length;
      const selectedTextLength = this._getLengthOfSelectedText();
      this.setState({
        // charTypeLength: currentContentLength - selectedTextLength - 1,
        charTypeLength: this.getCharCount(editorState),
        editorState
      });
    }
  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  _getLengthOfSelectedText = () => {
    const currentSelection = this.state.editorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();

    let length = 0;

    if (!isCollapsed) {
      const currentContent = this.state.editorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      console.log(currentSelection);
      if (isStartAndEndBlockAreTheSame) {
        length +=
          currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };

  _handleBeforeInput = () => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this._getLengthOfSelectedText();

    // this.setState({
    //   charTypeLength: currentContentLength - selectedTextLength
    // });

    // if (currentContentLength - selectedTextLength > this.props.maxLength - 1) {
      if (this.state.charTypeLength > this.props.maxLength - 1) {
      console.log("you can type max ten characters");

      return "handled";
    }
  };

  _handlePastedText = pastedText => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this._getLengthOfSelectedText();

    if (
      currentContentLength + pastedText.length - selectedTextLength >
      this.props.maxLength
    ) {
    // if (this.state.charTypeLength > this.props.maxLength - 1) {
      console.log("you can type max ten characters");

      return "handled";
    }
  };

  _handleChange = editorState => {
    this.setState({ editorState });
  };

  onEditorStateChange = editorState => {
    console.log("sbdhjf ", convertToRaw(editorState.getCurrentContent()));
    console.log("character count : "+this.getCharCount(editorState));
    this.setState({
      editorState,
      charTypeLength: this.getCharCount(editorState)
    });
  };

  Attachment = () => {
    return (
      <div className="row" style={{ marginBottom: "5px" }}>
        <label className="col-xs-12 col-md-2">Attachment </label>
        <div className="col-xs-3 col-md-2">
          <div className="text-center">
            <OtosalesDropzoneFile
              state={this.props.state}
              src={this.props.state.dataAttachmentEmail1}
              name="AttachmentEmail1"
              handleDeleteImage={this.props.handleDeleteImage}
              handleUploadImage={this.props.handleUploadImage}
            />
          </div>
        </div>
        <div className="col-xs-3 col-md-2">
          <div className="text-center">
            <OtosalesDropzoneFile
              state={this.props.state}
              src={this.props.state.dataAttachmentEmail2}
              name="AttachmentEmail2"
              handleDeleteImage={this.props.handleDeleteImage}
              handleUploadImage={this.props.handleUploadImage}
            />
          </div>
        </div>
        <div className="col-xs-3 col-md-2">
          <div className="text-center">
            <OtosalesDropzoneFile
              state={this.props.state}
              src={this.props.state.dataAttachmentEmail3}
              name="AttachmentEmail3"
              handleDeleteImage={this.props.handleDeleteImage}
              handleUploadImage={this.props.handleUploadImage}
            />
          </div>
        </div>
        <div className="col-xs-3 col-md-2">
          <div className="text-center">
            <OtosalesDropzoneFile
              state={this.props.state}
              src={this.props.state.dataAttachmentEmail4}
              name="AttachmentEmail4"
              handleDeleteImage={this.props.handleDeleteImage}
              handleUploadImage={this.props.handleUploadImage}
            />
          </div>
        </div>
        <div className="col-xs-3 col-md-2">
          <div className="text-center">
            <OtosalesDropzoneFile
              state={this.props.state}
              src={this.props.state.dataAttachmentEmail5}
              name="AttachmentEmail5"
              handleDeleteImage={this.props.handleDeleteImage}
              handleUploadImage={this.props.handleUploadImage}
            />
          </div>
        </div>
      </div>
    );
  };

  getCharCount(editorState) {
    // const decodeUnicode = str => punycode.ucs2.decode(str); // func to handle unicode characters

    // const plainText = editorState.getCurrentContent().getPlainText('').trim();
    // let counterChar = plainText.length;
    // const dataSplit = plainText.split("\n");
    // if(dataSplit.length > 1){
    //   counterChar += dataSplit.length - 1;
    // }
    // console.log("text : ",plainText);

    // const regex = /(?:\r\n|\r|\n)/g; // new line, carriage return, line feed
    // const cleanString = plainText.replace(regex, '').trim(); // replace above characters w/ nothing

    let plainText = draftToHtml(
      convertToRaw(
        editorState.getCurrentContent()
      )
    )
    plainText = (plainText + "")
    .replace(/<[^>]*>/g, "")
    .replace(/&nbsp;/g, " ").trim()
    let counterChar = plainText.length
    
    return counterChar;
  }

  render() {
    // console.log(this.props.state.Email1);
    return (
      <div>
        <Modal
          styles={{
            modal: {
              width: "100%",
              maxWidth: "1000px"
            },
            overlay: {
              zIndex: 1050
            }
          }}
          showCloseIcon={true}
          open={this.props.showModalSendEmail}
          onClose={() => this.props.onCloseModalSendEmail()}
          center
        >
          <div
            className="modal-content panel panel-primary"
            style={{ margin: "-15px" }}
          >
            <OtosalesLoading isLoading={this.props.state.isLoading || false}>
              <div className="modal-header panel-heading">
                <h4 className="modal-title pull-left" id="exampleModalLabel">
                  <b>{this.props.title || "Send Email"}</b>
                </h4>
              </div>
              <div className="modal-body">
                {
                  (this.props.hideToEmail || false) &&
                  <div className="row">
                  <label className="col-xs-12 col-md-2">To </label>
                  <div className="col-xs-12 col-md-10">
                    <textarea
                      type="email"
                      name="email"
                      onChange={event => this.onChangeFunction(event)}
                      value={this.state.email}
                      placeholder="enter email"
                      className="form-control"
                      style={{ resize: "none" }}
                      rows="1"
                    />
                  </div>
                </div>
                }
                {
                  !(this.props.isHideSubject || false)
                  &&
                <div className="row">
                  <label className="col-xs-12 col-md-2">Subject </label>
                  <div className="col-xs-12 col-md-10">
                    <textarea
                      type="text"
                      name="subject"
                      maxLength={this.props.maxLengthSubject || 100}
                      onChange={event => this.onChangeFunction(event)}
                      value={this.state.subject}
                      placeholder="enter subject"
                      className="form-control"
                      style={{ resize: "none" }}
                      rows="1"
                    />
                  </div>
                </div>
                }
                <this.Attachment />

                <Editor
                  handleBeforeInput={this._handleBeforeInput}
                  handlePastedText={this._handlePastedText}
                  editorState={this.state.editorState}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor"
                  toolbar={{ 
                    options: [
                      'inline', 
                      'blockType', 
                      'fontSize', 
                      // 'fontFamily', 
                      'list', 
                      'textAlign', 
                      'colorPicker', 
                      'link', 
                      'embedded', 
                      'emoji', 
                      'image', 
                      'remove', 
                      'history'
                    ]
                   }}
                  onEditorStateChange={this.onEditorStateChange}
                />
                <p>
                  {this.state.charTypeLength} / {this.props.maxLength}
                </p>
                {/* <textarea
                disabled
                value={draftToHtml(
                  convertToRaw(this.state.editorState.getCurrentContent())
                )}
              /> */}
              </div>
              <div className="modal-footer">
                <div className="pull-right col-xs-2 col-md-1 text-center">
                  <img
                    className="img img-responsive"
                    src={sendEmail}
                    onClick={() => {
                      if (/\S+@\S+\.\S+/.test(this.state.email)) {
                        this.props.sendQuotationEmail(
                          this.state.email,
                          this.state.subject,
                          draftToHtml(
                            convertToRaw(
                              this.state.editorState.getCurrentContent()
                            )
                          ),
                          this.props.OrderNo,
                          [],
                          () => {
                            this.props.onCloseModalSendEmail();
                          }
                        );
                      } else {
                        toast.warning("❗ Email Tidak Valid", {
                          position: "top-right",
                          autoClose: 5000,
                          hideProgressBar: false,
                          closeOnClick: true,
                          pauseOnHover: true,
                          draggable: true
                        });
                      }
                    }}
                  />
                  <span className="labelfontblack">Send</span>
                </div>
              </div>
            </OtosalesLoading>
          </div>
        </Modal>
      </div>
    );
  }
}
