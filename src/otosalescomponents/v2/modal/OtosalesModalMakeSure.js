import React, { Component } from "react";
import Modal from "react-responsive-modal";
import PropTypes from "prop-types";

class OtosalesModalMakeSure extends Component {
  render() {
    return (
      <Modal
        styles={{
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.props.showModalMakeSure}
        onClose={() => this.props.onCloseMakeSure()}
        showCloseIcon={false}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px", fontSize:"initial" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>{this.props.title || "Confirmation"}</b>
            </h4>
          </div>
          <div className="modal-body">{this.props.messages || "Are you sure?"}</div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.props.onCloseMakeSure()}
                  className="btn btn-warning form-control"
                >
                  No
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    this.props.onSubmitMakeSure();
                  }}
                  className="btn btn-info form-control"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

OtosalesModalMakeSure.propTypes = {
    showModalMakeSure: PropTypes.bool.isRequired,
    title: PropTypes.string,
    messages: PropTypes.string,
    onSubmitMakeSure: PropTypes.func,
    onCloseMakeSure: PropTypes.func.isRequired 
  };

export default OtosalesModalMakeSure;
