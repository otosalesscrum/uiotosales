import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";
import { Util, OtosalesSelectFullviewrev } from "../..";
import OtosalesLoading from "../loading/otosalesloading";
import PropTypes from "prop-types";
import {sendEmail} from "../../../assets";

export default class otosalesmodalsendwa extends Component {
    static propTypes = {
        dataphonelist: PropTypes.array.isRequired,
        phonelistselect: PropTypes.any,
        onOptionSelect2Change: PropTypes.func.isRequired
      };

      
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: this.props.phonenumber
    };
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (nextProps.phonenumber !== this.state.phonenumber && Util.isNullOrEmpty(this.state.phonenumber)) {
      this.setState({ phonenumber: nextProps.phonenumber });
    }
  }

  onChangeFunction = event => {
    let pattern = "^[0-9]*$";
    let value = event.target.value;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({
          [event.target.name]: event.target.value
        });
      }
    }
  };

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "80%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={true}
        open={this.props.state.showModalSendWA || false}
        onClose={() => this.props.onCloseModalSendWA()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <OtosalesLoading isLoading={this.props.state.isLoading || false}>
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send via WA</b>
            </h4>
          </div>
          <div className="modal-body">
            {/* <textarea
              type="number"
              maxLength="16"
              name="phonenumber"
              value={this.state.phonenumber}
              onChange={event => this.onChangeFunction(event)}
              placeholder="enter phone number"
              className="form-control"
              style={{ resize: "none" }}
            /> */}
            <div className="row">
                <label className="col-xs-12 col-md-1">To </label>
                <div className="col-xs-12 col-md-11">
                <OtosalesSelectFullviewrev
                  name="phonelistselect"
                  value={this.props.phonelistselect}
                  selectOption={this.props.phonelistselect}
                  onOptionsChange={this.props.onOptionSelect2Change}
                  options={this.props.dataphonelist}
                  placeholder="Choose phone number"
                />
                </div>
            </div>
          </div>
          <div className="modal-footer">
          <div className="pull-right col-xs-2 col-md-1 text-center">
              <img
                  className="img img-responsive"
                  src={sendEmail}
                  style={{ minWidth: "20px" }}
                  onClick={() => {
                    // if (/^[0-9-]{2,}[0-9]$/.test(this.state.phonenumber)) {
                      // this.props.sendQuotationSMS(
                      //   this.props.state.selectedQuotation,
                      //   this.state.phonenumber
                      // );
                      if(!Util.isSelectOptionContainValue(this.props.dataphonelist, this.props.phonelistselect)){
                        Util.showToast("Harap memilih nomor telephone","WARNING");
                      }else{
                        this.props.sendQuotationWA();
                        this.props.onCloseModalSendWA();
                      }
                    // } else {
                    //   toast.dismiss();
                    //   toast.warning("❗ Phone number invalid", {
                    //     position: "top-right",
                    //     autoClose: 5000,
                    //     hideProgressBar: false,
                    //     closeOnClick: true,
                    //     pauseOnHover: true,
                    //     draggable: true
                    //   });
                    // }
                  }}
                />
                <span class="labelfontblack">Send</span>
              </div>
          </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  }
}
