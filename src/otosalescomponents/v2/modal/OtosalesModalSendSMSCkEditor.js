import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";
import { Util, OtosalesSelectFullviewrev } from "../..";
import PropTypes from "prop-types";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import OtosalesLoading from "../loading/otosalesloading";
import { sendEmail } from "../../../assets";

export default class otosalesmodalsendsms extends Component {
  static propTypes = {
    dataphonelist: PropTypes.array.isRequired,
    phonelistselect: PropTypes.any,
    onOptionSelect2Change: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);

    let editorState = EditorState.createEmpty();
    if (!Util.isNullOrEmpty(this.props.bodySMS)) {
      const blocksFromHtml = htmlToDraft(this.props.bodySMS);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(
        contentBlocks,
        entityMap
      );
      editorState = EditorState.createWithContent(contentState);
    }

    this.state = {
      email: this.props.email,
      subject: this.props.subject,
      charTypeLength: 0,
      editorState
    };
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (nextProps.bodySMS !== this.props.bodySMS) {
      if (this.props.bodySMS != nextProps.bodySMS) {
        let editorState = EditorState.createEmpty();
        const blocksFromHtml = htmlToDraft(nextProps.bodySMS);
        const { contentBlocks, entityMap } = blocksFromHtml;
        const contentState = ContentState.createFromBlockArray(
          contentBlocks,
          entityMap
        );
        editorState = EditorState.createWithContent(contentState);
        const currentContent = editorState.getCurrentContent();
        const currentContentLength = currentContent.getPlainText("").length;
        const selectedTextLength = this._getLengthOfSelectedText();
        this.setState({
          // charTypeLength: currentContentLength - selectedTextLength - 1,
          charTypeLength: this.getCharCount(editorState),
          editorState
        });
      }
    }
  }

  onChangeFunction = event => {
    let pattern = "^[0-9]*$";
    let value = event.target.value;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({
          [event.target.name]: event.target.value
        });
      }
    }
  };


  _getLengthOfSelectedText = () => {
    const currentSelection = this.state.editorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();

    let length = 0;

    if (!isCollapsed) {
      const currentContent = this.state.editorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      console.log(currentSelection);
      if (isStartAndEndBlockAreTheSame) {
        length +=
          currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };

  _handleBeforeInput = () => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this._getLengthOfSelectedText();

    // this.setState({
    //   charTypeLength: currentContentLength - selectedTextLength
    // });

    // if (currentContentLength - selectedTextLength > this.props.maxLength - 1) {
    if (this.state.charTypeLength > this.props.maxLength - 1) {
      console.log("you can type max ten characters");

      return "handled";
    }
  };

  _handlePastedText = pastedText => {
    const currentContent = this.state.editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = this._getLengthOfSelectedText();

    if (
      currentContentLength + pastedText.length - selectedTextLength >
      this.props.maxLength
    ) {
    // if (this.state.charTypeLength > this.props.maxLength - 1) {
      console.log("you can type max ten characters");

      return "handled";
    }
  };

  _handleChange = editorState => {
    this.setState({ editorState });
  };

  onEditorStateChange = editorState => {
    console.log("sbdhjf ", convertToRaw(editorState.getCurrentContent()));
    console.log("character count : "+this.getCharCount(editorState));
    this.setState({
      editorState,
      charTypeLength: this.getCharCount(editorState)
    });
  };

  getCharCount(editorState) {
    // const decodeUnicode = str => punycode.ucs2.decode(str); // func to handle unicode characters

    // let plainText = editorState.getCurrentContent().getPlainText('').trim();
    // let counterChar = plainText.length;
    // const dataSplit = plainText.split("\n");
    // if(dataSplit.length > 1){
    //   counterChar += dataSplit.length - 1;
    // }
    // console.log("text : ",plainText);

    // const regex = /(?:\r\n|\r|\n)/g; // new line, carriage return, line feed
    // const cleanString = plainText.replace(regex, '').trim(); // replace above characters w/ nothing


    let plainText = draftToHtml(
      convertToRaw(
        editorState.getCurrentContent()
      )
    )
    plainText = (plainText + "")
    .replace(/<[^>]*>/g, "")
    .replace(/&nbsp;/g, " ").trim()

    let counterChar = plainText.length

    return counterChar;
  }

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "100%",
            maxWidth: "1000px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={true}
        open={this.props.state.showModalSendSMS || false}
        onClose={() => this.props.onCloseModalSendSMS()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <OtosalesLoading isLoading={this.props.state.isLoading || false}>
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send SMS</b>
            </h4>
          </div>
          <div className="modal-body">
            <div className="row">
              <label className="col-xs-12 col-md-1">To </label>
              <div className="col-xs-12 col-md-11">
                <OtosalesSelectFullviewrev
                  name="phonelistselect"
                  value={this.props.phonelistselect}
                  selectOption={this.props.phonelistselect}
                  onOptionsChange={this.props.onOptionSelect2Change}
                  options={this.props.dataphonelist}
                  placeholder="Choose phone number"
                />
              </div>
            </div>
            <div className="row" style={{ marginTop: 10 }}>
              <label className="col-xs-12 col-md-1">Pesan </label>
              <div className="col-xs-12 col-md-11">
                <Editor
                  toolbarHidden
                  handleBeforeInput={this._handleBeforeInput}
                  handlePastedText={this._handlePastedText}
                  editorState={this.state.editorState}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor"
                  onEditorStateChange={this.onEditorStateChange}
                />
                <p>
                  {this.state.charTypeLength} / {this.props.maxLength}
                </p>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <div className="pull-right col-xs-2 col-md-1 text-center">
              <img
                className="img img-responsive"
                src={sendEmail}
                onClick={() => {
                  if(!Util.isSelectOptionContainValue(this.props.dataphonelist, this.props.phonelistselect)){
                    Util.showToast("Harap memilih nomor telephone","WARNING");
                    return;
                  }
                  if (/^[0-9-]{2,}[0-9]$/.test(Util.clearPhoneNo(this.props.phonelistselect.replace("+","")))) {
                    this.props.sendQuotationSMS(
                      this.props.phonelistselect,
                      draftToHtml(
                        convertToRaw(
                          this.state.editorState.getCurrentContent()
                        )
                      ), () => {
                        this.props.onCloseModalSendSMS();
                      }
                    );
                    
                  } else {
                    toast.dismiss();
                    toast.warning("❗ Phone number invalid", {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true
                    });
                  }
                }}
              />
              <span class="labelfontblack">Send</span>
            </div>
          </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  }
}
