import React from "react";
import Modal from "react-responsive-modal";
import {
  OtosalesDatePicker,
  OtosalesSelectFullviewv2,
  OtosalesLoading,
  Util
} from "./../../index";
import NumberFormat from "react-number-format";

function maxDate(props){
  if(props.state.chItemTemp == "TPLPER"){
    let basicCoverage = [...props.state.databasiccoverall].filter(
      data => data.Id == props.state.vehiclebasiccoverage
    )[0];

    if(basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0){
      return props.state.vehicleperiodto;
    }
    return Util.convertDate(
      Util.formatDate(props.state.vehicleperiodfrom)
    ).setFullYear(
      Util.convertDate(
        Util.formatDate(props.state.vehicleperiodfrom)
      ).getFullYear() + basicCoverage.ComprePeriod
    )
  }else if(props.state.chItemTemp == "TS"){
    return props.state.periodtoTS;
  }
  else{
    return props.state.vehicleperiodto;
  }
}

export default function ModalSetPeriod(props) {
  return (
    <Modal
      styles={{
        modal: {
          maxWidth: "410px",
          width: "100%"
        },
        overlay: {
          zIndex: 1050
        }
      }}
      showCloseIcon={false}
      open={props.state.showdialogperiod || false}
      onClose={() => props.onClose()}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-17px" }}
      >
        <OtosalesLoading isLoading={props.state.isLoading}>
          <div className="modal-header panel-heading">
            <h5 className="modal-title" id="exampleModalLabel">
              Periode Pertanggungan
            </h5>
          </div>
          <div className="modal-body">
            {props.state.chItemTemp != "ACCESS" && (
              <div className="row" style={{ marginBottom: "10px" }}>
                <div className="col-xs-12">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={props.state.chItemSetPeriod}
                        name="chItemSetPeriod"
                        value={props.state.chItemSetPeriod}
                        onChange={event => props.onChangeFunction(event)}
                        disabled={
                          props.state.chItemSetPeriodEnable == 1 ? false : true
                        }
                      />
                      <span className="labelspanbold">Period</span>
                    </label>
                  </div>
                </div>
                {props.state.chItemSetPeriod && (
                  <React.Fragment>
                    <div className="col-xs-6">
                      <span className="labelspanbold">Period From</span>
                      <OtosalesDatePicker
                        minDate={Util.convertDate().setDate(Util.convertDate().getDate() + 1)}
                        // maxDate={(props.state.vehicleperiodfrommodalperiod !=
                        // null
                        //   ? Util.convertDate(
                        //       Util.formatDate(
                        //         props.state.vehicleperiodfrommodalperiod
                        //       )
                        //     )
                        //   : Util.convertDate()
                        // ).setDate(Util.convertDate().getFullYear() + 5)}
                        maxDate = {maxDate(props)}
                        className="form-control"
                        name="vehicleperiodfrommodalperiod"
                        selected={props.state.vehicleperiodfrommodalperiod}
                        dateFormat="dd/mm/yyyy"
                        onChange={props.onChangeFunctionDate}
                      />
                    </div>
                    <div className="col-xs-6">
                      <span className="labelspanbold">Period To</span>
                      <OtosalesDatePicker
                        minDate={
                          props.state.vehicleperiodfrommodalperiod != null
                            ? Util.convertDate(
                                Util.formatDate(
                                  props.state.vehicleperiodfrommodalperiod,
                                  "yyyy-mm-dd"
                                )
                              )
                            : Util.convertDate().setDate(Util.convertDate().getDate() + 1)
                        }
                        // maxDate={(props.state.vehicleperiodfrommodalperiod !=
                        // null
                        //   ? Util.convertDate(
                        //       Util.formatDate(
                        //         props.state.vehicleperiodfrommodalperiod
                        //       )
                        //     )
                        //   : Util.convertDate()
                        // ).setDate(Util.convertDate().getFullYear() + 5)}
                        maxDate = {maxDate(props)}
                        className="form-control"
                        name="vehicleperiodtomodalperiod"
                        selected={props.state.vehicleperiodtomodalperiod}
                        dateFormat="dd/mm/yyyy"
                        onChange={props.onChangeFunctionDate}
                      />  
                    </div>
                  </React.Fragment>
                )}
              </div>
            )}
            {props.state.chItemTemp == "PADRVR" && props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">PA Driver SI</span>
                <div className="col-xs-12 ">
                { (props.state.vehicleInsuranceType == 3) &&
                  <NumberFormat
                    allowNegative={false}
                    className="form-control"
                    onValueChange={value => {
                      props.onChangeFunctionReactNumber(
                        value.floatValue,
                        "PADRVCOVERtemp"
                      );
                    }}
                    value={props.state.PADRVCOVERtemp}
                    thousandSeparator={true}
                    prefix={""}
                  />
                }
                { (props.state.vehicleInsuranceType != 3) &&
                  <OtosalesSelectFullviewv2
                    name="PADRVCOVERtemp"
                    value={props.state.PADRVCOVERtemp}
                    selectOption={props.state.PADRVCOVERtemp}
                    onOptionsChange={props.onOptionSelect2Change}
                    options={props.state.dataextsi}
                    placeholder=""
                  />
                }
                </div>
              </div>
            )}
            {props.state.chItemTemp == "PAPASS" && props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">PA Passanger SI</span>
                <div className="col-md-3 col-xs-4 panel-body-list">
                  <div className="col-xs-7" style={{ paddingRight: "0px" }}>
                    <input
                      className="form-control"
                      type="number"
                      name="PASSCOVER"
                      disabled={true} ///
                      value={props.state.PASSCOVER}
                      onChange={event => {
                        props.onChangeFunction(event);
                      }}
                    />
                  </div>
                  <div className="col-xs-4">@</div>
                </div>
                <div
                  className="col-md-9 col-xs-8 panel-body-list"
                  style={{
                    marginLeft: "-5px",
                    paddingRight: "10px"
                  }}
                >
                  { (props.state.vehicleInsuranceType == 3) &&
                  <NumberFormat
                    className="form-control"
                    onValueChange={value => {
                      props.onChangeFunctionReactNumber(
                        value.floatValue,
                        "PAPASSICOVERtemp"
                      );
                    }}
                    value={props.state.PAPASSICOVERtemp}
                    thousandSeparator={true}
                    prefix={""}
                  />
                  }
                  { (props.state.vehicleInsuranceType != 3) &&
                  <OtosalesSelectFullviewv2
                    name="PAPASSICOVERtemp"
                    value={props.state.PAPASSICOVERtemp}
                    selectOption={props.state.PAPASSICOVERtemp}
                    onOptionsChange={props.onOptionSelect2Change}
                    options={props.state.dataextsi}
                    placeholder=""
                  />
                  }
                </div>
              </div>
            )}
            {props.state.chItemTemp == "TPLPER" && props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">TPL SI</span>
                <div className="col-xs-12">
                { props.state.dataTPLSI.length == 0 &&
                  <NumberFormat
                        allowNegative={false}
                        className="form-control"
                        onValueChange={value => {
                          props.onChangeFunctionReactNumber("", "TPLCoverageIdtemp");
                          props.onChangeFunctionReactNumber(
                            value.floatValue,
                            "TPLSICOVERtemp"
                          );
                        }}
                        value={props.state.TPLSICOVERtemp}
                        thousandSeparator={true}
                        prefix={""}
                    />
                    }
                  { props.state.dataTPLSI.length > 0 &&
                  <OtosalesSelectFullviewv2
                    name="TPLCoverageIdtemp"
                    value={props.state.TPLCoverageIdtemp}
                    selectOption={props.state.TPLCoverageIdtemp}
                    onOptionsChange={props.onOptionSelect2Change}
                    options={props.state.dataTPLSI}
                    placeholder=""
                  />
                  }
                </div>
              </div>
            )}
            {props.state.chItemTemp == "ACCESS" && (
              <div className="row">
                <div className="col-xs-12">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={props.state.chItemSetPeriod}
                        name="chItemSetPeriod"
                        value={props.state.chItemSetPeriod}
                        onChange={event => props.onChangeFunction(event)}
                        disabled={
                          props.state.chItemSetPeriodEnable == 1 ? false : true
                        }
                      />
                      <span className="labelspanbold">Cover Accessories</span>
                    </label>
                  </div>
                </div>
                {props.state.chItemSetPeriod && (
                  <React.Fragment>
                    <span className="col-xs-12 labelspanbold">Access SI</span>
                    <div className="col-xs-12">
                      <NumberFormat
                        allowNegative={false}
                        className="form-control"
                        onValueChange={value => {
                          props.onChangeFunctionReactNumber(
                            value.floatValue,
                            "ACCESSCOVERtemp"
                          );
                        }}
                        value={props.state.ACCESSCOVERtemp}
                        thousandSeparator={true}
                        prefix={""}
                      />
                    </div>
                  </React.Fragment>
                )}
              </div>
            )}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    props.onClose();
                  }}
                  className="btn btn-sm btn-warning form-control"
                >
                  Cancel
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => props.rateCalculationNonBasic()}
                  className=" btn btn-sm btn-info form-control"
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </OtosalesLoading>
      </div>
    </Modal>
  );
}
