import React, { Component } from "react";
import Modal from "react-responsive-modal";
import PropTypes from "prop-types";
import { Call, savedisk } from "./../../../assets";
import OtosalesLoading from "../loading/otosalesloading";
import * as Util from "./../../helpers/Util";
import * as Parameterized from "../../../parameterizedata";
import { Log } from "../..";

class OtosalesModalMakeSure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issetprimarymodal: false,
      phonenumbertype: this.props.phonenumbertype,
      picname: ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.phonenumbertype != this.props.phonenumbertype 
      // || nextProps.phonenumbertype != this.state.phonenumbertype
      ) {
      this.setState({
        phonenumbertype: nextProps.phonenumbertype
      });
    }

    if (nextProps.phonePicName != this.props.phonePicName 
      // || nextProps.phonePicName != this.state.picname
      ) {
      this.setState({
        picname: nextProps.phonePicName
      });
    }

    if(nextProps.issetprimarymodal != this.props.issetprimarymodal 
      // || nextProps.issetprimarymodal != this.state.issetprimarymodal
      ){
      this.setState({
        issetprimarymodal: nextProps.issetprimarymodal
      })
    }

    // if(nextProps.showInputPhoneNo == true){
    //   this.setState({
    //     issetprimarymodal: false,
    //     phonenumbertype: this.props.phonenumbertype,
    //     picname: ""
    //   })
    // }
  }

  onChangeFunction = event => {
    var nameEvent = event.target.name;
    const value =
      event.target[
        event.target.type === "checkbox"
          ? "checked"
          : event.target.type === "radio"
          ? "checked"
          : "value"
      ];
    const name = event.target.name;
    let pattern = event.target.pattern;

    if (!Util.isNullOrEmpty(pattern)) {
      // if value is not blank, then test the regex
      pattern = new RegExp(pattern);
      if (value === "" || pattern.test(value)) {
        this.setState({ [name]: value });
      }
    } else {
      this.setState({
        [name]: value
      });
    }

    if(name == "phonenumbertype"){
      if(this.props.state.ProspectCustomer.isCompany == 1) {
        if(this.detectMobilePhone(value)){  
          this.setState({
            issetprimarymodal: true
          })
        }else{
          this.setState({
            issetprimarymodal: false
          })
        }
      }
    }
  };

  AdditionalModal = () => {
    return (
      <React.Fragment>
        {!Util.isNullOrEmpty(this.props.state.ProspectCustomer) &&
          this.props.state.ProspectCustomer.isCompany == 0 && (
            <div
              className={
                "row" + (this.state.IsNotErrorrelationship ? " has-error" : "")
              }
            >
              <div className="col-xs-12">
                <span>Jelaskan hubungan PIC Call dengan Tertanggung : </span>
              </div>
              {this.props.relationComponent}
            </div>
          )}
        <div className={
                "row" + (this.state.IsNotErrorpicnamecall ? " has-error" : "")
              } style={{marginTop: "8px"}}>
          <div className="col-xs-4">
            <label>Input PIC Name</label>
          </div>
          <div className="col-xs-8" style={{paddingLeft: "0px"}}>
            <input
              type="text"
              // pattern="^[0-9]+$"
              disabled={!this.props.isEnableEditPicName}
              maxLength="50"
              className="form-control"
              name="picname"
              value={this.state.picname}
              onChange={event => this.onChangeFunction(event)}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  detectMobilePhone = PhoneNumber => {
    let temp = false;
    PhoneNumber += "";
    if (Util.detectPrefix(Util.clearPhoneNo(PhoneNumber), Parameterized.mobilePrefix)) {
      temp = true;
    }

    return temp;
  };

  detectLandLinePhone = PhoneNumber => {
    let temp = false;
    PhoneNumber += "";
    if (Util.detectPrefix(Util.clearPhoneNo(PhoneNumber), Parameterized.landLinePrefix)) {
      temp = true;
    }

    return temp;
  };

  isCompanyData = () => {
    let temp = false; 
    if(!Util.isNullOrEmpty(this.props.state.ProspectCustomer)){
      if( this.props.state.ProspectCustomer.isCompany == 1){
        temp = true
      }
    }

    return temp;
  }

  onInsertUpdatePhoneNo = () => {
    if (!this.ValidationData()) {
      Util.showToast("Harap mengisi semua data yang diperlukan.", "WARNING");
      return;
    }

    if(this.props.showInputPhoneNo && !Util.isNullOrEmpty(this.state.phonenumbertype)){
      if(this.state.phonenumbertype.length < 6 ){
        Util.showToast("Harap memasukan nomor telepon yang valid.", "WARNING");
          this.setState({
            IsNotErrorphonenumber: true
          });
          return;
      }
    }

      let dataReasonReject = this.props.searchDataReasonReject(this.props.state.modalchecklistrejectreasonInputNo);
      let CallRejectReasonId = 0;
      let IsWrongNumber = false;
      let isConnected = this.props.state.connectedModalInputPhoneNo;

      Log.debugGroupCollapsed("dataReasonReject", dataReasonReject);
      if(!Util.isNullOrEmpty(dataReasonReject)){
        CallRejectReasonId = this.props.state.modalchecklistrejectreasonInputNo;
        IsWrongNumber = dataReasonReject.IsValid ? false : true;
        isConnected = dataReasonReject.IsValid;
      }

      let PhoneNoParam = [
        {
          PhoneNo: Util.clearPhoneNo(this.state.phonenumbertype),
          ColumnName: this.props.ColumnName || "",
          Relation: this.props.state.modalchecklistrelationsip || "",
          IsPrimary: this.state.issetprimarymodal,
          IsWrongNumber,
          IsLandLine: this.detectLandLinePhone(Util.clearPhoneNo(this.state.phonenumbertype)), 
          isConnected,
          PICName: this.state.picname,
          CallRejectReasonId
        }
      ];

      if(this.props.showInputPhoneNo){
        if(!this.detectMobilePhone(Util.clearPhoneNo(this.state.phonenumbertype))){
          PhoneNoParam = [
            {
              PhoneNo: Util.clearPhoneNo(this.state.phonenumbertype),
              ColumnName: "",
              Relation: "",
              IsPrimary: false,
              IsWrongNumber,
              IsLandLine: !this.detectMobilePhone(Util.clearPhoneNo(this.state.phonenumbertype)), 
              isConnected,
              PICName: "",
              CallRejectReasonId
            }
          ];
        }
      }
      
      this.props.InsertUpdatePhoneNo(
        this.props.state.account.UserInfo.User.SalesOfficerID,
        this.props.state.OrderNo,
        PhoneNoParam,
        () => {
          if(this.props.showInputPhoneNo == false){
            this.props.onUpdatePhoneFunction(this.state.phonenumbertype)
          }
          if(this.props.showInputPhoneNo){
            this.setState({
              phonenumbertype: "",
              // picname: "",
              issetprimarymodal: false
            }, () => {
              if(!this.isCompanyData()){
                this.setState({picname: ""})
              }
            })
          }
          this.props.onCloseMakeSure();
        }
      );
  };

  onClickCallButton = () => {
    if(Util.isNullOrEmpty(this.state.phonenumbertype)){
      Util.showToast("Silahkan input phone number terlebih dahulu!", "WARNING");
      return
    }
    this.props.onClickCallButtonInputPhone(this.state.phonenumbertype)
   }

  validationInput = () => {
    let validation = true;
    let messages = "";

    if(this.props.showInputPhoneNo && Util.isNullOrEmpty(this.state.phonenumbertype)){
      validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Phone number is mandatory"
            : messages;
          this.setState({
            IsNotErrorphonenumber: true
          });
    }

    if(this.state.issetprimarymodal){

      if(this.props.state.ProspectCustomer.isCompany == 0) {

        if (
          Util.isNullOrEmpty(this.props.state.modalchecklistrelationsip)
        ) {
          validation = validation ? false : validation;
          messages = Util.isNullOrEmpty(messages)
            ? "Relationship PIC Call with Insured mandatory"
            : messages;
          this.setState({
            IsNotErrorrelationship: true
          });
        }

      }

      if (
        Util.isNullOrEmpty(this.state.picname)
      ) {
        validation = validation ? false : validation;
        messages = Util.isNullOrEmpty(messages)
          ? "PIC Call Name mandatory"
          : messages;
        this.setState({
          IsNotErrorpicnamecall: true
        });
      }

    }

    let returnvalidation = { validation: validation, messages: messages };
    return returnvalidation;
  };

  ValidationData = () => {
    let validation = true;
    let messages = "";

    let validationData = { validation: true, messages: "" };

    this.initializeErrorFlagMandatory();

    validationData = this.validationInput();
    validation = validationData.validation
      ? validation
      : validationData.validation;
    messages = Util.isNullOrEmpty(messages)
      ? validationData.messages
      : messages;

    // if (!Util.isNullOrEmpty(messages)) {
    //   Util.showToast(messages, "WARNING");
    // }
    return validation;
  };

  initializeErrorFlagMandatory = () => {
    this.setState({
      IsNotErrorrelationship: false,
      IsNotErrorpicnamecall: false, 
      IsNotErrorphonenumber: false
    });
  };

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "95%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.props.showModal}
        onClose={() => {
          this.initializeErrorFlagMandatory();
          if(this.props.showInputPhoneNo){
            this.setState({
              phonenumbertype: "",
              // picname: "",
              issetprimarymodal: false
            }, () => {
              if(!this.isCompanyData()){
                this.setState({picname: ""})
              }
            })
          }
          this.props.onCloseMakeSure()
        }}
        showCloseIcon={true}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px", fontSize: "initial" }}
        >
          <OtosalesLoading isLoading={this.props.state.isLoading || false}>
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>{this.props.title || "Add Phone Number"}</b>
            </h4>
          </div>
          <div
            className="modal-body"
            style={{
              fontSize: Util.isNullOrEmpty(this.props.state)
                ? "11pt"
                : this.props.state.width > this.props.state.height
                ? "11pt"
                : "8pt"
            }}
          >
            {this.props.showInputPhoneNo && (
              <div className={
                "row" + (this.state.IsNotErrorphonenumber ? " has-error" : "")
              }>
                <div className="col-xs-4">
                  <label>Input New Phone Number</label>
                </div>
                <div className="col-xs-6 panel-body-list">
                  <input
                    type="text"
                    maxLength="14"
                    pattern="^[0-9]+$"
                    name="phonenumbertype"
                    className="form-control"
                    value={this.state.phonenumbertype}
                    onChange={event => this.onChangeFunction(event)}
                  />
                </div>
                <div className="col-xs-2" onClick={() => {
                  this.onClickCallButton()
                }}>
                  <img
                    className="img img-responsive pull-right"
                    style={{ maxWidth: "20px" }}
                    src={Call}
                  />
                </div>
              </div>
            )}
            {Util.detectPrefix(Util.clearPhoneNo(this.state.phonenumbertype), ["628", "08"]) && this.props.showInputPhoneNo && (
              <div className="row">
                <div className="col-xs-6">Set Primary data ?</div>
                <div className="col-xs-6 text-right">
                  <div
                    class="checkbox"
                    style={{ marginTop: 0, marginBottom: 0 }}
                  >
                    <label>
                      <input
                        type="checkbox"
                        name="issetprimarymodal"
                        checked={this.state.issetprimarymodal}
                        disabled={this.isCompanyData() ? true : false}
                        onChange={event => this.onChangeFunction(event)}
                      />
                    </label>
                  </div>
                </div>
              </div>
            )}
            {this.detectMobilePhone(Util.clearPhoneNo(this.state.phonenumbertype)) && (
              <this.AdditionalModal />
            )}
          </div>
          <div className="modal-footer">
            <div className="pull-right">
              <div
                className="col-xs-3"
                onClick={() => {
                  this.initializeErrorFlagMandatory();
                  this.onInsertUpdatePhoneNo();
                }}
              >
                <img
                  className="img img-responsive"
                  src={savedisk}
                  style={{ maxWidth: "35px" }}
                />
              </div>
            </div>
          </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  }
}

OtosalesModalMakeSure.propTypes = {
  showModal: PropTypes.bool.isRequired,
  title: PropTypes.string,
  messages: PropTypes.string,
  onSubmitMakeSure: PropTypes.func,
  onCloseMakeSure: PropTypes.func.isRequired,
  showInputPhoneNo: PropTypes.bool,
  showModalAddPhone: PropTypes.bool,
  ColumnNameInsertUpdatePhone: PropTypes.string,
  phonenumbertype: PropTypes.string
};

export default OtosalesModalMakeSure;
