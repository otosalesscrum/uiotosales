import React, { Component } from "react";
import Modal from "react-responsive-modal";
import {
  OtosalesDatePicker,
  OtosalesSelectFullviewv2,
  OtosalesLoading,
  Util,
  Log
} from "../../index";
import NumberFormat from "react-number-format";

export default class OtosalesModalSetPeriodPremi2 extends Component{

  constructor(props) {
    super(props);

    this.state = {
      tempTPLCoverageId: Util.isNullOrEmpty(this.props.state.TPLCoverageId)? "" : this.props.state.TPLCoverageId,
      tempTPLSICOVER: Util.isNullOrEmpty(this.props.state.TPLSICOVER)? "" : this.props.state.TPLSICOVER,
      tempACCESSCOVER: Util.isNullOrEmpty(this.props.state.ACCESSCOVER)? "" : this.props.state.ACCESSCOVER,
      tempPADRVR : Util.isNullOrEmpty(this.props.state.PADRVCOVER)? "" : this.props.state.PADRVCOVER,
      tempPAPASSICOVER : Util.isNullOrEmpty(this.props.state.PAPASSICOVER)? "" : this.props.state.PAPASSICOVER
    };
  }

  // componentDidUpdate(prevProps){
  //   if(prevProps != this.props){
  //     this.setState({
  //       tempTPLCoverageId: Util.isNullOrEmpty(this.props.state.TPLCoverageId)? "" : this.props.state.TPLCoverageId,
  //       tempTPLSICOVER: Util.isNullOrEmpty(this.props.state.TPLSICOVER)? "" : this.props.state.TPLSICOVER,
  //       tempACCESSCOVER: Util.isNullOrEmpty(this.props.state.ACCESSCOVER)? "" : this.props.state.ACCESSCOVER,
  //       tempPADRVR : Util.isNullOrEmpty(this.props.state.PADRVCOVER)? "" : this.props.state.PADRVCOVER,
  //       tempPAPASSICOVER : Util.isNullOrEmpty(this.props.state.PAPASSICOVER)? "" : this.props.state.PAPASSICOVER
  //     })
  //   }
  // }

  // shouldComponentUpdate(prevProps){
  //   if(prevProps.state != this.props.state){
  //     this.setState({
  //       tempTPLCoverageId: Util.isNullOrEmpty(this.props.state.TPLCoverageId)? "" : this.props.state.TPLCoverageId,
  //       tempTPLSICOVER: Util.isNullOrEmpty(this.props.state.TPLSICOVER)? "" : this.props.state.TPLSICOVER,
  //       tempACCESSCOVER: Util.isNullOrEmpty(this.props.state.ACCESSCOVER)? "" : this.props.state.ACCESSCOVER,
  //       tempPADRVR : Util.isNullOrEmpty(this.props.state.PADRVCOVER)? "" : this.props.state.PADRVCOVER,
  //       tempPAPASSICOVER : Util.isNullOrEmpty(this.props.state.PAPASSICOVER)? "" : this.props.state.PAPASSICOVER
  //       })
  //     }    
  //     return true;
  //   }

  clickOk(){
    if(this.props.state.chItemTemp == "TPLPER"){
      this.props.onChangeFunctionReactNumber(
        this.state.tempTPLCoverageId,
        "TPLCoverageId",
        () => {
          this.props.onChangeFunctionReactNumber(
            this.state.tempTPLSICOVER,
            "TPLSICOVER",
            () => {
              this.props.rateCalculationNonBasic();
            }
          );
        }
      );
    }else if(this.props.state.chItemTemp == "ACCESS"){
      this.props.onChangeFunctionReactNumber(
        this.state.tempACCESSCOVER,
        "ACCESSCOVER",
        () => {
          this.props.rateCalculationNonBasic();
        }
      );
    }else if(this.props.state.chItemTemp == "PADRVR"){
      this.props.onChangeFunctionReactNumber(
        this.state.tempPADRVR,
        "PADRVCOVER",
        () => {
          this.props.rateCalculationNonBasic();
        }
      );
    }else if(this.props.state.chItemTemp == "PAPASS"){
      this.props.onChangeFunctionReactNumber(
        this.state.tempPAPASSICOVER,
        "PAPASSICOVER",
        () => {
          this.props.rateCalculationNonBasic();
        }
      );
    }else{
      this.props.rateCalculationNonBasic();
    }
  }

  clickClose(){
      this.state.tempACCESSCOVER = this.props.state.ACCESSCOVER;
      this.state.tempPADRVR = this.props.state.PADRVCOVER;
      this.state.tempPAPASSICOVER = this.props.state.PAPASSICOVER;
      this.props.onClose();
  }

  onOptionSelect2Change = (value, name) => {
    Log.debugGroupCollapsed("shjdkfs :", this.props.state.dataextsi);
    Log.debugStr("VALUE: "+ value);
    Log.debugStr("NAME: "+ name);
    this.setState({ [name]: value }, () => {
      Log.debugStr(name+" : "+this.state[name]);
    });

    if (name == "tempTPLCoverageId") {
      // this.rateCalculateTPLPAPASSPADRV("TPL");
      let tempTPLSICOVER = [...this.props.state.dataTPLSIall].filter(
        data => data.CoverageId == value
      )[0].TSI;
      this.setState({
        tempTPLSICOVER
      });
    }

  }

  // maxDate(props){
  //   if(props.state.chItemTemp == "TPLPER"){
  //     let basicCoverage = [...props.state.databasiccoverall].filter(
  //       data => data.Id == props.state.CoverBasicCover
  //     )[0];
  //     return Util.convertDate(
  //       Util.formatDate(props.state.vehicleperiodfrom)
  //     ).setFullYear(
  //       Util.convertDate(
  //         Util.formatDate(props.state.vehicleperiodfrom)
  //       ).getFullYear() + basicCoverage.ComprePeriod
  //     )
  //   }else{
  //     return props.state.vehicleperiodto;
  //   }
  // }

 maxDate(props){
    if(props.state.chItemTemp == "TPLPER"){
      let basicCoverage = [...props.state.databasiccoverall].filter(
        data => data.Id == props.state.CoverBasicCover
      )[0];
  
      if(basicCoverage.TLOPeriod == 0 && basicCoverage.ComprePeriod == 0){
        return props.state.vehicleperiodto;
      }
      return Util.convertDate(
        Util.formatDate(props.state.vehicleperiodfrom)
      ).setFullYear(
        Util.convertDate(
          Util.formatDate(props.state.vehicleperiodfrom)
        ).getFullYear() + basicCoverage.ComprePeriod
      )
    }else if(props.state.chItemTemp == "TS"){
      return props.state.periodtoTS;
    }
    else{
      return props.state.vehicleperiodto;
    }
  }

  render() {
    return(
    <Modal
      styles={{
        modal: {
          maxWidth: "410px",
          width: "100%"
        },
        overlay: {
          zIndex: 1050
        }
      }}
      showCloseIcon={false}
      open={this.props.state.showdialogperiod || false}
      onClose={() => this.clickClose()}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-17px" }}
      >
        <OtosalesLoading isLoading={this.props.state.isLoading}>
          <div className="modal-header panel-heading">
            <h5 className="modal-title" id="exampleModalLabel">
              Periode Pertanggungan
            </h5>
          </div>
          <div className="modal-body">
            {this.props.state.chItemTemp != "ACCESS" && (
              <div className="row" style={{ marginBottom: "10px" }}>
                <div className="col-xs-12">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.props.state.chItemSetPeriod}
                        name="chItemSetPeriod"
                        value={this.props.state.chItemSetPeriod}
                        onChange={event => this.props.onChangeFunction(event)}
                        disabled={
                          this.props.state.chItemSetPeriodEnable == 1 ? false : true
                        }
                      />
                      <span className="labelspanbold">Period</span>
                    </label>
                  </div>
                </div>
                {this.props.state.chItemSetPeriod && (
                  <React.Fragment>
                    <div className="col-xs-6">
                      <span className="labelspanbold">Period From</span>
                      <OtosalesDatePicker
                        minDate={Util.convertDate().setDate(Util.convertDate().getDate() + 1)}
                        maxDate={this.maxDate(this.props)}
                        //{(this.props.state.vehicleperiodfrommodalperiod !=
                        // null
                        //   ? Util.convertDate(
                        //       Util.formatDate(
                        //         this.props.state.vehicleperiodfrommodalperiod
                        //       )
                        //     )
                        //   : Util.convertDate()
                        // ).setDate(Util.convertDate().getFullYear() + 5)}
                        className="form-control"
                        name="vehicleperiodfrommodalperiod"
                        selected={this.props.state.vehicleperiodfrommodalperiod}
                        dateFormat="dd/mm/yyyy"
                        onChange={this.props.onChangeFunctionDate}
                      />
                    </div>
                    <div className="col-xs-6">
                      <span className="labelspanbold">Period To</span>
                      <OtosalesDatePicker
                        minDate={
                          this.props.state.vehicleperiodfrommodalperiod != null
                            ? Util.convertDate(
                                Util.formatDate(
                                  this.props.state.vehicleperiodfrommodalperiod,
                                  "yyyy-mm-dd"
                                )
                              )
                            : Util.convertDate().setDate(Util.convertDate().getDate() + 1)
                        }
                        maxDate={this.maxDate(this.props)}
                        // {(this.props.state.vehicleperiodfrommodalperiod !=
                        // null
                        //   ? Util.convertDate(
                        //       Util.formatDate(
                        //         this.props.state.vehicleperiodfrommodalperiod
                        //       )
                        //     )
                        //   : Util.convertDate()
                        // ).setDate(Util.convertDate().getFullYear() + 5)}
                        className="form-control"
                        name="vehicleperiodtomodalperiod"
                        selected={this.props.state.vehicleperiodtomodalperiod}
                        dateFormat="dd/mm/yyyy"
                        onChange={this.props.onChangeFunctionDate}
                      />
                    </div>
                  </React.Fragment>
                )}
              </div>
            )}
            {this.props.state.chItemTemp == "PADRVR" && this.props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">PA Driver SI</span>
                <div className="col-xs-12 ">
                  {this.props.state.CoverProductType == 3 &&
                  <NumberFormat
                    allowNegative={false}
                    className="form-control"
                    onValueChange={value => {
                      this.setState({
                        tempPADRVR: value.floatValue
                      })
                    }}
                    value={this.state.tempPADRVR}
                    thousandSeparator={true}
                    prefix={""}
                  />
                  }
                  {this.props.state.CoverProductType != 3 &&
                  <OtosalesSelectFullviewv2
                    name="tempPADRVR"
                    value={this.state.tempPADRVR}
                    selectOption={this.state.tempPADRVR}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.props.state.dataextsi}
                    placeholder=""
                  />
                  }
                </div>
              </div>
            )}
            {this.props.state.chItemTemp == "PAPASS" && this.props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">PA Passanger SI</span>
                <div className="col-md-3 col-xs-4 panel-body-list">
                  <div className="col-xs-7" style={{ paddingRight: "0px" }}>
                    <input
                      className="form-control"
                      type="number"
                      name="PASSCOVER"
                      disabled={true} ///
                      value={this.props.state.PASSCOVER}
                      onChange={event => {
                        this.props.onChangeFunction(event);
                      }}
                    />
                  </div>
                  <div className="col-xs-4">@</div>
                </div>
                <div
                  className="col-md-9 col-xs-8 panel-body-list"
                  style={{
                    marginLeft: "-5px",
                    paddingRight: "10px"
                  }}
                >
                  {this.props.state.CoverProductType == 3 &&
                  <NumberFormat
                    className="form-control"
                    onValueChange={value => {
                      this.setState({
                        tempPAPASSICOVER: value.floatValue
                      })
                    }}
                    value={this.state.tempPAPASSICOVER}
                    thousandSeparator={true}
                    prefix={""}
                  />
                  }
                  {this.props.state.CoverProductType != 3 &&
                  <OtosalesSelectFullviewv2
                    name="tempPAPASSICOVER"
                    value={this.state.tempPAPASSICOVER}
                    selectOption={this.state.tempPAPASSICOVER}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.props.state.dataextsi}
                    placeholder=""
                  />
                  }
                </div>
              </div>
            )}
            {this.props.state.chItemTemp == "TPLPER" && this.props.state.chItemSetPeriod && (
              <div className="row">
                <span className="col-xs-12 labelspanbold">TPL SI</span>
                <div className="col-xs-12">
                { this.props.state.dataTPLSI.length == 0 &&
                  <NumberFormat
                        allowNegative={false}
                        className="form-control"
                        onValueChange={value => {
                              this.setState({tempTPLCoverageId: "", tempTPLSICOVER:value.floatValue})
                            
                          // this.props.state.tempACCESSCOVER;
                        }}
                        value={this.state.tempTPLSICOVER}
                        thousandSeparator={true}
                        prefix={""}
                      />
                }
                { this.props.state.dataTPLSI.length > 0 &&
                  <OtosalesSelectFullviewv2
                    name="tempTPLCoverageId"
                    value={this.state.tempTPLCoverageId}
                    selectOption={this.state.tempTPLCoverageId}
                    onOptionsChange={this.onOptionSelect2Change}
                    options={this.props.state.dataTPLSI}
                    placeholder=""
                  />
                }
                </div>
              </div>
            )}
            {this.props.state.chItemTemp == "ACCESS" && (
              <div className="row">
                <div className="col-xs-12">
                  <div className="checkbox">
                    <label>
                      <input
                        type="checkbox"
                        checked={this.props.state.chItemSetPeriod}
                        name="chItemSetPeriod"
                        value={this.props.state.chItemSetPeriod}
                        onChange={event => this.props.onChangeFunction(event)}
                        disabled={
                          this.props.state.chItemSetPeriodEnable == 1 ? false : true
                        }
                      />
                      <span className="labelspanbold">Cover Accessories</span>
                    </label>
                  </div>
                </div>
                {this.props.state.chItemSetPeriod && (
                  <React.Fragment>
                    <span className="col-xs-12 labelspanbold">Access SI</span>
                    <div className="col-xs-12">
                      <NumberFormat
                        allowNegative={false}
                        className="form-control"
                        onValueChange={value => {
                          this.setState({tempACCESSCOVER: value.floatValue}, 
                            () => {
                              // this.props.onChangeFunctionReactNumber(
                              //   value.floatValue,
                              //   ""
                              // );
                            });
                          // this.props.state.tempACCESSCOVER;
                        }}
                        value={this.state.tempACCESSCOVER}
                        thousandSeparator={true}
                        prefix={""}
                      />
                    </div>
                  </React.Fragment>
                )}
              </div>
            )}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => {
                    this.clickClose();
                  }
                  }
                  className="btn btn-sm btn-warning form-control"
                >
                  Cancel
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={
                     () => {
                      this.clickOk();                                   
                  }}
                  className=" btn btn-sm btn-info form-control"
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </OtosalesLoading>
      </div>
    </Modal>
    );
  }
}

