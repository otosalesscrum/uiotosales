import React from 'react';
import Modal from "react-responsive-modal";

export default function ModalSetPeriod(props) {
    return (
        <Modal
        styles={{
          modal: {
            width: "210px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={props.state.showdialogperiod || false}
        onClose={() => props.onClose()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-header panel-heading">
            <h5 className="modal-title" id="exampleModalLabel">
              Periode Pertanggungan
            </h5>
          </div>
          <div className="modal-body text-center">
            {props.state.chOneEnabledVisible == 1 ? (
              <div className="col-xs-12">
                <input
                  type="checkbox"
                  name="chOne"
                  onChange={props.onChangeFunctionChecked}
                  value={props.state.chOne}
                  checked={props.state.chOne}
                  disabled={props.state.chOneEnabled == 0 ? true : false}
                />
                <label style={{ marginLeft: "10px" }}>1 Tahun</label>
              </div>
            ) : (
              ""
            )}
            {props.state.chTwoEnableVisible == 1 ? (
              <div className="col-xs-12">
                <input
                  type="checkbox"
                  name="chTwo"
                  onChange={props.onChangeFunctionChecked}
                  value={props.state.chTwo}
                  checked={props.state.chTwo}
                  disabled={props.state.chTwoEnable == 0 ? true : false}
                />
                <label style={{ marginLeft: "10px" }}>2 Tahun</label>
              </div>
            ) : (
              ""
            )}
            {props.state.chThreeEnabledVisible == 1 ? (
              <div className="col-xs-12">
                <input
                  type="checkbox"
                  name="chThree"
                  onChange={props.onChangeFunctionChecked}
                  value={props.state.chThree}
                  checked={props.state.chThree}
                  disabled={props.state.chThreeEnabled == 0 ? true : false}
                />
                <label style={{ marginLeft: "10px" }}>3 Tahun</label>
              </div>
            ) : (
              ""
            )}
            {props.state.chFourEnabledVisible == 1 ? (
              <div className="col-xs-12">
                <input
                  type="checkbox"
                  name="chFour"
                  onChange={props.onChangeFunctionChecked}
                  value={props.state.chFour}
                  checked={props.state.chFour}
                  disabled={props.state.chFourEnabled == 0 ? true : false}
                />
                <label style={{ marginLeft: "10px" }}>4 Tahun</label>
              </div>
            ) : (
              ""
            )}
            {props.state.chFiveEnabledVisible == 1 ? (
              <div className="col-xs-12">
                <input
                  type="checkbox"
                  name="chFive"
                  onChange={props.onChangeFunctionChecked}
                  value={props.state.chFive}
                  checked={props.state.chFive}
                  disabled={props.state.chFiveEnabled == 0 ? true : false}
                />
                <label style={{ marginLeft: "10px" }}>5 Tahun</label>
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => { props.onClose() }}
                  className="btn btn-sm btn-warning form-control"
                >
                  Cancel
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => props.rateCalculationNonBasic()}
                  className=" btn btn-sm btn-info form-control"
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
}
