import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import PropTypes from "prop-types";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  export default function AlertDialogSlide(props) {
  
    return (
      <div>
        <Dialog
          open={props.showModalInfo}
          TransitionComponent={Transition}
          keepMounted
          onClose={props.onClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">{props.title || "Information"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              {props.message || ""}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="outlined" onClick={props.onClose} color="primary">
              { props.btnno || "No" }
            </Button>
            <Button variant="outlined" onClick={props.onSubmit} color="primary">
              { props.btnyes|| "Yes" }
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  AlertDialogSlide.propTypes = {
    showModalInfo: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
    btnno: PropTypes.string,
    btnyes: PropTypes.string
  };