import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { OtosalesSelect2, OtosalesDatePicker } from "../../../otosalescomponents";
import { API_URL, API_VERSION } from "../../../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { toast } from "react-toastify";
import { secureStorage } from "./../../helpers/SecureWebStorage"; 

class otosalesmodaltasklistfilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      orderByItemModels: [],
      itemModelsSelected: null,
      spOrderByTypeItem: 0,
      spOrderByTypeItemsModels: [],
      lastFollowUpDate: new Date(),
      chFollowUpMonthlyChecked: false,
      chFollowUpDateChecked: false,
      chNeedFUChecked: false,
      chPotentialChecked: false,
      chCallLaterChecked: false,
      chCollectDocChecked: false,
      chOrderRejectedChecked: false,
      chNotDealChecked: false,
      chSentToSAChecked: false,
      chBackToAOChecked: false,
      chPolicyCreatedChecked: false,
      chNewChecked: true,
      chRenewableChecked: true
    };
  }

  componentDidMount() {
    var orderByItemModels = [
      {
        value: "Status",
        label: "Status"
      },
      {
        value: "Name",
        label: "Prospect Name"
      },
      {
        value: "EntryDate",
        label: "Input date"
      },
      {
        value: "LastFollowUpdate",
        label: "Follow up date"
      },
      {
        value: "NextFollowUpdate",
        label: "Next reminder"
      }
    ];

    var spOrderByTypeItemsModels = [
      {
        value: 0,
        label: "Ascending"
      },
      {
        value: 1,
        label: "Descending"
      }
    ];

    this.setState({ orderByItemModels, spOrderByTypeItemsModels });

    this.setDefault();
  }

  onChangeFunction = (value, name) => {
    // console.log(value);
    this.setState({
      [name]: value
    });
  };

  onChangeFunction2 = e => {
    // console.log(event.target.type);
    const value = e.target[e.target.type === "checkbox" ? "checked" : e.target.type === "radio" ? "checked" : "value"]
    const name = e.target.name;

    this.setState({
      [name]: value
    });

    if(name == "chFollowUpDateChecked"){
      this.setState({
        chFollowUpDateChecked: true,
        chFollowUpMonthlyChecked : false
      });
    }else if(name == "chFollowUpMonthlyChecked"){
      this.setState({
        chFollowUpDateChecked: false,
        chFollowUpMonthlyChecked : true
      });
    }
  };

  onOptionOrderByChange = itemModelsSelected => {
    this.setState({ itemModelsSelected });
  };

  onOptionspOrderByTypeChange = spOrderByTypeItem => {
    this.setState({ spOrderByTypeItem });
  };

  ExecuteSearch = () => {
    if(this.isValidated()){
      var datafilter ={
        itemModelsSelected : this.state.itemModelsSelected,
        spOrderByTypeItem: this.state.spOrderByTypeItem,
        chFollowUpMonthlyChecked: this.state.chFollowUpMonthlyChecked,
        chFollowUpDateChecked: this.state.chFollowUpDateChecked,
        chNeedFUChecked: this.state.chNeedFUChecked,
        chPotentialChecked: this.state.chPotentialChecked,
        chCallLaterChecked: this.state.chCallLaterChecked,
        chCollectDocChecked: this.state.chCollectDocChecked,
        chOrderRejectedChecked: this.state.chOrderRejectedChecked,
        chNotDealChecked: this.state.chNotDealChecked,
        chSentToSAChecked: this.state.chSentToSAChecked,
        chBackToAOChecked: this.state.chBackToAOChecked,
        chPolicyCreatedChecked: this.state.chPolicyCreatedChecked,
        FollowUpDate: this.state.lastFollowUpDate
      }

      secureStorage.setItem("datafilter", JSON.stringify(datafilter));
      this.props.onSubmitFilter(this.state);
      
    }
  }

  ExecuteReset = () => {
    this.setDefault();
  }

  setDefault = () => {
    this.setState({
      itemModelsSelected : "Status",
      spOrderByTypeItem : 1,
      chFollowUpMonthlyChecked : false,
      chFollowUpDateChecked : true,
      chNeedFUChecked : true,
      chPotentialChecked : true,
      chCallLaterChecked : true,
      chCollectDocChecked : true,
      chOrderRejectedChecked : true,
      chSentToSAChecked : true,
      chBackToAOChecked : true,
      chNotDealChecked : false,
      chPolicyCreatedChecked : false,
      lastFollowUpDate: new Date()
    });
}

  isValidated  = () =>{
    if(!this.state.chNeedFUChecked && !this.state.chPotentialChecked && !this.state.chCallLaterChecked &&
            !this.state.chCollectDocChecked && !this.state.chOrderRejectedChecked && !this.state.chNotDealChecked && !this.state.chSentToSAChecked &&
            !this.state.chBackToAOChecked && !this.state.chPolicyCreatedChecked){
        toast.dismiss();toast.warning(
          "❗ Please tick at least one status.",
          {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          }
        );
        return false;
    }

    return true;
  }


  render() {
    return (
      <BlockUi
        tag="div"
        blocking={this.state.isLoading}
        loader={<Loader active type="ball-spin-fade-loader" color="#02a17c" />}
        message="Loading, please wait"
      >
        <Modal
          styles={{
            modal: {
              maxWidth: "500px",
              width: "100%"
            },
            overlay: {
              zIndex: 1050
            }
          }}
          // showCloseIcon={false}
          open={this.props.showModal}
          onClose={() => this.props.onCloseModal()}
          center
        >
          <div className="modal-content panel panel-primary" style={{ margin: "-15px" }}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title pull-left" id="exampleModalLabel">
                <b>Sort &amp; Filter Prospect</b>
              </h4>
            </div>
            <div className="modal-body">
              <div class="row">
                <div class="col-xs-12">
                  <label>SORT PROSPECT</label>
                </div>
                <div class="col-xs-7">
                  <OtosalesSelect2
                    value={this.state.itemModelsSelected}
                    selectOption={this.state.itemModelsSelected}
                    onOptionsChange={this.onOptionOrderByChange}
                    options={this.state.orderByItemModels}
                  />
                </div>
                {
                  this.state.itemModelsSelected != "Status" &&
                  <div class="col-xs-5">
                  <OtosalesSelect2
                    value={this.state.spOrderByTypeItem}
                    selectOption={this.state.spOrderByTypeItem}
                    onOptionsChange={this.onOptionspOrderByTypeChange}
                    options={this.state.spOrderByTypeItemsModels}
                  />
                </div>
                }


                <br/>
                <br/>
                <br/>
                <br/>
                <div> 
                  <input
                    type="checkbox"
                    style={{ marginRight: "20px", marginLeft: "20px", width: "20px", height: "20px"}}
                    onChange={this.onChangeFunction2}
                    checked={this.state.chNewChecked}
                    name="chNewChecked"
                   />
                  <label>NEW</label>
                  <input
                    type="checkbox"
                    style={{ marginRight: "20px", marginLeft: "20px" , width: "20px", height: "20px"}}
                    onChange={this.onChangeFunction2}
                    checked={this.state.chRenewableChecked}
                    name="chRenewableChecked"
                   />
                  <label>RENEW</label>

                </div>

                <div class="col-xs-12">
                  <label>FILTER PROSPECT</label>
                </div>
                <div class="col-xs-12">
                  <div class="radio">
                    <label>
                      <input
                        type="radio"
                        checked={this.state.chFollowUpDateChecked}
                        name="chFollowUpDateChecked"
                        onChange={event => this.onChangeFunction2(event)}
                      />
                      FOLLOW UP DATE
                    </label>
                  </div>
                </div>
                <div class="col-xs-12">
                  <OtosalesDatePicker
                    minDate={new Date()}
                    className="form-control"
                    name="lastFollowUpDate"
                    selected={this.state.lastFollowUpDate}
                    dateFormat="dd/mm/yyyy"
                    onChange={this.onChangeFunction}
                  />
                </div>
                <div class="col-xs-12">
                  <div class="radio">
                    <label>
                      <input
                        type="radio"
                        checked={this.state.chFollowUpMonthlyChecked}
                        name="chFollowUpMonthlyChecked"
                        onChange={event => this.onChangeFunction2(event)}
                      />
                      PROSPECT ADDED THIS MONTH
                    </label>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="row" style={{fontSize:"9pt"}}>
                    <div class="col-xs-6">
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chNeedFUChecked"
                            checked={this.state.chNeedFUChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          NEED FU
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chCallLaterChecked"
                            checked={this.state.chCallLaterChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          CALL LATER
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chPotentialChecked"
                            checked={this.state.chPotentialChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          POTENTIAL
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chCollectDocChecked"
                            checked={this.state.chCollectDocChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          COLLECT DOC
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chOrderRejectedChecked"
                            checked={this.state.chOrderRejectedChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          ORDER REJECTED
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chSentToSAChecked"
                            checked={this.state.chSentToSAChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          SENT TO SA
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chBackToAOChecked"
                            checked={this.state.chBackToAOChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          BACK TO AO
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chPolicyCreatedChecked"
                            checked={this.state.chPolicyCreatedChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          POLICY CREATED
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="chNotDealChecked"
                            checked={this.state.chNotDealChecked}
                            onChange={event => this.onChangeFunction2(event)}
                          />
                          NOT DEAL
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div>
                <div className="col-xs-6">
                  <button
                    onClick={() => this.ExecuteReset()}
                    className="btn btn-warning form-control"
                  >
                    Reset
                  </button>
                </div>
                <div className="col-xs-6">
                  <button
                    onClick={() => this.ExecuteSearch()}
                    className="btn btn-info form-control"
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </BlockUi>
    );
  }
}

export default otosalesmodaltasklistfilter;
