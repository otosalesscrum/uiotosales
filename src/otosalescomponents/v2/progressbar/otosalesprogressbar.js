import React from "react";
import PropTypes from "prop-types";
import ProgressBar from 'react-bootstrap/ProgressBar';

function otosalesprogressbar(props) {
  let progressbar = props.dataitems.map((data, i) => {
    return (
      <ProgressBar
        key={i}
        now={(data.value && data.value > 0) ? data.value : 0}
        max={props.max && props.max > 100 ? 100 : props.max}
        label={data.label ? data.label : ""}
        variant={data.variant != undefined ? data.variant : "info"}
        animated={props.animated != undefined ? props.animated : true}
      />
    );
  });
  return (
    <div className="form-group">
      <div
        className="col-xs-12"
        style={{
          paddingLeft: "0px",
          paddingRight: "0px",
          marginBottom: "0px"
        }}
      >
        {progressbar}
      </div>
    </div>
  );
}

otosalesprogressbar.propTypes = {
    dataitems: PropTypes.array,
    max: PropTypes.number,
    animated: PropTypes.bool
};

export default otosalesprogressbar;
