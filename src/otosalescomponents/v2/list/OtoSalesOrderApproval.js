import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Util, Log } from "../../../otosalescomponents";
import { secureStorage } from "../../helpers/SecureWebStorage";

class OtoSalesOrderApproval extends Component {
  constructor(props) {
    super();
  }


  ApprovalComm(props) {
        const data = props.data;
        const itemInfo = data.ItemInfo;
        const index = props.index;
        return (
          <Link onClick={()=>props.onclickItem(data)} to="/orderapproval-detail" key={index}>
            <div className="col-md-12 col-xs-12">
              <div className="panel panel-default">
                
                <div className="panel-body">
                  <h5><strong>Approval Komisi</strong></h5>
                  <h4>
                    <strong>{itemInfo.Name}</strong>
                  </h4>
                  <div className="row">
                    <div className="col-xs-12">
                      <p>{"["+itemInfo.PayToCode + "] " + itemInfo.PayToName}</p>
                      <p>{(Util.isNullOrEmpty(itemInfo.SalesmanBankName)? "" : itemInfo.SalesmanBankName)} {Util.isNullOrEmpty(itemInfo.SalesmanAccountNo)? "" : "[" + itemInfo.SalesmanAccountNo + "]"}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Link>
        );
  }
  
  ApprovalLimit(props) {
    const data = props.data;
    const itemInfo = data.ItemInfo;
    const index = props.index;
    return (
          <Link onClick={()=>props.onclickItem(data)} to="/orderapproval-detail" key={index}>
            <div className="col-md-12 col-xs-12">
              <div className="panel panel-default">
                
                <div className="panel-body">
                  <h5><strong>Approval Limit</strong></h5>
                  <h4>
                    <strong>{itemInfo.Name.trim()}</strong>
                  </h4>
                  <div className="row">
                    <div className="col-xs-12">
                      <p>{"ORDER " + itemInfo.OrderType.trim()} - {Util.isNullOrEmpty(itemInfo.SalesmanDealerName)? "" : itemInfo.SalesmanDealerName.trim()}</p>
                      <p>Order No: {itemInfo.OrderNo.trim()}</p>
                      <p>{itemInfo.VehicleDescription}</p>
                      <p>TSI Rp {Util.formatMoney(itemInfo.TSI, 0, ",", ".")}</p>
                      <p>Premi Rp {Util.formatMoney(itemInfo.Premi, 0, ",", ".")}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Link>
      );
  }

  ApprovalAdjustment(props) {
    const data = props.data;
    const itemInfo = data.ItemInfo;
    const index = props.index;
    const AdjustmentItem = (props) => {
        const data = props.datas;
        if (props.datas != undefined && props.datas != null) {
          return (
              <ul style={{ listStyleType: "none", marginLeft: "0", paddingLeft: '0px' }}>
                {
                  (data.IsPSTB)? 
                  <li><input type="checkbox" checked="true" enabled="false"/>&nbsp;Policy Terbit Sebelum Bayar</li> : ""
                }
                {
                  (data.IsDelivery)? 
                  <li><input type="checkbox" checked="true" enabled="false"/>&nbsp;Penyesuaian Alamat Pengiriman Policy</li> : ""
                }
              </ul>
            );
        } else {
          return (<ul></ul>);
        }
    }
    return (
          <Link onClick={()=>props.onclickItem(data)} to="/orderapproval-detail" key={index}>
            <div className="col-md-12 col-xs-12">
              <div className="panel panel-default">
                
                <div className="panel-body">
                  <h5><strong>Approval Adjustment</strong></h5>
                  <h4>
                    <strong>{itemInfo.Name.trim()}</strong>
                  </h4>
                  <div className="row">
                    <div className="col-xs-12">
                      <p>{"ORDER " + itemInfo.OrderType.trim()} - {Util.isNullOrEmpty(itemInfo.SalesmanDealerName)? "" : itemInfo.SalesmanDealerName.trim()}</p>
                      <p>Marketing Name: {(Util.isNullOrEmpty(itemInfo.MarketingName)?"-": itemInfo.MarketingName.trim() )}</p>
                      <p>Branch: {(Util.isNullOrEmpty(itemInfo.BranchName)? "-": itemInfo.BranchName.trim())}</p>
                      <p>Order No: {(Util.isNullOrEmpty(itemInfo.OrderNo))?"":itemInfo.OrderNo.trim()}</p>
                      <p>{(Util.isNullOrEmpty(itemInfo.VehicleDescription))?"UNKNOWN":itemInfo.VehicleDescription.trim()}</p>
                      <p>TSI Rp {Util.formatMoney(itemInfo.TSI, 0, ",", ".")}</p>
                      <p>Premi Rp {Util.formatMoney(itemInfo.Premi, 0, ",", ".")}</p>
                      <AdjustmentItem 
                        datas={itemInfo}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Link>
      );
  }

  onclickItem(data){
    // this.props.onclickItem(data);
    secureStorage.setItem("OrderIdOA", data.ItemInfo.OrderId);
    secureStorage.setItem("OrderNoOA", data.ItemInfo.OrderNo);
    secureStorage.setItem("ApprovalType", data.ItemInfo.ApprovalType);
    secureStorage.setItem("IsSOOA", data.IsSo);
  }

  render() {
    var pg = this.props.rowscount || 0;
    // console.log("pg = " + pg);
    let texts = this.props.dataitems.map((data, index) => {
      Log.debugGroup("Data"+index+" :", data);
      if (index < pg) {
        switch(data.ItemInfo.ApprovalType.trim()) {
          case "KOMISI": 
          case "COMSAMD":
            return (
              <this.ApprovalComm 
                key={index}
                data={data}
                index={index}
                onclickItem={this.onclickItem}
                />
            );
          case "ADJUST": {
            return (
                <this.ApprovalAdjustment
                  key={index}
                  data={data}
                  index={index}
                  onclickItem={this.onclickItem}
                />
              );
          }
          case "NEXTLMT": {
            // return this.approvalLimit(data, index);
            return (
                <this.ApprovalLimit
                  key={index}
                  data={data}
                  index={index}
                  onclickItem={this.onclickItem}
                />
              );
          }
          default: {
            return "";
          }
        }
      }
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default OtoSalesOrderApproval;
