import React, { Component } from "react";
import { pdfjs, Document, Page } from "react-pdf";
import examplepdf from "./examplepdf.pdf";
import "react-pdf/dist/Page/AnnotationLayer.css";
import PropTypes from "prop-types";
import { Util } from "../..";

class ExamplePDFViewer extends Component {
  static propTypes = {
      file: PropTypes.any.isRequired,
  }
  constructor(props) {
    super(props);
    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  }
  

  state = {
    numPages: null,
    pageNumber: 1
  };

  onDocumentLoad = ({ numPages }) => {
    this.setState({ numPages });
  };

  onChangePages = (type = "NEXT") => {
    if (type == "NEXT") {
      if (this.state.pageNumber !== this.state.numPages) {
        this.setState((prevState, props) => ({
          pageNumber: prevState.pageNumber + 1
        }));
      }
    } else if (type == "PREV") {
      if (this.state.pageNumber !== 1) {
        this.setState((prevState, props) => ({
          pageNumber: prevState.pageNumber - 1
        }));
      }
    }
  };

  render() {
    const { pageNumber, numPages } = this.state;

    return (
      <div className="col-xs-12 panel-body-list">
        <center>
          <Document
            className="col-xs-12 panel-body-list"
            file={this.props.file || examplepdf}
            onLoadSuccess={this.onDocumentLoad}
            onLoadError={console.error}
          >
            <Page pageNumber={pageNumber} />
          </Document>
          <div className="row text-center">
            <a
              className="fa fa-arrow-circle-o-left fa-2x"
              onClick={() => this.onChangePages("PREV")}
              style={{ marginLeft: 5, marginRight: 5 }}
            />
            <a
              className="fa fa-arrow-circle-o-right fa-2x"
              onClick={() => this.onChangePages("NEXT")}
              style={{ marginLeft: 5, marginRight: 5 }}
            />
            <p>
              Page {pageNumber} of {numPages}
            </p>
          </div>
        </center>
      </div>
    );
  }
}

export default ExamplePDFViewer;
