import React, { Component } from "react";
import PropTypes from "prop-types";

class otosalesradiobutton extends Component {
  constructor(props) {
    super(props);
  }

  onOptionsChange = (value, name) => {
    this.props.onOptionsChange(value, name);
  };

  render() {
    let dataItems = this.props.options || [];

    let stylingClass =
      this.props.error || false ? "input-group has-error" : "input-group";

    let className = this.props.className || "";

    className += " " + stylingClass;

    let isDisabled = this.props.isDisabled || false;

    dataItems = dataItems.map((data, i) => {
      return (
        <React.Fragment key={"radioButton"+i}>
          <label
            className="radio-inline"
            key={`radiobtn-${this.props.name + "-" + i}`}
            onChange={() => {}}
            onClick={() => this.onOptionsChange(data.value, this.props.name)}
            style={{ marginLeft: 0, marginRight: 20 }}
          >
            <input
              type="radio"
              name={this.props.name}
              checked={data.value == this.props.value ? true : false}
              id={`radiobtninput-${this.props.name + "-" + data.value}`}
              disabled={isDisabled}
              onChange={() => {}}
            />
            {data.label}
          </label>
        </React.Fragment>
      );
    });
    return <div className={className}>{dataItems}</div>;
  }
}

otosalesradiobutton.propTypes = {
  className: PropTypes.string,
  options: PropTypes.array.isRequired,
  value: PropTypes.string,
  isDisabled: PropTypes.bool,
  onOptionsChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  error: PropTypes.bool,
};

export default otosalesradiobutton;
