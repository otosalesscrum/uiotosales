import React, { Component } from "react";
import PropTypes from "prop-types";
import OtosalesSelectModal from "./otosalesselectmodal";
import { Util, Log } from "../..";

class otosalesselectfullview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSelect: false,
      search: null
    };
  }

  onShowModal = () => {
    this.setState({ isShowSelect: true, search: null });
  };

  onCloseModal = () => {
    this.setState({ isShowSelect: false });
  };

  onChangeFunction = event => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState(
      {
        [name]: value
      },
      () => {
        if(name == "search"){
          if(!Util.isNullOrEmpty(this.props.onSearchFunction)){
            this.props.onSearchFunction(value);
          }
        }
      }
    );
  };

  onOptionsChange = value => {
    this.props.onOptionsChange(value, this.props.name);
    this.setState({ isShowSelect: false });
  };

  render() {
    let querySearch = (this.state.search+"").trim().split(" ");
    // Log.debugGroup("QUERY SEARCH", querySearch);
    let dataitems =
      this.state.search == null
        ? this.props.options
        : this.state.search == ""
        ? this.props.options
        : this.props.options.filter(option =>
            // option.label
            //   .toLowerCase()
            //   .includes(this.state.search.toLowerCase() + "")
            Util.sentenceContainsArrayString(option.label, querySearch)
          );

    // Log.debugGroup("dataitems", dataitems)

    let stylingClass =
      this.props.error || false ? "input-group has-error" : "input-group";

    let value =
      this.props.value == "" || this.props.value == null
        ? ""
        : this.props.options.filter(option => option.value == this.props.value)
            .length > 0
        ? this.props.options.filter(
            option => option.value == this.props.value
          )[0].label
        : "";
    return (
      <div>
        <div
          className={stylingClass}
          onClick={() => {
            if (!this.props.isDisabled) {
              if(!this.props.isDisabled){
                if(!Util.isNullOrEmpty(this.props.onClick)){
                  this.props.onClick(() => {
                    this.onShowModal()
                  });
                }else{
                  this.onShowModal()
                }
              }
            }
          }}
        >
          <input
            disabled={this.props.isDisabled || false}
            onChange={() => {}}
            type="text"
            style={{ marginBottom: "0px" }}
            value={value}
            className="form-control "
            placeholder={this.props.placeholder || "Select ..."}
          />
        </div>
        <OtosalesSelectModal
        {...this.props}
          state={this.state}
          placeholder={this.props.placeholder}
          onCloseModal={this.onCloseModal}
          onChangeFunction={this.onChangeFunction}
          value = {this.props.value}
          onOptionsChange = {this.onOptionsChange}
          dataitems={dataitems || null}
          rowscount={dataitems.length || 0}
        />
      </div>
    );
  }
}

otosalesselectfullview.propTypes = {
  className: PropTypes.string,
  options: PropTypes.array,
  selectOption: PropTypes.string,
  value: PropTypes.string,
  isDisabled: PropTypes.bool,
  onOptionsChange: PropTypes.func,
  onClick: PropTypes.func,
  error: PropTypes.bool,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onSearchFunction: PropTypes.func
};

export default otosalesselectfullview;
