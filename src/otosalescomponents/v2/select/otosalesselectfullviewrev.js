import React, { Component } from "react";
import PropTypes from "prop-types";
import Modal from "react-responsive-modal";
import OtosalesSelectOption from "./otosalesselectoptionrev";
import OtosalesLoading from "./../loading/otosalesloading";
import { Util } from "../..";

class otosalesselectfullview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSelect: false,
      search: null
    };
  }

  onShowModal = () => {
    this.setState({ isShowSelect: !this.state.isShowSelect, search: null });
  };

  onCloseModal = () => {
    this.setState({ isShowSelect: false });
  };

  onChangeFunction = event => {
    this.setState(
      {
        [event.target.name]: event.target.value
      },
      () => {}
    );
  };

  onOptionsChange = (value, name) => {
      this.props.onOptionsChange(value, name);
      this.setState({isShowSelect: false});
  }

  render() {
    let dataitems =
      this.state.search == null
        ? this.props.options
        : this.state.search.trim() == ""
        ? this.props.options
        : this.props.options.filter(option =>
            (option.label+"").toLowerCase().includes((this.state.search + "").toLowerCase())
          );
    let stylingClass =
      this.props.error || false ? "input-group has-error" : "input-group";

    let value =this.props.value == "" || this.props.value == null ? "" : (
      this.props.options.filter(option => option.value == this.props.value)
        .length > 0
        ? this.props.options.filter(
            option => option.value == this.props.value
          )[0].label
        : "");
    return (
      <div>
        <div className={stylingClass} onClick={() => {
            if(!this.props.isDisabled){
              if(!Util.isNullOrEmpty(this.props.onClick)){
                this.props.onClick(() => {
                  this.onShowModal()
                });
              }else{
                this.onShowModal()
              }
            }
          }}>
          <input
            disabled={this.props.isDisabled || false}
            onChange={() => {}}
            type="text"
            style={{ marginBottom: "0px" }}
            value={value}
            className="form-control "
            placeholder={this.props.placeholder || "Select ..."}
          />
        </div>
        <Modal
          styles={{
            modal: {
              width: "100%",
              maxWidth: "500px",
            //   maxHeight: "80%"
            },
            overlay: {
              zIndex: 1050
            }
          }}
          showCloseIcon={false}
          open={this.state.isShowSelect}
          onClose={() => this.onCloseModal()}
          center
        >
          <div
            className="modal-content panel panel-primary"
            style={{ margin: "-15px" }}
          >
            <OtosalesLoading isLoading={this.props.isLoading || false}>
              <div
                className="modal-header panel-heading"
                style={{ overflow: "hidden", display:"flex" }}
              >
                <i className="fa fa-search" style={{margin:"auto", marginRight: "10px"}}/>
                <label htmlFor="userlog" className="inp" style={{maxWidth:"100%"}}>
                  <input
                    type="text"
                    id="userlog"
                    autoFocus={true}
                    placeholder={this.props.placeholder || "Select ..."}
                    onChange={event => this.onChangeFunction(event)}
                    name="search"
                    autoComplete="off"
                    value={this.state.search || ""}
                  />
                  <span className="label" />
                  <span className="border" />
                </label>
              </div>
              <div
              className="modal-body panel-body-list"
              style={{ overflowY: "auto", maxHeight: "calc(80vh - 20vh)" }}
            >
              <OtosalesSelectOption
              {...this.props}
                value = {this.props.value}
                onOptionsChange = {this.onOptionsChange}
                dataitems={dataitems || null}
                rowscount={dataitems.length || 0}
              />
            </div>
            </OtosalesLoading>
          </div>
        </Modal>
      </div>
    );
  }
}

otosalesselectfullview.propTypes = {
  className: PropTypes.string,
  options: PropTypes.array,
  selectOption: PropTypes.string,
  value: PropTypes.string,
  isDisabled: PropTypes.bool,
  onOptionsChange: PropTypes.func,
  onClick: PropTypes.func,
  error: PropTypes.bool,
  placeholder: PropTypes.string
};

export default otosalesselectfullview;
