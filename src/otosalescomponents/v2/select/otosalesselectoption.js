import React, { Component } from "react";
import { AutoSizer, CellMeasurerCache } from "react-virtualized";
import { VariableSizeList } from "react-window";
import { isNullOrEmpty } from "../../helpers/Util";
import { Util } from "../..";

class otosalesselectoption extends Component {
  constructor(props) {
    super(props);

    this.cache = new CellMeasurerCache({
      fixedWidth: true,
      minHeight: this.props.minHeight || 10
    });
  }

  renderRow = ({ index, style }) => {
    // console.log(style);

    style = {
      ...style
    };

    let styling = {
      paddingTop: "5px",
      paddingBottom: "5px"
    };
    if (this.props.dataitems[index].value == this.props.value) {
      styling = {
        ...styling,
        background: "#18516c",
        color: "#FFF"
      };
    }

    return (
      <div style={style}>
        <div
          onClick={() =>
            this.props.onOptionsChange(this.props.dataitems[index].value)
          }
          className="form-group separatorBottom paddingside"
          style={styling}
          key={index}
        >
          <div
            className="row panel-body-list"
            style={{ marginLeft: "0px", marginRight: "0px" }}
          >
            <div className="col-xs-12 panel-body-list">
              <span className="smalltext" style={{ fontSize: "10pt" }}>
                {this.props.dataitems[index].label}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    var pg = this.props.rowscount || 0;

    if(this.props.onSearchFunction && Util.isNullOrEmpty(this.props.state.search) ){
      return (
        <div className="col-xs-12 panel-body-list" name={this.props.name}>
          <div className="list-group" style={{ padding: "0" }}>
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list text-center">
                Silahkan ketik terlebih dahulu
              </div>
            </div>
          </div>
        </div>
      );
    }

    if (pg == 0) {
      return (
        <div className="col-xs-12 panel-body-list" name={this.props.name}>
          <div className="list-group" style={{ padding: "0" }}>
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list text-center">
                No Data
              </div>
            </div>
          </div>
        </div>
      );
    }

    let Autolah = ({ width, height }) => {
      return (
        <VariableSizeList
          height={(height * 3.3) / 6}
          width={width}
          itemCount={this.props.rowscount}
          itemSize={this.cache.rowHeight}
        >
          {this.renderRow}
        </VariableSizeList>
      );
    };

    return (
      <div
        className="col-xs-12 panel-body-list"
        name={this.props.name}
        style={{ height: "inherit" }}
      >
        <div className="list-group" style={{ padding: "0", height: "inherit" }}>
          {/* {texts} */}
          <AutoSizer>{Autolah}</AutoSizer>
        </div>
      </div>
    );
  }
}

export default otosalesselectoption;
