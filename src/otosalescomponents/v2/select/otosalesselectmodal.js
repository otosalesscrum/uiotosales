import React, { Component } from "react";
import Modal from "react-responsive-modal";
import OtosalesSelectOption from "./otosalesselectoption";
import OtosalesLoading from "./../loading/otosalesloading";

export default class otosalesselectmodal extends Component {
  constructor(props) {
    super(props);
  }

  handleScroll = e => {
    let element = e.target;
    console.log(element.scrollTop);
    console.log(element.scrollHeight - element.clientHeight);
  };

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "100%",
            maxWidth: "500px"
            //   maxHeight: "80%"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.props.state.isShowSelect}
        onClose={() => this.props.onCloseModal()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <OtosalesLoading isLoading={this.props.isLoading || false}>
            <div
              className="modal-header panel-heading"
              style={{ overflow: "hidden", display: "flex" }}
            >
              <i
                className="fa fa-search"
                style={{ margin: "auto", marginRight: "10px" }}
              />
              <label
                htmlFor="userlog"
                className="inp"
                style={{ maxWidth: "100%" }}
              >
                <input
                  type="text"
                  id="userlog"
                  autoFocus={true}
                  placeholder={"Ketik "+(this.props.placeholder || "Select ...")}
                  onChange={event => this.props.onChangeFunction(event)}
                  name="search"
                  autoComplete="off"
                  value={this.props.state.search || ""}
                />
                <span className="label" />
                <span className="border" />
              </label>
            </div>
            <div
            className="modal-body panel-body-list"
            style={{ overflowY: "hidden", overflowX:"hidden", maxHeight: "calc(80vh - 20vh)", height:"100vh" }}
          >
            <OtosalesSelectOption
            {...this.props}
              value={this.props.value}
              onOptionsChange={this.props.onOptionsChange}
              dataitems={this.props.dataitems || null}
              rowscount={
                (this.props.dataitems && this.props.dataitems.length) || 0
              }
            />
          </div>
          </OtosalesLoading>
        </div>
      </Modal>
    );
  }
}
