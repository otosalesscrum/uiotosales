import React, { Component } from "react";
import Dropzone from "react-dropzone";
import {
  PlaceHolderImage,
  addImage,
  pdftype,
  notsupportpreview
} from "../../../assets";
import Lightbox from "react-image-lightbox";
import Modal from "react-responsive-modal";
import PDFPreview from "./../pdfviewer";
import * as Util from "./../../helpers/Util";

class otosalesdropzonefile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      showModal: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state != nextState ||
      this.props.src != nextProps.src ||
      this.props.state != nextProps.state
    );
  }

  handleClick = e => {
    this.inputElement.click();
    this.setState({ showModal: false });
  };

  fileUploaded = () => {
    let temp = addImage;

    if (this.props.src.length > 0) {
      temp = URL.createObjectURL(this.props.src[0]);
      // !Util.isNullOrEmpty(this.props.state) && (
      // (this.props.state[`type${this.props.name}`]+"").includes("pdf")
      if (!Util.isNullOrEmpty(this.props.state)) {
        if (
          !Util.stringArrayContains(
            this.props.state[`type${this.props.name}`] + "",
            ["pdf", "image"]
          )
        ) {
          temp = notsupportpreview;
        }
      }
    }
    return temp;
  };

  thumbnailFile = (fileImg) => {
    let temp = fileImg;

    if(!Util.isNullOrEmpty(this.props.state)){
      if((this.props.state[`type${this.props.name}`] + "").includes(
        "pdf"
      )){
        temp = pdftype;
      }
    }

    return temp;
  }

  render() {
    const Isimgstatusnotnull = this.props.src.length > 0 ? true : false;

    const fileSrc = this.fileUploaded();

    const ModalList = () => (
      <Modal
        styles={{
          modal: {
            width: "80%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.state.showModal}
        onClose={() => this.setState({ showModal: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-body">
            <div
              onClick={this.handleClick}
              className="form-group separatorBottom paddingside"
              style={{
                paddingTop: "5px",
                paddingBottom: "5px"
              }}
            >
              <div
                className="row panel-body-list"
                style={{ marginLeft: "0px", marginRight: "0px" }}
              >
                <div className="col-xs-12 panel-body-list">
                  <span
                    className="smalltext labelspanbold"
                    style={{ fontSize: "initial" }}
                  >
                    <i
                      className="fa fa-camera"
                      style={{ marginRight: "10px" }}
                    />{" "}
                    Choose
                  </span>
                </div>
              </div>
            </div>
            {Isimgstatusnotnull && (
              <div
                onClick={() =>
                  this.setState({ isOpen: true, showModal: false })
                }
                className="form-group separatorBottom paddingside"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <div
                  className="row panel-body-list"
                  style={{ marginLeft: "0px", marginRight: "0px" }}
                >
                  <div className="col-xs-12 panel-body-list">
                    <span
                      className="smalltext labelspanbold"
                      style={{ fontSize: "initial" }}
                    >
                      <i
                        className="fa fa-eye"
                        style={{ marginRight: "11px" }}
                      />{" "}
                      Preview
                    </span>
                  </div>
                </div>
              </div>
            )}
            {Isimgstatusnotnull && (
              <div
                onClick={() => {
                  this.props.handleDeleteImage(this.props.name);
                  this.setState({ showModal: false });
                }}
                className="form-group paddingside"
                style={{
                  paddingTop: "5px",
                  marginBottom: "0px"
                }}
              >
                <div
                  className="row panel-body-list"
                  style={{ marginLeft: "0px", marginRight: "0px" }}
                >
                  <div className="col-xs-12 panel-body-list">
                    <span
                      className="smalltext labelspanbold"
                      style={{ fontSize: "initial" }}
                    >
                      <i
                        className="fa fa-trash"
                        style={{ marginRight: "15px" }}
                      />{" "}
                      Delete
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );

    const ModalPDF = () => (
      <Modal
        styles={{
          modal: {
            width: "80%"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={true}
        open={this.state.isOpen}
        onClose={() => this.setState({ isOpen: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Preview</b>
            </h4>
          </div>
          <div className="modal-body">
            <PDFPreview file={fileSrc} />
          </div>
        </div>
      </Modal>
    );

    return (
      <div>
        <ModalList />
        {this.state.isOpen &&
          !Util.isNullOrEmpty(this.props.state) &&
          ((this.props.state[`type${this.props.name}`] + "").includes("pdf") ? (
            <ModalPDF />
          ) : (
            <Lightbox
              reactModalStyle={{
                overlay: {
                  zIndex: 1050
                }
              }}
              mainSrc={fileSrc}
              onCloseRequest={() => this.setState({ isOpen: false })}
            />
          ))}
        <Dropzone
          onDrop={(acceptedFiles, rejectedFiles) =>
            this.props.handleUploadImage(
              this.props.name,
              acceptedFiles,
              rejectedFiles
            )
          }
          multiple={false}
          accept="image/png, image/gif, image/jpeg, image/jpg, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx"
        >
          {({ getRootProps, getInputProps, isDragActive }) => (
            <section>
              <div {...getRootProps()} className="border-rounded-dropzone" style={isDragActive ? {backgroundColor: "#ccc"} : {}}>
                <input
                  {...getInputProps()}
                  ref={input => (this.inputElement = input)}
                />
                {isDragActive ? (
                  <p>Drop the files here ...</p>
                ) : (
                  // <p style={{fontSize:"8pt"}}>Drag 'n' drop a file here, or click to select files</p>
                  null
                )}
                
              <div
                className="squareimg"
                //   onClick={() => this.setState({ isOpen: true })}
                onClick={() => this.setState({ showModal: true })}
                style={{ margin: "5px 0px 5px 0px" }}
              >
                {/* {!Util.isNullOrEmpty(this.props.state) ? (
                  !(this.props.state[`type${this.props.name}`] + "").includes(
                    "pdf"
                  ) ? (
                    <img
                      className="contentimg"
                      style={{ width: "100%", imageOrientation: "from-image" }}
                      src={img}
                    />
                  ) : (
                    <img
                      className="contentimg"
                      style={{ width: "100%", imageOrientation: "from-image" }}
                      src={pdftype}
                    />
                  )
                ) : (
                  <img
                    className="contentimg"
                    style={{ width: "100%", imageOrientation: "from-image" }}
                    src={img}
                  />
                )} */}
                <img
                    className="contentimg"
                    style={{ width: "100%", imageOrientation: "from-image" }}
                    src={this.thumbnailFile(fileSrc)}
                  />
              </div>
              </div>
            </section>
          )}
        </Dropzone>
      </div>
    );
  }
}

export default otosalesdropzonefile;
