import React, { Component } from "react";


class HeaderSubContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: this.props.titleText
    }
  }
  render() {
    return (
        <div className="col-xs-12" style={{ backgroundColor: '#EAEAEA', fontSize: '18px', padding: '5px' }}>
          { this.state.titleText }
        </div>
    );
  }
}

export default HeaderSubContent;