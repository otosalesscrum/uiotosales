import React, { Component } from "react";
import { Container, Button, Link } from "react-floating-action-button";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { withRouter } from "react-router-dom";
import {
  NeedFu,
  CallLater,
  Potential,
  CollectDoc,
  SendToSA,
  NotDeal,
  PolicyCreated,
  BackToAO
} from "../../../assets";
import PropTypes from 'prop-types';

class index extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
    className: PropTypes.string
}

  constructor(props) {
    super(props);

    this.state={
      isOpenDrawer: false
    }
  }


  toggleDrawer = (open) => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    this.setState({ isOpenDrawer: open });
  };


  render() {
    return (
      <div>
        <SwipeableDrawer
        anchor="bottom"
        open={this.state.isOpenDrawer}
        onClose={this.toggleDrawer(false)}
        onOpen={this.toggleDrawer(true)}
      >
        <div className={this.props.className || "col-xs-12"}>
          {this.props.children}
        </div>
      </SwipeableDrawer>
      <Container>
        <Button styles={{ backgroundColor: "transparent" }}>
          <img src={NeedFu} style={{ width: "100%" }} onClick={this.toggleDrawer(true)}/>
        </Button>
      </Container>
      </div>
    );
  }
}



export default withRouter(index);
