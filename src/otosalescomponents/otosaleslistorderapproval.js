import React, { Component } from "react";
import { Link } from "react-router-dom";

class otosaleslistorderapproval extends Component {
  constructor(props) {
    super();
  }

  onclickItem = (data) => {
    // console.log(data);
    this.props.onclickItem(data);
  }

  render() {
    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((data, index) => {
      if (index < pg) {
        return (
          <Link onClick={()=>this.onclickItem(data)} to="/orderapproval-detail" key={index}>
          <div className="col-md-12 col-xs-12">
            <div className="panel panel-default">
              
              <div className="panel-body">
                <h4>
                  <strong>{data.name}</strong>
                </h4>
                <div className="row">
                  <div className="col-xs-12">
                    <p>{(data.paytocode !="" && data.paytoname !=null) ? "["+data.paytocode+"] "+data.paytoname: ""}</p>
                    <p>{(data.accountinfobankname != "" && data.accountinfoaccountno != "") ? data.accountinfobankname+" ["+data.accountinfoaccountno+"]": ""}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </Link>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default otosaleslistorderapproval;
