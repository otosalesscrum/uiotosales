import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Tab from './Tab';

class Tabs extends Component {
    static propTypes = {
        children: PropTypes.instanceOf(Array).isRequired,
    }

    constructor(props) {
        super(props);
    }

    onClickTabItem = (tab) => {
        // console.log(tab);
        this.props.onClickTabs(tab);
    }

    render() {
        // console.log(this.props.activetab);
        const {
            onClickTabItem,
            props: {
                children,
            }
        } = this;

        let styleCustomWidth = {};
        if(children.length == 4){
            styleCustomWidth = {
                width: "25%",
                minWidth: "0px",
            }
        }else if(children.length == 3){
            styleCustomWidth = {
                width: "33.34%",
                minWidth: "0px",
            }
        }

        return (
            <div className="tabbable">
                <ul className="nav nav-tabs text-center">
                    {children.map((child) => {
                        const { label, disabled } = child.props;
                        let disabledtemp = false;
                        if(disabled !== undefined){
                            disabledtemp = disabled;
                        }
                        return (
                          <Tab
                            activeTab={
                              this.props.activetab
                            }
                            styleCustomWidth = {styleCustomWidth}
                            key={label}
                            label={label}
                            onClick={onClickTabItem}
                            disabledtab={disabledtemp}
                          />
                        );
                    })}
                </ul>
                <div className="tab-content">
                    {children.map((child) => {
                        if (child.props.label !== this.props.activetab) return undefined;
                        return child.props.children;
                    })}
                </div>
            </div>
        );
    }
}

export default Tabs;