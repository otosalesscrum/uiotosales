import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "./otosalesdatepickerstyle.css";

class otosalestimepicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      isOpen: false
    };
  }

  toggleCalendar = e => {
    e && e.preventDefault();
    this.setState({ isOpen: !this.state.isOpen });
  };

  formatTimeFromDate = dates => {
    if(dates == null){
      return "No Data";
    }
    var hh = dates.getHours();
    if (hh < 10) {
      hh = "0" + hh;
    }

    var mm = dates.getMinutes();
    if (mm < 10) {
      mm = "0" + mm;
    }

    var ss = dates.getMinutes();

    return hh + ":" + mm;
  };

  render() {
    var className = this.props.className + "" || null;
    return (
      <div>
        <input
          type="text"
          className={className}
          value={this.formatTimeFromDate(this.props.selected) || ""}
          disabled={this.props.disabled || false}
          readOnly={this.props.readOnly || false}
          onClick={event => {
            if (
              !(this.props.readOnly || false) &&
              this.props.selected != null
            ) {
              this.toggleCalendar(event);
            }
          }}
        >
        </input>
        {this.state.isOpen && (
          <DatePicker
            {...this.props}
            className={className}
            selected={this.props.selected}
            showTimeSelect
            showTimeSelectOnly
            withPortal
            inline
            timeIntervals={10}
            onClickOutside = {() => this.toggleCalendar()}
            onChange={date => {
              this.props.onChange(date, this.props.name);
              this.toggleCalendar();
            }}
            dateFormat="HH:mm"
            timeCaption="Time"
            disabled={this.props.disabled || false}
            readOnly={this.props.readOnly || false}
          />
        )}
      </div>
    );
  }
}

export default otosalestimepicker;
