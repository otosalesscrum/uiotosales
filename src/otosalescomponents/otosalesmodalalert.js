import React from "react";
import PropTypes from "prop-types";
import Modal from "react-responsive-modal";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { OtosalesLoading } from "./../otosalescomponents";

export default function otosalesmodalalert(props) {
  return (
    <Modal
      styles={{
        modal: {
          maxWidth: "500px",
          width: "80%"
        },
        overlay: {
          zIndex: 1050
        }
      }}
      open={props.showModalInfo}
      onClose={() => props.onClose()}
      closeOnOverlayClick={false}
      showCloseIcon={false}
      center
    >
      <div
        className="modal-content panel panel-primary"
        style={{ margin: "-15px" }}
      >
        <OtosalesLoading isLoading={props.isLoading || false}>
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Information</b>
            </h4>
          </div>
          <div
            className="modal-body"
            style={{ fontWeight: "1000", textAlign: "center" }}
          >
            {props.message}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => props.onClose()}
                  className="btn btn-warning form-control"
                >
                  {props.btnno || "No"}
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => props.onSubmit()}
                  className="btn btn-danger form-control"
                >
                  {props.btnyes || "Yes"}
                </button>
              </div>
            </div>
          </div>
        </OtosalesLoading>
      </div>
    </Modal>
  );
}

otosalesmodalalert.propTypes = {
  showModalInfo: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  message: PropTypes.string,
  btnno: PropTypes.string,
  btnyes: PropTypes.string,
  isLoading: PropTypes.bool
};
