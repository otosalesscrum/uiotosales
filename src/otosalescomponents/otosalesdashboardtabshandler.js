import React, { Component } from "react";
import {  OtosalesSelect2,  OtosalesListDashboard } from "../otosalescomponents";
import {
  A2isTabs
} from "../components/a2is.js";
class otosalesdashboardtabshandler extends Component {
  constructor(props) {
    super(props);
  }
  
  setDate = () => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
      "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    const d = new Date();
    return monthNames[d.getMonth()] + " " + d.getFullYear();
  }
  render() {
   
    var pg = this.props.rowscount || 1;
    var dataexist=false;
    var datas = this.props.dataitems2.length>0?this.props.dataitems2:this.props.dataitems;
    let texts = datas.map((data, index) => {
      dataexist=data.Description==null?false:true
      if (index < pg) {
        return (
         <div label={(data.Description+"").toUpperCase().trim()}>
           <div className="panel-heading" style={{ background: "#eaeaea" }}>
                <div class="row">
                  <div class="col-md-4 col-xs-6" style={{ float: "left" }}>
                    <p style={{ textAlign: "left", color: "#606060" }}><b>NEW PROSPECT</b></p>
                  </div>
                  <div class="col-md-4 col-xs-6" style={{ float: "right" }}>
                    <p style={{ textAlign: "right", color: "#606060" }} id="textDate"><b> {this.setDate()}</b></p>
                  </div>
                </div>
              </div>
            <div className="">
              
            
              <div className="panel-body">
         <div class="row" style={{marginTop:'20px'}}>
            <div class="col-md-4 col-xs-6" style={{ float: "left" }} >
              <p>VIEW</p>
              <div className="input-group">
                <OtosalesSelect2
                  value={this.props.ViewCode}
                  selectOption={this.props.ViewCode}
                  onOptionsChange={this.props.onOptionViewChange}
                  options={this.props.ViewList}
                />
              </div>
            </div>
            <div class="col-md-4 col-xs-6" style={{ float: "right" }} >
              <p>PERIOD</p>
              <div className="input-group">
                <OtosalesSelect2
                  value={this.props.PeriodCode}
                  selectOption={this.props.PeriodCode}
                  onOptionsChange={this.props.onOptionPeriodChange}
                  options={this.props.PeriodList}
                />
              </div>
            </div>
          </div>
          <div class="row" style={{ marginTop: "20px" }}>
          {this.props.dataitems.length > 0 &&
                <OtosalesListDashboard
                  onClickListItems = {this.props.onClickListItems}
                  rowscount={this.props.dataitems[index].length}
                  viewFilter={this.props.ViewCode}
                  dataitems={this.props.dataitems[index]}//fuModel
                  dataitems2={this.props.dataitems2}//aabheads
                  Role={this.props.Role}
                />
              }
          </div>
           </div></div></div>
        );
      }
    }, this.pg);
    if(this.props.dataitems2.length>0&&dataexist){
      
    return (
      <A2isTabs>
     {texts}
      </A2isTabs>
    );
    }
    else{
    return (
      <div>
     {texts}
     </div>
    );
    }
  }
}

export default otosalesdashboardtabshandler;
