import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Util } from '.';

class Tab extends Component {
    static propTypes = {
        activeTab: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        disabledtab: PropTypes.bool.isRequired
    };

    onClick = () => {
        const { label, onClick } = this.props;
        onClick(label);
    }

    componentDidMount(){
        this.topFunction();
    }

    topFunction = () => {
        if(!Util.isNullOrEmpty(document.getElementsByClassName("content-wrapper"))){
            document.getElementsByClassName("content-wrapper")[0].scrollTop = 0;
        }
      }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.activeTab != this.props.activeTab){
            console.log("MELAKU !!!!!!!!!!!");
            this.topFunction();
        }
    }

    render() {
        const {
            onClick,
            props: {
                activeTab,
                label, 
                disabledtab,
                styleCustomWidth
            },
        } = this;

        let className = 'tab-list-item';

        let disabledstyle = (disabledtab
              ? {
                  ...styleCustomWidth,
                  pointerEvents: "none"
                }
              : {...styleCustomWidth});

        if (activeTab === label) {
            className += ' tab-list-active';
        }

        // console.log(styleCustomWidth);

        return (
            <li
                className={className}
                onClick={onClick}
                style={disabledstyle}
            >
                {label.toUpperCase()}
            </li>
        );
    }
}


export default Tab;