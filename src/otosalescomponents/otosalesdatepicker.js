import React, { Component } from "react";
import DatePicker from "react-datepicker";
import PropTypes from "prop-types";
import { Util } from "../otosalescomponents";
import "./otosalesdatepickerstyle.css";

class otosalesdatepicker extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    dateFormat: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  toggleCalendar = e => {
    e && e.preventDefault();
    this.setState({ isOpen: !this.state.isOpen });
  };

  formatDate = dates => {
    if (dates == null) {
      return "No Data";
    }
    var dd = dates.getDate();
    var mm = dates.getMonth(); //January is 0!

    var yyyy = dates.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }

    var mL = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    var mS = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ];
    return dd + " " + mS[mm] + " " + yyyy;
  };

  render() {
    var className = this.props.className + "" || null;
    return (
      <div>
        <input
          className={className}
          type="text"
          value={ (this.props.selected &&
            Util.formatDate(
              this.props.selected,
              this.props.dateFormat || "dd mmm yyyy"
            ) || "") || "No Data"
          }
          disabled={this.props.disabled || false}
          readOnly={this.props.readOnly || false}
          onChange={() => {}}
          onClick={event => {
            if (
              !(this.props.readOnly || false)
            ) {
              this.toggleCalendar(event);
            }
          }}
        />
        {this.state.isOpen && (
          <DatePicker
            {...this.props}
            withPortal
            className={className}
            onClickOutside={() => this.toggleCalendar()}
            selected={this.props.selected}
            onChange={date => {
              this.props.onChange(date, this.props.name);
              this.toggleCalendar();
            }}
            showYearDropdown
            dateFormatCalendar="MMMM"
            scrollableYearDropdown={true}
            yearDropdownItemNumber={80}
            dateFormat={this.props.dateFormat || "yyyy-MM-dd"}
            name={this.props.name}
            disabled={this.props.disabled || false}
            readOnly={this.props.readOnly || false}
            inline
          />
        )}
      </div>
    );
  }
}

export default otosalesdatepicker;
