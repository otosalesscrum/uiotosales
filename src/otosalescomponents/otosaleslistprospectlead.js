import React, { Component } from "react";

class otosaleslistprospectlead extends Component {
  constructor(props) {
    super();
  }

  onClick = (PolicyID) => {
    this.props.onClick(PolicyID);
  }

  render() {
    const props = this.props;
    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((data, index) => {
      const styleHotProspect = data.isHotProspect == 1 ? {color: "#D59F00"}:null;
      if (index < pg) {
        return (
          <div className="col-md-12 col-xs-12 panel-body-list" key={index}>
            <div className="panel panel-default">
              
              <div className="panel-body">
                <h4>
                  <strong style={styleHotProspect}>{data.customerName}</strong>
                </h4>
                <div className="row">
                  <div className="col-xs-7">
                    <p>{data.vehicleDescription}</p>
                    <p>PCT {data.followUpDateTime} </p>
                  </div>
                  <div className="col-xs-5">
                  <div className="pull-right">
                    <button onClick={()=>this.onClick(data.policyID)} className="btn btn-primary" type="button">
                      Get It
                    </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default otosaleslistprospectlead;
