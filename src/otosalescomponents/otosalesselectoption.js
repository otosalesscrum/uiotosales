import React, { Component } from "react";

class otosalesselectoption extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((text, index) => {
      if (index < pg) {
        let styling = {
          paddingTop: "5px",
          paddingBottom: "5px"
        }
        if(text.value == this.props.value){
          styling = {
            ...styling,
            background: "#18516c",
            color: "#FFF"
          }
        }

        return (
          <div onClick = {() => this.props.onOptionsChange(text.value)}
            className="form-group separatorBottom paddingside"
            style={styling}
            key = {index+1}
          >
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list">
                <span className="smalltext" style={{ fontSize: "10pt" }}>
                  {text.label}
                </span>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    if(pg == 0){
      return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        <div className="list-group" style={{ padding: "0" }}>
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list text-center">
              No Data
              </div>
              </div>
        </div>
      </div>
      );
    }

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        <div className="list-group" style={{ padding: "0" }}>
          {texts}
        </div>
      </div>
    );
  }
}

export default otosalesselectoption;
