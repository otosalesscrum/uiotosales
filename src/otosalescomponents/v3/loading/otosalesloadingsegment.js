import React from 'react';
import { Code } from 'react-content-loader';
import PropTypes from "prop-types";
 
const LoaderSegment = (props) => {
    return props.isLoading ? <Code /> : props.children;
};

LoaderSegment.propTypes = {
    isLoading: PropTypes.bool.isRequired 
  };

export default LoaderSegment;