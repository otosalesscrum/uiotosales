import React from 'react';
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import PropTypes from "prop-types";
import StringTranslation from "./../../helpers/stringTranslation";

export default function otosalesloading(props) {
    return (
        <BlockUi
            tag="div"
            className={props.className || ""}
            blocking={props.isLoading || false}
            loader={
              <Loader active type="ball-spin-fade-loader" color="#18516c" />
            }
            message={StringTranslation("IsLoadingPleaseWait")}
          >
              {props.children || ""}
        </BlockUi>
    )
}

otosalesloading.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    className: PropTypes.string
  };
  
