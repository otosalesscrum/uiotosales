import React, { Component } from "react";
import { AutoSizer, CellMeasurerCache } from "react-virtualized";
import { VariableSizeList } from "react-window";
import { isNullOrEmpty } from "../../helpers/Util";
import { Util } from "../..";

class otosalesselectoption extends Component {
  constructor(props) {
    super(props);

    this.cache = new CellMeasurerCache({
      fixedWidth: true,
      minHeight: this.props.minHeight || 10
    });
  }

  renderRow = ({ index, style }) => {
    // console.log(style);

    style = {
      ...style
    };

    let styling = {
      paddingTop: "5px",
      paddingBottom: "5px"
    };
    if (this.props.dataitems[index].value == this.props.value) {
      styling = {
        ...styling,
        background: "#18516c",
        color: "#FFF"
      };
    }

    return (
      <div style={style}>
        <div
          onClick={() =>
            this.props.onOptionsChange(this.props.dataitems[index].value)
          }
          className="form-group separatorBottom paddingside"
          style={styling}
          key={index}
        >
          <div
            className="row panel-body-list"
            style={{ marginLeft: "0px", marginRight: "0px" }}
          >
            <div className="col-xs-12 panel-body-list">
              <span className="smalltext" style={{ fontSize: "10pt" }}>
                {this.props.dataitems[index].label}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.props != nextProps;
  }

  render() {
    var pg = this.props.rowscount || 0;

    let isLogicCountLimit = pg <= 20;

    if(!this.props.showButtonSearch && this.props.onSearchFunction && Util.isNullOrEmpty(this.props.state.search) ){
      return (
        <div className="col-xs-12 panel-body-list" name={this.props.name}>
          <div className="list-group" style={{ padding: "0" }}>
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list text-center">
                Silahkan ketik terlebih dahulu
              </div>
            </div>
          </div>
        </div>
      );
    }

    if (pg == 0) {
      return (
        <div className="col-xs-12 panel-body-list" name={this.props.name}>
          <div className="list-group" style={{ padding: "0" }}>
            <div
              className="row panel-body-list"
              style={{ marginLeft: "0px", marginRight: "0px" }}
            >
              <div className="col-xs-12 panel-body-list text-center">
                No Data
              </div>
            </div>
          </div>
        </div>
      );
    }

    let texts = null;
    let stylingTexts = null;

    if(isLogicCountLimit){
      stylingTexts = {overflowY: "auto"};
      texts = this.props.dataitems.map((text, index) => {
        if (index < pg) {
          let styling = {
            paddingTop: "5px",
            paddingBottom: "5px"
          }
          if(text.value == this.props.value){
            styling = {
              ...styling,
              background: "#18516c",
              color: "#FFF"
            }
          }
  
          return (
            <div onClick = {() => this.props.onOptionsChange(text.value, this.props.name)}
              className="form-group separatorBottom paddingside"
              style={styling}
              key = {index+1}
            >
              <div
                className="row panel-body-list"
                style={{ marginLeft: "0px", marginRight: "0px" }}
              >
                <div className="col-xs-12 panel-body-list">
                  <span className="smalltext" style={{ fontSize: "10pt" }}>
                    {text.label}
                  </span>
                </div>
              </div>
            </div>
          );
        }
      }, pg);
    }

    let Autolah = ({ width, height }) => {
      return (
        <VariableSizeList
          height={(height * 3.3) / 6}
          width={width+(Math.floor(Math.random() * 100) / 100)}
          itemCount={this.props.rowscount}
          itemSize={this.cache.rowHeight}
        >
          {this.renderRow}
        </VariableSizeList>
      );
    };

    return (
      <div
        className="col-xs-12 panel-body-list"
        name={this.props.name}
        style={{ height: "inherit" }}
      >
        <div className="list-group" style={{ padding: "0", height: "inherit", ...stylingTexts }}>
          {isLogicCountLimit ? <div style={{marginBottom:"50vh"}}>{texts}</div> : <AutoSizer>{Autolah}</AutoSizer>}
          {/* <AutoSizer>{Autolah}</AutoSizer> */}
        </div>
      </div>
    );
  }
}

export default otosalesselectoption;
