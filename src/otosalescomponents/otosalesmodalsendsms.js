import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import Modal from "react-responsive-modal";

export default class otosalesmodalsendsms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: this.props.state.Phone1
    };
  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  componentDidUpdate(prevProps){
    if(this.props.state.Phone1 !== prevProps.state.Phone1){
      this.state.phonenumber = this.props.state.Phone1
    }
  }

  render() {
    return (
      <Modal
        styles={{
          modal: {
            width: "80%",
            maxWidth: "500px"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.props.state.showModalSendSMS || false}
        onClose={() => this.props.onCloseModalSendSMS()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-15px" }}
        >
          <div className="modal-header panel-heading">
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Send SMS</b>
            </h4>
            <div className="dropdown pull-right">
              <i
                className="fa fa-send fa-lg"
                onClick={() => {
                  if((this.props.state.Phone1.length < 9 ||
                    this.props.state.Phone1.length > 13)){
                      this.props.state.isErrPhone1 = true; 
                      toast.dismiss();
                      toast.warning("Panjang nomor telfon tidak sesuai!", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                      });
                }else if (/^[0-9-]{2,}[0-9]$/.test(this.props.state.Phone1)) {
                    this.props.sendQuotationSMS(
                      this.props.state.selectedQuotation,
                      this.props.state.Phone1
                    );
                    this.props.onCloseModalSendSMS();
                  } else {
                    toast.dismiss();
                    toast.warning("Nomor telefon tidak sesuai!", {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true
                    });
                  }
                }}
              />
            </div>
          </div>
          <div className="modal-body">
            <textarea
              type="number"
              name="phonenumber"
              value={this.state.phonenumber}
              onChange={event => this.onChangeFunction(event)}
              placeholder="enter phone number"
              className="form-control"
              style={{resize:"none"}}
            />
          </div>
          <div className="modal-footer" />
        </div>
      </Modal>
    );
  }
}
