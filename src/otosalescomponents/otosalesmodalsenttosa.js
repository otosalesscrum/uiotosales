import React, { Component } from "react";
import OtosalesQuotationHistory from "./otosalesquotationhistory";
import Modal from "react-responsive-modal";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import Otosalesmodalsenttosamakesure from "./otosalesmodalsenttosamakesure";
import Otosalesmodalsenttosaalreadysent from "./otosalesmodalsenttosaalreadysent";
import { toast, ToastContainer } from "react-toastify";

class otosalesmodalsenttosa extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alreadySent: false,
      FollowUpNo: props.FollowUpNo || null,
      selectedQuotation: props.selectedQuotation,
      quotationno: null,
      dataquotationhistory: props.dataquotationhistory,
      showModalMakeSure: false,
      showModalAlreadySent: false
    };
  }

  componentWillUnmount(){
    clearInterval(this.checkshowmodal);
  }

  componentDidMount() {
    this.checkshowmodal = setInterval(() => {
      if (this.props.showModal) {
        console.log("show modal sent to sa");
        if (
          this.props.selectedQuotation != null &&
          this.props.dataquotationhistory.length > 0
        ) {
          console.log("masuk if");
          this.setState(
            {
              selectedQuotation: this.props.selectedQuotation,
              dataquotationhistory: this.props.dataquotationhistory
            },
            () => {
              if (this.state.dataquotationhistory.length > 0) {
                for (
                  let i = 0;
                  i < this.state.dataquotationhistory.length;
                  i++
                ) {
                  const item = this.state.dataquotationhistory[i];
                  if (item.ApplyF) {
                    console.log("ApplyF");
                    this.setState({
                      selectedQuotation: item.OrderNo,
                      FollowUpNo: item.FollowUpNo,
                      quotationno: item.QuotationNo
                    });
                  }
                  if (item.SendF) {
                    console.log("SendF");
                    this.setState({ alreadySent: true });
                  }
                }
              }
            }
          );
        }
      } else {
        // console.log("not show modal sent to sa");
      }
    }, 100);
  }

  onClickQuotationHistory = (OrderNo, FollowUpNo, QuotationNo) => {
    console.log(OrderNo);
    console.log(FollowUpNo);

    this.setState({
      selectedQuotation: OrderNo,
      FollowUpNo: FollowUpNo
    });
  };

  clickSend = () => {
    console.log("masuk otosalesmodalsenttosa");
    this.setState({ showModalMakeSure: true });
  };

  yesSendToSA = () => {
    if (this.state.alreadySent) {
      this.setState({ showModalAlreadySent: true });
      // this.props.onclickSendToSAValid();
    } else {
      this.hitAPiSendToSA(
        this.state.FollowUpNo,
        this.state.selectedQuotation,
        this.state.quotationno
      );
    }
    this.setState({ showModalMakeSure: false });
  };

  hitAPiSendToSA = (followupno, orderno, quotationno) => {
    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/DataReact/sendToSA/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Bearer " + JSON.parse(ACCOUNTDATA).Token
      }),
      body:
        "followupno=" +
        followupno +
        "&orderno=" +
        orderno +
        "&quotationno=" +
        quotationno +
        "&salesofficerid=" +
        JSON.parse(ACCOUNTDATA).UserInfo.User.SalesOfficerID
    })
      .then(res => res.json())
      .then(datamapping => {
        if (datamapping.status) {
          toast.info("✔️ Quotation has been sent to SA.", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });

          this.props.onclickSendToSAValid();
        } else {
          toast.dismiss();
          toast.warning("❗ error - " + datamapping.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        this.setState({
          isLoading: false
        });
        if(!(error+"".toLowerCase().includes("token"))){
          this.hitAPiSendToSA(followupno, orderno, quotationno);
        }
      });
  };

  render() {
    return (
      <div>
        <Otosalesmodalsenttosaalreadysent
          state={this.state}
          onClose={() => this.setState({ showModalAlreadySent: false })}
          hitAPiSendToSA={this.hitAPiSendToSA}
        />
        <Otosalesmodalsenttosamakesure
          state={this.state}
          onClose={() => this.setState({ showModalMakeSure: false })}
          yesSendToSA={() => this.yesSendToSA()}
        />
        <Modal
          styles={{
            overlay: {
              zIndex: 1050
            }
          }}
          showCloseIcon={false}
          open={this.props.showModal}
          onClose={() => this.props.closeModal()}
          center
        >
          <div
            className="modal-content panel panel-primary"
            style={{
              marginBottom: "-13px",
              marginTop: "-12px",
              marginLeft: "-27px",
              marginRight: "-27px"
            }}
          >
            <BlockUi
              tag="div"
              blocking={this.state.isLoading}
              loader={
                <Loader active type="ball-spin-fade-loader" color="#02a17c" />
              }
              message="Loading, please wait"
            >
              <div className="modal-header panel-heading">
                <h4 className="modal-title pull-left" id="exampleModalLabel">
                  <b>Send to SA</b>
                </h4>
                <div className="dropdown pull-right">
                  <a
                    className="fa fa-send fa-lg"
                    onClick={() => this.clickSend()}
                    style={{ color: "#fff" }}
                  />
                </div>
              </div>
              <div className="modal-body">
                {this.state.selectedQuotation != null && (
                  <OtosalesQuotationHistory
                    onClickQuotationHistory={this.onClickQuotationHistory}
                    selectedQuotation={this.state.selectedQuotation}
                    dataitems={this.state.dataquotationhistory}
                  />
                )}
              </div>
              <div className="modal-footer" />
            </BlockUi>
          </div>
        </Modal>
      </div>
    );
  }
}

export default otosalesmodalsenttosa;
