import React, { Component } from "react";
import Dropzone from "react-dropzone";
import { PlaceHolderImage } from "../assets";
import Lightbox from "react-image-lightbox";

class otosalesdropzone extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  render() {
    const img =
      this.props.src.length > 0
        ? URL.createObjectURL(this.props.src[0])
        : PlaceHolderImage;

    const Isimgstatusnotnull =
      this.props.src.length > 0
        ? true
        : false;

    return (
      <div>
        {this.state.isOpen && (
          <Lightbox
            reactModalStyle={{
              overlay: {
                zIndex: 1050
              }
            }}
            mainSrc={img}
            onCloseRequest={() => this.setState({ isOpen: false })}
          />
        )}
        <div
          className="squareimg"
          onClick={() => this.setState({ isOpen: true })}
        >
          <img className="contentimg" style={{ width: "100%", imageOrientation:"from-image" }} src={img} />
        </div>
        <Dropzone
          onDrop={(acceptedFiles, rejectedFiles) =>
            this.props.handleUploadImage(
              this.props.name,
              acceptedFiles,
              rejectedFiles
            )
          }
          multiple={false}
          accept="image/png, image/gif, image/jpeg, image/jpg"
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <a className="btn btn-xs btn-primary">Choose</a>
              </div>
            </section>
          )}
        </Dropzone>

        {Isimgstatusnotnull && <a onClick={() => this.props.handleDeleteImage(this.props.name)} className="btn btn-xs btn-danger">Delete</a>}

      </div>
    );
  }
}

export default otosalesdropzone;
