import React, { Component } from "react";

class otosaleslist extends Component {
  constructor(props) {
    super();
    this.state = {
      items: props.dataitems,
      rowcount: props.rowscount || 0
    };
  }
  render() {
    var pg = this.state.rowcount;
    let texts = this.state.items.map(function(text, index) {
      if (index < pg) {
        return (
          <a
            href="#"
            key={index}
            className="list-group-item "
            style={{ padding: "5px 10px", minHeight: "26px" }}
          >
            <span
              className="badge"
              style={{ float: "right", backgroundColor: "orange" }}
            >
              {index + 1}
            </span>
            <span className="smalltext">{text}</span>
          </a>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12" name={this.props.name}>
        <div className="list-group" style={{ padding: "0" }}>
          {texts}
        </div>
      </div>
    );
  }
}

export default otosaleslist;
