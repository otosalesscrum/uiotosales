import React, { Component } from "react";
import { 
    NeedFu, 
    Potential,
    CallLater,
    CollectDoc,
    NotDeal,
    SendToSA,
    PolicyCreated,
    BackToAO
  } from "../assets";
import { Util } from ".";

class otosaleslistfollowuphistory extends Component {
  constructor(props) {
    super();
  }

  render() {
    const iconStatus = (status) => {
        if (status == 1) {
          return NeedFu;
        }
        else if (status == 2) {
          return Potential;
        }
        else if (status == 3) {
          return CallLater;
        }
        else if (status == 4) {
          return CollectDoc;
        }
        else if (status == 5) {
          return NotDeal;
        }
        else if (status == 6) {
          return SendToSA;
        }
        else if (status == 7) {
          return PolicyCreated;
        }
        else if (status == 8) {
          return BackToAO;
        }
        else if (status == 9) {
          return NeedFu;
        }else{
          return null;
        }
      }

    var pg = this.props.rowscount || 0;
    let texts = this.props.dataitems.map((data, index) => {
        const datainfo = this.props.datastatusinfoall.filter(datainfotemp => {
            return datainfotemp.InfoCode == data.FollowUpInfo
        });
        var datainforender = datainfo[0] != undefined ? datainfo[0].Description+"" : "";

        const date = Util.convertDate(data.LastFollowUpDate);
        const month = date.toLocaleString('en-us', { month: 'short' });
        const day = date.toLocaleDateString('en-us', { weekday: 'short' }); 
        const datadate = day+" "+date.getDate()+" "+month+" "+date.getFullYear();
        const datatime = date.getHours()+":"+date.getMinutes();
      if (index < pg) {
        return (
          <div className="col-md-12 col-xs-12" key={index}>
            <div className="panel panel-default">
              <div className="panel-body">
                <div className="row">
                  <div className="col-xs-3 col-md-1">
                    <img 
                    src={iconStatus(data.FollowUpStatus)}
                    style={{ width: "100%", marginRight:"5px" }}
                    />
                  </div>
                  <div className="col-xs-9 col-md-11">
                    <p><b>{data.ProspectName}</b></p>
                    <p>{datainforender}</p>
                    <p><b>{datadate}</b>{", "+datatime}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <div className="col-xs-12 panel-body-list" name={this.props.name}>
        {texts}
      </div>
    );
  }
}

export default otosaleslistfollowuphistory;
