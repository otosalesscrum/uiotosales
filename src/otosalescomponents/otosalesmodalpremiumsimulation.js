import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { ToastContainer, toast } from "react-toastify";

export default class hotosalesmodalpremiumsimulation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ProspectName: "",
      Phone1: "",
      ProspectPhone2: "",
      Email: "",
      isErrProsName: false,
      isErrPhone1: false,
      isErrEmail: false,
      isErrPhone2: false,
      messageError: "",
      isErrorProspect: false,
      isErrVehDetails: false,
      VehicleDetailsTemp: ""
    };
  }

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  showAlertError = (message) => {
    toast.dismiss();
    toast.warning("❗ "+message, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  }

  onSavePremium = () => {
    this.setState(
      {
        isErrProsName: false,
        isErrPhone1: false,
        isErrEmail: false,
        isErrPhone2: false,
        isErrorProspect: false
      },
      () => {

        if (this.props.state.ProspectName == "" 
            && this.props.state.Phone1 == "" 
            || this.props.state.Phone1 == null
            && this.props.state.ProspectEmail == "") {
          this.setState({
            isErrProsName: true,
            isErrorProspect: true,
            isErrPhone1: true,
            messageError:"Harap mengisi semua field yang dibutuhkan!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrProsName = true;
            this.props.state.isErrPhone1 = true;
            this.props.state.isErrEmail = true;
          });
          return;
        }

        if (this.props.state.ProspectName == "") {
          this.setState({
            isErrProsName: true,
            isErrorProspect: true,
            messageError:"Harap masukan prospect name!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrProsName = true;
          });
          return;
        }

        if (this.props.state.Phone1 == "" || this.props.state.Phone1 == null) {
          this.setState({
            isErrPhone1: true,
            isErrorProspect: true,
            messageError:"Harap masukan prospect phone!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrPhone1 = true;
          });
          return;
        }

        if (this.props.state.ProspectEmail == "" || this.props.state.ProspectEmail == null) {
          this.setState({
            isErrEmail: true,
            isErrorProspect: true,
            messageError:"Harap masukan prospect email!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrEmail = true;
          });
          return;
        }

        if (this.props.state.VehicleDetailsTemp == "" || this.props.state.VehicleDetailsTemp == null) {
          this.setState({
            isErrVehDetails: true,
            isErrorProspect: true,
            messageError:"Harap masukan data kendaraan!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrVehDetails = true;
          });
          return;
        }

        if (
          this.props.state.Phone1.length < 9 ||
          this.props.state.Phone1.length > 13
        ) {
          this.setState({
            isErrPhone1: true,
            isErrorProspect: true,
            messageError:"Nomor Telfon minimal 9 karakter dan maksimal 13 karakter!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrPhone1 = true;
          });

          return;
        }

        if (
          this.props.state.ProspectEmail != "" &&
          !/\S+@\S+\.\S+/.test(this.props.state.ProspectEmail)
        ) {
          this.setState({
            isErrEmail: true,
            isErrorProspect: true,
            messageError:"Format email tidak sesuai!"
          }, () => {
            this.showAlertError(this.state.messageError);
            this.props.state.isErrEmail = true;
          });

          return;
        }

        this.props.onSaveSimulation(
          this.props.state.ProspectName,
          this.props.state.Phone1,
          this.props.state.ProspectEmail
        );
      }
    );
  };

  onSendEmailPremium = () => {
    this.setState({
      isErrProsName: false,
      isErrPhone1: false,
      isErrEmail: false,
      isErrPhone2: false,
      isErrorProspect: false
    });

    const state = this.state;

    if (this.props.state.ProspectName == "") {
      this.setState({
        isErrProsName: true,
        isErrorProspect: true,
        messageError:"Harap masukan Prospect Name!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });
      return;
    }

    if (this.props.state.Phone1 == "" || this.props.state.Phone1 == null) {
      this.setState({
        isErrPhone1: true,
        isErrorProspect: true,
        messageError:"Harap masukan Prospect Phone 1!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });
      return;
    }

    if (
      this.props.state.Phone1.length < 9 ||
      this.props.state.Phone1.length > 13
    ) {
      this.setState({
        isErrPhone1: true,
        isErrorProspect: true,
        messageError:"Phone 1 harus diisi 9 - 13 characters!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });

      return;
    }

    if(this.props.state.ProspectEmail.length == 0){
      this.setState({
        isErrEmail: true,
        isErrorProspect: true,
        messageError:"Harap masukan alamat email !"
      }, () => {
        this.showAlertError(this.state.messageError);
      });

      return;
    }

    if (
      !/\S+@\S+\.\S+/.test(this.props.state.ProspectEmail)
    ) {
      this.setState({
        isErrEmail: true,
        isErrorProspect: true,
        messageError:"Email Address tidak valid!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });

      return;
    }

    if (state.isErrorProspect) {
      return;
    }
    console.log(this.props.state.ProspectEmail);
    this.props.onSendEmail(
      this.props.state.ProspectName,
      this.props.state.Phone1,
      this.props.state.ProspectPhone2,
      this.props.state.ProspectEmail
    );
  };

  onSendSMSPremium = () => {
    this.setState({
      isErrProsName: false,
      isErrPhone1: false,
      isErrEmail: false,
      isErrPhone2: false,
      isErrorProspect: false
    });

    const state = this.state;

    if (this.props.state.ProspectName == "") {
      this.setState({
        isErrProsName: true,
        isErrorProspect: true,
        messageError:"Harap masukan Prospect Name!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });
      return;
    }

    if (this.props.state.Phone1 == "" || this.props.state.Phone1 == null) {
      this.setState({
        isErrPhone1: true,
        isErrorProspect: true,
        messageError:"Harap masukan Prospect Phone 1!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });
      return;
    }

    if (
      this.props.state.Phone1.length < 9 ||
      this.props.state.Phone1.length > 13
    ) {
      this.setState({
        isErrPhone1: true,
        isErrorProspect: true,
        messageError:"Phone 1 harus diisi 9 - 13 characters!"
      }, () => {
        this.showAlertError(this.state.messageError);
      });

      return;
    }

    if (
      this.props.state.ProspectEmail != "" &&
      !/\S+@\S+\.\S+/.test(this.props.state.ProspectEmail)
    ) {
      this.setState({
        isErrEmail: true,
        isErrorProspect: true,
        messageError:"Email Address tidak valid"
      }, () => {
        this.showAlertError(this.state.messageError);
      });

      return;
    }

    if (state.isErrorProspect) {
      return;
    }

    this.props.onSendSMS(
      this.props.state.ProspectName,
      this.props.state.Phone1,
      this.props.state.ProspectPhone2,
      this.props.state.ProspectEmail
    );
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer/>
      <Modal
        styles={{
          modal: {
            width: "50%",
            textAlign: "center"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.props.showModal}
        showCloseIcon={false}
        onClose={() => this.props.closeModal()}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-18px" }}
        >
          {/* <div className="modal-header panel-heading"> */}
            {/* <div className="dropdown pull-right">
              <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                style={{backgroundColor:"#18516c", borderColor:"#18516c"}}
                data-toggle="dropdown"
              >
                <i className="fa fa-ellipsis-v fa-lg" />
              </button>
              <ul
                className="dropdown-menu container-shadow-box"
                style={{ marginTop: "0px" }}
              >
                <li>
                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSavePremium()}
                  >
                    Save
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                </li>
                <li>
                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSendEmailPremium()}
                  >
                    Send Email
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                </li>
                <li>
                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSendSMSPremium()}
                  >
                    Send Messages
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                </li>
              </ul>
            </div> */}
          {/* </div> */}
          <div className="modal-body">

                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSavePremium()}
                  >
                    Save
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSendEmailPremium()}
                  >
                    Send Quotation via Email
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                  <a
                    style={{ color: "#000", fontSize: "12pt" }}
                    onClick={() => this.onSendSMSPremium()}
                  >
                    Send Quotation via SMS
                  </a>
                  <hr style={{ marginTop: "3px", marginBottom: "3px" }} />

            {/* <label>PROSPECT NAME *</label>
            <div
              className={
                this.state.isErrProsName ? "form-group has-error" : "form-group"
              }
            >
              <div className="input-group">
                // <div className="input-group-addon ">
                  <i className="fa fa-pencil" />
                </div> //
                <input
                  onChange={this.onChangeFunction}
                  type="text"
                  style={{ marginBottom: "0px" }}
                  name="ProspectName"
                  value={this.state.ProspectName}
                  className="form-control "
                  placeholder="Enter name prospect"
                />
              </div>
            </div>
            <label>PROSPECT PHONE 1 *</label>
            <div
              className={
                this.state.isErrPhone1 ? "form-group has-error" : "form-group"
              }
            >
              <div className="input-group">
                // <div className="input-group-addon ">
                  <i className="fa fa-pencil" />
                </div> //
                <input
                  onChange={this.onChangeFunction}
                  type="number"
                  style={{ marginBottom: "0px" }}
                  name="Phone1"
                  value={this.state.Phone1}
                  className="form-control "
                  placeholder="Enter phone 1"
                />
              </div>
            </div>
            <label>PROSPECT PHONE 2 </label>
            <div
              className={
                this.state.isErrPhone2 ? "form-group has-error" : "form-group"
              }
            >
              <div className="input-group">
                // <div className="input-group-addon ">
                  <i className="fa fa-pencil" />
                </div> //
                <input
                  onChange={this.onChangeFunction}
                  type="number"
                  style={{ marginBottom: "0px" }}
                  name="ProspectPhone2"
                  value={this.state.ProspectPhone2}
                  className="form-control "
                  placeholder="Enter phone 2"
                />
              </div>
            </div>
            <label>PROSPECT EMAIL *</label>
            <div
              className={
                this.state.isErrEmail ? "form-group has-error" : "form-group"
              }
            >
              <div className="input-group">
                // <div className="input-group-addon ">
                  <i className="fa fa-pencil" />
                </div> //
                <input
                  onChange={this.onChangeFunction}
                  type="email"
                  style={{ marginBottom: "0px" }}
                  name="Email"
                  value={this.state.Email}
                  className="form-control "
                  placeholder="Enter email"
                />
              </div>
            </div> */}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-12">
                {/* <button
                  onClick={() => this.props.closeModal()}
                  className="btn btn-warning form-control"
                >
                  Cancel
                </button> */}
              </div>
            </div>
          </div>
        </div>
      </Modal>
      </React.Fragment>
    );
  }
}
