import React, { Component } from "react";
import Dropzone from "react-dropzone";
import { PlaceHolderImage, addImage } from "../assets";
import Lightbox from "react-image-lightbox";
import Modal from "react-responsive-modal";

class otosalesdropzonev2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      showModal: false
    };
  }

  handleClick = e => {
    this.inputElement.click();
    this.setState({ showModal: false });
  };

  render() {
    const Isimgstatusnotnull = this.props.src.length > 0 ? true : false;

    const ModalList = () => (
      <Modal
        styles={{
          modal: {
            width: "80%"
          },
          overlay: {
            zIndex: 1050
          }
        }}
        showCloseIcon={false}
        open={this.state.showModal}
        onClose={() => this.setState({ showModal: false })}
        center
      >
        <div
          className="modal-content panel panel-primary"
          style={{ margin: "-17px" }}
        >
          <div className="modal-body">
            <div
              onClick={this.handleClick}
              className="form-group separatorBottom paddingside"
              style={{
                paddingTop: "5px",
                paddingBottom: "5px"
              }}
            >
              <div
                className="row panel-body-list"
                style={{ marginLeft: "0px", marginRight: "0px" }}
              >
                <div className="col-xs-12 panel-body-list">
                  <span
                    className="smalltext labelspanbold"
                    style={{ fontSize: "10pt" }}
                  >
                    Choose
                  </span>
                </div>
              </div>
            </div>
            {Isimgstatusnotnull && (
              <div
                onClick={() =>
                  this.setState({ isOpen: true, showModal: false })
                }
                className="form-group separatorBottom paddingside"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <div
                  className="row panel-body-list"
                  style={{ marginLeft: "0px", marginRight: "0px" }}
                >
                  <div className="col-xs-12 panel-body-list">
                    <span
                      className="smalltext labelspanbold"
                      style={{ fontSize: "10pt" }}
                    >
                      Preview
                    </span>
                  </div>
                </div>
              </div>
            )}
            {Isimgstatusnotnull && (
              <div
                onClick={() => {
                  this.props.handleDeleteImage(this.props.name);
                  this.setState({ showModal: false });
                }}
                className="form-group separatorBottom paddingside"
                style={{
                  paddingTop: "5px",
                  paddingBottom: "5px"
                }}
              >
                <div
                  className="row panel-body-list"
                  style={{ marginLeft: "0px", marginRight: "0px" }}
                >
                  <div className="col-xs-12 panel-body-list">
                    <span
                      className="smalltext labelspanbold"
                      style={{ fontSize: "10pt" }}
                    >
                      Delete
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );
    const img =
      this.props.src.length > 0
        ? URL.createObjectURL(this.props.src[0])
        : addImage;

    return (
      <div>
        <ModalList />
        {this.state.isOpen && (
          <Lightbox
            reactModalStyle={{
              overlay: {
                zIndex: 1050
              }
            }}
            mainSrc={img}
            onCloseRequest={() => this.setState({ isOpen: false })}
          />
        )}
        <div
          className="squareimg"
          //   onClick={() => this.setState({ isOpen: true })}
          onClick={() => this.setState({ showModal: true })}
        >
          <img
            className="contentimg"
            style={{ width: "100%", imageOrientation: "from-image" }}
            src={img}
          />
        </div>
        <Dropzone
          onDrop={(acceptedFiles, rejectedFiles) =>
            this.props.handleUploadImage(
              this.props.name,
              acceptedFiles,
              rejectedFiles
            )
          }
          multiple={false}
          accept="image/png, image/gif, image/jpeg, image/jpg"
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input
                  {...getInputProps()}
                  ref={input => (this.inputElement = input)}
                />
              </div>
            </section>
          )}
        </Dropzone>
        {/* 
        {Isimgstatusnotnull && (
          <a
            onClick={() => this.props.handleDeleteImage(this.props.name)}
            className="btn btn-xs btn-danger"
          >
            Delete
          </a>
        )} */}
      </div>
    );
  }
}

export default otosalesdropzonev2;
