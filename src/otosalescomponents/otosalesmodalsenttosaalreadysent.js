import React, { Component } from 'react'
import Modal from "react-responsive-modal"

export default class otosalesmodalsenttosaalreadysent extends Component {
    render() {
        return (
            <Modal
          styles={{
            overlay: {
              zIndex: 1050
            }
          }}
          open={this.props.state.showModalAlreadySent}
          showCloseIcon={false}
          onClose={() => this.props.onClose()}
          center
        >
          <div className="modal-content panel panel-primary" style={{margin:"-25px"}}>
            <div className="modal-header panel-heading">
              <h4 className="modal-title pull-left" id="exampleModalLabel">
                <b>Confirmation</b>
              </h4>
            </div>
            <div className="modal-body">
            You have sent a quotation to SA. If you send this quotation, the previous quotation you have sent will be replaced. Are you sure you want to continue?
            </div>
            <div className="modal-footer">
              <div>
                <div className="col-xs-6">
                  <button
                    onClick={() => this.props.onClose()}
                    className="btn btn-warning form-control"
                  >
                    No
                  </button>
                </div>
                <div className="col-xs-6">
                  <button
                    onClick={() => {
                      this.props.onClose();
                      this.props.hitAPiSendToSA(this.props.state.FollowUpNo, this.props.state.selectedQuotation, this.props.state.quotationno)
                    }
                    }
                    className="btn btn-info form-control"
                  >
                    Yes
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal>
        )
    }
}
