import React, { Component } from "react";
import Modal from "react-responsive-modal";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import { OtosalesQuotationHistory } from "./index";

export default class otosalesmodalquotationhistory extends Component {
  render() {
    return (
      <div>
        <Modal
          styles={{
            modal: {
              maxWidth: "500px",
              marginTop: "30px"
            },
            overlay: {
              zIndex: 1050
            }
          }}
          open={this.props.state.showModal}
          showCloseIcon={false}
          onClose={() => this.props.onClose()}
          center
        >
          <div
            className="modal-content panel panel-primary"
            style={{ margin: "-17px" }}
          >
            <BlockUi
              tag="div"
              blocking={
                this.props.state.isLoading && this.props.state.showModal
              }
              loader={
                <Loader active type="ball-spin-fade-loader" color="#02a17c" />
              }
              message="Loading, please wait"
            >
              <div className="modal-header panel-heading">
                <h4 className="modal-title pull-left" id="exampleModalLabel">
                  <b>Quotation History</b>
                </h4>
                <div className="dropdown pull-right">
                  <button
                    className="btn btn-primary dropdown-toggle"
                    type="button"
                    style={{
                      borderColor: "#18516c",
                      backgroundColor: "#18516c"
                    }}
                    data-toggle="dropdown"
                  >
                    <i
                      style={{
                        marginTop: "5px",
                        marginBottom: "5px",
                        marginLeft: "10px",
                        marginRight: "10px"
                      }}
                      className="fa fa-ellipsis-v fa-lg"
                    />
                  </button>
                  <ul
                    className="dropdown-menu container-shadow-box"
                    style={{ marginTop: "0px" }}
                  >
                    <li>
                      <a
                        style={{ color: "#000", fontSize: "12pt" }}
                        onClick={() => this.props.editQuotationHistory()}
                      >
                        Edit Quotation
                      </a>
                      <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                    </li>
                    <li>
                      <a
                        style={{ color: "#000", fontSize: "12pt" }}
                        onClick={() => this.props.onClickSendEmail()}
                      >
                        Send Email
                      </a>
                      <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                    </li>
                    <li>
                      <a
                        style={{ color: "#000", fontSize: "12pt" }}
                        onClick={() => this.props.onClickSendSMS()}
                      >
                        Send Messages
                      </a>
                      <hr style={{ marginTop: "3px", marginBottom: "3px" }} />
                    </li>
                  </ul>
                </div>
              </div>
              <div className="modal-body" style={{maxHeight:"calc(100vh - 210px)", overflowY:"auto"}}>
                {/* {this.state.selectedQuotation != null && ( */}
                <OtosalesQuotationHistory
                  onClickQuotationHistory={this.props.onClickQuotationHistory}
                  selectedQuotation={this.props.state.selectedQuotation}
                  dataitems={this.props.state.dataquotationhistory}
                />
                {/* )} */}
              </div>
              <div className="modal-footer" />
            </BlockUi>
          </div>
        </Modal>
      </div>
    );
  }
}
