import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../css/a2is.css";
import { API_URL, API_VERSION, ACCOUNTDATA } from "../config";

export default class Otosalesheader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNameShow: false
    };
  }

  componentDidMount() {
    let checkname = setInterval(() => {
      if (ACCOUNTDATA != undefined) {
        if (document.body.classList.contains("sidebar-open")) {
          this.setState({ isNameShow: true });
        } else {
          this.setState({ isNameShow: false });
        }
      } else {
        clearInterval(checkname);
      }
    }, 100);
  }

  render() {
    return (
      <header className="main-header">
        <a className="logo hidden-xs">
          <span className="logo-mini">
            <img
              src="img/logo.bak.png"
              className="img-circle"
              style={{ height: "40px" }}
            />
          </span>
          <span className="logo-lg">
            <img
              src="img/logo.bak.png"
              className="img-circle"
              style={{ height: "30px" }}
            />
            <img
              src="img/otosales.png"
              style={{ height: "40px", marginLeft: "10px" }}
            />
          </span>
        </a>
        <nav className="navbar navbar-static-top">
          <a className="navbar-brand hidden-xs" style={{marginLeft:"-5px"}}>
          <i onClick={() => this.props.handleBackButton()}
                className="fa fa-arrow-circle-left pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              {
                this.props.titles != undefined ? <span style={{fontSize:"medium"}}>{this.props.titles }</span>:  
                (<img
                  src="img/otosales.png"
                  className="img pull-left"
                  style={{ height: "30px", marginRight: "10px" }}
                />)
              }
        </a>
          {this.state.isNameShow == true ? (
            <a className="navbar-brand hidden-lg" style={{marginTop:"-5px", marginLeft:"-5px"}}>
              <i onClick={() => this.props.handleBackButton()}
                className="fa fa-arrow-circle-left pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              {
                this.props.titles != undefined ? <span style={{fontSize:"medium"}}>{this.props.titles }</span>:  
                (<img
                  src="img/otosales.png"
                  className="img pull-left"
                  style={{ height: "30px", marginRight: "10px" }}
                />)
              }
            </a>
          ) : (
            <a className="navbar-brand hidden-lg hidden-sm hidden-md" style={{marginLeft:"-5px"}}>
              <i onClick={() => this.props.handleBackButton()}
                // src="img/logoonly.png"
                className="fa fa-arrow-circle-left pull-left"
                style={{ height: "30px", marginRight: "5px" }}
              />
              {
                this.props.titles != undefined ? <span style={{fontSize:"medium"}}>{this.props.titles }</span>:  
                (<img
                  src="img/otosales.png"
                  className="img pull-left"
                  style={{ height: "30px", marginRight: "10px" }}
                />)
              }
            </a>
          )}
          <div className="navbar-custom-menu" style={{marginRight:"20px"}}>
          {this.state.isNameShow == true ? "":this.props.children}
          </div>
          
        </nav>
      </header>
    );
  }
}
