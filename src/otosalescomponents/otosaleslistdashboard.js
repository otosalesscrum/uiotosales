import React, { Component } from "react";
import A2isProgressBar from '../components/loader/a2isprogressbar';
class otosaleslistdashboard extends Component {
  constructor(props) {
    super();
  }

  render() {
    const totalvalue = (code, data) => {
      if (code == 0) {
        return data.TotalProspect;
      }
      else if (code == 1) {
        return data.TotalNeedFu;
      }
      else if (code == 2) {
        return data.TotalPotential;
      }
      else if (code == 3) {
        return data.TotalCallLater;
      }
      else if (code == 4) {
        return data.TotalCollectDoc;
      }
      else if (code == 5) {
        return data.TotalNotDeal;
      }
      else if (code == 6) {
        return data.TotalSendToSA;
      }
      else if (code == 7) {
        return data.TotalPolicyCreated;
      }
      else if (code == 8) {
        return data.TotalBackToAO;
      }
      else if (code == 9) {
        return data.TotalDeal;
      } else {
        return null;
      }
    }
    const maxValue = (maindata, data) => {
      var Max = 0;
      for (var i = 0; i < maindata.length; i++) {

        if (maindata[i].TotalProspect > Max) {
          Max = maindata[i].TotalProspect;
        }
        if (maindata[i].TotalNeedFu > Max) {
          Max = maindata[i].TotalNeedFu;
        }
        if (maindata[i].TotalPotential > Max) {
          Max = maindata[i].TotalPotential;
        }
        if (maindata[i].TotalCallLater > Max) {
          Max = maindata[i].TotalCallLater;
        }
        if (maindata[i].TotalCollectDoc > Max) {
          Max = maindata[i].TotalCollectDoc;
        }
        if (maindata[i].TotalNotDeal > Max) {
          Max = maindata[i].TotalNotDeal;
        }
        if (maindata[i].TotalSendToSA > Max) {
          Max = maindata[i].TotalSendToSA;
        }
        if (maindata[i].TotalPolicyCreated > Max) {
          Max = maindata[i].TotalPolicyCreated;
        }
        if (maindata[i].TotalBackToAO > Max) {
          Max = maindata[i].TotalBackToAO;
        }
        if (maindata[i].TotalDeal > Max) {
          Max = maindata[i].TotalDeal;
        }
      }
      console.log("ena" + Max);
      return Max;
    }
    var pg = this.props.rowscount || 0;
    var datavalue = this.props.viewFilter;
    var maxval = maxValue(this.props.dataitems, datavalue);
    let texts = this.props.dataitems.map((data, index) => {
      var role = this.props.Role;
      var datadesc = "";
      if (role == "NATIONALMGR") {
        datadesc = data.Region;
      }
      else if (role == "REGIONALMGR") {

        datadesc = data.BranchDesc;
      }
      else if ((role == "BRANCHMGR") || (role == "SALESSECHEAD")) {

        datadesc = data.SalesName;

      }
      else if ((role == "SALESOFFICER") || (role == "SESALESOFFICER") || (role == "SESALESEXECUTIVE") ||
        (role == "SALESEXECUTIVE")) {

        datadesc = data.SalesName;
      }

      console.log("datanya" + pg);
      if (index < pg) {
      
        return (
          <div className="form-group" onClick={() => (role != "BRANCHMGR") || (role == "SALESSECHEAD") ? this.props.onClickListItems(role == "REGIONALMGR" ? this.props.dataitems[index].BranchCode : this.props.dataitems2[index].Param) : true}>
            <div className="row" style={{ marginLeft: "0px", marginRight: "0px", textAlign:"right" }}><div className="col-xs-6" >
              <span className="smalltext" style={{ fontSize: "10pt" }}>{datadesc.toUpperCase()}</span>
            </div>
              <div className="col-xs-6" style={{textAlign:"left"}}>
                {(totalvalue(datavalue,data)>0)&&<div className="col-xs-10 " style={{paddingLeft: "0px"}}> <A2isProgressBar
                  variant="warning"
                  now={totalvalue(datavalue, data)}
                  min={0}
                  max={maxval}
                  striped={false}
                  caption=" "
                /></div>}
                <span style={{ fontSize: "10pt", fontWeight: "500" }}>{totalvalue(datavalue, data)}</span>
              </div>
            </div>
          </div>
        );
      }
    }, this.pg);

    return (
      <div>
        {texts}
      </div>
    );
  }
}

export default otosaleslistdashboard;