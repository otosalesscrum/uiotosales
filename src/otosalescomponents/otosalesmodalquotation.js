import React, { Component } from 'react'
import Modal from "react-responsive-modal"
import {
    OtosalesQuotationHistory
  } from "../otosalescomponents";

export default class otosalesmodalquotation extends Component {
  render() {
    return (
        <Modal
        styles={{
          overlay: {
            zIndex: 1050
          }
        }}
        open={this.state.showModal}
        onClose={() => this.setState({ showModal: false })}
        center
      >
        <div className="modal-content" style={{ margin: "-11px" }}>
          <div className="modal-header" style={{marginTop:"30px"}}>
            <h4 className="modal-title pull-left" id="exampleModalLabel">
              <b>Quotation History</b>
            </h4>
            <div className="dropdown pull-right">
              <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                data-toggle="dropdown"
              >
                <i className="fa fa-ellipsis-v"></i>
              </button>
              <ul className="dropdown-menu container-shadow-box" style={{marginTop:"0px"}}>
                <li>
                  <a style={{color:"#000", fontSize:"12pt"}} onClick={this.props.editQuotationHistory}>Edit Quotation</a>
                  <hr style={{marginTop:"3px", marginBottom:"3px"}}/>
                </li>
                <li>
                  <a style={{color:"#000", fontSize:"12pt"}}>Send Email</a>
                  <hr style={{marginTop:"3px", marginBottom:"3px"}}/>
                </li>
                <li>
                  <a style={{color:"#000", fontSize:"12pt"}}>Send Messages</a>
                  <hr style={{marginTop:"3px", marginBottom:"3px"}}/>
                </li>
              </ul>
            </div>
          
          </div>
          <div className="modal-body">
            {this.state.selectedQuotation != null && (
              <OtosalesQuotationHistory
                onClickQuotationHistory = {this.props.onClickQuotationHistory}
                selectedQuotation={this.state.selectedQuotation}
                dataitems={this.props.dataquotationhistory}
              />
            )}
          </div>
          <div className="modal-footer">
            <div>
              <div className="col-xs-6">
                <button
                  onClick={() => this.setState({ showModal: false })}
                  className="btn btn-warning form-control"
                >
                  Cancel
                </button>
              </div>
              <div className="col-xs-6">
                <button
                  onClick={() => console.log("Clicked")}
                  className="btn btn-info form-control"
                >
                  Approve
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}
