import NeedFu from "./need_fu_180.png";
import Potential from "./potential_180.png";
import CallLater from "./call_later_180.png";
import CollectDoc from "./waiting_doc_180.png";
import NotDeal from "./not_deal_180.png";
import SendToSA from "./sent_to_sa_180.png";
import PolicyCreated from "./policycreated_180.png";
import BackToAO from "./backtoao_180.png";
import PlaceHolderImage from "./placeholder.png";
import HexCarInfo from "./hex_car_info.png";
import HexDealerInfo from "./hex_dealer_info.png";
import HexPolicyDetails from "./hex_policy_details.png";
import HexPolicyInfo from "./hex_policy_info.png";
import HexProspectInfo from "./hex_prospek_info.png";
import mrSmile from "./mr_smile.png";
import Call from "./call.png";
import addImage from "./addimage.png";
import SendQuotation from "./send_180.png";
import PlaceholderLoading from "./giphy.gif";
import sendEmail from "./send.png";
import sendEmailv2 from "./sendemail.png";
import sendSMS from "./sendsms.png";
import sendWA from "./sendwa.png";
import checklist from "./checklist.png";
import savedisk from "./save-disk.png";
import pdftype from "./pdftype.png";
import notsupportpreview from "./notsupportpreview.png";
import hangupphone from "./hang-up-icon-7.png";

export {
    NeedFu,
    Potential,
    CallLater,
    CollectDoc,
    NotDeal,
    SendToSA,
    PolicyCreated,
    BackToAO,
    PlaceHolderImage,
    HexCarInfo,
    HexDealerInfo,
    HexPolicyDetails,
    HexPolicyInfo,
    HexProspectInfo,
    mrSmile,
    Call,
    addImage,
    SendQuotation,
    PlaceholderLoading,
    sendEmail,
    sendEmailv2,
    sendSMS,
    sendWA,
    checklist,
    savedisk,
    pdftype,
    notsupportpreview,
    hangupphone
};