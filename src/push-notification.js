// import firebase from "firebase/messaging";
import * as firebase from "firebase/app";
import "firebase/messaging";
import {
  FCMmessagingSenderId,
  API_URL,
  API_VERSION,
  DeviceType,
  APP_VERSION,
  HEADER_API
} from "./config";
import { DeviceUUID } from "device-uuid/lib/device-uuid";
import * as Util from "./otosalescomponents/helpers/Util.js";
import {secureStorage} from "./otosalescomponents/helpers/SecureWebStorage";

let messaging;
let token;

export const initializeFirebase = () => {
  firebase.initializeApp({
    messagingSenderId: FCMmessagingSenderId
  });

  // messaging = firebase.messaging();

  // if ('serviceWorker' in navigator) {
  //   window.addEventListener('load', async () => {
  //     const registration = await navigator.serviceWorker.register('/firebase-messaging-sw.js', {
  //       updateViaCache: 'none'
  //     });
  //     messaging.useServiceWorker(registration);
  //     messaging.onMessage((payload) => {
  //       const title = payload.notification.title;
  //       const options = {
  //         body: payload.notification.body,
  //         icon: payload.notification.icon,
  //         actions: [
  //           {
  //             action: payload.fcmOptions.link,
  //             title: 'Book Appointment'
  //           }
  //         ]
  //       };
  //       registration.showNotification(title, options);
  //     });
  //   });
  // }
};

export const deleteTokenFCMandLogOut = async () => {
  try {
    if(!Util.isNullOrEmpty(token)){
      unregisterGCM();
      messaging.deleteToken(token);
    }
  } catch(ex) {
    console.log(ex);
  }
}

export const askForPermissioToReceiveNotifications = async username => {
  try {
    messaging = firebase.messaging();
    await messaging.requestPermission();
    token = await messaging.getToken();
    console.log("token do usuário:", token);

    // var myWorker = new Worker('firebase-messaging-sw.js'),
    //   data,
    //   changeData = function() {
    //     // save data to local storage
    //     secureStorage.setItem('data', (new Date).getTime().toString());
    //     // get data from local storage
    //     data = secureStorage.getItem('data');
    //     sendToWorker();
    //   },
    //   sendToWorker = function() {
    //     // send data to your worker
    //     myWorker.postMessage({
    //       data: data
    //     });
    //   };
    // setInterval(changeData, 1000)

    const registration = await navigator.serviceWorker.register(
      "/firebase-messaging-sw.js",
      {
        updateViaCache: "none"
      }
    );


      console.log("test masuk");
    // messaging.onMessage(payload => {
    //   console.log("bawah payload:");
    //   console.log("payload push notif :",payload);
    //   const title = payload.notification.title;
    //   const options = {
    //     body: payload.notification.body,
    //     icon: payload.notification.icon,
    //     actions: [
    //       {
    //         action: payload.notification.click_action,
    //         title: title
    //       }
    //     ]
    //   };
    //   registration.showNotification(title, options);
    // });

    messaging.onMessage(payload => {
      console.log("bawah payload:");
      console.log("payload push notif :", payload);
      
      if(!Util.isNullOrEmpty(payload.data.CustID)){
        let SelectedTaskDetail = {
          CustID: payload.data.CustID,
          FollowUpNo: payload.data.FollowUpNo
        }
        secureStorage.setItem("SelectedTaskDetail", JSON.stringify(SelectedTaskDetail));
      }

      const title = payload.notification.title;
      const options = {
        body: payload.notification.body,
        icon: payload.notification.icon,
        actions: [
          {
            action: payload.notification.click_action,
            title: title
          }
        ]
      };
      registration.showNotification(title, options);
    });

    if(!Util.isNullOrEmpty(token)){
      registerDevice(token);
    }

    // fetch(`${API_URL + "" + API_VERSION}/DataReact/saveTokenPushNotif/`, {
    //   method: "POST",
    //   headers: new Headers({
    //     "Content-Type": "application/x-www-form-urlencoded",
    //     Authorization:
    //       "Bearer " + JSON.parse(secureStorage.getItem("account")).Token
    //   }),
    //   body: "username=" + username + "&token=" + token
    // })
    //   .then(res => res.json())
    //   .then(jsn => {
    //     if (jsn.status == 1) {
    //       console.log("Success save token fcm");
    //     } else {
    //       console.log("error save token fcm");
    //     }
    //   })
    //   .catch(error => {
    //     console.log("parsing failed", error);
    //   });

    return token;
  } catch (error) {
    console.error(error);
  }
};

const registerDevice = token => {
  // fetch(`${API_URL + "" + API_VERSION}/Authentication/RegisterDevice/`, {
  //   method: "POST",
  //   headers: new Headers({
  //     "Content-Type": "application/x-www-form-urlencoded",
  //     Authorization:
  //       "Bearer " + JSON.parse(secureStorage.getItem("account")).Token
  //   }),
  //   body:
  //     "DeviceToken=" +
  //     token +
  //     "&DeviceType=" +
  //     DeviceType +
  //     "&UniqueID=" +
  //     new DeviceUUID().get() +
  //     "&AppVersion=" +
  //     APP_VERSION
  // })
  Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION}/Authentication/RegisterDevice/`,
    "POST",
    HEADER_API,
    {
      DeviceToken: token,
      DeviceType: DeviceType,
      UniqueID: new DeviceUUID().get(),
      AppVersion: APP_VERSION
    }
  )
    .then(res => res.json())
    .then(jsn => {
      if (jsn.Status == 1) {
        console.log("Success hit RegisterDevice");
        secureStorage.setItem("DeviceId", jsn.Data);
        console.log("DeviceId", jsn.Data)
        RegisterUserDevice();
      } else {
        console.log("error hit RegisterDevice");
      }
    })
    .catch(error => {
      console.log("parsing failed", error);
    });
};

const RegisterUserDevice = () => {
  // fetch(`${API_URL + "" + API_VERSION}/Authentication/RegisterUserDevice/`, {
  //   method: "POST",
  //   headers: new Headers({
  //     "Content-Type": "application/x-www-form-urlencoded",
  //     Authorization:
  //       "Bearer " + JSON.parse(secureStorage.getItem("account")).Token
  //   }),
  //   body:
  //     "UserID=" +
  //     JSON.parse(secureStorage.getItem("account")).UserInfo.User.SalesOfficerID +
  //     "&DeviceID=" +
  //     secureStorage.getItem("DeviceId")
  // })
  Util.fetchAPIAdditional(
    `${API_URL + "" + API_VERSION}/Authentication/RegisterUserDevice/`,
    "POST",
    HEADER_API,
    {
      UserID: JSON.parse(secureStorage.getItem("account")).UserInfo.User.SalesOfficerID,
      DeviceID: secureStorage.getItem("DeviceId")
    }
  )
    .then(res => res.json())
    .then(jsn => {
      if (jsn.Status == 1) {
        console.log("Success hit RegisterUserDevice");
      } else {
        console.log("error hit RegisterUserDevice");
      }
    })
    .catch(error => {
      console.log("parsing failed", error);
    });
}

const unregisterGCM = () => {
  fetch(`${API_URL + "" + API_VERSION}/Authentication/LogoutTokenGCM/`, {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization:
        "Bearer " + JSON.parse(secureStorage.getItem("account")).Token
    }),
    body:
      "DeviceToken=" +
      token +
      "&DeviceID=" +
      secureStorage.getItem("DeviceId")
  })
    .then(res => res.json())
    .then(jsn => {
      if (jsn.Status == 1) {
        console.log("Success hit unregisterGCM");
        secureStorage.setItem("DeviceId", jsn.Data);
      } else {
        console.log("error hit unregisterGCM");
      }
    })
    .catch(error => {
      console.log("parsing failed", error);
    });
}

export { messaging };
