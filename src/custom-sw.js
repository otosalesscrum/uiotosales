self.addEventListener("message", event => {
  if (event.data === "skipWaiting") {
    console.log("check service worker SkipWaiting");
    self.skipWaiting();
  }
});

self.addEventListener("fetch", event =>{
  console.log('Service working : fetching');
  event.respondWith(fetch(e.request).catch(()=> caches.match(event.request)));
})
