// let prefixPhone = ["08", "628", "02", "+628", "622", "+622"];
import { fetchAPIAdditional } from "./otosalescomponents/helpers/Util";
import { ACCOUNTDATA, API_URL, API_VERSION_0213URF2019, HEADER_API } from "./config";
import { get } from "http";
let landLinePrefix = [];

let mobilePrefix = [];

let prefixPhone = landLinePrefix.concat(mobilePrefix);

let applicationParam = null;

const maxSizeAttachmentEmail = 10000000;

let roleCTI = [];

const emailAdminWA = ["tarya@asuransiastra.com", "fhakim@asuransiastra.com"];

const emailBodyNewCTI =
  "<p>Pelanggan %s yang terhormat," +
  "<br><br>Terima kasih atas kepercayaan Anda untuk memilih " +
  "%s sebagai asuransi kendaraan bermotor Anda, " +
  "%s. Berikut kami lampirkan :<br>" +
  "• Penawaran Premi Penutupan Polis Asuransi Kendaraan Bermotor<br>" +
  "• Ikhtisar Produk dan Prosedur Klaim.<br><br>Mohon konfirmasi " +
  "Bapak/Ibu terkait Penawaran yang kami ajukan di file terlampir, " +
  "dengan membalas e-mail ini atau dengan menghubungi Marketing kami " +
  "di :<br>• %s : %s atau " +
  "%s<br>• Garda Akses 24 Jam di 1 500 112 " +
  "atau e-mail ke : customer_service@asuransiastra.com <br><br>" +
  "Atas perhatian dan kerjasama Bapak/Ibu, kami ucapkan terima kasih. " +
  "Adalah kebanggaan bagi kami dapat melayani kebutuhan asuransi kendaraan " +
  "Anda.<br><br><br>Salam,<br><br>CS Asuransi Astra</p>";

const additionalWA = "Berikut nomor customer : %s";

const emailBodyRenewCTI = "";

const stylingSendEmailWA =
  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><style type="text/css">.bodyContent{font-family:"Vag Rounded Std";font-size:11pt}.@font-face{font-family:"VAG Rounded Std";src:url(https://www.asuransiastra.com/wp-content/uploads/2015/08/VAGRoundedStd-Bold_gdi3.eot)}</style></head><body class="bodyContent">';

const suffixDigit = 4;

const prefixPhoneCTINotGetReturnStatus = [
  "0853"
];

function setRoleCTI(array, callback = () => {}){
  array = array.split(",");
  roleCTI = array;
  callback();
}

function setMobilePrefix(array, callback = () => {}){
  array = array.split(",");
  mobilePrefix = array;
  prefixPhone = landLinePrefix.concat(mobilePrefix);
  callback();
}

function setLandLinePrefix(array, callback = () => {}){
  array = array.split(",");
  landLinePrefix = array;
  prefixPhone = landLinePrefix.concat(mobilePrefix);
  callback();
}

function getApplicationParam(PARAMTYPE, PARAMNUMBER = 0, callback = () => {}){
  // fetch(`${API_URL + "" + API_VERSION_0213URF2019}/DataReact/GetApplicationParameter/`, {
  //   method: "POST",
  //   headers: new Headers({
  //     "Content-Type": "application/x-www-form-urlencoded",
  //     Authorization:
  //       "Bearer " + JSON.parse(ACCOUNTDATA).Token
  //   }),
  //   body: "ParamName=" + PARAMTYPE
  // })
  fetchAPIAdditional(
    `${API_URL + "" + API_VERSION_0213URF2019}/DataReact/GetApplicationParameter/`,
    "POST",
    HEADER_API,
    {
      ParamName: PARAMTYPE
    }
  )
    .then(res => res.json())
    .then(jsn => {
      console.log(jsn);

      let paramGetNumber = 0;

      if(typeof PARAMNUMBER == 'number'){
        if( PARAMNUMBER == 1 || PARAMNUMBER == 0){
          paramGetNumber = PARAMNUMBER;
        }
      }
      callback(jsn.data[paramGetNumber]);
    })
    .catch(error => {
      console.log("parsing failed", error);
      if(!(error+"".toLowerCase().includes("token"))){
        // this.searchDataHitApi();
        getApplicationParam(PARAMTYPE, callback);
      }
    });
}



export {
  prefixPhone,
  roleCTI,
  suffixDigit,
  emailBodyNewCTI,
  emailAdminWA,
  additionalWA,
  stylingSendEmailWA,
  landLinePrefix,
  mobilePrefix,
  maxSizeAttachmentEmail,
  prefixPhoneCTINotGetReturnStatus,
  setMobilePrefix,
  setLandLinePrefix,
  applicationParam,
  getApplicationParam,
  setRoleCTI
};
