import React, { Component, Suspense, lazy } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import {
  PageChangePassword,
  PageInputDetailTaskList,
  PageSerNet,
  Branch,
  Gardacenter,
  Workshop,
  PageTaskList,
  PageInputProspect,
  PageOrderApproval,
  PageProspectLead,
  PageQuotationDetails,
  PageOrderApprovalDetail,
  PageFollowUpDetails,
  PagePremiumSimulation,
  PageQuotationDetailsSimulation,
  PageDashboard,
  PageDashboardNational,
  PageDashboardRegional,
  PageDashboardBranch,
  PageDashboardPersonal,
  PageTasklistDetails,
  PageSurveyDocuments,
  PageDashboardv2
} from "./contents";

import OtosalesLoading from "./otosalescomponents/v2/loading/otosalesloading";
import ErrorBoundary from "./contents/v2/error_handling";
import { secureStorage } from "./otosalescomponents/helpers/SecureWebStorage";

const SideBar = lazy(() => import("./components/SideBar"));

class App extends Component {
  clickSideBarMenu = () => {
    document.body.classList.remove("sidebar-open");
    secureStorage.removeItem("StateTaskList");
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.state != nextState;
  }

  wrapComponent = Wraped => {
    return (
      <React.Fragment>
        <SideBar
          userInfo={this.props.userInfo}
          menu={this.props.menu}
          onSignOut={this.props.onSignOut}
          clickSideBarMenu={this.clickSideBarMenu}
        />
        {Wraped}
      </React.Fragment>
    );
  };
  

  render() {
    // let Home = () => <Content layout={<Layout />} />;
    let Home = null;
    const Changepassword = () =>
      this.wrapComponent(<PageChangePassword username={this.props.username} />);
    const Dashboard = () =>
      this.wrapComponent(
        <PageDashboard
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const DashboardNational = () =>
      this.wrapComponent(
        <PageDashboardNational
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const DashboardRegional = () =>
      this.wrapComponent(
        <PageDashboardRegional
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const DashboardBranch = () =>
      this.wrapComponent(
        <PageDashboardBranch
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const DashboardPersonal = () =>
      this.wrapComponent(
        <PageDashboardPersonal
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );

    const Dashboardv2 = () =>
      this.wrapComponent(
        <PageDashboardv2
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );

    const ServiceNetwork = () => this.wrapComponent(<PageSerNet />);
    const BranchLink = () => this.wrapComponent(<Branch />);
    const GardaLink = () => this.wrapComponent(<Gardacenter />);
    const WorkshopLink = () => this.wrapComponent(<Workshop />);
    const Tasklist = () =>
      this.wrapComponent(<PageTaskList username={this.props.username} />);
    const InputProspect = () =>
      this.wrapComponent(
        <PageInputProspect
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const OrderApproval = () =>
      this.wrapComponent(<PageOrderApproval username={this.props.username} />);
    const ProspectLead = () =>
      this.wrapComponent(<PageProspectLead username={this.props.username} />);
    const QuotationDetails = () => (
      <React.Fragment>
        <PageQuotationDetails username={this.props.username} />
      </React.Fragment>
    );
    const OrderApprovalDetail = () =>
      this.wrapComponent(
        <PageOrderApprovalDetail username={this.props.username} />
      );
    const FollowUpDetails = () => this.wrapComponent(<PageFollowUpDetails />);
    const TaskListEdit = () =>
      this.wrapComponent(
        <PageInputDetailTaskList
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const TaskListDetails = () =>
      this.wrapComponent(
        <PageTasklistDetails
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const PremiumSimulation = () =>
      this.wrapComponent(
        <PagePremiumSimulation
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      );
    const QuotationDetailsSimulation = () => (
      <React.Fragment>
        <PageQuotationDetailsSimulation
          username={this.props.username}
          userInfo={this.props.userInfo}
        />
      </React.Fragment>
    );

    const SurveyDocuments = () => this.wrapComponent(<PageSurveyDocuments />);

    const NotFound = () => (
      <div>
        <div className="content-wrapper" style={{ padding: "10px" }}>
          <h3>404 page not found</h3>
          <p>We are sorry but the page you are looking for does not exist.</p>
        </div>
      </div>
    );

    if (
      this.props.menu.filter(menu => menu.FormID === "PROSPECTLEAD").length > 0
    ) {
      Home = ProspectLead;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "TASKLIST").length > 0
    ) {
      Home = Tasklist;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "INPUTPROSPECT").length > 0
    ) {
      Home = InputProspect;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "ORDERSIMULATION").length >
      0
    ) {
      Home = PremiumSimulation;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "APPROVALORDER").length > 0
    ) {
      Home = OrderApproval;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "DASHBOARD").length > 0
    ) {
      Home = Dashboard;
    } else if (
      this.props.menu.filter(menu => menu.FormID === "INFO").length > 0
    ) {
      Home = ServiceNetwork;
    }

    return (
      <BrowserRouter>
        <div>
          {this.wrapComponent(
            <ErrorBoundary>
              <Suspense
                fallback={
                  <OtosalesLoading
                    className="col-xs-12 panel-body-list loadingfixed"
                    isLoading={true}
                  />
                }
              >
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/dashboard" component={Dashboardv2} />{" "}
                  {/* Dashboard */}
                  {/* <Route path="/dashboard-National" component={DashboardNational} />
            <Route path="/dashboard-Regional" component={DashboardRegional} />
            <Route path="/dashboard-Branch" component={DashboardBranch} />
            <Route path="/dashboard-Personal" component={DashboardPersonal} /> */}
                  {/* <Route path="/dashboard-v2" component={Dashboardv2}/> */}
                  <Route path="/tasklist" component={Tasklist} />
                  <Route path="/changepassword" component={Changepassword} />
                  <Route path="/servicenetworks" component={ServiceNetwork} />
                  <Route
                    path="/servicenetworks-branch"
                    component={BranchLink}
                  />
                  <Route
                    path="/servicenetworks-gardacenter"
                    component={GardaLink}
                  />
                  <Route
                    path="/servicenetworks-workshop"
                    component={WorkshopLink}
                  />
                  <Route path="/inputprospect" component={InputProspect} />
                  <Route
                    path="/premiumsimulation"
                    component={PremiumSimulation}
                  />
                  <Route path="/orderapproval" component={OrderApproval} />
                  <Route path="/prospectlead" component={ProspectLead} />
                  <Route
                    path="/quotationdetails"
                    component={QuotationDetails}
                  />
                  <Route
                    path="/orderapproval-detail"
                    component={OrderApprovalDetail}
                  />
                  <Route
                    path="/tasklist-followupdetail"
                    component={FollowUpDetails}
                  />
                  {/* <Route path="/tasklist-edit" component={TaskListEdit} /> */}
                  <Route path="/tasklist-details" component={TaskListDetails} />
                  <Route
                    path="/tasklist-surveydocuments"
                    component={SurveyDocuments}
                  />
                  <Route
                    path="/premiumsimulation-quotationdetails"
                    component={QuotationDetailsSimulation}
                  />
                  {/* <Route path="" component={Home} /> */}
                  {/* line below use for direct unknown path to home page or 404 page*/}
                  <Route path="*" exact={true} component={Home} />
                </Switch>
              </Suspense>
            </ErrorBoundary>
          )}
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
