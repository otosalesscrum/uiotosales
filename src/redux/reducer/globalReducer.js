import ActionType from "./globalActionType";

const globalState = {
    otosalesGlobal: 0
};

const rootReducer = (state = globalState, action) => {
    switch (action.type) {
        case ActionType.SELF_FUNC:   
            state = {...state, [action.self_key]: action.self_state}
            break;
    
        default:
            break;
    }
    return state;
};

export default rootReducer;
