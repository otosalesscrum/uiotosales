import React, { Component } from "react";
import {
  A2isLabel,
  A2isText,
  A2isTextArea,
  A2isCombo,
  A2isRadio,
  A2isCheckbox,
  A2isButton,
  A2isTable,
  A2isPager,
  A2isTime,
  A2isDate,
  A2isDateTime,
  A2isTabs,
  A2isList,
  A2isListAdapter,
  TaskListAdapter
} from "./components/a2is.js";
import "./css/a2is.css";

class UILayout extends Component {
  constructor() {
    super();
    this.state = {
      value: "Sample label update value",
      visible: true,
      disable: false,
      readonly: false,
      required: true,
      selected: 0,
      pageno: 2,
      pagecount: 5
    };
  }

  onclickbuttonsample = () => {
    alert("anda click button");
  };

  onChangeValue = val => {
    this.setState({ value: val });
  };

  onDisabled = () => {
    if (this.state.disable) this.setState({ disable: false });
    else this.setState({ disable: true });
  };

  onreadonly = () => {
    if (this.state.readonly) this.setState({ readonly: false });
    else this.setState({ readonly: true });
  };

  onGetValueText = () => {
    alert(this.state.value);
  };

  onSetValueText = () => {
    this.setState({ value: "test new value" });
  };

  onChangeComboValue = val => {
    this.setState({ selected: val });
  };
  onGetValueCombo = () => {
    alert(this.state.selected);
  };

  onChangePager = val => {
    this.setState({
      pagecount: val.pagecount,
      pageno: val.pageno
    });
  };

  onGetValuePager = () => {
    alert(
      "Pageno : " +
        this.state.pageno +
        " || Pagecount : " +
        this.state.pagecount
    );
  };

  render() {
    return (
      <div>
        <p className="text-center">Sampe body content for website & child</p>
        <hr />

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Sample Layout Framework</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#sampleLayout"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="sampleLayout">
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isTextArea
                  name="txtarealeft"
                  caption="Input text area"
                  minLength={0}
                  maxLength={10}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
                <A2isText
                  name="txtleft"
                  caption="Input textbox"
                  minLength={0}
                  maxLength={10}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
              </div>
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isCombo
                  onChangeCombo={value => this.onChangeComboValue(value)}
                  items={[
                    { text: "Beijing", value: "1" },
                    { text: "Hong Kong", value: "2" },
                    { text: "Seoul", value: "3" },
                    { text: "Sidney", value: "4" },
                    { value: "5", text: "Tokyo" }
                  ]}
                  name="cboLayoutsample"
                  caption="Input combobox"
                  readonly={false}
                  visible={true}
                  disabled={false}
                />

                <A2isCheckbox
                  name="chkSample"
                  caption="Checkbox"
                  dataitems={["satu", "dua"]}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
                <A2isRadio
                  name="rdoSample"
                  caption="Radio"
                  dataitems={["satu", "dua", "tiga", "empat", "lima"]}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Sample Action Framework</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#sampleGetValue"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="sampleGetValue">
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isText
                  name="txtsample"
                  caption="Sample textbox"
                  onChangeText={value => this.onChangeValue(value)}
                  minLength={0}
                  maxLength={20}
                  value={this.state.value}
                  readonly={this.state.readonly}
                  visible={this.state.visible}
                  disabled={this.state.disable}
                  required={this.state.required}
                />

                <div className="col-xs-12 col-sm-9">
                  <A2isLabel
                    name="lblsample"
                    caption={this.state.value}
                    visible={true}
                    align="left"
                  />
                </div>
              </div>
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isButton
                  name="btnSample"
                  caption="Sample button"
                  readonly={false}
                  visible={true}
                  disabled={false}
                  buttonstyle="success"
                  icons="book"
                  onclick={this.onclickbuttonsample}
                />
                <A2isButton caption="Disabled" onclick={this.onDisabled} />
                <A2isButton caption="readonly" onclick={this.onreadonly} />
              </div>
            </div>

            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isCombo
                  onChangeCombo={value => this.onChangeComboValue(value)}
                  items={[
                    { text: "Beijing", value: "1" },
                    { text: "Hong Kong", value: "2" },
                    { text: "Seoul", value: "3" },
                    { text: "Sidney", value: "4" },
                    { value: "5", text: "Tokyo" }
                  ]}
                  name="combosample"
                  caption="Sample combobox"
                  readonly={this.state.readonly}
                  visible={this.state.visible}
                  disabled={this.state.disable}
                />
              </div>
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isButton
                  caption="Set Value Text"
                  buttonstyle="warning"
                  onclick={this.onSetValueText}
                />
                <A2isButton
                  caption="Get Value textbox"
                  buttonstyle="warning"
                  onclick={this.onGetValueText}
                />
                <A2isButton
                  caption="Get Value Combo"
                  buttonstyle="danger"
                  onclick={this.onGetValueCombo}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Layout Datetime Picker</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#samplePicker"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="samplePicker">
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isTime
                  name="txttimesample"
                  caption="Sample time picker"
                  //value="10:00"
                  format="HH:mm"
                  minTime="08:00"
                  maxTime="18:00"
                  intervals={1}
                  breakTime={{ from: "12:00", to: "13:00" }}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
                <A2isDate
                  name="txtdatesample"
                  caption="Sample date picker"
                  //value="25-09-2018"
                  format="dd-MM-yyyy"
                  minDate="08:00"
                  maxDate="18:00"
                  offDay={["2019-01-01", "2019-02-01"]}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
              </div>
              <div className="col-xs-6 col-sm-6 col-md-6">
                <A2isDateTime
                  name="txtdatetimesample"
                  caption="Sample date time picker"
                  format="dd-MM-yyyyy HH:mm"
                  minTime="08:00"
                  maxTime="18:00"
                  breakTime={{ from: "12:00", to: "13:00" }}
                  minDate="08:00"
                  maxDate="18:00"
                  offDay={["2019-01-01", "2019-02-01"]}
                  readonly={false}
                  visible={true}
                  disabled={false}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Layout Table & Paging</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#PagingTable"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="PagingTable">
            <div className="row">
              <A2isTable
                name="sampleTable"
                rowscount={10}
                header={[
                  "Column 1",
                  "Column 2",
                  "Column 3",
                  "Column 4",
                  "Column 5",
                  "Column 6"
                ]}
                dataitems={[
                  [
                    "Row 1 column 1",
                    "Row 1 column 2",
                    "Row 1 column 3",
                    "Row 1 column 4",
                    "Row 1 column 5",
                    "Row 1 column 6"
                  ],
                  [
                    "Row 2 column 1",
                    "Row 2 column 2",
                    "Row 2 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 3 column 1",
                    "Row 3 column 2",
                    "Row 3 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 4 column 1",
                    "Row 4 column 2",
                    "Row 4 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 5 column 1",
                    "Row 5 column 2",
                    "Row 5 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 6 column 1",
                    "Row 6 column 2",
                    "Row 6 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 7 column 1",
                    "Row 6 column 2",
                    "Row 7 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 8 column 1",
                    "Row 6 column 2",
                    "Row 8 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 9 column 1",
                    "Row 6 column 2",
                    "Row 9 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ],
                  [
                    "Row 10 column 1",
                    "Row 6 column 2",
                    "Row 10 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3",
                    "Row 1 column 3"
                  ]
                ]}
              />
            </div>
            <A2isPager
              name="pagerSample"
              readonly={false}
              visible={true}
              disabled={false}
              pageno={this.state.pageno}
              pagecount={this.state.pagecount}
              onGetPaging={value => this.onChangePager(value)}
            />
            <A2isButton
              caption="GetValue pager"
              buttonstyle="warning"
              onclick={this.onGetValuePager}
            />
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Layout Data list</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#Datalist"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="Datalist">
            <A2isList
              name="sampleList"
              rowscount={5}
              multipleselect={true}
              dataitems={[
                "data 1",
                "data 2",
                "data 3",
                "data 4",
                "data 5",
                "data 6"
              ]}
            />

            <A2isListAdapter
              name="sampleList"
              rowscount={5}
              multipleselect={true}
              dataitems={[
                {
                  id: "1",
                  namelokasi: "namelokasi1",
                  alamat: "lokasi1",
                  phone: "0293820",
                  fax: "09238"
                },
                {
                  id: "2",
                  namelokasi: "namelokasi1",
                  alamat: "lokasi1",
                  phone: "0293820",
                  fax: "09238"
                },
                {
                  id: "3",
                  namelokasi: "namelokasi1",
                  alamat: "lokasi1",
                  phone: "0293820",
                  fax: "09238"
                },
                {
                  id: "4",
                  namelokasi: "namelokasi1",
                  alamat: "lokasi1",
                  phone: "0293820",
                  fax: "09238"
                },
                {
                  id: "5",
                  namelokasi: "namelokasi1",
                  alamat: "lokasi1",
                  phone: "0293820",
                  fax: "09238"
                }
              ]}
            />

            <TaskListAdapter
              name="sampleList"
              rowscount={5}
              multipleselect={true}
              dataitems={[
                {
                  id: "1",
                  name: "TaskList Adapter",
                  lastfollow: "lokasi1",
                  nextfollow: "0293820",
                  status: "1"
                },
                {
                  id: "2",
                  name: "TaskList Adapter",
                  lastfollow: "lokasi1",
                  nextfollow: "0293820",
                  status: "2"
                },
                {
                  id: "3",
                  name: "TaskList Adapter",
                  lastfollow: "lokasi1",
                  nextfollow: "0293820",
                  status: "3"
                },
                {
                  id: "4",
                  name: "TaskList Adapter",
                  lastfollow: "lokasi1",
                  nextfollow: "0293820",
                  status: "4"
                },
                {
                  id: "5",
                  name: "TaskList Adapter",
                  lastfollow: "lokasi1",
                  nextfollow: "0293820",
                  status: "5"
                }
              ]}
            />
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading text-right">
            <span className="panelheader">Layout Tabs</span>
            <A2isButton
              buttonstyle="info"
              toggle="collapse"
              target="#TabLayout"
              caption="expand"
            />
          </div>
          <div className="panel-body collapse" id="TabLayout">
            <A2isTabs>
              <div label="Tab1">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </div>
              <div label="Tab2">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
                normal distribution of letters, as opposed to using 'Content
                here, content here', making it look like readable English. Many
                desktop publishing packages and web page editors now use Lorem
                Ipsum as their default model text, and a search for 'lorem
                ipsum' will uncover many web sites still in their infancy.
                Various versions have evolved over the years, sometimes by
                accident, sometimes on purpose (injected humour and the like).
              </div>
              <div label="Tab3">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by
                injected humour, or randomised words which don't look even
                slightly believable. If you are going to use a passage of Lorem
                Ipsum, you need to be sure there isn't anything embarrassing
                hidden in the middle of text. All the Lorem Ipsum generators on
                the Internet tend to repeat predefined chunks as necessary,
                making this the first true generator on the Internet. It uses a
                dictionary of over 200 Latin words, combined with a handful of
                model sentence structures, to generate Lorem Ipsum which looks
                reasonable. The generated Lorem Ipsum is therefore always free
                from repetition, injected humour, or non-characteristic words
                etc.
              </div>

              <div label="Tab4">aasd kmdlaskd ldmakmd. asdas;lmdsas</div>
            </A2isTabs>
          </div>
        </div>

        {/* https://react-responsive-modal.leopradel.com/ */}
        {/* <Modal open={this.state.showModal} onClose={this.onCloseModal} center>
                                <div className="modal-content" style={{margin:"-11px"}}>
                                <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">hkjashdj ashjksa</h5>
                                </div>
                                <div className="modal-body">
                                "Sample detail body modal yang bisa di isi content div class botstrap"
                                </div>
                                <div className="modal-footer">
                                <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary btn-sm">Save changes</button>
                                </div>
                                </div>
                        </Modal>
                        <A2isButton name="btnModal" 
                                caption="Launch demo modal"  
                                readonly={false} 
                                visible={true} 
                                disabled={false}
                                buttonstyle="success"
                                icons="book"
                                onclick={this.onOpenModal}
                                /> */}
      </div>
    );
  }
}

export default UILayout;
