import React from "react";
import ReactDOM from "react-dom";
import { Suspense, lazy } from "react";
import registerServiceWorker from "./registerServiceWorker";
import {
  initializeFirebase,
  askForPermissioToReceiveNotifications,
  deleteTokenFCMandLogOut
} from "./push-notification";
// import { pdfjs } from 'react-pdf';


// redux
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./redux/reducer/globalReducer";

import "react-toastify/dist/ReactToastify.css";
import "./css/a2is.css";
import "react-block-ui/style.css";
import "loaders.css/loaders.min.css";
import "react-image-lightbox/style.css";
import "./otosalescomponents/style.css";
import "font-awesome/css/font-awesome.min.css";
import "./otosalescomponents/otosalesdatepickerstyle.css";
import { Util } from "./otosalescomponents";
import { secureStorage, decryptCryptoJS, encryptCryptoJS } from "./otosalescomponents/helpers/SecureWebStorage";
import { ACCOUNTDATA, API_VERSION_0219URF2019, API_URL, HEADER_API } from "./config";

import OtosalesModalInfo from "./otosalescomponents/otosalesmodalinfo";
import OtosalesLoading from "./otosalescomponents/v2/loading/otosalesloading";

// import Login from"./login";
// import UIOtosales from"./UIOtosales";

// const OtosalesModalInfo = lazy(() => Util.retry(() => import("./otosalescomponents/otosalesmodalinfo")));
// const OtosalesLoading = lazy(() => Util.retry(() => import("./otosalescomponents/v2/loading/otosalesloading")));
const Login = lazy(() => Util.retry(() => import("./login")));
const UIOtosales = lazy(() => Util.retry(() => import("./UIOtosales")));

// pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

class Index extends React.Component {
  constructor(props) {
    super(props);
    console.log("CONSTRUCTING INDEX");
    this.state = {
      showModal: false,
      isModalShowed: false,
      message: "Your session expired, please login again",
      user: null,
      password: null,
      isLoggedIn: false,
      userInfo: [],
      expiredSession: null
    };
    this.componentDidMount.bind(this);
    initializeFirebase();
  }

  componentDidMount() {
    console.log(
      window.location.pathname.replace(/^\/([^\/]*).*$/, '$1') +""
      );
    // // CRYPTOJS DECRYPT
    let message1 = 'U2FsdGVkX1878OlYkHQHSEmSBdfzRTRzo92DS6RGeMteL4E7Ou5fe9Zh6NvSDLuMrVO73h1bUsLAkHKyo9NPgq2lhnIQyeWuY0s5zRy0iUN6sdSO+EpLF6K7mRoxoE7bgdGomJ45BbjJo1XXVoWYph4BPtYii6bwulNUcNTYYDF/2YHCruw68fJMir9cpJw+h+gBeiFEn6LZbPjkzvk46VtUyfrxyr4OcMENAiAwsW/Yhosbqnph/4lRSd/Sf3gcJqqnoTIDeXZ9PG6BXnpeP2TDkqDWtvamZ1wuubGXhx/dtCI98pRYiK/2hXHrp657yzTqxEnIsMhH2sNdZa/fCCz8alRdZUzTG8XphkOngmWixJ+/TJCaP+rTWa/L4OiMkn5cpkMiP6piIeF/ZTW4BcIHk+8HsleUim0u9sJrAqsgPStnUImrUVKIO3W/C4AVcKZrRJ63kKL26P1Bjqn//CMLqaDf+sd9tTh4SqB0Vith4Qh8qtS0exAIBrQAo+d3LRBWysXLfGHwWQFlFmAGLpW6FFmm0NRYmFT22xz6O1r4zMRkXfW/U5FRCQ5WqujHOabWTeMxqj8HE/bvm2WA2DV7DbXMlox635aFeJpMdMdFnhNPBkfXuiAfBBFXP97yebZhGOCrLdAPAIGvwNELtWvlDAXbxHaZa7nFxqHMuegkGCr+8amjKWW7jZ0F3iabb8DF2MXqEF8ZUD5wfQsAwXLIiDegV7CUKJJKW44qeNsfj2YbOfno/mcVkyc0ZEKsshnW2tPlEZ/4lqkz4T6tWF2yxADYjE+4GfSCe4vGhiDddnJgdef2P7Mn/HGJ6UPkteD/5HdBtYTaPC2GvGTJPypsA93s2LV/ML9sMCTLbhQeTtY8BCKLpGMItzvphry+7QrqOqD2zAzCpgccytRCw1u9LDO+JP3rJz4yL1Skc7wEpgv/O5G3QrVw/4hrB590EawXEaCnAbTPZytHOU8dEsaEhYfl2gHxumSCTkojhuKedMytkmkiwS0Pjgk1xp0iL8mitn5ZPl3kj2GPDz1i7KkP0Tj5hlKkH/gnTuAiPog4X3pY11LxqK8f0+oRt1BvgtXPV/xGlTDxHhPHsTXWrp65j+YfIDmiMf15BPGLzbywBOcgKYlbRnUaa7GCaHuvSYtSH8IS0JjL++tHLe7sZKLthN9RiVA6/4KEZFg4+GAPELunZ8zXijz8hGFVHLWjG26aicPsL5fYRLZg2jboU7jJnoYUxVIQFcEFM899b6fKq4q3U7En3R/wUF6QhH4pPDt5YTjK9AaYKFRZ10H1VATmDse4wQT7L6xGq++51zrK4NOZ6/sNTNdZh+U87gu7u1ErBQS+nByTT2RtFZZ0mSbJ7R1SWbDf76EPYu5Ot3eWlX9Dk1WjuBuFUxaHOnl4GOXxjtFua3FEjL9jmFU6ZVYr3gziAfm8IyU6nkoXKrVkE+58L4pSVqOF29mZkEAydYE7fxDo/hqUJvO+ZjIhY7WHytzyWTrn7PCK60gPfvF7KrPNWDVxQnsSFNpW1KJDRi+KJZwEn8yVhJYe7Sn8eUPJ0IK29l2nQgwa6DUItAmSVgr5bKQ7V2wUysi+sQE0SuuFT7JWMkPYkNBtATK2kjy1BMbOM+jrSD1V8ORv6BZQM7v0TYreGkuIFkxed8aRi+HZhMhTkmyUwnUCN4gDZfb1phbKRuPqggZSqh0i9EIUujGEtbRYyHajFWqvLFq7728RaJ26YCS6qVrP48Wa3Ov7hNq1itJSqZ+4IxT/d22E6N3AdNcGHk/gKKejzJc27Z4sFZWfxvGIo6gkn0LXsZpld7njrr92HXinnBk9KeLH9Wt5IcT2En4s5aJAdJmwgYOcfPPr8QSrZXZorbQtHqqCPER5d1UQIMJHN/FErA8tMokcOERk4/YF6irNawpjQBWutAxt3U7/nDyzO6VRGE8yhnW6u8tz6cFPn70KZ3PLgpcXxlupYv5ELeb0MwYPw6IVFxgFvRFaaPBBjygMB2+9cIZ4oq4ZpQREOwanuQRIxsvgq4Pb4pGTCXtv6rWju6aBSGWVpQRiTnO3Mx4o2XllXKqiRzD4GDzOFWZktBZjminCn1fUJ+7AUmO0a1zNJ7q/3dChtV3OxiQbi3vH3onfPleZFvlVC/Ltrx55nFBb1+vwerMPz9FN8CkOk4DsqyzfqCa8eh+R+Nf/8RDf1LBNwGXwuyNIHdXVCc6h0CZ1PSfq1CGvC5wpMKtzPWpjJaCB+zY2Cpey4chXmpxtA0GPjWpPPkhjzr2PJ2lmkJcFwO6BRCoRyB5CqY1JCe1Nn/CKMdMCKL9YLMDs59BYfKCmg7/sKEFbx7Ph3aDQUEpb6BU8FLrIXUSoAJirRT7+6ZpomxsqieyAKh/uEOvE3pyY9hbuwa6KBBiElWAd7qhobQ6DuXw20pkgc8Ik/o/0vgguYURce7aVhu5xmk8GVKl8WivZY+enC/ohfy3+nOaxn5jX1cDGZOi2xCdK38Fi74HZkjlxOliLn8VgRtMW5TWHH79OlZyGiTEJkSYQrlW6EboDGHnSeEfWU8x8aZIUArREGzcaIwpPjzgBGmKufrh+h2fyxQrIZi7u2rjSbB7E9uAw7IaN4vLa54LCWLP5gucxlYDX5rXgiqsjumyTgwL6YAqY/Ngy9jNxj/qju7BOoKKGQzsyIDtMA1uJwM6NLouiUtiYN+q5LKGm3Gt5MCPoZ1hW71scZbk5RpCKliinUBRc8mxes3RZniI/pV1FifJhlyiiHT0iHTWPK2ugd4s3ZkXntYPrCE76+hW3t14xiD5N5AGnydQSZ1Z/RrlDAxZCLV6phHXGk1R/ot1SdlF16zRDzkNewdCwOU8ZhWy4uHMQcn3fNGUvGu1mdG81cIzBopw0YGHWfdJZs0m9ZQlaawK9Q5SSinGflvXOZBmRoRNfWYKvUT9K0rOepwHd'
    let decrypt = decryptCryptoJS(message1);
    console.log("decrypt :",decrypt);

    // // CRYPTOJS ENCRYPT
    // let messageEncrypt = '{"UserInfo":{"User":{"SalesOfficerID":"HUN","Email":"hsusanto@asuransiastra.com","Name":"Hoki Michael Susanto","Role":"BRANCHMGR","Class":","Password":null,"Validation":","Expiry":"2019-10-25 11:35:59.000","Password_Expiry":"2019-10-25 11:35:59.000","Department":","BranchCode":"017","Phone1":","Phone2":","Phone3":","OfficePhone":"(061) 4525 900","Ext":","Fax":"(061) 4518 800","ExtUserID":null,"Channel":null,"ChannelSource":null,"IsCSO":0,"EntryDate":"2016-08-24 16:23:17.200","LastUpdatedTime":"2016-08-24 16:23:17.200","RowStatus":1,"Key":{}},"Region":"SUMATERA","ReportTo":"yulianto@asuransiastra.com","FormAccessCollections":[{"FormID":"TASKLIST","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"INPUTPROSPECT","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"APPROVALORDER","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"ORDERSIMULATION","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"DASHBOARD","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1},{"FormID":"INFO","CanRead":0,"CanWrite":0,"CanAdd":1,"CanDelete":1}]},"IsAuthenticated":1,"Status":"OK","Token":"o36kYTjZ85TPYd9umhl67VDmgSHvl2fhcBCACu6a7PdU8ghKzoFiZUuqH5DugPyr13Fo5FDGTKZRwtYuFjjY85RxqJ0dGi9p6OOOdKdUraxXWgDPVvJHgSM4aaKMENOYcrwL57N2DgiUWYtFroBac5XNjqZp/8rkzP3dZs3Z+58kt88VrHpPFkyWFAEIoB5+UyVBCsvY8QiQYGOKH7KrM7Hepk99ui/RJI5YqoP00WZdHujrU5kW6p1tcss6Oaa3","ValidHour":120}';
    // let encrypt = encryptCryptoJS(messageEncrypt);
    // console.log("encrypt :", encrypt)
    const sessionAccount = JSON.parse(secureStorage.getItem("account"));
    console.log("INDEX MOUNTED!");
    if (sessionAccount != undefined) {
      this.setState({
        isLoggedIn: sessionAccount.IsAuthenticated,
        menu: sessionAccount.UserInfo.FormAccessCollections,
        user: sessionAccount.UserInfo.User.SalesOfficerID,
        userInfo: sessionAccount.UserInfo
      });
      // this.checkSessionExpiry(this.state.expiredSession);
    }
  }

  onCloseModal = () => {
    this.setState({ showModal: false });
    this.signOut();
  };

  shouldComponentUpdate(nextProps, nextState) {
    // THE COMPONENT WILL NOT UPDATE IF MODAL SHOWED
    if (nextState.showModal && nextState.isModalShowed) {
      return false;
    } else {
      return true;
    }
  }

  checkSessionExpiry = () => {
    console.log("starting check session expiry");
    var checkLogin = setInterval(() => {
      let time = Util.convertDate(secureStorage.getItem("timeout"));
      let now = Util.convertDate();
      if (this.state.userInfo == null) {
        this.setState({
          userInfo: JSON.parse(secureStorage.getItem("account")).UserInfo
        });
      }
      if (this.state.isLoggedIn) {
        if (window.location.pathname != "/tasklist-followupdetail") {
          secureStorage.removeItem("FollowUpEditStatus-statusInfo");
        }
        if (now >= time) {
          console.log("expired session");
          // this.onCloseModal();
          // THIS IS THE PROBLEM OF INFINITE LOOP OF RENDERING PAGE
          this.setState({ showModal: true }, () => {
            deleteTokenFCMandLogOut();
          });
        } else {
          // console.log("session");
        }
        if (this.state.isModalShowed) {
          // GUARDING SO NO LEAK MEM
          console.log("sessionexpired");
          clearInterval(checkLogin);
        }
      } else {
        console.log("sessionexpired");
        clearInterval(checkLogin);

        window.history.pushState(null, null, "/");
      }
    }, 500);
  };

  signIn(username, password, result) {
    // setSecretKey(result.Token);
    secureStorage.setItem("account", JSON.stringify(result));
    let expirytimelogin = Util.convertDate();
    expirytimelogin.setHours(
      expirytimelogin.getHours() + parseInt(result.ValidHour)
    );
    secureStorage.setItem("timeout", expirytimelogin);

    result = JSON.parse(secureStorage.getItem("account"));
    // var UserInfo = result.UserInfo;

    // this.setState({
    //   user: username,
    //   password: password,
    //   token: result.Token,
    //   userInfo: UserInfo,
    //   menu: result.UserInfo.FormAccessCollections,
    //   isLoggedIn: result.IsAuthenticated
    // });

    
    window.location.reload(true);

    return result.status;
  }

  signOut() {
    this.setState({ user: null, isLoggedIn: false });
    deleteTokenFCMandLogOut();
    this.hitIsNotActive();
    document.body.classList.remove("sidebar-open");
    /// HIT API
    secureStorage.clear();
    console.log("signout");
    window.history.pushState(null, null, "/");
  }

  hitIsNotActive = () => {
    let account = JSON.parse(ACCOUNTDATA);
    Util.fetchAPIAdditional(
      `${API_URL+""+API_VERSION_0219URF2019}/DataReact/updateUserIsActive`, 
      "POST",
      HEADER_API,
      {
        SalesOfficerID: account.UserInfo.User.SalesOfficerID,
        IsLogin : 0
      }
    )
  }

  RenderingPage = isLoggedIn => {
    // isLoggedIn=true;//for bypass not login

    let IndexPage = <Login onSignIn={this.signIn.bind(this)} />;

    if (isLoggedIn) {
      if (this.state.showModal && !this.state.isModalShowed) {
        this.setState({ isModalShowed: true });
      } else {
        askForPermissioToReceiveNotifications(this.state.user);
        this.checkSessionExpiry();
      }

      IndexPage = (
        <UIOtosales
          menu={this.state.menu}
          onSignOut={this.signOut.bind(this)}
          username={this.state.user}
          userInfo={this.state.userInfo}
        />
      );
    }
    return (
      <React.Fragment>
        <OtosalesModalInfo
          message={this.state.message}
          showModalInfo={this.state.showModal}
          onClose={this.onCloseModal}
        />

        <Suspense
          fallback={
            <OtosalesLoading
              className="col-xs-12 panel-body-list loadingfixed"
              isLoading={true}
            />
          }
        >
          {IndexPage}
        </Suspense>
      </React.Fragment>
    );
  };

  render() {
    return this.RenderingPage(this.state.isLoggedIn);
  }
}

const rootElement = document.getElementById("root");

// Store Redux
const storeRedux = createStore(rootReducer);

if (rootElement.hasChildNodes()) {
  ReactDOM.hydrate(<Provider store={storeRedux}><Index /></Provider>, rootElement);
} else {
  ReactDOM.render(<Provider store={storeRedux}><Index /></Provider>, rootElement);
}
registerServiceWorker();
