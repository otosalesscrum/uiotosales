import React, { Component } from "react";
import { API_URL, API_VERSION, API_VERSION_2 } from "./config";
import { ToastContainer, toast } from "react-toastify";
import BlockUi from "react-block-ui";
import { Loader } from "react-loaders";
import OtosalesModalInfo from "./otosalescomponents/otosalesmodalinfo";
import "./otosalescomponents/otosalesdatepickerstyle.css";
import { secureStorage } from "./otosalescomponents/helpers/SecureWebStorage";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalAlert: false,
      titlealert: "",
      user: "",
      password: "",
      confirmpassword: "",
      email: "",
      loginStatus: false,
      wantRegister: false,
      isLoading: false,
      isError: false,
      messageError: "Please fill all fields"
    };
  }

  onCloseModalAlert = () => {
    this.setState({ showModalAlert: false });
  };

  onShowModalAlert = (titlealert, messageError) => {
    this.setState({
      showModalAlert: true,
      messageError: messageError || null,
      titlealert: titlealert || null,
    });
  };

  handleSignUp(e) {
    const { email, password, confirmpassword } = this.state;
    e.preventDefault();

    if (email == "" || password == "" || confirmpassword == "") {
      this.setState({
        isError: true,
        messageError: "Please fill all fields"
      });
      toast.dismiss();
      toast.warning("❗ " + this.state.messageError, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
      return;
    }

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      this.setState({
        isError: true,
        messageError: "Invalid email. Please try again"
      }, () => {
        this.onShowModalAlert("Sign Up Failed", this.state.messageError);
      });
      // toast.dismiss();
      // toast.warning("❗ " + this.state.messageError, {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      return;
    }

    if (!/^(?=.*[A-z])(?=.*\d).{8,20}$/.test(password)) {
      this.setState({
        isError: true,
        messageError:
          "Password must be 8 - 20 characters and is a combination of character and number"
      }, () => {
        this.onShowModalAlert("Sign Up Failed", this.state.messageError);
      });
      // toast.dismiss();
      // toast.warning("❗ " + this.state.messageError, {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      return;
    }

    if (password != confirmpassword) {
      this.setState({
        isError: true,
        messageError: "Password doesnt match"
      }, () => {
        this.onShowModalAlert("Sign Up Failed", this.state.messageError);
      });

      // toast.dismiss();
      // toast.warning("❗ " + this.state.messageError, {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      
      return;
    }

    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION}/Authentication/AccountActivation/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded"
      }),
      body: "username=" + this.state.email + "&password=" + this.state.password
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.status) {
          this.setState({ wantRegister: false });
          toast.dismiss();
          toast.info("✔️ Berhasil sign up", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        } else {
          console.log("gagal");
          toast.dismiss();
          // toast.error("❗ Sign up - " + jsn.Message, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
          
        this.onShowModalAlert("Sign Up Failed", jsn.Message);
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log("parsing failed", error);
        toast.dismiss();
        toast.warning("❗ Server error - Gagal sign up", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        });

        this.setState({ isLoading: false });
      });

    this.setState({
      email: "",
      password: "",
      confirmpassword: ""
    });
  }

  handleSignIn(e) {
    e.preventDefault();

    if (this.state.user == "" || this.state.password == "") {
      // toast.dismiss();
      // toast.warning(`❗ Please fill in all required fields.`, {
      //   position: "top-right",
      //   autoClose: 5000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true
      // });
      this.onShowModalAlert("Login Failed", "Please fill in all required fields.");
      return;
    }

    if (
      this.state.user.length > 3 &&
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.user)
    ) {
      // toast.dismiss();
      // toast.warning(
      //   `❗ Invalid username. Asuransi Astra employees please use your 3 letters. Other users please use your registered email address.`,
      //   {
      //     position: "top-right",
      //     autoClose: 5000,
      //     hideProgressBar: false,
      //     closeOnClick: true,
      //     pauseOnHover: true,
      //     draggable: true
      //   }
      // );
      this.onShowModalAlert("Login Failed", "Invalid username. Asuransi Astra employees please use your 3 letters. Other users please use your registered email address.");
      return;
    }

    this.setState({ isLoading: true });
    fetch(`${API_URL + "" + API_VERSION_2}/Authentication/login/`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded"
      }),
      body: "userID=" + this.state.user + "&password=" + this.state.password
    })
      .then(res => res.json())
      .then(jsn => {
        if (jsn.IsAuthenticated) {
          secureStorage.setItem("account", JSON.stringify(jsn));
          secureStorage.removeItem("isStopHitAPI");
          this.props.onSignIn(this.state.user, this.state.password, jsn);
        } else {
          if (jsn.Status == "Authentication Failed") {
            // toast.dismiss();
            // toast.warning(`❗ Invalid username or password.`, {
            //   position: "top-right",
            //   autoClose: 5000,
            //   hideProgressBar: false,
            //   closeOnClick: true,
            //   pauseOnHover: true,
            //   draggable: true
            // });
            this.onShowModalAlert("Login Failed", "Invalid username or password.");
          } else {
            toast.dismiss();
            toast.warning(`❗ ${jsn.Status}`, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
            });
          }
          // toast.dismiss();toast.error(`❗ Gagal Log in - ${jsn.Status}`, {
          //   position: "top-right",
          //   autoClose: 5000,
          //   hideProgressBar: false,
          //   closeOnClick: true,
          //   pauseOnHover: true,
          //   draggable: true
          // });
        }

        this.setState({ isLoading: false });
        // console.log(jsn);
      })
      .catch(error => {
        // toast.dismiss();
        // toast.error(`❗ Gagal Log in ${error}`, {
        //   position: "top-right",
        //   autoClose: 5000,
        //   hideProgressBar: false,
        //   closeOnClick: true,
        //   pauseOnHover: true,
        //   draggable: true
        // });
        this.onShowModalAlert("Network Connection is Not Connected", "Mobile data is not active. Use Wi-Fi or active your mobile data and try again.");
        console.log("parsing failed", error);

        this.setState({ isLoading: false });
      });
  }

  onChangeUser = event => {
    this.setState({
      user: event.target.value
    });
  };
  onChangePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  onChangeFunction = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    const { wantRegister, isError, messageError } = this.state;
    return (
      <div style={{ backgroundColor: "#18516c", height: "100%" }}>
        <BlockUi
          tag="div"
          className="container-fluid backgroundcolorotosales"
          blocking={this.state.isLoading}
          loader={
            <Loader active type="ball-spin-fade-loader" color="#02a17c" />
          }
          message="Loading, please wait"
        >
          <ToastContainer />
          <OtosalesModalInfo
            showModalInfo={this.state.showModalAlert}
            message={this.state.messageError}
            title={this.state.titlealert}
            onClose={this.onCloseModalAlert}
          />
          <div
            className="row panel-primary"
            style={{ backgroundColor: "#18516c" }}
          >
            {" "}
            {/*style={{border:"1px solid grey", ,padding:"5px 5px 5px 5px"}} */}
            <div className="col-xs-12 col-sm-8 col-md-9 col-lg-9 hidden-xs text-center">
              {" "}
              {/*style={{backgroundColor:"black", height:"100%" }}*/}
              <img
                src="img/logo.png"
                style={{ height: "100vh", padding: "50px" }}
              />
            </div>
            <div
              className="col-xs-12 col-sm-4 col-md-3 col-lg-3 text-center hidden-xs"
              style={{ height: "100vh", backgroundColor: "#18516c" }}
            >
              <div
                className="panel panel-info"
                style={{ marginTop: "15px", height: "90vh" }}
              >
                <div className="panel-body">
                  {wantRegister ? (
                    <form id="FSigUp" onSubmit={this.handleSignUp.bind(this)}>
                      <div className="form-group form-group-sm ">
                        <span className="logo-lg">
                          <img
                            className="img-circle"
                            src="img/logo.bak.png"
                            style={{ height: "100px", marginBottom: "10px" }}
                          />
                          <b>
                            <br />
                            Sign Up Otosales
                          </b>
                        </span>
                      </div>
                      <div className="form-group form-group-sm hidden">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-user-secret" />
                          </div> */}
                          <select className="form-control"> </select>
                        </div>
                      </div>
                      <div className="form-group form-group-sm">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
                          <input
                            onChange={this.onChangeFunction}
                            type="text"
                            style={{ marginBottom: "0px" }}
                            name="email"
                            value={this.state.email}
                            className="form-control "
                            placeholder="Enter email"
                          />
                        </div>
                      </div>
                      <div className="form-group form-group-sm">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-asterisk" />
                          </div> */}
                          <input
                            onChange={this.onChangeFunction}
                            type="password"
                            name="password"
                            value={this.state.password}
                            className="form-control "
                            placeholder="Enter password"
                          />
                        </div>
                      </div>
                      <div className="form-group form-group-sm">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-asterisk" />
                          </div> */}
                          <input
                            onChange={this.onChangeFunction}
                            type="password"
                            name="confirmpassword"
                            value={this.state.confirmpassword}
                            className="form-control "
                            placeholder="Enter confirm password"
                          />
                        </div>
                      </div>
                      {isError ? (
                        <div className="col-md-12">
                          <div
                            className="alert alert-warning"
                            id="errorNoteQuestion"
                          >
                            <strong>Warning!</strong> {messageError}.
                          </div>
                        </div>
                      ) : null}
                      <button type="submit" className="btn btn-sm btn-primary">
                        Sign Up
                      </button>
                      <br />
                      <a
                        href="#"
                        onClick={() => this.setState({ wantRegister: false })}
                      >
                        Login
                      </a>
                    </form>
                  ) : (
                    <form id="FSigIn" onSubmit={this.handleSignIn.bind(this)}>
                      <div className="form-group form-group-sm ">
                        <span className="logo-lg">
                          <img
                            className="img-circle"
                            src="img/logo.bak.png"
                            style={{ height: "100px", marginBottom: "10px" }}
                          />
                          <b>
                            <br />
                            Login Otosales
                          </b>
                        </span>
                      </div>
                      <div className="form-group form-group-sm hidden">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-user-secret" />
                          </div> */}
                          <select className="form-control"> </select>
                        </div>
                      </div>
                      <div className="form-group form-group-sm">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-pencil" />
                          </div> */}
                          <input
                            onChange={this.onChangeUser}
                            type="text"
                            style={{ marginBottom: "0px" }}
                            name="user"
                            value={this.state.user}
                            className="form-control "
                            placeholder="Enter username"
                          />
                        </div>
                      </div>
                      <div className="form-group form-group-sm">
                        <div className="input-group input-group-sm">
                          {/* <div className="input-group-addon ">
                            <i className="fa fa-asterisk" />
                          </div> */}
                          <input
                            onChange={this.onChangePassword}
                            type="password"
                            name="password"
                            value={this.state.password}
                            className="form-control "
                            placeholder="Enter password"
                          />
                        </div>
                      </div>
                      <button type="submit" className="btn btn-sm btn-primary">
                        Log In
                      </button>
                      <br />
                      {/* not show sign up button */}
                      {/* <a
                        href="#"
                        onClick={() => this.setState({ wantRegister: true })}
                      >
                        Sign up
                      </a> */}
                    </form>
                  )}
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3 text-center hidden-lg hidden-md hidden-sm">
                <div>
                  <div className="panel-body" style={{ margin: "5%" }}>
                    {wantRegister ? (
                      <form
                        id="FSigUpMobile"
                        onSubmit={this.handleSignUp.bind(this)}
                      >
                        <div className="form-group form-group-sm ">
                          <span className="logo-lg">
                            <img
                              className="img"
                              src="img/logo.png"
                              style={{ height: "200px", marginBottom: "10px" }}
                            />
                            <b style={{ color: "#fff" }}>
                              <br />
                              <a
                                href="#"
                                onClick={() =>
                                  this.setState({ wantRegister: false })
                                }
                                style={{
                                  color: "#87b8e2",
                                  marginRight: "10px"
                                }}
                              >
                                Login
                              </a>
                              <a
                                href="#"
                                onClick={() =>
                                  this.setState({ wantRegister: true })
                                }
                                style={{ color: "#fff" }}
                              >
                                Sign up
                              </a>
                            </b>
                          </span>
                        </div>
                        <div className="form-group form-group-sm hidden">
                          <div className="input-group input-group-sm">
                            {/* <div className="input-group-addon ">
                              <i className="fa fa-user-secret" />
                            </div> */}
                            <select className="form-control"> </select>
                          </div>
                        </div>
                        <div style={{ marginBottom: "8px" }}>
                          <div className="row textlabellogin">
                            <span
                              className="label"
                              style={{ color: "#87b8e2", fontWeight: "400" }}
                            >
                              Email
                            </span>
                          </div>
                          <label htmlFor="emailres" className="inp">
                            <input
                              type="text"
                              id="emailres"
                              placeholder=" "
                              onChange={this.onChangeFunction}
                              name="email"
                              autoComplete="off"
                              value={this.state.email}
                            />
                            {/* <span className="label">Email</span>
                            <span className="border" /> */}
                            <span className="label" />
                            <span className="border" />
                          </label>
                        </div>
                        <div style={{ marginBottom: "8px" }}>
                          <div className="row textlabellogin">
                            <span
                              className="label"
                              style={{ color: "#87b8e2", fontWeight: "400" }}
                            >
                              Password
                            </span>
                          </div>
                          <label htmlFor="passres" className="inp">
                            <input
                              type="password"
                              id="passres"
                              placeholder=" "
                              onChange={this.onChangeFunction}
                              name="password"
                              autoComplete="off"
                              value={this.state.password}
                            />
                            {/* <span className="label">Password</span>
                            <span className="border" /> */}
                            <span className="label" />
                            <span className="border" />
                          </label>
                        </div>
                        <div style={{ marginBottom: "8px" }}>
                          <div className="row textlabellogin">
                            <span
                              className="label"
                              style={{ color: "#87b8e2", fontWeight: "400" }}
                            >
                              Confirm Password
                            </span>
                          </div>
                          <label htmlFor="conpassres" className="inp">
                            <input
                              type="password"
                              id="conpassres"
                              placeholder=" "
                              onChange={this.onChangeFunction}
                              name="confirmpassword"
                              autoComplete="off"
                              value={this.state.confirmpassword}
                            />
                            {/* <span className="label">Confirm Password</span>
                            <span className="border" /> */}
                            <span className="label" />
                            <span className="border" />
                          </label>
                        </div>
                        {/* <div className="form-group form-group-sm">
                          <div className="input-group input-group-sm">
                            <div className="input-group-addon ">
                              <i className="fa fa-pencil" />
                            </div>
                            <input
                              onChange={this.onChangeFunction}
                              type="text"
                              style={{ marginBottom: "0px" }}
                              name="email"
                              value={this.state.email}
                              className="form-control "
                              placeholder="Enter Email"
                            />
                          </div>
                        </div> */}
                        {/* <div className="form-group form-group-sm">
                          <div className="input-group input-group-sm">
                            <div className="input-group-addon ">
                              <i className="fa fa-asterisk" />
                            </div>
                            <input
                              onChange={this.onChangeFunction}
                              type="password"
                              name="password"
                              value={this.state.password}
                              className="form-control "
                              placeholder="Enter Password"
                            />
                          </div>
                        </div> */}
                        {/* <div className="form-group form-group-sm">
                          <div className="input-group input-group-sm">
                            <div className="input-group-addon ">
                              <i className="fa fa-asterisk" />
                            </div>
                            <input
                              onChange={this.onChangeFunction}
                              type="password"
                              name="confirmpassword"
                              value={this.state.confirmpassword}
                              className="form-control "
                              placeholder="Enter Confirm Password"
                            />
                          </div>
                        </div> */}
                        {/* {isError ? (
                          <div className="col-md-12">
                            <div
                              className="alert alert-warning"
                              id="errorNoteQuestion"
                            >
                              <strong>Warning!</strong> {messageError}.
                            </div>
                          </div>
                        ) : null} */}
                        <button
                          type="submit"
                          className="btn btn-sm btn-primary"
                        >
                          Sign Up
                        </button>
                      </form>
                    ) : (
                      <form
                        id="FSigInMobile"
                        onSubmit={this.handleSignIn.bind(this)}
                      >
                        <div className="form-group form-group-sm ">
                          <span className="logo-lg">
                            <img
                              className="img"
                              src="img/logo.png"
                              style={{ height: "200px", marginBottom: "10px" }}
                            />
                            <b style={{ color: "#fff" }}>
                              <br />
                              <a
                                href="#"
                                onClick={() =>
                                  this.setState({ wantRegister: false })
                                }
                                style={{ color: "#fff", marginRight: "10px" }}
                              >
                                Login
                              </a>
                              {/* not show sign up button */}
                              {/* <a
                                href="#"
                                onClick={() =>
                                  this.setState({ wantRegister: true })
                                }
                                style={{ color: "#87b8e2" }}
                              >
                                Sign up
                              </a> */}
                            </b>
                          </span>
                        </div>
                        <div className="form-group form-group-sm hidden">
                          <div className="input-group input-group-sm">
                            {/* <div className="input-group-addon ">
                              <i className="fa fa-user-secret" />
                            </div> */}
                            <select className="form-control"> </select>
                          </div>
                        </div>
                        {/* <div style={{ marginBottom: "8px" }}>
                          <label htmlFor="userlog" className="inp">
                            <input
                              type="text"
                              id="userlog"
                              placeholder=" "
                              onChange={this.onChangeUser}
                              name="user"
                              autoComplete="off"
                              value={this.state.user}
                            />
                            <span className="label">Username</span>
                            <span className="border" />
                          </label>
                        </div> */}
                        <div style={{ marginBottom: "8px" }}>
                          <div className="row textlabellogin">
                            <span
                              className="label"
                              style={{ color: "#87b8e2", fontWeight: "400" }}
                            >
                              Username
                            </span>
                          </div>
                          <label htmlFor="userlog" className="inp">
                            <input
                              type="text"
                              id="userlog"
                              placeholder=" "
                              onChange={this.onChangeUser}
                              name="user"
                              autoComplete="off"
                              value={this.state.user}
                            />
                            <span className="label" />
                            <span className="border" />
                          </label>
                        </div>
                        {/* <div>
                          <label htmlFor="passlog" className="inp">
                            <input
                              type="password"
                              id="passlog"
                              placeholder=" "
                              onChange={this.onChangePassword}
                              name="password"
                              autoComplete="off"
                              value={this.state.password}
                            />
                            <span className="label">Password</span>
                            <span className="border" />
                          </label>
                        </div> */}
                        <div>
                          <div className="row textlabellogin">
                            <span
                              className="label"
                              style={{ color: "#87b8e2", fontWeight: "400" }}
                            >
                              Password
                            </span>
                          </div>
                          <label htmlFor="passlog" className="inp">
                            <input
                              type="password"
                              id="passlog"
                              placeholder=" "
                              onChange={this.onChangePassword}
                              name="password"
                              autoComplete="off"
                              value={this.state.password}
                            />
                            <span className="label" />
                            <span className="border" />
                          </label>
                        </div>
                        {/* <div className="form-group form-group-sm">
                          <div className="input-group input-group-sm">
                            <div className="input-group-addon ">
                              <i className="fa fa-pencil" />
                            </div>
                            <input
                              onChange={this.onChangeUser}
                              type="text"
                              style={{ marginBottom: "0px" }}
                              name="user"
                              value={this.state.user}
                              className="form-control "
                              placeholder="Enter username"
                            />
                          </div>
                        </div> */}
                        {/* <div className="form-group form-group-sm">
                          <div className="input-group input-group-sm">
                            <div className="input-group-addon ">
                              <i className="fa fa-asterisk" />
                            </div>
                            <input
                              onChange={this.onChangePassword}
                              type="password"
                              name="password"
                              value={this.state.password}
                              className="form-control "
                              placeholder="Enter Password"
                            />
                          </div>
                        </div> */}
                        <br />
                        <button
                          type="submit"
                          className="btn btn-sm btn-primary"
                        >
                          Log In
                        </button>
                      </form>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-12" />
              {/* <Footer /> */}
            </div>
          </div>
        </BlockUi>
      </div>
    );
  }
}

export default Login;
