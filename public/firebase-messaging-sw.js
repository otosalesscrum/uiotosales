importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
firebase.initializeApp({
    messagingSenderId: "643082169723"
});
const messaging = firebase.messaging();

// self.addEventListener('notificationclick', (event) => {
//     if (event.action) {
//         clients.openWindow(event.action);
//     }
//     event.notification.close();
// }); 

// setInterval(() => {
//     let token = secureStorage.getItem("tokenfcm");
//     console.log("Console.log ya :",token);
// },1000);

self.addEventListener('notificationclick', function (event)
{
    //For root applications: just change "'./'" to "'/'"
    //Very important having the last forward slash on "new URL('./', location)..."
    const rootUrl = new URL('/', location).href; 
    event.notification.close();
    event.waitUntil(
        clients.matchAll().then(matchedClients =>
        {
            for (let client of matchedClients)
            {
                console.log(rootUrl);
                console.log("test fcm");
                if (client.url.indexOf(rootUrl) >= 0)
                {
                    return client.focus();
                }
            }

            return clients.openWindow(rootUrl).then(function (client) { 
            try{
                client.focus();
            }catch(e){
                console.log(e)} 
            });
        })
    );
});